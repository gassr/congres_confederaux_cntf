

.. _contre_motion_20_2010_ste93:

===========================================
Contre-motion à la motion N°20 (STE93)
===========================================


Les CCN servant aussi à coordonner des campagnes confédérales et même parfois à
en décider en fonction de l'actualité, le STE93 demande l'ajout suivant (en gras)

**La CNT se dote d'un secrétariat « suivi des décisions de congrès et de CCN » ayant
comme mandat d'informer les syndicats de l'avancée des différentes décisions prises
durant le congrès et les CCN. (...)**


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion  n°20    |            |          |            |                           |
| STE 93                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_20_2010`
