

.. _annonce_compte_rendu_xxxii_congres:

===============================================================
Annonce du compte-rendu partiel du XXXIIe Congrès CNT Metz 2012
===============================================================


Courriel
========

::

    Sujet:  [Liste-syndicats] 1ere circulaire confédérale
    Date :  Sat, 9 Feb 2013 19:41:24 +0100
    De :    Secrétaire Confédéral CNT <cnt@cnt-f.org>
    Pour :  Liste syndicat <liste-syndicats@bc.cnt-fr.org>


Bonjour camarades,

Vous trouverez en pièce jointe la circulaire confédérale n°1 (2 envois
sur la liste syndicat).

Elle comporte un relevé de conclusion du 32ème Congrès confédéral de Metz.

Un compte-rendu complet incluant les débats et les identifications de
votes vous sera adressé lors d'une prochaine circulaire.

Cette circulaire annonce le prochain CCN qui se déroulera les 1er & 2
Juin 2013 au siège de la CNT à Paris.

Le Bureau Confédéral attend les propositions d'ordre du jour des UR
avant le 14 Avril 2013.

Je vous rappelle que les circulaires confédérales et autres documents
confédéraux sont disponibles sur l'espace public de l'intranet confédéral

- https://www.intranet.cnt-fr.org/public.d/bc

Bonne réception!
Fraternelles salutations AS & SR
Manu - Secrétaire Confédéral CNT cnt@cnt-f.org


Introduction
============

Bonjour à toutes et tous,

Ci joint le relevé des motions adoptées lors de notre dernier congrès
de Metz.

Si vous relevez des erreurs, des manques, etc., merci de nous en faire
part en envoyant un mail à Tania : contact@cnt-f.org.

Par ailleurs le compte rendu complet du 32ème Congrès devrait être
finalisé pour fin Mars.

Désolée du délai, mais il a été difficile de retrouver toutes les notes
prises.

Salutations ASSR.
