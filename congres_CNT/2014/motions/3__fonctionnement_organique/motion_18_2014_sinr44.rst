
.. index::
   pair: Motion 18 ; 2014

.. _motion_18_2014_sinr44:

==================================================================================
Motion n°18 2014 : Redéfinition du terme "parti politique" (SINR 44, statutaire)
==================================================================================



Argumentaire
============

Si la situation du Secrétaire confédéral aux relations média lors des
dernières élections municipales a été clarifiée, cela a permis de
mettre en évidence l'insuffisance des statuts confédéraux sur le
thème des élections, qui pourraient être sujets à interprétation.

Autant clarifier les choses avant que le problème ne se pose réellement.

Motion
=======

Modification de :ref:`l'article 6 des statuts <article_6_statuts_CNT_2012>`

(en gras)

Les membres de la CA ne pourront occuper aucun poste responsable relevant
**d'une organisation** politique (**y compris toute sorte de groupement
se présentant à des élections, hors élections professionnelles**),
d'une secte philosophique ou religieuse.

Contre motions
==============

.. seealso::

   - :ref:`contre_motion_18_2014_stp67`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°18 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
