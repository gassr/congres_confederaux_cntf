

.. _motion_3_2008_etpic30:


=========================================================================
Motion n° 3 2008 de l’ETPIC 30 : Péréquation des frais de CCN, [adoptée]
=========================================================================

**Présentation de l’ETPIC 30**
	En deux points mais proposée en un bloc.
	Décidé au dernier CCN, la péréquation suppose qu'au final, chaque syndicat,
	même les organisateurs, aient les mêmes frais de transports.
	Elle doit favoriser la venue de deux mandats pour favoriser la collégialité.
	Le point n°2 de la motion doit servir au débat. La péréquation doit-elle
	se mener sur toutes les UR (mêmes absentes) pour favoriser la venue de tout
	le monde ?

**SSEC 59**
	demande le retrait du terme « si nécessaire ».

**STE 93**
d'accord dans l'ensemble, mais peut être que si des UR ne participent pas, c'est aussi à cause de problèmes
financiers ?

**Interco 34**
	sur le point n° 2, abstention. On considère que c’est un vœu pieu. Préférence sur un seul délégué.
	Proposition que la péréquation n'intègre qu'un délégué.

**Informatique RP**
	Des régions ne viennent pas en CCN pour cause de problèmes financiers ?

**Santé social 86**
	Solution cache misère. Les UR qui ont des problèmes financiers, c'est qu'il y a trois « pélots ». L a
	péréqu atio n av ec les U R absen tes, s'il n'y a pas de sanction, cela ne sert à rien...

**ETPIC 30**
	La réflexion est à avoir sur la question de la sanction ou non. Puisque la question est posée les
	fédérations ne rentrent pas dans la constitution des CCN. La composante du CCN est les UR...

**Santé social RP**
	on vote la motion en entier ou point par point.

**Max (à la table)**
	tableaux d'enregistrement électronique des votes sont au point.
	Se base sur un logiciel de reconnaissance vocale.
	Contrainte: pas trop de bruits...*


**ESS 34**
	ce système à déjà été utilisé au congrès de la fédération éducation...
	proposition de double prise de votes...

**SSEC 59**
	Concernant l'amendement sur le « si nécessaire » avez vous proposer deux votes...

**Présidence**
	est ce que des syndicats peuvent apporter des amendements à des motions présentes dans le cahier ?

**SCIAL RP**
	il faut se rappeler le congrès d'Agen. Pour nous, une motion doit être présentée « en l'état », et s'il y a
	problème... on avise.

**Educ 57**
	dans ce cas là, on n’a pas besoin de faire de congrès...

**CS RP**
	L'idée c'était de dire. On propose le vote: si la motion passe, il n'y a pas de problème. Si elle ne passe pas on
	peut proposer des amendements?


**Santé social 75**
	les motions ne sont adoptées telle quelles, uniquement s'il y a unanimité...

**ESS 34**
	vote en deux fois est possible. Si le syndicat qui a proposé la motion le souhaite, elle peut aussi retirer sa
	motion.

**Educ 57**
	possibilité de votes en contradictoire.

**ETPIC 30**
	Nécessaire d'explication du changement de texte pour bien comprendre.

**SSEC59**
	dans l'esprit, on est pour cette motion...pas grave le « si nécessaire ».

**ETPIC 30**
	« si nécessaire » signifiait de ne pas avoir à chaque fois impliquer le/la trésorièrE confédéralE.

**Interco 31**
	Nécessaire de se prononcer sur les amendements une fois pour toutes...


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 32         | 6        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

**Pour : 32**
    (32 syndicats non identifiés)


**Contre: 6**
	Educ 35, SSEC 59, Interco 32, STIS 59,
	Interco 86, santé social 86


**Abstention: 7**
	Interco 45, STE 93, santé social RP,
	ETPIC 94, SII RP, Interco 34, Interco
	91

**NPPV: 2**
    éduc 57, Interco 54.

**MOTION adoptée**



**Interco 54**
	la péréquation est quelque-chose d'habituel et est même un principe et de solidarité,
	« Répartition des richesses »...

**Présidence**
	pour en finir avec cette histoire, on peut trancher pour « ce » congrès si on essaye d'organiser une
	péréquation.

**Interco 31**
	péréquation s'inscrit de fait dans le fonctionnement du congrès...La solidarité devrait faire que tous les
	syndicats partagent...


Vote pour organiser une péréquation pour le congrès 2008 de Lille
------------------------------------------------------------------

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 36         | 1        | 4          | 6                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


**Pour : 36**
    (36 syndicats non identifiés)

**Contre: 1**
    Interco 32

**Abstention: 4**
    Interco 29, SCIAL RP, Interco 91, SUB 35

**NPPV: 6**
	SII RP, santé social 86, Interco 48,
	Interco 86, PTT 35, SUB 69

**Adopté**


Le STTE 33 se propose pour l’organisation de cette péréquation
