
.. _strategie_syndicale_congres_2010:

================================================================================================
Strategie syndicale (action syndicale, développement cohérent, élections professionnelles, etc.)
================================================================================================

.. toctree::
   :maxdepth: 2

   motion_36
   motions_37_38_39
   motion_38
   motion_39
   motion_40
   motion_41
   motion_42
   motion_43
   motion_44
   motion_56
