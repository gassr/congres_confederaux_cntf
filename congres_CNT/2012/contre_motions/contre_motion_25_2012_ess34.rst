

.. _contre_motion_25_2012_ess34:

================================================================================================
Contre-motion à la motion N°25 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_25_2012_etpic30`



Motion n°25
===========

La CNT s’oppose aux Pesticides clonés brevetés de tout genre dont les OGM.

La CNT se porte solidaire et soutient les collectifs anti-OGM.

Contre motion
=============

La CNT s’oppose aux Pesticides clonés brevetés de tout genre dont les OGM.

La CNT soutient les luttes anti-OGM.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°25     |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
