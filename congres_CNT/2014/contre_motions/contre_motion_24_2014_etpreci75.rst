
.. _contre_motion_24_2014_etpreci75:

=====================================================
Contre motion à la motion numéro 24 2014 (ETPRECI75)
=====================================================

.. seealso::

   - :ref:`motion_24_2014_ste75`



Argumentaire
=============

Nous comprenons la motion Création d'un comité site Internet comme la volonté
de rendre plus collective l'animation du site confédéral et de ne pas laisser
cette dernière à la charge de spécialiste. En cela nous sommes parfaitement en
adéquation avec l'esprit de cette motion.

En revanche, les modalités de mise en œuvre telles que proposées dans la motion
nous semblent d'une part ignorer la réalité pratique de cette animation,
d'autre part le découpage entre tâches de conception et d'exécution tel que
proposé nous rappelle la division taylorienne du travail et est à mille lieux
d'un fonctionnement autogestionnaire.

Afin de mieux cerner ce que pourrait être un fonctionnement collectif pour
l'animation du site web confédéral, il convient de rappeler le fonctionnement
actuel.

Le webmaster récupère un certain nombre de textes (tracts, communiqués, activités
et publications des syndicats, le sommaire du dernier CS et la une, l'ensemble
de la production confédéral – tracts, communiqués) et les publie sur le site.

Ils apparaissent du plus récent au plus ancien dans le fil d'actualité. Par
ailleurs, les structures de la CNT – que ce soit des syndicats, des ul, ud, ur
ou les fédérations – peuvent proposer elles-mêmes pour publication leur texte.

Actuellement rares sont les structures qui le font d'elles-mêmes : sur les deux
dernières années seule une petite dizaine l'ont fait. Un certain nombre
d'éléments sont mis en avant automatiquement sur la page d'accueil du site (le
dernier communiqué ou le dernier tract confédéral tient lieu d'éditorial, les
productions du SV, le dernier CS, le dernier livre des éditions CNT-RP) ;
d'autres le sont du fait du webmaster par l'association de mots-clés (les
éléments du slider, le dossier à la une). Enfin, il est possible de créer des
pages ex-nihilo pour des besoins spécifiques (exemples :
http://www.cnt-f.org/spip.php?page=secteurVideo, http://www.cnt-f.org/agenda/,
http://www.cnt-f.org/comite-soutien-defense-paris-populaire-33-rue-
vignoles.html, http://www.cnt-f.org/annuaire-des-syndicats-et-unions-locales-de-
la-cnt.html). Elles demandent plus de compétences techniques.

Contre-motion
==============

Le/la ou les mandaté-e-s pour l'animation du site Internet ont pour tâche de
faire vivre le site en le mettant à jour régulièrement et en valorisant
l'activité de la CNT.

Pour cela, plusieurs tâches (non exhaustives) relèvent de leur mandat:

- publier le sommaire du dernier Combat syndicaliste chaque mois ;
- mettre en ligne un ou plusieurs articles parus dans le CS selon leur pertinence
  (en privilégiant l'activité syndicale) ;
- le pôle média, en lien avec le comité de rédaction du CS et les syndicats,
  collecte les illustrations (photos, dessins, etc.) permettant d'égayer le site ;
- un-e mandaté-e par leur UR (ou par leur syndicat en cas d'absence d'UR) se
  charge de proposer des articles pour le site confédéral, à défaut, le pôle
  média (SRM, SV, Webmaster) se charge de récolter les articles ;
- le pôle média, en lien avec la CA, définit l'actualité «chaude » pour la CNT
  et décide de leur mise en avant (slider, dossier à la une, partie SV) ;
- une publication régulière, si possible mensuel, d'un éditorial,
  rédigé par le pôle média et validé par la commission administrative
  confédérale, s'ajoute aux communiqués de presse rédigés par le SRM ;
- créer, selon les besoins, des pages « thématiques » permettant d'enrichir
  le site web confédéral.

Afin de faciliter et transmettre les connaissances techniques liées à l'usage du
web (compréhension du fonctionnement d'un site, capacités de gestion technique),
le congrès favorise le mandatement de plus d'un-e camarade pour le mandat de
webmaster.

À charge pour ces mandaté-e-s, selon les possibilités, de transmettre
autant que faire ce peut, par des formations, des brochures, le fonctionnement
d'un site sous SPIP et la compréhension du web.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°24 2014 <motion_24_2014_ste75>`

       :ref:`ETPRECI75 <etpreci75>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
