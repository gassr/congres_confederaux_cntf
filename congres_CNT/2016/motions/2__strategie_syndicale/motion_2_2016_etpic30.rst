
.. index::
   pair: Motion 2 ; 2016

.. _motion_2_2016_etpic30:

============================================================================================
Motion n°2 2016: Élections dans les TPE (Très Petites Entreprises) (ETPIC 30) [adoptée]
============================================================================================



Argumentaire
=============

Depuis l’instauration des élections au sein des TPE en 2012 (élection
qui a lieu tous les 4 ans), la confédération a fait le choix d’y
participer mais sans que cela soit acté officiellement (même si à
l’occasion des CCN et congrès confédéraux, il n’y a eu aucune opposition
des syndicats et Unionsrégionales).

Ces élections ne peuvent que donner à la CNT une visibilité (Diffusion
de tract à des milliers d’exemplaires). Cependant, bien que les
professions de foi se doivent d’être généralistes, les revendications
que nous pouvons et devons y porter doivent être parlantes et concrètes
pour les salarié-es des différents secteurs concernés (restauration,
Bâtiment, agriculture, tertiaires etc...) notamment sur le renforcement
du droit syndical. La publication de différents matériels et supports
dans le cadre de la propagande, ne peut se faire qu’à condition qu’il y
ait un vrai travail collectif de l’ensemble des secteurs au sein de la
CNT, à travers les syndicats (d’industrie comme dans les interco) et les
fédérations.

La CNT peut être une force syndicale dans les TPE, en ce qui concerne
la réalité de précarité des travailleur-euses relevant des TPE (incluant
les structures associatives). Les salarié-es sont actuellement isolé-es
et sans protection juridique en cas de problème avec leur employeur.
Absence de syndicat (moins de 11 salarié-es), de délégué-e du personnel.
Ils-elles font appel le plus souvent aux conseiller-es du salarié
lorsqu’il y a licenciement, ou directement à la DIRECCT.

Cet isolement fait le lit des attaques patronales et sont le quotidien
de milliers de travailleurs et de travailleuses. Si la solidarité en
interne dans l’entreprise ou en interprofessionnelle et l’action directe
sont l’une des meilleures façons de combattre le patronat, il n’en
demeure pas moins que l’absence de toute instance salariale et syndicale
est profitable pour le patronat.
Dès lors, comme c’est le cas dans les entreprises de plus de 11 salarié-es
avec les DP, et de plus de 50 salarié-es avec le CE et le CHSCT,
l’instauration de telles instances se voient nécessaires. Cela permet à
l’heure où la casse sociale s’organise, d’exiger l’extension du droit
syndical à l’ensemble des entreprises quel que soit l’effectif et à
travers lui, la protection collective pour l’ensemble des travailleur-euses


Motion
=========

Motion (points pouvant être votés séparément) :

- La CNT confirme sa participation aux élections des TPE à venir ;
- L’ensemble des secteurs représentés au sein de la confédération
  devront participer à la publication de différents matériels en y
  mettant les spécificités par secteur professionnel ;
- La CNT revendique l’instauration de CHSCT à l’ensemble des entreprises
  quel que soit son effectif, permettant d’assurer au vu des prérogatives
  de cette instance, la protection collective de l’ensemble des salarié-es.

Amendements
===========


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°2 2016

       :ref:`ETPIC30 <etpic30>`
     - 37
     - 8
     - 2
     - 2
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
