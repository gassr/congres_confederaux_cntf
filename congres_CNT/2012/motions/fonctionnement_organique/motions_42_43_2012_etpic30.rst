
.. _motions_42_43_2012:
.. _motions_42_43_2012_etpic30:

====================================================================================================================================================
Motions n°42, 43 2012 : Cohésion de la CNT/Définition de la suspension confédérale, Capacité du CCN en terme d’exclusion ETPIC 30 Sud
====================================================================================================================================================
  

.. seealso::

  - :ref:`motion_4_2010`
  - :ref:`motions_etpic30_2010`
  - :ref:`article_24_statuts_CNT_2010`



Préambule
=========

Comme toute organisation, la CNT peut être amenée à connaitre en son sein des
conflits ou des problèmes avec ou entre ses propres structures.

:ref:`L’article 24 <article_24_statuts_CNT_2010>` des statuts confédéraux
traite de ces questions.

Même si la suspension ou l’exclusion d’une structure est toujours un aveu
d’échec, elle peut s’avérer parfois indispensable, voire souhaitable.
Dans certains cas les plus difficiles, des dispositions devraient pouvoir
être pris dans les délais les plus courts.

En vue de renforcer la cohésion de la CNT, cette motion vient en complément
d’autres motions présentées par notre syndicat traitant de labellisation
(inclusion), ou de la dé-labellisation par les CCN.

Elle vise à modifier les modalités d’exclusion et à préciser les modalités
de suspensions.

L’article 24 des statuts indique que :

« Le non respect des Statuts et  règles organiques issues du Congrès est un
motif d’exclusion. [...]

« Dans le cas d’incapacité à résoudre le conflit au sein des structures
géographiques et/ou d’industrie, et dans le cas ou le conflit dépasserait
le simple cadre interne d’une union géographique ou d’industrie, ou encore
dans le cas où il impliquerait directement des organismes confédéraux,
celui-ci devra alors être soumis au prochain CCN (ou au CCN extraordinaire
convoqué dans les conditions prévues à l'article 4), qui a pouvoir de décision
provisoire, pouvant aller jusqu'à la suspension de la structure incriminée.

« La structure incriminée peut faire appel devant le Congrès.

« Le Congrès seul peut se prononcer définitivement. Et il est le seul à pouvoir
le faire en cas de demande d’exclusion d’une structure de la CNT. [...]

« L'organisme incriminé garde le droit de présenter directement sa défense
soit au CCN, soit au Congrès. Tout conflit présenté au CCN ou au Congrès
devra être inscrit à l'ordre du jour dans les délais. »

**Nos statuts ne définissent pas ce qu’est une suspension ni mêmes ses
conséquences. Elle est diférente de l’exclusion en ce sens qu’elle interrompt
momentanément  la capacité d’une structure CNT.**

On voit de même que *seul [le Congrès] peut se prononcer définitivement* sur
une exclusion. En d’autres termes, il serait le seul compétent à échéance
bi-annuelle. Or, les CCN sont habilités à labelliser un syndicat, une Union de
syndicat. Nous pensons naturelle que la réciproque soit possible : les CCN
doivent aussi être en capacité de procéder à une exclusion.

Cette modification de statuts permettrait dès lors une meilleure réactivité
de la CNT face aux situations les plus critiques.


.. index::
   pair: suspension; intégrale


.. _motion_42_2012:
.. _motion_42_2012_etpic30:

Motion 42 2012 : Définition de la suspension confédérale (ajout aux règles organiques associées aux statuts) (ETPIC30)
========================================================================================================================

La suspension est à portée confédérale et s’impose aux syndicats ou leur Unions.

Elle peut porter sur diférentes capacités, cumulatives ou non, des structures
précitées :

- Organique : droit de vote au Congrès & CCN, mandatement, présentation de
  motions, accès aux médias de la CNT
- Syndicale : interdiction de produire toute propagande au nom de la CNT
- Juridique : incapacité d’acter en justice au nom d’intérêts généraux ou
  collectifs de la CNT (en dehors des intérêts individuels et spécifiques
  des membres de membres des structures concernées).

La suspension est dite **intégrale** lorsqu’elle regroupe toutes les suspensions
organiques, syndicales, et juridiques.

La suspension s’inscrit dans une durée indéterminée.

Il appartient à un CCN ou  congrès confédéral de lever la suspension.

Toute structure concernée par une suspension conserve, comme l’indique les
statuts confédéraux, *le droit de présenter directement sa défense soit au
C.C.N., soit au Congrès*.

Elle est incitée à produire un argumentaire au  CCN ou congrès où elle peut
être entendue ou représentée expressément par  son Union régionale
d’appartenance.


Vote
----

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°42          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. _motion_43_2012:
.. _motion_43_2012_etpic30:

Motion statutaire 43 2012 : Capacité du CCN en terme d’exclusion (Art. 24) ETPIC30
===================================================================================

.. seealso::

   - :ref:`article_24_statuts_CNT_2010`

Suppression de la partie suivante : *Le Congrès seul peut se prononcer
définitivement. Et il est le seul à pouvoir le faire en cas de demande
d’exclusion d’une structure de la CNT.*

Remplacement par : *L’exclusion d’une structure de la CNT peut être ordonnée
par le Congrès ou le CCN*

Vote
-----

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°43          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements à la motion 43
--------------------------

- :ref:`amendement_motion_43_2012_stgl_rp`
