

.. _motion_58_2010:

===============================================================
Motion n°58 2010 : Critique des compétitions sportives, STE 75
===============================================================


.. seealso::

   - :ref:`motions_ste75`



Motion reprise par STE75, 2012
===============================

- :ref:`motion_27_2012`

Argumentaire
=============

**Mise en jambes** : Cette motion n'a pas pour but de juger des loisirs personnels de chacun, ni de condamner des
passions privées, des joies et souvenirs éventuels de notre enfance. Chacun peut courir ou jouer comme il lui
plaît. Mais comme nous avons tous grandi au sein du capitalisme où la concurrence règne en maître, nous
sommes donc pénétrés des contradictions qu'il porte en lui. Il n'est pas rare que nos loisirs alimentent les profits
de grandes entreprises multinationales, mais ce n'est pas le rôle du syndicat de normer nos vies privées. Par
contre, collectivement, il est utile de mener une analyse politique qui, peut-être, nous aidera à vivre en cohérence
avec nos idées. C'est du ressort de chacun. Nous avons des amis, nous vivons parfois de grands moments dans le
sport, comme c'est possible aussi au travail. Il est vrai qu'on s'amuse un peu plus dans le sport qu'au travail ! Cela
ne nous empêchera pas de porter la critique autant sur l'une que l'autre institution.

Attaques au centre et sur les ailes pour argumenter
+++++++++++++++++++++++++++++++++++++++++++++++++++

Le sport est une pratique sociale compétitive d'origine anglaise (on disait d'ailleurs «les sports anglais» : foot,
rugby, athlétisme, boxe, aviron, nautisme...) importée en France à la fin du XIXe siècle par l'aristocratie et la
grande bourgeoisie, dont le Baron de Coubertin en est sans doute le plus connu des représentants. Pour débattre
et argumenter, il faut donc bien distinguer le sport, qui est l'institution de la compétition physique, et l'activité
physique plus ou moins quotidienne, la marche, la gymnastique d'entretien. Ou encore les jeux traditionnels qui
le plus souvent sont seulement tolérés comme préparation aux sports de fédération.

Et dès lors, on comprend que la fièvre de la compétition sportive s'insinue dans le moindre aspect de nos vies,
tout comme le capital. Le capitalisme et le sport avancent dans le même mouvement depuis le début. Leur
logique commune, c'est la compétition, l'élimination des faibles par les forts, à l'image de la devise bien connue :
"Que le meilleur gagne !" C'est aussi entériner la nécessaire défaite du plus grand nombre qui doit être vécue
comme "naturelle". Tout ceci ne correspond pas à notre idéal de société. À la guerre entre les peuples, nous
opposons la solidarité internationale. À la domination du plus fort, nous opposons l’égalité. Les jeux olympiques
ou les coupes du monde donnent l’occasion d’une union sacrée des classes sociales derrière un drapeau, un
maillot, bref un bric-à-brac patriotard complètement factice pour le plus grand bénéfice des dirigeants politiques
et économiques.

Les grandes compétitions internationales sont des machines à fric dont les villes et les pays s'arrachent
l'organisation pour doper leurs entreprises et l'activité capitaliste sur leur territoire. Elles contribuent à concentrer
le pouvoir et le capital entre les mains des puissants, politiciens, flics et patrons. Les dirigeants en profitent pour
chasser les populations les plus démunies du centre-ville et y installer des consommateurs plus riches
(gentrification). L'extension sans fin du capitalisme réclame toujours de nouveaux marchés, et les compétitions
sportives en sont un des vecteurs puissants. Le rôle de la FIFA, par exemple, est de conquérir la planète et de
convertir les peuples à la religion du ballon rond. Coca-cola, Nike et Adidas s'installent ensuite durablement
dans le moindre recoin du monde. Si tout ceci n’est pas directement syndical, cela concerne tout de même les
populations les plus vulnérables de la planète, y compris dans les pays dits riches. Il s’agit bien de lutte des
classes. C'est pourquoi la mobilisation obligatoire à leur « fête » lors des grandes compétitions internationales
nous semble nécessiter à chaque fois des réponses adéquates parce que c’est l'émancipation des travailleurs et
des paysans de tous les pays qui est en jeu.

Droit au but, la motion
-----------------------

La CNT adopte une position critique face aux grandes compétitions internationales de type JO, coupes du monde
ou d'europe. Il s'agit là de grands évènements destinés avant tout à faire fructifier les profits des entreprises,
qu'elles soient clubs sportifs, sponsors, fabricants de matériel sportif ou constructeurs des infrastructures.

La CNT aura donc vocation à soutenir les diverses oppositions à ces grandes
compétitions (campagnes de boycotts, manifs et rassemblements, débats publiques,
publications de brochures…).

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°58          |            |          |            |                           |
| STE 75               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
