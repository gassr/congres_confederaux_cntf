
.. _amendement_motions_2_3_2010_ste93:


=====================================
Amendement aux motion 2 et 3 (STE 93)
=====================================


La commission juridique et de formation n’a pas vocation à monter des dossiers 
juridiques ni à régler les problèmes individuels, mais à conseiller les adhérents 
de la CNT lorsqu’elle le peut, à les mettre en  relation avec d’autres adhérents 
ayant déjà une expérience dans la question soulevée. 

La commission juridique et de formation publiera également des brochures de formation 
juridique à bas coût.


+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Amendement aux motion n°2 et 3 |            |          |            |                           |
| STE 93                         |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2010`
   - :ref:`motion_3_2010`
