
.. _contre_motions_congres_2021:

=======================================
Contre-motions du congrès CNT 2021
=======================================

- :ref:`cahier_mandatee_2021`

.. toctree::
   :maxdepth: 1

   contre_motion_1_2021_etpics94
   contre_motion_1_2021_ste75
   contre_motion_1_2021_simrp
   contre_motion_4_2021_etpreci75
   contre_motion_7_2021_ste72
   contre_motion_8_2021_cnt42
   contre_motion_8_2021_sinr44
   contre_motion_8_2021_ste72
   contre_motion_8_2021_interco69
   contre_motion_10_2021_cnt42
   contre_motions_10_2021_ste75
   contre_motion_10_2021_stics72
   contre_motion_12_2021_etpics94
   contre_motion_12_24_27_2021_ste75
   contre_motion_14_2021_cnt30
   contre_motion_14_2021_cnt09
   contre_motion_14_2021_etpics94
   contre_motion_15_2021_cnt30
   contre_motion_19_2021_stp67
   contre_motion_20_2021_cnt42
   contre_motion_24_2021_tasra
   contre_motion_12_24_2021_cnt30
   contre_motion_24_2021_educ38
   contre_motion_24_2021_interpro07
   contre_motion_24_2021_etpics94
   contre_motion_24_2021_stp67
   contre_motion_24_2021_ste75
   contre_motion_24_2021_simrp

- :ref:`congres_CNT_2021_motions`
- :ref:`congres_CNT_2021_amendements`
