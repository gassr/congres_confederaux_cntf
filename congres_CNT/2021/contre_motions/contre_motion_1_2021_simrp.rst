
.. _contre_motion_1_2021_simrp:

=========================================================================================================================
Contre-motion N°3 à la motion n°1 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **SIMRP**
=========================================================================================================================

- :ref:`motion_1_2021_ste33`


Autres motions SIMRP

    - :ref:`paris:motions_simrp`


Statut concerné
==================

- :ref:`statuts_conf:titre_1_statuts_cnt_2010`

Argumentaire
================

3 systèmes ?

Selon cette motion, "Le capitalisme, le patriarcat et le racisme sont 3 systèmes".

Le capitalisme est protégé par les lois et les règlements de l’Etat
qui garantissent les droits des propriétaires, des actionnaires etc...
on peut donc parler de système capitaliste.

Le patriarcat et le racisme sont protégés par quelles lois et quels
règlements en France ? Poser la question c’est y répondre. Il n’y aucune
loi qui protège ces deux fléaux. Il n’y a donc pas de système.

En Arabie Saoudite actuellement les femmes sont sous domination des hommes
non par un effet culturel mais par des lois.
En Afrique du Sud, du temps de la ségrégation, les noirs n’avaient pas
les mêmes droits que les blancs. La loi garantissait cet état de fait.
En France, l’église catholique ne permet pas aux femmes d’accéder aux
postes de la hiérarchie et il n’y a pas d’exception. C’est donc un système.

Ecrire dans nos statuts qu’en France aujourd’hui la situation des femmes
pourrait être comparable à celle des femmes d’Arabie Saoudite ou à
celle de l’Eglise catholique et que la situation des noirs est
semblable à celle des noirs d’Afrique du Sud du temps de l’apartheid,
nous semblerait une faute d’analyse grave.

Bien sûr, il existe du patriarcat et du racisme en France et il faut les
combattre.
En tant qu’organisation syndicale nous savons que la lutte des classes
n’est pas simple, mais nous savons aussi en tant qu’anarcho-syndicalistes
et syndicaliste révolutionnaires, que notre engagement contre la société
capitaliste s’accompagne d’une éthique de combat définie par nos statuts et
décisions de congrès ou l’égalité entre toutes et tous quels que soient
nos origines, nos couleurs, nos genres est établie de façon constante.

Ces thèmes ne sont donc pas d’autres luttes à mener, elle s’intègrent
dans l’action syndicale quotidienne lorsque nous les rencontrons car
rappelons nous, nous sommes une organisation syndicale. C’est à dire la
forme d’organisation et de combat  appuyé sur l’action directe de ses
adhérents qui dans la cadre de l’action surtout sur les lieux de travail,
s’oppose au patron et propose un chemin vers la "syndicalisation des moyens
de production etc... ".

Dans ce cadre de la lutte quotidienne, les syndicats de la CNT s’appuient
sur les résolutions de congrès relatives aux questions sociétales ;
(antiracisme, anti-sexisme, antimilitarisme, écologie etc...)

Et nous procédons ainsi car la construction de la CNT et son développement
sont basés sur le premier paragraphe de l’article 1.

"La Confédération Nationale du travail a pour but de grouper, sur le terrain
**spécifiquement économique**, sans autre forme de discrimination, pour la
défense de leurs intérêts matériels et moraux, tous les travailleurs/ses
à l'exception des employeurs/ses et des forces répressives de l'Etat
considéréEs comme des ennemiE des travailleurs/ses."

**La lutte des classes, une option parmi d’autres ?**

La dernière phrase de la motion proposée par le Ste 33 se conclut par
« La CNT entend bien se donner les moyens d’articuler et de prôner **aussi
bien** la lutte des classes que la lutte anti-patriarcale et antiraciste. »

La lutte des classes devient une option parmi d’autres ouvrant ainsi la
possibilité qu’on puisse adhérer à propos de l’une des trois options.

Le patriarcat et le racisme ne sont pas l’apanage que de la bourgeoisie.
Nous en rencontrons aussi dans la classe ouvrière. (nous en viendrons à
bout par l’éducation, la culture, la pédagogie)

L’antipatriarcat et l’antiracisme ne sont pas que l’apanage de la classe
ouvrière.. Nous en voyons aussi parmi les patrons et les institutions..

Du coup ces deux thèmes interclassistes les organisations politiques et
les institutions du monde capitaliste les portent sans problème et à
moindre frais.
En :ref:`PJ une pub de Uber dans « Le Monde » <annexe_contre_motion_1_2021_simrp>`

Alors pour la CNT, porter ces thèmes et en faire un choix parmi d’autres ?
C’est mettre en danger son modèle syndical.

Le racisme et le patriarcat traversent donc toutes  les couches de la
société et concernent tout le monde. Ces deux comportements ont en commun
de considérer l’inégalité comme inéluctable entre certains types d’êtres
humains.

Il faut donc rappeler y compris dans nos statuts, y compris dans l’article 1,
que pour la CNT l’EGALITE entre ses adhérents est le fondement de son
pacte associatif.

Raisons pour lesquelles nous refusons :ref:`la motion N°1 <motion_1_2021_ste33>`
et proposons la contre motion suivante


Contre-motion
===============

Intégrer "et à égalité", dans le :ref:`paragraphe 1 de l’Article 1 <statuts_conf:titre_1_statuts_cnt_2010>`

Ce qui donnerait si cette contre motion est approuvée par le congrès.

"La Confédération Nationale du Travail a pour but :

- De grouper, sur le terrain spécifiquement économique, sans autre forme
  de discrimination, et à égalité pour la défense de leurs intérêts matériels
  et moraux, tous les travailleurs/ses à l'exception des employeurs/ses
  et des forces répressives de l'Etat considéréEs comme des ennemiEs des
  travailleurs/ses."


Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 |            |          |            |                           |
| SIMRP                         |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
