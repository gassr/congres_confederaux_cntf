.. index::
   pair: Motion 20 ; 2021

.. _motion_20_2021_etpic30:

================================================================================================================================================
Motion **n°20** 2021: **Notre fédéralisme** par **ETPIC 30**
================================================================================================================================================

- :ref:`motion_identite_cnt_congres_1993_paris`
- :ref:`article_27_statuts_CNT_2014`
- :ref:`motion_recueil_2008`
- :ref:`charte_de_paris`


Autres motions ETPIC30

    - :ref:`motions_etpic30`


Préambule
===========

Au-delà du corpus des orientations adoptées par les Congrès successifs,
au-delà des régles de fonctionnement inscrites aux statuts et règles
organiques, il nous est apparu que **l’esprit de notre fédéralisme**
devait figurer dans un texte de référence commun, spécifique.

Une **forme de Charte** constituant notamment **un liant** entre des
règles de fonctionnement existantes, adoptées de Congrès en Congrès.

Nous avons voulu retranscrire dans cette motion portée au 35ème Congrès,
tantôt des éléments établis par les textes socles, tantôt des pratiques
et des usages ayant court à la Confédération.

Le **Fédéralisme est au cœur** de notre structuration, de notre identité,
de notre indépendance, de notre liberté, de notre syndicalisme,
de notre projet, et de notre vie démocratique.

Nous avons souhaité l’écrire pour constitution d’un cadre de référence
collectif des principes fédéralistes qui nous animent.

Nous souhaitons que le principe de l’association aux statuts confédéraux de
cette motion puisse être de même voté par le Congrès, indépendamment de la
motion elle-même et comme le prévoit :ref:`l’article 27 des statuts confédéraux <article_27_statuts_CNT_2014>`.

Si adoptée, elle sera rangée dans une partie spécifique: **Principes de fonctionnement**

Plus largement, comme l’avait déjà souhaité la commission de refonte des statuts
ayant rendue ses :ref:`travaux en 2008 <motion_recueil_2008>`, nous appelons de
nos vœux la **rédaction prochaine d’une Charte associée aux statuts** au profit
de la CNT.


Motion : **Notre Fédéralisme**
=================================

Organisation de lutte, porteuse d’un projet révolutionnaire, la CNT entend rendre
tous pouvoirs aux travailleu·r·se·s et aux syndicats auxquels ils·elles sont
adhérent·e·s

Elle vise à remplacer l’État et le capitalisme par un autre pouvoir, **un ordre social
reposant sur l’organisation de l’échange et de la répartition [...] assuré par
des rouages syndicaux à tous les degrés** (:ref:`Charte de Paris 1946 Congrès constitutif de la CNT <charte_de_paris>`).

Attachée aux principes clés **d’autogestion** (cf. :ref:`article 3 des statuts confédéraux <article_3_statuts_CNT_2014>`)
et de démocratie directe, la CNT se structure, assure sa cohésion, par le souhait
d’une généralisation d’un nouvel **ordre social fédéraliste**.

**Son organisation interne est non hiérarchisée, égalitariste, et pluraliste**.

La CNT se réclame d’un fédéralisme dit **libertaire**.

Mais à bien observer les différentes formes historiques que revêt ce principe
d’organisation collective, il ne suffit pas d’énoncer le fédéralisme libertaire
pour en assurer la définition, la constitution.

Nous traduisons ici notre conception cénétiste du fédéralisme, sa vocation,
ses principes d’organisations, sa structuration de nature contractuelle entre
syndicats.

Nous définissons ce que la CNT, organisation anarcho-syndicaliste et syndicaliste
révolutionnaire, entend par fédéralisme:

Notre fédéralisme, un schéma d’organisation militant, démocratique, à visée productive
========================================================================================

La CNT est attachée tant à la **démocratie sociale** qu'à la **démocratie économique**.

Par **démocratie**, la CNT entend rendre véritablement son *pouvoir au peuple*
(δημοκρατία / dēmokratía).

Pour la CNT, la **démocratie est consubstantiellement faite de fédéralisme**.

Ces deux principes d’organisation sont considérés comme inséparables, imbriqués,
dépendants dans leur substance politique.

Au **centralisme démocratique** (dans sa définition historique, autoritaire),
au parlementarisme, la CNT oppose son projet global d’organisation révolutionnaire
fédéraliste.
Un projet fédéraliste qui s’étend de la base au sommet, qui établit l’intérêt
général par voie de consultation d’échelon en échelon.

Dés lors, notre fédéralisme s'oppose au centralisme démocratique en ce sens
qu'il **donne une part plus importante prépondérante au local**.
Il assoit son cadre démocratique sur l’organisation d’un fédéralisme territorial.

Sur un plan général, plus global, ce fédéralisme territorial regroupe et organise
des syndicats dans un rapport de contractualisation libre et mutuel : La Confédération.

Selon un principe de mutualité, leurs liens sont dits :term:`synallagmatiques`, réciproques,
et :term:`commutatifs`.

Organisation de lutte de classe, la CNT regroupe des personnes, syndiquées et
syndicalistes, portées d’une même utopie révolutionnaire, d’une même besogne
syndicale.

Les rapports entre les syndicats de la CNT ou leurs adhérent·e·s sont réputés :term:`adelphiques`.

Les statuts confédéraux, les règles organiques, les décisions de Congrès,
constituent le cadre le fonctionnement et le socle d’adhésion mutuel de
la CNT: son pacte associatif.

Au sein de la Confédération, **chaque syndicat** est considéré comme autonome,
libre, et à égalité de ses pairs.

Les syndicats peuvent s’unir en Unions Locales et en Fédérations d’industries et
développer par ailleurs librement leurs actions, ensemble ou séparément.

Au-delà de leurs contributions respectives à la vie confédérale, les syndicats
font le choix par leur adhésion à la CNT de se conformer à son cadre de
fonctionnement et son corpus d’orientation.
Par voie de conséquence, les statuts des syndicats doivent être en cohérence
de but et de fonctionnement avec les statuts confédéraux (:ref:`art.2 <article_2_statuts_CNT_2014>`).

Leurs autonomies au sein de l’organisation confédérale est toutefois relative
puisque liées à leur adhésion à la Confédération et au cadre des engagements
mutuels qui en découlent.

Organisation interne et principes de cohésions de notre fédéralisme
====================================================================

Les statuts confédéraux, les règles organiques, les décisions successives de
Congrès et de CCN fixent le cadre de fonctionnement de l’organisation de la CNT.

Au-delà des textes et motions de référence, nous tenons ici toutefois à rappeler
quelques principes clés qui sous-tendent cette organisation, **son adelphité**.

Le mandatement : Soutenu, placé sous contrôle, et révocable
=============================================================

Autogestionnaires, démocratiques, les syndicats et leurs adhérent·e·s assurent
le contrôle de leurs propres mandaté·e·s.

Lorsque des mandaté·e·s sont désigné·e·s à des tâches ou des fonctions visant
à assurer techniquement ou administrativement un mandat défini par une Union
de syndicats ou la Confédération, ils et elles sont présenté·e·s par leurs
syndicat d’adhésion.

Il est ainsi entendu que le syndicat est en mesure de répondre du sérieux, de
la sincérité, et de la probité de adhérent·e candidat·e.
Il garantit par la suite la permanence d’adhésion de ses mandaté·e·s.

L’Union de syndicats **mandatrice** assure le contrôle de la tenue du mandat
tel qu’il a été défini par ses soins.

De façon motivée, la révocabilité du mandat peut donc intervenir de deux façons:

- Soit qu’elle s’étudie par l’Union de syndicat ayant défini et proposé le mandat,
- Soit qu’elle se décide directement par le syndicat d’adhésion du ou de la
  mandaté·e. Dans ce dernier cas de figure, le syndicat veille autant que possible
  à pallier la carence, provisoirement, et jusqu’à confirmation du nouveau
  mandatement par l’Union de syndicats intéressée.

Les syndicats mandatent ou désignent de même des représentant.·e·s délégué.·e·s
ou des commis·e·s aux différentes instances de la CNT et en ce qui concerne la
Confédération d’abord : Congrès Confédéral ou commissions confédérales.

A l’identique, et de façon proportionnée à la nature et à la durée de leur
mandatement, les syndicats peuvent recourir à la révocabilité de leurs délégué·e·s
dans les instances décisionnaires de la CNT.

Les syndicats et leurs Unions peuvent aussi prévoir un cadre de ratifications
aux mandatement de leurs délégations et des décisions prises par eux ou elles
(Aucune disposition allant en ce sens n’est en vigueur à ce jour à la CNT dans
les textes et motions de références de la Confédération)

De la recherche de consensus au droit des minorités de vote
=============================================================

La liberté démocratique dévolue à tout syndicat de la CNT, associée à son droit
à l'autonomie, le place dans un principe d'égalité.
Aucune partie n'est supérieure à l'autre face au pacte associatif de la CNT

Chaque syndicat peut ainsi librement et à loisir apporter sa contribution à la
réflexion, chercher à convaincre, et viser par ses motions à infléchir le
fonctionnement et les orientations de la CNT, que ce soit au Congrès ou en
assemblée générale de syndicats.
L’ensemble des outils de communications interne est mis à sa disposition à
titre permanent pour permettre la diffusion d’idées.

En Congrès et aux réunions confédérales en général, les modalités de vote
prévoient le temps du débat.
Bien que proportionné, ce temps est considéré comme nécessaire et ne saurait
être supprimé au seul profit des procédures de vote.

La recherche de consensus est ainsi privilégiée par un débat facilité en amont,
puis lors des délibérations.
La prise en considération :term:`adelphique` des avis tiers, voire contradictoires,
contribue à faire évoluer le consensus vers plus de **sagesse collective**.
La pluralité des points de vue, multiples et sincères, est par conséquent
considérée comme une richesse dans le cadre démocratique choisi par la CNT.

L'unanimité et ici considérée comme illusoire.

La recherche de consensus, le consensus évolutif, visent d’abord à tendre à
l'unanimité : Une unanimité spontanée ou une unanimité de consentement, mais
pas une unanimité de soumission.

Notre organisation fait le choix du recours à la majorité de vote pour les
décisions de Congrès.
La majorité assure le départage numérique dans la mesure où il faut agir et
orienter par l’adoption d’une solution. Bonne ou mauvaise.

Si cela suppose que la majorité s'impose à la minorité, cette même minorité
conserve l'ensemble de ses droits démocratiques d’opposition ou d’amendement
pour contribuer à infléchir les choix ultérieurs.

Cette opposition se doit d’être **loyale, critique, et constructive**.

Aussi pourra-t-elle contribuer plus facilement aux aménagements et à la mise en
œuvre du choix majoritaire.
L’autonomie de chaque syndicat est donc primordiale, inaliénable, dans la mesure
où elle s'oppose de facto à toute forme de centralisme démocratique.

Le **principe d'autonomie** est ici l'un des éléments de principes fondamentaux
de la CNT parce qu'elle rime avec liberté.

:ref:`L'article 29 <article_29_statuts_CNT_2014>` des statuts confédéraux de la
CNT précise que *l'autonomie de chaque structure consiste en la liberté de
pouvoir s'abstenir quant aux décisions qui ne lui conviennent pas **sans aller
publiquement à l'encontre de ces décisions** et dans la limite du respects des
présents statuts et des règles organiques*.

Cet :ref:`article essentiel <article_29_statuts_CNT_2014>` tend à préciser et à
définir le cadre de l'autonomie de chaque syndicat et les limites de sa liberté
l'expression ou d'action dans la mesure où il fait le choix d’une adhésion au
pacte confédéral, à son fonctionnement, et à sa nécessaire cohésion.

Si chaque syndicat a le pouvoir de participer, de contribuer, d'investir le cadre
démocratique proposé par la Confédération, il a le devoir de se conformer aux
décisions collectives en matière de fonctionnement, de règles organiques et
statutaires mais il peut toutefois ne pas abonder, contribuer, participer des
campagnes et des orientations qui ne lui conviennent pas.

**Dans le cas contraire, et en cas d'opposition publique (entendue comme au-delà
de l'organisation), le syndicat s'expose évidemment à sa propre démission,
à son exclusion par ses pairs, voir plus en amont à une non intégration par un
refus de labellisation confédérale**.

Ainsi, pleinement intégré au pacte confédéral, chaque syndicat répond devant
ses pairs, de ses choix, de ses décisions, de ses orientations.

La question de l'autonomie des syndicats, de leurs Unions locales, et de leur souveraineté
=============================================================================================

À la Confédération Nationale du Travail, le ou la syndiqué·e est avant tout
adhérent·e de **son syndicat**.
Cela suppose que la constance de son adhésion à la Confédération relève avant
tout de cette **entité organisationnelle**.

Les chômeurs considéré·e·s comme involontairement privé·e·s de travail, les
étudiant·e·s et apprenti·e·s considéré·e·s comme des travailleu·r·se·s en
formation, se syndiquent à leur syndicat de référence d’industrie s’il en est.

Par défaut, la possibilité leur est donnée de choisir leur syndicat d’appartenance.

**Le syndicat**, groupement professionnel de travailleu·r·se·s (entendu au sens large),
constitue **la base de décision et d'action, l'élément démocratique de base du
fédéralisme de la CNT**.

L'autonomie syndicale propre à notre fédéralisme vise aussi à s'opposer à une
forme de gestion des débats, des conflits, ou des décisions que l'on pourrait
qualifier de **toutisme**, tendance collective qui tendrait à faire que les affaires
des un·e·s seraient les affaires de tou·te·s les autres.

Notre fédéralisme privilégie une gestion locale ou interprofessionnelle des
affaires courantes ou des conflits.

En matière de conflits internes, les statuts confédéraux précisent : *Dans la
mesure du possible, tout conflit existant entre adhérentEs d’un Syndicat se
règle à l’intérieur de celui-ci [...] Tout conflit existant entre structures
de la CNT, quelles qu’elles soient, doit être évoqué et résolu par les
Congrès des Syndicats de l’aire géographique et/ou d’industrie concernées* (:ref:`art.24 <article_24_statuts_CNT_2014>`).

A contrario, chaque syndicat, chaque union locale, qu'elle soit régionale, locale,
ou départementale, doit aussi lutter contre une forme de **localisme** préjudiciable
au fonctionnement général ou global.

En effet, la cohésion fédérale ou confédérale et la bonne marche de l’ensemble
supposent une contribution spontanée de tous et toutes à sa réalisation et
son maintien.

Chaque syndicat, chaque Union de syndicats, doivent ici alimenter de leurs
opinions, de leurs approches, le débat démocratique permettant la décision
des grandes orientations d'ensemble et le cadre de fonctionnement collectif.

Il convient dès lors que chaque syndicat ou union statutaire participe aux
échanges et aux débats par le mandatement de délégué·e·s élu·e·s en leur sein,
révocables.


:term:`Synallagmatique`
-------------------------

Lorsque les contractants s'obligent réciproquement les uns envers les autres.

Voir :term:`Synallagmatique`

Amendement
===========

- :ref:`amendement_motion_20_2021_ste72`

Contre-motion
==============

- :ref:`contre_motion_20_2021_cnt42`


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°20 2021 ETPIC30    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


Contre motions en rapport
==========================

.. seealso::

   - :ref:`contre_motion_14_2021_cnt30`
