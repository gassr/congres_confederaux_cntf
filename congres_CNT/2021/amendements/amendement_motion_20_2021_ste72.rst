
.. _amendement_motion_20_2021_ste72:

================================================================================================================================
Amendement à la motion n°20 2021 **De la reconquête syndicale - Motion d'orientations sur la syndicalisation** par **STE72**
================================================================================================================================

- :ref:`motion_20_2021_etpic30`

Autres motions STE2

    - :ref:`syndicats:motions_ste72`


Argumentaire
===============

- ?


Amendement 
============

:NDLR:
    non traité. Introduit des erreurs: du respect avec un 's' !


.. raw:: html
   :file: ste72/amendement_motion_20_2021_ste72.html

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°20 |            |          |            |                           |
| STE72                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
