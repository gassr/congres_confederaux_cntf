
.. _motions_37_38_39_2010:

===========================================================================
Motions 37,38,39 2010 Participation aux élections dans la fonction publique
===========================================================================


.. toctree::
   :maxdepth: 2

   motion_37
   motion_38
   motion_39


Vote
====

Vote indicatif sur les trois motions
------------------------------------

SUTE 69-01
    Ouvrir une discussion dans la conf. But maintenir les droits syndicaux: HIS,
    panneaux, préavis de grève...donner la parole à toutEs les travailleurs_Euses.
    Loi et décrêt d'application juillet 2010 = représentativité accordée qu'avec
    score aux élections professionnelles. Élections deviennent donc déterminantes.
    L'objectif c'est de permettre aux syndicats qui le souhaitent de s'y présenter,
    mais débat non tranché.

STP72
(amendement): distinguer commissions paritaires des comités techniques

STE75
contre participation dans le secteur public et en particulier éduc. nat. Élections ne
permettront pas de gagner des droits. Aujourd'hui on n'est pas représentatifs mais droits minimaux
sont là. Comité techniques, CHS = cogestion, les distinguer sert à rien.
Santé Social RP: expérience différente des autres syndicats. On ne choisit pas les dossiers sur
lesquels on travaille, élus n'arrivent pas à respecter leurs mandats sur lesquels ils ont été élus.

STICS 86
difficulté de compréhension, arrive pas à comprendre si la loi sur la représentativité
change quelque chose ou pas.

STEA
élection à l'université. On se développe dans les luttes sociales. Elus au CA n'ont rien
obtenu.

Educ 13
Pour, restreindre au comité technique. Nouvelle législation rapproche le public du privé.
Réalité dans les universités n'est pas la même que dans le primaire et secondaire

STE75
si élection, on perd tous nos jeunes, c'est un truc corpo. C'est pas en adoptant les moyens
des autres syndicats qu'on aura les mêmes droits. Prend trop de temps .

INTERCO 73
préciser pourquoi on se présente : pour l'exercice de ces droits, se présenter aux
élections et pratiquer un syndicalisme de lutte, possible.
amendement : pose garde fous mais faut les mettre au point car cele ne concerne pas tous les
domaines (ex:primaire: commission administrative paritaire)

PTT Aquitaine
pour car la nouvelle loi va entrainer une perte de droits ce qui implique de devenir
une fédé clandestine .

ESS34
fédé éduc ne permet pas présentation élection professionnelle. Loi pas encore en place. Se
présenter aux élections ne permet pas forcément de poser des préavis de grève.
Propose d'en discuter plus tard. Contre car pb d'image, de pouvoir... Score min à atteindre non
encore relié à la garantie de droits syndicaux.

Interco Limousin
retour d'expérience, secteur archéologie, INRAP, qui s'est présenté il y a 6 mois.
On a passé le cap des 10% et donc eu les droits minimum mais les autres droits (HIS...) non. Droit
public n'est pas le droit dans l'éducation. Autre part que dans l'éduc, droit "appliqué".
+ Nous siègeons aux CPP et CHS pour éviter politique de la chaise vide. Position assumée. + pas
forcé d'y faire de la cogestion CPP, CHS = que de l'information
Au sujet de l'amendement STP72 c'est un élément qui est faux car les élus sont contrôlés par la
section. Laissez nous le temps de faire notre expérience.

ETPIC 30
OK avec ESS34: abstention n°37, 38, contre 39 cf 2008. Pourquoi ces motions ne
passent pas par la fédé?

STE93
OK avec constat de l'importance de garder nos droits syndicaux. Mais comment ? +
comment les faire vivre ?
Juillet 2010 ne remet pas en question.
Pour les CHSCT pas besoin de participer aux élections. cf cinémathèque : seul moyen= action
collective. Fait d'être élu ne protège pas à 100%. Demande la reprise de la motion de interco 86.

Santé social 69
pas tous la même réalité, chez nous, refus catégorique d'HIS car pas représentatif.
Attention, ne pas prendre ce problème car pb de répression pas partout le même, au risque de
d'exposer autres sections, ça peut vite dégénerer

Educ 69
droits minimaux pas remis en cause. C'est poser HIS sur temps de travail et le dépôt
préavis de grève qui sont menacés. Les décrets lient clairement représentativité à ces deux droits.
La motion n'appelle pas à se présenter pour toutes les sections, mais laisse un libre-choix.
2 garde fous: ne pas siéger dans les CT, ne pas accepter de décharges.
Santé social RP: une fois qu'on rentre dans ce système, compliqué de dire aux collègues qu'on ne va
pas siéger.
Loi ne change rien jusqu'en 2013, donc pas d'urgence.
On voulait donner un cadre conf à cette question, ne pas se mettre petit à petit devant le fait
accompli.
Pas que de l'info : c'est des dossiers pilotés par le patron

SSEC 59 62
Motion qui n'engage pas tous les syndicats de la FP. Chaque syndicat autonome.
Est ce qu'on va faire comme pour les PTT à attendre jusqu'à ce qu'il soit trop tard.

PTT66
Pour car on a subi des injures lors d'une lutte sur facteur d'avenir. Ont posé un préavis
contesté par direction car non représentatif, la lutte a donc du s'arrêter. Il n'a pas été possible de
réagir.

PTT Centre
Pareil que PTT66.

SIPM RP
contre 37-38-39, pour la contre motion.
La situation peut rester en l'état mais on écoute les camarades de l'INRAP. Cas par cas peut ici être
intéressant, situations différentes.
Pas la peine de changer maintenant mais pas définitif, prochain congrès pourra statuer.

Educ 42
Régression de nos droits va être terrible quel que soit la décision qu'on peut ici prendre.
Peut être jouer le jeu de la légalité pourra nous aider.

STEA
pas position étudiante, mandaté par son syndicat. Reconquérir droits par la lutte.

Educ 69
Alignement du public sur le privé, pourquoi on différencie. On ne parle pas gagner des
droits mais de garder des droits. La grève est notre arme encore faut il pouvoir l'utiliser. Eviter de
faire du syndicalisme de trottoir.
Pas de position de principe, mais résultat de fait, on est en procès avec le rectorat depuis un an.

Interco 57
OK avec Lyon Faisant confiance à notre fonctionnement syndical. Refusons les réunions
patronales.

Interco Limousin
se présenter aux élections ne signifie pas devenir bureaucrate. L'INRAP n'est pas
une exception.

Vote définitif
--------------

- :ref:`vote_motion_37_2010`
- :ref:`vote_amendement_motion_37_stp72`
- :ref:`vote_motion_38_2010`
- :ref:`vote_motion_38_2010`
- :ref:`vote_contre_motion_39_2010_interco86`
