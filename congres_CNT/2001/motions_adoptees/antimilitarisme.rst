
.. _motion_antimilitarisme_cnt_congres_toulouse_2001:

===================================================================================
Motion Antimilitarisme, 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
===================================================================================


Creation d’une commission anti-anti-militariste nationale
=========================================================

Vote
----

61 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 35         | 4        | 1          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Création d'une commission anti-militariste nationale, chargée de recueillir et
de redistribuer des informations, de prendre des contacts avec les groupes
anti-militaristes existant (à charge des U.R. d'avaliser ces contacts), de
promouvoir des actions ou des revendications anti-militaristes, d'amener à
une réflexion sur les secteurs en lien avec l'armée et l'aménagement, de parler
de la guerre et de la non violence, en liaison avec les syndicats concernés par
ces secteurs du travail et le reste de la confédération.
