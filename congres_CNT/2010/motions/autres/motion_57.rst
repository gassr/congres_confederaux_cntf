

.. _motion_57_2010:

=============================================================================
Motion n°57 2010 : Abandon de la plate-forme confédérale, Santé-Social CT-RP
=============================================================================


    
Argumentaire
=============

Un projet de plate-forme confédérale est actuellement en discussion.

Le syndicat CNT interpro 16 a fait parvenir une contribution peu avant
le :term:`C.C.N` pour faire part de son questionnement quant à l'intérêt
de cette plate-forme.

Comme le soulignait aussi le texte de la CNT interpro 16, on ne sait pas à qui
est destiné cette juxtaposition de revendications.

- Document interne pour les nouveaux militants ?
- Feuillet destiné à faire venir de nouvelles personnes ?

Les revendications que l'on peut trouver sont pour la plupart des revendications
déjà rencontrées dans des luttes salariales ou dans des mouvements sociaux
mais qui correspondent à des moments et des luttes précis.

Elles ne peuvent en aucun cas être un socle de revendications permanentes dont on
pourrait se servir sur son lieu de travail, ne serait-ce que parce que c'est
contradictoire avec l'idée même que c'est la base qui décide librement de la
manière dont elle élabore ses revendications.

Nous pouvons cependant noter que la conclusion de la plate-forme évoquant un
autre futur défend cette même idée : les choses ne peuvent pas être figées sinon on
rentre dans le dogme.

Nous sommes d'accord avec cette conclusion en contradiction avec les 10 pages de
revendications précédentes.

Cette conclusion démontre que nous n'avons pas besoin de plateforme confédérale.
Les travailleurs et travailleuses, salarié-e-s ou pas, sont les mieux à même, en
fonction de leur situation, de leur environnement, de leurs besoins... de construire
dans l'autogestion un projet de transformation sociale.

  
Motion
======

La confédération décide d'abandonner l'idée d'élaborer une plate-forme
confédérale qui serait contraire à la liberté syndicale et qui ne peut être
le projet d'un autre futur dans la mesure où les revendications ne remettent
pas en cause le système capitaliste.

Dans la même logique, nous ne souhaitons pas non plus la création d'une
commission confédérale.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°57          |            |          |            |                           |
| Santé-Social CT-RP   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`contre_motion_57_2010_stp72`
