

.. _contre_motion_11_etpic30:

================================================================================================
Contre-motion à la motion N°11 Pour une cohésion fédérale de la CNT vers un autre futur ETPIC30
================================================================================================

.. seealso::

   - :ref:`motion_11_2012_ptt_centre`
   - :ref:`article_29_statuts_CNT_2010`




Argumentaire
============

Cette contre motion n’a pas pour vocation à être votée si la motion initiale du
syndicat CNT PTT Centre n’a pas été adoptée.

La modification de statuts de :ref:`l’article 29 <article_29_statuts_CNT_2010>`
proposée par le syndicat CNT PTT Centre introduit une nouvelle définition de
l’autonomie des structures labellisées CNT (Syndicats, Unions).

**Si elle était adoptée, elle modifierait radicalement et fondamentalement le
cadre organisationnel actuel de la CNT.**

.. warning:: Par cette contre motion, nous alertons là l’ensemble des syndicats
   et invitons  à la plus grande attention en lien avec les enjeux sous-jacents.

Le congrès confédéral seul par sa souveraineté détermine le cadre statutaire de la
Confédération et par conséquent, le niveau d’autonomie des syndicats en matière
d’orientations, de fonctionnement et de stratégie syndicale.

:ref:`L’article 29 <article_29_statuts_CNT_2010>`, adopté à 67% au Congrès
confédéral de 2008 à l’occasion de la  refonte intégrale des statuts confédéraux,
est rédigé actuellement ainsi:  «L’autonomie de chaque structure consiste en la
liberté de pouvoir s’abstenir  quant aux décisions qui ne lui conviennent pas,
sans aller publiquement à  l’encontre de ces décisions et dans la limite du
respect des présents Statuts  et des règles organiques. »

Cette définition nous convient donc parfaitement en ce sens qu’elle délimite de
facon claire la marge d’autonomie, et donc la marge de manœuvre syndicale et
organique, des syndicats et Unions de la CNT tout en respectant le nécessaire
cadre de cohésion confédérale. À charge des syndicats CNT en désaccord face à
la majorité, de convaincre pour faire évoluer ou infléchir les positions
confédérales dans le cadre démocratique défini aux statuts et règles organiques.

Le syndicat CNT PTT Centre nous propose bien plus qu’un simple « amendement »,
mais une nouvelle définition intégrale: « L’autonomie de chaque structure
consiste en la liberté de pouvoir déroger pour elles, dans le respect des
principes fondateurs du syndicalisme révolutionnaire et de l’anarchosyndicalisme,
aux décisions tactiques et stratégiques syndicales prises majoritairement par
d’autres structures de la Confédération et sans remettre publiquement en cause
le caractère majoritaire de ces décisions ».

Outre notre réelle difficulté à comprendre le sens de cette nouvelle définition,
nous comprenons que : La seule invocation les principes du syndicalisme
révolutionnaires et de l’anarchosyndicalisme (auxquels nous sommes néanmoins
clairement attachés) **ne suffit pas à déterminer statutairement les limites de
l’autonomie des structures syndicales de la CNT**.

En effet, ces références syndicales ne comportent pas de définition unique
ou sont malgré tout sujets à des interprétations multiples. Elles ne constituent
donc  pas un socle de références commun, suffisamment précis, sur le plan des
orientations confédérales.

(Si nous avons bien compris la phrase) la nouvelle définition proposée pour l’autonomie
de chaque structure CNT n’attribuerait aux décisions confédérales qu’un caractère
indicatif par une approche dérogatoire permanente. Chaque structure y étant alors
décrite comme totalement autonome vis-à-vis du cadre statutaire et décisionnel
confédéral, notre nouvelle organisation nationale (donc plus confédérale)
s’apparentera dès lors très vite à un type de réseau dont les modalités
organisationnelles nous échappent complètement.

**Nous appelons bien entendu à rejeter la motion du syndicat CNT PTT Centre**,
sauf à acter que la CNT soit amener à devenir un futur réseau de syndicats
autonomes, dont les décisions collectives seraient indicatives, et dont il faudra
s’enquérir de qui est en mesure de délivrer le label...

Nous considérons à ce titre que le terme organisationnel de «fédéralisme» ne
serait plus approprié.

Le caractère révolutionnaire de la nouvelle organisation devenant de même
assez aléatoire en fonction des différentes orientations choisies (de facon
dérogatoire ou non) par ses diférentes composantes locales, « fédérales », etc...

Aussi, notre syndicat défend le maintien de la définition actuelle de
:ref:`l’article 29 <article_29_statuts_CNT_2010>` inscrite dans une logique
fédéraliste garantissant l’unité et la  cohésion de la confédération.

À notre sens, le projet ré-évolutionnaire ou anarchosyndicaliste ne peut
d’ailleurs être entrevu que dans ce cadre là en vue d’un mouvement d’ensemble,
coordonné, vers un autre futur.

Vote
====


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°11     |            |          |            |                           |
| ETPIC30                            |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_11_2012`
