
.. _motion_bulletin_interieur_cnt_congres_lyon_1996:

================================================================================================
Motion Bulletin intérieur de la CNT  25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
================================================================================================

Bulletin intérieur
==================

Texte
-----


Le bulletin intérieur (BI) a vocation à véhiculer des textes de débat et de
réflexion émanant de structures syndicales ou d'adhérents (qui devront signaler
leur appartenance a telle ou telle structure confédérale).

Le BI contient également les articles refusés par le Combat Syndicaliste avec
motivation du refus.

Il n'est pas besoin dans le BI de s'adresser à un syndicat, à une fédération,
d'attendre un CCN ou un Congrès pour s'exprimer et connaître ou faire connaître
les points de vue qui nous parcourent.

En dehors de tout formalisme, les opinions, les interrogations, les humeurs,
les contradictions peuvent fleurir. La critique vive n'est pas exclue mais
l'insulte n'y aura pas sa place.

Si un texte est refusé pour ce motif, il sera  renvoyé à l'expéditeur.

Dans l'intérêt d'une meilleure circulation des idées, l'abonnement à un
exemplaire est obligatoire pour chaque syndicat (100 francs).

Les abonnements individuels bénéficieront d'un abonnement préférentiel (80 F).

Les adhérents devront passer par leurs syndicats pour s'abonner au BI.

La périodicité minimale du BI est mensuelle avec un numéro double d'été.
Celui-ci, en fonction des possibilités du syndicat responsable, devrait tendre
à devenir bi-mensuel.

En résumé, il nous semble nécessaire de faire plus de publicité parmi nous
au BI qui pourrait être notre Agora permanente pour peu que nous investissions
la place.
