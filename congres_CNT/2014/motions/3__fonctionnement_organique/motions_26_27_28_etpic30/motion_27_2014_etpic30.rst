
.. index::
   pair: Motion 27 ; 2014

.. _motion_27_2014_etpic30:

=============================================================================
Motion n°27 2014 **Recueil des motions de Congrès adoptées** (ETPIC 30 sud)
=============================================================================


- :ref:`syndicats:motions_etpic30`
- :ref:`motion_4_2008_etpic30`


Motion
========

Au-delà des comptes-rendus de Congrès, le « Recueil des motions de
Congrès adoptées » constitue un ouvrage interne de référence quant
aux résolutions adoptées par les Congrès confédéraux successifs.

Véritable outil de transparence démocratique, il tend à favoriser la
mémoire interne et une meilleure connaissance de tous et toutes des
résolutions prises pour et par la CNT.

Son actualisation régulière contribue donc à enrichir l’évolution du
débat et la compréhension du projet anarcho-syndicaliste et syndicaliste
révolutionnaire.

A l'issue de chaque Congrès, le « Recueil des motions de Congrès adoptées »
devra donc être mis à jour par le bureau confédéral ou par voie de
délégation sous sa responsabilité.

Le recueil est structuré selon deux axes principaux
*Fonctionnement & Orientations*, secondés d’un classement thématique
identifiable en sommaire afin de favoriser tant que possible la
lisibilité de l'ouvrage.

La date de la mise à jour est indiquée en page de garde.

Chaque motion figurant au recueil dispose d’un titre lui-même précédée
d’un chapeau comportant le Congrès confédéral correspondant et
l’identification des votes (nom des syndicats votants et  résultat
numérique des votes).

La mise à jour est réalisée dans les meilleurs délais.

Le CCN le plus proche valide l'ouvrage sur avis du Bureau Confédéral.
Il devra être portée à la connaissance des syndicats par le Bureau
Confédéral au moins 6 mois avant le Congrès suivant, en format papier
ou électronique (diffusion via la [listesyndicats]).

Le « Recueil des motions de Congrès adoptées » est par ailleurs
transmis gratuitement par le Bureau Confédéral à tous syndicats
en faisant la demande.

Il est par ailleurs systématiquement proposé à tous nouveau syndicat
labellisé par la CNT.


Amendement
===========

- :ref:`amendement_motion_27_2014_interco25`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°27 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
