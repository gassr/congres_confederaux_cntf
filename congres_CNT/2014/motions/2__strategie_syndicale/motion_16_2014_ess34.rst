
.. index::
   pair: Motion 16 ; 2014
   ! Municipalisme libertaire

.. _motion_16_2014_ess34:

=========================================================================================
Motion n°16 2014 : Appel à l'abstention de la CNT lors des élections politiques (ESS 34)
=========================================================================================

.. seealso::

   - :ref:`motions_ess34`




Argumentaire
==============

Adhérer à la Confédération Nationale du Travail , c'est défendre les
positions de ce syndicat et adhérer à son projet de société
*le Communisme Libertaire* par des moyens révolutionnaires.

Le projet politique de la CNT est un projet global qui ne peut être
*saucissonné*.

C'est pourquoi, le Communisme Libertaire qui est partie intégrante de
ces orientations, ne peut composer ou s’accommoder d'aménagements ou
d'entorses qui remettraient en cause ce projet.

L'action politique par le biais des élections politiques va à l'encontre
de cette voie, elle en est l’extrême opposé (délégation et confiscation
de notre pouvoir décisionnaire pendant 6 ans, réformisme, pouvoir,
représentation de l’État...).

Le **municipalisme** constitue un marchepieds vers d'autres échelons de
l’État, le maire en est le premier magistrat (1er flic), représentant
du préfet et de l'autorité.
D'autres échelons viennent ensuite *bétonner* l'ensemble (cantonal,
députation, sénatorial, présidentielle.).

**Tout cela dans le but de nous soumettre et de nous contrôler, voire
de nous réprimer socialement**.

Afin de réaffirmer son projet politique, la CNT et les syndicats qui
la composent s'engagent dans des campagnes d'abstention, à chaque fois
qu'ont lieu des élections politiques en vue de dénoncer la manipulation
et le mensonge autour de ces institutions de la société capitaliste,
ainsi que le frein, voire l'empêchement, de toute émancipation des
individus que représente le jeu électoral.

Quant aux faux arguments qui consistent à défendre des positions proches
d'un soi-disant **municipalisme libertaire**, ils ne résistent pas à
l'étude du fonctionnement des institutions, et en particulier à l'octroi
des subventions des conseils généraux, régionaux, étatiques, voire
européens en faveur des municipalités.

Toute idéologie qui ne rentre pas dans le moule se voit privée de moyens
financiers indispensables à la survie de la structure municipale, et
donc à l'échec de ses orientations.
A ce titre, rappelons pour mémoire l'exemple du FN, qui outre son
incompétence notoire en matière sociale et économique, et malgré ses
tentatives pour cacher ses penchants fascistes plombe financièrement
toutes les mairies qu'ils (elles) ont dirigé(e)s, en partie par la
coupe des subventions régionales, étatiques...
A tel point que même René Revol dirigeant national du Front de Gauche,
élu à plusieurs reprises maire de Grabels dans l'Hérault, ne se
présente pas sous son étiquette politique, mais sous l'intitulé
*Tous ensemble pour Grabels*.

Dernier point, il ne s'agit pas non plus de dire aux adhérents ce
qu'ils doivent penser ou faire, mais d'affirmer haut et fort nos
convictions les plus profondes, entre autres que l'autogestion est
incompatible avec le système politique de délégation.


Motion
======

La Confédération Nationale du Travail, conformément à son projet de
société ne présente aucun candidat en son nom propre à des élections
politiques, aucun adhérent ne peut se prévaloir de l'étiquette CNT
pour s'y présenter, et en conséquence organise des campagnes
d'abstention lorsque celles-ci sont d'actualité, et ce tout en
réaffirmant son attachement au **Communisme Libertaire** et aux voies
pour y parvenir.

Amendements à la motion 16
===========================

.. seealso::

   - :ref:`amendement_motion_16_2014_sim_rp`
   - :ref:`amendement_motion_16_2014_stics13`
   - :ref:`amendement_motion_16_2014_interco25`


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°16 2014

       :ref:`ESS34 <ess34>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
