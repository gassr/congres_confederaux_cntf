
.. _contre_motion_24_2021_simrp:

=======================================================================================================
Contre-motion N°8 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **SIMRP**
=======================================================================================================

- :ref:`motion_24_2021_ste33_cnt42`


Autres motions SIMRP

 - :ref:`paris:motions_simrp`


Contre-motion
================

Faire le procès du capitalisme (c'est à dire tenter de cerner sa spécificité
socio-historique et sa nocivité totale pour l'humain et la nature) pour
promouvoir un autre rapport social affranchi de toutes les formes de
d'exploitation, d'aliénation et de domination, suppose que la CNT se dote
des moyens d'informations et de formations susceptibles d'empêcher qu'en
son sein se reproduise des pratiques violentes quelles qu’elles soient.

Dans cette optique, il n'appartient pas à la CNT d'instruire un procès
contre un militant accusé de crime sexuel, pas davantage de se substituer
aux structures juridiques, médicales, sociales, associatives pouvant venir
en aide à la victime, mais de soutenir et d'orienter celle-ci vers ces
dernières afin qu'elle ne soit pas laissée abandonnée sans accès à la justice,
sans réparation, sans traitement spécialisé, à devoir survivre seule face aux
violences sexistes et à leurs conséquences nonobstant le fait que tout
militant accusé de violence s'exclut de lui-même de la CNT.

Cependant, à condition que la victime en soit d'accord et que l'agresseur
soit accompagnée vers une structure médicale adaptée (ex :sexologie psychiatrique)
une mesure de mise de retrait de la CNT de l’agresseur pourra être envisagée.

Il appartiendra au syndicat concerné de mettre en œuvre cette procédure.


Vote
====


+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°24 |            |          |            |                           |
| SIMRP                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
