.. index::
   pair: Motion 15 ; 2021

.. _motion_15_2021_syndicats_42:

================================================================================================================================================
Motion **n°15** 2021:  **création de boîte mail en cnt-f.org** par **Syndicats 42**
================================================================================================================================================

- :ref:`analyse_differends_internes`
- :ref:`mujeres_libres_1936_05_01`

Autres motions CNT 42

    - :ref:`CNT42 <rhone_alpes:cnt42_motions_congres>`


Contexte
==========

Pour le bon fonctionnement des groupes Femmes Libres et Mujeres Libres au niveau
confédéral qui le souhaitent, une boîte mail spécifique s’avère nécessaire.

Pour être contactés en tant que groupes Femmes Libres et Mujeres Libres par
des syndicats, des interlocutrices et interlocuteurs extérieur.e·s, des
personnes qui souhaitent solliciter du soutien et n’ont pas à exposer leur
requête et leurs difficultés sur une boîte mail générale des UL, une adresse
avait déjà été demandée par le groupe CNT Femmes Libres de St Etienne et des
syndicats de l’UD42, et refusée par le secrétariat confédéral.

Puisque l’argument avancé pour ce refus était la non reconnaissance officielle
de l’existence de ces groupes, et l’aspect non statutaire de l’adresse demandée,
puisque sur le fond la lutte contre le patriarcat n’est pas remise en cause par
les syndicats cénétistes et que de nombreux soutiens se sont manifestés de la
part d’autres syndicats, c’est dans un contexte de Congrès que la demande est
renouvelée, afin que soient levées les barrières formelles qui empêchent la
création de cet outil tant pratique que symbolique que serait une boîte mail
cnt dédiée aux groupes CNT FL/ML.

Motion soumise au vote
========================

Le bureau confédéral de la CNT donnera accès à une boîte mail cnt-fl.ml.zone
géographique@cnt-f.org à tout groupe Femmes Libres et Mujeres Libres qui en
fera la demande.


Santé social 42 ; Culture et Spectacle 42 ; Educ 42 ; Interpro 42


Amendements
===========

- :ref:`amendement_motion_15_2021_stp67`

Contre motion
=============

- :ref:`contre_motion_15_2021_cnt30`



Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°15 2021 CNT42      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
