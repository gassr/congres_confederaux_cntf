
.. _amendement_motion_18_2021_interco69:

===========================================================================================
Amendement N°3 à la motion n°18 2021 **Procédure de la labellisation** par **Interco69**
===========================================================================================

- :ref:`motion_18_2021_ste93`

Autres motions Interco69

  - :ref:`rhone_alpes:motions_interco69`


Argumentaire
=============

L’imposition d’un nombre minimum de travailleurs de la même branche pour
former un syndicat est aujourd’hui une nécessité pour plusieurs raisons,
au vu d’un contexte de plus en plus politiquement et socialement délétère,
il est important de pouvoir massifier en facilitant l’entrée et l’accueil
de nouveaux membres et en concentrant nos forces là où c’est déjà possible.
Ainsi, contraindre la création de nouveaux syndicats à 5 membres là où
c’est géographiquement possible (c’est-à-dire une aire départementale où
il y’a plus de 5 membres de la CNT).

Ainsi concernant la massification, des syndicats trop petits peuvent
décourager l’implication de nouveaux membres, ceux-ci doivent démultiplier
leur présence dans les instances de la confédération pour rencontrer de
nouveaux membres alors qu’ils viennent d’arriver ou encore faire face à
quelques camarades se connaissant très bien et avoir l’impression de se
retrouver dans un groupe affinitaire avec tous les biais que cela suppose.

Concernant la concentration de la puissance, des syndicats plus grands
peuvent permettre une facilitation évidente du renouvellement des mandats,
mandats qui auront d’ailleurs une implication sur un plus grand nombre
d’adhérents ce qui pourrait permettre d’éviter de les démultiplier pour le
même impact ainsi que d’impliquer progressivement davantage les nouveaux
militants sans leur imposer une charge de travail trop grande dès le début.

Enfin une concentration d’adhérents permet aussi de posséder une capacité
de mobilisation accrue (que ce soit pour des manifestations, des réunions...)
en évitant de multiplier les instances de rencontre et de décisions
ainsi qu’une facilitation du partage des expériences et des connaissances,
et une plus grande une pluralité des expériences et des opinions, que
l’organisation au sein de syndicats trop réduits pourrait empêcher, notamment
lorsque l’on vient d’arriver à la CNT et qu’on ne maîtrise pas tous les
codes et pratiques de la confédération.

Cette concentration vise donc à un développement de l’énergie militante
par une augmentation de l’émulation collective et l’économie de tâche
bureaucratique trop spécialisé et n’impactant qu’un nombre réduit de militant.

Enfin, elle permettrait aussi de davantage s’investir localement en
remettant au centre le fonctionnement intercorporatif.

L’interdiction  de  syndicat  de moins de  5  personnes n’enlève  d’ailleurs
pas  la possibilité de spécialiser l’action lorsque cela est nécessaire
avec par exemple des groupes de travail ponctuelles, des sections de
branches qui répondraient de leur interco mais ont tout de même une certaine
autonomie (on peut par exemple imaginer une prolongation d’AG interco
pour régler des questions spécifiques à une branche).

Néanmoins l’optique fondamental est bien à terme la création de syndicat
de branche d’au moins 5 personnes, qui soit cohérent, fonctionnel et apte,
l’idée étant notamment de renforcer la cohésion des UD.
Nous sommes une confédération syndicale minoritaire et il est essentiel
de ne pas s’éparpiller le temps de notre renforcement

Amendement
==========

Article 1
-----------

Dans le cadre de la procédure de la labellisation, le syndicat qui en
fait la demande doit être composé d'au moins cinq adhérent.e.s.
Si ça n'est pas le cas, les futur.e.s adhérent.e.s doivent être
syndiqué.e.s dans le syndicat interprofessionnel existant dans le
département.

Article 2
------------

Si aucun syndicat n'existe dans le département, un syndicat interprofessionnel
peut être labellisé avec moins de 5 membres.

Vote
======

+----------------------------------+------------+----------+------------+---------------------------+
| Nom                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==================================+============+==========+============+===========================+
| Amendement à la motion n°18      |            |          |            |                           |
| Interco69                        |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès              |            |          |            |                           |
|                                  |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+
