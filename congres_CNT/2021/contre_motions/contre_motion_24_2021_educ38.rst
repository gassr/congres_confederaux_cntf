.. index::
   pair: Violences patriarcales; Gestion
   pair: Educ38; Gestion des violences patriarcales

.. _contre_motion_24_2021_educ38:

===========================================================================================================================
Contre-motion N°3 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Educ38** **[Adoptée,🆗]**
===========================================================================================================================

- :ref:`motion_synthese_24_2021_ste38_stp_67`
- :ref:`motion_24_2021_ste33_cnt42`


Motions Educ38
================

- :ref:`rhone_alpes:educ_38_motions`

Argumentaire
==================

Cette contre-motion s’inscrit dans la démarche de constitution d’un protocole
de gestion interne des violences patriarcales initiée par les syndicats
CNT STE 33 et CNT du 42.

Nous en reprenons ici l’essentiel du texte et tenons à saluer le travail
des camarades. Un tel protocole nous semble indispensable au regard des
principes antisexistes que notre organisation révolutionnaire prétend
défendre et des violences sexistes et sexuelles passées que nous n’avons
pas su gérer collectivement.

Ne pas en tirer les conséquences aujourd’hui constituerait une violence
supplémentaire produite par le système patriarcal et serait une grave
erreur politique.

Toutefois, **nous restons attaché·e·s au principe d’autonomie des syndicats
et au modèle fédéraliste**, comme formulé dans l’article 3 de nos statuts
confédéraux qui stipule qu’``À la CNT, le pouvoir appartient aux syndicats,
cellule de base de la confédération, et à leurs adhérent·e·s au sein des
syndicats``. Voir :ref:`article_3_statuts_CNT_2014`

C’est pourquoi nous ne pensons pas, contrairement à ce que proposent les
camarades du STE 33 et du 42 dans leurs points 10 et 11 du protocole,
**que la confédération puisse se substituer aux syndicats dans un éventuel
processus de sanction**.

L’essentiel des modifications proposées dans cette contre-motion porte
donc sur ces paragraphes, que l'on retrouve ici dans le point 10 du
présent protocole.

**Conformément aux statuts en vigueur, nous souhaiterions laisser cette
responsabilité au syndicat**. Nous rappellerons cependant, et à toute
fin utile, que la confédération, par ses instances de contrôle (mandats confédéraux, :term:`CCN`...)
doit rester garante du respect du pacte confédéral dont les principes
lient tous les syndicats.

Par ailleurs, nous avons proposé, quand nous le pouvions, des alternatives
aux formulations et concepts appartenant à l’ordre judiciaire pour deux
raisons :

- d'une part, nous ne saurions nous satisfaire d’une décharge
  totale sur une justice institutionnelle trop souvent au service des
  dominants. Dans une perspective d’autodéfense et d’éducation populaire,
  il nous semble important de nous emparer d’outils qui nous ressemblent
  et d’employer un autre vocabulaire que celui utilisé par la justice bourgeoise.
- D’autre part, **nous souhaitons sortir de la seule logique punitive**.
  C’est pourquoi nous proposons dans notre contre-motion une piste
  d’accompagnement des agresseurs (point 11 du protocole).

A l’heure où de plus en plus de victimes des violences patriarcales osent
parler et se défendre, les déclarations de principe ne suffisent plus.

Nous avons le devoir d’agir, en cohésion avec nos valeurs, si nous
souhaitons encore prétendre lutter contre le patriarcat.
Cette contre-motion va dans ce sens.

Par souci de clarté, les modifications faisant l’objet de cette contre-motion
ont été rédigées en gras.

Contre motion
===============

**Aucune** personne reconnue coupable de viol/violences patriarcales
n’a sa place à la CNT.

Adoption par le 35ème congrès confédéral de la CNT d'un protocole qui
vise à établir des outils et une procédure à suivre systématiquement dès
qu'une accusation de viol et/ou de faits de violence patriarcale est
portée à la connaissance de membres de la CNT contre un·e de ses membres.

- Toute personne accusant de viol et/ ou agression sexuelle un membre de la
  CNT est considérée comme une victime et bénéficie de tous les traitements
  et égards dus à ce statut (aide psychologique, juridique, financière
  en cas d’incapacité au travail) et ce le temps nécessaire à l’instruction
  **du viol et/ou agression sexuelle** et sans jugement au préalable de
  son issue.
- La CNT met en place une liste de personnes référentes **pour la prise en
  charge d’accusations de viol et de violence patriarcale mandatées par
  leurs syndicats**.
  Cette liste est mise à disposition de tout·e·s les adhérent·e·s de la
  CNT et actualisée quand nécessaire.
  **Comme pour tout mandatement au sein d’une commission de travail, les
  syndicats s’assurent que les personnes mandatées soient formées à ce qui
  constitue l’objet de la commission (lutte contre le patriarcat, prise
  en charge de violences) ou se forment pour cela**.
  La victime peut saisir (elle-même ou par l’intermédiaire d’une tierce
  personne) une ou plusieurs personnes parmi cette liste pour porter accusation.
- **Les référent·e·s contacté·e·s sont alors chargé·e·s de mettre en place une
  commission extraordinaire, non mixte ou mixte suivant le choix de la victime.
  Les membres de cette commission extraordinaire sont choisi·e·s parmi la liste des
  référent·e·s. Aucune démarche, décision ne sera mise en œuvre sans l’approbation
  préalable de la victime**.
  Cette commission extraordinaire a tout mandat pour instruire l’affaire,
  décider quand lever l'anonymat de l’agresseur/ violeur et diffuser le
  rendu de ses travaux à la Confédération. Elle n’est nommée que pour le
  temps de l’instruction.
  Comme toute commission de travail confédérale, la commission extraordinaire
  peut solliciter des contributions externes à la CNT.
  **La victime peut décider, à n’importe quel moment de la procédure et
  quelles qu’en soient les raisons, de saisir d’autres personnes parmi
  la liste de référent·e·s**.
- La CNT, à travers le BC, doit se donner les moyens (financiers notamment et/
  ou d'hébergements, pour d'éventuels déplacements, voire informatiques à travers un espace
  d'échange sécurisé) du bon fonctionnement et déroulement de la procédure.
- Les membres du syndicat et de l’UD de l’accusé·e aussi bien que de la
  victime ne peuvent en aucun cas faire partie de la commission extraordinaire pour éviter tout
  risque de partialité et de pressions extérieures.
- Dès sa constitution, la commission extraordinaire informe le syndicat local de
  l’accusé·e des accusations portées contre lui/ elle (que la victime soit
  elle-même adhérente de la CNT ou pas). À partir de cette information,
  par principe de précaution, l’accusé·e est suspendu·e de la CNT, et ce
  pendant tout le temps nécessaire à l’établissement du rapport.
  Le syndicat local d’appartenance de l’accusé·e est responsable de
  l’application de la suspension. **Il est clair que cette suspension est
  provisoire et n’est pas une prise de décision de la CNT sur le fond de l’affaire**.
- La commission extraordinaire a pour mission de recueillir la parole de la
  victime (ce faisant de lui permettre de sortir d’un silence préjudiciable
  pour elle à court, moyen et long terme) et tout autre témoignage
  qu’elle jugera nécessaire.
  **Elle pourra par ailleurs l’accompagner dans ses démarches (y compris
  juridiques) si telle est la demande de la victime**.
- Quelle que soit la décision de la victime de saisir ou non la justice,
  elle ne pourra en aucun cas être jugée sur sa décision par la CNT.
  En cas de refus de porter plainte, il ne pourra en être tenu rigueur
  à la victime et cela ne vaudra pas non plus mise en doute de
  son accusation. **La commission ne doit en aucun cas se substituer à la
  victime et l’influencer sur cette décision**.
- Une fois la parole de la victime recueillie, **ainsi que tout autre témoignage
  que la commission jugera nécessaire**, la commission extraordinaire rend ses conclusions
  aux référent·e·s. Les référent·e·s et la commission extraordinaire rédigent un rapport à
  partir de ces conclusions qui sera remis à tous les syndicats de la Confédération.
- **Selon ses conclusions, la commission extraordinaire pourra demander au
  syndicat d’appartenance de l’accusé·e son exclusion en conformité avec les statuts. En
  cas de refus du syndicat de répondre à cette demande et plus largement en cas de non-
  respect de ce protocole ou d’obstruction à son application, la commission pourra
  préconiser la suspension du syndicat au prochain :term:`CCN`, et son exclusion au prochain
  congrès, en accord avec les règles organiques « Élaboration de l’ordre du jour du
  :term:`CCN` », « Commissions de travail confédérales : rôle, limites et compétences » et avec
  l’article 24 des statuts confédéraux (« gestion des conflits internes »)**.
- **Enfin le travail de cette commission est complémentaire d’un travail
  mené auprès de l’agresseur (orientation notamment vers une structure
  d’accompagnement des personnes auteures de violences sexuelles -structure
  reconnaissant le système de domination patriarcale), qui peut en constituer un
  prolongement, sous réserve que ce travail soit compatible avec la protection de la
  victime et que l’agresseur en reconnaisse la nécessité et son inscription dans un système
  de domination patriarcale**.


Vote [Adoptée,🆗]
====================

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°24 |            |          |            |                           |
| Educ38                         |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
