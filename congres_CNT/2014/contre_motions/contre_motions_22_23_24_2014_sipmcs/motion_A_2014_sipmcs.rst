
.. _motion_a_2014_sipmcs:

=======================
Motion A 2014 (SIPMCS)
=======================




Motion A
=========

Le CS continue à exister sous sa forme papier.

La commission CS prendra les décisions qu'elle juge opportunes pour:

- baisser le prix du journal ;
- adapter le format pour être plus en adéquation avec les pratiques de lecture
  actuelles ;
- rendre disponible sur internet et gratuitement le journal.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion A 2014 SIPMCS

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
