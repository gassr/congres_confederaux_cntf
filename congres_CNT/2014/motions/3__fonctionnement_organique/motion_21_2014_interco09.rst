
.. index::
   pair: Motion 21 ; 2014

.. _motion_21_2014_interco09:

=======================================================================
Motion n°21 2014: Utilisation des nouvelles technologies (Interco 09)
=======================================================================

.. seealso::

   - :ref:`motions_interco09`





Argumentaire
=============

Lors du dernier CCN à Nîmes, une motion contre l'utilisation des
codes-barres dans la communication confédérale a été adoptée.

Le syndicat CNT interpro 09 proposait également le principe d'une
consultation des syndicats avant utilisation d'une nouvelle technologie
pour la communication confédérale.
Il a été demandé à la CNT09 de formuler une demande exhaustive au
sujet des technologies concernées afin de la proposer sous forme de
motion de fonctionnement.


Motion
=======

Dans le contexte capitaliste, les avancées technologiques ne sont pas
pensées par la population, mais par les élites, dans le but de servir
leurs propres intérêts, principalement économiques et répressifs.

Pour sa communication interne et externe, la confédération continue
d'utiliser exclusivement les technologies figurant dans cette liste
qui ne pourra être modifiée qu'au cours d'un congrès sur consultation
des syndicats:

- écriture manuscrite, typographique et braille
- courrier postal
- imprimerie
- tractage et affichage
- audio (enregistrement, sonorisation, radiophonie, supports audio à télécharger)
- vidéo (tournage, projection, télévision, cinéma, supports vidéo à télécharger)
- radiocommunication
- téléphonie fixe
- téléphonie portable
- texto, télégramme, fax
- courriel
- site internet
- serveur intranet

Liste des technologies déjà écartées par la confédération :

- codes-barres à une ou plusieurs dimensions (CCN du 26 et 27 Avril 2014
  à Nîmes)


Amendement
==============

- seealso::

  - :ref:`amendement_motion_21_2014_interco25`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°21 2014

       :ref:`Interco09 <interco09>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
