
.. _amendement_motion_1_2010_ste93:


==========================================
Amendement à la motion 1 (STE 93) [rejeté]
==========================================


Le syndicat du nettoyage de la région parisienne n’a pas besoin de recourir à des
prestataires privés et payants pour organiser ses réunions, mais est invité à
utiliser les locaux confédéraux et régionaux qui lui reviennent de droit, au
même titre que tous les syndicats de la CNT.


Vote indicatif
==============

+----------------------------+------------+----------+------------+---------------------------+
| Nom                        | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============================+============+==========+============+===========================+
| Amendement à la motion n°1 | 2          | 32       | 4          | 12                        |
| STE93                      |            |          |            |                           |
+----------------------------+------------+----------+------------+---------------------------+


.. _vote_definitif_amendement_motion_1_2010_ste93:

Vote définitif amendement à la motion 1 (STE 93) [rejeté]
=========================================================

+----------------------------+------------+----------+------------+---------------------------+
| Nom                        | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============================+============+==========+============+===========================+
| Amendement à la motion n°1 | 19         | 39       | 5          | 10                        |
| STE93                      |            |          |            |                           |
+----------------------------+------------+----------+------------+---------------------------+


**Amendement rejeté**
