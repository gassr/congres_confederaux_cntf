
.. _motions_adoptéees_congres_CNT_1998_paris:

================================================
Motions adoptées au XXVIe Congrès CNT 1998 Paris
================================================


.. toctree::
   :maxdepth: 2

   strategie_syndicale
   international
   precarite_et_reduction_temps_de_travail
   presse_confederale
   statuts_confederaux
