
.. _amendement_motion_3_2021_interpro31:

=================================================================================================================================================
Amendement à la motion n°3 2021 **De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale** par **Interpro31**
=================================================================================================================================================

- :ref:`motion_6_2021_etpic30`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


Argumentaire 
================

Nous voulons que soit explicité le fait que les syndicats et associations
de travailleurs·euses avec lesquelles il serait pertinent de se jumeler
doivent être des camarades.

Amendement 
============

« La Confédération [...] des syndicats ou des associations de travailleurs
présentes dans d’autres pays **ayant des statuts et des buts similaires aux
notres** et avec lesquelles nous pourrions échanger et soutenir [...] »


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion      |            |          |            |                           |
| n°3 Interpro31              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
