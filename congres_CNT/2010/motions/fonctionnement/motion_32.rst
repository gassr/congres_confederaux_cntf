

.. _motion_32_2010:

============================================================
Motion n°32 2010 : Double affiliation, Culture spectacle RP
============================================================




.. seealso::

   - :ref:`motions_cs_rp_2010`

Motion reprise en 2012 par Chimie-Bzh
=========================================

- :ref:`motion_37_2012`

Motion
=======

Constatant que le monopole syndical à l'embauche n'existe plus, nous présentons
la motion suivante:

La CNT n'accepte pas la double affiliation syndicale.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°32          |            |          |            |                           |
| Culture spectacle RP |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
