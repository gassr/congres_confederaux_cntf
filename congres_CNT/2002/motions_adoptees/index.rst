

.. _motions_adoptéees_congres_CNT_2002_lille:

=========================================================
Motions adoptées au Congrès extraordinaire CNT 2002 Lille
=========================================================


.. toctree::
   :maxdepth: 2

..   strategie_syndicale
..   international
