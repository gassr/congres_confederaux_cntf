
.. _amendement_motion_14_2021_stp67:

========================================================================================================
Amendement N°3 à la Motion n°14 **Financement confédéral des groupes Femmes Libres** par **STP67**
========================================================================================================

- :ref:`motion_14_2021_syndicats_42`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`


Argumentaire
===============

- ?

Amendement
============

Ajout à la fin **"si une fédération des femmes est crée, cette cotisations
et ces groupes seront gérées par cette fédération."**


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°14 |            |          |            |                           |
| STP67                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
