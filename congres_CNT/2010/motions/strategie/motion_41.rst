


.. index::
   Représentativité syndicale

.. _motion_41_2010:

============================================================
Motion n°41 : Représentativité syndicale, Etpics 57 Nord Sud
============================================================



Argumentaire
============

Considérant la loi d’Août 2008 2010 sur la représentativité qui fait obligation
aux syndicats d’avoir deux ans d’existence.

Considérant notre mode d’organisation confédéré qui s’organise sur la base, entre autres,
des fédérations d’industries et des syndicats locaux, départementaux ou régionaux.

Considérant que nous estimons dangereux pour la démocratie interne de créer des
syndicats régionaux ou départements qui s’apparenteraient à des coquilles vides,
mais ayant statutairement le droit de vote aux congrès etc... ce qui ne sera pas
le cas avec des fédérations d’industries.

Considérant qu’il nous faut prévoir, dès à présent, des structures d’accueil afin
de couvrir des syndicats qui, inévitablement, viendront à se créer.

Motion
======

Le syndicat des Employés, des travailleurs et des précaires des industries, du
commerce et des services de la Moselle Nord et de la Moselle Sud invite le congrès
à se positionner en faveur de la création immédiate de plusieurs fédérations d’industries.
Nous invitons tous les syndicats dits «interco», à faire adhérer leurs membres,
à chaque fédération ainsi créée.

Nous laissons au congrès le soin de définir quelles fédérations il s’agit de créer;
bien que pour notre part nous pensions qu’il faille d’ores et déjà acter:

- une fédération des transports et de la logistique
- une fédération de l’industrie automobile et de la métallurgie
- une fédération du commerce, des industries et des services.

Pour la dernière fédération proposée, nous souhaiterions lui donner une dimension
interprofessionnelle, nous permettant ainsi de couvrir tous les champs de
syndicalisation.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°41          |            |          |            |                           |
| Etpics 57 Nord Sud   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso::

   - :ref:`contre_motions_40_41_2010_ste93`
   - :ref:`synthese_motions_40_41_2010`
