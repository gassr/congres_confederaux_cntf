

.. _motion_39_2010:

========================================================================================
Motion n°39 : Elections dans les trois fonctions publiques, Santé-Social CT RP [rejetée]
========================================================================================



Argumentaire
============

La motion de synthèse du dernier congrès confédéral de Lille a réaffirmé le fait
que la CNT ne se présentait pas aux élections dans la fonction publique mais
laissait le choix aux fédérations qui le souhaitaient d'en faire l'expérience.

Depuis le congrès, à notre connaissance, 3 syndicats de 3 fédérations différentes
(éducation, communication-culture-spectacle et santé social) ont pris l'initiative
de se présenter aux élections sans l'accord de leur fédération.

L'argument principal avancé est la réforme à venir de la représentativité dans
la fonction publique qui ferait perdre les droits syndicaux à la CNT et qu'il
faut anticiper.

Or, à ce jour, seul un texte très flou autorisant toutes les interprétations
a été publié. Un des autres arguments est de gagner le droit de déposer des
préavis de grève, droit qui n'existe pas actuellement sauf lorsqu'il a été
gagné comme dans l'éducation ou au CHU de Saint-Etienne pour ce qui est de la
fédération santé social.

Aller aux élections pour quel syndicalisme ?
=============================================

La tentation se fait de plus en plus grande de se présenter aux élections.
Si on regarde la réforme dans le secteur privé, le pourcentage à atteindre pour
être représentatif dans sa branche est une mission presque impossible mais
qui le reste au sein de l’entreprise. Dans la fonction publique c'est une mission
qui paraît encore plus périlleuse.

Cela semble impossible dans les grosses administrations (notamment FP et FPH).
Dans des petites ou moyennes collectivités territoriales, la possibilité d'être
représentatifs semble plus accessible. La réalité de notre implantation fait que
nous sommes souvent très isolés. Participer aux élections signifierait tout
d'abord de trouver des collègues pour remplir les listes.
Si ces collègues n'ont pas rejoint la CNT, pourquoi se mettraient-ils à militer
à la CNT sur le seul motif électoral ?

- Le 1er risque - et ce que beaucoup d'entre nous constatons sur nos lieux de
  travail - est d'avoir des sympathisants, voire des adhérents venant chercher
  une assurance comme dans la plupart des autres syndicats.
- Le 2ème risque pour les élus est de voir davantage de personnes en soutien mais
  d'être toujours tout seul à faire un travail syndical. Ce travail syndical
  fait librement, en fonction des envies, des besoins et du temps possible à
  y consacrer aujourd'hui, devra faire place à un travail syndical dicté,
  d'une part, par les salariés (syndiqués ou pas) qui trouveront normal de s'adresser
  à « l'élu-e de toutes et tous » et d'autre part, de l'administration qui
  contraindra les élu-e-s à travailler sur des dossiers que le syndicat n'aura
  pas choisi.
- Le 3ème risque est de voir venir des salarié-e-s à la CNT uniquement pour saisir
  l'opportunité de trouver un syndicat leur permettant d'aller aux élections,
  comme on peut le rencontrer dans le secteur privé (pour ce qui concerne notre
  syndicat).

La conséquence de ce choix créera très vite des professionnels du syndicalisme
de la fonction publique. Ceci justifiera que le temps de ces militants syndicaux
passé à répondre aux multiples sollicitations des collègues (par exemple lors
des commissions administratives paritaires pour les mutations) nécessite la mise en place de
décharges syndicales. Elles seront certainement justifiées, mais pour quel syndicalisme ?

Ne vaudrait-il pas mieux alors rejoindre Sud ou d'autres fédérations ou confédérations
déjà bien rodées à ce processus ?

Nous pourrions aussi faire le choix d'aller aux élections pour gagner les droits
syndicaux et ne jamais siéger dans les instances.
Combien de temps cette position serait tenable vis à vis de celles et ceux qui
auraient voté pour qu'on les représente ?

Un autre choix pourrait être d'annoncer que la CNT veut être élu mais ne
siègera jamais. C'est une proposition qui risque de manquer de crédit et contre
laquelle le syndicat ne pourra pas intervenir si les élus décident finalement de
siéger à cause d'une pression trop forte ou par choix stratégique.

S'il est un choix à faire, il ne peut relever en ce domaine que de la seule
confédération car il paraît compliqué qu'une fédération dise qu'elle ne se compromet
pas dans les élections et qu'une autre appelle à voter CNT pour préserver les droits
syndicaux.

Motion
======

La confédération réaffirme sa volonté de ne pas participer aux élections dans
la fonction publique (FPE, FPH, FPT) et ne laisse plus la possibilité aux
fédérations de le faire de manière autonome.

**Ce point nous parait assez important pour que le (non) choix des élections
soit une décision confédérale.**

Si entre deux congrès la situation des droits syndicaux venaient à changer
au point d'interdire l'existence de la CNT, un congrès extraordinaire serait
alors convoqué.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°39          | 18         | 48       | 4          | 2                         |
| Santé-Social CT RP   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion n°39 rejetée**


.. seealso::

   - :ref:`contre_motion_39_2010_interco86`
   - :ref:`motions_37_38_39_2010`
