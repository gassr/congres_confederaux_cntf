

.. _contre_motion_17_2010_culture_rp:

==========================================================
Contre-motion à la motions N°17 Culture spectacle RP
==========================================================


Nous proposons de scinder la motion en trois, permettant des votes
différenciés selon les points.


Vote indicatif
==============

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°17     | 38         | 10       | 7          | 6                         |
| Culture spectacle RP               |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_17_2010`
