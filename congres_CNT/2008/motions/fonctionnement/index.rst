

.. _fonctionnement_organique_congres_2008:

===========================================
Fonctionnement organique congrès 2008 Lille
===========================================

.. toctree::
   :maxdepth: 2

   motion_1_2008_ess34
   motion_3_2008_etpic30
   motion_4_2008_etpic30
   motion_5_2008_etpic94_ptt35
