

.. _motions_international_cnt_congres_paris_1998:

================================================================================
Motions "international" 26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
================================================================================


Fidélité à l’esprit de l’internationale de 1922
===============================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 20         | 10       | 7          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


La CNT-AIT reste fidèle à ses origines qui sont la première internationale et
celle de 1922 et, déclare qu'il n'y a actuellement pas l'intérêt de créer une
nouvelle organisation internationale



Pour une initiative internationale
==================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 28         | 1        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La CNT réunie en Congrès, constatant qu'il est temps de prendre les initiatives
nécessaires au développement du courant syndicaliste révolutionnaire et
anarcho-syndicaliste au niveau international et dans un premier temps européen,
forte de la réalité de son intervention dans les luttes qui ont placé
aujourd'hui le mouvement social français au carrefour de l'Europe sociale
appelle dans un esprit d’ouverture et de responsabilité :

- L'ensemble des organisations qui se réclament du syndicalisme révolutionnaire
  et de l'anarcho-syndicalisme ;
- L'ensemble des syndicats, sections qui ne se satisfont plus d’un syndicalisme
  intégrateur aussi rénové soit-il ;
- L'ensemble des structures de lutte sur un terrain particulier qui ressentent
  le besoin d’une organisation permanente de lutte et de proposition du
  prolétariat.


A une Initiative de masse au mois de mai de l'an 2000 à Paris, pour se
rencontrer, débattre et manifester :

- La nécessité d’une Europe des travailleurs face à l’Europe du capital et
  des Etats.
- La nécessité de faire converger les luttes au niveau international.
- L’urgence de faire de la solidarité internationale, une réalité.
