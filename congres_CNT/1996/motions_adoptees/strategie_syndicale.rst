

.. _motions_strategie_syndicale_cnt_congres_lyon_1996:

==================================================================
Motions strategie syndicale du XXVe Congrès de la CNT à Lyon, 1996
==================================================================


Elections prud’homales
======================

A l'occasion des prochaines élections prud'homales, la CNT réaffirme sa ferme
opposition à l'élection de leurs juges par les travailleurs et à l'institution
des conseils de prud'hommes, véhicule d'une justice de collaboration entre le
patronat et les syndicats pour appliquer le droit de l'Etat bourgeois.

Elle mènera une campagne pour l'abstention avec trois principaux slogans:

- celui des années précédentes : "les travailleurs ne s'émanciperont pas en
  élisant leurs juges"
- deux nouveaux slogans:

	* pour tenir compte de la nouvelle situation de la CNT : "contre l'injustice,
	  les travailleurs n'élisent pas leurs juges, Ils luttent et ils se syndiquent !"
	* Pour rappeler que néanmoins nous sommes présents pour défendre les
	  travailleurs devant le tribunal prud'homal : "défenseurs, pas juges!".

La communication RP devra proposer du matériel de propagande



La représentation du personnel dans le secteur privé
====================================================

L'action directe est la base incontournable de notre syndicalisme. Elle est
applicable partout où nous pouvons être présents, dans l'entreprise ou hors
de celle-ci.

Au nom de ce principe, la CNT opère une distinction entre les principales
institutions représentatives du personnel prévues par le code du travail:
S'agissant du comité d'entreprise, celui-ci n'ayant de pouvoir de décision
qu'en matière de gestion des œuvres sociales et ne donnant que des avis au
patron dans la gestion de l'entreprise, est l'institution type de collaboration
de classes que rejette la CNT, d'autant plus que ses membres sont élus sans
mandat précis et sont quasiment irrévocables.

Le délégué syndical sous le contrôle direct du syndicat et / ou de la section
syndicale, porteur de la revendication syndicale, est, au contraire, un outil
de lutte que les militants de la CNT doivent utiliser.

La situation du délégué du personnel est plus complexe. Il est élu par les
travailleurs sans mandat précis et n'est pratiquement pas révocable.
Mais l'expérience acquise ces dernières années par les sections syndicales CNT,
montre son utilité, dans certaines circonstances pour porter la revendication
et protéger les militants contre les licenciements, notamment dans les
entreprises de moins de cinquante salariés où n'existe pas le délégué syndical.

Aussi, la CNT, bien que ne prônant pas la participation systématique aux
élections de délégués du personnel, laisse à ses sections syndicales la
possibilité d'y recourir d'un point de vue tactique, sous le contrôle de
leur syndicat.

Le fait d'y participer ne pourra pas être un motif d'exclusion du syndicat
de notre confédération.

Un bilan écrit sera fait tous les deux ans sur les résultats obtenus au regard
de l'action directe et de la participation aux élections et ce pour tirer les
leçons de cette pratique et que cela puisse servir à l'ensemble de la
confédération.

Un débat devra s'ouvrir dans le BI afin de donner une solution pour les
entreprises où les élections CE et DP sont communes.
