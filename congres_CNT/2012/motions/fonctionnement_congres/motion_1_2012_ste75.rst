
.. index::
   pair: Motion 1 ; 2012 (STE75)


.. _motion_1_2012:
.. _motion_1_2012_ste75:

===================================================================================
Motion n°1 2012 : Disparition du vote par procuration (Art. 16), STE 75 (Rejetée)
===================================================================================
  
.. seealso::

   - :ref:`article_16_statuts_CNT_2010`
   - :ref:`ste_75_pub`
   - :ref:`motions_ste75_2012`




Motion reprise de STE75 2010
============================

.. seealso::

   - :ref:`motion_24_2010`

Argumentaire
============

Lors des congrès de la CNT, il arrive qu'un syndicat donne procuration
à un autre pour transmettre ses décisions d'AG concernant les motions
proposées.

Nous avons conscience qu'il n'est pas toujours facile pour certains syndicats
d'envoyer des mandatés à chaque congrès. Cependant, si les décisions se prennent
lors de rencontres entre personnes mandatées, c'est pour qu'elles puissent être
l'aboutissement d'une réflexion commune et constructive.

Les amendements en sont l'expression.

Un mandat écrit, sans représentant du syndicat qui ait assisté régulièrement à
ses AG et qui puisse y rendre des comptes à son retour, ne permet pas cette richesse.

L'organisation du congrès doit respecter les horaires prévus, afin de permettre
aux mandatés tributaires d'impératifs personnels de rester jusqu'au bout.

Nous pensons que dans l'expression "un syndicat, une voix", le mot voix doit
être entendu dans les deux sens : un vote et une parole.

Motion statutaire
==================

Lors des congrès, chaque syndicat, pour participer aux votes et donc aux débats
qui les précèdent, doit envoyer au moins un mandaté. Il ne peut plus donner
procuration à un autre syndicat.

+----------------------+------------+----------+------------+---------------------------+----------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote | Résultat |
+======================+============+==========+============+===========================+==========+
| Motion n°1           |    29      |    26    |     3      |  4                        |          |
| STE75                |            |          |            |                           |          |
+----------------------+------------+----------+------------+---------------------------+----------+
| Décision du congrès  |            |          |            |                           |          |
|                      |            |          |            |                           | Rejetée  |
+----------------------+------------+----------+------------+---------------------------+----------+


Votes
=====

Pour (29)
----------

- Interco 12, STE 53, STEA 67, STP 67, SIM RP , SIPM, Santé social RP
- STE 75, Interpro 92, ETPICS 94, STE 94, PTT 95, **Santé social 38**, **STICS 38**
- Interco 27, STAF 29,  Interco 31, SSE 31, Santé social 33, SUB TP BAM 33,
- ESS 34, SINR 44, ETPICS 57 Nord-Sud, STAS RA, STP 72, CS RP
- SGTL RP, SUB TP BAM RP Educ 93

Contre (26)
------------

- Interpro 07, Interco 09, Interco 25, ETPICS Sud 30,
- SITAC 30, STTLA, USI 32, PTT 33, ETPRECI 35, SS 35,
- STPM 53, Interco 54, STTLA 57, STE 72, ETPRECI 75, Interco 95
- Educ 13, STICS 13, Education 42, Interpro 42,
- Educ 57, PTT 69, Santé social 69, SUTE 69,
- Interco 74, SII RP

Abstention (3)
---------------

- PTT 37, STERC 59/62, Energie RP


NPPV
-----

- STE 35, Interco 57, STIS 59, Interco 71



Discussion
===========

**STE75**
    un syndicat débat dans son AG, il vient au congrès, on débat, on échange, on
    fait des commissions. On débat ensemble.
    Tous les syndicats doivent être là. Sans procuration, une dizaine de syndicats
    en moins, c'est pas grand-chose.
    On souhaite changer les statuts.

**ETPICS 57**
    un mandaté par syndicat. Le principe démocratique ce n'est pas juste voter,
    c'est exposer ses arguments.
    Source de dérive face aux arguments. Les congrès sont annoncés à l'avance,
    prévoir des aménagements plutôt qu'une procuration.

**STE 57**
    on est contre pas par rapport aux arguments. Le quorum oblige qu'on soit aux
    deux tiers des syndicats. Ceux qui ne peuvent pas se déplacer ne peuvent pas
    participer à la vie de la conf.

**USI 32**
    il faut pouvoir discuter. S'il n'y avait pas de procuration on ne pourrait pas
    tenir le congrès, on n'aurait pas le quorum.

**PTT 95**
    est-ce que ça s'applique à ce congrès ? ça fausserait le congrès et ceux qui
    ont fait la préparation.

**STEA**
    un congrès une fois tous les deux ans, ce n'est pas beaucoup.
    Prévoir des péréquations. On est pour cette motion.

**STE 57**
    une procuration plutôt que le silence pendant 4 ans en tout. Il peut y avoir
    des dérives, prévoir des garde-fous pour ne pas avoir la procuration de 10 syndicats.

**SIM RP**
    on est pour. Que le syndicat qui ne puisse pas se déplacer présente des motions
    que d'autres syndicats reprennent. Le débat est important.
    Cahier des mandatés avec arguments du pour et du contre et non seulement le
    vote par lui-même.

**Educ 13**
    on est contre, on se crée de faux problèmes. On n'a jamais vu de dérives.
    Cela n'est jamais arrivé. C'est très marginal.
    Pour les votes fermés, ils s'abstiennent de voter s'il y a des synthèses ou
    autres.

**ETPICS 57**
    il s'agissait d'assouplir pour des cas particuliers. De faire notre possible
    pour que les camarades viennent. Il faut tout mettre en œuvre pour venir.

**STE75**
    7 syndicats en procuration aujourd'hui, n'aurait pas empêcher la tenue du
    congrès. Rien ne dit que c'est marginal, on ne sait jamais.

**Interco 25**
    C'est la première fois qu'on vient au congrès.
    Sur les motions qu'on ne touche pas les procurations sont acceptables.
    Ce serait dommage qu'il n'y ait pas de procuration, surtout pour les petits
    syndicats, qui font du boulot, pour les congrès c'est galère pour venir.

**STTLA 30**
    les frais sont plus importants pour les petits syndicats, on ne veut pas plomber
    une trésorerie pour venir à un congrès.

**STE 72**
    garde-fou dans :ref:`l'article 16 <article_16_statuts_CNT_2010>`.

**ETPICS 57**
    l'argument n'est pas recevable sur le nombre de procurations aujourd'hui.

ETPICS 94
    inciter les camarades à venir en congrès, avec l'argument du SIM avec
    l'amendement, entraîne un consensus.

**CS RP**
    je comprends pour les petites structures, rentrer en contact avec la tréso conf,
    pour aider ces camarades à venir.

**SUB RP**
    la confédération pourrait prendre en charge financièrement pour les syndicats
    de petite taille.

**SIM RP**
    si question financière, faire la démarche auprès du secrétariat confédéral,
    pour pouvoir se déplacer, si c'est un manque de temps ou de mandatés, ça c'est
    autre chose. La procuration peut exister sur la base de l'argumentaire, que
    s'il y a une modification de la motion que le syndicat qui n'est pas là ne peut
    pas voter.

**STE 93**
    motion statutaire; la bricoler avec un amendement ici, ce n'est pas possible.
