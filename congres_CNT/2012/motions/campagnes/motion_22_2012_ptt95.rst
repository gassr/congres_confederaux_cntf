
.. _motion_22_2012:
.. _motion_22_2012_ptt95:

=============================================================================================================================================
Motion n°22 2012 : Pour une campagne confédérale permanente contre les licenciements, le chômage, la précarité, la rigueur de gauche. PTT 95
=============================================================================================================================================
  
.. seealso::

   - :ref:`ptt95`
   - :ref:`motions_ptt95_2012`
   - :ref:`amendement_motion_22_2012_sinr44`



Argumentaire
============

L’espace européen, mais aussi les nouvelles puissances capitalistes comme la
Chine ou l’Inde, sont confrontés à une résistance sociale contre les politiques
qui entendent faire payer la crise aux travailleurs-ses.

D’Athènes à Madrid en passant par l’Italie des luttes sociales se développent,
dans le même temps qu’au Moyen-Orient les peuples se mobilisent pour
l’obtention de droits démocratiques et sociaux. En France le gouvernement de
gauche entend anesthésier le monde du travail en prétendant appliquer
une politique d’austérité « juste et équitable », ceci alors que des mesures
d’austérité sont prises ou vont être prises dans les mois qui viennent.

Ce miroir aux alouettes socialiste s’appuie sur la mise en spectacle
de « nouvelles » relations sociales incarnées par le Sommet du grand dialogue
social donnant l’image des leaders du syndicalisme institutionnel euphoriques,
rassurés, tendant à montrer que Thibaut avait fait le bon choix en appelant à
voter pour Francois Hollande.

Pourtant la loi implacable de cette société va imposer des milliers de
licenciements, le blocage de salaires, des agressions contre les retraites,
l’augmentation du nombre de chômeurs-ses et la baisse de leurs droits, le
renforcement de la précarité. Cette offensive est susceptible de mobiliser
dans de nombreux secteurs économiques les salariés-ées frappées à un degré
ou à un autre, à la base et de créer des brèches au sein des syndicats
réformistes.

Dans ce contexte la responsabilité de la CNT est d’être présente sur ce terrain
en participant aux mobilisations locales ou nationales qui auront lieu avec
une expression autonome qui n’empêche nullement les actions unitaires à la
base quand elles sont possibles


Motion
======

Le Congrès de Metz se prononce pour l’organisation d’une présence confédérale
sur le terrain de la lutte contre la rigueur de gauche, contre les
licenciements, contre le développement de la précarité et pour la défense
et l’extension des droits des chômeurs, et se dote d’une logistique pour
assurer cette présence : groupe de travail interprofessionnel s’appuyant sur
les syndicats confrontés plus frontalement à cette offensive.

Dans le cadre de cette campagne il semble utile de développer plusieurs axes
spécifiques du positionnement de la CNT :

- Oeuvrer à la coordination des secteurs en lutte.
- Poser le problème du contenu du travail et de sa réduction.
- Populariser l’axe de la reprise en main de l’entreprise ici comme ailleurs.
- Contribuer à l’organisation des chômeurs par, dans un premier temps, une
  intervention confédérale en direction de Pôle Emploi, et par le renforcement
  de liens avec des associations de chômeurs.
- Déterminer des axes revendicatifs sur les droits des chômeurs-ses.
- Se mobiliser dans la lutte contre la précarité et l’intérim en particulier
  dans le secteur public.
- Veiller à l’unité des travailleurs en combattant les arguments visant à
  opposer travailleurs francais et étrangers, et en étant vigilants aux luttes
  des sans-papiers.
- Affirmer une démarche internationaliste par rapport aux délocalisations
  en posant la revendication du salaire égal au sein des mêmes firmes sans
  distinction de zones géographiques.
- Soutenir toutes les initiatives visant à construire des luttes, rencontres,
  actions sur le plan européen.
- Intégrer à cette campagne les propositions de la Région Alsace faites au
  CCN de Juin à Paris sur la revendication d’une mobilisation sur un salaire
  unique européen.


Logistique
----------

Le Congrès adopte le principe de création d’un groupe de travail sur cette
campagne chargée de faire des propositions de tracts, actualisation de ces
tracts, affiches, propositions de débats publiques, dossiers pour notre
presse et nos sites, suivi de l’actualité et information régulière des
syndicats. Ce groupe travaillant en lien avec le Secrétariat confédéral, en
tant que relais, et le Secrétariat international.


Vote
=====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°22          |            |          |            |                           |
| PTT95                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_22_2012_sinr44`
