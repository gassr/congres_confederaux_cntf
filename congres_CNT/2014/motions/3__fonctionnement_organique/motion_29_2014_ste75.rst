
.. index::
   pair: Motion 29 ; 2014

.. _motion_29_2014_ste75:

=====================================================================
Motion n°29 2014 : Outil militant et autogestion, perso@cnt (STE 75)
=====================================================================

.. seealso::

   - :ref:`motions_ste75`
   - :ref:`motion_60_2012_ste75`




Argumentaire
=============

Considérant que l'autogestion ne doit pas attendre la révolution,
la CNT a toujours pour volonté politique d'autogérer tout ce
qu'elle peut, ses média, ses locaux…

Nous considérons que dans le domaine des boîtes aux lettres
électroniques, les dix années précédentes ont montré que nous
savions le faire.
Pour celles et ceux qui en avait une, elle a aussi servi d'outil
militant, montrant justement que l'autogestion n'était pas un vain
mot chez nous.

De plus, cela permet de donner publiquement une adresse, lors d'un
mouvement social par exemple, sans crainte, en affichant son
appartenance syndicale sans pour autant s'y cantonner.
À la fois personnelle et syndicale, elle nous semble une parfaite
synthèse de ce que nous sommes et nous permet de boycotter les
compagnies informatiques.

Bien évidemment, nous avons conscience du problème technique, c'est
pourquoi nous proposons un mandaté spécifique, qui travaillera avec
le mandaté post master.

De même, le nom de l'extension n'a pas besoin d'être exactement
identique (cntf.org, par exemple) mais si nous continuons à nous
développer, il faudra de toute façon l'étendre.

Les mandaté/e/s au réseau informatique sont chargésde trouver les
solutions les moins onéreuses.

Motion
=======

Réactivation des adresses personnelles à la CNT (perso@cnt-f.org) pour
tout adhérent qui en fait la demande par l'intermédiaire de son syndicat.

Celui-ci est donc responsable des éventuelles suites (départ, transfert
à un autre syndicat…).

Un mandaté confédéral est désigné au congrès pour la gestion technique.


Amendements
===========

.. seealso::

   - :ref:`amendement_motion_29_2014_sipmcs`



Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°29 2014

       :ref:`STE75 <ste75>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
