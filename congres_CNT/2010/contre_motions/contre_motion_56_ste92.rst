
.. _contre_motion_56_2010_ste92:

====================================================================
Contre-motion à motion n°56 Orientation syndicale, STE 92, [rejetée]
====================================================================

Argumentaire
============


1) Depuis une trentaine d’années, les mutations du capitalisme sont caractérisées
   par un développement constant du travail précaire sous toutes ses formes (CDD,
   stages, contrats aidés,etc.) aussi bien dans le public que dans le privé et par
   la précarisation des conditions de vie en général. La précarité et le chômage
   de masse mettent en danger l'unité des travailleurs et préparent les régressions
   sociales de demain.
   Si les « grandes » centrales prennent en compte ce phénomène, c’est souvent pour
   la forme. La CNT doit continuer à porter un effort particulier sur la syndicalisation
   des précaires, y compris des travailleurs sans papiers. De ce point de vue,
   ces derniers forment incontestablement la couche la plus exploitée du prolétariat
   et leurs luttes récentes se sont clairement inscrites dans le champ de la production
   et donc des revendications syndicales.
2) Consciente tout à la fois de la force des convictions qui l’animent mais aussi
   de sa dramatique faiblesse au regard des combats à mener, la CNT ne peut que
   pratiquer l’ouverture envers les luttes sociales et considérer qu’elle a tout
   intérêt à coordonner son action avec des syndicats, sections syndicales ou
   collectifs de lutte, proches dans leur fonctionnement ou dans leur orientation.
3) Tout en assurant une coordination sérieuse de ses actions en région ou au niveau
   confédéral, la CNT demeure fidèle à un type d’organisation fédéraliste.
   A cet égard, et compte tenu des difficultés immenses à impulser toute action
   collective, elle considère qu’en son sein, nul n’est détenteur de la vérité
   anarcho-syndicaliste/syndicaliste révolutionnaire. La CNT devrait encourager
   les expérimentations tactiques et stratégiques qui s’inscrivent dans le respect
   du pacte confédéral.

Contre-Motion
=============

Dans le souci de rassembler les travailleurs le plus largement possible, mais aussi
de contrer les logiques de régression sociale portées par le capitalisme, la CNT
fait un effort particulier pour syndiquer les travailleurs précaires du public et
du privé, y compris les travailleurs sans papiers.

Elle fait sienne l'ambition d'un développement massif de ses syndicats, sans que
cela soit une fin en soi, afin de peser dans les luttes, de constituer un pôle
attractif et de donner corps à son projet révolutionnaire. Elle tend tout
autant à inscrire son action dans l’unité avec les syndicats, sections syndicales
et collectifs de lutte de classe, proches dans leur fonctionnement ou dans
leur orientation.

Face à la tâche difficile de reconstruire un syndicalisme révolutionnaire et
un anarchosyndicalisme, elle encourage en son sein les initiatives et les
expérimentations, pour autant que celles-ci s’inscrivent dans la lutte de classe
et la perspective révolutionnaire qui fonde le pacte confédéral.

Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 56       | 20         | 13       | 6          |  30                       |
| STE92                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


**Contre-motion rejetée**


.. seealso:: :ref:`motion_56_2010`
