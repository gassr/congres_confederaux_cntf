
.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center

.. un·e  employé·e·s ·ices ·es
.. pdfx <pdf> -t -o pdf-text.txt
.. pandoc texte.rst -o texte.html
..  <span class="modif_jaune"> texte  <del>initié</del></span>
..  <span class="modif_rouge"> texte </span>
.. <p class="gras vert">paragraphe gras et vert.</p>


.. _UR_CNT:
.. _cnt:
.. _cnt_f:
.. _congres_cnt_f:
.. _doc_cnt:

====================================================================
**Congrès Confédéraux de la CNT-F**
====================================================================


- http://cnt-f.org
- http://static.wiki/en/Conf%C3%A9d%C3%A9ration_nationale_du_travail
- :ref:`statuts_conf:cnt_statuts`
- :ref:`icl_cit_statutes`
- :ref:`regles_organiques`
- :ref:`syndicats_CNT`
- :ref:`glossaire_cnt`
- :ref:`glossaire_juridique`
- :ref:`genindex`



La Confédération nationale du travail (CNT-F) est une confédération syndicale
française de type anarchosyndicaliste.


Origines
========

- http://www.cnt-f.org/presentation.html
- :ref:`livret_accueil_histoire`
- http://fr.wikipedia.org/wiki/Conf%C3%A9d%C3%A9ration_nationale_du_travail_%28France%29


À l'origine, la **Confédération nationale du Travail** est une confédération
syndicale française créée en décembre 1946 à Paris.

Fondée par des militants de la CNT espagnole en exil, des anciens membres
de la CGT-SR (SR pour syndicaliste-révolutionnaire), ainsi que des jeunes
ayant participé à la *Résistance*, qui quittent la CGT du fait de la mainmise
du PCF sur cette organisation, la CNT prend son nom en référence à son
homologue espagnole.

Ayant connu une période de relatif développement après la Seconde Guerre
mondiale, la CNT devient un mouvement plus ou moins groupusculaire à
partir de l'essor de Force ouvrière, issue de la scission de la CGT.

Elle le reste jusque dans les années 1990, où elle connaît un renouveau
militant.

Statuts
=======

.. toctree::
   :maxdepth: 3

   statuts/statuts

Congres Confédéraux
=======================

.. toctree::
   :maxdepth: 3

   congres_CNT/congres_CNT

Commissions
======================

.. toctree::
   :maxdepth: 3

   commissions/commissions


Fédérations d'industrie
=======================

.. toctree::
   :maxdepth: 2

   federations_industrie/federations_industrie

Syndicats
==========

.. toctree::
   :maxdepth: 2

   syndicats/syndicats


Glossaire
==========

.. toctree::
   :maxdepth: 1

   glossaire/glossaire


Méta informations
===================

.. toctree::
   :maxdepth: 2
   :caption: Meta

   meta/meta
