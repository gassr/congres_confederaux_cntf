
==============================
Congrès Fédéral Extraordinaire
==============================


Article 15
==========

En cas de besoin, le bureau fédéral peut convoquer un congrès fédéral
extraordinaire.

De même, un congrès fédéral extraordinaire sera automatiquement convoqué si la
moitié au moins des syndicats adhérents à la fédération en fait la demande.

Commissions d'études techniques
Article 16: Des commissions d'études techniques peuvent être adjointes au bureau fédéral pour la
mise au point des questions revendicatives dans les différents secteurs des transports. Les
commissions n'ont aucun pouvoir de décision. Un Compte-rendu de travail effectué lors de chaque
réunion peut être établi dans les mêmes conditions que les procès-verbaux ou les Comptes-rendus
du bureau.

Les commissions sont constituées par des adhérents de la CNT


Grève
Article 17: Les syndicats fédérés s'engagent à avertir la Fédération, en cas de grève et à
communiquer un maximum d'informations sur la grève.
La Fédération s'engage à apporter son soutien, tant matériel que financier et juridique,
dans la mesure de ses possibilités.
Date et signature:
Le secrétaire:
Le trésorier:
* A chaque fois qu'il est fais mention de « transport », la fédération désigne l'ensemble de l'appellation, à savoir,
transport, logistique et activités auxiliaires.
