
.. index::
   pair: Fédération ; santé social


.. _federation_sante_social:

===============================
Fédération Santé Social
===============================

.. seealso::

   - http://www.cnt-f.org/sante-social.rp/

::

	33 rue des Vignoles - 75020 Paris
	Tel: 06 28 33 42 43
	fede.sante-social@cnt-f.org
	http://www.cnt-f.org/sante-social.rp/


Congrès
=======

.. toctree::
   :maxdepth: 4

   congres/index
