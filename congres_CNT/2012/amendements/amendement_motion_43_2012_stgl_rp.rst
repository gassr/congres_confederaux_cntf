
.. _amendement_motion_43_2012_stgl_rp:

==========================================
Amendement à la motion 43 (STGL RP)
==========================================

.. seealso::

   - :ref:`motions_stgl_rp_2012`
   - :ref:`motion_43_2012_etpic30`




.. warning:: Attention, amendement statutaire arrivé hors délai (NDC)

Amendement (statutaire)
=======================

Pour respecter un équilibre des instances dans le temps nous proposons que
l'exclusion soit entérinée par le CCN dans les 12 mois suivant le dernier
congrès, ensuite par le prochain congrès dans les 12 mois précédant celui-ci,
les congrès confédéraux étant en principe espacés de 2 ans.

Cette décision devra s'appliquer dans la mesure où aucun congrès extraordinaire
n'est envisagé dans la période séparant 2 congrès confédéraux consécutifs


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°43 |            |          |            |                           |
| STGL RP                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
