.. index::
   pair: Motion 7 ; 2021

.. _motion_7_2021_etpics94:

================================================================================================================================================
Motion **n°7** 2021 **Drogues, Alcool et émancipation révolutionnaire** par **ETPICS 94**
================================================================================================================================================

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`


Argumentaire
==============

**Contre les substances psychoactives en milieu militant !**

Les substances psychoactives agissent sur notre système nerveux central, il
existe 3 familles:

- les dépresseurs (Alcool, benzodiazépine, héroïne...),
- les stimulants (Cocaïne, Ecstasys, GHB,...)
- et les hallucinogènes (Kétamine, LSD,...).

Il ne nous semble pas nécessaire, d'aborder ici l'effet néfaste des drogues
*dures* et illégales (cocaïne, héroïne,...), il nous semble que sur ce point
nous serons tous d'accord dans la confédération (si jamais ce n'est pas le cas
nous attendons un retour ou une contre-motion).

Avant de poursuivre nous souhaitons indiquer que toute personne ayant ce genre
de consommation ponctuelle ou régulière, n’est pas une personne non fréquentable !

Qu'il serait bien qu'elle puisse en parler et être accompagnée par les associations
médico-sociales qui s'occupent de ce champ.
Par cette motion nous souhaitons vous convaincre que l'alcool est une drogue dure,
très dure !
**C’est la drogue désinhibitrice par excellence.**

À cela s’ajoutent des effets euphorisants, anxiolytiques, relaxants. Il donne
donc l’impression de se sentir "mieux", de perdre le contrôle, de rire, de pleurer,
de parler davantage, on se sent fort, invincible presque, la réalité fait moins peur !

Bref puisque les barrières inhibitrices s’effondrent *tout est permis !*

Ces effets s’accompagnent de **troubles neurologiques** : perte de l’équilibre,
analgésie, troubles cognitifs.
L’addiction a un effet dominant, c’est la définition même de la dépendance, qui
s’oppose à la liberté !

Certes l'alcool est légal, mais elle est de loin la pire ! D'une part car elle
est très accessible, ensuite elle provoque de graves dommages aux organes vitaux
(foie, estomac, cerveau), c'est la seule drogue, lors d’un arrêt brutal
(sevrage non médicalisé) qui peut provoquer la mort (crise d'épilepsie,
crise cardiaque).
C'est aussi la drogue la plus néfaste pendant la grossesse, les bébés naissent
avec de gros retard de développement cognitif souvent irrémédiable.
Voilà pour les infos si vous n'en aviez pas connaissance.

Nous souhaitons partager avec vous notre réflexion concernant la consommation
d'alcool dans nos milieux militants, en AG (et toutes instances de la CNT) mais
aussi lors de nos événements.
Dans nos instances, parce que nos espaces de débats, de réflexions, d’élaboration
de nos d’actions syndicale doivent être préservés, parce que nous devons être
tous lucides, notre force de convictions révolutionnaires doit rester intacte.
C’est pourquoi nous proposons la motion A.

Motion
=======

**La confédération prend toute la mesure des conséquences de l’alcool et des
autres drogues sur le mouvement révolutionnaire**.

Elle se donne les moyens en son sein de préserver ses espaces de réflexions et
de prises de décisions, pour développer et construire l’émancipation,
l’organisation et ses luttes en **refusant la consommation d’alcool et autres
drogues dures dans ses instances**.


Amendements
===========

- :ref:`amendements_motion_7_2021_interpro31`
- :ref:`amendement_motion_7_2021_stics72`
- :ref:`amendement_motion_7_2021_stp67`

Contre-motion
==================

- :ref:`contre_motion_7_2021_ste72`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°7 2021 ETPICS94    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
