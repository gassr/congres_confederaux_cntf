

.. _motion_x_2014_ste49:

===================================================================================
Motion à propos des élections (STE 49, statutaire),  (Hors délai)
===================================================================================




Motion
======


La Confédération Nationale du Travail est une organisation d'inspiration
anarcho-syndicaliste et révolutionnaire.

Fidèle à ce principe, elle ne peut accepter la participation ou
l'expression de l'un-e de ses membres dans le cadre d'une élection hors
du champ professionnel ainsi qu'à tout mouvement à visée électorale afin
de maintenir la Confédération en dehors de toute ambiguïté entre sa propre
orientation et les éventuels choix de ses membres.

Néanmoins, chacun-e est libre de ses choix en fonction des contextes,
des projets portés et de ses envies.

Un-e membre de la CNT peut donc ainsi se présenter s'il-elle le souhaite
mais sa candidature devra ainsi répondre à plusieurs obligations:

- abandonner l'ensemble de ses mandats à l'intérieur de la CNT, qu'ils
  soient syndicaux, régionaux ou confédéraux ;
- interdiction de se présenter comme membre de la CNT ou comme syndicaliste ;
- rester dans un cadre municipaliste : à savoir:

    - aucune étiquette politique identifiable sur la liste ;
    - retrouver des points programmatiques d'inspiration libertaire et
      autogestionnaire (refus de l'élection du maire, favoriser la
      démocratie directe, ré-appropriation des terres agricoles, conseillers
      municipaux considérés comme mandatés techniques révocables, entre
      autres exemples ...) ;
    - refuser les indemnités et toutes les délégations de pouvoir (adjoint, conseiller
      communautaire ...)
    - aucune candidature possible sur les préfectures, sous-préfectures
      et villes de + 10 000 habitants qui par leurs caractéristiques
      sont de fait soumises à des enjeux politiques (sauf cas particulier
      nécessitant d'être approuvé à l'unanimité par les syndicats de l'UR
      concernée par cette candidature).


De plus, toute candidature devra être préalablement validée par le
syndicat auquel est affilié le-la membre décidant de se présenter puis
par l'UR correspondante à ce syndicat ainsi que par la Confédération
(en CCN, celui-ci représentant les délégués des UR), ces instances
validant ou non la candidature par rapport aux conditions préalablement
définies.

Le non-respect de ces critères ainsi que toute démarche individuelle
entraîneront l'exclusion du syndicat pour le-s membre-s fautifs.
