
.. _motion_structuration_confederale_cnt_congres_1993_paris:

=======================================================================================
Motion **Structuration confédérale** 1993 de la CNT  XXIVe Congrès de la CNT à Paris
=======================================================================================

Compétences du Comité Confédéral (C.C.N.)
=========================================


Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 12         | 0        | 1          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le :term:`CCN` est composé par le bureau confédéral et le ou les délégués de
chaque région ainsi que des secrétaires des fédérations à titre consultatif.

Le CCN est convoqué par le bureau confédéral avec un ordre du jour, fourni aux
régions avec suffisamment de délai pour permettre l'étude de celui-ci dans
les syndicats.

Le CCN n'est pas un comité permanent de la CNT, mais une réunion ponctuelle
semestrielle de travail, permettant:

- d'aider le BC pour les missions de Congrès qu'il ne pourrait accomplir ;
- de coordonner les campagnes nationales;
- d'aider au développement des syndicats de formation récente.

Les zones sont constituées par au moins trois syndicats composés au minimum
de cinq adhérents chacun, et ce dans au moins deux villes différentes.



Siège de la CNT
===============

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 12         | 0        | 1          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le siège de la CNT reste au 33, rue des Vignoles 75020 PARIS.
