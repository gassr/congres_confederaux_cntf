

.. _motion_30_2010:
.. _motion_30_2010_ste75:


====================================================================
Motion n°30 2010 : Outil militant et autogestion, perso@cnt, STE 75
====================================================================



Motion reprise par la motion 60 STE75 2012
==========================================

.. seealso::

   - :ref:`motions_2010_2012`

- :ref:`motion_60_2012`


Argumentaire ,Syndicat des travailleuses et des travailleurs de l'éducation à Paris
====================================================================================

Considérant que l'autogestion ne doit pas attendre la révolution, la CNT a
toujours pour volonté politique d'autogérer tout ce qu'elle peut, ses média,
ses locaux...

Nous considérons que dans le domaine des boîtes aux lettres électroniques, les
dix années précédentes ont montré que nous savions le faire. Pour celles et
ceux qui en avaient une, elle a aussi servi d'outil militant, montrant justement
que l'autogestion n'était pas un vain mot chez nous. De plus, cela permet de
donner publiquement une adresse, lors d'un mouvement social par exemple, sans
crainte, en affichant son appartenance syndicale sans pour autant s'y cantonner.
À la fois personnelle et syndicale, elle nous semble une parfaite synthèse de
ce que nous sommes et nous permet de boycotter les compagnies informatiques.

Bien évidemment, nous avons conscience du problème technique, c'est pourquoi
nous proposons un mandaté spécifique, qui travaillera avec le mandaté postmaster.
De même, le nom de l'extension n'a besoin d'être exactement identique
(cntf.org, par exemple) mais si nous continuons à nous développer, il faudra
de toute façon l'étendre.

Les mandaté/e/s au réseau informatique sont chargés de trouver les solutions
les moins onéreuses.

Motion
------

Réactivation des adresses personnelles à la CNT (perso@cnt-f.org) pour tout adhérent qui en fait la demande par
l'intermédiaire de son syndicat. Celui-ci est donc responsable des éventuelles suites (départ, transfert à un autre
syndicat…). Un mandaté confédéral est désigné au congrès pour la gestion technique.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°30          |            |          |            |                           |
| STE 75               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
