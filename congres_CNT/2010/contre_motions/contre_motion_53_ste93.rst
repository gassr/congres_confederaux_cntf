

.. _contre_motion_53_2010_ste93:

===========================================
Contre-motion à la motion N°53 (STE93)
===========================================


Le STE 93 propose donc que le secteur vidéo ait la responsabilité et la liberté
de produire du contenu vidéo mais qu'il doit ensuite le soumettre aux membres
de la CA afin que la vidéo soit validée comme confédérale.

Charge ensuite aux mandaté-e-s internet de poster le contenu sur le site confédéral
pour en assurer la diffusion la plus large possible.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motions n°53    |            |          |            |                           |
| STE 93                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_53_2010`
