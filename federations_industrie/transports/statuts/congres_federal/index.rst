
.. index::
   pair: Congrès fédéral; Transports

===============
Congrès Fédéral
===============

Article 14
===========

Le congrès des syndicats adhérents à la Fédération a lieu tous les deux ans.

Il est seul habilité à modifier les présents statuts. Toute modification doit
être portée à la connaissance de l'ensemble des syndicats de la Fédération.

C'est le congrès qui:

- fixe les axes de travail collectif de la Fédération,
- qui discute le rapport moral et financier du bureau fédéral sortant
- et élit le nouveau bureau fédéral.

Les modalités de vote au congrès fédéral sont celles en vigueur pour
le :ref:`congrès confédéral de la CNT <modalites_vote_congres_confederal>`.
