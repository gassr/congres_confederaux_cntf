
.. _amendement_motion_6_2021_sanso69:

====================================================================================================================================================
Amendements N°1 à la motion n°6 2021 **De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale** par **SANSO69**
====================================================================================================================================================

- :ref:`motion_6_2021_etpic30`


Autres motions sans69

    - :ref:`rhone_alpes:motions_sanso69`


.. _amendement_1_motion_6_2021_sanso69:

Amendement 1
==============

Argumentaire 
---------------

**Nous sommes contre la revendication de la décroissance.**

Bien que la notion de décroissance trouve son origine dans une écologie
radicale et un refus du capitalisme, elle est basée sur la diminution
du PIB donc garde les mêmes références du capitalisme, elle est utilisée
en restant dans un cadre capitaliste. (pour un autre capitalisme à la rigueur).

La décroissance n'est pas en soi anticapitaliste (et certains capitalistes
pourraient même y trouver leur compte) et ne dit rien de l'exploitation
de classe.

Associée à des termes comme assumée, absolue, choisie elle reste vide de sens.

Amendement
------------

Remplacement du titre **Pour une décroissance assumée, absolue, choisie**
par **Pour une société basée sur nos désirs et non sur la production et
la consommation**.


Vote
====

+------------------------------+------------+----------+------------+---------------------------+
| Nom                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==============================+============+==========+============+===========================+
| Amendement 1 à la motion n°6 |            |          |            |                           |
| SANSO69                      |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès          |            |          |            |                           |
|                              |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_6_2021_sanso69:

Amendement 2
==============

Argumentaire
--------------

Décroître les productions inutiles nous semble absurde, si elles sont
inutiles elles doivent disparaître (ou alors on ne raisonne pas en terme
d'utilité/inutilité, la question de l'utilité est en soi un débat).

Amendement
-----------

Remplacement de la phrase « Nous soutenons que le nombre de productions
inutiles doit décroître parce qu’elles sont polluantes et nuisibles »
par « Nous soutenons que les productions inutiles, polluantes et nuisibles
doivent disparaître ».


Vote
====

+------------------------------+------------+----------+------------+---------------------------+
| Nom                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==============================+============+==========+============+===========================+
| Amendement 2 à la motion n°6 |            |          |            |                           |
| SANSO69                      |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès          |            |          |            |                           |
|                              |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
