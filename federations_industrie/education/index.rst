
.. index::
   Fédération (Travailleurs de l'éducation)


.. _federations_fte:

==========================================
Fédération des travailleurs de l'Education
==========================================


.. seealso::

   - https://fr.wikipedia.org/wiki/CNT-FTE
   - http://www.cnt-f.org/fte

.. toctree::
  :maxdepth: 4

  publications/index
