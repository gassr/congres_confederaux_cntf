
.. _amendement_motion_9_2021_tasra:

============================================================================================
Amendement N°1 à la Motion n°9 2021 **Lutte contre la Précarité** par **TASRA**
============================================================================================

- :ref:`motion_9_2021_interco69`

Autres motions TASRA

    - :ref:`rhone_alpes:motions_tasra`


Amendement
============

.. raw:: html

       Se mobiliser dans la lutte contre la précarité, l’intérim en particulier
       dans le secteur public, et l’expansion d'un salariat déguisé
       <span class="modif_jaune"> (auto-entrepreneurs, faux indépendants,…) promu <del>initié</del></span>
       par l'économie numérique et la libéralisation dans tous les secteurs (prive/public).


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°9  |            |          |            |                           |
| TASRA                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
