.. index::
   pair: Motion 5 ; 2021

.. _motion_5_2021_ptt95:

================================================================================================================================================
Motion **n°5** 2021: **Pour l’adhésion de la CNT à la Confédération Internationale du Travail. C.I.T - Engager le processus** par **PTT 95**
================================================================================================================================================

- :ref:`annexe_motion_5_2021_ptt95`
- :ref:`icl_cit`

Autres motions PTT95

    - :ref:`paris:motions_ptt95`


.. figure:: ../../../images/ICL-CIT_Logo.png
   :align: center

   :ref:`icl_cit`

Préambule
==========

Les motions de congrès sur la démarche internationale de la CNT.

Notre demande d’adhésion est la suite logique de nos analyses, nos actions
et campagnes internationales de toutes ces dernières décennies, comme il
apparaît dans le récapitulatif des motions de Congrès confédéraux antérieurs,
qui suit

Argumentaire.
==============

Petit historique.

Depuis que le mouvement ouvrier s’organise pour mener un combat anticapitaliste
le courant syndicaliste révolutionnaire, antiautoritaire, a toujours situé son
intervention d’un point de vue international.
Comme en attestent quelques repères : de 1864 à 1872 au sein de la Première AIT,
puis jusqu’en 1879/1880 dans le cadre des liens tissés avec la Fédération Jurassienne
animée par Bakounine et James Guillaume.

Ensuite, cette tendance internationale est intervenue dans le cadre de la
IIème Internationale créée le 14 juillet 1889 et dans le cadre de la Fédération
Syndicale Internationale, ceci en étant minoritaire.

Une tentative de constituer une centrale syndicale internationale révolutionnaire
a échoué à Londres, en 1913, et la CGT porte une part de la responsabilité de
cet échec.

Cette internationale ASSR a finalement vu le jour en 1922 avec la fondation
de l’AIT, **autonome vis-à-vis de la IIIème Internationale** créée dans la foulée
de la révolution russe de 1917.

Pour la France la CGT-SR, constituée en 1926 en tant que scission de la CGTU
sous influence communiste, a immédiatement adhéré à l’AIT dite de Berlin, ceci
jusqu'à la disparition de la CGT-SR en 1939.

La CNT et l’internationalisme
==============================

Dès sa création en 1946 la CNT-F a adhéré à l’AIT, participant à ses congrès
jusqu'en 1996, date à laquelle **nous avons été exclus par 2 voix contre 1**.

Depuis nous avons tissé des liens selon des axes définis en Congrès (voir :ref:`annexe <annexe_motion_5_2021_ptt95>`).

Nous avons mené des initiatives internationales ayant eu un écho significatif
(Mai 2000 avec 5000 manifestants.es à Paris le 1er mai, Colloque IO7 en 2007,
notamment).
Nous participons aujourd’hui au Réseau Syndical International de Solidarité et
de Luttes (RSISL) et à la Coordination Rouge et Noir qui a été à l’initiative de
rencontres et d’actions communes notamment le 1er mai et le 8 mars, de solidarités
autour de luttes.

Après 1996 l’AIT a suivi sa propre voie et les contradictions présentes en son
sein ont fait voler en éclat l’unité interne qui s’était formée contre la CNT-F.

Des sections significatives comme la FAU allemande, puis plus récemment la CNT-E,
l’USI, ont quitté une AIT devenue inaudible et groupusculaire, et entamé un
processus de refondation d’une nouvelle AIT.

Ce processus a été mis en mouvement en 2016 à Bilbao, s’est poursuivi à Francfort
en 2017, et en **mai 2018 la CIT a été constituée à Parme**.

A chacune de ces étapes nous avons été présents.es en tant qu’observateurs.trices,
comme d’autres organisations : COB-Brésil, VB-Belgique, FORA Argentine, et des
compte-rendus ont été communiqués par le SI.

Yohann pour le SI a publié en Janvier 2019 une circulaire qui fournit tous les
compte- rendus de ce processus et les statuts de la nouvelle Internationale ASSR.
Au final outre les syndicats à l’initiative du congrès, CNT-E, FAU, IP (Pologne),
ESE(Grèce) il faut noter que les IWW présents dans plusieurs pays sont partie
prenante de cette dynamique internationaliste, ce qui est un petit évènement
dans l’histoire de cette organisation.

Ont rejoint la CIT en tant que membres la IP-Pologne et l’ESE-Grèce, qui
appartiennent également à la Coordination Rouge et Noire et les IWW (Etats-Unis
et Canada, Grande-Bretagne, Allemagne).

**Nous proposons que la CNT-F adhère à la CIT**, et qu’un processus soit mis en
place, afin de rejoindre à terme les forces du mouvement anarchosyndicaliste
mondial, organisé de manière autonome, pour ses buts spécifiques.

**L’adhésion de la CNT à la CIT** ne nous empêchera pas de continuer de travailler
avec les contacts que nous avons gardés, d’être présents au sein de la
Coordination Rouge et Noir, comme IP et ESE.

L’adhésion à une Internationale constitue un saut qualitatif et quantitatif sur
le plan de l’unité du syndicalisme révolutionnaire et de l’anarchosyndicalisme
qui participe alors à l’apprentissage d’un fonctionnement collectif ou hommes
et femmes ignorent les frontières.

Cette organisation anticipe le monde à construire.

La CIT ne pratique pas l’exclusivité : plusieurs organisations-membres d’un même
pays peuvent exister, ce qui est la même position que nous avons porté dans la
Coordination Rouge et Noire.
Le choix des stratégies syndicales reste libre et ne concerne que les
organisations membres.

L’absence d’une référence internationaliste dans le sigle (Confédération **nationale** tout seul),
qui a posé problème dans le passé, sera réglée.

**Rejoindre la CIT s’est renouer un fil brisé en 1996 lors de notre exclusion
bureaucratique.**

Motion
=======

La Confédération CNT met en place une démarche de contact avec la CIT dans le but
d’examiner les possibilités d’adhésion de notre organisation à cette nouvelle
internationale ASSR, de confrontation de nos pratiques, d’analyse des statuts
de la CIT, afin de voir ce qui est réalisable.

Le prochain SI, ou un groupe de travail spécifique, en lien avec le :term:`BC`, gère
cette démarche et en rend compte aux syndicats de la CNT.

Parallèlement, en interne, des échanges ont lieu sur les statuts de la CIT,
son activité. A l’issue de ce processus les syndicats de la CNT seront appelés
à se prononcer pour ou contre une éventuelle adhésion.

CNT PTT 95 le 30/01/2021.


Annexe
========

- :ref:`annexe_motion_5_2021_ptt95`


Amendements
===========

- :ref:`amendement_motion_5_2021_sipmcs`
- :ref:`amendement_motion_5_2021_stp67`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°5 2021 PTT 95      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
