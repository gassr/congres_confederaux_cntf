
.. _contre_motion_1_2021_etpics94:

===========================================================================================================================
Contre-motion N°1 à la motion n°1 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **ETPICS94**
===========================================================================================================================

- :ref:`motion_1_2021_ste33`

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`


Argumentaire
================

La CNT est un **syndicat de lutte de classe** qui se définit
sur le plan **économique et social**.
La société actuelle nous fabrique une **bourgeoisie anti-raciste**,
des **féministes bourgeoises**. La liste des antagonismes et des
binarités est illimitée, c'est pourquoi il est inutile de les nommer.

**Soyons contre toutes les discriminations et toutes les oppressions, ne
nous mettons pas de carcans**.

Le patriarcat et le racisme existent mais pas seulement et se voient
impliqués au-delà du capitalisme (avec l'Etat et la religion entre
autres), **pour les combattre nous l'avons toujours fait** et le ferons
au niveau de chaque syndicat et le syndicat reste et restera l'identité
originelle de la CNT.

C'est ce pourquoi nous luttons. La :ref:`motion 1 du ste 33 <motion_1_2021_ste33>`
transforme la CNT en **quelque chose d'autres**.


Contre-motion
===============

Contre-motion à la motion 1 à inclure dans l'article 1 des statuts
confédéraux :

**L'émancipation des travailleurs·euses sera l'oeuvre des travailleurs·euses
elleux-mêmes.**


Chartes en rapport (Charte d'Amiens)
======================================

- :ref:`statuts_conf:tant_materielles_que_morales`

::

    Le Congrès considère que cette déclaration est une reconnaissance de
    la lutte de classe, qui oppose sur le terrain économique, les travailleurs
    en révolte contre toutes les formes d'exploitation et d'oppression,
    tant matérielles que morales, mises en oeuvre par la classe capitaliste contre
    la classe ouvrière.

Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 |            |          |            |                           |
| ETPICS94                      |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
