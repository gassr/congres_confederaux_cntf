

.. _motion_ecologie_cnt_congres_lyon_1996:

==============================================================================
Motion "Ecologie", 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
==============================================================================


.. index::
   écologie

La C.N.T et l’écologie
======================

Aujourd’hui comme hier le capitalisme ne se définit pas par la seule
exploitation de l'homme par l'homme. II exerce ses ravages non seulement dans
l'entreprise mais aussi dans tous les moments de la vie quotidienne (qu’il tend
d'ailleurs a réduire à la seule consommation) jusqu'à l’ensemble de la
biosphère.

Dominant pratiquement la planète entière, ne cessant de vouloir tout réduire à
de la marchandise, le capitalisme dans sa logique productiviste gaspille,
épuise et détruit une à une les ressources de la nature.

Ainsi la terre devient une immense poubelle dont certains déchets comme ceux
du nucléaire, seront encore dangereux dans des milliers d’années.

On sait déjà que le futur de l’humanité quelle que soit l'allure qu'il prenne,
sera dépendant de ces conditions présentes.

On s'en doute, ce n'est pas le système capitaliste qui pourra résoudre à coup
de décrets ministériels les dégâts qu'il cause à la nature.

Donc ceux-ci ne peuvent être réduits à un aspect secondaire de notre lutte.

Ils doivent même être constituants de notre projet de société car nous ne
pourrons pas faire l'impasse de ce handicap majeur. Les ignorer, comme les
sous-estimer, seraient pour le mouvement anarcho-syndicaliste une grave erreur.

Notre conception de l’écologie ne vise pas à développer une "meilleur gestion
de l'environnement" mais tout d’abord à faire une critique radicale du rapport
d’exploitation utilitaire que la modernité essentiellement technologique
entretient envers la nature.

Acteur du mouvement écolo lorsque celui-ci n’était pas encore encadré et
représenté politiquement, nous avons eu tort de délaisser ce terrain de lutte.

Celui-ci ou se jouent les contradictions du capitalisme avec l'ensemble du
milieu naturel dont l’humanité fait partie intégrante, amène forcement à une
prise de conscience telle qu’elle ne peut être apaisée par un bulletin de
vote.

Une telle lucidité ne peut être déléguée aux seuls spécialistes et autres
techniciens d'un « capitalisme propre ». Elle ne peut s'exercer qu'au travers
d'un mouvement social en liaison avec toutes les nécessités de la révolte
anticapitaliste, antiétatique, et antiautoritaire.

C'est parce qu’elle participe pleinement de la remise en cause du capitalisme
que nous ne devons pas laisser récupérer l’écologie aussi bien par les
politiciens que par les sectes new-age ou les fascistes.

**En ce sens notre syndicalisme de classe est écologiste !**
