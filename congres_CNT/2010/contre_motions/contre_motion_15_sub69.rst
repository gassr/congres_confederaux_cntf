
.. _contre_motion_15_2010_sub69:

==========================================================================================================================================
Contre-motion à la Motion n°15 2010: Structuration du Bureau Confédéral de la CNT et de la Commission administrative, SUB69 [rejetée]
==========================================================================================================================================

Voir la :ref:`motion 15 <motion_15_2010>`



Argumentaire
============

Nous constatons depuis de nombreuses années, que l'organisation et la rotation
des mandats confédéraux, ne permet pas toujours d'en tirer le maximum pour le
développement de la confédération.

Essentiellement du fait du manque de camarades volontaires, les tâches se retrouvent
morcelées et éclatées entre divers mandaté-e-s souvent éloigné-e-s géographiquement.

Pour des postes clefs comme le :term:`bureau confédéral`, ce manque d'investissement
collectif dans les mandats confédéraux, s'est traduit trop souvent par une
limitation aux tâches administratives ne permettant pas d'investir au mieux
les tâches de coordination et d'animation syndicale.

C'est pourquoi les syndicats (Educ, Santé-Social et SUB 69) ont décidé de proposer
au mandatement du congrès, une équipe locale pour les mandats du secrétariat
confédéral qui serait composée d’une équipe formée d'un secrétaire confédéral,
de deux secrétaires confédéraux adjoints administratifs, d’un secrétaire confédéral
adjoint  au développement, à la coordination des campagnes et à la solidarité
confédérale (si sa création est entérinée par le congrès) ainsi que d'un secrétariat
à la communication extérieure (voir motion ci-dessous).

**Ces cinq mandats nous apparaissent centraux pour dynamiser la vie syndicale
confédérale et le développement de notre confédération.**

Pour nous, le fait qu'ils soient éventuellement assurés par des camarades
d'une même ville, avec bien sûr les camarades d'autres syndicats pouvant les
épauler, sera un élément supplémentaire de dynamisme, facilitant la réactivité
de la confédération. Cela nous permettrait, entre autre, d'assurer une
permanence de travail hebdomadaire, pour se répartir et mutualiser les tâches.

Ce principe d'organisation avait par exemple été très bénéfique lorsque le
secrétariat fédéral FTE avait été assuré par l'Educ 69.

Cette contre-motion, reprend donc l'esprit de la motion initiale présentée par
l'Etpic 30 Sud mais en l'élargissant et en présentant des amendements en lien
avec le mode de fonctionnement que nous proposons au congrès.

C'est comme cela qu'il faut comprendre par exemple la proposition différente
de répartitions des tâches entre les secrétaires confédéraux ou encore la précision
concernant les liens de travail entre secrétariat confédéral, secrétariat à
la communication extérieure (voir ci-dessous) et celui au développement et à
la coordination des campagnes.

Elle propose aussi des modifications dans l'optique générale d'une plus grande
cohérence de certains mandats en visant notamment à séparer les taches **politiques**
et **techniques**.

Dans ce sens, nous proposons de créer un Secrétariat à la communication
extérieure, regroupant:

- les activités des relations médias
- animation du site confédéral
- production de matériel à destination des syndicats et salariés
  (tracts, argumentaires...).

Toutes ces activités sont étroitement liées, elles représentent une vitrine
extérieure de la confédération, une organisation commune les rendraient plus
pertinentes.

Cela a aussi l'avantage de retirer ces tâches **politiques** des mandats
propagande et :term:`webmaster` pour en faire de véritables mandats techniques
ce qui devrait aussi faciliter leur rotation.

Motion
======

Annule et remplace la motion «Organisation du Bureau confédéral» du 25ème
Congrès confédéral des 1, 2, et 3 novembre 1996 à Lyon.

Le Bureau Confédéral se compose des mandatéEs suivantEs:

- L'équipe du Secrétariat Confédéral:

    * Secrétaire confédéral : MandatéE de la coordination et l'animation générale
      de la Confédération, de la CA, et du BC. Assure une veille quant au
      fonctionnement, aux orientations et au respect des statuts de la CNT.
      Validation des prises de positions confédérales.
    * Secrétaire confédéral adjoint : Aide administrative générale et représentation
      du Secrétariat confédéral. MandatéE aux relations aux contacts isoléEs et
      du développement, des relations contacts diverses, de l’archivage électronique
      et l’animation de l’intranet BC. Aide administrative générale et représentation
      du Secrétariat confédéral.
    * Secrétaire confédéral adjoint: Aide administrative générale et représentation
      du Secrétariat confédéral. MandatéE aux labellisations, et de la gestion
      de l’annuaire confédéral des syndicats et des Unions
    * Secrétaire confédéral adjoint au développement, de la coordination des
      campagnes, et de la solidarité confédérales (cf motion présentée par
      Sanso 69 sur la création d'un secrétariat confédéral au développement)
    * Secrétaire confédéral adjoint  à la communication extérieure : Rédaction
      et diffusion des communiqués confédéraux, développement des contacts avec
      la presse, ChargéEs de susciter la réalisation d'affiches tracts,
      répondre aux propositions reçues des syndicats, ainsi que de de l’animation
      du site web confédéral.

- L'équipe de la Trésorerie Confédérale:

    * TrésorierE confédéralE : MandatéEs à la gestion des comptes confédéraux,
      et la validation des engagements financiers
    * TrésorierE confédéralE adjointE : MandatéEs à la mise à jour de l’annuaire
      confédéral & comunications, aux relances et relations avec le secrétariat
      chargé des labellisations et à la gestion de l’annuaire confédéral, à la
      gestion des prêts confédéraux.
      Aide et représentation du/dela trésorierE confédéralE.
    * TrésorierE confédéralE adjointE : MandatéEs à la gestion des cartes confédérales
      et de la caisse de solidarité. Aide et représentation du/dela trésorierE confédéralE.

La Commission Administrative se compose des mandatéEs confédéraux suivants:

- Le Bureau Confédéral
- Secrétariat International : Animation et coordination d’une commission internationale
  regroupant plusieurs responsables par secteurs géographiques ou linguistiques,
  un webmaster, un chargé des relations le CS
- Secrétariat à la propagande : Une commission de mandatéEs chargéEs de gérer
  des besoins en matière de matériel de propagande confédéral, d’assurer les tirages
  et productions nécessaires, assurer la distribution du matériel. Elle peut aussi en
  lien avec le Secrétariat à la communication extérieure et la CA CNT éditer du matériel
  adapté aux campagnes de la CNT et développer une ligne éditoriale confédérale
  de brochures ;
- Combat syndicaliste : Comité de rédaction & administration & webmaster ;
- Secrétariat au Bulletin Intérieur : Edition et diffusion
- Temps Maudits : Comité de rédaction & administration
- Secrétariat aux affaires juridiques : Animation et coordination d’une commission
  confédérale juridique. Assure une veille et une assistance juridique pour
  les syndicats et leurs Unions ;
- Webmaster : ChargéEs De l’administration du site web confédéral, de l’ouverture
  des domaines pour les Unions et syndicats de la CNT, de l’assistance technique
  aux syndicats et Unions, des relations avec l’hébergeur ;
- Postmaster : Création et gestion des mails confédéraux, création et gestion des
  listes de discussions confédérales, assistance technique aux syndicats et Unions,
  relations avec l’hébergeur, gestion et animation de la liste info contacts ;
- Intranet : Commission de mandatéEs chargées de la création et de la gestion des
  accès des syndicats et Unions à l’Intranet Confédéral, assistance technique
  aux syndicats et Unions, de la maintenance technique de l’intranet confédéral,
  des relations avec l’hébergeur, de la modération du forum ;

Les mandatés de la Commission administrative et du Bureau Confédéral sont invités à
concerter leur action régulièrement par tous moyens à leur convenance : réunions,
liste de discussion, vidéoconférence, etc.

Ils agissent collectivement dans l’intérêt de la CNT, dans la fraternité et la dignité
que leur suggère leur mandat.

L’échange et le partage des informations sont favorisés vers la mutualisations
des productions respectives, dans la limite des prérogatives propre chaque mandat.

SUB 69

.. seealso::

   - :ref:`motion_15_2010`


.. _vote_contre_motion_15_2010_sub69:

Vote contre-motion 15
=====================

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion  n°15    | 32         | 23       | 6          | 8                         |
| SUB69                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


**Contre motion rejetée**


.. seealso::

   - :ref:`motion_15_2010`
