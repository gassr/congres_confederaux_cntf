
.. index::
   pair: Motion 6 ; 2016
   pair: Réseaux sociaux ; twitter
   pair: Réseaux sociaux ; Facebook

.. _motion_6_2016_sante_social_lorraine:

==================================================================================
Motion n°6 2016 : CNT et réseaux sociaux (Santé Social CT Lorraine) [adoptée]
==================================================================================

Argumentaire
============

Les réseaux sociaux constituent de nouveaux moyens de diffusion massive
de l'information. Si ceux-ci suscitent souvent l'émoi chez de nombreux
camarades de la CNT, force est de constater que de nombreuses structures
syndicales de la CNT disposent notamment de pages Facebook : CNT
Lille, CNT 31, UD Moselle, CNT Gironde, Syndicat SUB RP, Fédération du
Travail et de l'emploi...

Il existe en outre sur Facebook une page non officielle intitulée « CNT-f »
et animée par des individus adhérents de la CNT, suivie par de nombreuses
personnes.

Comble de cette situation, de nombreux adhérents de notre syndicat sont
plus informés sur l'activité des syndicats CNT dans d'autres régions
en consultant Facebook qu'en lisant la liste-syndicats !


Il est difficilement contestable que ces réseaux sociaux donneraient une
visibilité phénoménale à la CNT : pour exemple la page Facebook de
l'UD CNT Moselle est régulièrement suivie par 2500 personnes.
Chiffre qui dépasse largement celui du nombre de tracts lus lors d'une
diffusion papier, qui dépasse largement celui des inscrits sur nos
listes mails de diffusion publique, mais également le chiffre des
sympathisants que nous croisons physiquement de manière régulière.

Au-delà des abonnés à cette page Facebook, la fréquentation de celle-ci
est sans aucune mesure avec celle de notre site internet départemental.

Finalement, l'organisation même des contacts dans le cadre des « réseaux sociaux »
implique qu'une telle page ne touche pas que les 2500 abonnés qui la
suivent ou la consultent, mais bien au-delà celle-ci, celles et ceux
qui sont en relation avec ces 2500 abonnés.
Si nous reprenons la comparaison avec un tract papier, il y a tout de
même peu de chance qu'un individu ayant apprécié notre tract le duplique
pour le diffuser à son tour. Sur un réseau social c'est le cas : et même
lorsque l'individu n'adhère pas totalement à notre propos.

Ainsi, nous pensons avant tout qu’utiliser les réseaux sociaux
permettrait à la CNT de jouir d'une visibilité qui lui fait cruellement
défaut dans la société actuelle.

Il faut donc dans un second temps déconstruire les argumentaires
régulièrement rencontrés contre l'utilisation des réseaux sociaux par
nos structures syndicales

L'argument sécuritaire
-----------------------

L'argument sécuritaire, défend bien souvent l'idée que l'utilisation de
réseaux sociaux poserait des problèmes de sécurité, et de confidentialité
militante. A nos yeux, cet argument ne tient pas, dès lors que ce que
nous publions sur les réseaux sociaux relève strictement d'une
documentation publique et non de données internes à la CNT.

En effet, nos tracts, communiqués et autre articles ont de toute façon
vocation à être massivement diffusés et consultés, se retrouvant ainsi
de fait librement accessible pour nos détracteurs et ennemis de classe.

Bien sûr Facebook et Twitter, alliés du Capital traitent avec les
autorités de l'État, que cela soit volontaire ou non, et cette situation
est incontestablement dangereuse en terme de respect de la privé et de
situations individuelles. Mais nous rappelons que la diffusion publique
des écrits de la CNT ne peut être assimilée à des éléments d'une vie
personnelle : il s’agit bien de propos et d'informations qui ont pour
essence même le fait d'être accessibles publiquement.


L'argument éthique
------------------

L'argument éthique, défend quant à lui l'idée que Facebook et Twitter
sont des entreprises privées, capitalistes et dont le but n'est pas de
diffuser les idées défendues par la CNT mais bien de faire du profit.

C'est vrai : tout comme c'est le cas des médias à qui nous envoyons des
communiqués de presse, des opérateurs téléphoniques via lesquels nous
diffusions des rendez vous militants, comme les imprimeurs ou les
fournisseurs de matériel d'imprimerie, les fabricant d’ordinateurs, les
fournisseurs d'accès à internet, les hébergeurs de serveurs de messagerie
électronique, que nous sollicitions pourtant quotidiennement pour
diffuser de l'information militante.

Sur ce point, le débat dépasse donc largement la simple question des
réseaux sociaux et pose la question de si une organisation syndicale
doit ou non utiliser des services du capital pour faire de l'information.
À cette question, et au-delà de l'usage réel qui est déjà celui de
nombreuses structures qui a donc quelque part, déjà tranché cette
question dans les faits, nous répondrons qu'éviter ces outils à tout prix
constitueraient aujourd'hui un véritable repli sur nous-même.

Convaincus par l'anarcho-syndicalisme et sa pratique, convaincus par
l’organisation qu'est la CNT, nous avons la prétention de la développer,
et il nous semble que cantonner notre audience à des réseaux alternatifs
et intimistes ne permet bien souvent que de toucher des initiés et non
pas un public qui, parce qu'il n'en a jamais eu l'opportunité, ne
connaît pas l'existence même de la CNT.

En conclusion, loin de nous l'idée de faire l'apologie des réseaux
sociaux par essence. Néanmoins, compte tenu du manque de visibilité
publique dont souffre la CNT, et considérant que les désagréments causés
par l'utilisation des réseaux sociaux par une organisation syndicale sont
dérisoires par rapport au bénéfice qu'elle susciterait, nous estimons
nécessaire d'un point de vue pragmatique que d'utiliser ces outils.
Bien sûr, cela devra s'accompagner de gardes fous explicites
en ce qui concerne le contrôle par les syndicats des publications
sur ces plates-formes.


Motion
=======

La CNT se dote de pages officielles sur les réseaux sociaux Facebook et
Twitter.

Ces pages sont réalisées et alimentées par un.e mandaté.e réseaux
sociaux qui intègre l'équipe du pôle médias de la Confédération.

Ne sont diffusées sur des plates-formes que les documents publics émis
par la CNT ou ses structures adhérentes (communiqués de presse, tracts,
visuels, articles, vidéos, CS...) et ne constituent en aucun cas des
outils de communication interne.

Les commentaires publics des usagers y sont bloqués ou à défaut
strictement modérés par le mandaté confédéral afin d'éviter que
ces pages ne soient utilisés pour véhiculer une propagande qui
ne serait pas celle de la CNT.

Finalement, toute publication de la CNT via ces réseaux sociaux
intégrera un renvoi vers le site internet confédéral.


Contre motions
==============


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°5 2016

       :ref:`Santé social CT Lorraine <sest_lorraine>`
     - 24
     - 13
     - 7
     - 5
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
