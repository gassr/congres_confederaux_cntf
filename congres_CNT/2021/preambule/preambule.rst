
===========
Préambule
===========

Mode d'emploi du présent cahier de la.du mandaté·e.

Vous trouverez dans ce cahier une aide pour reporter le vote de votre
syndicat quant à chaque motion, contre-motion et amendement.

**Le cadre suivant est placé à la suite de chaque texte mis au vote :**

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°X                  |            |          |            |                           |
| Nom du syndicat             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


**La première colonne, intitulée "Nom" donne la nature du texte à voter
(motion, amendement, contre-motion)** et son numéro à l'intérieur de ce
cahier (pour les motions, ou pour les amendements d’une motion, quand
il y en a plusieurs du même syndicat), suivi du nom du syndicat qui
présente le texte.
Les colonnes suivantes donnent les différents choix de vote possible pour
le syndicat.
Il ne reste plus qu'à mettre une petite croix, un chat hérissé ou quelque
autre signe distinctif dans la case correspondant au vote du syndicat
par lequel vous êtes mandaté.e.
**La dernière ligne vous permet de noter les votes du congrès et la décision
finale**. Donc conservez ce cahier, vous pourrez faire un compte-rendu
clair et précis de retour dans vos assemblées générales de syndicat.

En fin de cahier, vous retrouverez de l'espace afin de faire des prises
de notes ainsi que des feuillets à remettre au moment de votre départ.

Ces feuillets récapitulent les votes de votre syndicat et permettront
une confirmation des votes.

Nous vous rappelons qu'un amendement constitue une proposition de
modification du texte initialement proposé par une motion.
Le congrès confédéral ne procédant à l'examen des propositions d'amendement
que lorsque la motion initiale a été adoptée par le congrès.

Une contre-motion constitue, quant à elle, une proposition qui entre en
contradiction avec le sens de la motion initiale.

Les contre-motions ne sont examinées par le Congrès Confédéral que lorsque
la motion initiale n'a pas été adoptée.

**Le congrès confédéral reste souverain, tout dans ce cahier n'est qu'une
proposition.**

Nous vous souhaitons un bon congrès,
La commission de préparation du XXXVe Congrès confédéral 2021
