
.. _motion_20_2012:

=================================================================================================================================================================================
Motion n°20 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Utilisation des droits horaires syndicaux pour l’activité militante, ETPIC30)
=================================================================================================================================================================================
  
.. seealso::

   - :ref:`etpic_30_pub`



Motion
======

Les syndiqué.e.s de la CNT, les élu.e.s CNT (délégué.e.s du personnel,
représentant.e.s au Comité d’entreprise, élu.e.s aux Commissions paritaires de
la fonction publique), les délégué.e.s syndicaux CNT peuvent se saisir librement
des heures de délégation et autorisations spéciales d’absences mises à leur
disposition pour l’activité syndicale (au sens le large) et de représentation
du personnel.

Le recours aux décharges syndicales ou de service, lorsqu’elles sont accessibles,
est limité aux seules fins d’activité syndicale de terrain et/ou de propagande.
Leur utilisation ne peut donc être associée à la tenue des mandats propre à
l’organisation interne de la CNT.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°20          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
