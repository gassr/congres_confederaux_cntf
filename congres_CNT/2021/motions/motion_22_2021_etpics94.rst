.. index::
   pair: Motion 22 ; 2021

.. _motion_22_2021_etpics94:

================================================================================================================================================
Motion **n°22** 2021: **Drogues, Alcool et émancipation révolutionnaire** par **ETPICS 94**
================================================================================================================================================

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`


Argumentaire
=============

D’autre part, nous proposons presque à chaque fois de l'alcool au public venant
à nos événements, et nous pouvons même dire que c’est de là que proviennent
principalement nos recettes.
Ceci doit nous interroger, ne contribuons-nous pas au maintien de notre classe
opprimée dans cette situation ?

Ceci pose également la question de notre éthique collective et révolutionnaire.
Nous qui autogérons la CNT et qui voulons auto-gérer nos entreprises,
nos quartiers,...
Néanmoins, l’expérience de la prohibition a démontré l’inutilité d’une
interdiction totale.

L’interdiction s’est révélée être une illusion autoritaire, inefficace et
contraire à nos principes fondamentaux d’émancipation. C’est en conscience
que doit venir le recul de l’alcool et des autres drogues.
C’est pourquoi nous proposons la motion **16-B**.

Les ravages de l’alcool et de l’alcoolisme font des dizaines de milliers de
morts chaque année en France, des millions dans le monde.

Pourtant, l’idéologie dominante continue à valoriser la consommation d’alcool.

Les défenseurs, parlent de ``tradition``, de ``culture``.
Nous sommes convaincus que c’est par **intérêts économiques**.

Motion
=======

La lutte contre cette *culture* de l’alcool doit faire partie de nos réflexions
contre toutes les aliénations, et pour l’émancipation des travailleurs et
travailleuses.
Parce que nous sommes contre tout ce qui affaiblit la force et l’élan
révolutionnaire des êtres humains.

C’est aussi contribuer à faire reculer la violence dans la société,
**entre autres les violences sexistes**.

La confédération se dote de tous les moyens pédagogiques et émancipateurs
pour informer le plus largement possible sur ce sujet (brochures, tracts,
affiches, conférences...)

Amendements
===========

- :ref:`amendements_motion_22_2021_interpro31`
- :ref:`amendement_motion_22_2021_sest_lorraine`
- :ref:`amendement_motion_22_2021_stp67`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°22 2021 STP67      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
