
.. index::
   pair: Motion 9 ; 2014

.. _motion_9_2014_sinr44:

===========================================================
Motion n°9 2014 : Rapprochement avec la CNT-AIT ( SINR 44)
===========================================================






Argumentaire
============

Il apparaît que depuis quelques années un travail commun est possible
avec la CNT-AIT (tendance « Le Mans-Pau », donc pas « SIA »), avec des
cortèges communs le 1er mai au Mans et à Paris notamment.

De plus il semblerait que depuis peu la CNT-AIT adapte sa stratégie
syndicale à la loi de 2008 (désignation de RSS).

Enfin il semblerait que la scission de 2012 et le départ de la CNT de
certains militants desserrent certains noeuds de conflits ancrés entre
nos organisations depuis des années.
Le temps est peut être venu de reprendre le dialogue avec cette
organisation proche de la nôtre, et pourquoi pas à terme aller vers une
union des forces anarcho-syndicalistes.

Motion
======

La CNT mandate son secrétaire confédéral pour inviter le secrétariat
confédéral de la CNT-AIT à une rencontre, sur les bases suivantes :

- échange sur l'implantation syndicale géographique et professionnelle
- échange sur les stratégies syndicales employées
- réflexion sur de possibles actions communes


Amendements
===========

.. seealso::

   - :ref:`amendement_motion_9_2014_interco25`
   - :ref:`amendement_motion_9_2014_sse31`



Contre motion
==============

.. seealso::

   - :ref:`contre_motion_9_2014_etpics57`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°9 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
