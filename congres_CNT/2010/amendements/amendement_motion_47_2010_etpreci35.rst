
.. _amendement_motion_47_2010_ETPRECI35:


=====================================
Amendement à la motion 47 (ETPRECI35)
=====================================

Chaque adhérent de la CNT est abonné au Combat syndicaliste à prix coûtant,
en plus de la cotisation.


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°47 |            |          |            |                           |
| ETPRECI35                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_47_2010`
