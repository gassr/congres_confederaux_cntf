
.. _contre_motion_23_2014_ess34:

=======================================================================================================
Contre motion à la motion n°23 : Evolution du tarif et du format du Combat syndicaliste (ETPIC 30 sud)
=======================================================================================================


.. seealso::

   - :ref:`motion_23_2014_etpic30`




Argumentaire
=============

Nous pensons que, malgré le travail fourni par les différentes équipes de
comité de rédaction, il persiste plusieurs problèmes vis à vis du combat
syndicaliste.

Problèmes qui sont liés aux exigences de délais dus à la formule mensuelle,
aux décalages temporels entre l'actualité, la rédaction, la parution et la
diffusion.

Problème du à la vocation de notre presse : information interne ou outil de
propagande externe.

Problèmes de contenu : information, compte-rendu d'action, analyse et/ou
diffusion des prises de position confédérales.

Nous pensons aussi que si il peut être intéressant de diffuser, via un site
internet une partie du contenu du CS, cela demande réflexion, afin d'éviter
de rendre la formule papier obsolète.

Pour toutes ces raisons nous souhaitons engager un vaste travail de réflexion
de la confédération sur son organe de presse.
Nous proposons donc la création d'une commission, mais ce travail ne saurait
être uniquement le fruit de la réflexion de la commission et du comité de
rédaction, il doit bien être l’œuvre de l'ensemble des syndicats de la CNT.

Motion
========

Un travail de réflexion et de proposition est entamé dans la confédération
sur les buts, la forme et la diffusion du Combat Syndicaliste.

Ce travail est centralisé et synthétisé par une commission créé à cet effet.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°23 2014 <motion_23_2014_etpic30>`

       :ref:`ESS34 <ess34>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
