
.. _amendement_motion_56_2010_ste34:

=============================================
Amendement à la motion N°56 (STE 34) [rejeté]
=============================================


Ajout à la 5ème ligne du :ref:`paragraphe <motion_56_2010_revendications>`::

    «articuler des revendications immédiates avec des revendications de rupture »

« [...], le communisme libertaire, sans déléguer cette tâche aux partis politiques »


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 56       | 24         | 21       | 13         | 13                        |
| STE34 34                           |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+



**Amendement rejeté**



.. seealso:: :ref:`motion_56_2010`
