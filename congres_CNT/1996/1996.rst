
.. index::
   pair: Congrès ; 1996 (Lyon, XXIVe)
   pair: Lyon ; XXIVe congrès 1, 2, et 3 Novembre 1996
   ! Congrès (1996, Lyon)

.. _congres_CNT_1996_lyon:

===============================================================
Le **XXVe Congrès CNT 1996 de Lyon** du 1 au 3 novembre 1996
===============================================================




.. _syndicats_presents_1996_lyon:

Nombre de syndicats présents à Lyon en 1996: 40
===============================================

40 syndicats présents à jour de cotisations.


Motions adoptées
================

.. toctree::
   :maxdepth: 2


   motions_adoptees/index


.. seealso::

   - :ref:`congres_CNT_1998_paris`
   - :ref:`congres_CNT_1993_paris`
