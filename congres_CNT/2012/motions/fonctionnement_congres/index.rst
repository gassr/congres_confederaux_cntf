
.. index::
   pair: Motions; Fonctionnement 2012

.. _motions_fonctionnement_congres_2012:

Motions Fonctionnement congres 2012
===================================

.. toctree::
   :maxdepth: 4

   motion_1_2012_ste75
   motion_2_2012_sanso69
   motion_3_2012_ste72
   motion_4_2012_stel
   motion_5_2012_stea
   motion_6_2012_ste72
   motion_7_2012_ste72
   motion_8_2012_ste72
