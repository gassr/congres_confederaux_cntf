

.. _contre_motion_57_2010_stp72:

==================================================================
contre Motion n° 57 : Abandon de la plate-forme confédérale, STP72
==================================================================

Contre-motion présentée par le STP72  Syndicat des travailleurs et travailleuses
précaires de la Sarthe  complément à l'envoi du 31-10-2010


Argumentaire
------------

Le document peut avoir une utilité mais pas sous cette forme et pas en se
présentant comme une plate-forme revendicative, outil trop restrictif et
source de malentendus sur le caractère réformiste de certaines propositions
sorties de leur contexte de lutte.


Amendement / Contre-proposition
-------------------------------

Le document intitulé «Plate-forme revendicative de la CNT» peut avoir
comme rôle d'inventorier et de classer, selon des modalités qu'il reste
à définir et qui pourraient distinguer les revendications immédiates des
revendications révolutionnaires, toutes les revendications portées par les
syndicats et fédérations de la CNT. Ce document devrait donc être mis à jour
régulièrement afin de pouvoir constituer un outil alimentant  les réflexions
en cours des syndicats sur leurs propres revendications.

Le document doit donc être renommé en «Inventaire des revendications de la CNT».
Il s'agit d'un rôle confédéral à ajouter aux prérogatives de l'un des mandats existants.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 57       |            |          |            |                           |
| STP 72                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_57_2010`
