
.. _motion_28_2012:
.. _motion_28_2012_ptt95:

===========================================================================================================================
Motion n°28 2012 : Pour une mobilisation confédérale autour de la défense du 33 rue des Vignoles à Paris (PTT 95)
===========================================================================================================================
  
.. seealso::

   - :ref:`ptt_95_pub`
   - :ref:`motions_ptt95_2012`



Le 33 rue des Vignoles
======================

- :ref:`33_rue_des_vignoles`

Argumentaire
============

Depuis plusieurs mois l’Union régionale parisienne est mobilisée pour un double
objectif : conserver le 33 rue des Vignoles comme local régional, objectif qui
implique dans l’état actuel du rapport de force, une rénovation de l’espace
avec une perspective d’augmentation de la superficie.

Dans ce sens des négociations sont engagées avec la ville de Paris qui, si le
projet aboutit, aura comme conséquence le recours à un emprunt bancaire qui
évoluera selon les tranches de travaux. Aussi notre campagne vise à :

- Disposer de fonds.
- Assurer une rentrée mensuelle stabilisée de 3.500,00 euros, somme qui sera à la
  base des remboursements.

En arrière plan de tout cela existe la possibilité d’une tentative d’expulsion
par la ville de Paris, tentative qu’en 1996 nous avons fait échouer par la
mobilisation, ou, autre forme de liquidation possible, la remise des terrains
à une société immobilière qui nous fixera un loyer prohibitif que nous ne
pourrons payer, le résultat étant le même. Et la place serait libre pour les
spéculateurs.

Un combat ancien
-----------------

Après les tentatives d’expulsion en 1996 nous avons mis en place une
caisse de résistance alimentée par des dons mensuels de syndicats mais surtout
d’individus, ce qui nous permet aujourd’hui d’avoir un fonds disponible mais
qui est grandement insuffisant pour satisfaire au coût global des travaux.
Mais cela permet de se lancer dans la bataille avec une petite assise.
Un combat réactualisé : Depuis avril l’Union régionale a lancé cette campagne pour
dynamiser un élan de solidarité autour du 33. Dans ce combat la région parisienne
compte en priorité sur ses propres forces mais entend également élargir la
campagne sur le plan national et international. En efet le 33 n’est pas
seulement un lieu réservé à la région parisienne :

- C’est un espace historique de la lutte anarcho-syndicaliste internationale
  que les révolutionnaires espagnols en exil ont aménagé.
- C’est un carrefour internationaliste où les pas de dissidents de l’Est, de
  réfugiés italiens après les années de plomb, d’exilés sud- américains se
  sont croisés.
- C’est un espace de constructions de pratiques internationalistes comme en Mai
  2000 ou en 2007.
- C’est un lieu de débats, d’échanges ouvert à diférentes sensibilités.
- C’est un espace de cultures alternatives où s’expriment des mouvements, groupes
  qui mènent également un combat anti-capitaliste.
- Enfin c’est un lieu de prise de contacts avec la CNT, en particulier
  étrangers, en effet de nombreux militants de passage à Paris viennent nous y
  rencontrer, prendre des livres, échanger.
- C’est aussi le siège de la Confédération et de Fédérations.

Un espace autogéré
------------------

À l’heure de l’intégration de plus en plus grande du syndicalisme le 33 demeure
un lieu autogéré, fonctionnant dans l’esprit des premières Bourses du Travail.

Une dynamique autour du 33 est en mouvement comme en atteste le suivi de notre
mobilisation, chiffres donnés ici à la date du 24 juillet 2012 :


==================================  ==================
A                                   B
==================================  ==================
Virements mensuels issus de la RP   867,13 euros
Virements mensuels hors-RP          145,00 euros
Total                               1 012,13 euros
Dons issus de la RP                 15 614,00 euros
Dons hors RP                        1 580,00 euros
Total                               17 194,00 euros
==================================  ==================

Ce sont des premiers résultats encourageants que nous devons accentuer car
notre objectif de prélèvements mensuels est de 3 500,00 euros afin d’assurer
notre  capacité de remboursement sans amoindrir notre capacité à réaliser
notre  propagande et nos interventions dans les luttes. Et sachant qu’au fur
et à  mesure de la rénovation des services proposés pourront pérenniser notre
financement : service-librairie, coopératives, location de salles...

C’est pourquoi, pour les raisons invoqués ici nous présentons cette motion au
Congrès de Metz.

Motion
=======

Le Congrès confédéral de la CNT réuni à Metz décide de soutenir la campagne
pour la défense et la rénovation du 33 rue des Vignoles ceci sur le plan
confédéral et en assurant une popularisation de cette campagne à tous les niveaux.

Le Congrès de Metz affirme que le combat pour le maintien à Paris d’un espace
autonome, autogéré, internationaliste est une bataille qui concerne toute la
confédération et appelle chaque structure à se saisir de cet axe de lutte qui
met aux prises la CNT et le monde des spéculateurs.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°28          |            |          |            |                           |
| PTT95                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
