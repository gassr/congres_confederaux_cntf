
.. _motions_presse_2004:

=====================================================================================================
Motions Presse confédérale de la CNT  28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=====================================================================================================



Papier recyclé
==============

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 38         | 15       | 10         | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
=====

Les publications de la CNT (:term:`CS`, :term:`Temps Maudits`,...) seront dorénavant
(dans un délai de 6 mois pour permettre de trouver un fournisseur au meilleur
prix) éditées sur du papier recyclé blanchi sans chlore et pour éviter tout
gâchis, il sera recommandé pour les documents de la CNT (BI, tracts, etc.)
d'effectuer les impressions ou les photocopies en mode RECTO/VERSO.

Une commission sera créée pour suivre et mettre en application ces résolutions
(recherche de fournisseurs, etc... ).


Abonnements au Combat Syndicaliste
==================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 26         | 22       | 7          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

1. **Abonnements personnels**
   Pour optimiser la diffusion du Combat syndicaliste, les syndicats (liste de
   syndicats) proposent que tous les adhérents de la Confédération soient par
   principe abonnés personnellement au Combat syndicaliste. Lors de l'adhésion,
   chaque syndicat propose un abonnement au Combat syndicaliste.
   Le nombre d'abonnés au CS et le nombre d'abonnements subventionnés seraient
   des données que les syndicats pourraient fournir dans leurs comptes-rendus
   d'activités, lors des Congrès et CCN.
2. **Abonnement des syndicats**
   Chaque syndicat en tant que tel s'abonne obligatoirement aux revues
   confédérales : Combat syndicaliste et Temps maudits.



Amélioration du Combat Syndicaliste
===================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 51         | 0        | 9          | 7                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le Combat syndicaliste est souvent le premier contact qu'un travailleur peut
avoir avec la C.N.T., il est son image publique la plus visible, aussi le
Congrès charge la Confédération de donner aux camarades mandatés pour le
réaliser, les moyens matériels et financiers pour qu'il ait une présentation,
maquette et impression, dignes de la C.N.T.


Création d’une commission d’amélioration du Combat Syndicaliste
===============================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 41         | 6        | 14         | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le Congrès confédéral créé une commission visant à:

- proposer une nouvelle maquette, avec un devis financier à l'appui ;
- structurer le CS, à titre d'exemple on pourrait penser à un cadre fixe pour
  les articles :

      * 4 pages d'analyses "politiques"
      * 4 pages de  luttes/grèves/brèves syndicales
      * 2 pages internationales
      * 2 pages "culturelles" (livres, articles historiques...) ;

- réfléchir pour inciter les syndicats et les camarades à s'investir (écrire)
  et à faire cet effort collectivement (campagne à l'intérieur de la
  Confédération ?) ;
- profiter de l'expérience de camarades du métier : correcteurs, maquettistes
  ou encore journalistes.

Commission
++++++++++

.. hlist::

   - PTT75
   - SIPM RP
   - Educ 93
   - Comm RP
   - Educ 75
   - Educ 13
   - Interpro 13
   - Interpro 31
   - Interco 93 sud
   - SUB TP 35

Coordination: PTT 75


Parution mensuelle du Combat Syndicaliste
=========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 33         | 24       | 9          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le C.S passe de nouveau à une parution mensuelle avec une pagination renforcée.

Une équipe de rédaction est formée pour s'occuper de la rubrique dossier
(juridique, information sur telle ou telle action, analyse, projet de loi ...)

La couverture est améliorée (soit en papier glacé ou autre) pour avoir une
présentation d'une revue mensuelle de qualité.

Etude d'une distribution nationale et proposition pour le prochain CCN.



Brochures " Orientation et fonctionnement de la CNT " et "la CNT ???"
=====================================================================

But
---

Intégration des nouveaux/elles adhérent(e)s

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 58         | 3        | 2          | 7                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Par rapport à l’intégration et la formation des nouveaux/velles adhérent(e)s
de la CNT, chaque nouvel(le) adhérent(e) reçoit gratuitement, en même temps
que sa première carte syndicale, les brochures " Orientation et Fonctionnement
de la CNT" et "La CNT??? ".

Cette proposition ne résoudra pas à elle seule le problème de la formation mais
elle en est un apport important.

Actualisation de la brochure **Orientation et Fonctionnement de la CNT** sur
la partie internationale puisque nous ne sommes plus à l’AIT.


.. seealso:: :ref:`motions_presse_confederale_cnt`
