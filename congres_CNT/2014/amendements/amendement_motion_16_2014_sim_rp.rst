


.. _amendement_motion_16_2014_sim_rp:

===========================================
Amendement à la motion n°16 2014 (SIM RP)
===========================================

.. seealso::

   - :ref:`motion_16_2014_ess34`





Courriel
=========

::

    Sujet :     [Liste-syndicats] Amendement Motion N°16
    Date :  Tue, 4 Nov 2014 17:11:22 +0100
    De :    sim.rp@cnt-f.org
    Pour :  Confédération <liste-syndicats@bc.cnt-fr.org>

Bonjour,

Veuillez trouver ci après notre proposition d'amendement concernant la
:ref:`Motion N°16 <motion_16_2014_ess34>`.

Argumentaire 
=============

Nous sommes d'accord avec l'orientation générale de cette  Motion,
néanmoins nous souhaitons apporter un amendement.

Il est dit dans cette motion que la CNT est appelée à organiser des
campagnes d'abstention lorsqu'il y'a des élections politiques. Pourquoi
pas, mais cela ne nous paraît pas essentiel.
Ce qui nous paraît essentiel au quotidien, tout au long de l'année, c'est
que la CNT ait une neutralité complète à l'égard de la politique et des
politiciens. Et lorsque nous demandons aux institutions politiques
l'amnistie par exemple, il nous semble qu'on s'eloigne de cette neutralité
et aussi de l'action directe. Nous pensons qu'il faut nous adresser
directement à ceux qui nous posent les problèmes. Si c'est la SNCF, la
SNCF. Si c'est un patron, c'est pas le député du coin etc...

Du coup la motion N° 16, nous l'a proposons de la façon suivante:

La CNT conformément à son projet de société ne présente aucun candidat en
son nom propre à des élections politiques, aucun adhèrent ne peut se
prévaloir de l'étiquette CNT pour s'y présenter.

La CNT réaffirme son attachement au Communisme Libertaire et aux voies pour
y parvenir, en particulier l'action directe dont l'objet est que les
travailleurs s'emparent de leurs problématiques sans les soumettre à la
politique et aux politiciens.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°16 2014 <motion_16_2014_ess34>`

       :ref:`SIM RP <simrp>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
