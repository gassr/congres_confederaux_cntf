
.. _motion_10_2012:

===================================================================================
Motion n°10 2012 : La cohérence de notre combat autogestionnaire (PTT 95)
===================================================================================
  
.. seealso::

   - :ref:`ptt_95_pub`
   - :ref:`motions_ptt95_2012`



Argumentaire
=============

La construction d’un outil syndical autogestionnaire est un combat qui n’a rien
de linéaire, il est marqué par des avancées, des reculs, qui, à des degrés
divers, témoignent des rapports de force entre les classes, de la progression
ou de la régression de la conscience de classe.

Lors des périodes d’ofensives sociales notre syndicat connait des embellies,
en situation de reflux du front social nous pouvons enregistrer des revers qui
alimentent parfois des débats ou pratiques portant sur la nature de
l’organisation à construire.

Cela est le propre de la CNT comme de toute forme d’organisation collective,
aujourd’hui comme hier. Aussi nous avons connu, nous connaissons, des débats
contradictoires, des crises dont la résolution, selon les orientations choisies, conditionne
soit le maintien du Syndicat sur une perspective ayant, malgré les adaptations
nécessaires, une continuité historique, soit une rupture avec son histoire,
ses objectifs.

Ainsi en 1993 la CNT a connu ce qu’un texte public publié dans
Le Combat Syndicaliste a appelé « Une crise de croissance » et dont l’enjeu
était simplement de savoir si la CNT était une organisation affinitaire
territoriale ou une Confédération syndicaliste s’implantant sur le terrain
social dans et hors des entreprises.

Près de 20 ans après les faits la pratique a tranché.

Aujourd’hui, depuis quelques années, la CNT connait des contradictions qui
freinent son développement car donnant de notre organisation une image floue,
contrastée, qui met en cause la cohérence de ses orientations et nuit à la
lisibilité de ses propos et de ses pratiques. Cette contradiction s’est
focalisée autour de la démarche du syndicat du Nettoyage de la RP qui, au
nom des nécessités du développement d’un syndicalisme de masse, a instauré
en son sein, un fonctionnement basé sur la création d’un poste de
permanent/salarié, et ceci de manière secrète durant plusieurs années.

Cette démarche parallèle, non issue d’un débat collectif, est à l’origine
d’une confrontation interne puis désormais publique, qui se cristallise en
particulier en RP même si l’activité de cette région ne saurait se réduire
à cet afrontement. La présentation très schématique de cette opposition
se résume généralement à une approche réductrice : la légitimation du
recours à un permanent salarié par les impératifs du développement d’un
syndicalisme de masse et en face des syndicats prônant une démarche sectaire,
indiférents au renforcement de la CNT, adeptes de l’antifascisme primaire
peu préoccupés du syndicalisme d’entreprises, groupusculaires, bref des
syndicats victimes d’une « maladie infantile » du syndicalisme révolutionnaire
et de l’anarcho-syndicalisme.

Sans préjuger de l’évolution de la gestion de cette situation par la
Confédération – même si à la date de la rédaction de cette motion le
syndicat du Nettoyage n’a plus versé de cotisations à la Région depuis
près de 2 ans, et a annoncé la « suspension de son adhésion à la
Confédération » - nous avons choisi de proposer à ce Congrès une motion
d’orientation générale qui tente de synthétiser le sens de notre combat et
donc du type d’organisation syndicale que nous entendons construire,
conception au-delà de laquelle il n’existe que la voie du syndicalisme
institutionnel ou celle de la spontanéité révolutionnaire.


Motion
=======

La CNT est une organisation de classe en rupture avec le capitalisme,
alternative au syndicalisme institutionnel qui est le relais étatique et
patronal au sein du monde du travail. De cette approche il découle que la
CNT est un adversaire de classe du patronat et de l’État, au service des
luttes sociales, et non un partenaire loyal, interlocuteur
responsable chargé de l’accompagnement des mutations du capitalisme.

La CNT s’oppose à la cogestion et à la collaboration de classe que l’on
peut définir comme la reconnaissance d’un « intérêt général » au nom duquel
le triptyque Patronat-État-Syndicat se regroupe au sein d’organismes
inter-classistes pour trouver les meilleurs moyens, réformes, susceptibles
de rendre l’exploitation et l’oppression plus supportables.

La CNT, organisation syndicaliste pragmatique, intègre les évolutions en
matière de droit syndical à partir de ses analyses de classe et lorsqu’elle
participe à des réunions face aux représentants patronaux c’est toujours en
conservant comme centre de gravité, comme lieu décisionnel, ou le syndicat
ou l’assemblée générale et nullement le délégué détenteur
de pouvoirs décisionnaires.

Si la CNT, organe de lutte s’implantant dans les entreprises, adapte son
intervention aux conditions de son époque elle n’entend pas diluer sa pratique
dans l’esprit co-gestionnaire des législateurs.

La CNT est porteuse d’un projet de transformation révolutionnaire de la
société fondé sur la capacité des exploités-ées et des opprimés-ées à
autogérer la production et la distribution des biens et des services en
s’appuyant sur la logistique syndicale et en se dotant de ses propres organes
de pouvoir issus de l’auto-organisation des travailleurs-ses.

Ce projet ne s’arrête pas aux frontières de l’hexagone mais s’inscrit dans
une problématique internationaliste. Cette perspective met le mouvement
social au centre de la lutte collective, refusant les médiations politiques
qui prétendent représenter les exploités et conduire leur destinée.

De cette conception globale du combat à mener, fruit de l’expérience
historique et international du mouvement ouvrier et non lubie idéaliste,
qui détermine la finalité de notre organisation, découle une approche des
tâches du syndicat sur le terrain social et un point de vue sur l’organisation
syndicale à développer, outil qui se doit d’être en phase avec les moyens et
le but de notre action.

La CNT dans le mouvement social
===============================

Dans l’entreprise comme dans les quartiers notre Confédération assure une
présence dépassant les pratiques catégorielles – élément caractéristique du
réformisme – pour unifier les luttes interprofessionnelles.

Le syndicat met en avant les formes d’action directe où ceux et celles qui
luttent décident, contrôlent les modalités de l’action sans déléguer à des
« spécialistes » ou « experts » la gestion du mouvement.

La CNT milite pour un syndicalisme de masse, de classe et autogéré, et elle
n’est nullement indiférente à son impact sur le terrain social. Elle est
attentive au développement de son réseau organisationnel ayant le souci de
participer comme élément moteur aux confrontations de classe dans et hors
l’entreprise.

Dans ce but notre organisation agit pour le renforcement de ses syndicats,
pour la création de nouvelles structures, pour l’élargissement de son
audience.

Cependant notre Confédération n’entend nullement sacrifier ses orientations,
son mode de fonctionnement, à la réalisation de ces objectifs, même si cela
implique une construction plus lente. Le respect de nos principes et pratiques
sont le gage de construction d’un syndicat qui pourra inscrire son activité
dans la durée, assurant un renouvellement générationnel, et survivant aux
individualités.

Notre audience de masse résultera de pratiques organisationnelles et de
l’essor du mouvement social. Notre souci est d’associer développement et
pratiques autogérées. À ce jour si l’on prend comme critère d’efficacité
syndicale le nombre d’adhérents ce sont les syndicats institutionnelles
managés comme des PME qu’il faudrait rejoindre.

En terme de formes de lutte, et cela résulte des analyses de la société,
de la nature du syndicalisme institutionnel, mais aussi de notre projet de
transformation sociale autogestionnaire, la CNT soutient l’Assemblée générale
comme lieu de décisions, tout en conservant son droit d’expression
indépendant, et toutes les formes d’organisation qui incarnent l’autonomie
ouvrière : comités de grève, coordinations élues et révocables,
comités de soutien, auto-organisation.

Ce choix d’organisation ne relève nullement d’un radicalisme verbal mais
s’inscrit en perspective de notre projet de rupture avec le capitalisme.

La lutte collective est également une école de la démocratie directe.La CNT considère que ses tâches sur le terrain social ne se limite pas à la
préparation de grèves mais impliquent une action pédagogique, d’éducation,
visant à former les militants-es sur une base de classe pour leur permettre
de prendre en mains leur destinée et de devenir des acteurs conscients et
autonomes des combats à mener comme dans la gestion de l’outil syndical.

Enfin la CNT est à l’écoute de toutes les formes d’opposition et de résistance
au système, en particulier sur le plan culturel, qui sont des moments, des
lieux d’alternative au formatage culturel du pouvoir, de solidarité

La CNT construit un outil syndical autogéré
============================================

Le syndicat CNT n’est pas une fin en soi, c’est un moyen adapté pour une
double finalité :

- luttes immédiates et préparation d’une rupture avec cette société. En cela
  l’organisation syndicale que nous voulons doit tendre à représenter, dans
  sa gestion, son fonctionnement, le plus possible les valeurs, pratiques
  de son combat. La fin ne justifie pas tous les moyens sous peine de
  dérapage, l’expérience historique du mouvement ouvrier en témoigne.

Pour Fernand Pelloutier les Bourses du Travail devaient anticiper en leur
sein les valeurs de l’autonomie ouvrière, ce qu’il exprime en 1899 dans
sa « Lettre aux Anarchistes » quand il écrit « ... nous devons, non seulement
prêcher aux quatre coins de l’horizon le gouvernement de soi par soi-même,
mais encore prouver expérimentalement à la foule ouvrière, au sein de ses
propres institutions, qu’un tel gouvernement est possible, ... ».

Ainsi il est cohérent qu’une structure syndicale qui plaide pour
l’auto-organisation des luttes, pour la gestion directe de la société par
les producteurs, soit administrée selon des pratiques autogestionnaires.

Comme il est cohérent que les syndicats institutionnels qui n’ont aucune
volonté de transformer radicalement la société, laissant le soin aux partis
politiques de gérer la cité, adopte un mode de fonctionnement fondé sur la
délégation, la spécialisation, la professionnalisation du syndicalisme.

La CNT dans son fonctionnement interne se doit donc d’être en cohérence avec
ses orientations : autogestion du syndicat, libre expression des points de
vue dans le débat, respect des décisions collectives, loyauté vis-à-vis de
l’organisation.

Réunis en Congrès à Metz les syndicats de la CNT réaffirment la cohérence de
leurs orientations, de leurs pratiques, de leurs valeurs qui forment un
socle, celui du syndicalisme autogestionnaire.

Si chaque point n’est pas un dogme et peut être remis en cause cela ne
saurait être fait indépendamment de la finalité de notre combat.
Faute de quoi c’est l’identité anarcho-syndicaliste et syndicaliste
révolutionnaire de la Confédération qui serait niée avec comme conséquence
inéluctable l’institutionnalisation de notre syndicat ou sa dilution ou
son intégration dans la mouvance du syndicalisme institutionne.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°10          |            |          |            |                           |
| PTT95                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
