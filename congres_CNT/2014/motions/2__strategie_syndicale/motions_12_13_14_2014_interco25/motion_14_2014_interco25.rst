
.. index::
   pair: Motion 14 ; 2014

.. _motion_14_2014_interco25:

======================================================================
Motion n°14 2014 **Rôle du secrétariat international** (Interco 25)
======================================================================

Motions interco25

   - :ref:`syndicats:motions_interco25`

Motion
=======

Le secrétariat international (SI) a pour rôle d'effectuer les
tâches suivantes:

- synthèse et information aux syndicats de l'activité des organisations
  anarcho-syndicalistes et syndicalistes révolutionnaires (toutes
  tendances confondues) et des campagnes de solidarité organisées,
  par une circulaire ou un bulletin au moins trimestriel,
- rédaction d'une rubrique internationale dans le Combat syndicaliste,
- réponse aux demandes d'organisations étrangères, transmission aux
  syndicats des demandes dépassant la compétence du SI, information
  des organisations étrangères sur l'activité de la CNT,
- si la participation de la CNT à la Coordination rouge et noire est
  confirmée : traduction et transmissions aux syndicats de toutes les
  informations nécessaires à leur participation effective.
- pour réaliser ce travail, coordination d'une commission de
  traducteur.trice.s,
- La signature de textes collectifs de solidarité et les déplacements
  du SI à l'étranger sont soumis à une décision des syndicats par
  référendum.
  Les rencontres et rassemblements en France sont organisés en commun
  avec les structures locales de la CNT, les rencontres de branche
  avec les fédérations et syndicats concernés.


Amendement
===========

.. seealso::

   - :ref:`amendement_motion_14_2014_sinr44`



Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°14 2014

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
