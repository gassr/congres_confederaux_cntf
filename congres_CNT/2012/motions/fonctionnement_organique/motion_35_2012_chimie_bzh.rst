
.. index::
   pair: 35; Motion 2012

.. _motion_35_2012:

==================================================================================
Motion n°35 2012 : Création d’un Institut de formation confédéral (Chimie Bzh)
==================================================================================
  

.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`
   - :ref:`motion_34_2012`




Motion reprise de STE93, 2010
=============================

- :ref:`motions_34_35_2010`

Motion
======


Une commission « Institut de formation » sera mise en place pour travailler sur
la création d'un institut de formation et présenter un projet concret et chiffré
à la Confédération au prochain congrès.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°35          |            |          |            |                           |
| Chime-Bzh            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Conclusion
===========

.. seealso::

   - :ref:`motions_34_35_36_xxxii`
