

.. _contre_motion_2_3_2010_ste93:

===========================================
Contre-motion aux motions N°2 et 3 (STE93)
===========================================


Estimant que ces deux motions se complètent la :ref:`motion_3_2010` précisant 
la :ref:`motion_2_2010`, le STE93 demande à ce que ces deux motions soient fusionnées.

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion aux motions n°2 et 3 |            |          |            |                           |
| STE 93                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2010`
   - :ref:`motion_3_2010`
