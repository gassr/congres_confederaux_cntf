
.. _amendement_motion_23_2021_stp67:

=========================================================================================================================================
Amendement N°2 à la motion n°23 2021 **Lutte antisexiste et anti-patriarcale en interne et sur nos lieux de travail** par **STP67**
=========================================================================================================================================

- :ref:`motion_23_2021_stt59_62`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`


Argumentaire
===============

- ?

Amendements
============

Le sondage devra assurer la protection des sondées, en utilisant des
outils libres, hébergés par nous même ou via des procédures d’anonymat
et de sécurité des données.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°23 |            |          |            |                           |
| STP67                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
