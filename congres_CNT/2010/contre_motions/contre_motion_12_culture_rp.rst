

.. _contre_motion_12_2010_culture_rp:

==========================================================
Contre-motion à la motions N°12 Culture spectacle RP
==========================================================


sur la dernière phrase de la :ref:`Motion 12 <motion_12_2010>`::

    Cet accompagnement d’une durée minimale allant jusqu’au premier CCN.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°12     |            |          |            |                           |
| Culture spectacle RP               |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_12_2010`
