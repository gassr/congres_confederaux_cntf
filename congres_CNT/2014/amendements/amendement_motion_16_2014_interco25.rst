
.. _amendement_motion_16_2014_interco25:

================================================
Amendement à la motion n°16 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_16_2014_ess34`




Argumentaire
=============

Quelle importance que les gens s'abstiennent s'ils ne luttent pas davantage ?

Quelle importance que les gens continuent de voter s'ils se mettent à lutter ?

Les élections passent, les problèmes restent, même quand l'abstention est
massive, autant profiter de ce moment où les gens s'intéressent plus
aux questions politiques pour les inciter à agir par eux-mêmes, même s'ils
votent.

Amendement
==========

Remplacer "organise des campagnes d'abstention" par **ne soutient aucun candidat**.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°16 2014 <motion_16_2014_ess34>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
