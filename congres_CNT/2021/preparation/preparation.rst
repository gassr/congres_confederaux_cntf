
.. _prepa_congres_xxxv_CNT:

========================================================
Préparation du XXXVe Congrès CNT 2021 de Dijon
========================================================

- https://framaforms.org/congres-cnt-25-27-juin-2021-dijon-1574424395


Le :ref:`syndicat SINR 44 <syndicats:motions_sinr44>` s’est porté candidat
pour coordonner la commission.

La commission est donc composée des adhérent.es du :ref:`SINR44 <sinr44>`
et du :term:`secrétariat confédéral`.


.. toctree::
   :maxdepth: 4
   :caption: 2021

   2021/2021

.. toctree::
   :maxdepth: 2
   :caption: 2020

   2020/2020

.. toctree::
   :maxdepth: 2
   :caption: historique


   historique
