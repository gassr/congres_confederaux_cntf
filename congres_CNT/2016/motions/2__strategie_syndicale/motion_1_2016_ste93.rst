
.. index::
   pair: Motion 1 ; 2016

.. _motion_1_2016_ste93:

===================================================================================================================================
Motion n°1 2016: Solidarité avec la lutte du peuple kanak (STE 93) [adoptée]
===================================================================================================================================

.. seealso::

   - http://www.cnt-f.org/international/La-Kanaky-a-l-heure-du-choix.html



Argumentaire
=============

En 2018, un référendum d'autodétermination sera organisé en Kanaky.

Pour la première fois depuis l'Algérie, une colonie pourra choisir de
s'émanciper de la tutelle française. Ce référendum est le fruit d'une
lutte constante du peuple kanak pour avoir le droit de vivre libre sur
sa propre terre.

Dans les années 1980, l'opposition entre indépendantiste kanak, notamment
réunis au sein du FLNKS (Front de Libération Nationale Kanak et Socialiste),
et loyalistes issus, en grande partie, de la droite et de l'extrême-droite,
ont provoqué d'importantes violences et exactions du pouvoir colonial.
Parmi celles-ci, par exemple, l'assassinat d'Eloi Machoro en 1985.

La manifestation organisée à Paris suite à la mort du dirigeant
indépendantiste avait été interdite et, déjà, la CNT était présente pour
rappeler son engagement internationaliste.

Un engagement ayant conduit le Secrétariat international a noué des liens
forts avec l'USTKE, l'Union des Syndicats des Travailleurs Kanaks et des
Exploités. Le nom de ce syndicat illustre son discours et ses pratiques.
Outil pour l'émancipation des travailleurs kanaks sur leur terre, liant
lutte de libération nationale et lutte de libération sociale,l'USTKE
est naturellement ouvert à tous les travailleurs vivant en Kanaky, sur
des bases de classe.

Ces liens ont été renforcés lors d'i07 avec la participation d'une
délégation de l'USTKE à ce moment fort de la solidarité internationaliste
à la CNT.

En décembre 2015, pour la première fois, la CNT mandatait l'un de ses
militants pour la représenter au congrès de l'USTKE. Cette venue,
supposant un effort financier considérable pour une organisation comme
la notre, a été particulièrement bien accueillie par nos camarades kanak.
Elle fut suivie par plusieurs rencontres, à Paris, avec des membres du
Bureau Confédéral de l'USTKE.

A l'heure où nous nous opposons légitimement aux guerres impérialistes
menées par la France en Afrique et au Moyen-Orient, il apparaît essentiel
de rappeler que l'Etat français reste un Etat colonial, notamment en
Kanaky. Et, par conséquent, de rappeler notre solidarité avec les hommes
et les femmes luttant pour se débarrasser du joug colonial français et
du capitalisme, en premier lieu nos camarades de l'USTKE.

Aussi, ce sujet étant totalement absent des revendications portées par
les organisations politiques ou syndicales, nous devons le remettre à
sa place, c'est à dire un enjeu de premier plan, par exemple en
organisant des réunions publiques sur le sujet.

Pour cela, plusieurs supports vidéos existent (fiction ou documentaires),
des camarades kanaks sont présents dans plusieurs villes et des
militants CNT prêts à se déplacer pour intervenir... Ces événements
pourraient ainsi être organisés par le groupe de travail Océanie du SI
avec un ou plusieurs syndicats, dans plusieurs villes françaises.


Motion
=========

La Confédération Nationale du Travail rappelle son engagement
internationaliste pour la reconnaissance du droit à l'autodétermination
des peuples, notamment ceux dominés par la France.

Elle inscrit ce combat dans la lutte des classes.

Dans la perspective du référendum d'autodétermination en Kanaky, en 2018,
la CNT :

- soutient le peuple kanak dans son droit à l'autodétermination et à
  pouvoir maîtriser son futur ;
- rappelle sa solidarité avec l'USTKE et la non-implantation de la
  CNT en Kanaky, y compris à travers des *adhérents isolé* ;
- s'engage à organiser une campagne d'information, notamment à travers
  des évènements publics, sur la lutte du peuple kanak, en lien avec les
  militantes et militants kanak présents en France et l'USTKE.

Amendements
===========


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°1 2016

       :ref:`SINR44 <sinr44>`
     - 42
     - 0
     - 3
     - 1
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
