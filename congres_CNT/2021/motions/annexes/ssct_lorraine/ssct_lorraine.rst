

.. _ssct_lorraine_2017_03_05:

===============================================================================================================================
Dimanche 5 mars 2017 **Conclusions du SSCT Lorraine concernant l'accusation pour viol de son adhérent** (00:33:17)
===============================================================================================================================

- :ref:`syndicats:motions_sest_lorraine`
- :ref:`syndicats:etpics57_to_sipmcs`


.. figure:: courriel_2017_03_05.png
   :align: center


::

    -------- Forwarded Message --------
    Subject: 	[XXXXXXXXXXX] Conclusions du SSCT Lorraine concernant l'accusation pour viol de son adhérent
    Date: 	Sun, 5 Mar 2017 00:33:17 +0100
    From: 	sanso-ct.lorraine@cnt-f.org


    Bonjour,

    voici en pièce jointe le compte-rendu de conclusions de notre syndicat
    suite à la procédure qu'il a menée au sujet des accusations de viol contre
    son adhérent.

    Il est accompagné de toutes les pièces jointes relatives à cette affaire
    qui permettront aux syndicats de la CNT de juger notre positionnement.

    Nous demandons au webmaster confédéral de bien vouloir publier le
    communiqué de presse de notre syndicat, également en pièce jointe.

    Par ailleurs, notre syndicat indique qu'il est favorable à la tenue d'un
    congrès confédéral extraordinaire afin que la CNT puisse traiter les
    questions qui la traversent aujourd'hui.

    Bonne lecture,

    Salutations anarchosyndicalistes et syndicalistes révolutionnaires,

    Syndicat SSCT Lorraine


.. toctree::
   :maxdepth: 3

   compte_rendu/compte_rendu
   communiques/communiques
