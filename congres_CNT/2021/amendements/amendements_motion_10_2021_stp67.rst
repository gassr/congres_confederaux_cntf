
.. _amendements_motion_10_2021_stp67:

=========================================================================================================================================
Amendements N°5 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **STP67**
=========================================================================================================================================

- :ref:`motion_10_2021_etpic30`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`


Amendement n°1 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **STP67**
=========================================================================================================================================


Argumentaire 
----------------

- ?

Amendement
-------------

Correction syntaxique, ajout de « est »:: **"La CNT groupe des travailleurs
et des travailleuses est conscient.e.s de la lutte à mener contre toutes
formes de discriminations et d’oppressions économiques et sociales en lien
avec le sexe, le genre, ou les orientations sexuelles.**"


Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
|  n°10 STP67                 |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


Amendement n°2 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **STP67**
=========================================================================================================================================


Argumentaire 
----------------

- ?

Amendement
-----------


Dans le paragraphe suivant : « – pour soutenir ses travaux, la Commission
peut être amenée à proposer la non mixité qui permet notamment, entre
personnes du même genre de libérer et de se réapproprier la parole,
d’éviter la remise en question du vécu, de gagner du temps et de déterminer
des priorités, etc. La non mixité est vue ici comme un moyen ou un outil,
non comme une finalité ; »,

remplacer la première occurrence « la non mixité » par « la mixité choisie sans hommes cis ».


Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°2 à la motion  |            |          |            |                           |
|  n°10 STP67                 |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+



Amendement n°3 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **STP67**
=========================================================================================================================================


Argumentaire 
----------------

- ?

Amendement
-----------


Ajout à la fin « si une fédération des femmes est constituée, cette
commission pourra exister pour exécuter ses décisions en interne. »

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°3 à la motion  |            |          |            |                           |
|  n°10 STP67                 |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
