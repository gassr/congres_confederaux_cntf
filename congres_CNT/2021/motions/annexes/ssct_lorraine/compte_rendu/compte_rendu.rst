

.. _rendu_ssct_lorraine_2017_03_05:

==========================================================================================================================
Dimanche 5 mars 2017 **Conclusions du SSCT Lorraine concernant l'accusation pour viol de son adhérent** (00:33:17)
==========================================================================================================================

.. seealso::

   - https://mail.google.com/mail/u/0/?tab=wm&ogbl#inbox/FMfcgxmTmtckCRQslHRPqxGDjZVNkMQf


:download:`Compte rendu au format PDF <compte_rendu.pdf>`

::

    -------- Forwarded Message --------
    Subject: 	[Liste-syndicats] Conclusions du SSCT Lorraine concernant l'accusation pour viol de son adhérent
    Date: 	Sun, 5 Mar 2017 00:33:17 +0100
    From: 	sanso-ct.lorraine@cnt-f.org
    To: 	Liste syndicats <liste-syndicats@bc.cnt-fr.org>
    CC: 	webmaster@cnt-f.org


    Bonjour,

    voici en pièce jointe le compte-rendu de conclusions de notre syndicat
    suite à la procédure qu'il a menée au sujet des accusations de viol contre
    son adhérent.

    Il est accompagné de toutes les pièces jointes relatives à cette affaire
    qui permettront aux syndicats de la CNT de juger notre positionnement.

    Nous demandons au webmaster confédéral de bien vouloir publier le
    communiqué de presse de notre syndicat, également en pièce jointe.

    Par ailleurs, notre syndicat indique qu'il est favorable à la tenue d'un
    congrès confédéral extraordinaire afin que la CNT puisse traiter les
    questions qui la traversent aujourd'hui.

    Bonne lecture,

    Salutations anarchosyndicalistes et syndicalistes révolutionnaires,

    Syndicat SSCT Lorrain

.. toctree::
   :maxdepth: 3

   preambule/preambule
   methodologie/methodologie
   les_faits/les_faits
   contexte/contexte
   conclusion/conclusion
