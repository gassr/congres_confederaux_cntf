
==========================================
Candidatures aux mandats confédéraux
==========================================

- :download:`Télécharger le CAHIER_de_la_du_mandate·e_35eme_Congres_confederal_2021_06_01.pdf  <../preparation/2021/06/01/CAHIER_de_la_du_mandatee_35eme_Congres_confederal_2021_06_01.pdf>`


Mode d'emploi du présent cahier de la.du mandaté·e.

Vous trouverez dans ce cahier une aide pour reporter le vote de votre
syndicat quant à chaque motion, contre-motion et amendement.

**Le cadre suivant est placé à la suite de chaque texte mis au vote :**

Secrétaire confédéral·e
============================

+-------------------------------------------------+------------+----------+------------+---------------------------+
| Secrétaire confédéral·e                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=================================================+============+==========+============+===========================+
| Serge Morisset (PTT Région centre)              |            |          |            |                           |
+-------------------------------------------------+------------+----------+------------+---------------------------+
| Serge Panel (STP 26)                            |            |          |            |                           |
+-------------------------------------------------+------------+----------+------------+---------------------------+



Secrétaire confédéral·e adjoint·e (administration)
====================================================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secrétaire confédéral·e adjoint·e (administration) | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Serge Morisset (PTT Région centre)                 |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
| Serge Panel (STP 26)                               |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Secrétaire confédéral·e adjoint.e (contacts)
================================================


+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secrétaire confédéral·e adjoint.e (contacts)       | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|                                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Trésorerie confédérale 💰
============================

+----------------------------------------------------+------------+----------+------------+---------------------------+
|  Trésorerie confédérale  💰                        | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|  Christian (Interco 09)                            |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Comptabilité 💰
==================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Comptabilité  💰                                   | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|                                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Secrétariat international
=============================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secrétariat international                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Tristan (Etpreci 75)                               |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Relations média 📢
=====================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Relations média 📢                                 | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Niko (ESS 34)                                      |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Secteur vidéo 🎥
==================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secteur vidéo  🎥                                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|                                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Webmaster
==============

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Webmaster                                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Juliette (SINR 44)                                 |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Postmaster
=============

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Postmaster                                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|                                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+



Secteur Propagande 📣
======================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secteur Propagande 📣                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Kelly (Etpic 30)                                   |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
| Vincent (Etpic 30)                                 |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Combat syndicaliste (administration)
=======================================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Combat syndicaliste (administration)               | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Daniel (Interpro 07)                               |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Combat syndicaliste (rédaction)
=====================================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Combat syndicaliste (rédaction)                    | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Emmanuelle Daudin (SSECT 71/58)                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+

Bulletin intérieur
=======================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Bulletin intérieur                                 | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Fred (Chimie Bretagne)                             |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+

Secrétariat juridique
=========================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Secrétariat juridique                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Fred (Chimie Bretagne)                             |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+

Revue théorique
==================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Revue théorique                                    | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|                                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Intranet 🖥
============

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Intranet                                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Ben (Educ 38)                                      |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
| Ned (Educ 21)                                      |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
| Dad (Educ 21)                                      |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
| Cédric (SIPMCS)                                    |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Imprimerie confédérale 🖨
============================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Imprimerie confédérale                             | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Interco 42                                         |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Réseaux sociaux 🌐
=====================

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Réseaux sociaux 🌐                                 | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
| Fonz (SEST 57)                                     |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+


Textile 👕
============

+----------------------------------------------------+------------+----------+------------+---------------------------+
| Textile  👕                                        | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================================+============+==========+============+===========================+
|  Gilles (Interpro 07)                              |            |          |            |                           |
+----------------------------------------------------+------------+----------+------------+---------------------------+
