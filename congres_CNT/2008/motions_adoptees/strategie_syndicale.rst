

.. _strategie_syndicale_cnt_congres_lille_2008:

===================================================================================
Stratégie syndicale 30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
===================================================================================

- :ref:`motion_synthese_droit_syndical_2008_lille`

Développement de bourses du travail
===================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 31         | 7        | 7          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

En réponse à l’éclatement du salariat, à la création de bureau de placement
privé, à la forte tension sur certains secteurs professionnels le syndicalisme
en général et la C.N.T en particulier doivent **développer des bourses du travail
ou des bureaux de placement associatif lié au syndicat.**

Le secteur SAP de la fédération santé social, a la volonté de développer le
secteur par des formations régionales à destinations des syndicats intéressés
par le projet, et de décliner sa campagne en différentes langues en réponse
concrète à l’immigration choisie de Sarkozy.

Ces formations nécessitent un financement confédéral y compris du secteur
international, afin de financer déplacements, campagnes, défraiements des
intervenants.

La précarité du secteur a pour conséquence un faible financement pour les
cotisations. La précarité des conditions et des droits syndicaux où n’existent
aucune décharge syndicale possible, rend nécessaire le financement par le
syndicat de journées de travail perdues.


Création d’une commission confédérale "Coopératives syndicales" & économie coopérative
======================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 26         | 4        | 3          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Il est créé une commission confédérale "coopératives syndicales/économie
coopérative" chargée de :

- mutualiser les informations liées aux systèmes coopératifs existants par la
  création d’un site internet.
- coordonner les camarades déjà présents dans le réseau coopératif
- réfléchir à la question des reprises en coopératives comme réponse aux
  délocalisations, fermetures d’entreprises et comme alternative aux petites
  entreprises et à l’artisanat.
