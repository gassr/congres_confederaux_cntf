
=========================================
Articuler droits syndicaux & projet ASSR
=========================================




Au regard des droits syndicaux délivrés par le patronat et l’administration,
exposés dans la partie précédente, nous pouvons à présents interroger leur
utilisation. Il en va de même du recours aux permanents qu’ils soient salariés
ou détachés de leur entreprise ou de leur administration.

.. toctree::
   :maxdepth: 3


   A_administration
   B_besoins_techniques/index
