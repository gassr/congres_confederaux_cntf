
.. _amendement_motion_9_2014_sse31:

================================================
Amendement à la motion n°9 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_9_2014_sinr44`




Argumentaire
=============

On voudrait insister, dans la motion, sur le fait que cette rencontre est une
nécessité voulu par la base.

Amendement
===========

Dans le but d'unir localement les forces militantes anarcho-syndicalistes,
la CNT mandate son secrétaire confédéral pour inviter le secrétariat
confédéral de la CNT-AIT à une rencontre, sur les bases suivantes :

- échange sur l'implantation syndicale géographique et professionnelle
- échange sur les stratégies syndicales employées
- réflexion sur de possibles actions communes



Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°9 2014 <motion_9_2014_sinr44>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
