
.. _contre_motion_43_2010_stp72:

====================================================================================================
Contre Motion n°43  : Syndicalisation des travailleurs-euses  des collectivités territoriales, STP72
====================================================================================================


Contre-motion présentée par le STP72  Syndicat des travailleurs et travailleuses
précaires de la Sarthe  complément à l'envoi du 31-10-2010

Argumentaire
------------

Les collectivités territoriales, de par la diversité des métiers et de leurs
champs d'action,  ne constituent pas une « industrie » ni un secteur d'activité
spécifique.

Néanmoins, les salarié-es rémunéré-es par les collectivités relèvent du même
type d'employeur composé d'élus locaux et sont régies par des mêmes règles
spécifiques regroupées dans un statut particulier de la fonction publique
territoriale.


Amendement / Contre-proposition
-------------------------------

Certaines singularités statutaires justifient l'existence à la CNT d'outils
permettant les échanges, le partage d'informations et les analyses entre les
fonctionnaires territoriaux de différentes fédérations ou syndicats qui en
ressentiraient le besoin. Ces outils prennent la forme d'une liste de discussions
interne (territorial@cnt-f.org) et d'un espace de stockage de documents publics
géré par un ou des mandatés désignés au sein d'une coordination de collectivités
territoriales composées de plusieurs membres volontaires, éventuellement
libres de se réunir quand ils le souhaitent pour leurs travaux.

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 56       |            |          |            |                           |
| STP72                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso:: :ref:`motion_43_2010`
