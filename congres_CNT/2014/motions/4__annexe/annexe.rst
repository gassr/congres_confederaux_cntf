
.. _annexe_motions_2014:

====================
Annexe motions 2014
====================


.. toctree::
   :maxdepth: 3

   motion_x_2014_ste49
   argumentaire_contre_motion_9_etpics57
   argumentaire_contre_motion_11_tasra
   debat_ptt_centre
