.. index::
   pair: Féminisation ; Statuts confédéraux


.. _motions_statuts_confederaux_cnt_congres_paris_1998:

====================================================================================
Motions Statuts Confédéraux 26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
====================================================================================






.. _motion_feminisation_cnt_congres_paris_1998:

Féminisation
============

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 33         | 1        | 5          | 6                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

**Féminisation du vocabulaire des statuts confédéraux.**

Afin que soit mieux pris en compte les femmes dans nos statuts, nous souhaitons
une féminisation du vocabulaire (ex : travailleurs-se(s), etc.).


Remplacement du terme "salariéE" par le terme "travailleur/se" dans les statuts confédéraux
===========================================================================================

Vote
----

Adoptée suite à amendement.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 36         | 0        | 1          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le terme de "salarié-e(s)" ne nous semble pas adapté aux différentes réalités
rencontrées par chacun de nous au sein de la confédération.

Nous souhaiterions qu’il soit remplacé chaque fois par le terme,
**travailleur-se(s)**.

Il est ajouté par amendement **à l’exception des employeurs/ses et des agents
des forces répressives de l’Etat**.
