
.. _contre_motion_8_2021_cnt42:

==========================================================================================================
Contre-motion N°1 à la motion n°8 2021 **Prostitution et Abolitionnisme révolutionnaire** par **CNT42**
==========================================================================================================

- :ref:`motion_8_2021_etpics94`

Proposée par
=============

Proposée par les syndicats CNT Educ 42, CNT Culture et Spectacle 42, CNT Santé-Social 42, Interco 42.

Argumentaires
================

La traite des humain.e·s est un cas particulier du travail du sexe, et
dans ce cas il est nécessaire de lutter contre, mais pas contre l’ensemble
du travail du sexe qui est un travail au même titre que les autres métiers.
Tout travail est une appropriation de la force de travail.
Les migrant.e·s (souvent les migrantes) bloquées dans des familles pour
faire le ménage et s’occuper des enfants sans salaire pour rembourser
leur « dette » de passage font aussi partie de la traite des humain.e·s.
Il ne viendrait à l’idée de personne de réclamer l’abolition du métier
de femme de ménage ou de nounou sous prétexte que ce secteur d’activité
esclavagise un certain nombre de migrant.e·s.
Même chose pour les métiers du bâtiment ou du travail saisonnier agricole
qui pourtant emploie dans des conditions dramatiques et mal ou non payés,
sous la pression et les menaces de l’absence de papiers.
C’est la même chose pour le secteur du travail du sexe.
Dans un contexte global d'exploitation, tous.te travailleur.euse doit
pouvoir choisir de disposer de son corps comme iel l’entend, et iel est
libre de le monnayer, que ce soit avec ses muscles, sa tête ou
son cul qu’iel travaille. Il est important de rappeler que les TDS ne
sont pas des victimes à sauver, mais des précaires à soutenir, en particulier
lorsqu’iel font le choix de s’auto-organiser.
Nous rejetons la posture abolitionniste qui s’appuie sur une condamnation
morale, et consiste à faire le choix à la place des personnes des limites
à ne pas dépasser pour leur propre bien. Posture au combien paternaliste
et infantilisante, qui précarise encore plus les plus précaires des
travailleur.euses.
La CNT est abolitionniste du travail en général, mais on n'empêche pas
les personnes de travailler tant qu’une nouvelle économie ne sera pas
imaginée. Elle est contre l’état, mais on syndique les fonctionnaires !
En attendant...
Dans ce contexte il est important de laisser aux personnes les moyens
de se protéger, de se défendre.
Les travailleur.euses exploité.e·s (donc ni les forces de répression ni
les dirigeant.e·s) ont tous.tes besoin d’un syndicat pour s’organiser,
et de la solidarité des autres groupes et syndicats organisés au
sein de la CNT.

Contre-motion
===============

Les outils confédéraux de lutte sociale et au sein du travail doivent
permettre à toute personne qui le souhaite de s’organiser pour se
défendre au sein de son secteur d’activité. Dans ce contexte, des
travailleur.euses du sexe qui le souhaitent peuvent s’organiser au
sein d’un syndicat rattaché à la CNT pour défendre leurs droits.

Les syndicats CNT Educ 42, CNT Culture et Spectacle 42, CNT Santé-Social 42, Interco 42


Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°8 |            |          |            |                           |
| CNT42                         |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
