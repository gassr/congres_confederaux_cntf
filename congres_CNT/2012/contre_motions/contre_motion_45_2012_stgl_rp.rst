

.. _contre_motion_45_2012_stgl_rp:

================================================================================================
Contre-motion à la motion N°45 2012 STGL RP
================================================================================================

.. seealso::

   - :ref:`motion_45_2012_ess34`




Argumentaire
=============

Dans le but de faire émerger une majorité qualifiée lors des décisions prises
dans les CCN.

Nous proposons ce texte

Contre-motion
=============

Adoption d'une motion : Une motion est adoptée si la majorité absolue des UR présentes :
50% +1 votent pour celle-ci. Dans tous les autres cas la motion ne peut être adoptée.



Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°45     |            |          |            |                           |
| STGL-RP                            |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
