
.. _amendements_motion_7_2021_interpro31:

===============================================================================================================================================
Amendements N°1 à la motion n°7 2021 **Lutte contre la Précarité** par **Interpro31**
===============================================================================================================================================

- :ref:`motion_7_2021_etpics94`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`

.. _amendement_1_motion_7_2021_interpro31:

Amendement N°1 à la motion n°7 2021 **Lutte contre la Précarité** par **Interpro31**
=======================================================================================

Argumentaire 
---------------

La motion manque de précision sur le périmètre d’application.
Nous considérons que cette motion doit s’appliquer aux instances confédérales
et ne doit pas empiéter sur la liberté de fonctionnement local (UL, UR, syndicats…).

Amendement 
------------

La confédération prend toute la mesure des conséquences de l’alcool et
des autres drogues sur le mouvement révolutionnaire.
Elle se donne les moyens en son sein lors des CCN et des congrès de préserver
ses espaces de réflexions et de prises de décisions, pour développer et
construire l’émancipation, l’organisation et ses luttes en refusant la
consommation d’alcool et autres drogues dures dans ses instances.

.. figure:: interpro31/amendement_1_motion_7.png
   :align: center

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement 1 à la motion    |            |          |            |                           |
| n°7 Interpro31              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_7_2021_interpro31:

Amendement N°2 à la motion n°7 2021 **Lutte contre la Précarité** par **Interpro31**
=======================================================================================


Argumentaire 
--------------

Nous reconnaissons la capacité de chacun à s’autodiscipliner.
De plus, nous préférons la prévention à la répression donc nous introduisons
une notion de limite.

Amendement 
---------------

La confédération prend toute la mesure des conséquences de l’alcool et
des autres drogues sur le mouvement révolutionnaire.
Elle se donne les moyens en son sein de préserver ses espaces de réflexions
et de prises de décisions, pour développer et construire l’émancipation,
l’organisation et ses luttes en refusant la consommation d’alcool limitant
la consommation d'alcool et autres drogues dures dans ses instances.

.. figure:: interpro31/amendement_1_motion_7.png
   :align: center

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement 2 à la motion    |            |          |            |                           |
| n°7 Interpro31              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
