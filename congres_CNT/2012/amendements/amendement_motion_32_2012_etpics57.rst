
.. _amendement_motion_32_2012_etpics57:

==========================================
Amendement à la motion 32 (ETPICS57)
==========================================

.. seealso::

   - :ref:`motion_32_2012`



Argumentaire
=============

Demande que les deux points de la motion soient votés séparément.

Le deuxième point devrait être amendé.

Amendement
===========

**À tous les niveaux de l'organisation, la CNT refuse les permanents et/ou le
salariat**.

Toutefois si les syndicats sont confrontés à des difficultés réelles, exclusivement
en lien avec les UR et les fédérations, et selon des conditions multiples et
cumulatives, ils sont autorisés à prendre part à des coopératives et à participer
directement à leur gestion.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°32 |            |          |            |                           |
| ETPICS57                    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
