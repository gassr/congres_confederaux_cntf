
.. index::
   pair: motion 6  2012; Permanents syndicaux

.. _motion_etpic6_2012:

===============================
Permanents syndicaux (motion 6)
===============================

.. seealso::

   - :ref:`motion_18_2012`




Attachée à son autonomie, à la démocratie directe, au fédéralisme libertaire,
et à sa vocation révolutionnaire, la CNT est autogestionnaire et en promeut la
généralisation à l’ensemble de la société.

**Elle privilégie l’effort militant et l’action directe et collective**.

L’administration, la direction, et la représentation syndicales et politiques
de la CNT sont garanties aux travailleurs.

La CNT n’entend pas ici confier  ces mandats à un corps de professionnels,
soient-ils titrés ``syndicaux``.

**A tous les niveaux de l’organisation, la CNT refuse dés lors tous recours aux
permanents syndicaux en ce sens que l’ensemble de ses mandaté.e.s syndicaux ne
sont ni salarié.e.s, ni déchargé.e.s de service**.

Les travailleurs/ses syndiqué.e.s en charge de ces mandats (confédéraux, fédéraux,
régionaux, d’Unions locales, ou de syndicats) demeurent adhérent.e.s à leurs
syndicats.
