
.. _amendement_motion_20_2014_sse31:

================================================
Amendement à la motion n°20 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_20_2014_etpic30`




Argumentaire
=============

Dans l'idéal, nous souhaiterions qu'il y ait plusieurs modérateurs-trices de
différents syndicats. Nous voulons simplement rajouter **ou plusieurs**.

Amendement
==========

Modification du second paragraphe : Le forum de l’intranet confédéral est
modéré par **un ou plusieurs modérateur/trice** dont le mandat prévoit
expressément :ref:`... <texte_motion_20_2014>`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°20 2014 <motion_20_2014_etpic30>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
