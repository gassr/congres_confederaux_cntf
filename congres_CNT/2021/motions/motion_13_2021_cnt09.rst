
.. _motion_13_2021_cnt09:

================================================================================================================================================
Motion **n°13** 2021 **Gestion de la trésorerie confédérale et réécriture des statuts concernant la trésorerie confédérale** par **CNT 09**
================================================================================================================================================

Autres motions CNT09

    - :ref:`motions_interco09`

Titre complet
=============

Gestion de la trésorerie confédérale et réécriture des statuts concernant
la trésorerie confédérale (CNT 09)


Pour le congrès de mai 2021
=============================

Bilan du mandat de trésorier confédéral après trois ans et demi
d’exercice à ce poste.
Après de nombreuses heures de travail, des phases de déprime, des tas
de mails sans réponse, aujourd’hui le bilan est respectable.


Constat
=========

Petit *état des lieux* à ce jour, quelques mois avant le prochain congrès:


1) Cotisations réglées assez régulièrement
-------------------------------------------

Cotisations réglées assez régulièrement bien que les syndicats payent 1 ou
plusieurs trimestres au rythme qui leur convient, contribuent au CS un peu
comme ça leur va, certains commandent encore des timbres (mais de moins en moins)...

2) De temps en temps un trésorier découvre ceci ou cela
----------------------------------------------------------

De temps en temps un trésorier découvre ceci ou cela (quelque chose qui existe
depuis pas mal de temps) et m’interroge.

L’adresse de la trésorerie confédérale, ou la lecture de certaines informations
dans le tableau des cotisations, ou sur le solde du compte du secteur XX...
Je crois que cela fait partie du mandat.

3) Le tableau des cotisations est trop chargé d’information
-------------------------------------------------------------

Le tableau des cotisations est trop chargé d’information pour que la lecture
en soit simple.

Dans le tableau des cotisations, l'intérêt de mémoriser le nombre de cotisants
*précaires* et *standards* m'interroge.

Lien avec des pratiques historiques !?

Finalement que cette différence se justifie au moment du paiement de la cotisation.
Et cela devrait rester interne au syndicat.

Avons-nous besoin de ces données ? Statistiques ?
Je vous soumets la réflexion...

Cela allègera grandement le travail du trésorier.

Et compte tenu que la rotation des mandats est loin de n'être qu'une simple
formalité.
Autant que cette gestion trésorière/comptable soir la plus facile possible !

4) La notion de *standards* et *précaires* ne servent à rien
-------------------------------------------------------------------

La notion de *standards* et *précaires* ne servent à rien au niveau de la
trésorerie confédérale. Elles ne sont pas exploitées !!!

Ces informations doivent rester au niveau de chaque syndicat.
Et encore pourquoi avoir des distinctions entre les syndiqués. Un adhérent est
toujours le *bienvenu* quelque soit ses moyens financiers !

De plus cela crée du travail supplémentaire en saisie !

5) La répartition en trésorerie/comptabilité des cotisations perçues est d’une complexité sans nom
-------------------------------------------------------------------------------------------------------

La répartition en trésorerie/comptabilité des cotisations perçues est d’une
complexité sans nom.
Elle est ancienne et **n’a aucun sens à ce jour**.


.. figure:: motion_13/constat_5.png
   :align: center


Simple ? Pourquoi pas ! Je l’ai fait pendant longtemps.

6) La liste des comptes était bizarre, très très mal exploitée, peu compréhensible
------------------------------------------------------------------------------------

La liste des comptes était bizarre, très très mal exploitée, peu compréhensible
Les débits et crédits pour ces comptes étaient un peu folklorique.
Beaucoup d’erreurs et difficile de s’y retrouver.

7) Sur le bordereau de cotisation existait une ligne concernant le nombre d’adhérents
---------------------------------------------------------------------------------------

Sur le bordereau de cotisation existait une ligne concernant le nombre d’adhérents
que l’on multipliait par 0,40€.

Ce montant était crédité sur le compte *Trésorerie Confédérale*.

8) Il était instauré depuis longtemps que la caisse internationale
---------------------------------------------------------------------

Il était instauré depuis longtemps que la caisse internationale gérait directement
les remboursements en faisant directement les chèques.
Cela ne permet pas une gestion rigoureuse des comptes.
La trésorerie demande aujourd’hui une gestion beaucoup plus simple et permet
d’être réactif quant à ces remboursements, voir en émettant les chèques à l’avance.

Modifications apportées au cours du mandat
=============================================

1) J’ai rédigé un nouveau bordereau de cotisation
----------------------------------------------------

J’ai rédigé un nouveau bordereau de cotisation que j’ai envoyé sur la liste syndicale.
Celui-ci permet aux syndicats de :

- de préciser clairement le nom du syndicat et celui de la.e trésorière.er
- indiquer pour un trimestre un nombre d’adhérents *standards* et *précaires*.

Ensuite est calculé le montant de la cotisation à reverser à la trésorerie
confédérale, en additionnant le total *standards* et *précaires*.

- on renouvelle l’opération pour tous les trimestres pour lesquels on veut cotiser.
- on indique le nombre de mois pour lesquels on cotise, et le montant de la
  contribution est calculé sur la base de 4€ par mois.
- le total de la somme à verser à la trésorerie confédérale est calculé !

Je propose d’utiliser ce bordereau (ou d’en reprendre l’idée).

Ce dernier peut être envoyé par mail ou courrier.
Ces cotisations peuvent être payées par virement ou chèque bancaire.

Avec ce bordereau, qui est un classeur (réalisé avec un tableur), il y a un
onglet *mode d’emploi*.

2) J’ai changé la répartition des cotisations
------------------------------------------------



J’ai changé la répartition des cotisations car cela était très contraignant
pour la saisie.
Pour tous les trimestres, on utilise la même règle :

.. figure:: motion_13/modifications_2.png
   :align: center

J'ai supprimé le montant par adhérent (0,40 €), car peu de gens l'utilisaient,
et si payé alors versé dans le compte CONF. = X / 6

En **fonctionnement depuis mars 2018**, elle fait gagner beaucoup de temps
en saisie comptable.

3) J’ai opté pour la gestion comptable, la répartition suivante
--------------------------------------------------------------------

J’ai opté pour la gestion comptable, la répartition suivante :

.. figure:: motion_13/modifications_3.png
   :align: center


4) J’ai simplifié la saisie des cotisations
---------------------------------------------

J’ai simplifié la saisie des cotisations en ne saisissant qu’une ligne *cotisation*
du syndicat XXX et le montant total de la cotisation dans le compte *CONF*.

Je fais ensuite la répartition suivant la règle de *répartition des cotisations*
que j’ai stipulé plus haut.

5) Je propose une modification du tableau des cotisations
--------------------------------------------------------------

Je propose une modification du tableau des cotisations ( auquel j’ai rajouté un
onglet explicatif) :

Suite à la réception du bordereau, je ne saisis plus que le nombre d’adhérent
pour chaque trimestre cotisé : à titre d’exemple


Cela donnerait un tableau clair et plus lisible.

6) je propose pour le congrès, plutôt qu’une commission *trésorerie* qui à mon avis ne sert à rien
-----------------------------------------------------------------------------------------------------

Je propose pour le congrès, plutôt qu’une commission *trésorerie* qui à mon
avis ne sert à rien, de faire une formation de 3/4 heures sur la trésorerie.

C’est une façon de vérifier la cohérence du travail fait, de transmettre le
*savoir*, de répondre aux interrogations des camarades...

7) J’essaie d’envoyer régulièrement un état des comptes
-------------------------------------------------------------

J’essaie d’envoyer régulièrement un état des comptes et le tableau des cotisations,
pour que les choses soient le plus clair possible.


Proposition de nouveaux statuts pour la TRÉSORERIE
====================================================

Article 17 : Principe
-------------------------

Les syndicats encaissent les cotisations syndicales.

La carte confédérale peut être commandée au Bureau confédéral, ou peut se procurer
par le canal de l’Union locale et/ou l’Union régionale.

Elle est donnée par le syndicat à la personne qui se syndique.

Les timbres sont remplacés par un tampon que les syndicats réalisent ou se
procurent auprès de leur Union locale ou Union régionale.
Le modèle ayant été défini par la Confédération.

**La carte confédérale pour chaque syndiquéE, avec la validation des mois cotisés,
est obligatoire**.


Article 18 : Cotisation
--------------------------

Trimestriellement, chaque syndicat s’acquitte des cotisations au bureau
confédéral, aux bureaux exécutifs de sa Fédération, son Union locale et
son Union régionale.

Le montant des cotisations est fixé par les congrès respectifs au niveau de
l’aire géographique ou de d’industrie concernée.

Les informations nécessaires à fournir lors du reversement des cotisations
au Bureau confédéral pour chaque syndicat, sont:

1) L'année
2) Nom et adresse du syndicat (comme indiqué dans le tableau de cotisations).
3) Nom et coordonnées de/du la trésorière écrit clairement
4) Pour chaque ligne de chaque trimestre :
   Saisir un nombre de cotisants, qui sera multiplié par 3 et donnera le montant
   de la cotisation pour la ligne
   Le montant reversé à la trésorerie confédérale est de 2,60€ pour un adhérent
   standard et de 1,20€ pour un adhérent précaire.
5) Le nombre d'adhérents est calculé en additionnant le nb standards et précaires.
6) Pour la ligne Contribution au CS : noter le nombre de mois de cotisation

La contribution mensuelle est de 2*2€ par mois, soit 48€ pour l’année.

Dans le tableau des cotisations, n’est saisi qu’un nombre d’adhérents
(standards et précaires additionnés), la contribution au CS et le nombre de
trimestre cotisés.


Article 19 : Gestion comptable de la trésorerie
---------------------------------------------------

.. figure:: motion_13/article_19_gestion_comptable.png
   :align: center

Si c’est une entrée en trésorerie, cela donne une sortie dans l’un des six comptes.

Répartition des cotisations en trésorerie
++++++++++++++++++++++++++++++++++++++++++++

.. figure:: motion_13/article_19_repartition_tresorerie_1.png
   :align: center

Pour tous les trimestres, on utilise la même règle

Montant de la cotisation = CONF     International Solidarité Propagande
              X            X / 2    X / 6         X / 6      X / 6

.. figure:: motion_13/article_19_repartition_tresorerie_2.png
   :align: center


On saisit en trésorerie une seule ligne cotisation pour un syndicat, que celui-ci
ait cotisé pour un ou plusieurs trimestres:

- Cotisations CNT Syndicat XXX / année 2019
- Contribution au CS de CNT Syndicat XXX / année 2019

Une entrée est faite pour le compte banque en trésorerie, et elle est répartie
suivant la règle de répartition décrite ci-dessus.


Article 20
----------------

Les comptes de la CNT sont confiés au trésorier confédéral, qui en est responsable
sous le contrôle du :term:`B.C`.

La nature des dépenses est contrôlée par le congrès, et un compte-rendu financier
sera fait à chaque :term:`C.C.N` par le/la trésorierE confédéralE.

Article 21 : Commission de contrôle
---------------------------------------

Il est constitué à chaque congrès et CCN une commission de contrôle.

Elle est chargée de la vérification de la comptabilité, et du contrôle des
opérations financières de la C.N.T., aini que la vérification des conditions
de cotisations exigées – des régions au C.C.N. et des syndicats au congrès –
pour leur participation.

Elle devra établir à l’occasion de chaque C.C.N. et de chaque congrès un
rapport sur la situation financière qui sera présenté à chaque organisation
participante.

Article 22 : Caisse de Solidarité
---------------------------------------

Il y a un compte Solidarité/Procédures, qui remplace ce qui était appelé
*Caisse de Solidarité*, et qui est alimenté par une part des cotisations
versées à la Confédération.

Ce compte peut être alimenté par des dons, contributions diverses.

Article 23 : Caisse INTERNATIONALE
------------------------------------

Il y a un compte *INTERNATIONAL*, qui est alimenté par une part des cotisations
versées à la Confédération, et peut l’être par des dons ou des contributions.

Les déplacements à l’international générant des frais parfois élevés, ils
doivent être gérer directement par la trésorerie confédérale.


Amendements
===========

- :ref:`amendement_motion_13_2021_interpro31`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°13 2021 CNT09      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+

     -
