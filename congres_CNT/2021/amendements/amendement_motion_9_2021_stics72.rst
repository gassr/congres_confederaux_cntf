
.. _amendement_motion_9_2021_stics72:

============================================================================================
Amendement N°4 à la Motion n°9 2021 **Lutte contre la Précarité** par **STICS72**
============================================================================================

- :ref:`motion_9_2021_interco69`

Autres motions STICS72

    - :ref:`syndicats:motions_stics72`

Amendement
============

.. raw:: html

    Se mobiliser dans la lutte contre <del>la</del> <span class="modif_rouge">toute forme</span>de précarité,
    l’intérim, <del>en particulier</del> dans le secteur public, <span class="modif_rouge">les travailleur·ses
    indépendant·es de la santé, les prestataires individuel·les, les
    vacataires, contractuel·les, autoentrepreneur·ses, les temps
    partiels subis</span>, et l’expansion d’un salariat déguisé initié par
    l’économie numérique et la libéralisation dans tous les secteurs
    (privé/public).


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°9  |            |          |            |                           |
| STICS72                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
