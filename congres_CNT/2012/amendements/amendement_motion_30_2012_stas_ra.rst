
.. _amendement_motion_30_2012_stas_ra:

==========================================
Amendement à la motion 30 (STAS-RA)
==========================================

.. seealso::

   - :ref:`motion_30_2012_etpics57`



Argumentaire
=============

La motion ci-dessous est un amendement à la motion n°30, elle n’a vocation à
être discutée que si la motion n°30 est adoptée dans un premier temps.

Il est indiscutable que la place prise aujourd'hui par l'extrême droite
nécessite la création d'une commission ad hoc. Toutefois, l'anti-sexisme ne se
résume pas aujourd'hui au combat contre l'extrême droite.

Les luttes actuelles de défense des centres IVG en sont une amère illustration.

Amendement
===========

Le congrès acte la création de 2 commissions, l'une anti fasciste, l'autre
anti sexiste.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°30 |            |          |            |                           |
| STAS-RA                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
