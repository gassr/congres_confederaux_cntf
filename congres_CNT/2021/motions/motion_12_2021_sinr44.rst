.. index::
   pair: Motion 12 ; 2021

.. _motion_12_2021_sinr44:

================================================================================================================================================
Motion **n°12** 2021 **Modification des statuts pour la gestion interne des violences patriarcales** par **SINR44**
================================================================================================================================================

- :ref:`statuts_conf:article_24_statuts_CNT_2014`

Autres motions SINR44

    - :ref:`syndicats:motions_sinr44`


Argumentaire
==============

Dans le :ref:`protocole proposé par le STE 33 <motion_1_2021_ste33>`, ainsi que
dans notre amendement, il est envisagé qu’un·e adhérent·e d’un syndicat soit
exclu·e par **décision confédérale**.

Or dans l’état actuel des choses, cette exclusion est contraire aux
:ref:`statuts confédéraux <statuts_CNT>`, seul un syndicat pouvant exclure
l’un·e de ses membres.

Cette modification vise à rendre possible une telle exclusion, uniquement
dans des circonstances extraordinaires qui seront définies par les congrès
confédéraux.

Motion
========

Ajout à :ref:`l’article 24 <article_24_statuts_CNT_2014>` des Statuts confédéraux,
**GESTION DES CONFLITS INTERNES**

Dans des circonstances exceptionnelles, prévues par des motions de congrès,
la confédération peut se substituer à un syndicat pour décider de l’exclusion de
l’un·e de ses adhérent·e·s.

Amendements
===========

- :ref:`amendement_motion_12_2021_interpro31`

Contre motions
================

- :ref:`contre_motion_12_24_2021_cnt30`
- :ref:`contre_motion_12_2021_etpics94`
- :ref:`contre_motion_12_24_27_2021_ste75`



Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°12 2021 SINR44     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
