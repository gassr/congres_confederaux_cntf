

.. _contre_motion_4_2010_ess34:

======================================
Contre-motion à la motion N°4 (ESS 34)
======================================

1. Prise de contact entre le BC (ou une UR) avec des militantEs souhaitant 
   constituer  un syndicat CNT: 

  - Apport d’informations sur l’identité de la CNT (si demandé), explication de la 
    démarche de création à organiser en lien avec l’UR de référence si elle existe,
    information et échange sur l’existence de syndicats sur la même aire géographique
    ou à proximité.
  - Information et échange sur la nécessité d’apparaître sur un champ de syndicalisation
    «professionnel» et un champ géographique de syndicalisation inexistant, ou renvoi 
    sur les syndicats existants. 

2. Prise de contact avec le Bureau Régional de l’UR CNT concernée, transmission 
   de statuts type (si demandés), apports de conseils sur la définition du champ de
   syndicalisation : pour les Interco, inscription à minima sur un bassin
   d’emploi/économique reconnu (commerce et industrie par exemple, STICS, ETPIC, ETPRECI, etc.). 

3. Demande des documents nécessaires à la validation par le BC: 

  - Projet de statuts à déposer et validation ou rejet par le BC (en lien avec l’UR concernée) 
    en fonction de la conformité avec les statuts et les orientations de la CNT & des
    champs de syndicalisation (géographique et professionnel).
    Suivi et avis motivé des rejets. Une souplesse d'adaptabilité sera 
    observée pour les syndicats CNT réactivés si inactivité manifeste depuis plusieurs
    années ou délabellisation antérieure ; 
  - Récépissé de dépôt en mairie faisant apparaître le n° de déclaration pour : procès verbal
    d'AG (désignation nouveau bureau ou renouvellement du bureau), dépôt des nouveaux statuts ; 
  - Coordonnées des membres du bureau (non obligatoire mais utile pour des contacts directs) ;

4. Sollicitation de l'accord et de l’avis de l’UR concernée (si elle existe) 
5. Réception des premières cotisations confédérales par la trésorerie confédérale. 
6. Labellisation par le BC et information au syndicat et simultanément à son UR (+ prochaine 
   circulaire confédérale + liste web d'information aux syndicats). 
   Dans un délai de 2 mois, lorsqu’il y a opposition d'une union régionale ou d'une 
   fédération d'industrie, le BC doit suspendre sa décision, qui doit être soumise au prochain
   C.C.N. Les organismes concernés gardent le droit de présenter directement leur défense 
   soit au C.C.N., soit au Congrès.

7. Information au CCN suivant en vue de la labellisation définitive et parution à la prochaine 
   circulaire confédérale (information préalable du BC via la liste syndicat). Information du BC au 
   syndicat sur l’ensemble des outils confédéraux mis à sa disposition : circulaires confédérales, recueil 
   des motions en vigueur, aide du secteur propagande, outils net, accès aux colonnes du CS, des Temps 
   maudits, etc. 
   Le bureau confédéral sera souple dans l'application de cette motion en particulier pour l'ordre des 
   démarches à effectuer avec un syndicat en création ou en cours de labellisation qui n'aurait pas 
   respecté l'ordre des points présentés ci-­dessus. Cette procédure de labellisation sera envoyée par le 
   bureau confédéral ou par les UR, UD, UL, fédérations ou syndicats à tout syndicat en création 
   intéressé pour rejoindre la confédération.


+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°4 |            |          |            |                           |
| ESS 34                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


.. seealso::

  - :ref:`motion_4_2010`
