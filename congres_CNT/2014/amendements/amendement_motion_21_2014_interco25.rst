
.. _amendement_motion_21_2014_interco25:

================================================
Amendement à la motion n°21 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_21_2014_interco09`




Argumentaire
=============

Les réseaux sociaux sont des entreprises capitalistes qui vivent de la collecte
et de l'exploitation des données personnelles.

Nous estimons que la CNT, au niveau confédéral, n'a pas à s'exprimer par ce
biais.

D'autant plus que dans de nombreux cas, ces sites contraignent l'utilisateur à
posséder lui-même un compte pour accéder au contenu.
Il nous semble inacceptable que la CNT contraigne les internautes à collaborer
à ces entreprises pour accéder à nos informations.

Amendement
===========

Ajouter à la listes des technologies écartées :

- Réseaux sociaux (Facebook, Tweeter, etc...)

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°21 2014 <motion_21_2014_interco09>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
