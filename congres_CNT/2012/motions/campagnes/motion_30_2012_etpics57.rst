
.. _motion_30_2012:
.. _motion_30_2012_etpics57:

=======================================================================================================================================================================================
Motion n°30 2012 : Constitution d’une commission antifasciste et anti-sexiste confédérale (motion reprise du STE 93 posée lors du Congrès de Saint-Étienne) ETPICS 57 Nord Sud
=======================================================================================================================================================================================
  
.. seealso::

   - :ref:`etpics_57_pub`
   - :ref:`ste93`
   - :ref:`motions_etpics57_2012`



Reprise motion 54 STE93
========================

- :ref:`motion_54_2010`


Argumentaire
============

Signe des temps, depuis quelques années, l’extrême droite affiche une santé
resplendissante. Le Front national présente une version mariniste rajeunisée
et modernisée, mais non moins radicale. Son pouvoir d’attraction ne se dément
guère. À sa droite se sont néanmoins développés de nouveaux mouvements,
certains assez classiques : le Renouveau francais rassemble la jeunesse qui
cogne et qui prie ; le Bloc identitaire est en quête de respectabilité mais
rassemble la jeunesse païenne qui fait facilement le coup-de-poing ; le nouveau
GUD a été relancé avec l’aide des anciens naziskins des années 80 et 90...

D’autres sont plus surprenants : la Ligue 732 mélange les supporters
nationalistes, le Projet apache est une émanation « jeune et fashion »
du Bloc identitaire.

Certains mouvements cherchent à brouiller les pistes comme le groupe Égalité et
réconciliation d’Alain Soral (fort du soutien de Dieudonné) ou encore
Riposte laïque de Pierre Cassen, un groupe passé de la défense de la République
et de la laïcité aux apéros-saucisson racistes...

À ces groupes s’ajoutent des mouvements communautaristes : Ligue de défense
juive récemment recréée, Ligue de défense des musulmans (une réponse à la
précédente), nationalistes autonomes lorrains, flamands, occitans, bretons...

Sans sombrer dans la paranoïa, il semble nécessaire de prendre conscience des
dangers physiques et politiques que peut présenter cette nouvelle extrême
droite plurielle. Depuis deux à trois ans, les rassemblements à l’appel du
Réseau éducation sans frontières (RESF) sont régulièrement attaqués (à Lyon et
à Nancy dernièrement) ; les distributions de tracts d’organisations syndicales
et politiques sont empêchées (dans le Midi notamment) ; les « kiss-in » et
autres manifestations en faveur du droit à vivre librement sa sexualité, les
rassemblements pour le droit à l’IVG sont perturbés et attaqués (à Paris,
cela s’est produit deux fois en trois mois).

Dans les manifestations de solidarité avec le peuple palestinien, lors des
débats et des rencontres au 33 rue des Vignoles, les militants de la CNT sont
exposés à de nombreux dangers. Enfin, à Lyon, au début de l’année 2010,
ce sont plusieurs de nos camarades qui ont été attaqués physiquement en
raison de leur appartenance à la CNT.

Depuis 2010, l'implantation et les actions de l'extrême droite se sont encore
amplifiés. Le FN faisant sont entrée à l'Assemblée après avoir réussi à
imposer ses thématiques de campagne

Motion
=======

Le STE-93 (et l'ETPICS 57 Nord Sud) demandent au congrès d’acter la création
d’une commission antifasciste et antisexiste confédérale qui aura pour rôle
d’informer les militants sur les nouveaux visages et dangers présentés par
l’extrême droite francaise et internationale.

Cette commission produira un bulletin régulier et un matériel spécifique
(affiches, autocollants, brochures) sous le contrôle du BC et de la CA.

Elle se chargera de répondre aux sollicitations de l’ensemble des syndicats et
des syndiqué-e-s de la confédération et à prendre des initiatives afin de
lutter contre l'extrême droite.


Vote
=====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°30          |            |          |            |                           |
| ETPIC57              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Amendements
===========

- :ref:`amendement_motion_30_2012_stas_ra`
