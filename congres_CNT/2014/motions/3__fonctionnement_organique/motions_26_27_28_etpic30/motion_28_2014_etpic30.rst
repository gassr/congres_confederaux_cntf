
.. index::
   pair: Motion 28 ; 2014

.. _motion_28_2014_etpic30:

==================================================================================================================================================
Motion n°28 2014: **Ouvrage de synthèse permanent - 'CNT Fonctionnement & Orientations – Résolutions confédérales en vigueur'** (ETPIC 30 sud)
==================================================================================================================================================

Autres motions ETPIC30

    - :ref:`syndicats:motions_etpic30`
    - :ref:`motion_4_2008_etpic30`


Motion
======

L’ouvrage **CNT Fonctionnement & Orientations – Résolutions confédérales
en vigueur** est un document de synthèse interne permanent et évolutif.

Cet ouvrage est l’expression d’une synthèse des résolutions confédérales
en vigueur.

Il vise à offrir à chaque adhérent au travers d’un document unique,
accessible, et simplifié une bonne connaissance de la CNT en termes
de fonctionnement interne et/ou d’orientations.

Sa réalisation et sa mise à jour prennent appui sur l’évolution des
dernières résolutions adoptées par la CNT figurant au **Recueil des
motions de Congrès adoptées**.

Le Congrès confédéral (ou par défaut le CCN) charge une commission
de travail à laquelle il revient de rédiger ou de mettre à jour
l’ouvrage **CNT Fonctionnement & Orientations Résolutions confédérales
en vigueur**.

Tout comme le **Recueil des motions de Congrès adoptées**, l’ouvrage
**CNT Fonctionnement & Orientations – Résolutions confédérales en
vigueur** est organisé selon deux axes principaux :

- Fonctionnement & Orientations,
- et un plan thématique identifiable dans un sommaire.

Le :term`CCN` le plus proche valide l'ouvrage sur avis du Bureau Confédéral.

L’ouvrage **CNT Fonctionnement & Orientations – Résolutions confédérales
en vigueur** est par ailleurs transmis gratuitement par le Bureau Confédéral
à tous syndicats en faisant la demande.

Il est de même systématiquement proposé à tous nouveau syndicat
labellisé par la CNT.


Amendements
===========

- :ref:`amendement_motion_28_2014_interco25`
- :ref:`amendement_motion_28_2014_sipmcs`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°28 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
