

.. _amendement_motion_25_2012_sanso_rp:

==========================================
Amendement à la motion 25 (Sanso-RP)
==========================================

.. seealso::

   - :ref:`motion_25_2012_etpic30`




Argumentaire
=============

Après : « Non aux OGM, Non aux Pesticides clonés brevetés »

Au-delà des OGM, la CNT s’oppose à la brevetabilité du vivant et aux tentatives
de semenciers pour contrôler l’ensemble de la distribution des semences et donc
de l’alimentation mondiale.

L’obligation de plus en plus répandue faites aux paysans de devoir acheter
chaque année leurs semences aux entreprises de l’agroalimentaire, les empêchant
de perpétuer des pratiques ancestrales comme l’échange des semences fermières
ou la réutilisation d’une partie des récoltes pour les semailles suivantes,
rend non seulement les paysans complètement dépendants et inféodés aux
semenciers mais fait aussi peser un risque majeur sur la sécurité alimentaire
des décennies à venir en appauvrissant la diversité génétique des cultures
destinées à l’alimentation.


Amendement
==========

La CNT s’oppose aux Pesticides clonés brevetés de tout genre dont les OGM.

La CNT se porte solidaire et soutient les collectifs anti-OGM.

La CNT s’oppose à la brevetabilité du vivant.

Elle réafrme le droit et la nécessité pour les paysans, partout dans le monde,
de pouvoir échanger, multiplier et réutiliser leurs semences sans devoir les
racheter aux semenciers.

Dans ce cadre elle soutient aussi les collectifs et associations œuvrant à la
sauvegarde des variétés anciennes potagères et à la difusion libre de leurs
semences ; gage à la fois de maintien de la biodiversité et donc de la sécurité
alimentaire, et de l’autonomie des paysans face aux semenciers cherchant à les
rendre dépendant de leurs produits.


Vote indicatif
==============

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°25 |            |          |            |                           |
| Sanso-RP                    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
