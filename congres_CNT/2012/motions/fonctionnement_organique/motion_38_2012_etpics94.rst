.. index::
   pair: Gestion; Conflits

.. _motion_38_2012:
.. _motion_38_2012_etpics94:

===========================================================================
Motion **n°38** 2012  **Gestion des conflits** (ETPICS 94)
===========================================================================
  

.. seealso::

   - :ref:`etpics94`
   - :ref:`motions_etpics94_2012`



Autres motions ETPICS94
=========================

.. seealso::

   - :ref:`motions_etpics94`

Argumentaire
============

**Dans la mesure où nous sommes une organisation qui a pour projet de participer
à organiser la société**. Même si ce jour semble encore loin.

Il est à notre sens indispensable de réfléchir et d’élaborer des moyens
pour la gestion des conflits.

Que ce soit des conflits entre un ou des adhérents et leurs syndicats, que ce
soit entre des syndicats, que ce soit entre des syndicats et l’UR, que ce soit
entre des URs.

À partir des statuts confédéraux qui sont la base de notre réflexion, il est
nécessaire d’aller plus loin que ce qu’ils proposent.

Par ailleurs on constate qu’il existe déjà des conflits dans la CNT malgré
notre petit nombre.
**Il nous semble qu’on ne peut être crédible que si on sait gérer les conflits**.

On connait certaines organisations qui ont explosé parce qu’elles étaient
défaillantes sur ce plan.

**Nous rappelons que le conflit est inhérent à l’être humain et à notre projet,
qui se veut libre, ouvert à l’échange et au débat**.


Motion
======

Le congrès confédéral décide qu’une commission confédérale réfléchit sur la
question de la gestion des conflits et élabore des moyens pour gérer ces
conflits.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°38          |            |          |            |                           |
| ETPICS94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
