
.. _amendement_motion_7_2012_stp72:

==========================================
Amendement à la motion 7 (STP72)
==========================================

.. seealso::

   - :ref:`motion_7_2012_ste72`



Amendement
===========

Un congrès permet environ 20 heures de débat sur 3 jours.

Ainsi, compte tenu des probables seconds tours de votes pour les motions les
plus partagées, on ne doit pas accepter plus de 25 motions à l’ordre du jour
d’un congrès.

Si un plus grand nombre sont proposées, il faut alors procéder à soit :

a) des « pré-votes indicatifs » par intranet, qui donnent une idée
   approximative de ce que chaque syndicat compte voter, mais ne préjuge
   absolument pas de la décision finale du congrès qui reste souverain.
   Ces « pré-votes indicatifs » permettraient de voir rapidement
   quelles motions risquent de ne pas faire consensus et donc de leur accorder
   par avance un temps de discussion suffisant.

ou


b) la commission chargée de la préparation du congrès regroupe par paquets
   les motions concernant un même sujet et demande aux syndicats de classer
   ces motions par ordre d'intérêt (par une note de 1 à 10 par exemple).
   Après synthèse, les motions les plus importantes apparaitront alors en tête
   de paquets et quel que soit l'ordre, aléatoire, d'examen des paquets pendant
   le congrès, elles auront alors l'assurance d'être discutées.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°7  |            |          |            |                           |
| STP72                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
