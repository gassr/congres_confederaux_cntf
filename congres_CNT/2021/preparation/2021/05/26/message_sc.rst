

===========================================================================================
Cahier des mandaté.e.s du XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
===========================================================================================

::

    -------- Message transféré --------
    Sujet : 	[Liste-syndicats] Cahier des mandaté.e.s du XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date : 	Wed, 26 May 2021 10:21:12 +0200
    De : 	Secrétariat Confédéral <cnt@cnt-f.org>
    Pour : 	Liste-syndicats@bc.cnt-fr.org
    Copie à : 	liste CA <liste.ca@bc.cnt-fr.org>, liste-bureau@bc.cnt-fr.org, congres@cnt-f.org


Je vous transmets, ci dessous, la communication de la commission de
préparation du congrès avec le "cahier des mandaté.e.s et le feuillet
de vote (un grand merci pour le travail effectué par Valérie et la commission).

Nous pensons n'avoir oublié aucune motion et surtout contre motion toutefois
au vu des problèmes rencontrés avec la maintenance de Globenet vous voudrez
bien nous signaler en URGENCE tout oubli.

Serge STP26

S.C. provisoire


::

    "Voici finalisé le cahier de la.du mandaté.e en vue du congrès 2021,
    ainsi que les feuillets de vote qui seront à rendre à l'issue du congrès.

    Les problèmes techniques occasionnés par la migration des courriels
    n’ont pas facilité la communication et la compilation des contre-motions
    et amendements dans la dernière ligne droite.

    Une ou deux contre-motion ou amendement peuvent se trouver encore dans
    les tuyaux... N’hésitez pas à nous le faire savoir si c’est le cas !

    Nous espérons que les camarades s’y retrouveront dans un cahier que
    nous avons essayé de rendre clair et cohérent, malgré le grand nombre
    de motions, et le nombre encore plus grand d’amendements et de contre-motions !

    Nous souhaitons une bonne lecture à tou.tes en attendant le congrès !

    Valérie, pour La Commission de préparation du congrès 2021
