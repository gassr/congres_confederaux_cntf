

.. _contre_motion_1_2010_ess34:

================================================
Contre-motion à la motion N°1 (ESS 34) [rejetée]
================================================

Nous faisons les propositions suivantes pour une sortie par le haut de cette
situation  qui nous semble assez éloignée d’une situation CéNéTiste classique.

- Le salarié en question poursuit son activité uniquement concernant les dossiers
  juridiques en cours jusqu’à leur extinction.
- Poursuite des discussions pour une meilleure compréhension mutuelle avec les
  camarades du Syndicat du nettoyage sur la spécificité de la C.N.T.
  Action revendicative collective, autogestion du syndicat, autogestion de la
  C.N.T (participation aux instances), action juridique défensive.
  Concernant ce dernier aspect, la Confédération met à disposition des travailleurs du
  syndicat du nettoyage ses moyens en plus de ceux existants déjà au sein du
  syndicat pour continuer la formation entamée et traiter les cas juridiques les plus difficiles.

Fluidifier les relations avec le reste de la confédération.

Pour ce faire, les mandatés de la Confédération participent aux réunions de bureau
du syndicat du Nettoyage RP non pour le diriger mais permettre ainsi au syndicat
du Nettoyage de retrouver son autonomie dans le cadre de la CNT (**les mandaté-es sont
issus d'au moins trois syndicats différents**).

Pour mettre en œuvre ce projet, si les travailleurs du syndicat du Nettoyage
le souhaitent et si les syndicats de la confédération soutiennent cette initiative,
le syndicat CNT Etpic 94 se propose d’accepter un mandat avec deux autres syndicats
pour la mise en œuvre des propositions ci-dessus.

Si c’est le cas, les mandaté-es rendront compte mensuellement à la région d’appartenance
c'est-à-dire l’URP avec copie au Bureau Confédéral.
De plus l’URP informerait de l’évolution de la situation au cours des CCN.

Nous espérons que cette proposition sera accueillie avec le même intérêt que nous la
proposons, c'est-à-dire dans l’intérêt du syndicat du Nettoyage, de l’Union régionale
parisienne et de l’ensemble de la Confédération.

Mais bien sûr, nous restons ouverts aux aménagements éventuels et aux amendements
argumentés. De même, nous participerons aux débats éventuels si des propositions
alternatives ou complémentaires sont proposées pour que le syndicat du Nettoyage retrouve
un fonctionnement plus conforme aux aspirations de la CNT.


Vote indicatif
==============

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 | 26         | 31       | 4          | 9                         |
| ESS 34                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


.. _vote_definitif_contre_motion_1_2010_ess34:

Vote définitif contre-motion à la motion N°1 (ESS 34) [rejetée]
===============================================================

Etpic 94 retire sa motion circonstancielle n°1 au profit de celle de l'ESS 34.

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 | 28         | 35       | 3          | 7                         |
| ESS 34                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


**Contre motion rejetée**
