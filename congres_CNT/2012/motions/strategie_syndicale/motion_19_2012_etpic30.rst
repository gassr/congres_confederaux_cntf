
.. _motion_19_2012:

=======================================================================================================================================================
Motion n°19 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Formation (non soumise au débat & au vote, ETPIC30)
=======================================================================================================================================================
  
.. seealso::

   - :ref:`etpic_30_pub`
   - :ref:`motions_34_35_2010`



Motion
======

Notre syndicat considère cet axe comme essentiel mais ne présente pas là de
proposition particulière considérant que le 31e Congrès de la CNT a accueilli
des propositions concrètes (:ref:`motions n° 34 et 35 <motions_34_35_2010>`) auxquelles nous aurions pu
adhérer et contribuer.

Nous espérons juste qu’elles puissent être réintroduites au débat comme un
outil de solidarité et d’entraide interprofessionnel.

Nous rappelons néanmoins cet extrait de la motion adoptée au 31e Congrès
(présentée par le SUTE 69-01) : «Dans ce cadre le syndicat doit jouer un rôle
d'éducation populaire permanent par la formation et l'organisation régulières
de débats, internes comme externes, la diffusion de nombreuses publications et
l'édition de journaux revues ou livres »



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°19          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
