
.. _amendement_motion_26_2014_interco25:

================================================
Amendement à la motion n°26 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_26_2014_etpic30`




Argumentaire
=============

Un Comité Confédéral National (:term:`CCN`) ne peut modifier ou aller à l'encontre d'une
décision de congrès, et leurs résolutions ne sont pas compilées dans le
recueil des motions de congrès adoptées.

Amendement
===========

Suppression de la mention des CCN


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°26 2014 <motion_26_2014_etpic30>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
