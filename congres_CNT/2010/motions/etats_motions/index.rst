

.. _etat_motions_congres_CNT_2010:

========================================================
Etat des motions du XXXIe Congrès CNT Saint-Etienne 2010
========================================================

.. toctree::
   :maxdepth: 3

   motions_adoptees
   motions_synthetisees
   motions_rejetees
   motions_a_synthetiser
   motions_non_votees
   motions_hors_statuts
