
.. _message_sc_2021_05_30:

===========================================================================================================================================================================
Message du dimanche 30 mai 2021 Cahier des mandaté.e.s du XXXVème Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon par **Secrétariat Confédéral <cnt@cnt-f.org>**
===========================================================================================================================================================================

::

    Objet: [Liste-syndicats] Cahier des mandaté.e.s du XXXVème Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date: 2021-05-30 06:37
    De: Secrétariat Confédéral <cnt@cnt-f.org>
    À: Liste-syndicats@bc.cnt-fr.org
    Cc: liste CA <liste.ca@bc.cnt-fr.org>, liste-bureau@bc.cnt-fr.org, congres@cnt-f.org


Vous trouverez, ci joints, le cahier des mandaté.e.s et le feuillet de
vote établis par la commission de préparation du congrès que nous
espérons définitifs.

Cette fois nous pensons n'avoir oublié aucune motion. Comme indiqué
précédemment il sera fourni lors du congrès une version papier pour
chaque syndicat présent.

Bonne lecture et préparation du congrès.

Serge STP26

S.C. provisoire


- :download:`Télécharger le CAHIER_de_la_du_mandatee_35eme_congres_confederal_29_MAI_2021.pdf  <CAHIER_de_la_du_mandatee_35eme_congres_confederal_29_MAI_2021.pdf>`
- :download:`Télécharger FEUILLET_DE_VOTE_2021_a_rendre_29_MAI _2021.pdf  <FEUILLET_DE_VOTE_2021_a_rendre_29_MAI _2021.pdf>`
