
.. index::
   pair: Motion 3 ; 2014

.. _motion_3_2014_sinr44:

=================================================================================
Motion **n°3** 2014 : Limitation du vote par procuration (SINR 44, statutaire)
=================================================================================




Argumentaire
=============

S'il paraît difficile de supprimer le vote par procuration lors du
Congrès confédéral, la règle actuellement en vigueur est bien trop
souple.

En effet, le congrès pourrait se tenir si chaque syndicat présent
portait réellement deux procuration avec seulement un tiers des
syndicats votants présents physiquement au congrès.

Motion (non applicable pour le 33ème Congrès confédéral)
==========================================================

Modification de :ref:`l'article 16 des statuts <article_16_statuts_CNT_2012>`.

La phrase::

    Chaque déléguéE ne peut représenter exceptionnellement
    que trois Syndicats au maximum.

est remplacée par::

    Chaque syndicat présent physiquement au congrès ne peut représenter
    qu'un **seul autre** syndicat.

    De plus ne sont prises en compte que les procurations prévues pour
    l'ensemble du congrès, et signalées au préalable à la commission
    d'organisation du congrès par le syndicat qui donne procuration
    (autrement dit un syndicat qui quitte le congrès en cours ne peut
    pas donner procuration à un autre syndicat pour finir le congrès à
    sa place).


Argumentaire (commun aux motions 4 et 5)
=========================================

En parallèle de la limitation du nombre de procurations qui peuvent
être portées par un syndicat, il apparaît absurde qu'un Congrès puisse
se tenir ou se poursuivre si la règle de procuration était appliquée à
son maximum (chaque syndicat présent physiquement portant deux voix).

En effet avec la règle actuelle, même avec la limitation du nombre de
procuration, on pourrait arriver à un Congrès avec 25% des syndicats
de la CNT réunis physiquement et étant décisionnaire.

Le Quorum doit donc comporter un nombre minimum de syndicats présents
physiquement.



Amendements
============

.. seealso::

   - :ref:`amendement_motions_2_3_2014_stics13`
   - :ref:`amendement_motion_3_2014_stp67`
   - :ref:`amendement_motion_3_2014_interco25`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°3 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
