
.. index::
   pair: Cotisations; Transports

===========
Cotisations
===========

Article 13
==========

Le montant des cotisations est fixé en Congrès Fédéral.

Il est de  1,5 euros par mois et par adhérent, afin d'assurer le bon
fonctionnement de la  Fédération.
