

.. _motions_rejetees_congres_CNT_2010:

========================================================
Motions rejetées au XXXIe Congrès CNT Saint-Etienne 2010
========================================================

Stratégie syndicale
====================


- :ref:`motion_38_2010`
- :ref:`motion_39_2010`
- :ref:`motion_43_2010`


.. _motions_rejetees_fonctionnement_organique_congres_CNT_2010:

Fonctionnement organique
========================

- :ref:`motion_15_2010`
- :ref:`motion_21_2010`
