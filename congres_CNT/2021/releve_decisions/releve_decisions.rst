
.. index::
   pair: Relevé de décisions; Congrès Dijon


.. _releve_decisions_xxxv_congres:

===========================================================================================================================
Relevé de décisions **XXXVe Congrès CNT 2021 de Dijon** du vendredi 25 au dimanche 27 juin 2021
===========================================================================================================================

.. seealso::

   - :ref:`congres_statuts_CNT`
   - :ref:`fonctionnement_congres_confederal`



Motions adoptées
============================

+-------------------------------------------------+
| Motions adoptées                                |
+=================================================+
|                                                 |
+-------------------------------------------------+




Contre-motions adoptées
============================

+-------------------------------------------------+
| Contre-motions adoptées                         |
+=================================================+
| :ref:`contre_motion_24_2021_educ38`             |
+-------------------------------------------------+
| :ref:`contre_motion_24_2021_stp67`              |
+-------------------------------------------------+
