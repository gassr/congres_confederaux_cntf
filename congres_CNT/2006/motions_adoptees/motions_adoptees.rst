

.. _motions_adoptéees_congres_CNT_2006_agen:

========================================================
Motions adoptées au XIXXe Congrès CNT 2006 Agen
========================================================


.. toctree::
   :maxdepth: 2

   strategie_syndicale
   international
   solidarite_confederale
   travail_revendications_salariales
   bureau_confederal
   secretariat_international
   outils_confederaux
   structuration_confederale
   internet_et_intranet
   bulletin_interieur
   tresorerie_confederale
