
.. _amendement_motion_13_2010_culturerp:


=======================================
Amendement à la motion 13 (culture rp)
=======================================


Il est demandé aux syndicats d’être vigilants quant aux rotations, cumul et
durée des mandats selon nos principes de fonctionnement autogestionnaire.



+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°13 |            |          |            |                           |
| culture rp                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_13_2010`
