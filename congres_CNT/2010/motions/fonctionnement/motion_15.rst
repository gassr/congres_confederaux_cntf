

.. _motion_15_2010:

=============================================================================================================================
Motion n°15 2010 : Structuration du Bureau Confédéral de la CNT et de la Commission administrative, Etpic 30 – Sud [rejetée]
=============================================================================================================================

.. seealso::

   - :ref:`motions_etpic30`




Motion reprise de ETPIC30, 2012
================================

- :ref:`motion_46_2012`

Argumentaire
============

Le Congrès confédéral de septembre 2008 donnait une nouvelle définition de la
Commission administrative «Dans l'intervalle des Comités confédéraux nationaux,
la C.N.T. est administrée par le Bureau confédéral (B.C) et la Commission Administrative
(C.A.) élus par le Congrès. La C.A. est composée des différents secrétariats
confédéraux mis en place par le Congrès.

Les membres de la CA ne pourront occuper aucun poste responsable relevant d'un
parti politique, d'une secte philosophique ou religieuse. […]» (art.6 des statuts
confédéraux)

L’ensemble des mandatéEs confédéraux se sont donc retrouvés dans ce fonctionnement
pour ce dernier mandat de deux ans. Au-delà des échanges quotidiens, trois réunions
ont permis de donner du relief à cette concertation collective en vue d’une meilleure
administration de la :term:`C.N.T` (Octobre 2009, Avril 2010, Octobre 2010).

Il a été maintenu une dissociation de travail :term:`B.C`/CA (deux listes de discussion
internet différentes), considérant que le :term:`B.C` administre les questions
de vie interne, la CA assure tous les mandats «politiques» et techniques.

Le :term:`B.C` est partie intégrante de la :term:`CA` néanmoins.

- Le :term:`B.C` valide les décisions confédérales. Garant du respect des orientations et du fonctionnement il
  assoit ses décisions sur une base de réflexion plus collégiales au sein de la CA. (Secrétariat et Trésorerie
  confédéraux)
- Tous les mandatéEs confédéraux sont dans la CA (Secrétariat international, secteur propagande,
  Bulletin intérieur, Postmaster, Webmaster, MandatéEs intranet, Combat Syndicaliste, Temps maudits)

Un bilan de ce nouveau fonctionnement a été débattu en Avril 2010 en réunion CA. Les syndicats ont reçu à la
circulaire n° 10 trois schémas de proposition de fonctionnement. Ces schémas ont été validés par tous les
membres de la CA présentEs.

La CA et le :term:`B.C` affichent clairement leur souci de voir des candidatures aux mandats mieux préparées, plus
anticipées que lors des Congrès antérieurs. C’est pourquoi ces descriptifs et propositions doivent:

- Permettre aux mandatéEs à venir de pouvoir mieux se projeter dans les tâches et missions possibles sur
  des mandats plus accessibles
- Ouvrir une réflexion sur une meilleure répartition des tâches

Au gré de son expérience et des relations avec les syndicats de la :term:`C.N.T`, la Commission Administrative propose
la création de deux nouveaux mandats supplémentaires en son sein:

- Un secrétariat aux affaires juridiques ayant pour mandat d’assurer une veille et une assistance juridique.
  Il pourra coordonner une équipe de personnes ayant acquise des compétences juridiques ;
- Un secrétariat chargé du développement, de la coordination des campagnes, et de la solidarité
  confédérales.

Par ailleurs, le :term:`B.C` et la CA ont tenu à repréciser les rôles des différents mandats confédéraux en leur sein. Cette
motion en reprend l’essentiel.

Motion
------

Annule et remplace la motion «Organisation du Bureau confédéral» du 25ème
Congrès confédéral des 1, 2, et 3 novembre 1996 à Lyon.


La Commission Administrative se compose des mandatéEs confédéraux suivants:


- Equipe du Bureau Confédéral
- Secrétariat International : Animation et coordination d’une commission internationale regroupant
  plusieurs responsables par secteurs géographiques ou linguistiques, un webmaster, un chargé des
  relations le CS
- Secrétariat à la propagande : Une commission de mandatéEs chargéEs de gérer des besoins en
  matière de matériel de propagande confédéral, d’assurer les tirages et productions nécessaires, susciter
  la réalisation d'affiches tracts, répondre aux propositions reçues des syndicats, assurer la distribution du
  matériel. Elle peut aussi en lien avec la CA :term:`C.N.T` éditer du matériel adapté aux campagnes de la :term:`C.N.T` et
  développer une ligne éditoriale confédérale de brochures ;
- Combat syndicaliste : Comité de rédaction & administration & webmaster ;
- Secrétariat au Bulletin Intérieur : Edition et diffusion
- Temps Maudits : Comité de rédaction & administration
- Secrétariat aux affaires juridiques : Animation et coordination d’une commission confédérale
  juridique. Assure une veille et une assistance juridique pour les syndicats et leurs Unions ;
- Secrétariat mandaté au développement, de la coordination des campagnes, et de la solidarité
  confédérales
- Secrétariat mandaté relations média : Rédaction et diffusion des communiqués confédéraux,
  développement des contacts avec la presse
- Webmaster : ChargéEs de l’animation et de l’administration du site web confédéral, de l’ouverture des
  domaines pour les Unions et syndicats de la :term:`C.N.T`, de l’assistance technique aux syndicats et Unions,
  des relations avec l’hébergeur ;
- Postmaster : Création et gestion des mails confédéraux, création et gestion des listes de discussions
  confédérales, assistance technique aux syndicats et Unions, relations avec l’hébergeur, gestion et
  animation de la liste info contacts ;
- Intranet : Commission de mandatéEs chargées de la création et de la gestion des accès des syndicats et
  Unions à l’Intranet Confédéral, assistance technique aux syndicats et Unions, de la maintenance
  technique de l’intranet confédéral, des relations avec l’hébergeur, de la modération du forum ;


.. image:: _static/organisation_cnt.png


L’Equipe du Bureau Confédéral se compose des mandatéEs suivantEs:

- Secrétariat Confédéral:

    * Secrétaire confédéral : MandatéE de la coordination générale de la Confédération, de la CA,
      et du :term:`B.C`. Assure une veille quant au fonctionnement, aux orientations et au respect des statuts
      de la :term:`C.N.T`. Validation des prises de positions confédérales
    * Secrétaire confédéral adjoint : MandatéE aux labellisations, et de la gestion de l’annuaire
      confédéral des syndicats et des Unions. Aide administrative générale et représentation du
    * Secrétariat confédéral.
    * Secrétaire confédéral adjoint : MandatéE aux relations aux contacts isoléEs et du
      développement, des relations contacts diverses, de l’archivage électronique et l’animation de
      l’intranet :term:`B.C`. Aide administrative générale et représentation du Secrétariat confédéral.



.. image:: _static/secretariat_confederal.png


  
- Trésorerie Confédérale:

    * TrésorierE confédéralE : MandatéEs à la gestion des comptes confédéraux, et la validation
      des engagements financiers
    * TrésorierE confédéralE adjointE : MandatéEs à la mise à jour de l’annuaire confédéral &
      comunications, aux relances et relations avec le secrétariat chargé des labellisations et à la
      gestion de l’annuaire confédéral, à la gestion des prêts confédéraux. Aide et représentation
      du/dela trésorierE confédéralE.
    * TrésorierE confédéralE adjointE : MandatéEs à la gestion des cartes confédérales et de la
      caisse de solidarité. Aide et représentation du/dela trésorierE confédéralE.

  
Les mandatés de la Commission administrative et du Bureau Confédéral sont invités à concerter leur action
régulièrement par tous moyens à leur convenance : réunions, liste de discussion, vidéoconférence, etc.
Ils agissent collectivement dans l’intérêt de la :term:`C.N.T`, dans la fraternité et la dignité que leur suggère leur
mandat. L’échange et le partage des informations sont favorisés vers la mutualisations des productions
respectives, dans la limite des prérogatives propre chaque mandat.

Discussion
==========

SUB69
    Clarifier la motion donc c'est plus un amendement.

	- séparation entre politique et technique: on entend par "politique" = mise
	  en oeuvre des campagnes et clarification des tâches des syndicats.
	  Le Secrétariat confédéral (S.C) doit pouvoir travailler sur des axes tactique,
	  politique et organisationnel.
	  De plus, un B.C efficace ne peut fonctionner que s'il s'appuie sur la C.A
	- intégration d'un secrétariat au développement : importance majeure. On est
	  sollicité de toute part sur des adhésions isolées, des déclarations de section...
	- répression des militants


STE75
    le mandat du BC est un mandat de coordination. On fait confiance aux copains
    du 69 mais on ne votera pas leur contre motion.


SIPM
    pour la motion et contre la contre motion du 69, impression d'un bureau politique
    pour le nouveau BC, sorte de plan quinquennal !

STE93
    faveur de la motion et contre la contre motion, pas de différence à faire
    entre politique et technique et centrer sur une seule ville les mandatés = danger.
    Le seul mandat du BC est la coordination c'est-à-dire de contrôler que les
    syndicats appliquent les décisions prises en congrès.
    La référence à la FTE dans la contre motion n'est pas pertinente.

SUTE 69-01
	La question de la confiance est très bien mais ne suffit pas. Dès qu'il y a
	des tensions cette confiance est remise en cause. On trouve mieux et plus
	simple de pouvoir agir sur des choses déterminées dans le cadre d'une motion.
	Notre contre motion ne s'oppose pas à la motion du Gard.
	3 changements:

	- intégrer le secrétariat au développement et le secrétariat à la communication
	  extérieur dans le BC et pas dans la CA
	- changement des tâches du BC de deux à cinq personnes.
	- Idée de concentrer le SC sur la même ville pour plus d'efficacité et se
	  voir chaque semaine mais ce n'est pas écrit dans la motion, on verra ça
	  sur les mandatements.

ETPIC30
    complètement opposé à ce que les 5 SC soient sur la même ville. C'est pas
    une réunion hebdomadaire qu'il vous faudra c'est 3 à 4 heures par jour.

ESS34
	Toujours mieux qu'avant quelque soit la motion adoptée car plus de deux SC.
	Favorable à l'augmentation du nombre de gens dans le BC + à l'augmentation
	du nombre de syndicats (avant deux et avec la motion du sub69 trois syndicats).
	Être à côté c'est pas délirant (+ pratique) voir pour le SF de la fédé.
	On est pour qu'au moins une des deux motions soit adoptée.

STICS38
    merci à l'ETPIC30 pour mettre en place quelque chose de pérenne sur le boulot
    du BC.

STE57
	contre la motion de Lyon (centralisation géographique) contre la motion du
	Gard à cause du secrétariat au développement.

SIPM
	Problème politique et syndicale sur ces motions. Le cadre du BC c'est nos
	statuts. Les membres de la CA ne sont pas là pour faire la besogne syndicale
	et le BC la part politique !

SUTE 69-01
	Ne pas confondre la contre motion et les mandatements. La concentration
	géographique sur une même ville n'est pas écrite dans la contre motion.
	Je renvoie au compte-rendu du S.C sortant disant que les mandats politiques
	doivent être regroupés.

Educ 38
    Réserve sur la contre motion car seule une grande ville pourrait prendre
    ce mandat.


.. _vote_motion_15_2010:

Vote de la motion 15 [rejetée]
==============================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°15          | 27         | 15       |  14        | 9                         |
| Etpic 30 Sud         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

**Motion n°15 rejetée**


.. seealso::

   - :ref:`contre_motion_15_2010_sub69`
