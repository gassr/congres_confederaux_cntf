
.. _contre_motion_24_2021_tasra:

=====================================================================================================
Contre-motion N°1 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **TASRA**
=====================================================================================================

- :ref:`motion_24_2021_ste33_cnt42`

Autres motions TASRA

    - :ref:`rhone_alpes:motions_tasra`


Argumentaire
================

Notre syndicat propose une autre version du protocole pour que, conformément
à notre histoire et à nos statuts, les syndicats retrouvent la place qui
leur incombe.

Nous nous sommes également attachés à **intégrer explicitement** des principes
que nous considérons comme primordiaux dans une  **instruction**, comme
le **respect du contradictoire**.

Nous espérons que ce protocole pourra évoluer avec le temps, au fil des
expériences, car il ne sera surement pas pleinement satisfaisant.

Il a simplement le mérite de poser un cadre qui nous apparait préférable
à la motion initiale, des bases qui nous semble être les bonnes pour avancer.


Contre-motion
===============

**GESTION INTERNE DES VIOLENCES SEXUELLES (AGRESSION, HARCELEMENT, VIOL)**

Protocole qui vise à établir des outils et une procédure à suivre dès
qu'une accusation de violence sexuelle est portée à la connaissance de
la confédération contre un syndiqué.

Principes directeurs du protocole confédéral :
------------------------------------------------

- Conformément à notre histoire et à nos statuts, le syndicat est la
  structure de base pour les affaires internes. 
  Le protocole confédéral n'intervient qu'à la demande du syndicat ou
  de la personne dénonçant des faits de violences sexuelles si elle
  considère que le syndicat est défaillant sur cette question.
- La confédération n'étant pas une somme d'individus mais une **fédération
  de syndicats**, seule la responsabilité du(des) syndicat(s) peut être
  engagée au niveau confédéral.
  La responsabilité des syndiqués ne pouvant être engagé que par les
  syndicats concernés.
- Toute personne accusant de viol et/ ou d'agression sexuelle doit être
  protégée contre toute mesure répressive - de quelque nature que ce soit -
  pour avoir, de bonne foi, relaté ou témoigné de faits.
- **La procédure doit répondre aux principes du contradictoire, de l'impartialité
  et de la transparence**. Tout avis rendu doit être motivé et circonstancié. 
- La CNT, à travers le :term:`BC`, doit, en cas de besoin, se donner les
  moyens matériels, moraux et financiers pour le fonctionnement et le
  déroulement normal du protocole ainsi que pour soutenir toute personne
  accusant de viol et/ou d'agression sexuelle un membre de la CNT.


Procédure
----------

Soutien au recueil d'accusation
+++++++++++++++++++++++++++++++++++++

Au niveau confédéral, la CNT met en place une liste de personnes référent.es
compétent.es sur les questions des violences sexuelles. 

Cette liste est mise à disposition de tout.es les adhérent.es de la CNT
et actualisée quand nécessaire. Ces référents sont désignés par leurs
syndicats et/ou Union régionale.

Chaque référent est susceptible d'être contacté par une personne qui
cherche à exposer des faits de violences sexuelles.
Pour le recueil de son témoignage, la personne peut obtenir la mise en
place d'une cellule d'écoute et de soutien.


Suite de la procédure
+++++++++++++++++++++++

Une commission extraordinaire est mise en place pour instruire le signalement.

Les membres de cette commission extraordinaire sont choisis parmi la
liste des référents. Dès sa constitution, la commission extraordinaire
informe le syndicat local de l’accusé, la victime, et l'accusé lui-même,
des accusations portées contre ce dernier.

La commission extraordinaire a pour mission d'instruire l'affaire en appui
du syndicat concerné, notamment par le recueil de la parole de la victime,
de tout autre témoignage et de toutes pièces portées à sa connaissance.

La commission extraordinaire doit veiller au respect des principes du
contradictoire et notamment faire part sans délais, à l'accusé et à
l'accusateur, des éléments portés à sa connaissance.
Dans le même sens, l'accusé et l'accusateur doivent être à même de porter
à la connaissance de la commission extraordinaire tout élément afin
d'éclairer cette dernière.

Une fois la parole de la victime recueillie, la commission extraordinaire
rend ses conclusions aux référent.es.
Les référent.es et la commission extraordinaire se réunissent alors et
rendent un avis sur la réalité des faits par rapport à la plainte initiale
au syndicat concerné. Ce dernier est le seul à décider des suites à donner.

A l'issue de la procédure, si la commission extraordinaire considère
être en présence d'une défaillance avérée, grave et persistante dans
la gestion de l'affaire par le syndicat concerné, elle établit un rapport
circonstancié afin d'appuyer, dans le cadre des procédures statutaires,
une mise en œuvre de la responsabilité du syndicat devant la confédération.


Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°24 |            |          |            |                           |
| TASRA                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
