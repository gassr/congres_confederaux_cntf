
.. _contre_motion_15_2021_cnt30:

=====================================================================================================
Contre motion à la motion n°15 2021 **création de boîte mail en cnt-f.org** par **ETPIC30**
=====================================================================================================

.. seealso::

   - :ref:`motion_15_2021_syndicats_42`

Autres motions ETPIC30
========================

.. seealso::

   - :ref:`motions_etpic30`


Contre motion
=============

La motion présentée par nos camarades de quatre syndicats de la Loire
demande à la Confédération de mettre à disposition des moyens
confédéraux pour les groupes auto-labellisés **CNT Femmes libres**.

Nous ne reviendrons pas sur l'absence de cadre de reconnaissance de ces
groupes dans le pacte confédéral, nous l'avons développé dans notre
:ref:`contre motion à la motion n°14 2021 <contre_motion_14_2021_cnt30>`.

L'attribution de mails confédéraux à ces groupes est évidemment dépendante
de cette reconnaissance.
Cette contre motion vise à clarifier une toute autre problématique,
plus globale, indépendante de la demande initiale à laquelle le
Bureau confédéral mandaté en 2016 a apporté une réponse que nous
avons approuvée.

L'attribution de mail @cnt-f.org a déjà fait débat au sein de la Confédération.
Ce fut notamment l'objet d'un arbitrage de sens lors d'un CCN CNT en 2009,
notamment s'agissant de maintenir ou non des mails existants du type perso@cnt-f.org.

Abordons un premier niveau de constat que nous pouvons communément partager:

- L'usage et la création des mails@cnt-f.org est le fait de la seule CNT
  française dite « Vignoles », en tant que Confédération, qui en détient
  les droits ;
- L'usage et la création de ces mails sont payants et entrent dans un
  forfait limité numériquement avec notre hébergeur ;
- L'usage individuel ou collectif d'un mail@cnt-f.org suppose une
  adhésion effective à la CNT ;
- L'usage des adresses mails@cnt-f.org laisse à penser aux destinataires
  de leurs courriels que l'expéditeur·trice est rattaché·e à la CNT en
  tant que Confédération ;

La gestion d'un webmail perso@cnt-f.org suppose de pouvoir en répondre
juridiquement.

En 2009, non sans contraindre quelques camarades déjà équipé·e·s
(et ex-camarades, eh oui !), la suppression des mails de type perso@cnt-f.org
a été décidée.

Les raisons qui ont alors présidé à ce choix sont les suivantes :

- La Confédération n'a pas les moyens techniques, humains et financiers
  de répondre aux nombreuses demandes individuelles de créations de mails
  perso@cnt-f.org.
- Les mails @cnt-f.org doivent répondre d'abord aux besoins structurels
  de la Confédération.
- Le développement éventuel d'une plateforme webmail destinée à délivrer
  un service de mails à usage personel devra faire l'objet d'un mandat
  ad hoc, de moyens et d'une terminaison spécifique dédiée ;
- Considérant que l'usage d'un mail@cnt-f.org suppose une adhésion
  effective à la CNT, la Confédération peut seule attester de l'adhésion
  de ses structures adhérentes, statutaires et mandatées par ses soins: syndicats,
  unions, fédérations, mandats confédéraux.
- Plus largement, l'usage public ou interne d'un mail@cnt-f.org doit
  pouvoir garantir aux destinataires des courriels que ces mails sont
  utilisés par des cénétistes dont l'adhésion est effective, la terminaison
  @cnt-f.org laissant par ailleurs entrevoir un rattachement officiel de
  type confédéral.
- Dans la mesure où seuls les syndicats peuvent attester de l'adhésion
  effective d'un·e adhérent·e ou d'un groupe d'adhérent·e·s, le postmaster
  confédéral n'est pas en mesure d'assurer directement ce contrôle
  d'adhésion indispensable.

Nous considérons que ces derniers éléments sont déterminants pour l'examen
de la requête de moyens supplémentaires demandés par ces syndicats CNT
de la Loire.
Aussi, et dans la lignée des décisions prises par le CCN CNT de 2009,
nous demandons à la Confédération d'adopter la contre motion suivante
assortie de son argumentaire::

    La Confédération Nationale du Travail délivre des mails marqués du
    label confédéral du type x@cnt-f.org à l'usage exclusif de ses
    structures adhérentes, statutaires ou mandatées par ses soins :
    syndicats, unions, fédérations, mandats confédéraux, commissions.

    Le développement d'un service webmail dédié à l'usage de tous les
    syndicats, de leurs adhérent·e·s, et de leurs activités connexes
    fera l'objet d'un mandat spécifique dès que les conditions techniques
    le permettront.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°15 |            |          |            |                           |
| CNT30                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
