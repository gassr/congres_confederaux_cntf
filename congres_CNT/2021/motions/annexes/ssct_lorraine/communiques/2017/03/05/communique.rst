

.. _communique_ssct_lorraine_2017_03_05:

==================================================================================================================================================================================
Dimanche 5 mars 2017 **Communiqué du syndicat Santé Social Collectivités Territoriales de Lorraine suite à l'accusation de viol concernant l'un de ses adhérents, Fouad**
==================================================================================================================================================================================



Communiqué
==========


Le 5 mars 2017
Communiqué du syndicat Santé Social Collectivités Territoriales de Lorraine
CNT, suite à l'accusation de viol concernant l'un de ses adhérents, Fouad.

Suite à la communication publique d'Alternative Libertaire, informant le
grand public d'une accusation de viol à l'égard d'un militant de notre
syndicat, **le syndicat SSCT Lorraine CNT tient tout d'abord à dénoncer
avec véhémence la tentative de déstabilisation de notre syndicat et
l'offensive politique que constitue cette communication à l'égard de la CNT**.

En effet, nous tenons à préciser qu'AL, qui a bien eu confirmation officielle
que notre syndicat avait été saisi de cette accusation, et à qui le
Bureau Confédéral de la CNT a précisément expliqué les formes et le
calendrier de notre procédure interne, a **fait le choix de ne pas laisser
notre syndicat mener sa réflexion sur ces accusations, et a préféré lui
mettre publiquement le couteau sous la gorge par le biais d'une injonction
à l'exclusion de notre adhérent**.

À ce titre nous rappelons à Alternative Libertaire que notre syndicat a
toujours défendu son autonomie, et son indépendance vis à vis de toutes
organisations politiques et qu'il ne tolèrera pas, alors même que nos
organisations respectives avaient ouvertes une discussion au sujet de
cette affaire - dans le cadre de laquelle Alternative Libertaire a
toujours refusé de transmettre à la CNT les éléments d'accusation à
l'encontre de son adhérent - cette tentative de mise sous pression
d'Alternative Libertaire **qui fait aujourd'hui, comme nous pouvons le
constater sur la toile, le bonheur des organisations fascistes**.

**Ensuite, nous tenons à condamner avec la plus grande des fermetés, la
vision moyenâgeuse de la justice*** que semble soutenir Alternative Libertaire.

En effet quand bien même notre adhérent accusé s'avèrerait coupable de viol,
nous trouvons que cette pratique de la diffusion publique de son identité
marquée d'infamie et de son lieu de résidence est **digne des pires pratiques
de l'Ancien Régime** et ne relève en rien d'une vision humaniste de
notre société.

Bien au contraire, **notre syndicat estime que cette pratique de la diffusion
publique de l'identité d'un criminel potentiel s'apparente plutôt aux
réflexions auxquelles nous avaient jusqu'ici habituées l'extrême droite
et la droite conservatrice**, et qui rêvent d'un retour à la peine de mort.

A minima, cette communication publique par Alternative Libertaire de
l'identité de l'accusé constitue, et cela de manière très concrète, a
minima la mort social de l'accusé, si ce n'est un appel au suicide, voir
un appel aux représailles.

Par ailleurs et sur le fond de cette affaire, notre syndicat tient à
informer l'ensemble du milieu, qu'il a mis en œuvre une procédure de
recueil d'éléments à charge et décharge à l'égard de son adhérent.

Procédure qui a permis de mettre en lumière que :

- Il n'existait dans cette affaire aucun élément matériel et que l'accusation
  repose sur la parole de la victime supposée et sur des témoignages anonymes
  transmis par des adhérents d'Alternative Libertaire à une commission
  non mixte constituée pour l'occasion.
  Qu'en outre aucune plainte n'avait été déposée par la victime supposée
  malgré le fait que depuis les premiers jours de l'accusation, l'accusé
  se déclare se tenir à l'entière disposition de la police et de la justice.
- Il existait néanmoins plusieurs témoins factuels à décharge, dont nous
  disposons de l'identité et qui ont été auditionnés par notre syndicat,
  et qui **contredisent formellement certaines des déclarations de
  l'accusation et des témoins à charge**.
  Que ces témoins ont subis des **pressions odieuses** de la part
  d'Alternative Libertaire (demandes de modifications de leurs témoignages,
  **injonctions à soutenir la victime supposée)**.
  **Que les déclarations de ces témoins à la commission non mixte d'AL
  n'ont pas été retranscrites** dans le rapport d'instruction interne à AL
  et n'avaient donc pas été portés à la connaissance des adhérent·es
  d'Alternative Libertaire qui ont voté·es son exclusion.
  Que ces mêmes témoins ont souligné ce fait précis dans un courrier
  adressé au Secrétariat fédéral d'AL pour s'en étonner : **courrier qui,
  lui non plus, n'a pas été porté à la connaissance des adhérent·es qui
  ont voté·es son exclusion de l'organisation**.
- Lors de son audition par la commission non mixte d'AL, **notre adhérent
  n'a pu disposer de défenseur**, avait été sommé de **présenter sa défense
  sans même qu'on lui présente les faits qui lui étaient reprochés**.
  Que les éléments de défense qu'il a formulé devant cette commission
  n'ont pas été retranscrits dans le rapport d'instruction interne à AL
  et n'avaient donc pas été portés à la connaissance des adhérent·es
  d'Alternative Libertaire qui ont voté son exclusion.
  Ce qui est également le cas des éléments de défense que l'accusé a pourtant
  fait parvenir par écrit à la même commission.
- Que **dès l'enclenchent de sa procédure interne, AL avait officiellement
  considéré, conformément à ces statuts, l'accusé comme coupable**, et
  assuré le fait que **tous les éléments qui pourraient éventuellement
  remettre en cause la version présentée par la victime supposée seraient
  de fait considérés comme irrecevables**.
  Que la mobilisation de tels éléments de défense seraient de fait
  l'illustration de la culpabilité de l'accusé.
  Ainsi, dans le cadre de sa procédure, AL n'a jamais cherché à analyser
  objectivement ni le propos de l'accusation, ni celui de la défense
  et **a donc demandé à ses adhérent·es de voter l'exclusion de l'accusé
  sur la base d'un rapport totalement orienté à charge**.
- Que le contexte de formulation de l'accusation ainsi que les voix qui
  ont été employées pour que celle-ci parviennent à la CNT sont entourées
  **d'éléments plus que troublants**, liés non seulement à un **affrontement
  idéologique et politique** propre aux organisations Alternative Libertaire
  et CNT, mais également à des **règlements de comptes inter-individuels**.

Aussi, au regard de l'ensemble des éléments, factuels comme contextuels
que notre syndicat a pu récolter, les conclusions de notre procédure
interne pour accusation de viol sont claires:

- L'accusation de viol portée à l'encontre de son adhérent repose sur des
  déclarations de la victime supposée absolument invérifiables et sur
  des témoignages anonymes, qu'AL refuse de nous transmettre, et qui
  sont **contredits par des témoins factuel·les et identifié·es** que nous
  avons pu recevoir.
- Que les éléments à décharge, bien qu'ils restent concrètement invérifiables,
  semblent cohérents au regard des témoignages actuels dont nous disposons.
- Qu'au regard des éléments dont il dispose, et ne disposant pas de moyens
  d'investigations supplémentaires, notre syndicat est totalement
  incompétent pour se prononcer sur la culpabilité ou l'innocence de son
  adhérent.
- Que les présomptions formulées à l'égard de notre adhérent ne sont pas
  suffisamment étayées ou crédibles pour qu’elles puissent conduire à
  son exclusion.

Au regard de cette décision, notre syndicat, profondément attaché aux
valeurs du féminisme et de l'antisexisme, rappelle que les militant.es
de ces causes se sont historiquement battues pour que leur parole soit
prise en compte par la justice.

Aussi, nous considérons qu'il est du devoir de toute organisation politique
ou syndicale qui se déclare de ces luttes et consciente de la difficulté
que peut éprouver une femme à porter plainte pour viol, d'épauler celle-ci
dans ces démarches.

Nous précisions à ce titre que notre syndicat tiendra à disposition de la
justice l'ensemble des éléments dont il dispose - qu'ils soient à charge
ou à décharge - en cas de dépôt de plainte de la victime supposée, et
qu'il proposera à la CNT, sur le plan national de prendre les dispositions
nécessaires pour que la plaignante puisse être épaulée dans le cadre
d'une éventuelle démarche judiciaire.

Finalement, **nous tenons à rappeler à Alternative Libertaire qu'elle
n'est en rien ni garante ni dépositaire d'un positionnement féministe**
et que, plutôt que de faire la leçon à la CNT sur comment elle doit traiter
une accusation de viol, ferait mieux d'appliquer à elle-même ses propres
préceptes et de laver avant tout son propre linge.

Alors qu'à l'écriture de ce communiqué, nous imaginons déjà les attaques
politiques qu'AL formulera à notre encontre ces prochains jours, nous
tenons à rappeler à cette organisation politique qu'elle a elle-même,
il n'y a pas si longtemps, décidé de blanchir deux de ses adhérents, eux
aussi accusés de viols, en raison du manque d'éléments dont elle disposait
alors pour démontrer leur culpabilité.

Que ces deux adhérents alors accusés de viol sont bel et bien toujours
membres, et animateurs de groupes locaux d'Alternative Libertaire.

Aussi, **nous refusons catégoriquement que l'organisation politique qu'est
Alternative Libertaire cherche aujourd'hui à se racheter une crédibilité
aux yeux du mouvement antisexiste, mais aussi aux yeux de tendances
internes qui la composent, sur le dos de la CNT**.

Au regard, non pas du fond de l'affaire concernant l’adhérent, mais de
l'attitude politique d'Alternative Libertaire à notre égard, notre syndicat,
SSCT Lorraine CNT, rompt immédiatement toute relation inter organisation
avec Alternative Libertaire.

Nous précisons finalement, à toute fins utiles que l'ensemble des décisions
prises par notre syndicat concernant son adhérent, ainsi que ce présent
communiqué, n'engagent exclusivement que le syndicat SSCT Lorraine et
non la CNT.

Que notre décision de non exclusion, tout comme nos prises de positions
externes vis à vis d'Alternative Libertaire seront prochainement débattues
par la CNT au niveau Confédéral.
