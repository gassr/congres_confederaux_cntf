
.. index::
   pair: Commission ; Juridique


.. _motion_etpic4_2012:

============================
L’enjeu juridique (motion 4)
============================



La CNT privilégie deux axes principaux d’intervention juridique distincts:

Constitution de commissions permanentes d’entraide juridique
============================================================

La CNT met en place une commission juridique et de formation confédérale.

Elle est composée d’un comité de coordination de mandaté.e.s et personnes
ressources de différentes régions et de différents secteurs d’activités
(du public comme du privé).

Cette commission aura à charge de conseiller, voire assister, l’ensemble des
structures de la CNT et de leurs syndiqué.e.s sur les questions juridiques
(droit syndical, droit du travail).

Elle n’a pas pour vocation de suppléer à l’action et l’investissement des
syndicats eux-mêmes.

Dans l’intérêt de la Confédération, ou pour des affaires recouvrant un enjeu
confédéral, elle est directement sollicitée et mise à contribution. En ce sens,
elle recense et archive l’ensemble des affaires juridiques de nature à assurer
la défense des intérêts de la CNT, de ses syndicats, et de leurs Unions.

- La commission juridique confédérale développe un axe de formations relatives
  au droit du travail et au droit syndical en fonction des demandes et des
  besoins des syndicats et des fédérations.
- Elle publie également des brochures  thématiques.
- Elle rend compte de ses activités à chaque CCN.
- Elle se dote de moyens de communications adaptés et bénéficie de la
  possibilité d’acquérir toute documentation utile.
- La commission juridique confédérale étudie le rattachement de la CNT
  d’organismes de formation habilités sur le plan national ou local à recevoir
  des camarades sur leurs droits à la formation.

En vertu d’une démarche d’entraide et de solidarité interprofessionnelle, le
Congrès invite de même chaque Union Régionale à se doter de leur propre
commission juridique. Elles ont pour vocation d’assurer au plus près conseils
et assistances à l’ensemble des syndicats des Régions et de mutualiser les
compétences locales.


Recours à des prestations juridiques externes
=============================================

Bien que privilégiant les compétences et les outils collectifs internes, les
syndicats et les Unions peuvent être amenés à recourir à l’intervention de
professionnels pour défendre leurs intérêts sur le plan juridique.

En guise d’alternative, la CNT peut promouvoir l’émergence de sociétés
coopératives à vocation juridique avec lesquelles elle développera un
partenariat privilégié.

Lorsqu’il est avéré que les ressources et outils internes sont insuffisants ou
inefficients, la CNT admet le recours à un « permanent juridique »,
exclusivement salarié.

Emploi d’un permanent chargé des questions juridiques
=====================================================

Considérant que l’action juridique doit s’inscrire dans une démarche
inter-corporatiste, seules les Unions Régionales et Confédération ont
compétence pour procéder à cet emploi.

Tout recours au salariat devra répondre des mêmes conditions préalables que
pour les permanents techniques.
