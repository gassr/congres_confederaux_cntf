

.. _contre_motion_9_2012_ptt95:

================================================================================================
Contre-motion à la motion N°9 2012 PTT95
================================================================================================

.. seealso::

   - :ref:`motion_9_2012_interpro31`



Argumentaire
============

Nous soulignons tout d’abord que nous comprenons que des syndicats hors de la
RP se lassent d’un conflit dont ils sont éloignés – en apparence- mais la
solution proposée par le Syndicat Interpro 31 ne répond pas aux véritables
enjeux. C’est pourquoi nous présentons cette contre-motion.

En premier lieu il convient de noter que l’argumentaire de l’Interpro 31 donne
une vision du conflit pour le moins rapide et tire des conclusions générales
pour le moins surprenantes. Il ne s’agit pas d’un conflit entre personnes mais
d’une opposition entre deux conceptions de l’outil syndical à construire:
fonctionnement autogéré ou recours à des permanents salariés, construction
interprofessionnelle ou catégorielle.

De ce point de vue la CNT est confrontée à un débat réel, à moins de faire
l’autruche est de déclarer qu’il faut «l’apaisement» **ce qui signifie
considérer qu’à la CNT chaque syndicat peut développer des orientations
contradictoires.**

L’Interpro 31 effectue un procès d’intention vis-à-s-vis de l’URP en parlant
d’exclusion car aucun syndicat de l’UR n’a fait cette proposition.

Les faits sont simples : une majorité des syndicats de l’URP ne partage pas
la conception du syndicat du Nettoyage de fonctionner en ayant recours à un
permanent salarié, en se servant du local de la Martinique comme d’une entité
autonome pour organiser des syndicats en-dehors de l’URP – alors que l’achat
de ce local a été possible grâce à l’aide de la région et de la Confédération-
en ayant abandonné toute pratique interprofessionnelle alors que pendant
plusieurs années les militants du nettoyage pouvaient rencontrer les militants
d’autres secteurs, agir ensemble, faire en commun des réunions publiques, et
en ayant cessé depuis 2 ans de payer des cotisations régionales.

**Là est le fond du débat et il concerne chaque syndicat de la Confédération.**

Ensuite l’Interpro 31 raisonne par amalgame : Midi-Pyrénées à été suspendue il
y a plusieurs années donc la suspension de l’URP s’impose.

Il faudrait proposer une modification des statuts organiques pour déclarer que
désormais en cas de conflits- nécessairement différents- le même remède doit
s’appliquer.

Mais l’Interpro 31 semble méconnaitre le fonctionnement de la CNT quand il
déclare « la suspension entraine uniquement l’arrêt des activités en tant
qu’Union régionale » comme si cela était de peu d’importance.

On suspend, circulez, y’a rien à voir.

La suspension d’une région a des conséquences extrêmement graves:

- Sur le plan des orientations suspendre les activités en tant qu’UR c’est
  briser le fondement de la construction interprofessionnelle de la CNT et
  imposer une construction catégorielle, base du syndicalisme institutionnel.
  C’est précisément le reproche fait par l’UR au syndicat du Nettoyage.
  Or le rapport d’activités de l’URP montre que malgré les tensions l’URP n’a
  jamais renoncé à ce fonctionnement interprofessionnel qui regroupe la
  majorité des syndicats de l’URP. Chaque mois, chaque semaine des syndicats
  de l’UR se rencontrent et militent ensemble.
- L’Interpro 31 qui dans son argumentaire minimise les conséquences d’une
  suspension de l’URP omet de souligner que la région parisienne « suspendue »
  ne pourra naturellement plus s’exprimer en tant qu’UR lors des CCN, lieu de
  décision de la CNT entre deux congrès. Nous avons des difficultés à croire
  que ce « détail » ait pu échapper à nos camarades du 31.

De plus l’Interpro 31 réalise un véritable tour de passe-passe pour justifier
sa motion : « Pour réaffirmer les positions prises par la Confédération ces
dernières années.. » : quelles positions prises permettent de justifier une
suspension de l’URP ?

En conclusion la proposition de l’Interpro 31 outre son argumentaire
tendancieux, dissimulé sous une pseudo-objectivité – dissoudre l’URP c’est en
fait dissoudre la majorité des syndicats qui fonctionnent encore en UR car les
autres n’y participent plus ou de manière épisodique, voir les compte-rendus
des UR, et c’est le but recherché sans doute- présente l’inconvénient majeur
de donner une réponse administrative à une contradiction politique.

Nous ne croyons pas à ce type de réponse dans la situation présente, par
ailleurs inapplicable car personne ne pourra briser le lien interprofessionnel
qui a permis le développement de la RP et qui le permet encore.

Nous donnerons juste pour mémoire un dernier argument, conjoncturel : suspendre
l’UR c’est-à-dire la dissoudre en tant qu’entité autonome au moment où nous
sommes engagés dans la bataille pour la défense du 33 face à la Mairie de
Paris, sans certitude sur le résultat final, serait non une erreur
d’appréciation mais une faute.


Contre-motion
=============

Le syndicat CNT PTT 95 sur la base des motifs développés dans l’argumentaire
demande au congrès de rejeter la motion de l’Interpro 31 demandant la
suspension de l’Union régionale - Région parisienne.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°9      |            |          |            |                           |
| PTT95                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_9_2012_interpro31`
