

.. index::
   pair: Motion 13 ; 2014

.. _motion_13_2014_interco25:

=========================================================================
Motion n°13 2014 **Reconstruction d'une internationale** (Interco 25)
=========================================================================

Motions interco25

   - :ref:`syndicats:motions_interco25`

Motion
=======

Le congrès de la CNT est convaincu de la nécessité de construire une
fédération internationale des organisations syndicales révolutionnaires
ET anti-autoritaires.

Il affirme que cet objectif ne saurait être atteint par la création
d'une organisation concurrente (de fait) des rassemblements
internationaux existant (AIT, IWW et maintenant Coordination rouge
et noire).

C'est au contraire à une convergence des composantes de notre
mouvement que nous souhaitons travailler, et c'est par la pratique
effective de la solidarité internationale que ce projet pourra émerger.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°13 2014

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
