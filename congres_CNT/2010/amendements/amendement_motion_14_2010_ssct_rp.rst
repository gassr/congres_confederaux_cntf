
.. _amendement_motion_14_2010_ssct_rp:


=======================================
Amendement à la motion 14 (ssct rp)
=======================================


Nous sommes en accord avec le fond de la motion mais pensons qu'il ne faut
pas laisser de possibilité de cumul «en cas de carence».

C'est en général la principale raison qui justifie ce cumul. Nous sommes
assez nombreux et nombreuses pour que les mandats tournent sans cumul.

Par ailleurs, les mandaté-e-s ne peuvent participer au bureau confédéral
que pour deux mandats consécutifs, quel que soit le mandat.

Cette motion (avec l'amendement) est applicable dès le présent congrès.


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°14 |            |          |            |                           |
| ssct rp                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_14_2010`
