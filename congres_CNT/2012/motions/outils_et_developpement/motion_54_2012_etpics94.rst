
.. _motion_54_2012:
.. _motion_54_2012_etpics94:

===========================================================================
Motion n°54 2012 : Locaux confédéraux au 33 (ETPICS 94)
===========================================================================
  

.. seealso::

   - :ref:`etpics94`
   - :ref:`motions_etpics94_2012`



Le 33 rue des Vignoles
======================

- :ref:`33_rue_des_vignoles`

Argumentaire
============

La situation actuelle et l’ouverture qui est offerte à la région parisienne de
négocier avec la mairie un bail de longue durée, sans toutefois devenir
propriétaire, permet avec réalisme d’envisager de disposer d’une surface
militante assez importante pour que nous puissions apporter à la confédération
et à l’UR des possibilités de réponses à nos besoins en locaux.

**Au-delà de l’opportunité offerte il nous est également permis d’affirmer notre
droit sur un lieu historique**.

D’autres régions accèdent à pérenniser leurs lieux de lutte et c’est au
même titre que nous souhaitons conserver le 33 rue des Vignoles. C’est donc
un outil de plus et non pas un choix de centralisation - que nous rejetons
par principe - dont nous désirons doter la confédération.

Motion
======

La CNT décide qu'un prélèvement mensuel sera mis en place d'une valeur de
200 euros (chifre indicatif à discuter par le congrès en fonction des
informations qui seront données par la trésorerie confédérale) de la
trésorerie confédérale vers le compte locaux de la souscription pour le
projet du 33 rue des Vignoles, siège social de la CNT.

L'URP s'engage dans le même temps à assumer l'archivage des documents
confédéraux (y compris international, Combat syndicaliste, etc.)
au 33 rue des Vignoles ainsi que de mettre à disposition ces locaux, de
manière prioritaire, pour des réunions du BC ou de la CA, ainsi que pour
des réunions confédérales (CCN par exemple).



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°54          |            |          |            |                           |
| ETPICS94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
