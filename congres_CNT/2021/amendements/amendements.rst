
.. un·e  employé·e·s ·ices ·es
.. pandoc texte.rst -o texte.html
..  <span class="modif_rouge"> texte  <del>initié</del></span>
..  <span class="modif_rouge"> texte </span>

.. _congres_CNT_2021_amendements:

=========================================
Amendements du congrès CNT 2021
=========================================

- :ref:`cahier_mandatee_2021`

.. toctree::
   :maxdepth: 1


   amendement_motion_1_2021_interpro07
   amendement_motion_2_2021_interpro31
   amendement_motion_2_2021_sest_lorraine
   amendement_motion_2_2021_ste72
   amendement_motion_3_2021_interpro31
   amendement_motion_4_2021_stics72
   amendement_motion_5_2021_sipmcs
   amendement_motion_5_2021_stp67
   amendement_motion_6_2021_cnt42
   amendement_motion_6_2021_ste72
   amendement_motion_6_2021_interpro31
   amendement_motion_6_2021_sanso69
   amendements_motion_7_2021_interpro31
   amendement_motion_7_2021_stics72
   amendement_motion_7_2021_stp67
   amendement_motion_8_2021_ptt_aquitaine
   amendement_motion_9_2021_sest_lorraine
   amendement_motion_9_2021_interpro31
   amendement_motion_9_2021_stics72
   amendement_motion_9_2021_stp67
   amendement_motion_9_2021_tasra
   amendement_motion_10_2021_interpro07
   amendements_motion_10_2021_sanso69
   amendement_motion_10_2021_sipmcs
   amendement_motion_10_2021_ste72
   amendements_motion_10_2021_stp67
   amendement_motion_11_2021_ste72
   amendement_motion_12_2021_interpro31
   amendement_motion_13_2021_interpro31
   amendement_motion_14_2021_sest_lorraine
   amendement_motion_14_2021_interpro31
   amendement_motion_14_2021_stp67
   amendement_motion_15_2021_stp67
   amendement_motion_17_2021_sest_lorraine
   amendements_motion_18_2021_interpro31
   amendement_motion_18_2021_interco69
   amendement_motion_18_2021_stics72
   amendements_motion_19_2021_interpro31
   amendement_motion_19_2021_interco69
   amendement_motion_19_2021_stics72
   amendement_motion_20_2021_ste72
   amendement_motion_22_2021_sest_lorraine
   amendement_motion_22_2021_stp67
   amendements_motion_22_2021_interpro31
   amendement_motion_22_2021_interco69
   amendement_motion_23_2021_sest_lorraine
   amendement_motion_23_2021_stp67
   amendement_motion_24_2021_sest_lorraine
   amendements_motion_24_2021_interpro31
   amendements_motion_26_2021_stp67
   amendement_motion_27_2021_stp67


- :ref:`congres_CNT_2021_motions`
- :ref:`contre_motions_congres_2021`
