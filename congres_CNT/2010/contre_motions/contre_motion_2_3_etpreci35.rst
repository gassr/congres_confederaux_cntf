

.. _contre_motion_2_3_2010_etpreci35:

==============================================
Contre-motion aux motions N°2 et 3 (ETPRECI35)
==============================================


La CNT  décide de  la création  d’une  commission  juridique  et de formation  
chargée  d’intervenir  en soutien aux besoins de cet ordre émanant des syndicats. 

Elle remplace la commission juridique créée au congrès confédéral de 2008. 

Elle est dotée d’au moins 2 mandatés (un en droit du travail privé et un en droit public), 
d’un budget, d’un ou plusieurs numéros de téléphone et d’une adresse mail. 

Elle se  compose de tous les adhérents de la CNT qui désirent mutualiser leur 
savoir et leur expérience.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion aux motions n°2 et 3 |            |          |            |                           |
| ETPRECI 35                         |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2010`
   - :ref:`motion_3_2010`
