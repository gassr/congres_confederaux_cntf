.. index::
   pair: Motion 19 ; 2021

.. _motion_19_2021_ste93:

================================================================================================================================================
Motion **n°19** 2021 **Motion mise en sommeil d’un syndicat** par **STE 93**
================================================================================================================================================

- :ref:`motion_18_2021_ste93`

Autres motions STE93

    - :ref:`paris:motions_ste93`


Préambule
==========

- :ref:`preambule_motions_18_19_2021`


Motion sur la mise en sommeil d’un syndicat
===========================================

Article 1
------------

**Un syndicat est déclaré en sommeil lorsqu’il déclare moins de cinq adhérent-e-s**.

Article 2
-----------

La décision de mise en sommeil est prononcée 3 mois avant le congrès confédéral.

Article 3
----------

Entre deux congrès, le bureau confédéral met à jour la liste des syndicats
répondant aux conditions de participation aux congrès en terme de nombre
d’adhérent-e-s, et a pour mandat, avec l’UR de rattachement, d’accompagner
les syndicats qui seraient moins de cinq adhérent-e-s.


Amendements
===========

- :ref:`amendements_motion_19_2021_interpro31`
- :ref:`amendement_motion_19_2021_stics72`
- :ref:`amendement_motion_19_2021_interco69`

Contre-motions
================

- :ref:`contre_motion_19_2021_stp67`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°19 2021 STE93      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
