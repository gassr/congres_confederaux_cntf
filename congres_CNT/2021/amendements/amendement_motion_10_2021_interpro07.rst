
.. _amendement_motion_10_2021_interpro07:

============================================================================================================================================
Amendement N°2 à la Motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **Interpro07**
============================================================================================================================================

- :ref:`motion_10_2021_etpic30`

Autres motions Interpro07

    - :ref:`rhone_alpes:interpro07_motions_congres`

Argumentaire 
============


Amendement
============

.. figure:: interpro07/amendement_10.png
   :align: center

page 12: rajout en rouge et italique dans l’onglet Juridique:

Juridique : outre l’assistance et le conseil juridique des victimes
déclaré-e-s et plaignant-e-s, les syndicats de la CNT verront à intégrer
dans leurs statuts  respectifs la question de la lutte contre les
discriminations pour leur permettre de se porter le cas échéant partie
civile.

::

    Même s’ils se dotent  d'outil spécifique, les syndicats de la CNT ne
    devront pas hésiter à orienter les plaignantes vers des associations
    d'aides aux victimes.

Page 12 fin de page :

garder uniquement commission confédérale antisexiste et anti-patriarcale
pour l'égalité.

::

    supprimer «et l’équité».



Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°10 |            |          |            |                           |
| Interpro07                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
