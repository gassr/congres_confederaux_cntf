
.. _motions_tresorerie_confederale_cnt_congres_lille_2008:

===========================================================================================================
Motions "Trésorerie confédérale" de la CNT  30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
===========================================================================================================





.. seealso::

   - :ref:`motion_53_2012`


.. _cotisations_confederales_et_repartitions_congres_lille_2088:

Cotisations confédérales et leurs répartitions en 2008
======================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 35         | 4        | 6          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Répartition du Timbre confédéral standard
+++++++++++++++++++++++++++++++++++++++++

Timbre standard : 2,60 euros avec:

- 0,50 euros pour l’international
- 0,50 euros pour solidarité
- 1,60 euros pour la part confédérale.

Répartition  du Timbre précaire : 1,20 euros
++++++++++++++++++++++++++++++++++++++++++++

Timbre précaire : 1,20 euros avec:

- 0,30 euros pour l’international
- 0,30 euros pour solidarité
- 0,60 euros pour la part confédérale.



.. index::
   caisse de solidarité confédérale


.. _caisse_solidarite_grevistes_lille_2008:

Caisse de solidarité confédérale pour les grévistes (Lille, 2008)
=================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 32         | 12       | 19         | 11                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La  caisse de grève confédérale.

Comme son nom l'indique, cette caisse doit servir uniquement **aux grévistes **
(retenues sur salaire constatées) ceci pour que **nous nous donnions sérieusement
les moyens de nous investir dans une grève (générale ou pas),
mot d'ordre central de notre confédération.**

Priorité sera donnée à ceux qui ont des bas salaires.
