
.. _motion_precarite_cnt_congres_lille_2008:


==================================================================================================================
Motions Précarité et réduction du temps de travail 30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
==================================================================================================================

Création d'une Commission nationale chômeurs et précaires
=========================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 29         | 11       | 4          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Afin de stimuler les syndicats sur la question du chômage et de la précarité
dans une optique «lutte de classe», le congrès décide de la création d’une
commission nationale chômeurs et précaires.

Comme son nom l’indique, il ne s’agit pas d’un syndicat ; elle n’est pas
uniquement composée que de chômeurs et de précaires, et ceux-ci n'ont pas
l'obligation d'y participer. La dite commission est sous contrôle des syndicats.

Le travail de la commission consiste à proposer du matériel de propagande,
des campagnes et des conseils juridiques.

Proposition de fonctionnement :

1. Concernant le chômage, la commission peut exercer un rôle de formation
   juridique (radiations, rapports ANPE/RMI, primes de retour à l’emploi,...)
   par le biais de brochures juridiques (ou 4 pages) et l’organisation de
   formations dans différentes villes qui sont demandeuses.
   L’aspect juridique est à accompagner d’un autre discours sur les chômeurs,
   en vue d’une solidarité de classe.
   Enfin la commission aurait mandat à proposer des campagnes (réalisation
   d’affiches, etc), notamment en réaction à l’actualité.
2. Pour ce qui concerne la précarité, étant donné le travail qui est fourni
   régulièrement par les syndicats sur la question, la commission peut dans un
   premier temps synthétiser le matériel existant. Par ailleurs, là aussi elle
   peut proposer des campagnes, en lien avec l’actualité.
3. De manière générale, la commission marcherait mieux si des commissions
   locales/régionales se mettent en place, travaillant sur des sujets précis.
   En ce cas, la commission nationale peut appuyer et compléter ces commissions,
   recenser leur travail et le diffuser pour des villes demandeuses de matériel.

Pour ce qui est de la coordination de la commission, **Internet pourrait être
un outil intéressant**.

La commission peut apparaître en son nom propre "CNT chômeurs et précaires"
mais la participation parallèle à un syndicat est obligatoire pour ses membres.
