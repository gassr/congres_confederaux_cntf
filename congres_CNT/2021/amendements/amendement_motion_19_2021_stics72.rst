
.. _amendement_motion_19_2021_stics72:

===========================================================================================
Amendement N°2 la motion n°19 2021 **mise en sommeil d’un syndicat** par **STICS72**
===========================================================================================

- :ref:`motion_19_2021_ste93`

Autres motions Interpro31

    - :ref:`syndicats:motions_stics72`


Amendement 
============

Article 1 :

    .. raw:: html

        Un syndicat est déclaré en sommeil lorsqu’il déclare moins de cinq
        adhérent·es, <span class="modif_rouge"> sauf s’il est interprofessionnel
        ou bien membre d’une Union Locale ou Départementale.</span>

Article 2
    La décision de mise en sommeil est prononcée 3 mois avant le congrès confédéral.

Article 3
    Entre deux congrès, le bureau confédéral met à jour la liste des
    syndicats répondant aux conditions de participation aux congrès
    en terme de nombre d’adhérent·es, et a pour mandat, avec l’UR de
    rattachement, d’accompagner les syndicats qui seraient moins de
    cinq adhérent·es.»

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement     à la motion  |            |          |            |                           |
| n°19 STICS72                |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
