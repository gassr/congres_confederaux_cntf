
.. _motion_17_2012:
.. _motion_17_2012_etpic30:

==============================================================================================================================================
Motion n°17 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Permanents techniques  (ETPIC30)
==============================================================================================================================================
  
.. seealso::

   - :ref:`etpic_30`



Motion
======

La CNT privilégie l’investissement militant, l’inter-corporatisme, et la prise
en main par tous & toutes des tâches collectives et techniques.

Elle opère une distinction entre les mandats d’ordre techniques et les mandats
d’ordre décisionnels.

En fonction des compétences requises, des besoins, des moyens disponibles,
la CNT peut faire appel à l’intervention de professionnels qualifiés : imprimerie
et sérigraphie, moyens informatiques, achats de matériel manufacturés,
transports, etc.

Convaincue par la nécessité d’un autre modèle société fondé sur **l’égalité
sociale et économique**, la CNT privilégie l’émergence de sociétés coopératives
de production avec lesquelles elle développera un partenariat privilégié.

A tous les niveaux de l’organisation, à l’égard de tous les besoins techniques,
le recours au salariat ou au détachement d’un permanent technique ne peut être
admis que selon des conditions multiples et cumulatives :

- Echec ou incapacité à mobiliser les compétences individuelles ou collectives
  militante;
- Incapacité, difficulté, à recourir aux services d’un prestataire extérieur ;
- Evaluation collective des besoins techniques réels ;
- Priorité données aux besoins interprofessionnels ou inter-corporatistes ;
- Etablissement d’un consensus confédéral sur les niveaux de rémunération, de
  droits aux congés, d’évolution de carrière, de formation, de droits syndicaux
  (y compris au sein de la CNT ;
- Etablissement d’une fiche poste spécifique, détaillée, et proportionnée à la
  tâche à accomplir ;
- La désignation d’un encadrement accessible et disponible à la/au salarié.e ;
- La viabilité financière de la création de poste à long terme.


Vote
====


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°17          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Amendements
============

- :ref:`amendement_motion_17_2012_etpics57`
