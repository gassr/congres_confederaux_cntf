
.. index::
   luttes contre les media de l'Etat et du patronat

.. _contre_motion_59_2010_ess34:

=======================================
Contre-motion à la motion N°59 (ESS 34)
=======================================

Titre : **Lutte contre la propagande du patronat et de l'Etat**.

Conscient du rôle de propagande contre-révolutionnaire joué par les médias
de masse audiovisuels aux mains de l'Etat et du patronat, la confédération
décide de faire du combat contre la vision monolithique du monde véhiculée
par ceux-ci un axe de la lutte anarcho-syndicaliste/syndicaliste révolutionnaire
de la CNT.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 59       |            |          |            |                           |
| ESS 34                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso:: :ref:`motion_59_2010`
