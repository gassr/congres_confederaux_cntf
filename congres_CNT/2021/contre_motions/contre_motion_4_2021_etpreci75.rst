
.. _contre_motion_4_2021_etpreci75:

=========================================================================================================================
Contre-motion à la motion n°4 2021 **Motion Fédéraliste pour une réunification** par **ETPRECI75**
=========================================================================================================================

- :ref:`motion_4_2021_ptt_centre`

Autres motions STE75

    - :ref:`paris:motions_etpreci75`


Argumentaire
================

Étude de la motion du syndicat PTT Région centre
Étude de Michel et Frédéric du syndicat ETPRECI 75.

Ce texte à été proposé, au syndicat ETPRECI 75, et adopté à l’unanimité
des membres du syndicat ETPRECI.

Suite au courrier venant du Syndicat P.T.T Région centre, dont l’intitulé
est* **Appel pour une refondation réunification des C.N.T**.

Cette proposition est très louable, elle est certainement partagée par un
certain nombre d’adhérents-es.
Pour se faire, ce texte réaffirme les principes fondamentaux de la C.N.T.,
qui serait une conception ’une éventuelle réunification.
Nous pensons bien évidemment que si cela pouvait se faire, ce serait une
base commune ‘’ ratifiée par les C.N.T’’ sur une élaboration des propositions
écrites (Plateforme projets des C.N.T.).

Nous pensons que ce projet serait fédérateur, mais cela commencerait
plutôt par un rapprochement unitaire sur les luttes, entreprises,
hors entreprises, sur des thèmes sociaux chers à chacun-es, par une
convergence solidaire unitaire sur le terrain ensemble ; dans le respect
des principes de la C.N.T, éthique, pratique, fonctionnement fédéralisme
de celles-ci.

Bien que certains points de divergences existent sur le point des
élections professionnelles (commission paritaire, CSE, CHSCT…).

La question des sections syndicales d’entreprise et le RSS sont acquis
pour tous. Ceux-ci nous apparaissent comme des points cruciaux de la C.N.T.

C’est en cela que peut être que les C.N.T., pourraient se réunifier.
Michel avec l’aide de Frédéric Syndicat ETPRECI 75.

Paris le 22 /04/2020
À la réunion du syndicat ETPRECI75 Sept/Oct 2020, il s’est avéré que
plutôt qu’une contre-motion nous préférons les termes d’une motion de
synthèse avec la motion du syndicat PTT région centre
‘’Pour une réunification des C.N.T.’’

Contre-motion
===============

Nous réaffirmons les principes fondamentaux de la C.N.T., éthique, pratique,
fonctionnement, et du fédéralisme communiste libertaire.

Le projet fédérateur unitaire sur les luttes (entreprises, hors entreprises,
manifestations sur des thèmes sociaux, par une convergence solidaire unitaire,
sur le terrain).

Bien que certains points de divergences existent, sur le point des élections
professionnelles (commission paritaire, CSE, CHSCT, etc.).

La question des sections d’entreprises et le RSS sont acquis pour tous.
Ce serait la une base commune ‘’ ratifier par les C.N.T.’’, sur une
élaboration des propositions écrites (plateformes, projets des C.N.T.).
Il reste aux syndicats au prochain congrès de l’adopter ou pas.
Restera à la C.N.T. d’en informer toutes les autres C.N.T.

Vive la CNT, Vive le fédéralisme communiste libertaire.

Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°4 |            |          |            |                           |
| ETPRECI75                     |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
