
.. index::
   pair: Fonctionnement; Congrès confédéral


.. _fonctionnement_congres_confederal:
.. _congres_confederal:

====================================
Qu'est ce qu'un Congrès Confédéral ?
====================================

- :ref:`congres_statuts_CNT`


Introduction
============

Le congrès confédéral est ce moment si particulier où tous les syndicats de la
Confédération se réunissent pour dresser le bilan de leur activité et fixer les
orientations pour les deux années à venir.

Rendez-vous essentiel de la vie de la Confédération, le congrès confédéral est
aussi un espace convivial de rencontre et d’échange pour des militant(e)s
qui peuvent habiter loin les un(e)s des autres et vivent parfois des réalités
de terrain très différentes.

«Réussir» un congrès c’est donc donner des bases solides à la vie de la
Confédération pour les mois qui suivent.

**Cela passe par l’implication de tous les adhérent(e)s.**

Cette réussite implique une attention particulière de chacun et de chacune,
une bonne dose de préparation et surtout beaucoup de bonne volonté...

La préparation du congrès
=========================

- :ref:`article_14_statuts`
- :ref:`article_8_statuts`
- :ref:`regles_organiques_elections_BC_CNT`

.. _temps_prevu_congres:

Calendrier de préparation du Congrès confédéral
-----------------------------------------------

Les syndicats peuvent à tout moment envoyer leurs propositions de motions:

- **7 mois avant le congrès** : le bureau confédéral avise les syndicats de la
  tenue du congrès.
- **6 mois avant le congrès** : le CCN de préparation du congrès fixe lieu,
  date, modalités prévues (et éventuellement des thèmes) du Congrès.
  Il désigne les mandatés aux différentes commissions techniques et arrête la
  date limite de réception des motions et le calendrier précis de préparation
  du congrès
- **4 mois avant le congrès** : date limite de réception des motions rédigées
  par les syndicats
- **3 mois avant le congrès** : fusion et/ou rejet éventuels de certaines
  motions, tri, classement, numérotation des motions, élaboration (mise en forme)
  du recueil des motions et envoi du recueil aux syndicats.
- **2 mois avant le congrès** :
  Rédaction des contre-motions et amendements par les syndicats.
  Propositions: ordre du jour et planning des séances (provisoires).
  Candidatures aux mandats confédéraux.
- **1 mois avant le congrès** : envoi du dossier complet aux syndicats,
  recueil motions et contre-motions, ordre du jour.
- **Avant l’ouverture du congrès**:

  * commission de révision des comptes (mandatés+trésorier(e) confédéral(e))
  * commission de contrôle des mandats
  * clôture provisoire de la réception des délégations : liste des assistants
    faite par le :term:`BC` sortant.
  * détermination des mandatés à la table d’ouverture.


Ouverture du Congrès faite par le/la président(e) de la table d’ouverture (1ère séance).


Calendrier motions 2021
-----------------------------

- :ref:`calendrier_motions_2021`


Après le congrès
================

.. seealso::

   - :ref:`article_15_congres`

Les délégués, sans attendre le compte-rendu officiel, rendent compte à leur
syndicat de leur mandat et exposent les décisions arrêtées.

Les syndicats sont responsables de leurs décisions, en particulier de la mise
en oeuvre des campagnes confédérales.

Les Comités Confédéraux Nationaux (:term:`CCN`) et le :term:`bureau confédéral`
sont chargés de veiller à l’application des décisions de congrès.
