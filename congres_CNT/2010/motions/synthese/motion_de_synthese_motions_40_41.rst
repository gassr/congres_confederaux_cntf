
.. index::
   pair: Synthèse ; motions 40 et 41 (Motions)



.. _synthese_motions_40_41_2010:
.. _motion_synthese_des_motions_40_41_2010:

==============================================================================================================================================
Motion de synthèse des motions 40 et 41 2010 sur les créations de fédérations d'industries et la stratégie de développement syndical [adoptée]
==============================================================================================================================================



Proposée par:

- le SAM éduc 13,
- le STE 93
- le SIPM RP
- l'ETPICS 57 nord sud,
- l'interco 57 Forbach- Moselle Est
- et l'ETPIC Gard

Considérant que la loi du 20 août 2008 impose les conditions d'ancienneté
de  deux ans d'une structure syndicale sur les champs géographique et
professionnel  de l'entreprise où elle souhaite s'implanter, la CNT
s'efforce de créer les  fédérations d'industries nécessaires afin de
couvrir toutes ses futures  implantations, en regroupant les syndicats
et syndiqués relevant de ces champs d'industries.

A ce jour, la CNT identifie 4 fédérations d'industries à créer
prioritairement:

- une fédération des transports et de la logistique
- une fédération de l'industrie automobile et de la métallurgie
- une fédération du commerce et des services
- une fédération des productions et transformations industrielles,

dénominations qui restent bien sûr à débattre lors du congrès fondateur
de  ladite fédération.

Chaque fédération fera l'objet d'un congrès fondateur où seront définis
les statuts et les champs professionnels de syndicalisation, et
mandatés les camarades qui les animeront.

Ces fédérations n'auront aucune existence réelle au sein de la CNT tant
qu'elles  n'auront pas été labellisées par un CCN ou un congrès
confédéral.


.. seealso::

   - :ref:`motion_40_2010`
   - :ref:`motion_41_2010`
   - :ref:`etat_motions_congres_CNT_2010`
