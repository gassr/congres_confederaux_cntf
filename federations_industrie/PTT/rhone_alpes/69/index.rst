
.. index::
   pair: PTT ; 69

.. _ptt_rhone:

===============================
PTT Rhône (69)
===============================

.. seealso:: http://www.cnt69.org/index.php/Cnt-ptt

Adresse
=======

	Syndicat CNT-PTT Rhône (69)
	44 rue Burdeau
	69001 Lyon
	Tél. : 04 78 27 05 80

:Courriel: cnt.ptt69@cnt-f.org
