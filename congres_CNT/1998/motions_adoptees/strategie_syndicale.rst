

.. _motion_strategie_syndicale_cnt_congres_paris_1998:

======================================================================================
Motion "Stratégie syndicale" 26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
======================================================================================




Boycott des élections universitaires
====================================

Vote
----

47 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 31         | 1        | 6          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Les différents conseils de gestion des universités : Conseil d'administration
(C.A), Conseil des études et de la vie universitaire (C.E.V.U.), et C.R.O.U.S.
ont été créés après Mai 68 dans le but de détourner et d'affaiblir les luttes
étudiantes. Ils se caractérisent par plusieurs éléments :

- Les élus étudiants sont minoritaires dans ces conseils. Ce qui signifie
  qu'ils sont là pour servir de caution à l'administration et qu'ils n'ont
  aucune possibilité d'influencer les décisions de ces conseils.
- Ces conseils n'ont de toute façon que des pouvoirs limités, puisqu'ils ne
  peuvent que repartir les moyens budgétaires et de personnels que leur accorde
  le ministère de l'éducation.
  Véritables organes de cogestion, leur rôle sont de gérer la pénurie
  budgétaire et d'appliquer les reformes visant à la privatisation de
  l'enseignement supérieur.
- Les membres de ces conseils sont élus pour 2 ans, sans mandats et sans
  aucune possibilité de contrôle ou de révocation par leurs électeurs.
  Soit un mode de fonctionnement totalement contraire aux principes de
  démocratie directe que défend la C.N.T.

En conséquence, les sections Formation Action Universitaire (F.A.U.) de la
C.N.T. affirment leur condamnation déterminée de toute participation à ces
conseils et appellent au boycott de ce type d'élections.

Nous précisons qu'aucun argument "technique" ne saurait justifier la
participation d'une section F.A.U. à ces élections : Tout membre d'une
université pouvant assister librement aux réunions de ces conseils et consulter
leurs comptes-rendus.

Cette motion n'est que l'aboutissement et la confirmation de notre pratique
dans les universités, à savoir la dénonciation de la cogestion et de cette
parodie de démocratie que sont ces élections.

Nous ne saurions déroger à ces  principes qui sont un des fondements essentiels
de notre identité.
