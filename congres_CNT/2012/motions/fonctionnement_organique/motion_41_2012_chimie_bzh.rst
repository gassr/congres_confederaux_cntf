
.. _motion_41_2012:
.. _motion_41_2012_chimie_bzh:

===========================================================================
Motion n°41 2012: Labellisation des fédérations d’industrie (Chimie Bzh)
===========================================================================
  

.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`





Motion reprise de SIPM-RP , 2010
=====================================

- :ref:`motion_7_2010`

Argumentaire
============

Si la CNT décide de créer légalement des fédérations aptes à désigner des
représentants de sections syndicales, il est nécessaire de mettre en place
une labellisation des fédérations, afin de savoir quelle fédération existe
réellement au sein de la CNT et peut donc s’y exprimer, par exemple en étant
présente au CCN.

Les fédérations non labellisées n’auront alors vocation qu’à pouvoir créer
des sections syndicales.

Quant aux fédérations déjà existantes, on considère qu’elles sont, de fait,
labellisées.

Motion
======

Une fédération d’industrie nouvellement créée doit être labellisée par un CCN
(ou un congrès confédéral), en suivant la même procédure qu’un syndicat.

Elle doit être composée d’au moins trois syndicats distincts dans la branche
d’industrie ou le secteur considéré.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°41          |            |          |            |                           |
| Chime-Bzh            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
