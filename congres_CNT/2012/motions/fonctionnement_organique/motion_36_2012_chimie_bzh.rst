
.. _motion_36_2012:
.. _motion_36_xxxii:

==============================================================================
Motion n°36 2012 : Partenariat pour la formation aux adhérentEs  (Chimie Bzh)
==============================================================================
  

.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`
   - :ref:`motion_34_2012`



Motion
======


Le congrès mandate une commission « Institut de formation » pour envisager et
évaluer un partenariat avec une association ou un organisme permettant aux
adhérents de la CNT de se former sur le temps de travail.

La Confédération sera tenue régulièrement informée des démarches entreprises
et des engagements à prendre

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°36          |            |          |            |                           |
| Chime-Bzh            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Conclusion
===========

.. seealso::

   - :ref:`motions_34_35_36_xxxii`
