

.. _contre_motion_1_2010_stp72:

================================================
Contre-motion à la motion N°1 (STP 72) [adoptée]
================================================


Argumentaire
============

La question des salarié-es d'un syndicat mérite d'être posée à part entière, avec
des débats argumentés, en ignorant le cas spécifique de la CNT nettoyage.

Amendement / Contre-proposition
===============================

La motion n° 1 ne traite que du cas pratique de la CNT nettoyage dont elle
cherche à « normaliser » la situation compte tenu des positions actuelles de
la CNT à propos des salarié-es et permanents syndicaux. En aucun cas, la
motion ne peut prétendre résoudre de manière définitive la question de la
présence des permanents et salarié-es à la CNT, question qui mérite un débat
spécifique, contradictoire, et que les échanges de courriers et courriels au
sujet de la CNT nettoyage n'ont pas épuisée.


Vote indicatif
==============

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 | 36         | 15       | 11         | 5                         |
| STP 72                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


.. _vote_definitif_contre_motion_1_2010_stp72:

Vote définitif contre-motion à la motion N°1 (STP 72) [adoptée]
===============================================================

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 | 46         | 11       | 9          | 6                         |
| STP 72                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


**Contre motion adoptée**
