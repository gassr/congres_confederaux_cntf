

.. _motions_structuration_confederale_cnt_congres_lille_2008:

==============================================================================================================
Motions "Structuration confédérale" de la CNT  30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
==============================================================================================================

Elaboration de l'ordre du jour du CCN
=====================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 26         | 14       | 5          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Chaque U.R de la CNT peut déposer les points à l'ordre du jour aux CCN.

En cas de carence avérée ou d'inexistence d'UR, les syndicats sont autorisés à
déposer des points à l'ordre du jour aux CCN sous contrôle du BC.


Dans le cadre de son mandat, le BC peut de même et par lui-même inscrire des
points d'ordre du jour en lien avec les mandats confédéraux en cours
(ou non assurés), les commissions confédérales, le fonctionnement interne
(notamment fédéral), les campagnes à développer sur la base d'un argumentaire
détaillé.



Des locaux syndicaux pour les syndicats CNT
===========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 29         | 3        | 9          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La CNT est consciente que les locaux syndicaux sont une nécessité pour ses
syndicats tant pour les luttes syndicales et sociales que pour mettre en œuvre
immédiatement des formes concrètes d'alternatives anticapitalistes et
autogestionnaires.

Malheureusement, en 2008, tous les syndicats CNT ne disposent pas de ces locaux.
La CNT choisit donc comme l'un des axes prioritaires de son développement,
de s'organiser pour aider financièrement et techniquement à l'émergence de
ces lieux.

Une commission est créée, chargée d'étudier les différentes réalités locales,
les besoins et les volontés de chaque ville, département ou région.

Cette étude servira ensuite a élaborer des propositions concrètes pour aider à
l’émergence de ces lieux.

Ces idées seront exposées et rediscutées en CCN avant leur mise en œuvre.


.. _motion_reactivation_commision_juridique_2008_lille:

Réactivation d'une commission juridique confédérale, Congrès de Lille, 2008
===========================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 26         | 14       | 5          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le but de la commission juridique confédérale sera :

- Autant que possible, de répondre aux questions émanant des syndicats dans le
  domaine juridique afin qu'aucune demande ne reste sans réponse.
- **D'organiser la centralisation des dossiers juridiques** traités par les
  syndicats, sous formes de comptes-rendus succincts.
- De mettre en relation les syndicats entre eux (mutualisation des expériences,
  entraide)
- D’organiser la base de données juridiques (réglementation) de produire des
  outils de formation.

Les modalités
+++++++++++++

Elle permettra d'offrir un contact réel pour renseigner, répondre aux questions
juridiques et orienter les syndicats en difficulté, avec et au-delà des outils
disponibles tels que l'intranet confédéral.

Pour un meilleur suivi et un contact plus facile, ETPRECI 35 propose qu'un
syndicat référent soit en charge de cette commission, sans exclure tous les
militants volontaires.

Il n'est pas nécessaire que cette commission regroupe uniquement des gens
compétents au préalable en matière juridique, puisqu'il s'agit principalement
de coordonner et référencer les expériences et non d'assumer toutes les tâches
juridiques pour l'ensemble de la Confédération.

Mise en oeuvre
++++++++++++++

Si le but est clairement d'éviter de former une commission d'experts qui ne
correspondrait pas à nos principes et nos pratiques, normalement, les modalités
peuvent tout à fait prendre une forme différente de celle que nous proposons.

C'est pourquoi ETPRECI 35 suggère la mise en place dés le début du congrès,
d'un groupe de travail qui aurait pour rôle d'affiner cette proposition et,
s'il y a lieu, de rédiger une motion de synthèse entre les différentes motions
susceptibles d'être soumises sur le même thème.

Ainsi on pourrait éviter de reléguer en fin de congrès ou aux calendes grecques
le vote de cette motion qui nous semble essentielle.

.. seealso:: :term:`Secrétariat Juridique`


Péréquation des frais de transport entre les délégations de zones / UR
======================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 32         | 6        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

1. En vue de plus d’équité financière, il est réalisé, avec l’aide de la
   trésorerie confédérale si nécessaire, lors de chaque C.C.N une péréquation
   des frais transports de chaque délégation d’Union Régionale présente.
   Afin de favoriser la venue et la collégialité des délégations, cette
   péréquation peut intégrer jusqu’à 2 déléguéEs mandatéEs par Union Régionale.
2. Le Congrès invite les Unions Régionales constituées à mener une réflexion
   en vue d’une évolution éventuelle de la péréquation des frais de transport
   des délégations d’U.R. présentes en C.C.N. (si adoptée) à l’ensemble des
   U.R constituées présentes comme absentes.


Il est attendu une position, de préférence motivée, de chaque Union Régionale
pour le 2ème C.C.N. suivant le Congrès confédéral.

En cas d’unanimité des délégations d’UR présentes, un nouveau type de
péréquation pourrait être adopté.

Dans le cas contraire, il appartiendra éventuellement au prochain Congrès
Confédéral souverain de s’en saisir.
