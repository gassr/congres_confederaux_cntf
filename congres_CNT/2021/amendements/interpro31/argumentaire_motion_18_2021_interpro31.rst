

.. _argumentaire_motion_18_2021_interpro31:

===================================================================================================================================================================
Argumentaire complémentaire à nos amendements à la motion n°18 « Procédure de la labellisation » du STE 93 et réponses à nos camarades du syndicat CNT PTT Centre
===================================================================================================================================================================

- :ref:`motion_18_2021_ste93`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


Introduction
============

Pour commencer, et c'est étonnant de se sentir obligé·e·s de le rappeler,
nous sommes évidemment, en tant que syndicalistes d’action directe, pour
la généralisation des syndicats d’Industrie, que ce soit pour des raisons
revendicatives, comme pour des raisons révolutionnaires.

Le débat et la réflexion que nous nous proposons de provoquer par notre
amendement (car nous sommes pratiquement sûr·e·s que notre amendement
ne vas pas passer), n’est pas ici.

Historiquement, après la rechute des effectifs au sein de notre
Confédération dans les années 70, notre organisation syndicale s’est
patiemment reconstruite à travers la création et le développement
de syndicats intercorporatifs (souvent non départementaux) dans les
grands centres urbains.

Les camarades considéraient, au vu de nos effectifs d’alors, la
construction de syndicat d’Industrie comme prématurée.

Il suivait en cela la stratégie de la CNT espagnole qui interdit les
syndicats d’Industrie de moins de 25 adhérentEs (il y a même en ce
moment un débat pour remonter ce chiffre à 40).

Encore aujourd’hui la CNT-CIT, qui est pourtant beaucoup plus importante
numériquement et qualitativement que notre confédération, compte une
majorité de « sindicatos de oficios varios » (interpro).
Même à Barcelone, la CNT préfère garder un syndicat de oficios varios
de plus de 200 adhérent·e·s que se diviser en syndicats d’Industrie.

Par ailleurs, le champ géographique des syndicats de nos camarades ibériques
n’y est pas départemental (provincial dans l’État espagnol), mais local.
L’idée étant de faciliter, grâce à un champ géographique à échelle
humaine, l’implication et l’entraide des adhérent·e·s dans le syndicat.

Revenons à la situation française. Suite à la scission de 1993, et le
développement que nous avons ensuite connu, ont commencé à fleurir
des syndicats d’Industrie départementaux ou régionaux, en région
parisienne, puis en province.

Parfois des syndicats interco dynamiques ont été cassés en 2, 3, voire
plus de syndicats. Dans notre contexte de développement syndical continu
dans les années 1990’ et début 2000’, cette stratégie a pu être pensée
comme un moyen d’appuyer le développement en cours.
Et ce en attirant des travailleur·euses qui n’auraient peut-être
pas adhéré à la CNT sans syndicat de branche, en facilitant un travail
syndical spécifique de branche, et en permettant la création de
Fédérations d’Industrie.
Cette stratégie a parfois relativement bien marché.
Mais de nombreuses fois, des syndicats de branche (et les syndicats
interpro ainsi désertés) ont végété, et n’ont jamais atteint
le seuil minimum pour avoir et/ou maintenir une vraie vie de syndicat.

Alors pourquoi notre motion.

1- Le but n'est pas d'empêcher l'implantation ou le maintien
embryonnaire de syndicats CNT dans tel ou tel département. Notre
amendement est très clair là-dessus. Mais nous assistons, selon nous, à
une hyperinflation contreproductive du nombre de syndicats au sein de
notre confédération, qui sont parfois des coquilles relativement vides.
Le but de notre amendement est donc, dans des départements où nos
syndicats sont particulièrement petits, de ne pas les encourager à
exploser en syndicats encore plus groupusculaires. Nous pensons que ces
explosions en micro-syndicats de branche, cassent ou empêchent des
dynamiques collectives nécessaires à notre développement, nuisent à
l'organisation effective de l'entraide syndicale revendicative,
matérielle et juridique ; freinent notre capacité à développer nos
activités syndicales culturelles, de réflexion économique et de loisirs
; créent une hyperinflation du nombre de structures, de mandats et de la
bureaucratie ; et font obstacle à la rotation des mandats.

2- Par ailleurs, dans la situation actuelle, on arrive parfois à la
situation ubuesque de certains départements où il existe des syndicats
d'Industrie spécifiques sans syndicat interpro, ou alors des
départements où il existe des syndicats d'Industrie actifs, mais où le
syndicat interpro est laissé quasiment à l'abandon alors même qu'en
province, il couvre souvent l'essentiel des champs professionnels
(Industries et Tertiaire).

3- Nous le répétons, nous pensons qu'il est important de permettre la
création de syndicats dans les départements où nous ne sommes pas
présent·e·s, même de 2 adhérent·e·s. Mais nous ne sommes pas dupes. La
vie démocratique dans nos syndicats et unions de syndicats, implique le
débat critique et collectif, possible uniquement par la présence active
d'un nombre minimum d'adhérent·e·s en AG de syndicat. Selon nous, seules
ces conditions permettent qu'un vote de syndicats dans des réunions
souveraines d'union de syndicats, comme les congrès confédéraux,
représente vraiment une position collective, et non les positions
personnelles d'un ou deux individus portant un micro-syndicat.

4- Cet amendement est motivé par le bilan qu'on tire de notre propre
expérience syndicale, notamment depuis 2 ans et la renaissance modeste
de la CNT en Haute-Garonne. Notre syndicat CNT interprofessionnel
regroupe pour le dernier trimestre de 2020, 22 adhérent·e·s. Nos
camarades du syndicat CNT Santé Social sont elleux 15 adhérent·e·s pour
le dernier trimestre de 2020. Depuis le paiement de nos cotisations
confédérales, nous nous sommes encore développé·e·s, et nous allons
normalement rapidement passer la barre des 50 adhérent·e·s dans notre
Union Départementale. Et même avec ce nombre, malgré une très grande
implication militante, une très bonne dynamique collective, et une
organisation interne de plus en plus rigoureuse, nous peinons à porter
nos syndicats et notre UD. Pour cela, certains camarades de nos 2
syndicats posent même la question de la fusion de nos deux syndicats,
question qui nous reste à débattre et à trancher.

La dernière jurisprudence qui fragilise notre solidité juridique en cas
de nomination de RSS ou DS, ou de présentation de liste aux élections
professionnelles, par des syndicats interpro, ne nous a été communiquée
qu'après notre proposition d'amendement. Mais la solution n'est pas pour
nous forcément de créer des groupuscules syndicaux d'Industrie. Cela
peut être de mettre l'énergie à faire (re)vivre nos fédérations, Unions
Régionale, notre confédération et passer, **lorsque les syndicats
d'Industrie font défaut, par ces unions de syndicats pour nommer nos
RSS/DS et/ou présenter des listes CNT aux élections professionnelles.**

**Nous sommes demandeuse·eurs de retours des camarades d'autres Unions
Départementales sur leur expérience de division de leur syndicat
interpro en plusieurs syndicats de branche de moins de 10 adhérent·e·s.
Quel bilan syndical en faites-vous ? Comment fonctionnez-vous entre
syndicats ? Combien arrivez-vous à être en AG de syndicat et en AG d'UD
? Quelles sont les conséquences positives et/ou négatives de cette
stratégie que vous avez pu observer ?**

Pour finir, nous voulons revenir brièvement sur les accusations de nos
camarades du syndicat CNT PTT Centre. Les camarades sous-entendent que
nous avancerions peut-être avec cet amendement à « visage masqué » pour
comploter en vue de « faire de la Confédération Nationale du Travail, de
ses syndicats, une Nième organisation spécifique, affinitaire,
"anarchiste" qui s'exclurait elle-même du monde du Travail. ».
Premièrement nous ne voyons pas le rapport entre notre amendement et
l'affinitarisme anarchiste. Deuxièmement l'affinitarisme anarchiste n'a
évidemment aucun rapport avec notre pratique syndicale cénétiste.
Troisièmement, on remarque, et ce n'est pas nouveau dans la vie
confédérale, que les leçons de syndicalisme « vous êtes des faux
syndicalistes », sont utilisées à des fins polémiques pour esquiver les
débats de fonds, et/ou les bilans concrets d'expérience. Cher·e·s
camarades du syndicat CNT PTT Centre, et nous vous le demandons avec
toute notre camaraderie, avez-vous vraiment besoin pour défendre votre
point de vue, de tomber à la fin de votre texte dans ce genre de procès
d'intention gratuit ? Nous trouvons, en tout cas de notre côté, que cela
nuit à votre argumentaire.

Salutation anarcho-syndicaliste et syndicaliste révolutionnaire
