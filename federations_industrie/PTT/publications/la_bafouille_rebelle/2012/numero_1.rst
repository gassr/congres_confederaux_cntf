



.. _bafouille_rebelle_1:

===========================
La bafouille rebelle N°1
===========================

.. seealso:: http://www.cnt-f.org/fedeptt/spip.php?article121

Page 1
======

.. seealso::

   - http://www.cnt-f.org/fedeptt/IMG/pdf/baf1_bafouille.pdf


Page 2
======

.. seealso::

   - http://www.cnt-f.org/fedeptt/IMG/pdf/baf2_bafouille.pdf

Page 3
======

.. seealso::

   - http://www.cnt-f.org/fedeptt/IMG/pdf/baf3_bafouille.pdf

Page 4
======

.. seealso::

   - http://www.cnt-f.org/fedeptt/IMG/pdf/baf4_bafouille.pdf
