
.. _contre_motion_14_2021_etpics94:

===============================================================================================================
Contre-motion N°3 à la motion n°14 2021 **Financement confédéral des groupes Femmes Libres** par **ETPICS94**
===============================================================================================================

- :ref:`motion_14_2021_syndicats_42`


Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`

Argumentaire
================

Aucune notification sur la liste de syndicats sur la convocation à un
congrès de Femmes Libres donc pour Etpics 94 ce congrès n'existe pas.

Découverte avec stupéfaction dans le combat syndicaliste de ce congrès
où il est écrit :

    "Ainsi, tous les jours, notre quotidien de militantes c'est : propos
    condescendants et paternaliste, insultes, intimidations, agressions
    physiques et verbales, viols...
    Nous refusons de risquer le viol et/ou l'agression physique lors de
    rencontres militantes (congrès, réunions, campings, soirées,
    hébergements)."

Pourquoi rester à la CNT si c'est tellement horrible ? Nous ne nous
reconnaissons pas dans cet état des lieux.

Les groupes femmes libres n'ont jamais existé au sein de la CNT (même espagnole)
parce que oui elles voulaient rester libres. Et que les femmes rejoignaient
leurs syndicats d'appartenance.

**Rappelons que la CNT est un syndicat de défense des travailleuses et
travailleurs pour l'abolition du salariat et pour l'action directe**.

Contre-motion
===============

Dans la perspective de création du groupe femmes libres,
la CNT propose qu'il se développe en dehors de la CNT afin d'être **plus
libre et plus sécure**.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°14 |            |          |            |                           |
| SEST Lorraine                  |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
