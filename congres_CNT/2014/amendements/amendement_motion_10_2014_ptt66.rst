
.. _amendement_motion_10_2014_ptt66:

================================================
Amendement à la motion n°10 2014 (PTT66)
================================================

.. seealso::

   - :ref:`motion_10_2014_ste49`




Préambule
=========

La commission de préparation suggère que cet amendement soit voté avant la
motion.

Amendement
===========

La CNT-PTT 66 demande à ce que la motion n°10 soit fractionnée en quatre
parties pour le vote.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la motion N°10 2014

       :ref:`PTT66 <ptt66>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
