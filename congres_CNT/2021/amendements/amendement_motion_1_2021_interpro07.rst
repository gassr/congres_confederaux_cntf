
.. _amendement_motion_1_2021_interpro07:

=====================================================================================================================
Amendement à la Motion n°1 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **Interpro07**
=====================================================================================================================

- :ref:`motion_1_2021_ste33`

Autres motions Interpro07

    - :ref:`rhone_alpes:interpro07_motions_congres`

Argumentaire 
============

Motion incomplète sur la liste des suppressions.  **Elle est exclusive**.
Le patriarcat et le racisme sont deux formes de domination mais ce ne sont
pas les seules (religieuses, ...).

Amendement 
============

Elle est consciente que plusieurs formes de domination  interagissent et
se renforcent les unes les autres.

C’est pourquoi elle tient à préciser que la suppression du
capitalisme ira de pair avec avec la suppression de toute forme de
domination.

La CNT entend bien se donner les moyens d’articuler toutes ces luttes
dans ce sens.

NDLR
=====

- Voir :ref:`statuts_conf:chartes_amiens`

::

    Le Congrès considère que cette déclaration est une reconnaissance
    de la lutte de classe , qui oppose sur le terrain économique, les
    travailleurs en révolte contre toutes les formes d’exploitation et
    d’oppression, tant matérielles que morales, mises en oeuvre par la
    classe capitaliste contre la classe ouvrière.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°1  |            |          |            |                           |
| Interpro07                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
