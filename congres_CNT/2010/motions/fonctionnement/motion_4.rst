



.. index::
   motion 4 Labellisation confédérale d'un syndicat ou réactivation de syndicat (2010)

.. _motion_4_2010:

==================================================================================================================
Motion n°4 2010 : Labellisation confédérale d'un syndicat ou réactivation de syndicat, Etpic 30 - Sud [non votée]
==================================================================================================================

.. seealso::

   - :ref:`motions_etpic30`



Motion reprise par ETPIC30, 2012
================================

- :ref:`motion_39_2012`

Préalable
==========

Cette motion reprend en substance la proposition administrative du :term:`B.C`
adoptée à l'occasion du :term:`C.C.N` des 5 & 6 décembre 2009 à Rennes.

Son adoption par le Congrès viendrait remplacer la motion «Label confédéral»
adoptée au 28ème Congrès confédéral de 2004.

Elle viendrait modifier dès lors les règles organiques associées aux statuts.

Argumentaire
=============

Durant son mandat écoulé, le :term:`B.C` de la :term:`C.N.T` s'est trouvé devant la nécessité de traduire une procédure unique et
transparente en vue de la labellisation d'un syndicat, la motion de 2004 se montrant particulièrement incomplète.
Si les syndicats doivent déposer des statuts en cohérence avec les statuts confédéraux (art.2 des statuts
confédéraux), il appartient bien au :term:`B.C` de réceptionner cette demande de labellisation et de procéder à une
première étude pour validation. À ce titre, le :term:`B.C` n'a pour seule compétence que de valider ou examiner la
labellisation de syndicats, mais pas d'unions (unions locales ou départementales, fédérations, unions régionales).
Attention particulière est donc faite quant aux buts, au caractère syndical, à l'adhésion à la :term:`C.N.T`, au champ de
syndicalisation géographique et d'industrie du syndicat.

Procès verbal de dépôt en mairie pourquoi ? Parce que le syndicat, selon nous, doit avoir une existence légale
avant tout, car c'est indispensable à la création de section syndicale ou même pour pouvoir prétendre à acter en
justice. Il est aussi nécessaire de produire le nouveau PV de bureau pour réactivation de vieux syndicats inactifs.
Procédure qui peut avoir l'air fastidieuse mais qui peut ne prendre que deux semaines au total. Ce processus a
aussi un rôle d'accueil. Il a permis (les nouveaux syndicats présents au Congrès confirmeront) un temps
d'accompagnement et d'explication sur ce qu'est l'organisation de la :term:`C.N.T`. La « lettre de bienvenue » aux
nouveaux syndicats en est l'illustration finale. Aussi, fort de l'expérience de notre syndicat au cœur du Bureau
confédéral et de la Commission administrative ces deux dernières, nous proposons:

Motion
======

- Prise de contact entre le :term:`B.C` (ou une UR) avec des militantEs souhaitant constituer un syndicat :term:`C.N.T` :
  Apport d’informations sur l’identité de la :term:`C.N.T` (si demandé), explication de la démarche de création à organiser
  en lien avec l’UR de référence si elle existe, information et échange sur l’existence de syndicats sur la même aire
  géographique ou à proximité. Information et échange sur la nécessité d’apparaître sur un champ de
  syndicalisation « professionnel » et un champ géographique de syndicalisation inexistant, ou renvoi sur les
  syndicats existants.
- Prise de contact avec le Bureau Régional de l’UR :term:`C.N.T` concernée, transmission de statuts type (si
  demandés), apports de conseils sur la définition du champ de syndicalisation : pour les Interco, inscription à
  minima sur un bassin d’emploi / économique reconnu (commerce et industrie par exemple, STICS, ETPIC,
  ETPRECI, etc.).
- Demande des documents nécessaires à la validation par le :term:`B.C` :

    * Projet de statuts à déposer > validation ou rejet par le :term:`B.C` (en lien avec l’UR concernée) en fonction de
      la conformité avec les statuts et les orientations de la :term:`C.N.T` & des champs de syndicalisation
      (géographique et professionnel). Suivi et avis motivé des rejets. Une souplesse d'adaptabilité sera
      observée pour les syndicats :term:`C.N.T` réactivés si inactivité manifeste depuis plusieurs années ou
      délabellisation antérieure ;
    * Récépissé de dépôt en mairie faisant apparaître le n° de déclaration pour : procès verbal d'AG
      (désignation nouveau bureau ou renouvellement du bureau) , dépôt des nouveaux statuts ;
    * Coordonnées des membres du bureau (non obligatoire mais utile pour des contacts directs)

- Sollicitation de l'accord et de l’avis de l’UR concernée (si elle existe)
- Réception des premières cotisations confédérales par la trésorerie confédérale.
- Labellisation par le :term:`B.C` > information au syndicat et simultanément à son UR (+ prochaine circulaire
  confédérale + liste web d'information aux syndicats). Dans un délai de 2 mois, lorsqu’il y a opposition d'une
  union régionale ou d'une fédération d'industrie, le :term:`B.C` doit suspendre sa décision, qui doit être soumise au
  prochain C.C.N. Les organismes concernés gardent le droit de présenter directement leur défense soit au C.C.N.,
  soit au Congrès.
- Information au :term:`C.C.N` suivant en vue de la labellisation définitive et parution à la prochaine circulaire
  confédérale (information préalable du :term:`B.C` via la liste syndicat). Information du :term:`B.C` au syndicat sur l’ensemble
  des outils confédéraux mis à sa disposition : circulaires confédérales, recueil des motions en vigueur, aide du
  secteur propagande, outils net, accès aux colonnes du CS, des Temps maudits, etc.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion 4             |            |          |            |                           |
| Etpic 30 - Sud       |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::  :ref:`contre_motion_4_2010_ess34`
