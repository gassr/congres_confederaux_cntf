
.. _motion_bureau_confederal_cnt_congres_toulouse_2001:


==============================================================================
Bureau Confédéral, 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
==============================================================================


Concernant les mandatés du Bureau Confédéral
============================================

Vote
----

70 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 33         |  5       | 13         | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


De par leur fonction, les mandatés du BC représentent l'organisation vis à vis
de l'extérieur. Ils se doivent donc d'être en conformité avec les statuts
confédéraux, d'avoir un minimum d'expérience syndicale et au moins une année
d'adhésion.
