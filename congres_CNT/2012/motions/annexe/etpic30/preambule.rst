
==========
Préambule
==========

.. seealso::

   - :ref:`motion_synthese_droit_syndical_2008_lille`


Cette motion s’inscrit dans un contexte de débat qui secoue plus ou moins
la CNT, à divers endroits, concernant nos choix de pratiques militantes,
syndicales et organisationnelles.

En effet, les récentes réformes gouvernementales du droit syndical dans le
secteur privé, comme dans le public, conduisent la CNT à devoir adapter ses
positionnements en termes de stratégie syndicale.

Compte tenu de ce nouveau contexte, l’enjeu consiste à concilier préservation
de notre identité révolutionnaire avec de nouvelles dispositions légales.

La motion :ref:`«représentativité» <motion_synthese_droit_syndical_2008_lille>` du 30ème Congrès Confédéral en a été la première
illustration. Elle a permis au secteur privé d’établir un consensus, peu
contesté à ce jour.

C’est à présent aux fonctions publiques de faire face aux modifications des
droits syndicaux. Nos camarades concernés auront à relever ce défi en proposant
de nouvelles stratégies syndicales, si nécessaires.

Par ailleurs, la CNT apprend début 2010 que le syndicat du nettoyage CNT RP
salarie depuis plusieurs années à titre permanent une personne désignée comme
chargée essentiellement des questions juridiques et de la représentation des
intérêts du syndicat auprès des instances prud’homales.

Placé devant le fait accompli et au regard des orientations défini par le
Congrès de 1996, le Bureau Confédéral demande en Mai 2010 à ce syndicat de
bien vouloir se conformer aux décisions confédérales : "Le Congrès réaffirme
la position de notre Confédération quant au refus des permanents syndicaux,
rémunérés directement ou indirectement par l'Etat, le patronat ou le syndicat
(patron). C'est aussi dans ce sens que nous devons lutter pour la réduction
du temps de travail, afin de développer nos outils syndicaux".

En l’absence de réponse, le BC relance ce syndicat en Octobre 2010 et précise :
"la perspective et la proximité du Congrès Confédéral de la CNT vous offre la
possibilité d'introduire une ou plusieurs motions exposant vos positions et
contributions. L'occasion vous est donc donnée de faire évoluer, dans le cadre
démocratique prévu à cet effet, les orientations de la CNT si elles ne vous
convenaient pas."

Aucune motion n’a été déposée en ce sens au Congrès 2010 par les intéressés
d’abord, les autres syndicats ensuite.

A notre sens, cette carence collective n’a pas permis à la CNT de trouver une
issue immédiate, réfléchie, et sereine aux problématiques de fond et de forme.

Le recours aux permanents est un choix stratégique majeur. Cette situation, si
elle perdure dans son état actuel, tend à déstabiliser le pacte confédéral et
la cohésion de notre organisation.

Cette série de motions vise donc, par l’introduction un débat constructif et
contradictoire au sein de la confédération, à établir un cadre collectif et
cohérent à une évolution pragmatique de l’action militante et syndicale de la
CNT au regard des possibilités «offertes» par le droit syndical et à définir
les modalités et les moyens organisationnels de notre structuration interne.

Elle s’appuie sur un diagnostic et un inventaire introductifs préalables,
indissociables des motions concernées.

Au vu de la vivacité des échanges entre militant.e.s ces dernières années, nous
ne doutons pas naturellement et par conséquent que leurs syndicats respectifs
puissent autant que possible contribuer au débat par l’apport de motions
contributives, d’amendements, et de contre motions.

Au besoin repéré, une ou des commissions de synthèses pourront bien sûr se
tenir sur tout ou partie des thématiques et propositions abordées et
référencées ci-dessous.
