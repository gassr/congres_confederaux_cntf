
.. _amendement_motion_52_2012_sinr44:


==========================================
Amendement à la motion 52 (SINR44)
==========================================

.. seealso::

   - :ref:`amendements_sinr44_2012`
   - :ref:`motion_52_2012`



Motivation
==========

Il nous semble important qu'un contrôle se fasse sur les envois de mandatés aux
rencontres internationales, on imagine mal que toute liberté puisse lui être
donné, notamment en ce qui concerne les déplacements les plus coûteux.

Motion modifiée
===============

Un compte-rendu des dépenses et des recettes du Secrétariat international doit
être établi trimestriellement.

Lorsque des rencontres internationales exigent la présence de plus d'un mandaté,
le Secrétariat international décide de ces mandatements après
**consultation de la CA**.

Vote indicatif
==============

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°52 | 2          | 32       | 4          | 12                        |
| SINR44                      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
