
.. _motions_crise_et_scission_cnt_congres_lyon_1996:

=================================================================================================
Motions "Crise et scission" de la CNT 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
=================================================================================================

Demande d’exclusion de la CNT le Mans (AIT)
===========================================

Le syndicat Interco de Perpignan se trouve aujourd'hui considérablement freiné
dans son élan par la présence dans notre région d'une section CNT-AIT du
secteur "Bordeaux "

Les problèmes liés à ce phénomène sont faciles à imaginer et d’ores et déjà
bien connus au sein de notre confédération. Nous ne tenons donc pas à nous
étendre ni à analyser le problème de manière exhaustive.

Cependant nous tenons a rappeler que le simple fait qu'il y ait deux
organisations avec le même sigle, appartenant à la même Internationale,
diffusant deux canards au noms identiques, suffit à semer la confusion et à
nous discréditer aux yeux de tous ceux qui nous manifestent leur sympathie,
sans parler des rapports exécrables que nos deux organisations entretiennent.

Nous pouvons affirmer que nombre de gens qui ont pris contact avec nous, sont
singulièrement refroidis par la dichotomie des deux CNT et de leurs rapports,
ils sont donc plus qu'hésitants à nous rejoindre.

Il n'est donc plus possible pour nous d'en rester là !

Certains camarades extérieurs à notre région à qui nous avons déjà exposé le
problème pensent qu'il se résoudra tout seul, dans la mesure où nous
développerons notre activité syndicale et où nous créerons des sections
syndicales d'entreprises : la "CNT Bordeaux" étant "incapable" ou ne voulant
pas se situer sur ce terrain, nous n'y serions pas gênés.

Or nous pensons nous que ce point de vue est singulièrement naïf:

- d'une part parce que la "CNT Bordeaux" est implantée dans la région depuis
  des années (elle y est donc plus connue que notre section) et qu'elle y a
  développé son implantation à la faveur des grèves de décembre 1995.
- d'autre part, pour créer des sections syndicales en entreprise, il faut
  que nous nous fassions d'abord connaître, ce qui signifie que nos militants
  sont dans un premier temps avant tout des propagandistes d'un projet syndical
  Cela justifie pour nous le choix syndical d'une section Interco.


Tant que nous ne sommes pas en mesure de créer des sections syndicales en
entreprise, nous nous retrouvons donc de fait sur le même terrain que l'autre
groupe CNT !! Et là, ceux-ci ont tout le loisir d'utiliser avec efficacité
leurs armes favorites contre notre Confédération et notre syndicat: calomnies,
médisances, voire menaces.

En dernière analyse, le problème se résume à une question d'identité : c'est
pourquoi nous pensons qu'il concerne toute l'organisation que nous appelons à
se déterminer sur la position suivante : Nous demandons que notre confédération
exige de l'AIT l'exclusion du secteur"Bordeaux" (l'AIT possédant suffisamment
d'information sur notre litige pour pouvoir se prononcer : au stade où nous en
sommes, tout refus de trancher serait hypocrite de sa part).



Développement confédéral
========================

Le Congrès retient l'idée d'une politique confédérale d'aide au développement
de la CNT dans le sud de la France notamment là où nous sommes confrontés à la
scission "CNT Le Mans"


Réalisation d’une brochure sur la scission de 1992
==================================================

Le Congrès vote pour la réalisation d'une brochure sur la scission, brochure
qui aura l'avantage de répondre aux interrogations de nouveaux adhérents ou
syndicats et de pouvoir répondre aux calomnies répandues par nos ex-camarades.

Tous tes syndicats ou adhérents possédant des informations, documents sur le
sujet sont priés de les envoyer au Bureau Confédéral qui se charge de réaliser
cette brochure.


Relation avec « l’autre CNT »
=============================

Compte tenu de la motion de la **CNT Le Mans** votée au 25ème Congrès, compte
tenu des réalités de terrain, nous prenons en compte le refus de
la "CNT Le Mans" de rencontres nationales entre les deux CNT.

Nous encourageons, partout où il est possible, le travail à la base de
syndicat à syndicat des deux Confédérations qui permette à long terme la
réunification.
