.. index::
   pair: intranet ; 2006

.. _motions_intranet_2006:

================================================================================================================
**Internet & intranet confédéral** 29ème Congrès confédéral du vendredi 2 juin au dimanche 4 Juin 2006 à Agen
================================================================================================================

.. seealso::

   - :ref:`motions_internet_intranet_cnt`




.. _principe_dutilisation_internet_2006:

Principes d’utilisation d’internet [2006]
===============================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 45         | 5        | 11         | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

L’utilisation d’internet peut être **source d’inégalité et d’exclusion** de par
son mode d’accès, tout le monde ne disposant pas d’un ordinateur.

Internet n’est pas un outil de prise de décisions et est contraire à notre
éthique de fonctionnement collectif ;

Internet est un simple outil de communication utiles à ceux et celles qui
souhaitent l’utiliser ;

**Le bureau Confédéral doit faire en sorte que chaque syndicat ait bien accès à
toutes les informations internes à la CNT, que ce soit par internet ou par les
envois postaux, en accord avec la volonté des syndicats** ;

**Internet ne doit en aucun cas être source de discrimination ou d’exclusion
au sein de la Confédération**.


.. _mise_en_oeuvre_intranet_2006:

Mise en oeuvre de l'intranet confédéral [2006]
==================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 46         | 8        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La CNT se dote d'un ensemble de services Internet accessibles uniquement
aux adhérent.e.s des syndicats de la CNT,


.. _adherents_syndicats_2006:

Accès des adhérents d'un syndicat (2006)
+++++++++++++++++++++++++++++++++++++++++

Les cénétistes accèdent à l'intranet en tant **qu'adhérentEs de  leurs syndicats.**

Chaque syndicat peut gérer des ressources réservées à ses adhérentEs
(partage de documents, forums, etc.).

La gestion des accès des cénétistes est effectuée dans les syndicats
auxquels ils et elles sont affiliéEs.

Ainsi, chaque syndicat adhérant à la CNT qui le souhaite se dote d'au
moins unE mandatéE qui:

- reçoit un accès au nom de tout le syndicat;
- gère et tient à jour les accès à l'intranet confédéral des adhérentEs
  de ce syndicat ;
- fournit l'assistance technique nécessaire aux adhérentEs de son syndicat ;
- gère les services réseaux associés à ce syndicat.

Accès par les structures composées d'individus de la CNT
++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Les autres structures de la confédération (les unions locales et régionales,
les fédérations, le :term:`BC`, la :term:`CA`, les commissions, les :term:`TM`,
le :term:`CS`) peuvent aussi gérer des ressources propres.

Les autres structures prévues dans les statuts et composées d'individus
(telles les commissions confédérales ou régionales; ou encore, les
publications: CS, TM, etc.) peuvent aussi fournir ponctuellement un accès
intranet à leurs membres.

Au besoin, cet accès est géré par unE mandatéE appartenant à la structure,
comme pour les syndicats.
L'usage de cet accès est réservé aux activités de la structure.

Accès par les structures non individuelles de la CNT
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Les autres structures non composées d'individus (par exemple UR, fédérations,
UL) n'ont pas vocation à fournir des accès à l'intranet pour les cénétistes.

Sur l'intranet, elles peuvent servir à la gestion des services réseaux propres
à la structure (par exemple pour une UR, inscription des syndicats de sa
région ou forum de niveau régional).

Elles sont alors gérées par unE mandatéE appartenant à la structure.


.. _services_intranet_2006:

Services [2006]
++++++++++++++++++


L'intranet confédéral offre, d'une part, des forums de discussion et, d'autre
part, divers services pour la conservation, l'échange et la rédaction de
documents électroniques confédéraux, fédéraux, régionaux ou syndicaux.

D'autres services peuvent être mis en place, mais en aucun cas l'intranet
confédéral ne peut fournir des services allant à l'encontre des principes
de fonctionnement de la confédération ou mettant celle-ci dans l'illégalité.

**[Exemple : échange de fichiers sous copyright, organisation d'une trésorerie
parallèle ou d'un congrès bis.]**


Autonomie et confidentialité
++++++++++++++++++++++++++++

L'intranet confédéral est mis en oeuvre de manière autonome par la
confédération.

**[Ne pas employer les services de boites spécialisées, etc.]**

Les informations qui s'y échangent ne doivent pas être communiquées à
l'extérieur.

L'infrastructure physique mise en place pour accueillir l'intranet
est choisie de manière à minimiser les risques de vol ou de saisie.


.. _commission_permanente_intranet_2006:

Commission permanente intranet [2006]
============================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 44         | 8        | 9          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Une commission confédérale permanente **intranet** est créée à partir de
l'actuelle commission intranet (ex-commission interne au SIIRP, validée au
niveau confédéral par le CCN de décembre 2005).

Elle se dote **d'unE secrétaire et d'unE trésorier/ère**.

La commission intranet fait elle-même les choix techniques pour mettre en
oeuvre l'intranet confédéral, en accord avec les principes adoptés par la
confédération et le budget qui lui est alloué.

Les services réseau sont installés sur un serveur administré uniquement par
des membres de la commission intranet et mandatéEs par celle-ci.

La commission en assure le fonctionnement. Elle met en oeuvre une assistance
technique et des formations auprès des syndicats, UL, UR, fédérations et
autres structures de la confédération.

Elle reçoit de la confédération les moyens (pécuniaires et matériels)
nécessaires au fonctionnement de l'intranet et à la formation.


.. _budget_intranet_2006:

Budget intranet [2006]
==========================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 37         | 13       | 8          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


La Confédération alloue à l'intranet confédéral un budget de frais de
fonctionnement de 100 euros par mois (serveur exclusivement dédié à la CNT,
hébergé par Globenet à prix coûtant)


.. _gestion_autonome_intranet_2006:

Intranet : Gestion autonome des régions syndicat & gestion des accès des syndicats et autres structures [2006]
=================================================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 41         | 11       | 10         | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Gestion par les UR
+++++++++++++++++++

Inscription et désinscription des syndicats.

Chaque UR a la possibilité de mandater unE gestionnaire pour inscrire ses
syndicats qui en font la demande par le biais de leur adhérentE mandatéE.

Les UR qui font ce choix se chargent également de supprimer les accès des
syndicats qui n'existent plus.

Gestion directe par la commission intranet
++++++++++++++++++++++++++++++++++++++++++

Pour les UR qui ne souhaitent pas gérer les inscriptions de leurs syndicats,
pour les régions qui n'ont pas d'UR et pour les autres structures de la CNT
(commissions, etc.), **le/la gestionnaire des inscriptions, membre de la
commission intranet confédérale inscrit les syndicats (ou autres structures)
et leurs mandatés respectifs**.


.. _gestion_editoriale_intranet_2006:

Gestion éditoriale des services de niveau confédéral (forums et wiki) [2006]
================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 39         | 14       | 6          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Pour les deux premières années (2006-2008), la commission intranet choisit en
son sein unE mandatéE à la gestion du contenu des forums de niveau confédéral,
qui peut intervenir dans la bonne tenue des débats et qui assure aussi la
coordination de la publication des documents confédéraux (wiki et
porte-documents).

À l'issue de ces deux ans, un rapport d'activité sur ce mandat est remis à la
confédération qui pourra alors s'appuyer sur cette expérience pour décider de
la meilleure manière de gérer l'intranet confédéral d'un point de vue
éditorial.


.. seealso:: :ref:`motions_internet_intranet_cnt`
