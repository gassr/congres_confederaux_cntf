

.. index::
   pair: Stratégie; Implantation de la CNT dans les entreprises privées
   pair: Stratégie; R.S.S
   pair: 36; Motion RSS
   pair: 36; R.S.S
   ! Motion 36 (Congrès de Saint-Etienne 2010)


.. _motion_36_2010_saint_etienne:
.. _motion_36_2010:

=========================================================================================================================================================
Motion n°36 2010 : Implantation de la CNT dans les entreprises privées/Nomination des représentants de section syndicales (RSS) Etpic 30 - Sud [adoptée]
=========================================================================================================================================================





Argumentaire
============

La motion adoptée au Congrès CNT de septembre 2008 intitulée :ref:`Stratégie syndicale et représentativité <motion_strategie_2008>`
(p.8 du recueil confédéral des motions en vigueur) prévoyait
que la Confédération étudie la pertinence (protection, capacités) de la
désignation de représentants de la section syndicale (:term:`R.S.S`), telle que
permise par  les nouvelles dispositions législatives.

Lors des différents :term:`C.C.N` des deux années écoulées, les délégués
régionaux ont pu faire remonter les expériences locales d'implantation
syndicale (Wolters Kluwer, PSA, People and Baby, DUC, EXAPAQ, etc.).

Il en ressort que la présentation de :term:`R.S.S` CNT sans entraves juridiques
préalables spécifiques dans les entreprises de + de 50 salariéEs, **offre à
la CNT la possibilité d'apparaître assez aisément dans le privé**.

Bien que la pression patronale reste forte, **cette nouvelle situation permet
dès lors aux cénétistes de rompre avec plusieurs décennies de tentatives
d'implantations difficiles**, sur un terreau juridique très aléatoire,
en vue d'acquérir les droits d'expression syndicaux essentiels.

Nous pouvons toutefois noter un recours très inégal dans les différentes régions
de la CNT du mandat syndical :term:`R.S.S`.

Ce constat nécessiterait une analyse plus poussée:

- absence de maîtrise ou de connaissance des droits syndicaux ?
- manque de formation ou d'assurance ?
- absence totale ou partielle de salariéEs du privé ?
- représentation majoritaire des salariéS des TPE ou PME ?
- stratégies locales spécifiques ?

Nombre de départements, de bassins d'emploi, ne sont pas couverts
géographiquement par des syndicats de la CNT.

Pour les travailleurs/ses isoléEs désireux/ses de se syndiquer à la CNT
et d'ouvrir une section d'entreprise, les unions locales, par défaut les
fédérations voire les :term:`unions régionales` pourraient être les structures
à l'initiative des désignations.

Mais la manœuvre est plus risquée juridiquement et suppose que la section ne
connait quasi pas de vie syndicale en dehors de l'entreprise. Pour que l'adhésion
à la CNT comporte un contrôle syndical de la section et une solidarité
interprofessionnelle locale, **le développement des syndicat intercos
(STICS, ETPICS, etc.) reste à privilégier**.

Il convient de s'intéresser à ce nouveau mandat R.S.S avec précaution. S'il est
avant tout mandat syndical, son effectivité demeure en lien étroit avec
l'échéance des élections professionnelles.

La désignation d'un :term:`R.S.S` comporte de même certaines contraintes ou limites
juridiques, statutaires, ou temporelles:

- L'ancienneté du syndicat de 2 ans minimum
- La personne désignée doit avoir au moins un an d'ancienneté dans l'entreprise
- L'adhésion de deux personnes minimum sur l'entreprise
- Des statuts de syndicat couvrant les champs d'industrie et/ou géographique
- La désignation par des membres du bureau du syndicat dans le respect des dispositions
  prévues aux statuts de chaque syndicat
- La désignation des seulEs déléguéEs du personnel dans les entreprises et moins
  de 50 salariés ETP et de plus de 11 salariés ETP

Le mandat demeure précaire puisqu'il prend fin dés les premières élections
professionnelles suivant le mandat. L'incapacité de négocier ou de procéder
à la signature d'accords d'entreprises (protocoles préélectoraux compris).

UnE ancienNE :term:`R.S.S` ne peut être désignéE comme :term:`R.S.S` que 6 mois avant l'échéance
des prochaines élections professionnelles suivant la fin de son précédent mandat :term:`R.S.S`.

Le mandat de :term:`R.S.S` offre néanmoins l'avantage de permettre à la CNT:

- d'accéder à une diffusion libre de sa propagande syndicale dans l'entreprise
  via un panneau syndical ou la diffusion de documents papiers, voire numérique
- d'accéder à une expression directe et syndicale libre via son R.S.S
- de protéger l'un de ses adhérents parfois en situation contractuelle précaire
- de disposer d'heures de délégations rémunérées sur leur temps de travail.
  
Et plus largement:

- de renforcer son implantation numérique dans l'entreprise en sortant de la
  clandestinité ;
- de préparer la pérennisation des mandats syndicaux dans l'entreprise via
  les élections professionnelles;

L'expérience des syndicats de la CNT nous apprend que les patrons ou les autres
syndicats contestent nos :term:`R.S.S` sur la base:

- du nombre de personnes adhérentes. Les syndicats prendront la précaution
  d'exiger des tribunaux ne pas communiquer l'identité des autres adhérentEs
  de la section pour éviter d'exposer des camarades non protégéEs face à la
  répression syndicale inutilement
- de la qualité des personnes signant les désignations en lien avec les modalité
  d'élection ou de renouvellement des membres du bureau du syndicat ou de
  l'union de syndicats. Aux syndicats d'assurer le renouvellement de leur
  bureau en fonction de leurs dispositions statutaires ou de modifier leurs
  statuts vers plus de « souplesse » statutaire
- l'adéquation du critère législatif intitulé « valeurs républicaines » avec
  les buts définis aux statuts de chaque syndicat. Les procès en cours
  (Affaires BAUD vs CNT) laissent entrevoir l'instauration d'un jurisprudence
  favorable à la CNT en ce qui s'agit de ses orientations fondamentales: abolition
  de l'Etat et du patronat, pratique de l'action directe, etc.

Protégés du licenciement par les inspections du travail, les :term:`R.S.S` et leurs
syndicats devront toutefois être vigilants quant à la protection des autres
syndiquées sur l'entreprise face à la répression patronale.

Motion
======

Section I
---------

En vue de renforcer l'action syndicale de la CNT dans les entreprises, les
syndicats de la CNT tendent à présenter, partout où les conditions le permettent,
des représentantEs de sections syndicales (:term:`R.S.S`).

Cette orientation confédérale n'a toutefois de sens que si les syndicats CNT
prennent correctement la mesure de la précarité temporelle du mandat :term:`R.S.S`
avant toute désignation.

Dans les entreprises de + de 50 salariés, deux stratégies principales
d'implantation syndicale sont possibles:

- **stratégie n°1** – Désignation de :term:`R.S.S` ayant pour seul objectif de bénéficier
  des droits syndicaux de base : expression syndicale libre (orale, écrite),
  protection du licenciement, heures de délégation
- **stratégie n°2** – Désignation de :term:`R.S.S` pour l'obtention de droits syndicaux
  de base destinés à assoir la représentation de la section syndicale par la
  désignation de déléguéE syndicalE via la présentation de candidatEs aux
  élections professionnelles.

En cherchant à éviter de tomber dans le piège de la cogestion, ou dans l'illusion
qu'une réelle force syndicale puisse être issue d’une quelconque élection,
les syndicats de la CNT pourront privilégier la première stratégie d'implantation
via la désignation syndicale de :term:`R.S.S`.

Cette stratégie n°1 suppose que:

- La personne désignée puisse prétendre à 1 an d'exercice du mandat avant la
  date prévue pour les élections professionnelles. Seuls les :term:`R.S.S` (comme les DS)
  ayant exercéEs leur mandat pendant + d'1 an bénéficie d'une protection post-mandat
  de 12 mois (le même :term:`R.S.S` ne peut pas être désigné à nouveau comme représentant
  syndical jusqu’aux six mois précédants la date des élections professionnelles
  suivantes dans l’entreprise en cas d'échec d'obtention de la représentativité).
  Une année c'est aussi une période minimale pour un renforcement de
  la section syndicale en terme de nombre d'adhérentEs et donc de soutien mutuel.
  Le syndicat pourra toutefois, dans le doute, recourir à la stratégie n°2
  consistant à recourir aux élections professionnelles pour pérenniser la protection
  des personnes désignées ;
- La personne désignée :term:`R.S.S` ait une ancienneté contractuelle significative lui
  garantissant une relative protection face aux licenciements. En l'absence
  de candidatures aux élections professionnelles, le mandat et la protection
  associée s'arrêtant à l'occasion de l'organisation de ces mêmes élections.
  L'exposition volontaire d'unE syndiquéE salariéE et ex :term:`R.S.S` de la CNT à une
  potentielle répression patronale est mesurée avec attention par le
  syndicat et la personne concernée.

La deuxième stratégie consisterait à pérenniser les mandats syndicaux de :term:`R.S.S`
via la désignation de délégués syndicaux accessible que par l'acquisition
d'une audience électorale suffisante dans l'entreprise (+ de 10% des
voix exprimées pour la liste de candidatEs CNT aux élections professionnelles
des Comités d'entreprise). Elle s'inscrit essentiellement dans la volonté
du syndicat et de sa section de vouloir protéger les syndiquéEs CNT de
la répression patronale.

Le seuil « fatidique » de 10% de votes favorables requiert une attention particulière.

Puisque liée à une logique électorale, la **stratégie n°2** suppose :

- L'audience électorale de candidatures CNT aient bien été évaluée par le
  syndicat en fonction de la taille de l'entreprise, de la répartition des
  militants CNT dans les différents secteurs ou services de l'entreprise
  (réalité structurelle et nombre de lieux de travail), du nombre et de
  l'implantation des organisations syndicales présentes, de la reconnaissance
  professionnelle bénéficiant aux candidats, de la durée du mandat
- La personne désignée puisse prétendre à 1 an d'exercice du mandat avant
  la date prévue pour les élections professionnelles. Seuls les :term:`R.S.S` (comme les DS)
  ayant exercéEs leur mandat pendant + d'un an bénéficie d'une protection
  post-mandat de 12 mois (Le même :term:`R.S.S` ne peut pas être désigné à nouveau comme
  représentant syndical jusqu’aux six mois précédant la date des élections
  professionnelles suivantes dans l’entreprise en cas d'échec d'obtention de
  la représentativité).

Quelle que soit la stratégie retenue par les syndicats CNT dans leur désir
de présenter des :term:`R.S.S`, il conviendrait que ces mandatées syndicaux
participent de façon régulière aux assemblées statutaires de leur syndicat
d'appartenance.

Les unions de la CNT (fédérations, unions régionales)
*****************************************************

Les :term:`C.C.N` inscriront systématiquement à leur ordre du jour un point
sur les **questions de stratégie syndicale en lien avec les réformes de la
représentativité syndicale**.

Dès lors, les UR de la CNT, ou des fédérations, y exposeront de
façon exhaustive les prises de décisions de ses syndicats afin qu’une analyse
globale puisse être appréciée en vue d’éclairer le prochain congrès confédéral.

Section II
----------

Afin de répondre aux opportunités d'implantations syndicales sur l'ensemble
du territoire, les unions régionales et la confédération veilleront à
poursuivre le **développement des syndicats intercos et apparentés**.

En lien avec la commission juridique confédérale, les syndicats de la CNT
et leurs unions (unions locales / départementales, fédérations,
unions régionales) étudieront la pertinence et la capacité à désigner
des :term:`R.S.S` (et DS) par ces mêmes unions.

Section III
-----------

Compte-tenu du développement de l'emploi intérimaire, la commission juridique
et la commission précarité confédérale étudieront la capacité à désigner
des :term:`R.S.S` (et DS) dans les entreprises intérimaires.


**SSE 31**
    On demande à ce que chaque section de la motion soit votée séparément.

**Santé social RP**
    Cette motion ne doit pas être traité, il s'agit d'éléments de formation
    ou de compte rendu, on la trouve très longue, des syndicats veulent
    qu'on ne la vote pas.

Vote indicatif implantation de la CNT dans les entreprises privées/Nomination des représentants de section syndicales (RSS) Etpic 30 - Sud
==========================================================================================================================================

Vote indicatif demandé par la tribune.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°36          | 39         | 12       | 10         | 4                         |
| Etpic 30 - Sud       |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Etpic 30 sud**
    On a rien a dire de plus, elle peut être discutée en 3 temps pas de problème.

**Etpic 57**
    Cette motion approfondit le :ref:`débat <motion_strategie_2008>` du congrès de Lille,
    qui a amené à des départs de syndicats de la conf. C'est aussi une motion stratégique
    afin d'avoir une vision globale.

**Educ 38**
   Première section "en vue .... " rajouter "qui le veulent"

**STEA**
    Contre, ca n'empêche pas la répression. Fonctionnement : pas de contrôle,
    rotation des mandats, et c'est pour cela que c'est incompatible.

**Culture spectacle RP**
    Appuie cette motion, dans le cadre législatif actuel, favorable à l'action
    syndicale. Cela nous permet d'avoir une réelle implication syndicale
    dans les boites.

**SIM RP**
    La pratique on est pour, donne une couverture légale et syndicale à
    des copains. On s'abstient sur cette motion, c'est une fiche de formation,
    cela aurait dû sortir dans le :term:`C.S`.

**SIPM RP**
    On a voté pour, en appliquant :ref:`la motion de synthèse de Lille <strategie_syndicale_cnt_congres_lille_2008>`, il y a
    des sections syndicales qui se sont montées, et expérimentent les
    différentes stratégies par rapport à la nouvelle loi.
    Les différentes stratégies selon l'entreprise sont à discuter.
    Les :term:`R.S.S` sont contrôles par le syndicat, et ne peuvent pas sortir
    du  syndicat. Cela permet de protéger des gens. C'est intéressant de pouvoir
    les protéger.

**Interco 31**
    Pour un découpage car la section 1 nous paraît trop directive, on est
    pour voter la motion à 100%.

**Santé social RP**
    La désignation du :term:`D.S` implique une participation aux élections;
    alors on ne peut pas avoir le choix de participer ou non aux élections.

**ETPIC 74**
    Que cette motion soit dans un guide d'accueil pour les adhérents etc..

**ETPIC 57**
    Cette motion s'articule sur la :ref:`motion 56 <motion_56_2010>`. Il faut définir une stratégie.
    La loi et la représentativité sont fonction d'un rapport de force, on
    aura un rapport de force pour faire évoluer la loi, ce n'est pas le cas,
    donc on n'a pas le choix, on utilise ce qui existe, les outils qui nous
    permettent de le faire. On peut se retrouver coincé sur les élections
    professionnelles mais cela se discute à l'intérieur pour que les mandats
    puissent tourner le plus possible. On invite les syndicats à soutenir
    cette motion.

**SSE 34**
    On va voter contre, cela ne correspond pas à notre type de fonctionnement
    sur le :term:`CE`.

**SIPM RP**
    La participation aux élections n'est pas un choix ; c'est la loi qui l'a
    imposé, si on voulait continuer à exister dans les entreprises.
    On participe aux élections si on veut, cela permet de nommer
    un :term:`R.S.S`, d'avoir les droits fondamentaux.


Vote définitif implantation de la CNT dans les entreprises privées/Nomination des représentants de section syndicales (RSS) Etpic 30 - Sud [adoptée]
====================================================================================================================================================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°36          | 52         | 8        |  7         |  5                        |
| Etpic 30 - Sud       |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

**Motion n°36 Adoptée**
