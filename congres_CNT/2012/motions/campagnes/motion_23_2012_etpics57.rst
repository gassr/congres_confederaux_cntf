
.. _motion_23_2012:
.. _motion_23_2012_etpics57:

=======================================================================================================
Motion n°23 2012 : Plafonnement des revenus pour un partage des richesses plus juste ETPICS 57 Nord Sud
=======================================================================================================
  
.. seealso::

   - :ref:`etpics_57_pub`
   - :ref:`motions_etpics57_2012`




Argumentaire
============

Le véritable malaise de la répartition des richesses de notre société ne se
trouve pas seulement dans la répartition des revenus et salaires au sein de
la classe populaire, mais entre cette dernière et la classe économiquement
dominante.

En partant de principe qu'un travailleur ne peut produire ou servir 20 fois
plus qu’un autre, aucun ne devrait pouvoir gagner 20 fois plus d’argent
qu'un autre sur un même temps de travail, qu’il soit actionnaire, PDG d’une
multinationale ou footballeur.

Si on veut que nos revenus aient une juste valeur, il ne suffit pas d’établir
de plancher minimal tel que le RSA ou le SMIC, il faut aussi mettre en place
un plafond maximal, afin d’établir une rémunération proportionnelle et
relative aboutissant ainsi à une économie mesurable. Sans quoi, gagner
le SMIC, n’est qu’une valeur algébrique déconnectée de tout référentiel.

Le fait que certains revenus de cette planète dépassent le milliard, rend
les revenus des classes populaires insignifiant, négligeable.


Motion
======

Dans le but de freiner le développement du capitalisme et à titre transitoire
vers le communisme libertaire, la CNT revendique l'encadrement des hauts
revenus. À ce titre aucun revenu ne pourra être supérieur à 20 fois le
revenu minimal. Le diférentiel d'argent ainsi créé pourra donc être utilisé
pour les moyens de production, les services publics, la sécurité sociale,
les retraites.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°23          |            |          |            |                           |
| ETPIC57              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
