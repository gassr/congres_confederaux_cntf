
.. index::
   pair: action ; juridique
   pair: loi ; août 2008


==============================================
I.B.b- L’action juridique, un enjeu spécifique
==============================================

Si la voie du juridique n’est pas forcément incompatible avec notre engagement
révolutionnaire, elle ne saurait se suffire à elle-même.

Elle est une composante immédiate de l’action syndicale mais notre syndicalisme
de transformation sociale ne peut se résumer à une prestation de service,
soit-elle efficace.

Certains syndicats sont effectivement plus soumis à une pression juridique de
par les difficultés des travailleurs de leur secteur d’industrie face un
patronat organisé et offensif.

Bien souvent, ces secteurs cumulent de même des difficultés en termes de
précarité, de méconnaissance des droits, et d’accès au droit (illettrisme,
maîtrise de la langue, problème de niveau scolaire). Nous l’avons vu et compris
au cours des discussions de la commission confédérale liée au syndicat du
nettoyage RP.

La loi d’août 2008 portant sur le droit syndical dans le privé, certes
critiquable au regard de bons nombres d’aspects, a permis malgré tout à la CNT
de connaitre un développement de son implantation dans les entreprises.

La constitution de sections syndicales étant facilitée à travers la désignation
des RSS et une acquisition « simplifiée » de la représentativité nécessaire à
l’expression syndicale dans l’entreprise.

Dans la fonction publique, le décret paru au journal officiel le 17 février
2012, remplaçant la circulaire de 1982 jusqu’alors en vigueur, modifie en
certains points la réglementation du droit syndical dans la fonction publique
d’Etat.

Dans nos expériences récentes, les syndicats ont été confrontés à des enjeux
juridiques plus ou moins lourds en vue de l’implantation de leurs sections
d’entreprises. La CNT a eu a affronté plusieurs procès relatifs à la liberté
d’expression sur internet. Des Unions Locales CNT ont dû faire face à des
procédures judiciaires pour l’accès ou le maintien à des locaux syndicaux.


Certaines affaires ont eu une résonnance confédérale et interprofesionnelle eu
égard aux enjeux jurisprudentiels (affaires BAUD / SCIAL RP, Fédération des
entreprises de propreté / nettoyage RP)

L’action juridique prend par conséquent dans certains secteurs, ou plus
largement, une place non négligeable que notre inter-corporatisme ne peut ignorer.

Dès lors, elle requiert:

- Compétences spécifiques et renouvelées (évolution du droit)
- Disponibilité

:ref:`Le XXXIème Congrès de 2010 <congres_CNT_2010_saint_etienne>`  adoptait les
orientations suivantes::

	«Le syndicalisme prôné par la CNT est fondé sur l'importance du lien
	interprofessionnel. C'est ce lien qui donne la dimension de solidarité de
	classe, un des fondements de notre pratique et de notre projet révolutionnaire»

L’approche intercorporatiste ou interprofessionnaliste propre à l’identité de
la CNT, ou l’AS&SR, doit nous amener à développer de prime abord des outils
internes mutualisés basés essentiellement sur l’effort militant.

Il conviendrait dès lors à tous les niveaux de l’inter-professionnalisme ou des
Unions de la CNT que cette action juridique constitue un effort collectif
d’entraide mutuelle.

L’établissement de commissions juridiques permanentes doit pouvoir répondre à
ces besoins avant tout autre type d’intervention à caractère professionnel ou
prestataire (cabinet d’avocat, voire embauche d’un « permanent juridique »).

La formation syndicale et juridique est un autre levier indispensable pour notre
développement et pour nos militants et plus particulièrement pour ceux qui
exercent une activité syndicale dans l’entreprise.

Les camarades militants élus ou représentants des sections d’entreprises ont
parfois peu ou pas d’expériences et sont amenés à siéger dans les IRP (DP, CE,
CHSCT) sans pour autant en connaitre les fonctionnements et prérogatives.

Des sections syndicales sont donc livrées à elles-mêmes et ont aujourd’hui
recours aux conseils et à l’assistance de quelques camarades reconnus pour
leur expertise. Cette situation n’est pas satisfaisante. Elle repose
exclusivement sur un système de type informel ou non mutualisé.

Cette hiérarchie de compétences peut tendre de surcroit à l’instauration d’une
ascendance sur les choix d’orientation des syndicats par la dépendance qu’elle
peut générer.

Ainsi, le développement de sections syndicales d’entreprises, l’arrivée de
nouveaux adhérents doivent pouvoir s’accompagner de formations syndicales,
trop souvent laissées au second plan alors que de tout évidence il s’agit d’une
nécessité.

Ces formations syndicales doivent être organisées et construites pour et par
une démarche interprofessionnelle.
