
.. index::
   pair: Motion 11 ; 2014

.. _motion_11_2014_tasra:

=================================================================================================================
Motion n°11 2014: Création d'une fédération du travail, de l'emploi et de la formation professionnelle (TAS-RA)
=================================================================================================================

.. seealso::

   - :ref:`tasra_motions`




Argumentaire
=============

Notre syndicat Travail et affaires sociales existe depuis 2010.

Notre action syndicale au sein du ministère du travail nous amène à
développer des contacts en dehors de la région Rhône-Alpes (Strasbourg,
Dijon,…).

Afin de franchir un nouveau cap dans notre développement nous ressentons
aujourd’hui le besoin de créer une nouvelle structure et de nouveaux
outils.

Une nouvelle fédération nous permettrait ainsi de mieux assurer le
contact et le suivi des contacts isolés dans l’objectif de créer de
nouvelles sections et de nouveaux syndicats dans notre secteur.
Le but est également de coordonner notre action dans les luttes
qui nous rassemblent et de produire du matériel transversal.

Notre secteur d’activité croisant des métiers du travail, de l’emploi
et de la formation professionnelle.

Nous prévoyons d’étendre notre champ de syndicalisation à ces trois
secteurs.

Motion
========

Le 33e Congrès de la CNT décide la création d’une fédération du travail,
de l’emploi et de la formation professionnelle.


Contre motions
==============

.. seealso::

   - :ref:`contre_motion_11_2014_sinr44`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°11 2014

       :ref:`TASRA <tasra>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
