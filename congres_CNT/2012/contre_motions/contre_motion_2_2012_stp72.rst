

.. _contre_motion_2_2012_stp72:

================================================================================================
Contre-motion à la motion N°2 2012 STP72
================================================================================================

.. seealso::

   - :ref:`motion_2_2012_sanso69`




Préambule
==========

Rappel des règles de vote conformément aux modalités adoptées au Congrès
d’Agen en 2006 et appliquées depuis :

- Pour l'ouverture d'un congrès: :term:`quorum` à 50% des syndicats à jour de
  cotisations (décision du Congrès d'Agen 2006, applicable à partir du
  Congrès de Lille 2008).
- Pour la poursuite d'un Congrès : :term:`quorum` à 50% des syndicats présents
  ou représentés dans la salle au début du Congrès.

Pour qu'une motion soit adoptée, elle doit recueillir plus de 50% des votes sur
la base du nombre de syndicats enregistrés au Congrès.

Contre-motion
=============

Une motion, pour être adoptée, doit recueillir plus de 60 % des votes des
syndicats enregistrés ayant exprimé leur voix.

Mais toute motion ayant recueilli entre 40 et 60 % des votes exprimés nécessite
la réunion d’une commission de synthèse chargée de proposer une nouvelle motion
au vote avant la fin du congrès.

Ces pourcentages proches de la majorité expriment en effet un doute légitime de
la part du congrès, lequel ne doit alors imposer ni l’abandon ni l’adoption de
la motion mais sa refondation.

Cette nouvelle motion, dite de compromis, doit à son tour recueillir 60 % des
votes exprimés des syndicats enregistrés.

La commission de synthèse est  composée par tiers et par tirage au sort de
délégués des syndicats ayant voter  pour, contre ou abstention.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°2      |            |          |            |                           |
| STP72                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2012_sanso69`
