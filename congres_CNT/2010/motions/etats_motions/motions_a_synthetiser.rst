

.. _motions_a_synthetiser_congres_CNT_2010:

=============================================================
Motions à synthétiser au XXXIe Congrès CNT Saint-Etienne 2010
=============================================================


Fonctionnement organique
========================

- :ref:`motion_2_2010`
- :ref:`motion_3_2010`
