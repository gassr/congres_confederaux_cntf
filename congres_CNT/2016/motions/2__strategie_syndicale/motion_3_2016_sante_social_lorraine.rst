
.. index::
   pair: Motion 2 ; 2016

.. _motion_3_2016_etpic30:

===================================================================================================================================
Motion n°3 2016: Institut de Formation Confédéral (Santé Social CT Lorraine) [adoptée]
===================================================================================================================================



Argumentaire
=============

Comme cela a été évoqué lors des différents CCN depuis le congrès de
Metz en 2012 l'IFC constitue un outil intéressant pour la confédération
dans a mesure où il lui permet :

- De former à leur mandat, sur leur temps de travail, et avec
  financement patronal, les élu.e.s aux CE et aux CHSCT
- De faire rentrer à la CNT une certaine somme d'argent lui permettant
  de financer des formations FESS (Formations Économiques, Sociales et
  Syndicales) sur le temps de travail des salariés.

De plus, considérant que l'IFC permet de convier aux formations
organisées par la CNT des salariés qui ne sont pas adhérents de notre
organisation syndicale, celui-ci constitue un outil de développement:
en effet il parait bénéfique pour diffuser nos pratiques syndicales que
de pouvoir dispenser des formations syndicales, économiques, ou sociales,
à la place des employeurs.

L'IFC, en plus de dégager à nos militants des espaces et des temps de
formation et d’échanges collectifs, constitue donc indéniablement un
outil de développement pour la CNT.

Néanmoins l’expérience de la première année de convention avec Culture
et Liberté met en évidence le fait que la CNT dispose de peu d'élue.e.s
CE et CHSCT lui permettant de financer l'institut de formation.
Si l'opération a été réalisable lors de cette première année, nos élu.e s
désormais formés ne pourront plus participer à cette démarche avant le
renouvellement de leur mandat.

Aussi, si la CNT souhaite que l'IFC devienne un outil pérenne et viable
sur le plan financier, et ainsi jouir d'un véritable outil de diffusion
de ses pratiques syndicales, il est nécessaire qu'elle conçoive
cet outil comme ouvert sur l’extérieur, et non pas cantonner à ses
propres militants.

Cette démarche pourra alors lui permettre de bénéficier d'un financement
pour les formations CE et CHSCT, lequel pourra être réinvesti dans
l'organisation des formations FESS, non financées.

Il est donc nécessaire que les syndicats se saisissent localement de la
possibilité d'organiser les formations CE et CHSCT.


Motion
=========

La CNT renouvelle son partenariat avec Culture et Liberté afin de
pouvoir disposer d'un Institut de Formation Confédéral.

Le Bureau Confédéral réalise une brochure explicative de l'IFC à usage
interne : constituant un mémo sur son fonctionnement

Le Bureau Confédéral réalise un matériel confédéral (affiche, tract)
de base qu'il met a disposition des structure confédérées afin de
faciliter l'organisation, par ces dernières et sur le plan local, de
formations du plus grand nombre possible d'élu.e.s CE et CHSCT,
syndiqués ou non.

Ces formations permettant à la CNT d'être financée, le congrès confédéral
invite ses structures adhérentes à multiplier ce type de formations.

Si le Bureau Confédéral et la Trésorerie Confédérale sont responsables
des relations administratives avec Culture et Liberté et des transactions
financières entre les deux structures, l'organisation des formations CE
et CHSCT, jouissant d'un financement patronal, est réalisée par les
syndicats, les unions régionales et les fédérations.

Les syndicats, Union régionales, ou fédérations de la CNT formulent des
propositions de formation FESS (non financées) au CCN. C'est le CCN
qui, au regard de ces propositions, des besoins de la confédération en
termes de formation et des moyens financiers dont elle dispose, définit
un plan de formation pour les 6 mois à venir et donne mandat à un
syndicat, à une union régionale, ou au Bureau Confédéral, pour
l'organisation concrète de ses formations.

Amendements
===========


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°2 2016

       :ref:`ETPIC30 <etpic30>`
     - 31
     - 6
     - 9
     - 3
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
