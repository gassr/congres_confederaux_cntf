
.. _contre_motion_24_2021_etpics94:

=========================================================================================================
Contre-motion N°5 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **ETPICS94**
=========================================================================================================

- :ref:`motion_24_2021_ste33_cnt42`

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`


Argumentaire
================

**La lutte anti patriarcale.**

A l’occasion des :ref:`accusations <ssct_lorraine_2017_03_05>` portées
par une militante à l’encontre d’un adhérent de la CNT hors structure de notre syndicat ,
une première étape a eu lieu à la fin du congrès de Paris dans des circonstances
qui se sont voulues **spectaculaires** parvenant à un congrès finissant
comme une information qui relevait du **sensationnel**.

Non qu’il ait fallu taire cette situation grave , on peut cependant
honnêtement regretter qu’il n’ait pas été donné un cadre plus serein
pour cette question qui méritait de bénéficier d’une information
circonstanciée afin que les syndicats puissent débattre et échanger.
**Par défaut on ne peut que constater le manque de confiance originel
dans  le fonctionnement de notre organisation.**

Evolution
------------

Cet événement fut le départ d’une :ref:`véritable campagne de dénigrement <role_de_linterpro_31_fouad>`
à la fois sur les réseaux sociétaux qui diffusaient les coordonnées de
l’agresseur livré à la vindicte et dont l’extrême droite a fait ses
choux gras et également par l’intermédiaire de bombages où la **CNT était
dénoncée comme violeurs** : rue des  Vignoles et sur les parcours de
manifestations dans Paris.
Enfin nous avons supporté une manifestation, à l’occasion de notre festival (en 2018),
qui nous reprochait d’abriter un violeur ( ? ) et les quelques échanges
que nous avons pu avoir avec les manifestants.es nous ont permis de
comprendre que les événements étaient mal connus voire falsifiés.

D’anciens.nes adhérents.es participaient à cette manifestation pour y
régler, à notre sens, des questions de revanche plus personnelles.
On a revu certains.es d’entre elleux dans des cortèges CNT en région.

**Il a été rapidement accordé un caractère public aux questions qui se posaient à la CNT ( ? )**

Comme on peut l’admettre des syndicats ont souhaité la mise en place
d’un protocole sur le viol. Sur la forme s’est posé la question de vouloir
imposer ce protocole de la façon la plus rapide possible, selon nos
règles il convenait que les propositions se fassent à l’occasion de
notre congrès confédéral.

Dans la foulée : création des groupes femmes libres.
Nous avons vécu comme une tentative de passage en force la création de
ces groupes.
Au-delà la création de lieux d’échanges ou d'activité, nous retenons
qu’en dehors du pacte confédéral on a voulu organiser et imposer une
structure cénétiste qui **n’a en rien reçu l’accord de la confédération**.

Ce **fonctionnement autoritaire** s’est confirmé lorsque ces mêmes
groupes qui, par définition ne sont pas des syndicats seuls éléments
constructeurs de notre confédération syndicale, se sont présentés vers
l’extérieur de l’organisation comme structures officielles.
Diffusion de ces créations dans notre organe de communication confédéral
le Combat Syndicaliste et diverses émissions de radio et mise en place
d’un congrès ( ? ). **Nous avons à faire à des structures non labellisées**.

**Le non respect de notre fonctionnement et la volonté de mise en place du
fait accompli nous apparaissent comme une attitude autoritaire doublement
incompatible avec nos principes.**

Les contradictions avec notre fonctionnement:
Si la priorité du syndicat est de faire du syndicalisme et de préparer
la mise en place de moyens pour permettre à la population dans son ensemble
d’organiser elle même la production pour répondre à ses besoins, le
syndicat doit également réfléchir aux réponses posées par les questions
de société.
Il le fait avec, une fois de plus les femmes et les hommes de son temps
en s’opposant si besoin aux contradictions que comporte notre société.

A travers les groupes cités et les protocoles ou procédures soumises
nous relevons quelques caractères autoritaires, élitistes, exclusifs
qui présentés sous forme de réflexion de la déconstruction nous apparaissent
plutôt comme une **forme de destruction**.

Hormis qu’il sera toujours possible de réfléchir à toute initiative, il
nous est présenté sans aucune forme d’alternative quelques idées force
qui nous inquiètent:

- Les réunions de la CNT sont ouvertes à tous.tes les adhérents.es, en
  établissant une règle de non mixité il est clairement convenu qu’une
  partie de ces réunions devient interdite à certains.es.
  Tout à la CNT n’est plus ouvert à tous.tes. Si ce principe est retenu
  sous prétexte de lutte anti patriarcale il devient applicable dans
  d’autres circonstances. Rappelons que la non mixité n’a jamais constitué
  une protection contre le viol et que des associations non mixtes ont
  eu à déplorer des viols.
- La distinction des luttes permet de décider de donner une priorité à
  certaines. S’il s’avère qu’un syndicat doive faire face à des questions
  prioritaires dans le cadre de sa lutte ou de son actualité il adaptera
  son activité en conséquence. Quand un groupe devient thématique il
  donne priorité à un aspect de la lutte et comme nous avons vu apparaître
  un *groupe jeune* il faut nous attendre à voir naître diverses
  “organisations” dans l’organisation qui oeuvreront autour de thèmes
  choisis. Alors que notre lutte évolue en appréhendant les questions
  qui se posent à nous, nous risquons de diluer notre activité dans
  des choix d’autant plus inquiétants qu’il est question de faire
  fonctionner ces groupes dans des contextes *unitaires* où on se demande
  ce qui restera des choix cénétistes.

**Ces déconstructions, pour nous, sont plus proches de la destruction.**

- A partir des consignes des divers protocoles et procédures nous sommes
  surpris.es et inquiets.es à la fois de la mise en place de structures
  de pouvoir et consécutivement de sanctions qui les accompagnent.
- Si nous partageons l’idée qu’un.e violeur.se n’a pas sa place à la CNT
  nous remarquons, à la lumière des faits, que la sanction d’exclusion
  pour viol a progressivement servi à demander une exclusion pour la
  tenue de propos jugés non conformes.
- Qu’en sera-t-il bientôt ? Dès l’instant où se crée une instance de
  décision composée par des gens désignés comme spécialistes ( par qui?)
  il nait un lieu de pouvoir une sorte de **haute autorité libertaire**,
  pourrait-on dire , dont rien ne permet d’envisager qu’il tienne le
  rôle attribué. Ainsi que l’illustre le dérapage indiqué, avant même
  la mise en place d’une procédure, nous n’avons aucune garantie sur la
  fiabilité d’une telle structure à l’égard de la CNT.
- Notons l'abandon du principe d’autonomie des syndicats.
- Enfin , dans les différents courriers que nous avons pu lire dans le
  cadre du bulletin intérieur, s’il n’y a pas toujours la chaleur et la
  solidarité entre camarades que nous pourrions attendre nous sommes
  inquiets des propos insultants écrits par des personnes évoquant leur
  lassitude de la pédagogie et ceci avant que nous ayons entamés notre
  prochain congrès.

La mise en place et l’évolution de la lutte anti patriarcale à la CNT
s’est accompagnée très régulièrement d’attitudes, de propos et de projets
qui ne mettent pas notre syndicat en situation de confiance pour
aborder comme il est de principe à la CNT un débat essentiel qui devrait
nous permettre de répondre tous.tes ensemble à développer notre audience
et notre action qui ne sont pas à la hauteur de ce que nous devrions
représenter.


Contre-motion
===============

La CNT réitère le précepte selon lequel le syndicat est la seule et
unique entité et **rejette toute forme de prise de pouvoir quelle qu'elle
soit**.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°24 |            |          |            |                           |
| ETPICS94                       |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
