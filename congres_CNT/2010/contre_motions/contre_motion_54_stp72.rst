
.. index::
   colonialisme
   race
   sexe
   Rapports sociaux de sexe et de race

.. _contre_motion_54_2010_stp72:

=======================================
Contre-motion à la motion N°54 (stp72)
=======================================


Le développement de la solidarité de classe entre les travailleur-euses doit
absolument prendre en compte les autres rapports sociaux, rapport de domination
entre les sexes, rapports de domination raciste en lien avec le passé colonial
de la France.

Afin de diffuser la connaissance chez les militant-es de l'histoire des luttes
d'émancipation des femmes et des luttes de décolonisation, et **afin que les
camarades issu-es de ces groupes sociaux puissent prendre la place qui leur
revient dans nos syndicats, il sera créé une commission «Rapports sociaux de sexe et de race».**

Cette commission aura pour objectif d'encourager et d'organiser, sous des formes
qui restent à définir, l'appropriation par les militant-es des outils politiques
forgés par ces luttes.

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°54 |            |          |            |                           |
| stp72                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+


.. seealso::

  - :ref:`motion_54_2010`
