
.. index::
   pair: Identité ; CNT


.. _motion_identite_cnt_congres_1993_paris:

=======================================================================
Motion **Identité** 1993 de la CNT  XXIVe Congrès de la CNT à Paris
=======================================================================

Contexte
========

Suite à un travail de commission et amendement.

Vote
====

Syndicats participants au vote : 13

**Adoptée à l’unanimité**

Texte
=====

Adhérer à la CNT, adhérer a l’AIT, c'est assumer un contenu et un contenant
dont la lettre se trouve explicitement définie dans les statuts respectifs de
ces organismes.

Pour la CNT:
------------

**Article 1** , La Confédération Nationale du Travail a pour but:


- de grouper, sur le terrain spécifiquement économique, pour la défense de leurs
  intérêts matériels et moraux, tous les salariéEs, salariés, à l'exception des
  forces répressives de l'Etat, considérés comme des ennemis des travailleurs.

- de poursuivre, par la lutte de classe et l'action directe, la libération des
  travailleurs qui ne sera réalisée que par la transformation totale de la
  société actuelle.

**Article 2** La C.N.T est constituée par:


1. les syndicats, groupés dans les Unions locales et régionales, et les
   fédérations d'industrie;
2. les unions régionales de syndicats;
3. les fédérations d'industrie.


Cette association est conçue et organisée sur des bases fédéralises.

Nul syndicat ne peut faire partie de la CNT s'il n'adhère pas à la fédération
d'industrie, à son union locale et, à son union régionale.

Les organisations adhérentes à la CNT ont droit à la marque distinctive appelée
label confédéral.

Pour l'A.I.T:
-------------


::

    Le syndicalisme révolutionnaire, en se fondant sur la lutte des classes,
    tend à l'union de tous les travailleurs
    dans des organisations économiques de combat, qui luttent pour la libération
    du double joug du capital et de l'Etat. Sa finalité consiste en la
    réorganisation de la vie sociale en la fondant sur le communisme libertaire,
    moyennant l'action révolutionnaire de la classe ouvrière. Considérant que
    seules les organisations économiques du prolétariat sont capables d'atteindre
    cet objectif, le syndicalisme révolutionnaire s'adresse aux travailleurs en
    leur qualité de producteurs, de créateurs de richesses sociales, afin qu'il
    germe et se développe entre eux, en opposition aux partis ouvriers modernes
    qu'il déclare incompétents pour la réorganisation économique de la société.


Dans la fidélité à ce qui fonde son identité, la CNT se doit donc de mener le
combat de la construction d'une organisation syndicale révolutionnaire, dont
l'élément de base, le syndicat professionnel ou interprofessionnel
d'implantation locale, se trouve au carrefour d'une double structure:

- **structure géographique** d'abord qui, de l'union locale à l'union régionale
  puis au plan national, est le lieu privilégie de l'action inter corporative
  dans la cité.
- **structure professionnelle** ensuite qui, par les fédérations d'industrie,
  facilite l'implantation et rend plus efficaces les sections syndicales dans
  les entreprises.

**Sans ces deux champs d'actions**, sans la volonté en actes de s'implanter
durablement, il n'y a pas de syndicalisme tel que l'entendent la CNT et l'AIT.


---------------------------------------------------


.. _lutte_egalite_des_sexes_1993:

Rien n'est étranger à notre syndicalisme: ni l'antimilitarisme, ni l'anticléricalisme, ni **l'écologie**, ni la lutte pour **l'égalité des sexes**
-------------------------------------------------------------------------------------------------------------------------------------------------------

**Partant de cette assise, rien n'est étranger à notre syndicalisme:
ni l'antimilitarisme, ni l'anticléricalisme, ni l'écologie, ni la lutte
pour l'égalité des sexes.**

---------------------------------------------------

Organisation de classe, la CNT-AIT ouvre à une transformation révolutionnaire
de la société fondée sur le **communisme libertaire**, ce qui implique
l'abolition :

- du salariat
- du capitalisme privé ou d'Etat
- et de l'Etat lui-même.

Sur le plan tactique, elle revendique **l'action directe**, refuse toute sorte de
cogestion du système économique dominant et condamne quelque institution ou
organisme paritaire que ce soit.

La CNT affirme que la condition incontournable de sa croissance dans les
entreprises passe par le développement d'une pratique anarcho-syndicaliste et
syndicaliste révolutionnaire amenant à la création de sections syndicales et
de syndicats de branche (leur implication dans les unions locales doit être
simultanée avec leur travail dans l'entreprise).

Le Congrès reconnaît comme toujours actuelles les analyses précédentes
concernant les organismes représentatifs en leur attribuant, entre autres,
une fonction :

- de collaboration de classe
- de délégation de pouvoir contraire à notre projet de gestion directe et de
  mandatement révocable à tout moment.

La CNT reconduit donc le principe de la non-participation aux instances
représentatives (23ème congrès).

Le développement de la CNT étant, dans la situation actuelle, un impératif
incontournable, elle se doit de promouvoir la création de syndicats de branche
ou intercorporatifs ainsi que de sections syndicales d'entreprise, sur la base
explicite de nos principes d'action directe, de solidarité, de fédéralisme,
d'anti-hiérarchie, le tout émanant et étant contrôlé par les assemblées
générales des différentes structures.

Dans toute la mesure du possible, le recours aux démarches juridiques ou de
participation aux consultations de représentativité doivent être évitées et en
tout état de cause réservées aux situations défensives et contrôlées en
permanence par les syndicats.

La CNT reconduit le principe de la non participation aux instances
représentatives. Sous l'éclairage de ce principe et sous le contrôle des
unions locales, le Congrès ne reconnaît la pratique de présentation
exceptionnelle, tactique et temporaire, à des élections professionnelles que
dans le cas où nos droits syndicaux ne seraient pas reconnues dans l'entreprise
(délégué syndical et représentant syndicale au comité d'entreprise, dans
le privé ; en ce qui concerne la fonction publique, le problème ne se pose pas,
puisque les droits minima sont acquis).

Cette règle peut également être appliquée dans les entreprises privées de
moins de 50 salariés où la fonction de délégué syndical est assurée par un
délégué du personnel.

Il est entendu que la présentation de candidats ne saurait être interprétée
comme un passage obligé, d'autant que les élections professionnelles ne sont
pas la garantie d'obtenir les droits syndicaux, et que d'autres formes d'action
sont toujours possibles. Les positions de principe et les raisons justifiant
l'intervention devront être clairement explicitées dans le matériel de
propagande et / ou la déclaration d'intention.

En tout état de cause, la présence de la CNT dans l'entreprise ne peut être
assurée que par sa section syndicale (23ème Congrès).

Représentation directe des syndicats, **le Congrès confédéral est l'instance qui
édicte les directives et les orientations de l'organisation**.

Fédéraliste et solidaire, la CNT revendique pour ses différents groupements
(syndicats, unions locales, unions régionales, fédérations) la plus large
autonomie, dans le respect des orientations définies en commun.
