



.. _congres_federation_sante_social_vii:

===================================================================
VIIe Congrès Fédération Santé Social, Lille - 28 & 29 janvier 2012
===================================================================


L’ensemble des mandats fédéraux (secrétaire, secrétaires adjoints, trésorier-e, gestion du
site internet, gestion des listes...) seront rendu à l’ouverture de ce congrès et aucun des
mandatés actuels ne reprendra de quelconque mandat. Les syndicats sont donc invités à
proposer des adhérents pour la reprise des mandats du secrétariat fédéral afin que ces
derniers ne soient pas laissé vacant, ce qui serait préjudiciable à notre organisation et
créerait une situation juridique délicate.
Pour ce qui est du comité de rédaction de La Sociale !, ses membres actuels, qui pour
certain-e-s y sont investi depuis 6 ans, n’envisagent pas de conserver leur mandat. C’est
donc l’intégralité du comité de rédaction qu’il faudra renouveler.
Un descriptif succinct du contenu et de la fonction de chaque mandat est joint à ce
document afin que les syndicats et syndiqué-e-s sachent à peu près précisément de quoi il
retourne et puissent évaluer la charge de travail et la disponibilité que cela necessite. La
gestion du secrétariat fédéral est d’autant moins lourde que les mandaté-e-s sont nombreux-
ses et que son fonctionnement est collectif.
Davantage de précisions seront fournies prochainement sur l’organisation pratique de ce
congrès. Le système de péréquation sera comme d’habitude appliqué concernant les frais
de déplacement.
Les syndicats santé sociaux ainsi que les intercos peuvent dès maintenant informer le
secrétariat fédéral de leur venue et également faire des suggestions concernant la tenue et
l’organisation de ce congrès.


Fraternellement
Le secrétariat fédéral



où ? Quand? Comment?
=====================

Où?

Au local de la CNT 59/62 32 rue d’Arras - 59000 Lille

Quand?

Le samedi 28 et le dimanche 29 janvier 2012

Comment?

Concernant les frais de déplacement des
mandaté-e-s, le système de
péréquation habituel sera
appliqué. Il faudra penser à
conserver les titres de
transport ou autres justificatifs
pour la trèsorerie fédérale.
Pour l'hébergement, les
syndicats sont invités à faire
connaitre le nombre de
personnes à loger et, le
cas échéant, à préciser les
particularismes alimentaires .


5) Point sur les sections fédérales
- Cimade
- People and baby
- SEA 35
