
.. index::
   Fédération (PTT)


.. _federation_PTT:

================
Fédération PTT
================

.. seealso::

   - http://www.cnt-f.org/fedeptt/
   - http://www.cnt-f.org/fedeptt/spip.php?rubrique5


Adresse
=======

::

	33 rue des Vignoles
	75020 Paris
	Tél. : 05 57 89 21 72


:Courriel: fede.ptt@cnt-f.org

Cotisations fédérales
---------------------

::

	CNT-PTT 7
	Rue des Bouleaux
	33600 Pessac


Publications
============

.. toctree::
   :maxdepth: 4

   publications/index


Syndicats
=============

.. toctree::
   :maxdepth: 4

   rhone_alpes/index
