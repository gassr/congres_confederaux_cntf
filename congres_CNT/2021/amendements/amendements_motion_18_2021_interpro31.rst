
.. _amendements_motion_18_2021_interpro31:

=============================================================================================
Amendements N°1 à la motion n°18 2021 **Procédure de la labellisation** par **Interpro31**
=============================================================================================

- :ref:`motion_18_2021_ste93`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


Argumentaire supplémentaire
=============================

- :ref:`argumentaire_motion_18_2021_interpro31`


.. _amendement_1_motion_18_2021_interpro31:

Amendement 1
===============

Argumentaire 
---------------

La motion en l’état, empêche la formation d’un syndicat interpro de moins
de 5 membres si un syndicat d’industrie (ex : STE ) existe déjà au niveau
départemental !

Amendement 
-------------

Ajout dans le chapitre **Structuration & cohésion confédérales** des
articles suivants :

Article 1 
    Dans le cadre de la procédure de la labellisation, le syndicat **d’industrie**
    qui en fait la demande doit être composé d’au moins cinq adhérent-e-s.

Article 2 
    Si aucun syndicat n’existe dans le département, Un syndicat interprofessionnel
    peut être labellisé avec moins de 5 membres.


Vote
-------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
| n°18 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_18_2021_interpro31:

Amendement 1
===============

Argumentaire 
-------------

Nous pensons que 5 adhérent·es c’est trop peu pour faire vivre un syndicat
correctement. Il nous semble plus pertinent que la motion encourage les
syndicats à rester en interpro tant qu'ils ne sont pas assez nombreux·euses.

Amendement 
-------------

Ajout dans le chapitre « Structuration & cohésion confédérales » des
articles suivants :

Article 1 
    Dans le cadre de la procédure de la labellisation, le syndicat
    d'industrie qui en fait la demande doit être composé d’au moins
    cinq dix adhérent-e-s.

Article 2 
    Si aucun syndicat n’existe dans le département, un syndicat interprofessionnel
    peut être labellisé avec moins de 5 10 membres.


Vote
--------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°2 à la motion  |            |          |            |                           |
| n°18 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+

Argumentaire supplémentaire
=============================

.. toctree::
   :maxdepth: 3

   interpro31/argumentaire_motion_18_2021_interpro31
