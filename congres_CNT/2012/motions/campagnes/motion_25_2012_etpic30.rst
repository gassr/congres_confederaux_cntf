
.. _motion_25_2012:
.. _motion_25_2012_etpic30:

===================================================================================================
Motion n°25 2012 : Environnement : «Lutter contre les OGM » ETPIC 30 Sud
===================================================================================================
  
.. seealso::

   - :ref:`etpic_30_pub`
   - :ref:`motions_etpic30_2012`




Argumentaire
============

Depuis 1836, il existe une logique du clonage : favoriser l’industrialisation
capitaliste.

Les industriels de l’agroalimentaire pour augmenter toujours plus leur profit
investissent dans la recherche. Monsanto , mais aussi Limagrin, Segenta,
Pionner entre autres ont un seul objectif rendre les paysans de toute la
planète dépendants de leur semence.

Sous prétexte de répondre à la faim dans le monde, ils mettent sur le marché
ces OGM (Organisme génétiquement modifié) qui ne sont que des Pesticides
clonés brevetés. Le résultat final serait la main mise du vivant en le
brevetant.

Les OGM, c’est le maïs au Mexique, le coton en Inde, le soja au Guatemala, etc.

Sur toute la planète des paysans se retrouvent sans terre, ils rentrent dans
la mendicité ou se suicident. Au Maroc, mais aussi en Tunisie, au Sénégal,
Monsanto et sa clique continue de tisser sa toile.

En France, depuis plus de 10 ans, les Faucheurs volontaires résistent.
Ils alertent en informant l’opinion ; de nombreux chercheurs les soutiennent.
Ils agissent aussi en fauchant de jour et de nuit, en occupant des
coopératives... C’est de la désobéissance civile.

Leur but reste toujours le même : l’arrêt des OGM et de leur dérivés. Ils
sont aussi à l’initiative des Collectifs anti-OGM. La loi fait du sur place.
Le moratoire sur le Maïs Monsanto 810 a été remplacé par une directive
l’interdisant.

L’Europe a légiféré un décret interdisant du miel contenant des traces
d’OGM. Mais des scientifiques payés par les semenciers continuent leur
recherche. Ils manipulent autrement les gènes, par transgenèse ce sont
les mutés qui ne sont pas considérés comme OGM ... par la loi !

Du Soja transgénique continue d’arriver par tonnes dans les ports de
Bretagne et de Méditerranée. Ils serviront de nourriture animale qui finira
en bout de chaine ... dans nos assiettes.

Il est temps de résister à la main mise des semenciers capitalistes sur la
planète.
Il est temps de résister pour avoir des aliments de qualité.
Il est temps d’être solidaires des paysans de la planète en disant :
«Non aux OGM, Non aux Pesticides clonés brevetés »


La CNT s’oppose aux Pesticides clonés brevetés de tout genre dont les OGM.
La CNT se porte solidaire et soutient les collectifs anti-OGM.


Motion
======

Dans le but de freiner le développement du capitalisme et à titre transitoire
vers le communisme libertaire, la CNT revendique l'encadrement des hauts
revenus. À ce titre aucun revenu ne pourra être supérieur à 20 fois le
revenu minimal. Le diférentiel d'argent ainsi créé pourra donc être utilisé
pour les moyens de production, les services publics, la sécurité sociale,
les retraites.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°25          |            |          |            |                           |
| ETPIC57              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_25_2012_sanso_rp`

Contre-motions
==============

- :ref:`contre_motion_25_2012_ess34`
