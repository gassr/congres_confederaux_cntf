
.. _amendement_motion_9_2021_sest_lorraine:

========================================================================================
Amendement  N°2 à la Motion n°9 2021 **Lutte contre la Précarité** par SEST Lorraine
========================================================================================

- :ref:`motion_9_2021_interco69`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`


Amendement
============

Remplacer la phrase proposé par Interco 69::

    «Se mobiliser dans la lutte contre la précarité, l’intérim en particulier
    dans le secteur public, et l’expansion d'un salariat déguisé initie par
    l’économie numérique et la libéralisation dans tous les secteurs (prive/public).»

par::

    «Se mobiliser dans la lutte contre la précarité, l’intérim en particulier
    dans le secteur public, et l’expansion d'un salariat déguisé initie
    par l’économie capitaliste »

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°9  |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
