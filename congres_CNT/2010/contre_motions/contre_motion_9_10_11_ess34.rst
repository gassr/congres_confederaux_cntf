

.. _contre_motion_9_10_11_2010_ess34:

==========================================================
Contre-motion aux motions N°9, 10, 11 ESS34 [rejetée]
==========================================================


Une organisation adhérente à la CNT se verra accorder la marque distinctive
appelée **label confédéral** automatiquement par le :term:`B.C` si aucun syndicat
UR ou UL limitrophe et aucune fédération d’industrie  existante ne conteste
dans un délai de 2 mois. En cas de contestation, l’article 24 des statuts entrera en
application.

En plus des autres conditions actuelles, pour avoir le droit de vote au
congrès CNT, les syndicats doivent s'être acquittés du règlement de leur
participation aux congrès précédents.


+------------------------------------------+------------+----------+------------+---------------------------+
| Nom                                      | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==========================================+============+==========+============+===========================+
| Contre motion aux motions n°9, 10 et 11  | 19         | 20       | 12         | 10                        |
| ESS34                                    |            |          |            |                           |
+------------------------------------------+------------+----------+------------+---------------------------+

**contre motion rejetée.**

.. seealso::

   - :ref:`motions_9_10_11_2010`
