
.. _amendement_motion_37_2010_stp72:


===========================================
Amendement à la motion 37 (STP 72) [rejeté]
===========================================


A la place de la phrase::

    «Charge aux syndicats et fédérations concernés de  prendre les dispositions
    suffisantes au respect de cette orientation fondamentale »,

nous proposons::

    «les syndicats de la CNT refusent toute participation aux élections des
    commissions administratives paritaires dont les fonctions s'apparentent à une
    participation à la gestion du personnel par l'employeur et au traitement des
    dossiers individuels. Le rôle d'un syndicat n'est pas de s'ériger en juge des
    salarié-es en validant ou invalidant les prétendus mérites attribués par
    l'employeur. C'est la porte ouverte au clientélisme.

    Pour ce qui concerne les commissions techniques et les commissions d'hygiène
    et de sécurité, les dispositions suivantes sont appliquées par les sections
    syndicales en garantie du fonctionnement autogestionnaire face à une délégation
    de pouvoir : le délégué siège avec des mandats impératifs ; il s'efforce de
    réunir en assemblée générale préalable l'ensemble du personnel du lieu de travail
    avant les sessions des commissions paritaires et toute prise de décision ;
    il rend obligatoirement compte de son mandat; il est révocable immédiatement par la
    section à l'aide d'une démission en blanc remise signée avant l'entrée en fonction
    du délégué; la rotation des mandats de délégué est appliquée à chaque élection.»



.. _vote_amendement_motion_37_stp72:

Vote Amendement à la motion 37 (STP 72)
=======================================

+----------------------------------+------------+----------+------------+---------------------------+
| Nom                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==================================+============+==========+============+===========================+
| Amendement à la motion n°37      | 28         | 26       | 11         | 8                         |
| STP 72                           |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+


**Amendement rejeté**


.. seealso::

   - :ref:`motion_37_2010`
   - :ref:`motions_37_38_39_2010`
