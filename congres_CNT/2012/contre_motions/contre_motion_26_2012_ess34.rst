

.. _contre_motion_26_2012_ess34:

================================================================================================
Contre-motion à la motion N°26 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_26_2012_chimie_bzh`




Contre-motion
=============

La CNT considère la question énergétique comme directement liée au capitalisme
et au productivisme mais elle est aussi fondamentale au fonctionnement d’une
société autogérée.

C’est pourquoi la CNT prend position pour la sortie immédiate du nucléaire et
crée une commission ayant pour but d’affiner son argumentaire sur le sujet des
énergies, de leurs usages, de leurs impacts environnementaux, de leurs utilités...


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°26     |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
