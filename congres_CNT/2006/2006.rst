
.. index::
   ! 29e Congres CNT 2006 (Agen)


.. _congres_CNT_2006_agen:

===========================================================
Le **XIXXe Congrès CNT 2006 d'Agen** du 2 au 4 juin 2006
===========================================================

.. _syndicats_presents_2006_agen:




Nombre de syndicats présents à Agen, 2006 : 70
==============================================

70 syndicats présents à jour de cotisations.


Motions adoptées
================

.. toctree::
   :maxdepth: 2


   motions_adoptees/motions_adoptees



.. seealso::

   - :ref:`congres_CNT_2008_lille`
   - :ref:`congres_CNT_2004_saint_denis`
