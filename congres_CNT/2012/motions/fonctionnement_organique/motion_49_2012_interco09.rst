
.. _motion_49 _2012:
.. _motion_49 _2012_interco09:

=====================================================================================
Motion n°49  2012 : Règle limitant les possibilités de cumul de mandats (Interco 09)
=====================================================================================
  

.. seealso::

  - :ref:`motions_interco09`




Argumentaire
============

Nos statuts confédéraux préconisent la rotations des mandats mais sans limiter
leurs cumuls.

Il y a la une contradiction à laquelle il serait bon de remédier à l'avenir,
sans toutefois remettre en cause les mandats actuels afin d'éviter tout effet
rétroactif.


Motion
======

Sauf en cas de carence de candidatures, toute personne déjà en charge d'un
mandat syndical interne n'est pas habilitée à en briguer un autre.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°49          |            |          |            |                           |
| Interco09            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
