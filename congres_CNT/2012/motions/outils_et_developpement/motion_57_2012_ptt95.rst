
.. _motion_57_2012:
.. _motion_57_2012_ptt95:

=======================================================================================================================================
Motion n°57 : Pour une nouvelle revue de réflexions théoriques de la CNT (PTT 95)
=======================================================================================================================================
  
.. seealso::

   - :ref:`ptt95`
   - :ref:`motions_ptt95_2012`




Argumentaire
============

Les Temps Maudits, revue théorique de la CNT créée à l’issue du Congrès de
Lyon en 1996 doit être considérée comme ayant sombré corps et biens en tant
que revue vivante, régulière et susceptible de mobiliser les énergies
militantes tant à l’intérieur de nos rangs qu’au-delà.

Il conviendra de tirer un bilan de cette revue, de son rayonnement jusqu’à
son déclin : évolution des abonnements, difusion militante, participation
des syndicats à son élaboration comme à sa diffusion.

Il conviendra de s’interroger sur les causes de sa disparition.

Aujourd’hui nous considérons que la CNT ne peut laisser le monopole de la
réflexion, du débat, des échanges aux autres courants du mouvement ouvrier
dont certains sont en opposition, ouverte ou non, avec notre démarche ASSR.

Notre histoire, nos orientations, nos avancées, notre pratique, nos
alternatives, mérite une tribune qui argumente, polémique, défende ses
points de vue sur la scène sociale : la revue, entre le livre, la
brochure et le journal est le lieu pour la confrontation, les témoignages,
y compris sur le plan international.

Mieux cet espace est indispensable pour ne pas laisser le terrain libre
aux différentes mouvances ou organisations qui entendent représenter une
alternative au syndicalisme de combat et élaborent des analyses que nous
devons contester, réfuter, quand cela est nécessaire.

Comme nos anciens nous considérons que la réflexion, la théorie, n’est
nullement le domaine réservé des intellectuels, des experts, qui
conceptualisent en étant parfois éloignés de toute activité sociale,
de toute lutte collective.

De même nous considérons que notre aptitude à ne pas déserter ce terrain
conditionne aussi notre crédibilité tout en étant un instrument de formation
pour nos adhérents-es.

L’acceptation de la disparition d’une revue de réflexions serait interprétée
par ailleurs comme un recul de l’influence de la CNT alors que nous avons un
espace à tenir, partie intégrante du combat social.

Mais il faut cependant tirer un bilan de l’échec actuel des TM pour ne pas
répéter les mêmes erreurs.


Motion
======

Le syndicat :ref:`CNT PTT 95 <ptt95>` propose que le Congrès prenne la décision
de créer une  nouvelle revue et désigne une commission de mise en place
d’un premier numéro  pour le premier CCN suivant le Congrès.

Cependant, tirant le bilan des échecs passés, quelques points sont à prendre
en considération :

- La revue ne devra pas être déconnectée de la vie confédérale en étant
  représentée à chaque CCN.
- Les UR pourront régulièrement mettre un point à l’ordre du jour pour
  solliciter des articles de syndicats, au choix de dossiers, aux enquêtes
  possibles sur les luttes sociales, aux interviews possibles.
- La nécessité d’avoir un lien étroit avec le Secrétariat international pour
  la partie internationale de la revue.
- Un échange sur le contenu pouvant avoir lieu dans les diférentes réunions
  pour aborder le contenu, faire des critiques, et s’assurer que la revue
  n’est pas celle d’un comité de rédaction qui rédige et diffuse à côté
  de la CNT.
- L’obligation de veiller à la lisibilité des textes par le plus grand nombre,
  condition pour que la revue contribue à la formation des militants-es.
- L’obligation de choisir un nouveau titre et un nouveau format (type 21/29,7cm).
- La capacité à assurer sur le site Confédéral ou en lien un espace revue
  de la CNT.
- Le souci d’avoir un correspondant par région.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°57          |            |          |            |                           |
| PTT95                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
