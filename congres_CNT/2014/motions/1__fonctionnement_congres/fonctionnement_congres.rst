
.. index::
   pair: Motions; Fonctionnement 2014

.. _motions_fonctionnement_congres_2014:

===================================
Motions Fonctionnement congres 2014
===================================

.. toctree::
   :maxdepth: 4


   motion_1_2014_sinr44
   motion_2_2014_ste75
   motion_3_2014_sinr44
   motion_4_2014_sinr44
   motion_5_2014_sinr44
   motion_6_2014_sinr44
   motion_7_2014_etpic30
