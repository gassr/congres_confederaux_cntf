.. index::
   pair: trésorerie ; 2021

.. _motion_2021_cnt_09:

===========================================================
Motion N°X 2021 CNT09 trésorerie
===========================================================



Courriel
==========

::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] motion trésorerie
    De:      "CNT Interpro 09" <cnt.09@cnt-f.org>
    Date:    Lun 22 mars 2021 11:15
    À:       "liste-syndicats" <liste-syndicats@bc.cnt-fr.org>
    --------------------------------------------------------------------------

Salut,

Le trésorier confédéral et le syndicat 09, ont modifié l'article 19 des
statuts concernant la trésorerie et ses règles de fonctionnement.

Nous avons fait cela, car un secteur, le Textile, se remet "en route" .

Ce secteur a besoin d'un minimum de gestion. Pour que celle-ci soit
claire, lisible par tous les syndiqués, un compte a été créé.

Et plutôt qu'écrire une ventilation des comptes dans les statuts, qui
serait rigide, nous avons créé une annexe qui peut-être modifiée en
fonction des besoins, qui sera "annexée aux statuts" et qui n'obligera pas
à modifier ceux-ci à chaque fois.

La motion est en :download:`pièce jointe <tresorerie/Modif_motion_tresorerie_03-21.ods>`.

C'est un classeur avec deux onglets:

- Un qui est l'article 19
- et l'autre "l'annexe Trésorerie".


Cette motion ne modifie la motion déjà présentée que pour l'article 19 et
rajoute l'annexe aux Statuts.

Bises violettes noires et rouges,
Christian - CNT09
