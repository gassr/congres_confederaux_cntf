
.. _amendement_motion_60_2010_ETPRECI35:


=====================================
Amendement à la motion 60 (ETPRECI35)
=====================================

Conservation de la phrase : «Cet avis sera soumis pour validation effective à l’approbation d’un CCN.»

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°60 |            |          |            |                           |
| ETPRECI35                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_60_2010`
