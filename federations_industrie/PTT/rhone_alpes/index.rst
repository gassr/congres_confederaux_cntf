
.. index::
   pair: BRA; Bourgogne Rhône-Alpes

.. _ptt_rhone_alpes:

===============================
PTT Rhône-Alpes
===============================

Adresse
=======

::

	CNT PTT BRA (Bourgogne-Rhône Alpes)
	44 rue Burdeau
	69001 LYON
	Courriel : cnt.ptt69@cnt-f.org


Syndicats
=========


.. toctree::
   :maxdepth: 4

   69/index
