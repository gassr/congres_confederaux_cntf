.. index::
   pair: Motion 21 ; 2021

.. _motion_21_2021_stp67:

================================================================================================================================================
Motion **n°21** 2021: **Redéfinition dynamique des Unions Régionales** par **STP 67**
================================================================================================================================================


Autres motions STP67
======================

    - :ref:`syndicats:motions_stp67`

Argumentaire
==============

Les statuts de la CNT prévoient que tout syndicat membre de la confédération
doit adhérer à sa fédération d'industrie, à son union locale et à son union
régionale (Art. 2).

A propos des UR il est précisé que leur délimitation et leur nombre sont fixés
par le Congrès confédéral (Art 11).

Les règles de constitution des UR (3 syndicats au minimum) et les décisions
prises en Congrès par le passé (nombre et délimitation des UR) font
qu'aujourd'hui un certain nombre de nos syndicats ne peuvent se constituer en UR
réglementairement valides et ne peuvent donc, à travers leur UR, participer à
l'administration de confédération par le comité comité confédéral national (CCN).

La présente motion a pour objectif d'éviter qu'un syndicat soit évincé de façon
durable de toute possibilité de participation à l'administration de la CNT par
le CCN en instaurant une redéfinition dynamique des UR par le congrès confédéral.

Motion
=======

Ajout dans le chapitre **Structuration & cohésion confédérales**  les articles
suivants :

Article 1
----------

Les syndicats qui en raison de leur nombre ne peuvent se constituer en unions
régionales telles que définies jusque là par le Congrès confédéral, sont autorisés,
durant toute réunion du Congrès confédéral, à proposer de constituer de nouvelles
UR en se regroupantavec d'autres syndicats.

Article 2
-----------

La remise en cause de la définition d'une union régionale ne pourra être acceptée
que si au moins un majorité des deux tiers (ou majorité qualifiée) des syndicats
la composant donne son accord.
Cette remise en cause peut s'inscrire dans une logique de fusion ou de morcellement
restant compatible avec les règles de constitution des unions régionales.


Amendements
===========


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°21 2021 STP67      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
