
.. index::
   pair: glossaire ; CNT
   ! Glossaire

.. _glossaire_cnt:

=============
Glossaire
=============

- :ref:`glossaire_juridique`


.. glossary::
   :sorted:

   action directe
   Action directe
       L’**action directe** est la base incontournable du syndicalisme de la CNT
       (comme  l’affirmait déjà les Congrès de paris 93, 96, 2001 et :ref:`motion 56 2010 <motion_56_2010>`
       L’action directe c’est l’idée toute simple que ce sont les personnes
       directement concernées qui agissent en définissant, menant et
       contrôlant collectivement leur mouvement.voir :term:`démocratie directe`
       Nous devons réaffirmer, loin de tout radicalisme contemporain voulant
       faire de l'action directe un mode d'action violent, l’action directe
       comme une pratique syndicale où ce sont les travailleurs eux-mêmes qui
       interviennent directement dans leur lutte, à tous les niveaux et à
       toutes les étapes, sans recours aucun à des spécialistes de la
       représentation et de la négociation.


   adelphique
   adelphiques
   Adelphique
   Adelphiques
   adelphité
       De relation fraternelle et/ou sororale.

       .. seealso:: :ref:`motion_20_2021_etpic30`


   Adoption d'une motion
       Le "quorum" pour qu'un congrès confédéral soit valide est de la moitié
       des syndicats de la CNT à jour de cotisations au 4e mois précédent le
       Congrès. Une motion est adoptée si au moins la moitié des syndicats
       présents au Congrès vote pour.

   amendement
   Amendement
       Sert à apporter une modification à une :term:`motion`.
       Il doit porter sur la proposition débattue et doit être proposé
       et appuyé.
       A noter qu'un amemdement doit être en principe en accord avec la motion et ne
       vouloir changer qu'un détail (le sens de la motion doit demeurer le même).
       Les amendements sont rédigés lors du vote des motions en AG de syndicat.
       Des amendements peuvent être proposés lors du Congrès au regard des
       débats et des différentes propositions des syndicats.

       Le congrès confédéral ne procéde à l'examen des propositions
       d'amendements que lorsque la motion initiale a été adoptée par
       le congrès.


   Anarcho-syndicalisme
       syndicalisme se réclamant de l'anarchisme dans les moyens et la finalité.
       Proche du :term:`Syndicalisme révolutionnaire`, mais pas strictement.

   Annuaire des syndicats
       L'annuaire des syndicats disponible en ligne ici:

           - http://www.cnt-f.org/annuaire-des-syndicats-et-unions-locales-de-la-cnt.html



   Ateliers
       Les ateliers sont des moments où les délégués se répartissent pour
       travailler autour de thèmes et produire du matériel (textes, affiches,
       slogans...). Ces travaux sont préparés en séance plénière avant l'atelier.
       Les ateliers rendent ensuite compte de leur travail en séance plénière
       pour adoption.


   A.G
   AG
   assemblée générale
   Assemblée Générale
   Assemblées générales
       Réunion des adhérents du syndicat destiné à la prise de décision
       collective visant à élaborer les orientations et actions du syndicat
       (ou de la structure correspondante).


   ASA (Autorisation Syndicale d'Absence)
       eiste dans certains services publics, ces autorisations d'absence sont
       définies par l'article 13 du décret 82/447. Elles donnent droit à 10 jours
       d'absence pour participation aux congrès syndicaux. La demande doit être
       faite en général un mois à l'avance (même si certains textes parlent de
       8 jours). Il est possible de demander des jours de plus en plus des dates
       du congrès pour le temps de trajet.

   Antifascisme
       La lutte contre le fascisme est l'un des combats de la C.N.T.


   autogestion
   Autogestion
   autogestionnaire
   Autogestionnaire
       En partant du principe que l’autogestion sera le moteur de la société
       future et que **celle-ci n’est pas innée**, mais s’apprend, se pratique
       et se confronte à la réalité, nous devons la mettre au centre de nos
       dynamiques et de nos initiatives.

   A.S
       Anarcho-syndicalisme


   ASSR
   AS & SR
       Anarcho-syndicalisme et Syndicalisme Révolutionnaire

   Autonomie d'action
       Notre autonomie d’action s’exprime principalement au travers des
       principes de :term:`démocratie directe` et d’:term:`action directe` revendiqués comme
       pratique syndicale

   BC
   B.C
   bureau confédéral
   Bureau confédéral
   Bureau Confédéral
       Le Bureau confédéral est l’agent d’exécution et de coordination de la C.N.T.
       Il doit veiller au **respect des statuts et des décisions de Congrès** et de :term:`C.C.N`.

       Son mandat est donc:

       - de coordonner les activités des structures confédérées,
       - d’administrer l’organisation avec l’aide technique des mandatés de la :term:`C.A`,
       - de représenter la Confédération auprès des institutions, des médias
         et des autres organisations, et de développer la :term:`C.N.T`
         suivant la ligne définie par le :term:`Congrès confédéral` et
         les campagnes mises en place, lors de :term:`C.C.N` par exemple.

       Les  commissions  confédérales sont sous contrôle permanent des
       syndicats, mais **aussi** sous la responsabilité du :term:`BC <bureau confédéral>`.

       :Adresse courriel: cnt@cnt-f.org

       .. seealso::

           - CNT Bureau Confédéral <cnt@cnt-f.org>



   commissions
   Commission
       Définition 1
           Prévues dans les statuts confédéraux, elles peuvent être de 2 types:

           - des commissions chargées d'un mandat entre 2 congrès
             (exemple: commission Statuts, commission juridique)
           - des commissions ponctuelles de Congrès (vérification des comptes,
             synthèse de motions, péréquation...)

       Définition 2
           Réunion de personnes chargées d'étudier une question, de contrôler des
           comptes, un mandat, etc.
           Au sein d'un syndicat, elle est constituée par des adhérents travaillant
           sur un thème précis.

   C.A
   CA
   C.A.
   Commission Administrative
   commission administrative
       La grande nouveauté depuis 2008 est la mise en place d’une équipe
       fonctionnant de manière collégiale communément appelée
       :ref:`Commission administrative <statuts_CNT_2010>`.
       Le secrétariat confédéral (:term:`B.C`) se complète et compose avec la C.A.

       La C.A. est composée des différents secrétariats confédéraux mis en
       place par le :term:`congrès`.

       .. seealso::  :ref:`article_6_statuts`

   commission confédérale
   commissions confédérales
       Les  commissions  confédérales sont sous contrôle permanent des
       syndicats, mais **aussi** sous la responsabilité du :term:`BC <bureau confédéral>`.

       Liste des commissions confédérales:

       - la Commission de préparation du congrès confédéral de l'année '2XXX'


   BI
   B.I
   Bulletin intérieur
       Organe de discussion et de débat interne au sein de la :term:`C.N.T`

       :Adresse courriel: ``bi@cnt-f.org``


   Caisse de grève 
       Un pourcentage de la cotisation au syndicat local est versé à la
       caisse séparée, dédiée et touché quand un syndiqué est en grève
       et a son salaire minoré retenu à cause de grève.


   Caisse de solidarité 
       Pour les soutiens aux autres initiatives extérieurs vers les
       autres syndicats et syndiqués ou non syndiqués extérieur du
       syndicat local.

   Caisse de Solidarité confédérale
       La cotisation du mois de janvier (depuis le congrès :ref:`d'Agen 2006 <congres_CNT_2006_agen>`)
       est versée intégralement à la Caisse de Solidarité.

       .. seealso:: :ref:`cotisation_caisse_solidarite_confédérale_agen_2006`


   commutatif
   Commutatif
   commutatifs
       Convention par laquelle chacune des parties s'engage à donner ou à
       exécuter une chose considérée comme l'équivalent de ce qu'on lui donne
       ou de ce que l'on fait pour elle.


   CCN
   C.C.N
   Comité Confédéral National
       Réunion des mandatés confédéraux, des commissions confédérales, des Unions
       Régionales (U.R) et fédérations entre deux congrès.

       Il a lieu tous les 6 mois (3 C.C.N) entre 2 congrès). Ces réunions assurent
       le suivi des décisions de congrès et des campagnes confédérales.

       Elles discutent des propositions des U.R concernant la réaction de la C.N.T
       à l'actualité sociale. Les décisions prises en C.C.N par les U.R (seules à voter)
       ne peuvent aller à l'encontre d'une décision de :term:`Congrès`.


   CdR
   Comité de Rédaction
       Comité de rédaction (des Temps Maudits par exemple)

   Cénétiste
       UnE militantE de :term:`La Confédération`.


   C.E
   CE
   Comité d'Entreprise
       S'agissant du comité d'entreprise (CE), celui-ci n'ayant de pouvoir de
       décision  qu'en matière de gestion des œuvres sociales et ne donnant
       que des avis au  patron dans la gestion de l'entreprise, est
       l'institution type de collaboration  de classes que rejette la CNT,
       d'autant plus que ses membres sont élus sans mandat précis et sont
       irrévocables pendant deux ans.


   Circulaire
   Circulaire confédérale
       La **circulaire confédérale** est l’élément essentiel de liaison: des
       documents y sont compilés, des infos sur ce qui se fait, des évènements
       locaux, des rappels, des sollicitations, appels à contribution, des
       tracts, etc. s’y trouvent afin que chaque syndicat puisse agir en étant
       informédes activités du :term:`BC` et de la :term:`C.A`.


   Charte d'Amiens
       adoptée en 1906 par la CGT dans un congrès. Ce compromis entre anarchistes
       et socialistes stipule que "la CGT groupe, en dehors de toute école politique,
       tous les travailleurs conscients de de la lutte à mener pour la disparition
       du salariat et du patronat"


   CS
   C.S
   Combat Syndicaliste
       Le Combat Syndicaliste est un élément important et complémentaire présentant
       les luttes, activités locales et internationales de façon plus détaillée
       que le bulletin intérieur.


   Compte rendu d'activité des mandaté(e)s
       Les congrès sont l'occasion, en ouverture, de rendre les mandats.
       Chaque mandaté propose, si possible par écrit, un bilan de son mandat.
       Les adhérents décident alors de lui donner ou non le :term:`quitus`.

   Coordination Anarcho-syndicaliste
       dite coordination "RedBlack"

   Cotisation
       L’engagement syndical se traduit par un premier acte militant:
       s’acquitter de sa cotisation.

   CP
   Communiqué de presse
       Communiqué de presse.


   contre-motion
   contre-motions
   contre motion
       Une contre-motion constitue une proposition qui entre en contradiction
       avec le sens de la motion initiale.
       Elle n'est discutée et soumise au vote que si la motion à laquelle
       elle s'oppose **n'est pas adoptée**.


   La Confédération
   C.N.T
   Confédération
   Confédération Nationale du Travail
       À l'origine, la `Confédération Nationale du Travail <http://fr.wikipedia.org/wiki/Conf%C3%A9d%C3%A9ration_nationale_du_travail_%28France%29>`_
       est une confédération syndicale française créée en décembre 1946 à Paris.
       Cette organisation a été fondée par des militants de la Confederación
       nacional del trabajo (CNT espagnole) en exil, des anciens membres de
       la CGT-SR (SR pour syndicaliste-révolutionnaire) ainsi que des jeunes
       ayant participé à la Résistance, qui quittèrent la CGT du fait de la
       mainmise du PCF sur cette organisation. Elle a pris son nom en référence
       à la CNT espagnole.
       Appelé :term:`La Confédération`.

       .. seealso:: http://www.cnt-f.org/

   congrès
   Congrès
   Congrès confédéral
       Réunion statutaire des délégués de tous les syndicats de la Confédération
       fixant les grandes orientation de la C.N.T et désignant les mandatés(e)s
       confédéraux. Il se déroule tous les deux ans.

       Un mode d'emploi de la C.N.T est disponible auprès du B.C.

       .. seealso::

          - :ref:`fonctionnement_congres_confederal`


   Congrès de syndicat
       Réunion statutaire des adhérents du syndicat durant laquelle sont prises
       les différentes orientations générales et désiné(e)s les mandaté(e)s du
       syndicat.


   Déléguée(e)s
       Les syndicats envoient des délégués aux divers congrès et réunions de
       structure (U.L, U.R, fédérations, etc.) pour les représenter et porter leurs
       mandats sur les motions. Les délégués s'expriment au nom de leur syndicat
       et non en leur nom propre.

   démocratie directe
   Démocratie directe
       Notre autonomie d’action s’exprime principalement au travers des
       principes de démocratie et d’action directe revendiqués comme pratique
       syndicale. La pratique de l’:term:`action directe` comme mode d’action n’est que
       le prolongement des conceptions de démocratie directe.
       Le moteur interne de notre fonctionnement à tous les niveaux doit être
       la pratique de la démocratie directe.

   DS
   D.S
       Délégué(e) Syndical

   Entraide
       facteur et condition de l'évolution sociale.

   Ethique
       est à la morale ce que l'anarchie est au gouvernement, fondée sur l'immanence
       et non la transcendance des principes.

   ETPICS
       Syndicat des Employés, des Travailleurs et des Précaires des Industries,
       du Commerce et des Services.

       .. seealso::

          - :term:`STICS`
          - :term:`STIS`

   Exploité
       Au plan individuel, il n'est pas nécessaire au départ d'être
       révolutionnaire pour adhérer à la C.N.T. Notre but est d'attirer le plus
       grand nombre de travailleurs dont la caractéristique commune est
       *le statut d'exploité*.

   FTTLA
       Fédération des Travailleurs des Transports,de la Logistique et Activités
       auxiliaires.

       .. seealso::  :ref:`federation_des_transports`

   I.C
       Imprimerie Confédérale.


   Intranet confédéral
       Un des moyens de communication employé par la C.N.T

   Label confédéral
       validation de la création et de la reconnaissance d'un syndicat par la
       CNT.

   mandat
   mandats
   Mandat
       2 sens:

       - les tâches prises en charge par un(e) mandaté(e) (ex: mandat de trésorier)
       - ou bien les décisions du syndicat portées par le délégué aux diverses
         réunions statutaires (les votes des motions, les :term:`quitus`...)

   Mandatements
       Il existe différents types de mandats que le syndicat peut confier à
       son/sa délégué(e):

       - Le :term:`mandat` **ferme** (ou **fermé**)
         Le syndicat s’est réuni en AG et a pris position de manière ferme sur
         un certain nombre de points, il mandate un(e) de ses syndiqué(e)s pour
         faire part de ses décisions et travailler à la réalisation de ses
         objectifs.
         Le ou la mandaté(e) a alors une mandat très clair qui a été entièrement
         planifié par l’AG du syndicat.

         + **Avantages** : le travail de la ou du mandaté(e) est grandement
           facilité, elle ou il n’a pas de souci à avoir quant à son respect
           des positions de son syndicat.
         + **Inconvénients** : il n’y a pas de place pour la discussion, si
           tous les mandats étaient de ce type, nous n’aurions plus besoin de
           nous déplacer pour un congrès, il suffirait de voter par
           correspondance sur les motions.

       - Le :term:`mandat` **semi ouvert**
         Le syndicat s’est réuni en AG et a pris position, mais il laisse une
         marge de manœuvre à sa ou son mandaté(e) afin de réagir aux discussions
         qui ont lieu lors du congrès. Le ou la mandaté(e) connait parfaitement
         la position de son syndicat qui lui a laissé une possibilité
         d’interprétation de ses positions.

         + **Avantages** : le syndicat se garde une possibilité de réagir aux
           échanges par la voix de son ou sa mandaté(e)
         + **Inconvénients** : la ou le mandaté(e) doit être particulièrement
           vigilant(e) lors de la réunion afin de respecter scrupuleusement
           son mandat

       - Le :term:`mandat` **ouvert**
         Une AG du syndicat a eu lieu durant laquelle les membres se sont
         exprimés autour des sujets relevant du mandat, mais il n’y a pas eu
         de décisions fermes. Le ou la mandaté(e) doit alors faire des choix en
         essayant de respecter «l’esprit» du syndicat.

         + **Avantages** : le syndicat peut parfois gagner beaucoup de temps en
           évitant par exemple de discuter chaque motion et en les regroupant
           par thème afin d’avoir une discussion sur l’ensemble de celles-ci.
         + **Inconvénients** : la tâche de la ou du mandaté(e) peut devenir
           plus difficile car il n’est pas toujours aisé de traduire des prises
           de position générale en décisions précises.

       - Le :term:`mandat` **en blanc**
         Il n'y a pas eu de discussions au sien du syndicat et celui-ci fait
         entièrement confiance à son ou sa mandaté(e) pour le représenter et
         prendre des décisions.

         + **Avantages** : le gain de temps est énorme et peut permettre à un
           syndicat de continuer à "fonctionner" lorsque celui-ci traverse une
           période difficile/
         + **Inconvénients** : aucune discussion n'a lieu au sein du syndicat
           autour des points traités et donc pas d'auto-formation des
           syndiqué(e)s, de plus ils ou elles risquent fort que le ou la
           délégué(e) ne représente que lui/elle même.

       Quel que soit le type de mandatement, le ou la mandaté(e) devra répondre
       de ses décisions devant son syndicat.

   motions
   motion
   Motion
       Proposition sous forme de texte définissant avec précision son objectif
       et les modalités de sa mise en application.
       Une motion est généralement appuyée et expliquée par un argumentaire distinct.


   Ordre du jour
   ordre du jour
       L’:term:`assemblée générale` d'un syndicat 'est préparée par l’envoi à
       chaque adhérent d’un ordre du jour proposé, que **chacun peut compléter**.


   OS
       Organisation syndicale


   Outils confédéraux
       Liste des outils confédéraux:

       - :term:`Recueil des résolutions confédérales`


   Pacte confédéral
       .. seealso:: :ref:`regles_organiques`

   postmaster
   postmaster@cnt-f.org
       Le postmaster (postmaster@cnt-f.org) s’occupe des adresses mail et listes @cnt-f.org


   Présidence de séance
       Un(e) ou pluisieurs (c'est mieux !) déléguée(s) prennent en charge la
       présidence de séance pour une partie du :ref:`congrès <congres_CNT>`
       (une demi-journée en général).
       Il s'agit de faciliter le déroulement de la réunion.

       La présidence rappelle **l'ordre du jour** et les éventuelles modifications.

       - accorde le droit de parole / dirige l'Assemblée au niveau des
         procédures et des discussions
       - rappelle à l'ordre tout membre qui ne respecte pas l'ordre des
         procédures et des discussions
       - décide des points d'ordre
       - procède au relevé des décisions qui émergent des discussions pour
         éventuellemement les soumettre au vote après s'être assurée qu'elles
         sont comprises par tous et toutes.

   présomption d'innocence
   Présomption d'innocence
       Source: https://fr.wikipedia.org/wiki/Pr%C3%A9somption_d%27innocence

       La présomption d'innocence est un principe juridique selon lequel
       toute personne qui se voit reprocher une infraction est réputée
       innocente tant que sa culpabilité n’a pas été légalement démontrée.

       La plupart des pays d'Europe reconnaissent et utilisent le principe
       de la présomption d'innocence (`article 6 de la Convention européenne des droits de l'homme <https://fr.wikipedia.org/wiki/Article_6_de_la_Convention_europ%C3%A9enne_des_droits_de_l%27homme>`_).

       Ce concept est distinct de la relaxe ou de l'acquittement au
       bénéfice du doute, qui en est une application particulière.

       Motions relatives à présomption d'innocence

       - :ref:`contre_motion_12_24_2021_cnt30`


   Propagande
       Mandat de la CNT pour faire de la propagande. Gestion des besoins en
       matière de matériel confédéral, assurer les tirages nécessaires,
       susciter la réalisation d’affiches et de tracts, répondre aux
       propositions reçues des syndicats.
       Peut aussi gérer la mise en place d’une politique éditoriale
       confédérale, brochures.


   Quitus
   quitus
       Acte qui arrête un compte/un mandat et qui reconnaît l'exactitude de la gestion
       de celui qui le tenait.
       Chaque mandat au sein du syndicat est validé par le vote d'un quitus en
       congrès de syndicat.


   quorum
   Quorum
       En droit, le quorum est le nombre minimal de membres d'un corps
       délibératif nécessaire à la validité d'une décision. C'est souvent la
       moitié des membres, mais beaucoup d'entités ont un pré requis plus bas
       ou plus haut.
       Lorsque le quorum n'est pas atteint, le corps délibératif ne peut pas
       tenir de vote et ne peut pas changer le statu quo. Ainsi, les votants
       en faveur du statu quo peuvent bloquer une décision en ne se présentant
       pas au vote. Le vote sera alors automatiquement rejeté et le
       statu quo conservé.

       .. seealso::

           - http://fr.wikipedia.org/wiki/Quorum
           - :ref:`motion_2_2012_sanso69`


   Recueil des résolutions confédérales
       Le recueil des résolutions confédérales en vigueur est un ouvrage
       synthétique des :ref:`différentes motions adoptées <recueil_motions_adoptees_cnt>`.
       Fait partie des outils confédéraux.


   Révolution espagnole (1936-1937)
       Réalisations révolutionnaires (collectivités, municipalisations, gestions
       directes, milices...) permises par la puissance des anarcho-syndicalistes
       au sein du camp républicain.


   R.S.S
       Représentant de Section Syndicale


   SC
   S.C
   Secrétaire confédéral
   Secrétariat confédéral
       Agent d’exécution et de coordination.

       .. seealso:: :term:`Bureau Confédéral`


   S.C.A.A
       Secrétaire Confédéral Adjoint Administratif


   Secrétariat relations médias
   Relations médias
       Mandat de la CNT pour communiquer avec les médias.
       relations-medias@cnt-f.org

   Séance plénière
       Moment où l'ensemble des délégué(e)s se retrouvent ensemble (par opposition
       aux temps de travail en commissions).

   SGTL
   Syndicat Général des Transports et de la logistique
       Syndicat Général des Transports et de la logistique

   S.I
   SI
   Secrétariat International
       Le Secrétariat International est composé d’adhérents de toute la
       confédération et est représenté par trois mandatés, désignés par le
       congrès confédéral.
       Le travail du Secrétariat International se justifie par le développement
       des relations internationales, mené par les syndicats et les militants
       eux-mêmes: il apporte les conditions nécessaires pour que les relations
       de solidarité puissent se développer et se renforcer.
       Ainsi donc, il se structure à partir de groupes de travail (GTs) qui
       consolident leurs échanges selon différentes zones géographiques, suivant
       de près leurs problématiques spécifiques.

       .. seealso:: :ref:`secretariat_international_CNT_public`


   S.J
   SJ
   Secrétariat Juridique
       Secrétariat Juridique créé lors du Congrès de Saint-Etienne en 2010.

       .. seealso::  comm.juridique.conf@cnt-f.org



   Soviet
       Conseil des travailleurs dans l'entreprise qu'ils occupent.
       Le premier est fondé par des anarchistes en 1905 à Saint Pétersbourg.


   STE
       Syndicat des travailleuses et travailleurs de l’éducation.

   STICS
       Syndicat des Travailleurs de l’Industrie, du  Commerce et des Services

       .. seealso::

          - :term:`ETPICS`
          - :term:`STIS`

   STIS
       Syndicat des Travailleurs de l’Industrie et des Services.

        .. seealso::

           - :term:`ETPICS`
           - :term:`STICS`

   STTLA
       Syndicat des Travailleurs des Transports, de la Logistique et des
       Activités auxiliaires.

   SR
   Syndicalisme révolutionnaire

       .. seealso:: :ref:`syndical_rp_2018`

       Théorie et pratique du syndicalisme fondé sur la :ref:`Charte d'Amiens <chartes_amiens>`

   Patriarcat
   patriarcat
   patriarcales
   patriarcale
   Système patriarcal
       Définition 1 système patriarcal
           Source: https://fr.wikipedia.org/wiki/Patriarcat_%28sociologie%29

           Le patriarcat est, dans son acception moderne, «une forme d’organisation
           sociale et juridique fondée sur la détention de l’autorité par
           les hommes, à l'exclusion explicite des femmes».

           Il s'agit d'un «système où le masculin incarne à la fois le
           supérieur et l'universel».

           Dans une culture dite patrilinéaire en effet, comme c'est le
           cas des sociétés occidentales, l'homme occupe une position
           mythique de « père fondateur » supposée lui octroyer une
           autorité et des droits sur les personnes dépendant de lui.

           Aujourd'hui, l'organisation patriarcale est critiquée et contestée.
           À partir des années 1970, le concept de patriarcat, revisité
           dans ses fondements théoriques, est notamment utilisé par la
           deuxième vague féministe pour désigner un système social
           d'oppression des femmes par les hommes.

           Voir: `Critiques_féministes du concept <https://fr.wikipedia.org/wiki/Patriarcat_%28sociologie%29#Critiques_f%C3%A9ministes_du_concept>`_

       Définition 2 système patriarcal
           source: **Aux origines de la catastrophe**, éditions Les liens qui libèrent, 2020, ISBN: 979-10-209-0834-6
           Chapitre '**Le patriarcat**' de Charlotte Luyckx, p.90,91

           "Selon `Mary Mellor <https://greattransition.org/contributor/mary-mellor>`_,
           le **patriarcat** peut être défini comme une matrice de valeurs,
           de symboles, de croyances et de pratiques qui **imprègne
           la plupart des cultures humaines depuis l'aube de l'humanité**.

           Cette matrice présente les caractéristiques suivantes: un système
           marqué par une domination du masculin sur le féminin, c'est à dire
           un système dans lequel **le masculin jouit d'une prévalence de principe
           sur le féminin** (*le masculin l'emporte*) et dans lequel le féminin
           est **non seulement dévalorisé, mais également invisibilisé**
           (*l'universel est masculin*).

           A ces deux pôles correspondent, **particulièrement en Occident**,
           toute une série de binômes eux aussi hiérarchisés qui leur sont
           symboliquement associés (raison-émotion, actif-passif, culture-nature,
           esprit-corps, sujet-objet, productif-reproductif, transcendant-immanent).
           Ils forment ce que `Karren Warren <https://en.wikipedia.org/wiki/Karen_J._Warren>`_
           appelle des **cadres conceptuels d'oppression** qui **structurent l'organisation sociale**.

           ...

           Penser le patriarcat comme méta-cause c'est le voir comme cause
           d'autres causes et **établir ainsi qu'il possède à leur égard un
           caractère structurant** c'est à dire une certaine prévalence logique
           et historique.

           ...

           A titre d'exemple, voici 3 causes mobilisées dans la réflexion sur
           les racines de **la crise écologique** dont on peut considérer qu'elles
           ont le patriarcat comme **méta-cause**: le capitalisme, l'anthropocentrisme,
           la négation du sacré immanent."

       Motions relatives au système patriarcal

           - :ref:`motion_synthese_24_2021_ste38_stp_67`

   Syndicat
   syndicats
   syndicat
       Structure de base de la C.N.T.

       Le rôle des syndicats interco et/ou interpro, essentiels dans un
       premier temps pour pouvoir grouper les syndiqué-e-s isolés
       dans leur secteur professionnel, doit être de favoriser et d'aider
       à la création de syndicats professionnels dans différents domaines en
       lien avec ses adhérents, tout en offrant un espace de solidarité et de
       **formation syndicale**.

       Le syndicat professionnel d'appartenance et ses assemblées générales
       restant l'**organe de base des décisions**.

       Le syndicat doit jouer un **rôle d'éducation populaire** permanent
       par **l'auto formation** et l'organisation régulières de débats, internes
       comme externes, la diffusion de nombreuses publications et l'édition de
       journaux, revues ou livres.

   synallagmatique
   Synallagmatique
   synallagmatiques
       Lorsque les contractants s'obligent réciproquement les uns envers les autres.

       .. seealso:: :ref:`motion_20_2021_etpic30`


   tribune
   Table du Congrès
       la table du congrès est composée du ou des président(e)s de séance, du
       ou des preneurs de tours de paroles, du ou des preneurs de notes.
       Elle est renouvellée à chaque demi-journée et elle est définie en
       ouverture de Congrès.


   Video
   Secteur vidéo
       Commision chargée de faire des vidéos concernant la CNT.

       .. seealso::

          - http://www.cnt-f.org/video/
          - https://twitter.com/#!/SecteurVideoCnt

   Agression sexuelle
   viol
   Violence sexuelle
       Source: https://fr.wikipedia.org/wiki/Agression_sexuelle

       Une agression sexuelle (ou violence sexuelle) désigne tout
       acte de nature sexuelle, non `consenti <https://fr.wikipedia.org/wiki/Consentement_sexuel>`_.

       Dans la plupart des pays la législation indique que l'agression
       doit être imposée par une contrainte physique ou psychologique
       pour être considérée comme un crime, ce qui n'est pas le cas
       notamment au Canada, où la contrainte n'est pas un critère pour
       caractériser l'acte d'agression sexuelle.

       Certaines juridictions traitent de façon distincte l'agression
       impliquant une pénétration, spécifiquement désignée comme un **viol et constituant un crime sexuel**.

       En droit français, « constitue une agression sexuelle toute
       atteinte sexuelle commise avec violence, contrainte, menace
       ou surprise ».

       Le Code pénal distingue:

       - le `viol <https://fr.wikipedia.org/wiki/Viol>`_, `crime <https://fr.wikipedia.org/wiki/Crime_en_France>`_ caractérisé par un
         acte de pénétration sexuelle (et en tant que tel jugé par la `cour d'assises <https://fr.wikipedia.org/wiki/Cour_d%27assises_(France)>`_),
         objet du paragraphe premier de la section III
         du code pénal,
       - et les « autres agressions sexuelles », objets du paragraphe
         second et qui sont les faits d'agression sexuelle
         stricto sensu, `délit <https://fr.wikipedia.org/wiki/D%C3%A9lit_p%C3%A9nal>`_
         jugé par le `tribunal correctionnel <https://fr.wikipedia.org/wiki/Tribunal_correctionnel_(France)>`_.

       Motions en rapport au viol, violence sexuelle

       - :ref:`motion_synthese_24_2021_ste38_stp_67`



   TC
   T.C
   Trésorerie Confédérale
       La trésorerie confédérale de la CNT.


   TM
   T.M
   Temps maudits
   Temps Maudits
   Les Temps Maudits
       Revue semestrielle de la C.N.T abordant de façon plus détaillée et approfondie
       les questions et actualités du syndicalisme que le :term:`C.S`.


   UD
   U.D
   Union Départementale
      Union départementale de :term:`syndicats`.
      Il s'agit sur le plan d'un département d'un regroupement de syndicats
      départementaux de branches avec éventuellement des :term:`ULs`.
      C'est à ce niveau que se définissent les orientations pour assurer
      le développement de la CNT sur la zone géographique.

      **L’UD n’a rien à dire sur les questions internes d'un syndicat**.

      Cette AG ne traite que des informations apportées par les différents
      syndicats sur leurs problématiques pour en faire des actions locales
      à mener. Ou tout simplement prévoir des affichages, dire qui pourra
      être aux permanences etc, rien que du concret vers l’extérieur de la CNT.

   ULs
   Union Locale
      L'Union Locale regroupe des syndicats sur un plan territorial ou géographique.
      C'est donc un lieu interprofessionnel où des syndicats de branches
      professionnelles différentes se retrouvent, dépassant le combat catégoriel
      pour lutter ensemble sur des revendications communes de type économiques
      (retraites, temps de travail, etc. ou plus politiques: combat contre le
      racisme, défense de locataires ou mal-logés, de sans-papiers, antifascisme
      solidarité, etc.) En principe les UL se constituent sur une ville ou un
      secteur géographique. **C'est au niveau de l'UL que se décident les
      orientations locales** pour développer la CNT sur la ville.

   UR
   U.R
   Union Régionale
   unions régionales
       C'est le regroupement sur un plan géographique des syndicats d'une même
       zone qui peut regrouper plusieurs départements. C'est le le lieu de coordination
       de l'action de la CNT sur le plan de la région.

       .. seealso::

          - :ref:`UR_CNT`
          - :ref:`urra`

   URP
   Union Régionale Parisienne
       Union régionale parisienne


   webmaster@cnt-f.org
   webmaster
       Le webmaster (webmaster@cnt-f.org) s'occupe du site confédéral
       (http://www.cnt-f.org) de sa «une», de la modération des post publiés
       sur le fil d’actualité et de l’ouverture de sites web @cnt-f.org
       pour les syndicats, les UR, etc

   Zen
       Il faut le rester dans tous les cas, et si ce n'est pas toujours évident
       c'est indispensable !
