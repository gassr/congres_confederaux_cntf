.. index::
   pair: Motion 2 ; 2021

.. _motion_2_2021_staf29:

================================================================================================================================================
Motion **n°2** 2021 **Antiproductivisme** par **STAF29-CNT**
================================================================================================================================================

Autres motions STAF29

    - :ref:`syndicats:motions_staf29_congres`


Argumentaire
=============

Le productivisme capitaliste et étatique conduit à une dégradation sensible
des conditions de vie sur terre.

**En s’appuyant sur l’exploitation des humains**, le productivisme réduit l’humanité
à ses dimensions productrices et consommatrices.

**En s’appuyant sur l’exploitation des ressources naturelles**, le productivisme
détruit la nature et entrave sa capacité à se régénérer.

Dans la deuxième moitié du 20ème siècle et aujourd’hui encore, beaucoup de
mouvements sociaux se sont appuyés sur l’imaginaire productiviste pour établir
des lignes revendicatives et construire des argumentaires:

- augmentation de la productivité du travail,
- partage des *fruits de la croissance*,
- sauvegarde de l’emploi quelles qu’en soient la nature et la fonction,
- augmentation du pouvoir d’achat...

Si l’introduction a minima du prolétariat à la société de consommation de
biens matériels et de loisirs a pu constituer pour certains et à certains
moments un compromis acceptable à l’exploitation salariale, aujourd’hui le
**caractère mortifère** de telles revendications ne peut plus être contesté.

Motion
=========

Les syndicats constituant la CNT visent:

- par les textes qu’ils publient,
- par les formations qu’ils organisent,
- par les luttes sociales où ils agissent,

à **déconstruire l’imaginaire productiviste** et ses corollaires : l’exploitation
salariale et le consumérisme.

Les syndicats inscrivent leurs réflexions et leurs actions d’une part dans une
volonté d’émancipation culturelle des travailleurs et d’autre part, dans une
**volonté farouche de protéger les équilibres naturels**.

**Nous voulons des vies riches, pas des vies de riches !**

Amendements
===========

- :ref:`amendement_motion_2_2021_sest_lorraine`
- :ref:`amendement_motion_2_2021_interpro31`
- :ref:`amendement_motion_2_2021_ste72`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°2 2021 STAF29      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
