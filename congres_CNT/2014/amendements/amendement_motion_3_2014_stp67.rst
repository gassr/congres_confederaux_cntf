
.. _amendement_motion_3_2014_stp67:

================================================
Amendement à la motion n°3 2014 (STP67)
================================================

.. seealso::

   - :ref:`motion_3_2014_sinr44`






Argumentaire
=============

Sensibles aux arguments soulevés par la :ref:`motion n°2 <motion_2_2014_ste75>`
et à l'intérêt des échanges et des débats au cours du Congrès, mais ne
souhaitant supprimer le vote par procuration, nous proposons l'amendement
suivant à la :ref:`motion 3 <motion_3_2014_sinr44>`.

Amendement
==========

**Chaque syndicat présent physiquement au congrès ne peut représenter qu'un seul
autre syndicat**.

De plus ne sont prises en compte que les procurations prévues pour l'ensemble
du congrès, et signalées au préalable à la commission d'organisation du
congrès par le syndicat qui donne procuration (autrement dit un syndicat qui
quitte le congrès en cours ne peut pas donner procuration à un autre syndicat
pour finir le congrès à sa place).

En outre, sont uniquement prises en compte les procurations pour les votes
des motions, contre-motions et amendements déposés dans les délais
préalablement au Congrès.

Lorsqu'une motion de synthèse ou une motion modifiée au cours du Congrès
lui-même est portée au vote, seules les voix des syndicats physiquement
présents sont comptabilisées.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°3 2014 <motion_3_2014_sinr44>`

       :ref:`STP67 <stp67>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
