
.. _motion_61_2012:
.. _motion_61_2012_etpic30:

==================================================================================================================================================
Motion n°61 2012 : Modification de la motion «Recueil des résolutions confédérales en vigueur» du 30e Congrès confédéral de 2008 (ETPIC 30 Sud)
==================================================================================================================================================
  

.. seealso::

  - :ref:`motions_etpic30`





Motion 4 2008 EPIC30
====================

- :ref:`motion_4_2008`

Préambule
=========

.. seealso:: :ref:`motion_4_2008`

Rappel de la motion adoptée à 76% des syndicats présents au Congrès:

**"Le Congrès confédéral décide l'élaboration d'un recueil des résolutions
confédérales en vigueur pré-titré CNT - Fonctionnement & Orientations.
Cet ouvrage annule et remplace le recueil des motions en vigueur"**.

**"À l'issue de chaque congrès, le recueil des résolutions confédérales en
vigueur devra être mis à jour par le bureau confédéral ou par voie de
délégation sous sa responsabilité. Les modifications attendues intégreront
les nouvelles dispositions et orientations adoptées.""**

**"Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
des différentes motions adoptées. L'effort de synthèse portera sur l'intégration
des décisions les plus récentes, invalidant, supprimant, voire complétant par
voie de conséquence les décisions antérieures."**

« Le recueil des résolutions confédérales en vigueur est structuré selon deux
axes principaux : Fonctionnement et Orientations. Chacun de ces axes comprend
plusieurs catégories thématiques afin de favoriser tant que possible la
lisibilité de l'ouvrage.

Chaque résolution sera annotée d'un historique des différents congrès
confédéraux ayant apporté des modifications.

**"Afin de conserver l'historique des différentes décisions de Congrès
confédéraux, l'intégralité des motions adoptées est portée en annexe selon la
même classification, annotées des Congrès confédéraux concernés**.

**"Une fois l'ouvrage mis à jour dans des délais raisonnables, il est porté à la
connaissance des syndicats par le Bureau confédéral.
Le CCN le plus proche valide l'ouvrage sur avis du Bureau Confédéral."**

Nous considérons que les dispositions prévues au 2e paragraphe doivent être
modifiées.

En effet, le travail demandé pour l’élaboration de la synthèse prévue au
``Recueil des résolutions en vigueur CNT - Fonctionnement & Orientations`` est
conséquent.

L’effort de synthèse ne peut être placé sous la seule responsabilité
du Bureau confédéral au regard de l’ampleur du travail et de la collégialité
que cela nécessite.

Nous proposons donc, que ce travail puisse être réalisé par une commission
de travail confédérale associant plusieurs syndicats, sous la coordination
du Bureau confédéral.


Motion
=======

Annule et remplace le paragraphe 2 de la motion ::ref:`"Recueil des résolutions
confédérales en vigueur"<motion_4_2008>`  du 30e Congrès confédéral de 2008

À l'issue de chaque congrès, le recueil des résolutions confédérales en vigueur
devra être mis à jour par une commission confédérale de plusieurs mandatéEs
de plusieurs syndicats de la CNT sous la coordination du bureau confédéral.

Les modifications attendues intègrent les nouvelles dispositions et
orientations adoptées par le Congrès.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°61          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
