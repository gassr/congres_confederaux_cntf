
.. _amendement_motion_9_2014_interco25:

================================================
Amendement à la motion n°9 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_9_2014_sinr44`




Argumentaire
=============

Pas de rencontre "d'état-major" au sommet, si on veut faire quelque chose
ensemble le plus important c'est que les militant-e-s se rencontrent et
échangent.

Amendement
===========

Modifier le 1er paragraphe comme suit : "La CNT mandate son secrétaire
confédéral pour qu'il contacte le secrétariat confédéral de la CNT-AIT en vue
d'organiser une rencontre ouverte à tous les adhérent-e-s, sur les bases
suivantes : ...".

Et ajouter un paragraphe à la fin : "Tous les syndicats et unions régionales
concernés (Aquitaine, Languedoc-Roussillon, PACA, Pays de la Loire, Région
parisienne, Rhône-Alpes) sont invités à organiser sans attendre des rencontres
locales ou régionales.


Votes
======

.. list-table::
   :widths: 30 18 18 18 9
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°9 2014 <motion_9_2014_sinr44>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
