
.. _motion_tresorerie_confederale_cnt_congres_lyon_1996:

======================================================================================================
Motion "Trésorerie confédérale" de la CNT  25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
======================================================================================================

Suppression des timbres "précaires/étudiants"
=============================================

Pour éviter toute discrimination entre les adhérents de la CNT : Suppression
des timbres verts, c'est à dire l'utilisation d'une seule couleur quelque soit
le montant de la cotisation.
