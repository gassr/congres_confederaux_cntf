
.. _amendement_motion_17_2012_etpics57:

==========================================
Amendement à la motion 17 (ETPICS57)
==========================================

.. seealso::

   - :ref:`motion_17_2012`



Amendement
===========

... à tous les niveaux de l'organisation, la CNT refuse les permanents et/ou
le salariat.

Toutefois si les syndicats sont confrontés à des difcultés réelles, exclusivement
en lien avec les UR et les fédérations, et selon des conditions multiples et
cumulatives, ils sont autorisés à prendre part à des coopératives et à participer
directement à leur gestion.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°17 |            |          |            |                           |
| ETPICS57                    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
