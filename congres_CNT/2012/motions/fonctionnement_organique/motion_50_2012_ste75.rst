
.. _motion_50_2012:
.. _motion_50_2012_ste75:

===================================================================================
Motion n°50 2012 : Disparition du vote par procuration (Art. 50 6), STE 75
===================================================================================
  
.. seealso::

   - :ref:`ste75`
   - :ref:`motions_ste75_2012`



Préambule
=========

La présente motion a pour but de définir plus spécifiquement le fonctionnement
du Secrétariat international et en particulier le mode de mandatement des
nouveaux secrétaires.

Depuis le Congrès de St Denis en 2004, afin de pouvoir assurer toutes les
responsabilités que ce mandat exige, le SI est composé de trois mandatés.


Argumentaire
============

Grâce aux très nombreux contacts internationaux qui se sont tissés depuis des
années, le Secrétariat international doit faire face à une très grande charge
de travail.

Les enjeux de chaque pays sont divers, les organisations avec lesquelles nous
travaillons ont des particularités et des subtilités qu’on ne peut négliger,
les réseaux et les coordinations internationaux ont un historique qu’il est
également important d’appréhender : assumer ce mandat exige, de la part de ses
mandatés, une bonne connaissance de ces problématiques.

C’est-à-dire que si les nouveaux mandatés n’ont pas ces connaissances-là, le
travail international de la CNT risque de se voir entravé : une période d’un
an est souvent nécessaire pour comprendre tous ces enjeux et pouvoir les
maitriser. Il apparait donc indispensable que, pour assurer la continuité de
ce travail d’un mandat à un autre, les nouveaux secrétaires aient déjà une
expérience, aussi bien du fonctionnement du SI que des diférentes réalités
des pays et des organisations.

Ainsi, pour garantir le fonctionnement du SI, il est souhaitable que les
nouveaux mandatés aient déjà pu participer au travail du Secrétariat
international, soit par leur présence dans la liste internationale, soit par
leur engagement dans un des groupes de travail qui le composent.

Une période de six mois peut être proposée, à titre indicatif.

L’expérience nous prouve, de ce fait, que les nouveaux mandatés ont également
besoin d’être accompagnés par les anciens dans la prise de leur mandat.
Un travail de tuilage nous semble indispensable, travail qui peut être fait
en amont grâce à leur investissement au sein du Secrétariat international et
des groupes de travail et en aval après mandatement.

Motion
=======

Il parait plus que souhaitable que les personnes qui se présenteraient au
mandatement du Secrétariat international aient participé à un de ses groupes
de travail depuis au moins six mois. Au vu des expériences antérieures, une
candidature groupée de trois cénétistes qui se connaissent bien serait
souhaitable.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°50          |            |          |            |                           |
| STE75                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
