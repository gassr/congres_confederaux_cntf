
.. _amendement_motion_22_2014_interco25:

================================================
Amendement à la motion n°22 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_22_2014_etpic30`




Argumentaire
=============

Pas la peine de multiplier les noms de domaines et commissions quand ce qu'on
a déjà permet de faire la même chose en plus simple.


Amendement
===========

Modification du texte comme suit:

"Le Congrès charge les mandaté-e-s au site web confédéral et au Combat
syndicaliste de réaliser la mise en ligne internet de l’édition mensuelle
du Combat Syndicaliste, via une page spécifique du site cnt-f.org dont la mise
en page rappellera celle du journal, dans les délais les meilleurs.

[...]
Ces travaux sont placés sous le contrôle et la validation des CCN qui suivent
le Congrès. [...]"


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°22 2014 <motion_22_2014_etpic30>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
