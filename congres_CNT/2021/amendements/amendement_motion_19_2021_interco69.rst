
.. _amendement_motion_19_2021_interco69:

===========================================================================================
Amendement N°3  la motion n°19 2021 **mise en sommeil d’un syndicat** par **Interco69**
===========================================================================================

- :ref:`motion_19_2021_ste93`

Autres motions Interco69

  - :ref:`rhone_alpes:motions_interco69`


Argumentaire
===============

Tout syndicat de moins de 5 adhérent.e.s doit fonctionner avec l'interprofessionnel
(cf, motion 18 et son amendement), cependant ses statuts doivent être
conservés parce qu'un syndicat de branche est plus solide en prud'homme,
mais aussi parce que ça facilite la relance d'un syndicat en sommeil.

Amendement 
============

Article 1
-----------

Un syndicat (hors interprofessionnel) est déclaré en sommeil lorsqu'il
déclare moins de 5 adhérent.e.s.
Cependant son existence préfectorale doit être conservée.

Article 2
-----------

La décision de mise en sommeil est prononcée 3 mois avant le congrès
confédéral.

Article 3
----------

Entre deux congrès, le bureau confédéral met à jour la liste des syndicats
répondant aux conditions de participation aux congrès en terme de nombre
d'adhérent.e.s, et a pour mandat, avec l'UR de rattachement, d'accompagner
les syndicats qui seraient moins de cinq adhérent.e.s.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion      |            |          |            |                           |
| n°19 Interco69              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
