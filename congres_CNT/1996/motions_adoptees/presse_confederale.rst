
.. _motions_presse_confederale_cnt_congres_lyon_1996:

====================================================================================================
Motions Presse confédérale de la CNT 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
====================================================================================================

Le Combat Syndicaliste
======================

Le "Combat Syndicaliste" reflète les activités de la CNT et, il ne peut ni
par son rôle, ni pour des raisons d'espace, être la tribune d'autres
organisations ;

Concernant le choix des textes individuels internes et externes a la CNT, nous
laissons a l'appréciation du comité de rédaction le soin de faire paraître les
articles les plus riches ;

L'auteur d'un article ayant subi une transformation ou une suppression en sera
informé.

La non parution d'un texte passe obligatoirement par le bulletin intérieur avec
explications de ce refus.

Pour les articles sujets à polémique nous faisons confiance a la rédaction pour
qu'elle les introduise avec une présentation (chapeau) sans qu'elle y porte un
jugement politique.

Il en sera de même pour les modifications d'articles.

Tous les militants, les syndicats sont invités à continuer dans le bulletin
intérieur le débat sur le rôle, les fonctions et le fonctionnement de notre
mensuel.

La croissance qu'a connue notre organisation depuis notre précédent Congrès
lui impose de renforcer et d'améliorer ses outils d'information.

Le mensuel de la CNT doit contribuer à rendre compte de l'activité confédérale
sous toutes ses formes:

- pratiques syndicales
- discussions
- débats théoriques.

Pour que le CS puisse continuer à assurer cette fonction dans les meilleures
conditions, notre syndicat a formulé les propositions suivantes:

- Le comité de rédaction devra comporter au moins six personnes ;
- Le nombre de pages sera porté à 20 voire si nécessaire à 24 par numéro ;
- L'abonnement au CS sera proposé systématiquement à toute personne adhérant
  à la CNT ;
- Chaque union régionale, et dés que cela sera possible, chaque union locale
  (voire chaque syndicat) devront désigner un délégué chargé d'être le
  correspondant local du CS.
- Les correspondants du CS seront en relation régulière avec le comité de
  rédaction et auront notamment pour tâche:

     * de collecter les informations, annonces à faire paraître (fêtes, manifs,
       débats...), dessins, documents divers, projets d'articles, afin de les
       transmettre au comité de rédaction, si possible sous la forme d'une
       disquette accompagnée d'une sortie papier ;
     * de répercuter auprès du comité de rédaction les réactions, les critiques
       (positives ou négatives) relatives au numéro paru.

Tout envoi au CS mentionnera un numéro de téléphone ou tout autre moyen pour
le comité de rédaction de joindre si nécessaire l'auteur de l'article.

Le comité de rédaction est responsable des titres, intertitres, modifications
éventuelles et « chapeaux » des articles publiés.

Les syndicats, les unions locales et les unions régionales s'engagent à
organiser régulièrement des ventes militantes afin d'augmenter la diffusion
de notre mensuel.


Les Brochures
=============

Pour poursuivre sa croissance, notre Confédération doit également produire de
nombreux documents, qu'il s'agisse de guides pratiques (comme "les droits des
CES") ou de brochures traitant de sujets théoriques (telle celle évoquant
l'itinéraire de la CFDT).

Ce Congrès pourrait être l'occasion d'établir une liste des thèmes méritant de
faire l'objet d'une brochure.

La réalisation d'une brochure sera effectuée sous la responsabilité d'un
syndicat d'une union locale ou d'une union régionale, à qui seront adressés
tous les documents, suggestions, contributions.... émanant du reste de
la Confédération.

Dans la mesure du possible, l'édition d'une brochure s'accompagnera d'un envoi
régulier de textes à faire paraître dans le :term:`CS`.


Brochure sur l’Espagne du syndicat éducation 93
===============================================

Une brochure éditée par le syndicat de l'éducation 93 lançant un débat critique
sur la révolution espagnole sans travail commun de plusieurs syndicats vient
de sortir, ce qui est son droit.

Nous demandons que le Congrès publie un texte que nous soumettons au débat.

En effet, que nous soyons d'accord ou pas avec le contenu, les personnes qui
seront mandatées pour nous représenter au Congrès de l'AIT vont se trouver
en position délicate devant nos compagnons espagnols.

Nous demandons que le texte suivant soit envoyé à la CNT Espagnole en exil,
à tous les syndicats et soit publié dans le Combat Syndicaliste : "le syndicat
de l'éducation du 93 vient d'éditer une brochure sur la révolution espagnole.

Celle-ci étant le résultat d'un travail d'un syndicat unique, elle ne saurait
être considérée comme reflétant la position de la CNT dans son ensemble".


Création d’une revue théorique
==============================

Le Congrès vote pour l'idée d'une revue théorique de la CNT.

Elle sera accessible à tous (il n'est pas question de faire une revue élitiste !)
et n'enlèvera pas au CS tout ses textes de fond.

La forme définitive de cette revue et des détails techniques seront adoptés
lors du prochain CCN.
