
.. _amendement_motion_22_2021_stp67:

=======================================================================================================
Amendement N°3 à la motion n°22 **Drogues, alcool et émancipation révolutionnaire** par **STP67**
=======================================================================================================

- :ref:`motion_22_2021_etpics94`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`

Argumentaire
===============

Amendement
============

Ajout à la fin **"Chaque personne présente à la réunion peut ajouter une
substance à la liste des drogues."**.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°22 |            |          |            |                           |
| STP67                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
