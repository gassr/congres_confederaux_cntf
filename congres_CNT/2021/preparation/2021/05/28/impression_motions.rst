
===========================================================================================================================================================
Vendredi 28 mai 2021 Cahier des mandaté.e.s du XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon, par Secrétariat Confédéral <cnt@cnt-f.org>
===========================================================================================================================================================


::

    Objet: Re: [Liste-syndicats] Cahier des mandaté.e.s du XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date: 2021-05-28 11:17
    De: Secrétariat Confédéral <cnt@cnt-f.org>
    À: ptt95@cnt-f.org
    Cc: Liste-syndicats@bc.cnt-fr.org, liste CA <liste.ca@bc.cnt-fr.org>,
    liste-bureau@bc.cnt-fr.org, congres@cnt-f.org


Il ne sera pas envoyé de version papier aux syndicats (décision du
B.C. et de la C.A.) toutefois nous prévoyons d'en imprimer une version
papier qui sera à disposition de chaque syndicat lors du congrès.

Nous étudions la formule la plus économique avec les camarades du 42
qui gèrent le duplicopieur fédéral, la solution la plus simple semble
une impression à l’extérieur ( environ 100 cahier d'environ 160
pages).

Si vous avez des pistes "économiques" merci de nous les communiquer
rapidement.

Au sujet du cahier nous allons rajouter les contres motions que la
commission de préparation n'avait pas reçues dû à la maintenance de
Globenet notamment 2 contres motion du SIM RP (ci-jointes).

Nous vous invitons à vérifier que tous vos motions et contre motions sont bien
présentes dans ce cahier de façon URGENTE afin de vous renvoyer le
cahier COMPLET d'ici la fin de la semaine.

Serge STP26

S.C. provisoire
