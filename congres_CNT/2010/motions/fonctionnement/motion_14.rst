


.. _motion_14_2010:

==============================================================================
Motion n°14 2010 : Règle limitant les possibilités de cumul de mandats, CNT 09
==============================================================================



Argumentaire
=============

Nos statuts confédéraux préconisent la rotation des mandats mais sans limiter leur cumul.
Il y a là une contradiction à laquelle il serait bon de remédier à l'avenir, sans toutefois
remettre en cause les mandats actuels afin d'éviter tout effet rétroactif.

Motion
======

Sauf en cas de carence de candidatures, toute personne déjà en charge d'un mandat syndical
n'est pas habilitée à en briguer un autre.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°14          |            |          |            |                           |
| CNT 09               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso::

   - :ref:`contre_motion_12_13_14_2010_interco68`
   - :ref:`amendement_motion_14_2010_ssct_rp`
