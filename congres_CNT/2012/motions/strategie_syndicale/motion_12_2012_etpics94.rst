.. index::
   pair: Gestion; Débats

.. _motion_12_2012:

=========================================
Motion **n°12** 2012: Débats ETPICS 94
=========================================
  
.. seealso::

   - :ref:`motions_etpics94`
   - :ref:`motion_38_2012_etpics94`



Autres motions ETPICS94
=========================

.. seealso::

   - :ref:`motions_etpics94`

Argumentaire
============

Depuis plusieurs années, certains mettent en avant le fait que si ce
n’est pas dans les statuts, c’est que c’est autorisé.

Nous portons un projet humain, de lutte des classes, fondé sur la discussion et
le débat. Sinon pourquoi nous réunissons nous en AG, en UR, en CCN et en congrès ?
Notre but est de nous émanciper, pour cela il faut que ca est du sens et que ca
prenne sens.
**Pour y parvenir la discussion et le débat sont incontournables**.

En ayant ce genre de raisonnement on a des situations qui déstabilisent la CNT.

- Il y a eu le cas des motions groupées au congrès régional de 2011.
- Le cas Étienne Deschamps, qui a caché pendant 6 ans qu’il était salarié au
  syndicat du nettoyage à la confédération et la région parisienne.
- Le 1er mai 2011 apparition d’un autre cortège dit « CNT-UTS ».

Toutes ces situations ont déstabilisé la CNT RP

Nous sommes contre la confusion des genres, la compromission avec les politiques, etc.

**Toutes ces choses favorisent la confusion, l’incompréhension**.

Le fait d’apprendre qu’il y a un permanent dans un syndicat CNT qui est
à l’encontre de notre propre propagande, met la CNT dans une position
inconfortable.
Mais au-delà de la propagande, cela pose la question quelle construction
de la CNT ?

La construisons nous ensemble, avec tout le monde ? De par le fait que nous
sommes confédéréEs, la réponse est oui.

Les cas évoqués précédemment peuvent se reproduire. Mais ça va mieux en
le disant et en l’ajoutant au statut ou règles organiques.

Nous nous sommes aussi apercuEs qu’à la CNT on se mettait à voter sur
tout, **au lieu de favoriser la discussion, le débat, même et surtout le débat
contradictoire**.

**Cette motion a aussi vocation symbolique de relancer les débats, la discussion**.

Symbolique parce que c’est a chacun dans nos syndicats et aux mandatés de nos
syndicats de mettre en avant cette pratique.

Motion
======

Tout ce qui n’est pas dans les statuts doit nécessairement faire l’objet de
discussion, que ce soit au niveau régional ou au niveau confédéral.

Et donc d’un dépôt de motion.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°12          |            |          |            |                           |
| ETPICS94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
