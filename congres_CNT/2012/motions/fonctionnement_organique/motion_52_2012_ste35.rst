
.. _motion_52_2012:
.. _motion_52_2012_ste35:

===========================================================================
Motion n°52 2012 : Trésorerie et Secrétariat international (STE35)
===========================================================================
  

.. seealso::

   - :ref:`ste35`
   - :ref:`motions_ste35_2012`
   - :ref:`amendement_motion_52_2012_sinr44`



Préambule
============

Par la présente motion, nous aimerions déterminer les relations entre la
trésorerie et le secrétariat international, dans le but de permettre au
SI d’avoir une information plus concise de la part de budget qui lui
revient, et de donner un cadre plus précis aux modalités de remboursement
lorsqu’il y a des éventuels déplacements de mandatés.


Argumentaire
=============

Connaitre avec exactitude la part du budget qui est réservée au SI se révèle
compliqué, dans la mesure où il s’agit d’un pourcentage calculé selon les
cotisations de chaque adhérent puis de chaque syndicat. C’est dire qu’il
s’agit d’une quantité variable, selon les adhérents et le paiement des
cotisations.

Aussi, le SI et la trésorerie se sont mis d’accord au long de ce dernier
mandat pour mettre en place un certain nombre de stratégies pour clarifier
les comptes du SI.

Cette motion a pour but d’entériner, par le vote des syndicats, une pratique
qui se fait déjà et qui semble nécessaire, dans le but de déterminer
clairement ce budget et d’éviter des dépenses qui dépasseraient ce qui est
réservé au SI.

Il est indispensable, dans ce sens, qu’un compte-rendu des dépenses et des
recettes puisse être dressé trimestriellement par la trésorerie, pour que
le SI sache de combien d’argent il dispose pour prévoir et organiser les
tournées et les invitations des camarades internationaux, pour publier la
revue du SI et pouvoir participer à des rencontres internationales.

Par ailleurs, il a été établi par la motion de 2008 que lorsqu’il y a des
déplacements nécessaires pour assister à une rencontre internationale, seul
un billet serait financé par la Confédération - donc seulement un mandaté
peut y assister. Bien souvent, afin de pouvoir s’y rendre, les membres du SI
payent de leur poche leurs billets, sans aucun remboursement.

Or, il est arrivé que ces rencontres internationales exigent la présence de
plus d’un mandaté – par exemple, lorsque ces rencontres regroupent à leur
tour plusieurs réunions dont le champ d’action est assuré par des militants
diférents.

Cette politique d’un seul billet remboursé empêche donc que des mandatés
du SI puissent y participer. Nous voudrions donc assouplir cette décision
et avoir la possibilité de permettre plusieurs billets remboursés si
l’occasion le demande.

Tout cela permettrait de simplifier, à terme, le travail et les
responsabilités de chacun : aussi bien de la trésorerie lorsqu’elle doit
prévoir le budget du SI, et du SI, lorsqu’il doit planifier ses activités.


Motion
======

Un compte-rendu des dépenses et des recettes du Secrétariat international
doit être établi trimestriellement.

Lorsque des rencontres internationales exigent la présence de plus d'un
mandaté, le Secrétariat international décide de ces mandatements


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°52          |            |          |            |                           |
| STE35                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
============

- :ref:`amendement_motion_52_2012_sinr44`
