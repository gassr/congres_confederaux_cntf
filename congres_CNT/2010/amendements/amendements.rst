
.. _congres_CNT_2010_amendements:

=======================================
Amendements du XXXIe congrès CNT 2010
=======================================

.. toctree::
   :maxdepth: 2

   amendement_motion_1_2010_ste93
   amendement_motions_2_3_2010_ste93
   amendement_motion_7_2010_etpreci35
   amendement_motions_9_10_11_2010_educ13
   amendement_motion_13_2010_ssctrp
   amendement_motion_14_2010_ssct_rp
   amendement_motion_13_2010_culture_rp
   amendement_motion_16_2010_stea
   amendement_motion_17_2010_stea
   amendement_motions_26_29_2010_stp72
   amendement_motion_23_2010_etpreci35
   amendement_motion_33_2010_stea
   amendement_motion_35_2010_etpreci35
   amendement_motion_37_2010_stp72
   amendement_motion_47_2010_sipm_rp
   amendement_motion_47_2010_etpreci35
   amendement_motion_50_2010_educ91
   amendement_motion_56_ste34
   amendement_motion_60_2010_etpreci35
