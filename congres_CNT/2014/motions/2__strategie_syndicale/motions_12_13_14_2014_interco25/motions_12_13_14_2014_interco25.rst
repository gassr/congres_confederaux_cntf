

.. _motions_12_13_14_2014_interco25:

===============================================================================================================
Motions 12,13,14 2014 **Positionnement de la CNT dans le mouvement international et rôle du SI** (Interco 25)
===============================================================================================================


Motions interco25

   - :ref:`syndicats:motions_interco25`


Argumentaire (commun aux motions 12, 13 et 14)
==============================================

.. include:: argumentaire.txt

Motions 12, 13 14 2014 Interco25
=================================


Motion 12
----------

.. toctree::
   :maxdepth: 3

   motion_12_2014_interco25

Motion 13
----------

.. toctree::
   :maxdepth: 3

   motion_13_2014_interco25

Motion 14
----------

.. toctree::
   :maxdepth: 3

   motion_14_2014_interco25
