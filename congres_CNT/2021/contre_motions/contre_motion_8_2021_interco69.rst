
.. _contre_motion_8_2021_interco69:

==============================================================================================================
Contre-motion N°4  à la motion n°8 2021 **Prostitution et Abolitionnisme révolutionnaire** par **Interco69**
==============================================================================================================

- :ref:`motion_8_2021_etpics94`

Autres motions Interco69

  - :ref:`rhone_alpes:motions_interco69`


Argumentaire
==============

La question n’est pas tant de savoir si la CNT est abolitionniste ou pas,
mais si elle peut protéger des travailleur.es.e exploité.e.s.

La réponse est bien évidemment oui. L’abolitionnisme existe depuis très longtemps et il n’a jamais
amélioré la condition des TDS. Nous partons donc du principe que même si la visée est l’abolition,
celle-ci ne pourra passer que par l’organisation et la défense de tous les secteurs où sont exploité.e.s
des travailleur.es.s. Tant que ces TDS n’emploient personnes ou participent à l’action des forces de
la répression, elles ne contreviennent en rien avec les statuts et la visée sociale et révolutionnaire de
la CNT. Qui plus-est la CNT a déjà syndiqué des professions libérales comme des graphistes, à ce
titre là qu’est-ce qui nous empêche de syndiquer des TDS si ce n’est pas pure morale. En effet, de
notre point de vue, un point de vue matérialiste (un point de vue révolutionnaire donc), empêcher
un.e travailleur.se de s’organiser et de se syndiquer est en contradiction la plus totale avec notre
tâche révolutionnaire. Les TDS sont pleinement des prolétaires au sens où ielles n’ont pour seul
moyen de survie la dépense de leur force de travail, or quel révolutionnaire leur refuserait le droit à
la survie économique tant que celle-ci n’implique pas le vol d’autres prolétaires, ce que le travail du
sexe n’est pas. De plus ces prolétaires que sont les TDS sont des victimes en puissance de non-
encadrement de leur condition de travail (que ce soit économiquement, psychologiquement ou
physiquement). Si l’argument était la protection des TDS de leur propre métier, devrions nous
syndiquer des ouvrier.e.s spécialisé.e.s qui s’éreintent la santé sur leur machine à force d’accomplir
le même geste tous les jours ?

La seule différence avec des salarié.e.s est que pour les TDS, leur corps est aussi le moyen de
production de la marchandise, marchandise qui est le sexe. Leur corps est leur moyen de créer de la
valeur. Par ailleurs, rappelons que les TDS en majorité ne possèdent même pas leur moyen de
production, leur corps donc. C’est donc seulement une minorité qui pourra commencer à se
syndiquer,  mais   une  minorité  qui pourrait  d’ailleurs   à terme  nous  permettre  de  toucher  aux
conditions de travail des TDS qui ne possèdent pas leur outil de travail (leur corps) et permettre de
les appuyer dans leur lutte face à leurs exploiteurs mais aussi face à des législations répressives qui
les contraignent à encore plus de prise de risques et de précarité. La question nous apparaît donc
pour l’instant purement morale et nous refusons de ne pas protéger et organiser des prolétaires.

De plus, la réalité et le développement actuel des forces productives du travail du sexe (on assiste,
entre autres, à une facilitation de l’accès au travail du sexe grâce à internet ou encore à un
développement de la prostitution estudiantine provoquée notamment par la précarité de la condition
étudiante, inhérente à sa massification, ainsi qu’à une visibilisation plus grande de ce fait social),
rend d’autant plus nécessaire un questionnement et une prise en compte des TDS. En effet,
aujourd’hui, le TDS n’est plus seulement le fait de prostitué.e.s qui racolent dans la rue, mais aussi
le fait de toute une nouvelle population qui travaille sur des plateaux de tournages ou sur internet
depuis leur domicile (cam-girl, acteur.rice.s pornographique...) et dont le flou moral, politique et
juridique qui les entoure encourage des conditions de travail souvent précaires et parfois très
dangereuses. Rien que cette année, plusieurs « grands » réalisateurs et producteurs français de
pornographie ont été sujets à une mise en examen pour fait d’esclavage, abus de faiblesses et
atteintes à l’intégrité physique, morale et psychologique d’actrices ayant travaillés pour eux.
Ainsi, tout comme nous voulons l’abolition du salariat et nous syndiquons les salarié.e.s, nous
voulons l’abolition de la violence qu’entraîne le travail du sexe et nous pensons donc qu’il faut
syndiquer les TDS.


Contre-Motion
=============

La vraie question est « la CNT doit décider si elle syndique les travailleur.se.s du sexe », pas se
définir abolitionniste ou pas. On peut être abolitionniste et être « pour » le fait de syndiquer les
travailleur.se.s du sexe. Sur la question de la légalité de les syndiquer, il est tout à fait possible de
décider que les personnes ne paient pas leurs cotisations et que c'est la confédération qui le fait à
hauteur de cotisations « précaires », pour éviter d'être accusé.e.s de proxénétisme. Nous sommes
donc pour syndiquer les travailleur.se.s du sexe. Nous précisions qu’aucun échange d’argent n’est
possible en dehors du financement des syndicats de travailleur.se.s du sexe par la confédération,
confédération qui prendra en charge les cotisations à hauteur d’une cotisation précaire.


Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°8 |            |          |            |                           |
| Interco69                     |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
