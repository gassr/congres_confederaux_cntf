
.. _motions_solidarite_confederale_cnt_congres_agen_2006:

======================================================================================
Motion "Solidarité confédérale" 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
======================================================================================

Solidarité financière aux grévistes et contre la répression
===========================================================

Vote
----

70 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 40         | 7        | 10         | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Nous demandons à ce qu'il y ait une priorité budgétaire, au niveau de la
trésorerie confédérale, pour ce qui concerne les besoins financiers de la
solidarité contre la répression et du soutien aux grévistes.

Que ces problèmes soient traités en urgence, à la demande des syndicats et des
mandatés confédéraux.
