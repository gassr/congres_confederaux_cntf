
.. _amendement_motion_13_2021_interpro31:

==============================================================================================================================================================
Amendement à la motion n°13 2021 **Gestion de la trésorerie confédérale et réécriture des statuts concernant la trésorerie confédérale**  par **Interpro31**
==============================================================================================================================================================

- :ref:`motion_13_2021_cnt09`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`

Argumentaire 
================

Libre à chaque syndicat de faire comme bon lui semble.

Amendement 
============

Enlever le paragraphe suivant :

**"Les timbres sont remplacés par un tampon que les syndicats réalisent
ou se procurent auprès de leur Union locale ou Union régionale, le modèle
ayant été défini par la Confédération."**

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°13 |            |          |            |                           |
| Interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
