.. index::
   pair: Economie; Décroisssance
   ! Décroissance

.. _motion_economie_cnt_congres_lille_2008:

==============================================================================================================================
**Motion Economie décroissance, la CNT et la décroisance**, 30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
==============================================================================================================================


La CNT et la décroissance
=========================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 31         | 1        | 7          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Alors que le capitalisme ne peut envisager sa survie que par plus de croissance,
alors que se développe enfin un mouvement pour la décroissance qui appelle
la nuisance (le capitalisme) par son nom, la CNT précise par ce qui suit
sa vision économique globale et sa position sur le sujet croissance/décroissance :

1. **Construire une société sans Etat ni patronat** implique la décroissance
   absolue, c'est à dire la disparition de nombreuses activités au service de
   ces deux composantes de la société actuelle, et qui, bien que non réellement
   productives, sont comptabilisées dans le P.I.B (salaire des militaires,
   juges, gardiens de prisons, vigiles, gardes du corps, fabrique d'armes,
   de vidéosurveillance, publicité, etc....).

   Cette **décroissance absolue de toutes ces activités nous l'appelons aussi
   bien abolition**, car il s'agit là de libérer l'humanité de diverses
   contraintes.

2. A l'inverse, **la CNT approuve et revendique la croissance, démocratiquement
   maîtrisée, d'un grand nombre d'activités pouvant procurer de meilleures
   conditions d'existence aux travailleur(se)s et à l'humanité en général**:

       * éducation
       * santé,
       * transports collectifs,
       * productions vivrières re-localisées et non polluantes,
       * artisanat
       * habitat décent.

3. Enfin, **la CNT affirme que nombre de productions inutiles, polluantes et
   nuisibles, dans des domaines très divers, doivent décroître** et se verront
   décroître lorsque que seront démocratiquement exposés leurs applications et
   dangers et surtout lorsque les choix économiques, industriels,
   technologiques seront assurés par l'ensemble des travailleur(se)s ou par
   l'ensemble de la société.

   Nous entendons par là que **la décision de produire telle ou telle chose
   doit être le fruit du débat démocratique parmi les intéressés**.

   Nous avons d'autant plus de raisons d'être partisans de la démocratie
   directe et de **l'abolition de la propriété privée des moyens de production**
   que ce sont les seules solutions pour obtenir que le choix et l'intérêt
   collectif l'emportent sur l'initiative et l'intérêt privés.

   La décroissance de la production de produits inutiles, voire nuisibles
   pour l'humanité et l'environnement et son corollaire, le développement
   des activités amélioratrices des conditions de vie, passent par
   l'autogestion de tout le système de production et de distribution.


**En conclusion, la CNT est donc partisane de la décroissance.**

Néanmoins, elle garde ses méthodes, ses moyens et ses armes propres et ne pense
pas qu'une décroissance choisie soit possible **sans révolution sociale
expropriatrice**.

**Une décroissance choisie c'est produire choisi.**
