

.. _motion_48_2010:

==========================================================
Motion n°48 2010 Combat syndicaliste en ligne, ETPRECI 35
==========================================================

.. seealso::

   - :ref:`combat_syndicaliste`
   - :ref:`motions_etpreci35`



Motion similaire reprise par les motions 55 et 56 2012 ETPIC30
==============================================================

- :ref:`motion_55_2012`
- :ref:`motion_56_2012`

Argumentaire
=============

Notre organe de presse confédéral est de très bonne qualité mais pas assez connu du public pour que l’audience
des activités de notre organisation soit massivement répandue. Afin de faire connaître notre journal et faire
connaître nos activités, le CS se doit d’être mis en ligne et à disposition du plus grand nombre. Actuellement le
CS est téléchargeable à partir du site CNT mais peu mis en avant.
Un nom de domaine existe et est disponible au niveau confédéral pour créer un site dédié à notre organe. Ce site
pourrait s’appeler combat-syndicaliste.org par exemple.

Motion
======

En plus de son édition en version « papier », le CS est mis en ligne en version maquettée téléchargeable sur un
site dédié.

Ce site dédié à notre organe de presse, dont le nom sera à définir sera combat-syndicaliste.org,
permettra par mot clé la recherche d’article antérieur par sujet – un archivage des articles
sera fait et des rubriques mobiles avec des communiqués des syndicats, des structures, des liens etc.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°48          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
