
.. _amendement_motion_5_2012_interpro31:

==========================================
Amendement à la motion 5 (interpro31)
==========================================

.. seealso::

   - :ref:`motion_5_2012_stea`




Amendement
===========

La motion est découpée en 4 points à voter distinctement:

1) Fonctionnement en double liste de tour de parole.
2) Limitation du nombre d'intervention du même syndicat à deux (hors
   présentation de la motion, contre-motion ou amendement).
3) Respect du temps imparti pour les prises de parole (trois minutes voire
   moins si le débat s'éternise).
4) Attribution d'un temps limite à la tenue d'un débat sur une motion.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°5  |            |          |            |                           |
| interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
