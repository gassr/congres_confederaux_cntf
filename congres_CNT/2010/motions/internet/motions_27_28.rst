

======================================================================================================================
Motions n°27 et 28 2010 : Simplification, clarification et sérénité des débats sur le forum Intranet, INTERPRO CNT 31
======================================================================================================================




Argumentaire
=============

"Simplification, clarification" : Depuis quelques années, nous avons mis en place en forum interne censé nous
permettre une meilleure communication au sein de la CNT. D'un coté, nous le vérifions, car la précédente liste
mail posait comme problème majeur de recevoir l'intégralité des communications sans capacité de sélection.
Mais d'un autre côté, il nous semble que la forme du forum actuelle pose différents problèmes. Les rubriques
trop nombreuses et classées sans cohérence n'incitent pas les nouveaux inscrits à utiliser le forum. Un premier
chantier de clarification et de simplification en vue de rendre le forum plus agréable d'accès est nécessaire.

Un autre problème est le manque de retenue de certains camarades qui dans certains débats donnent une image
bien triste de la CNT. Nous proposons donc que les prochains mandatés à la gestion du forum intranet de la CNT
s'occupent de simplifier et de clarifier le forum avec l'aide éventuelle des utilisateurs de cet outil confédéral.

.. _motion_27_2010:

Motion n°27 2010 Simplification, clarification et sérénité des débats sur le forum Intranet, INTERPRO CNT 31
=============================================================================================================


Les mandatés chargés de gérer le forum intranet ont pour mandat de veiller à la simplification et à la clarification
de cet outil, en évitant un foisonnement de rubriques peu propice à une bonne compréhension.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°27          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. _motion_28_2010:

Motion n°28 2010 Simplification, clarification et sérénité des débats sur le forum Intranet, INTERPRO CNT 31
============================================================================================================

Le forum confédéral de la CNT doit être un espace de débat. Les mandatés chargés de gérer le forum intranet
s'attacheront donc à maintenir un climat de sérénité et de respect mutuel. Charge à ces mandatés de contacter le
syndicat des utilisateurs qui s'emploieraient à multiplier les provocations gratuites et les polémiques stériles.
  
+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°28          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
