
.. index::
   Fédération (batiment)


.. _federation_batiment:

===============================
Fédération du bâtiment (SUB)
===============================

.. image:: logo_sub_reasonably_small.jpg


.. toctree::
   :maxdepth: 4

   publications/index
