
.. index::
   pair: Motions; Fonctionnement organique 2014
   pair: Fonctionnement ; organique


.. _motions_fonctionnement_organique_2014:

=====================================
Motions Fonctionnement organique 2014
=====================================

.. toctree::
   :maxdepth: 4

   motion_18_2014_sinr44
   motion_19_2014_sinr44
   motion_20_2014_etpic30
   motion_21_2014_interco09
   motion_22_2014_etpic30
   motion_23_2014_etpic30
   motion_24_2014_ste75
   motion_25_2014_etpic30
   motions_26_27_28_etpic30/motions_26_27_28_2014_etpic30
   motion_29_2014_ste75
   motion_30_2014_ste75


- :ref:`contre_motions_congres_2014`
- :ref:`congres_CNT_2014_amendements`
