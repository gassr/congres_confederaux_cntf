
.. index::
   pair: Motion 8 ; 2016

.. _motion_8_2016_sante_social_lorraine:

===========================================================================================
Motion n°8 2016 : Nouvelles implantations syndicales (Santé Social CT Lorraine) [adoptée]
===========================================================================================



Argumentaire
============

Comme ce fut le cas en 2015 pour la section Lidl du STP 67, il arrive
parfois que les syndicats de la CNT parviennent à s'implanter dans des
entreprises privées dont le champ géographique n'est pascouvert par
celui du syndicat de rattachement de la section syndicale. En effet
l’implantation administrative des entreprises ou leur unité sociale et é
conomique ne correspond pas toujours ni aux délimitations géographiques
administratives, ni à celles des structures de la CNT.

Aussi cette situation pose problème puisqu'une structure qui effectuerait
une désignation de RSS sur un champs géographique quelle ne couvre pas
s'exposerait à une potentielle contestation juridique de sa section.

S'il ne s'agit pas de remettre en question le contrôle des sections
syndicales par leur syndicat de rattachement, force est de constater
que la structuration même de la CNT peut contraindre son développement
dans certains cas, où ni le syndicat, ni l'UR concernée, ni la fédération
ne sont à même de couvrir une implantation. Or, dans un souci de
développer de notre organisation nous pensons que se priver de telles
implantations pour des raisons organisationnelles n'est pas
acceptable.

Il nous semble donc logique que le Bureau Confédéral puisse, en ultime
recours, effectuer des déclarations de RSS au nom de la confédération
lorsque la situation l'impose et cela sans avoir à attendre plusieurs
mois la tenue du prochain CCN. En effet, les implantations de sections
se faisant régulièrement dans le cadre d'une lutte ou d'une problématique
concernant les salariés de l'entreprise, un délai pouvant courir
jusqu'à 6 mois sans donner de garantie sur l'aboutissement de la
démarche constitue un véritable risque pour les adhérents de la section
syndicale qui ne peuvent alors se protéger durant toute cette période.

Aussi la motion rédigée ci dessous est inspirée de la prise de position
du CCN de Nantes sur ce sujet.



Motion
=======

Le Bureau Confédéral de la CNT peut, s'il est sollicité par un syndicat
et après consultation de la Commission Administrative, effectuer des
désignations de RSS dans les entreprises du privé.

Néanmoins cette possibilité doit constituer un recours ultime. Elle
n'est possible que lorsque l'ensemble des conditions suivantes sont
réunies :

- Le syndicat demandeur n'a pas la capacité statutaire d'effectuer la
  désignation parce que son champ géographique ne couvre pas
  l'implantation de la section
- Le syndicat demandeur se retrouve dans l'incapacité d'étendre son
  champ géographique de syndicalisation
- L'union régionale concernée ne couvre pas le champ géographique et se
  trouve dans l'incapacité d'élargir ce dernier (ou en cas d'absence
  d'union régionale)
- Il n'existe pas de Fédération d'industrie couvrant le champ de
  syndicalisation concerné

En cas de désignation d'un RSS par la confédération :

- le syndicat demandeur reste le responsable du suivi syndical de sa
  section et reste en lien avec le Bureau Confédéral afin notamment de
  lui signaler tout fait qui pourrait avoir des conséquences sur la
  responsabilité juridique de la confédération
- le syndicat demandeur effectue, à chaque CCN, un bilan complet de
  l'activité, des problématiques, et du développement de la section syndicale
- le syndicat demandeur s'engage à travailler dans le sens d'une création
  de fédération d'industrie qui pourra à termes couvrir la section.

Contre motions
==============


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°8 2016

       :ref:`Santé social CT Lorraine <sest_lorraine>`
     - 39
     - 0
     - 4
     - 5
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
