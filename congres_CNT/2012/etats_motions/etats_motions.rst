

.. _etat_motions_xxxii_congres:

========================================================
Etat des motions du XXXIIe Congrès CNT Metz 2012
========================================================

.. toctree::
   :maxdepth: 3

   annonce_compte_rendu
   motions_adoptees
   motions_rejetees
