
.. _amendement_motion_14_2021_sest_lorraine:

============================================================================================================
Amendement N°1 à la Motion n°14 **Financement confédéral des groupes Femmes Libres** par **SEST Lorraine**
============================================================================================================

- :ref:`motion_14_2021_syndicats_42`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`

Argumentaire
===============

Le SEST Lorraine est pour que la Confédération donne des moyens financiers
nécessaire au bon fonctionnement des groupes CNT Femmes Libres.

Nous souhaitons donc une part financière plus importante accordée par la
Confédération. Afin de travailler en toute transparence, nous avons porté
nos réflexions sur la création d’une Fédération des groupes locaux
CNT-Femmes Libres au sein de la Confédération afin que les différents groupes
puissent gérer leur trésorerie, champs d’actions... comme tous les autres
organes de la CNT.

Amendement
============

Une cotisation de 10 % sur les cotisations syndicales confédérales de
chaque adhérent・e est affectée au fonctionnement des groupes Femmes Libres
pour organiser des rencontres, formation,actions...

Cette cotisation ne retire pas aux différents groupes la tâche de
s’autofinancer, mais engagerait la confédération dans leur reconnaissance
et le soutien a leur combat.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°14 |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
