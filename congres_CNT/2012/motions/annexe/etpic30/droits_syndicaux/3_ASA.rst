

========================================================
III – Autorisations individuelles d’absences syndicales
========================================================

A - Dans la fonction publique
=============================

Les autorisations spéciales d’absence (ASA)
--------------------------------------------

Ces autorisations permettent, sous réserve des nécessités de service et dans
des limites fixées par décret, aux adhérents d’une organisation syndicale de
participer au congrès de son syndicat ou à des assemblées générales statutaires.
Leur nombre varient selon les dispositions légales propres à chaque administration.


B – Dans le privé
=================

Les autorisations d’absence syndicale sont quasi inexistantes dans le code du
travail.

Seules les conventions collectives peuvent prévoir des jours et heures
spécifiques d’absences syndicales.

A noter, la possibilité pour les salarié.e.s du privé de pouvoir recourir
jusqu’à 12 jours de congés de formation économique, sociale (CFES) au sein
de Centre de formation agréés.

Généralement confiés à la gestion des grosses organisations syndicales, les
cénétistes ont peu d’accès à ces centres de formation dans l’immédiat.
