.. <p class="gras vert">paragraphe gras et vert.</p>


.. _congres_CNT_2021_synthese_motions:

===============================
Synthèse des motions 2021
===============================

- :ref:`cahier_mandatee_2021`

.. toctree::
   :maxdepth: 1
   :caption: Motions de fonctionnement

   motion_synthese_22_2021_ste38_stp67.rst
