
.. _motion_59_2010:

===================================================================================================================
Motion n°59 2010 : Mass Média Audiovisuel (MMA) : la monoforme, une arme au service du pouvoir, CCS 32/Interpro 32
===================================================================================================================

.. seealso::

   - :ref:`motions_ccs32`



Argumentaire
=============

La confédération doit s'interroger, critiquer et se positionner sur la réalité
du pouvoir du Mass Média Audiovisuel (MMA) comme l'outil de propagande
idéologique des dominants capitalistes-politique-religieux.

Le MMA complice du pouvoir a aussi son propre pouvoir.
  
C'est à travers la télévision et encore pour très longtemps dans le monde et dans une autre mesure « la Toile »,
que le MMA diffuse une communication de masse abrutissante et agressive dont le seul but est de manipuler la
conscience collective de la population internationale à ses propres fins. Les MMA comme « Pouvoir » pour lui
même s'exprime par le biais d' une structure narrative complexe et définie « Monoforme » par Peter Watkins. En
voici la définition non exhaustive :

- « La télévision a imposé des structures narratives totalitaires à la société sans que nul ait eu le temps de
  réagir, à cause de sa rapidité, de son arrogance et de son côté mystérieux.
  C’est ça, la "monoforme" : un torrent d’images et de sons, assemblés et montés de façon rapide et dense, une
  structure fragmentée mais qui donne l’impression d’être lisse.
- En dépit des apparences la "monoforme" est rigide et contrôlée, elle ignore les possibilités immenses et sans
  limites du public que les médias estiment immature. » PW.

Ainsi, la pratique du MMA de trier, sélectionner, arranger, interpréter, commenter et structurer en monoforme
est devenue l' arme de destruction massive au service de l'information pour la personne face à son écran. C'est un
véritable danger qui nous concerne tous. De plus, la « liberté d'expression » des luttes sociales/syndicales et plus
particulièrement de la CNT est totalement exclue des grands médias qui fabriquent l'opinion. Le MMA est donc
par principe un ennemi de classe à détruire dans l'état actuel. Le débat est ouvert.

Motion
======

En adoptant cette motion et en l'inscrivant dans les statuts confédéraux, la Confédération se positionne et décide
de faire de la lutte contre la « monoforme » et le rôle du Mass Média Audiovisuel, un principe inaliénables et
indissociable de la lutte anarcho-syndicaliste / syndicaliste révolutionaire de la CNT.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°59          |            |          |            |                           |
| CCS 32/Interpro 32   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso:: :ref:`contre_motion_59_2010_ess34`
