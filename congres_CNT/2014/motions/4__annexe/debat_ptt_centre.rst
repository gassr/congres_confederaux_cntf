

.. _contrib_ptt_centre:

=========================================================================
Contribution du syndicat PTT Centre pour le XXXIIIème congrès confédéral
=========================================================================




Motion
======



Notre syndicat (CNT PTT Région Centre) demande au bureau confédéral , si cela est possible, de
faire parvenir à tous les syndicats CNT de la Confédération ce texte comme contribution aux débats
du prochain Congrès de la CNT devant se tenir à Angers.
Voilà, ce texte déjà publié sur la liste fédérale devient un texte de syndicat puisque nous l' avons
validé pour le syndicat CNT PTT Centre. Il sera, envoyé à la Confédération qui, normalement, le
retransmettra à tous les autres syndicats pour servir de contribution au prochain Congrès Confédéral
(il ne s' agit pas d'une motion mais d' une contribution), libre au futur Congrès de la CNT d' en
débattre (ou non) et éventuellement de la revendiquer. Il pourrait en être de même pour notre
Fédération, c' est pour cela que nous la soumettons au débat de notre prochaine CAFédérale à
Brest conscient que l' ordre du jour en est déjà bien chargé mais qu' aussi il y a urgence à nous
positionner.
"Collabo ou résistant, choisir et le faire savoir
Pour un Autre Futur, il nous faut choisir dès aujourd’hui entre l’ espérance de pouvoir
continuer demain notre combat pour une société libertaire, notre idéal de révolution sociale, et le
possible avènement annoncé et prochain d’ une société barbelée, autoritaire et néo-fasciste.
Il nous faut choisir et le faire savoir.
Nous pourrions simplement continuer avec toutes nos difficultés mais aussi avec tout nos
engagements militants et syndicaux, à tenter de faire vivre notre projet d’ un communisme
libertaire face à un capitalisme semble-t-il triomphant. Il est bien possible que nous n’ ayons plus le
temps de nous contenter de ce modus vivendi face à l’ urgence que représente la montée d’ une
extrême-droite à peine refroquée des habits neufs de la «démocratie» et prétendant prendre les rênes
du pouvoir par le suffrage universel.
Nous n’ avons pas oublié toutes les dictatures, les barbaries légalisées par le vote ou le
plébiscite. Nous n’ avons pas perdu la mémoire.
Il est de toute première urgence de faire comprendre à tous ceux qui voient l’avènement
permis et prochain d’ un régime néo-vichyste se faire en douceur et en toute «démocratie» que rien
de cela ne se fera sans affrontement, et que nous n’accepterons pas de revoir la bête immonde,
même affublée des oripeaux du vote «démocratique», asservir notre vie quotidienne et nos idéaux.
Sur les décombres des mouvements sociaux, les trahisons et les faux semblants de la
«sociale démocratie» vautrée dans les bras du libéralisme ont refait le lit de la bête immonde qui
revient sous les oripeaux «démocrates» de la banalisation de la haine, de l’exclusion et de l'
autoritarisme.
Il est de notre devoir, non par fanfaronnade, mais par réalisme de déclarer que nous, anarcho-
syndicalistes, syndicalistes révolutionnaires,de la Confédération Nationale du Travail, à la mesure
de nos moyens (mais de tous nos moyens) ne laisseront pas s’ installer au pouvoir, de nouveau, une
extrême-droite néo-vichyste pour asservir nos vies et idéaux.
Nous n’ avons pas d’ autre choix de dire dès aujourd'hui que s’ il le fallait, entre l’ avènement
d’ une nouvelle barbarie ou la résignation et la résistance, nous avons choisiront la Résistance
même si cela devait impliquer la lutte clandestine et le recours à nos fusils rouillés."
Syndicat CNT PTT Centre
