
.. _amendement_motion_15_2014_stics13:

================================================
Amendement à la motion n°15 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_15_2014_interco71`





Amendement
===========

Le Bureau Confédéral est mandaté pour développer une campagne contre la
mutuelle obligatoire avec d'autres organisations syndicales.


Votes
======

.. list-table::
   :widths: 30 18 18 18 15
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°15 2014 <motion_15_2014_interco71>`

       :ref:`STICS13 <stics13>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
