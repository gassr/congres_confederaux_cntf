

.. _motion_49_2010:

===============================================================
Motion n°49 2010 Prison / Combat Syndicaliste, INTERPRO CNT 31
===============================================================



Argumentaire
=============

Dans la continuité d'un travail entamé depuis plusieurs années par la commission prison de la CNT (aujourd'hui
en sommeil), le syndicat CNT interpro 31 propose d'abonner gratuitement tous les prisonniers qui en feraient la
demande.

  
Motion
=======

Le Combat Syndicaliste sera envoyé gratuitement à tous les individu-es emprisonné-es en hexagone qui en font
la demande. Chaque mois cette possibilité sera intégrée dans le CS. A charge à l'administration du CS, avec
l'aide des syndicats d'en informer les médias, assos ou orgas qui se battent contre l'enfermement.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°49          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
