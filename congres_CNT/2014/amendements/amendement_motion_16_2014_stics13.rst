
.. _amendement_motion_16_2014_stics13:

================================================
Amendement à la motion n°16 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_16_2014_ess34`






Argumentaire
=============

Étant donné qu'il y' a deux éléments distincts dans cette motion : le refus de
la présence d'un adhérent sous l'étiquette CNT dans des élections et la mise
en place d'une campagne confédérale pour l'abstention aux élections, nous
proposons de supprimer la 2eme partie de la motion.

Amendement
===========

Retrait de la proposition :

    et en conséquence organise des campagnes d'abstention lorsque
    celles-ci sont d'actualité, et ce tout en réaffirmant son attachement
    au Communisme Libertaire et aux voies pour y parvenir


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°16 2014 <motion_16_2014_ess34>`

       :ref:`STICS13 <stics13>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
