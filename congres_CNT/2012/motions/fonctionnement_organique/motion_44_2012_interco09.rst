
.. _motion_44_2012:
.. _motion_44_2012_interco09:

=========================================================================================================================================================================================================================
Motion n°44 2012 : Précision du laps de temps nécessaire pour la mise à disposition des syndicats de l'ordre du jour du CCN et respect mécanique de la date limite de remise des points du dit ordre du jour (Interco 09)
=========================================================================================================================================================================================================================
  

.. seealso::

  - :ref:`motions_interco09`
  - :ref:`amendement_motion_44_2012_sinr44`




Motion reprise de la CNT09 en 2010
==================================

- :ref:`motion_13_2010`


Argumentaire
============

Lors du CCN des 16 et 17 mai 2009, l'ordre du jour n'était pas mis à
disposition des syndicats avant le 6 mai, ce qui empêche le fonctionnement
démocratique de notre confédération du fait que les syndicats qui ne se
réunissent qu'une fois par mois n'ont pas pu l'examiner à temps pour faire
part de leur point de vue aux Unions régionales qui de ce même fait n'ont pu
envoyer (sauf une) de mandatés à ce CCN.

Même si nos statuts confédéraux qui mentionnent : « Les Syndicats sont informés
au moins un mois avant la tenue du CCN de l'ordre du jour définitif », avaient
été respectés à minima (un mois) certains syndicats sus-cités n'auraient pas
davantage pu donner leur avis sur l'ordre du jour, vu qu'un mois ne contient
guère plus que 4 semaines auxquelles il faut retrancher une semaine, si
toutefois les réunions d'UR prévues pour examiner le dit ordre du jour se
tiennent le week-end précédent le CCN.

En efet un syndicat se réunissant tous les mois ne se réunit pas toutes les 3
semaines, mais bien toutes les 4 ou 5 semaines, et c'est le hasard qui fera si
une de ses réunions mensuelles peut comporter à son programme l'examen des
points de l'ordre du jour du CCN.

Motion
======

Pour que tous les syndicats aient le temps d'examiner les motions lors de leur
AG un délai de huit semaines est indispensable (en comptant deux semaines pour
la mise à disposition de l'ordre du jour du CCN aux syndicats, délais postaux
compris), dans le cas où les réunions d'UR se tiennent le week-end précédent
le CCN, il reste cinq semaines aux syndicats pour examiner les motions au cours
de leur AG habituelle.

La date d’échéance de dépôt des points de l'ordre du jour par les Unions
régionales ainsi fixée 56 jours avant la date du CCN doit donc être respectée
mécaniquement, le cachet de la poste faisant foi (ou la date d'envoi par e-mail),
les points de l'ordre du jour recus le lendemain étant reportés au CCN suivant.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°44          |            |          |            |                           |
| Interco09            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_44_2012_sinr44`
