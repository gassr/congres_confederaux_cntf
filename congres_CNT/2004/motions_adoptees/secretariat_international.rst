

.. _motion_secretariat_international_cnt_congres_saint_denis_2004:

=================================================================================================
Motion Secrétariat international, 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=================================================================================================

Organisation du secrétariat international
=========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 39         | 1        | 6          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Etant devenu impossible à un seul militant d'assumer la responsabilité de
toutes ces activités, le congrès 2004 mandate un secrétariat collégial de
trois personnes pour assurer la responsabilité des relations internationales
de la CNT.

Ces trois militants se répartiront les activités internationales, et se
chargeront de s'entourer de camarades pour les aider dans leurs tâches.
