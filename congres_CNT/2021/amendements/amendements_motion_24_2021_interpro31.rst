
.. _amendements_motion_24_2021_interpro31:

========================================================================================================
Amendements N°2 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
========================================================================================================

- :ref:`motion_24_2021_ste33_cnt42`
- :ref:`role_de_linterpro_31_fouad`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


.. _amendement_1_motion_24_2021_interpro31:

Amendement N°1 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
=========================================================================================================

Argumentaire 
---------------

Nous jugeons nécessaire une commission permanente plutôt que des commissions
extraordinaires créés au besoin.
En conséquence, notre amendement remplace la notion de **liste de référent·es**
par des mandaté·es dans cette commission permanente.

Amendement 
------------

Remplacer le point 2:

La CNT met en place une commission permanente de mandaté·es jugé·es
compétent·es sur les questions de violences patriarcales. Ces mandaté·es
sont proposé·es par leurs syndicats et mandaté·es à l'occasion des
congrès confédéraux.

La victime peut alors saisir (elle-même ou par l’intermédiaire d’une
tierce personne) une personne parmi cette liste pour porter accusation.
la commission permanente.

Les référent·es contacté·es sont alors chargé·es de mettre en place une
commission extraordinaire, non mixte ou mixte suivant le choix de la victime.
Les membres de cette commission extraordinaire sont choisi·es parmi la
liste des référent·es. Cette commission extraordinaire permanente a tout
mandat pour instruire l’affaire, décider quand lever l'anonymat et diffuser
le rendu de ses travaux à la Confédération. Elle n’est nommée que pour
le temps de l’instruction. La CNT, à travers le :term:`BC`, doit se donner les
moyens (financiers notamment et/ou d'hébergement pour d'éventuels déplacements,
voire informatique à travers un espace d'échange sécurisé) du bon
fonctionnement et déroulement de la procédure.

- Les membres du syndicat et de l’UD de l’accusé·e aussi bien que de la
  victime ne peuvent en aucun cas faire partie des sessions de la commission
  extraordinaire permanente pour éviter tout risque de partialité et de
  pressions extérieures.

- Dés sa constitution, la commission extraordinaire permanente informe
  le syndicat local de l’accusé·e des accusations portées contre lui / elle
  (que la victime soit elle-même adhérente de la CNT ou pas). À partir de
  cette information, par principe de précaution, l’accusé·e est suspendu·e
  de la CNT, et ce pendant tout le temps de l’instruction.
  Le syndicat local d’appartenance de l’accusé·e est responsable de
  l’application de la suspension. Il doit être clair que cette suspension
  ne préjuge pas de la culpabilité.

- La commission extraordinaire permanente a pour mission de recueillir
  la parole de la victime et tout autre témoignage qu’elle jugera nécessaire.
  Une autre de ses missions est d’aider la victime à porter plainte, et
  ce faisant de lui permettre de sortir d’un silence préjudiciable pour
  elle à court, moyen et long terme.

- Quelle que soit la décision de la victime de saisir ou non la justice,
  elle ne pourra en aucun cas être jugée sur sa décision par la CNT.
  En cas de refus de porter plainte, il ne pourra en être tenu rigueur à
  la victime et cela ne vaudra pas non plus mise en doute de son accusation.

- Une fois la parole de la victime recueillie, la commission extraordinaire
  permanente rend ses conclusions aux référent·es. Les référent·es et la
  commission extraordinaire se réunissent alors et prennent ensemble
  conclut et prend une décision qui est remise à tous les syndicats de la
  Confédération.

- Il ne peut en aucun cas être laissé à la responsabilité du syndicat et
  de l’UD de l’accusé·e de définir la sanction à porter contre l’accusé·e.

- Selon les conclusions de la commission extraordinaire permanente,
  l’accusé·e pourra être exclu·e de la CNT. La CNT communique alors
  publiquement sa décision d'exclure un·e camarade.

- Dans tous les cas, le :term:`BC` s’engage à donner les moyens à la
  commission extraordinaire permanente d’assurer un accompagnement aux
  protagonistes, s’ils le souhaitent. Sans interférer avec le déroulement
  de la procédure ni évidemment avec ses conclusions.


Vote
-------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
| n°24 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_24_2021_interpro31:

Amendement N°2 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
=========================================================================================================


Argumentaire 
---------------

L’idée est  d’expliciter que la levée de l’anonymat n’est pas décidée
sans la victime et qu’elle peut concerner seulement une partie des
protagonistes.


Amendement 
-------------

Cette commission extraordinaire a tout mandat pour instruire l’affaire,
décider, avec la victime, quand lever l'anonymat de tout ou partie
des protagonistes et diffuser le rendu de ses travaux...


Vote
-----------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°2 à la motion  |            |          |            |                           |
| n°24 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_3_motion_24_2021_interpro31:

Amendement N°3 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
=========================================================================================================


Argumentaire 
--------------

Nous estimons que la précision et la distinction entre les termes
**viol** et **violences patriarcales** sont nécessaires.

Amendement 
-------------

Toute personne reconnue coupable de viol ou de violences patriarcales
n’a pas sa place à la CNT.

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°3 à la motion  |            |          |            |                           |
| n°24 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_4_motion_24_2021_interpro31:

Amendement N°4 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
=========================================================================================================


Argumentaire 
--------------

Le choix revient à la victime de porter plainte ou non.
La commission se doit de l’accompagner dans tous les cas de figure.

Amendement 
-------------

La commission extraordinaire a pour mission de recueillir la parole de
la victime et tout autre témoignage qu’elle jugera nécessaire.

Une autre de ses missions est d’aider la victime à porter plainte,
accompagner la victime dans ses démarches si elle le désire, et ce
faisant de lui permettre de sortir d’un silence préjudiciable pour
elle à court, moyen et long terme.

Vote
--------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°4 à la motion  |            |          |            |                           |
| n°24 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_5_motion_24_2021_interpro31:

Amendement N°5 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **Interpro31**
=========================================================================================================


Argumentaire 
--------------

Nous considérons que la commission seule ne peut être décisionnaire.
Cette décision finale appartient à la Confédération.

Amendement :

Amendement 
------------


Reformuler

point 9 
    Une fois la parole de la victime recueillie, la commission extraordinaire
    rend ses conclusions aux référent·es. Les référent·es et la commission
    extraordinaire se réunissent alors et prennent ensemble une décision
    proposent une résolution de conflit, pouvant aller jusqu'à l'exclusion,
    qui est remise à tous les syndicats de la Confédération.

Point 11
    Selon les conclusions de la commission extraordinaire, l’accusé·e
    pourra être exclu·e de la CNT, les éventuelles sanctions sont adoptées
    en CCN ou en congrés. La CNT communique alors publiquement sa décision
    d'exclure un·e camarade.


Vote
-------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°5 à la motion  |            |          |            |                           |
| n°24 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
