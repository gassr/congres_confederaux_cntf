
===============================================================================
Contre-motion de l'ETPIC 94 : problèmes relatifs aux motions communes [rejetée]
===============================================================================

Présentation de l’ETPIC 94 : C’est le premier congrès où il y a autant de
motions préparées par plusieurs syndicats.

Interrogations sur ce genre de démarche. Nous voulons souligner le fait que
ces syndicats se sont choisis en dehors de tout fonctionnement interne, en
dehors des instances qui permettent aux syndicats de se rencontrer pour
discuter.

Une sorte de « pré-congrès ». Interrogation pour l'avenir : est-ce que ça
devient la règle à la CNT de travailler avec certains syndicats choisis, ou
est-ce qu’on travaille tous ensemble ?

ESS 34
nous avons présenté une motion commune, pour autant, nous sommes d'accord avec ETPIC
94. Notre but était de soulager le congrès et pas de choisir avec qui travailler.

Santé Social RP
Nous concerne pour la motion 15 « Discussion sur les bourses du travail ».
Après.. sur la question de l'égalité, nous ne comprenons pas bien.

SUTE 69
On demande à ce que cette contre motion ne soit pas passée au vote, car il semble difficile de voter une motion
qui n'apporte pas de réponses, mais ne soulève que des questions.
Néanmoins, cela ne nous choque pas que certains syndicats proches par la
géographie ou les domaines de travail présentent des motions communes.

ETPIC 94 :

Cette motion n'appelait pas un vote, on voulait ne surtout pas incriminer
les syndicats en question. L'égalité entre syndicats se définit par le fait
que chaque syndicat apporte ses motions s'il en a, et qu'il discute...
Certains syndicats commencent le congrès avant les autres... Si on se choisit
des syndicats avec qui on veut travailler, c’est un risque de dérive vers des
courants, des tendances.

STTE 33
On est tous sensé débattre avant le congrès.

CS RP
OK / réflexion d’ETPIC 94. On comprend bien l'idée de la motion et on y adhère.
Ce sont des syndicats qui apportent des motions et des contre-motions


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 12         | 16       | 7          | 8                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

**Pour : 12**
	PTT 35, SSEC 59 (10 syndicats non
	identifiés)

**Contre: 16**
	Interco 42, Santé social 42, éduc 42,
	santé social RP, ETPRECI 35, SCIAL 75
	(9 syndicats non identifiés)
	(7 syndicats non identifiés)
	Educ 35, STE 93 (6 syndicats non
	identifiés)

**Abstention: 7**
   (7 syndicats non identifiés)

NPPV: 8
	Educ 35, STE 93 (6 syndicats non
	identifiés)

MOTION non retenue
