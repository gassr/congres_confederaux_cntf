
.. _motion_55_2010:

===============================================================================
Motion n°55 2010: Réactivation de la commission "gens du voyage", Education 78
===============================================================================



.. seealso::

   - :ref:`motions_educ78`

Motion reprise par ETPICS57, 2012
=================================

- :ref:`motion_29_2012`

Argumentaire
============

La commission des gens du voyage de la CNT a été créée en 2002 afin de fédérer tziganes et non-tziganes dans
la lutte contre la double oppression que subissent les gens du voyage : oppression en tant que minorité
transnationale et oppression en tant que travailleurs. Depuis, cette commission a apporté son soutien lors du
procès "Boboye" (membre de la commission accusé de menaces de mort sur un commissaire lors d'une
expulsion), elle est intervenue auprès des Roms roumains contre leurs expulsions, pour l'acquisition des
caravanes, pour la scolarisation de leurs enfants, pour faire appliquer le droit au stationnement.

Elle a appelé aux manifestations contre les lois Sarkozy contre les prostituées et les gens
du voyage. La commission, d'abord rattachée à l'UD 95 est devenue nationale
(entériné par le congrès de Saint-Denis). Elle a collaboré au Combat Syndicaliste
et aux Temps Maudits et a sorti épisodiquement un bulletin, le Niglo en colère.
Les membres de cette commission étaient actifs avec l'interco 95 mais sont aujourd'hui
majoritairement installés dans les Yvelines (78) où ils sont adhérents.

Motion
======

Le syndicat éducation des Yvelines demande a être mandaté pour prendre en charge la commission gens du
voyage de la CNT avec comme objectif de coordonner au niveau national les différentes actions des syndicats,
des unions locales / départementales / régionales et de faire la liaison avec les organisations tziganes
indépendantes. La commission animera aussi un site afin de regrouper les informations sur les luttes des gens du
voyage.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°55          |            |          |            |                           |
| Education 78         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`contre_motion_55_2010_ess34`
