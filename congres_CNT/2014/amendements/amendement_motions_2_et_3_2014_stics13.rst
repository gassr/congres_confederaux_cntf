
.. _amendement_motions_2_3_2014_stics13:

================================================
Amendement aux motions n°2 et n°3 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_3_2014_sinr44`
   - :ref:`motion_2_2014_ste75`
   - :ref:`stics13`






Argumentaire
=============

Si c'est pour des raisons financières qu'un syndicat ne peut pas venir au
congrès confédéral, le trésorier de ce syndicat prend contact avec la
trésorerie confédérale qui voit comment aider le syndicat à se rendre au
congrès (prise en charge totale, partielle ou échéanciers, etc).
