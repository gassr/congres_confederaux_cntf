

.. _cnt_delais_2021_02_12:

=================================================================
Vendredi 12 février 2021 **Délais de recevabilité des motions**
=================================================================

.. contents::
  :depth: 3



Délais de recevabilité des motions
====================================


::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] Délais de recevabilité des motions
    De:      congres@cnt-f.org
    Date:    Ven 12 février 2021 21:05
    À:       Liste-syndicats@bc.cnt-fr.org
    --------------------------------------------------------------------------



Camarades,

Nous rappelons qu'il n'y a pas de date de début de dépôt des motions
pour un congrès confédéral. Toutes les motions (statutaires ou non),
contre-motions et amendements à ces motions qui nous sont parvenues
depuis le 34ème congrès confédéral de novembre 2016 seront prises en
compte pour le 35ème congrès confédéral.

Nous rappelons les délais d'envois :

* 6 mois avant le congrès pour motions statutaires
* 3 mois avant le congrès pour les motions non statutaires
* 1 mois avant le congrès pour les contre-motions et amendements

Pour les textes parvenus hors de ces délais, le 35ème congrès
confédéral tranchera concernant leur recevabilité ou non.

Salutations rouges & noires. Le SINR 44 pour la commission de
préparation du 35ème congrès confédéral
