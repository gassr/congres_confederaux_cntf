
.. _annexe_contre_motion_1_2021_simrp:

================================================================================================================================================
Annexe à la contre-motion n°1 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **SIMRP**
================================================================================================================================================

.. figure:: simrp/annexe_contre_motion_1_simrp.png
   :align: center
