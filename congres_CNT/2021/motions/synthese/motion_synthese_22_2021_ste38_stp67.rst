.. index::
   ! viol
   ! violences sexuelles

.. _motion_synthese_24_2021_ste38_stp_67:

=======================================================================================
**⚖ ♀ Gestion interne des violences sexuelles, sexistes et patriarcales** (synthèse)
=======================================================================================

.. note:: Synthèse des contres motions:

   - :ref:`contre_motion_24_2021_educ38`
   - :ref:`contre_motion_24_2021_stp67`

:download:`Télécharger la synthèse au format .odt <motion_synthese_22_2021_ste38_stp67.odt>`


**Aucune personne reconnue coupable de viol/violences** :term:`patriarcales`
**n’a sa place à la CNT**.

Adoption par le 35ème congrès confédéral de la CNT d'un protocole qui
vise à établir des outils et une procédure à suivre systématiquement dès
qu'une accusation de :term:`viol` et/ou de faits de violence :term:`patriarcale` est
portée à la connaissance de membres de la CNT contre un·e de ses membres.

Toute personne accusant de :term:`viol` et/ ou :term:`agression sexuelle` un membre de
la CNT est considérée comme une victime et bénéficie de tous les traitements
et égards dus à ce statut (aide psychologique, juridique, financière en
cas d’incapacité au travail) et ce le temps nécessaire à l’instruction
du viol et/ou agression sexuelle et sans jugement au préalable de son issue.

Les :term:`syndicats` de la CNT mettent en place une liste de personnes référentes
pour la prise en charge d’accusations de :term:`viol` et de violence :term:`patriarcale`
mandatées par leurs :term:`syndicats`.

Cette liste est actualisée à chaque fois que le syndicat mandate un·e
référent·e parmi ses membres.
La liste des référent·e·s pourra être anonyme, mais devra faire au moins
figuré le syndicat d’appartenance, et être transmis à l’ensemble des :term:`syndicats`.

Comme pour tout mandatement au sein d’une commission de travail, les :term:`syndicats`
s’assurent que les personnes mandatées soient formées à ce qui constitue
l’objet de la commission (lutte contre le patriarcat, prise en charge de
violences) ou se forment pour cela.

La victime peut saisir (elle-même ou par l’intermédiaire d’une tierce
personne) une ou plusieurs personnes parmi cette liste pour porter accusation.

Les référent·e·s contacté·e·s sont alors chargé·e·s de mettre en place
une commission extraordinaire, non mixte ou mixte suivant le choix de la
victime.
Les membres de cette commission extraordinaire sont choisi·e·s parmi la
liste des référent·e·s. Aucune démarche, décision ne sera mise en œuvre
sans l’approbation préalable de la victime.

Cette commission extraordinaire a tout mandat pour instruire l’affaire,
décider quand lever l'anonymat de l’agresseur/violeur et diffuser le
rendu de ses travaux à la Confédération. Elle n’est nommée que pour le
temps de l’instruction.

Comme toute commission de travail confédérale, la commission extraordinaire
peut solliciter des contributions externes à la CNT.
La victime peut décider, à n’importe quel moment de la procédure et quelles
qu’en soient les raisons, de saisir d’autres personnes parmi la liste de
référent·e·s ou d’en récuser.

Les :term:`syndicats` de la CNT, à travers le :term:`BC`, doivent se donner les moyens
(financiers notamment et/ou d'hébergements, pour d'éventuels déplacements,
voire informatiques à travers un espace d'échange sécurisé) du bon
fonctionnement et déroulement de la procédure.

Les membres du syndicat et de l’:term:`UD` de l’accusé·e aussi bien que de la
victime ne peuvent en aucun cas faire partie de la commission extraordinaire
pour éviter tout risque de partialité et de pressions extérieures.

Dès sa constitution, la commission extraordinaire informe le syndicat
local de l’accusé·e des accusations portées contre lui/ elle (que la victime
soit elle-même adhérente de la CNT ou pas). À partir de cette information,
par principe de précaution, l’accusé·e est suspendu·e de la CNT, et ce
pendant tout le temps nécessaire à l’établissement du rapport.
Le syndicat local d’appartenance de l’accusé·e est responsable de l’application
de la suspension. Il est clair que cette suspension est provisoire et
n’est pas une prise de décision de la CNT sur le fond de l’affaire.

Le :term:`syndicat` local d’appartenance de l’accusé·e est responsable de l’application
de la suspension. Il est clair que cette suspension est provisoire et
n’est pas une prise de décision de la CNT sur le fond de l’affaire.
Cependant, les organisations confédérales ou fédérales ne reconnaissent
aucun mandat confié à une personne suspendue.

De même aucune participation, aux groupes de travail et aux formations
ne sont possibles pour l’adhérent·e·s suspendu·e·s.

Proposition du SINR 44
=========================

La commission extraordinaire a pour mission de recueillir la parole de
la victime (ce faisant de lui permettre de sortir d’un silence préjudiciable
pour elle à court, moyen et long terme) et tout autre témoignage qu’elle
jugera nécessaire. Elle pourra par ailleurs l’accompagner dans ses démarches
(y compris juridiques) si telle est la demande de la victime.

Quelle que soit la décision de la victime de saisir ou non la justice,
elle ne pourra en aucun cas être jugée sur sa décision par la CNT.
En cas de refus de porter plainte, il ne pourra en être tenu rigueur à
la victime et cela ne vaudra pas non plus mise en doute de son accusation.
La commission ne doit en aucun cas se substituer à la victime et
l’influencer sur cette décision.

Une fois la parole de la victime recueillie, ainsi que tout autre témoignage
que la commission jugera nécessaire, la commission extraordinaire rend ses
conclusions aux référent·e·s.  Les référent·e·s et la commission extraordinaire
rédigent un rapport à partir de ces conclusions qui sera remis à tous les
:term:`syndicats` de la Confédération.

Selon ses conclusions, la commission extraordinaire pourra demander au
:term:`syndicat` d’appartenance de l’accusé·e de proposer une voie de résolution,
après lui avoir transmis son rapport ; si cette voie de résolution est
suffisante pour la victime, elle sera retenue.

Par exemple, il peut s’agir d’accompagnements, de formations, ou de
suspension temporaire auprès de la confédération.

Si cette médiation échoue, la commission peut demander jusqu’à son exclusion
en conformité avec les statuts. En cas de refus du :term:`syndicat` de répondre
à cette demande et plus largement en cas de non- respect de ce protocole
ou d’obstruction à son application, la commission pourra préconiser la
suspension du :term:`syndicat` au prochain CCN, et son exclusion au prochain congrès,
en accord avec les règles organiques « Élaboration de l’ordre du jour du CCN »,
« Commissions de travail confédérales : rôle, limites et compétences »
et avec l’article 24 des statuts confédéraux (« gestion des conflits internes »).

Le :term:`BC`, communiquera alors publiquement la décision des :term:`syndicats`.

Enfin le travail de cette commission est complémentaire d’un travail mené
auprès de l’agresseur (orientation notamment vers une structure d’accompagnement
des personnes auteures de violences sexuelles -structure reconnaissant
le système de domination :term:`patriarcale`), qui peut en constituer un prolongement,
sous réserve que ce travail soit compatible avec la protection de la victime
et que l’agresseur en reconnaisse la nécessité et son inscription dans
un système de domination :term:`patriarcale`.
