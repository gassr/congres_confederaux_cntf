.. <p class="gras vert">paragraphe gras et vert.</p>

.. index::
   pair: Motions ; 2021

.. _congres_CNT_2021_motions:

===============================
**Motions** Congrès CNT 2021
===============================

- :ref:`cahier_mandatee_2021`


.. toctree::
   :maxdepth: 1

   liste_syndicats


.. toctree::
   :maxdepth: 2
   :caption: Synthèse des motions

.. synthese/synthese


.. toctree::
   :maxdepth: 1
   :caption: Motions de stratégies et orientations

   motion_01_2021_ste33
   motion_02_2021_staf29
   motion_03_2021_ptt_centre
   motion_04_2021_ptt_centre
   motion_05_2021_ptt95
   motion_06_2021_etpic30
   motion_07_2021_etpics94
   motion_08_2021_etpics94
   motion_09_2021_interco69
   motion_10_2021_etpic30
   motion_11_2021_etpic30

.. toctree::
   :maxdepth: 1
   :caption: Motions de fonctionnement

   motion_12_2021_sinr44
   motion_13_2021_cnt09
   motion_14_2021_syndicats_42
   motion_15_2021_syndicats_42
   motion_16_2021_cnt09
   motion_17_2021_ptt95
   motion_18_2021_ste93
   motion_19_2021_ste93
   motion_20_2021_etpic30
   motion_21_2021_stp67
   motion_22_2021_etpics94
   motion_23_2021_stt_59_62
   motion_24_2021_ste33_cnt42
   motion_25_2021_ptt_centre
   motion_26_2021_educ21
   motion_27_2021_ptt_centre
   motion_28_2021_etpreci75
   statuts/statuts
   synthese/synthese


.. toctree::
   :maxdepth: 2
   :caption: Annexes aux motions

   annexes/annexes


- :ref:`contre_motions_congres_2021`
- :ref:`congres_CNT_2021_amendements`
