

.. index::
   pair: Fédérations ; Industrie
   pair: Loi ; 20 août 2010

.. _motion_40_2010:

========================================================================================================================
Motion n°40 2010: Fédérations d’industrie et développement syndical de la CNT, SIPM-RP [adoptée synthèse avec motion 41]
========================================================================================================================


.. seealso::

   - :ref:`motion_synthese_des_motions_40_41_2010`
   - :ref:`motions_sipm_rp`



Argumentaire
============

La loi du 20 août 2010 impose les conditions d’ancienneté de deux ans sur
les champs géographique et professionnel de l’entreprise où une structure
syndicale souhaite créer une section syndicale.

Or la CNT ne recouvre pas l’ensemble des champs professionnels et
géographiques par ses syndicats. Mais la désignation d’une section
syndicale peut également être faite par une fédération, ce qui peut être
une possibilité s’il est bien précisé que les fédérations ne deviennent
pas des syndicats nationaux.

Motion
======

La CNT décide de créer toutes les fédérations d’industrie nécessaire à
couvrir  l’ensemble des champs géographiques et professionnels en France.

Ces fédérations n’existent que légalement par le dépôt de bureau en
préfecture ou mairie, mais n’ont aucune existence administrative réelle
au sein de la CNT tant qu’elles n’ont pas été labellisées par un C.C.N.

Ces fédérations pourront au bout de deux ans d’existence désigner des
:term:`R.S.S` dans des entreprises s’il n’existe pas de syndicat local
remplissant les critères de représentativité.

Cependant, la CNT étant composée de syndicats locaux d’industrie et refusant
les syndicats nationaux, les adhérents d’une telle section syndicale devront
dans le même temps de la désignation créer un syndicat local d’industrie ou,
dans un premier temps, au syndicat interprofessionnel de leur lieu géographique.

Toute cette procédure devra être faite en relation avec le bureau confédéral
de la CNT et, si ils existent, les syndicats locaux et unions régionales concernées.


Vote
====

.. seealso::  :ref:`contre_motions_40_41_2010_ste93`

Educ 13
    Fusionner OK, pour les débats, mais pas pour les votes, si les rédacteurs
    de la motion sont d'accord.

SIPM RP
    pour cet amendement.
	Présentation de la motion:

	- valider des stratégies pour créer des fédérations d'industries,
	  pour rattacher des gens qui adhérent surtout dans la presse et le commerce.
	- Pouvoir nommer des représentants de section syndicale sur l'ensemble du
	  territoire comme pour People and baby, et en Moselle, etc..
	- permet de dire que la section syndicale  est représentative au niveau national.


Interco 57
    D'accord avec la fusion, on va compléter l'intervention. Pour le développement
    futur de la CNT, si des camarades veulent basculer chez nous, et bien ils auront
    une structure.
    Les fédés sont plus à mêmes d'être représentatives d'une réalité. Être basé
    sur des fédés, c'est plus réaliste cela empêche d'avoir des syndicats qui
    sont des coquilles vides.

EDUC 91
    C'est une bonne tactique qui est nécessaire.

STICS 38
    vote pour et pour la fusion, développement plus facile.

INTERCO Limousin
    Pour, et aussi une stratégie depuis 2003 pour les archéo qui est un parfait
    exemple de cette stratégie.

EDUC 13
    Pour cette stratégie, mais contre la fusion parce que le 57 est plus clair.
    Elle part des syndicats existants et donc d'une réalité.
    Remarque : commerce industrie service, ce serait un interco et mangerait sur
    tous les champs de syndicalisation, c'est trop complexe.

STE 75
    Contre, le syndicalisme qu'on doit développer doit se développer de bas en haut.
    C'est l'inverse, on est contradictoire avec les motions précédentes.

INTER 57
    Pour, on est pas une coquille vide, mais on est pour, cela permettrait de se
    développer, on est 6 cheminots, ce serait bien, on espère que ce sera pour.
    Si c'est voté, on veut créer cette fede.

STP 67
    On va retrouver là une logique de haut vers le bas. Elle n'engage pas la CNT
    vers une vie fédérale dynamique. Déjà la situation de certaines fédérations
    est précaire, pas de fonctionnement, si on continue de créer ces fédé, ce
    sera pire. Après, il y a des champs d'industrie existants et qu'il faut créer
    ces fédérations. Pour une synthèse, mais contre les deux.

SSEC 59 62
    Désaccord avec l'Educ 13, manque d'être une fédération ils ne sont pas une
    coquille vide, les sections d'entreprises nous ont permis de développer un
    interco intéressant Il faut garder la fusion reprendre le 57 avec les 2 ans
    d'existence pour le RSS.

EDUC 38
    Réservé sur la création par le haut. La création ne peut être faite que si
    c'est un syndicat qui la crée.

STEA
    Pour la fusion, la base c'est des syndicats, pourquoi créer des fédérations ??
    S'il y en a OK, mais pourquoi faire une motion s'il existe une fédération ?

STE 93
    On est a côté des motions, oui ce sont des coquilles vides, mais on parle
    du délai de 2 ans pour designer un RSS.

SIPM RP
    Précision sur le « bas vers le haut » et sur l'inutilité de coquilles vides.
    Oui, la lutte de p&b a permis de differ sur toute la France, et aussi si des
    gens veulent rejoindre la CNT, ils le peuvent parce que la fédération existe;
    pas de volonté autoritaire de mener par le haut... de faire des luttes nationales,
    d'être beaucoup plus forts, 2 ans d'ancienneté, c'est pas nous qui l'avons décidé,
    c'est pour pouvoir faire du syndicalisme.

PTT66
    Motion 40 OK mais 41 non, il faut voir quelle est la convention
    collective la plus intéressante. Il faut attirer l'attention sur ce
    problème.

ETPIC 30
    Pour, nous sommes dans 3 départements avec des sections syndicales
    pour 3 entreprises.
    Pour les conventions collectives, sous la convention collective des
    transports on n'est pas dans la poste, boite privé la fédé nous
    aiderait à nous développer et à travailler.

ETPIC 94
    Mandat régional pour redémarrer une fédération dans le transport ; nous avons pu
    designer un rss, dans une compagnie de voyageurs et pilotes, contacts à Roissy.
    Bagagistes de l'aéroport Roissy, si on parle du transport, ca peut commencer
    à avoir un sens. Il y a matière à fédération, si ces motions sont votées, on
    pourrait faire des prises de contacts.

Santé-social RP
    Pour la motion du sipm et contre celle d'Etpic 57. Il y a une responsabilité
    juridique, donc, attention à la responsabilité juridique, donc il faut s'interroger
    sur les affaires juridiques quand on sait pas ce qui se passe en activité syndicale.
    Les syndicats concernés peuvent peut-être faire une proposition. Commencer par
    des fédérations où il y a des syndicats.

deuxième liste

Educ 91
    **Le véritable enjeu c'est les deux ans**. On espère que les coquilles vides
    vont être remplies.

    Point de détails : les conventions collectives, c'est les entreprises
    qui les choisissent.


ETPIC 57
    Précision "coquilles vides", les réalités dans la fédération : c'est un compte
    rendu de la situation, ce sont des réflexions qui viennent de réalité, des
    cheminots, des structures doivent exister, pour accueillir des camarades.
    Les commerce industrie et service, on s'adapte aujourd'hui à cause de la loi
    de représentativité. On propose au SIPM de se rencontrer pour fusionner les deux
    motions.

STE 75
    Pas contre des nouvelles fédérations, mais alors, faites un congrès
    fondateur, et faites vous labelliser. La question est de passer par
    quel moyen pour créer des fédérations. Faites le en définissant votre
    champ de syndicalisation.

    Que ce soit le Congres qui décide de la création de fédération n'a aucun intérêt.

STE 93
    Public et privé, sont pas d'accord. On propose de recommencer sur cela
    après la pause.

SIPM RP
    Pour gagner du temps, on peut rajouter ce que dit Metz qui permet aux gens
    qui sont syndiqués dans un interco de se rejoindre. Telle quelle la motion
    du SIPM en retirant les exemples de fede. Rajouter "et des syndiqués interco
    qui existent bel et bien", on peut fusionner rapidement.

EDUC 13
    Pas fusionnable, pour nous le problème c'est que le SIPM est moins clair:
    le sipm propose de créer des bureaux, cela pose des problèmes de contrôle,
    ça ne nous semble pas du tout la même chose.

TRIBUNE
    C'est pour ca qu'on demande que vous réécriviez pour qu'on puisse le lire.

ETPIC 30
    On n'a pas de syndicat du transport, si on sort de l'interco, on n'a plus
    d'ancienneté, si on n'a pas de fédération on ne pourra pas créer de section
    syndicale.

PTT 66
    Il y a une fede des transports depuis 1937 voir avec cette ancienneté.


TRIBUNE
    **vote sur Motions n° 40 et 41 reporté en attente d'une :ref:`proposition de synthèse <motion_synthese_des_motions_40_41_2010>`.**


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°40          |            |          |            |                           |
| SIPM-RP              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`contre_motions_40_41_2010_ste93`
   - :ref:`synthese_motions_40_41_2010`
