
.. _amendement_motion_7_2021_stics72:

==========================================================================================================
Amendement N°2 à la Motion n°7 2021 **Drogues, Alcool et émancipation révolutionnaire** par **STICS72**
==========================================================================================================

- :ref:`motion_7_2021_etpics94`

Autres motions STE2

    - :ref:`syndicats:motions_ste72`


Argumentaire 
============


Amendement 
============

.. raw:: html

    La Confédération prend toute la mesure des conséquences de l’alcool
    et autres drogues sur le mouvement révolutionnaire.
    Elle se donne les moyens en son sein de préserver ses espaces de
    réflexion et de prises de décisions, pour développer et construire
    l’émancipation, l’organisation et ses luttes en <del>refusant la consommation
    d’alcool et autre drogues dures dans ses instances</del> <span class="modif_rouge">adoptant un
    comportement de protection des personnes et en les orientant vers
    des organismes qualifiés.</span>



Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°7  |            |          |            |                           |
| STICS72                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
