
.. index::
   pair: Travail; Revendications salariales

.. _motion_protection_sociale_cnt_congres_agen_2006:

=======================================================================================================================
Motion Travail, revendications salariales et protection sociale, 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
=======================================================================================================================




Revendications salariales
=========================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 36         | 14       | 10         | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


.. index::
   autogestion
   répatition égalitaire des richesses


Texte
-----

Les propositions d'augmentation uniforme (en pourcentage, du point d'indice ou
du type «X euros pour tous!») ont le mérite d ́être claires, mais elles
avantagent les salariés les mieux payés.

La CNT lutte pour des augmentations différenciées inversement proportionnelles
aux salaires afin de réduire les inégalités entre travailleurs.

Ce que propose la CNT dans l ́immédiat :

- Réduction des écarts de la grille des salaires et refonte des grilles
  indiciaires.
- Pas d'augmentation des salaires sans relèvement en parallèle des minima
  sociaux, des contrats précaires, des contrats d'apprentissage, des
  indemnités-chômage et des retraites.

**La CNT ne saurait se satisfaire de ces revendications, son objectif étant
l'abolition de toutes les hiérarchies salariales et la construction d'une
société fondée sur la répartition égalitaire des richesses et l'autogestion.**
