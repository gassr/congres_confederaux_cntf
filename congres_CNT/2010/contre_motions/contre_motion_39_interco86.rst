
.. _contre_motion_39_2010_interco86:

=========================================================
Contre-motion à la motion N°39 (interco86)  [pas de vote]
=========================================================

Convaincue qu’elle doit peser sur le cours de la lutte des classes et que la
révolution ne saurait être l’œuvre d’un petit groupe, la CNT choisit la voie
d’un syndicalisme de masse et de classe qu’elle conjugue avec ses orientations
anarchosyndicalistes et syndicalistes révolutionnaires et se donne les moyens
de ses ambitions.

Ses syndicats et fédérations peuvent avoir recours à des permanents techniques
si leur développement l’exige et se présentent aux élections professionnelles,
aussi bien dans le secteur public que dans le secteur privé, chaque fois qu’ils
le jugent utile à leur développement qui se confond avec celui des luttes.

L’expérience du mouvement ouvrier a montré que la présence de permanents présente
un danger de bureaucratisation. Ainsi, au cas où des syndicats ou des fédérations
estimeraient nécessaires d’avoir recours à des permanents salariés, les permanents
ne pourront en aucun cas être membres du bureau du syndicat ou de la fédération.


.. _vote_contre_motion_39_2010_interco86:

Vote contre-motion à la motion N°39 (interco86)
===============================================

Proposition du STE 93 de reprendre la contre motion de l'Interco 86

SIPM RP
	voter sans débat.

Etpic 57
	Pour. Motion 1 ne pourra être votée efficacement que si congrès se positionne sur cette
	contre-motion. Si reprise de cette motion, on pourra enfin discuter de ce débat non tranché
	(permanents syndicaux).

Educ 93
	On est fatigué des discussions sur internet, des bruits de couloir. Si on ne s'exprime par
	sur la question des salariés/permanents dans un congrès où va t on le faire ? Contre-motion
	maladroite, mais prenons la comme base afin de voir ce que pense la CNT.

Etpic 30
	Souci car la motion apporte 2 sujets différents : les élections et les permanents
	techniques , alors que ces 2 sujets sont dissociables.

Interco 86
	On ne discutera pas cette contre-motion car on l'a retirée. On en reparlera dans l'avenir;
	car si la CNT poursuit son développement, ces questions se reposeront. On voulait faire quelque
	chose de frontal, lever un tabou, mais cette façon de poser les choses étaient maladroites.

SUTE 69-01
	On a voté contre cette contre-motion car trop floue, elle mélange trop de chose,
	notamment à la fois la question des élections professionnelles et celle des permanents techniques.

Educ 13
	Abstention car estime que ça n'a pas lieu d'être. On n'a pas préparé cette discussion, on ne
	peut pas se positionner.

SIM RP
	d'abord définir ce dont on parle : permanents techniques, permanents, salariés, personnes
	à fonctions multiples mais non permanents. SIM a deux permanents techniques : un au SMIC, un
	retraité. Quand beaucoup de syndiqués, quand ils sont dans des situations difficiles. C'est au
	syndicat de contrôler.

Etpic 57
	on veut aller plus loin dans ce débat. Quelle stratégie syndicale pour le développement de
	la CNT ? Problème quand on voit que le plus gros syndicat de la Conf ne participe pas à un grand
	mouvement de grève comme celui des retraites. A quoi ça sert d'être nombreux quand on ne pèse
	pas dans la lutte des classes ? Mutualisation des défenses, des revendications... On n'en entend pas
	parler.

Educ91
	Il y a déjà eu des permanents dans la CNT-E et -F. Permanent aux Vignoles ? nettoyage
	s'est expliqué sur pourquoi il y a peu d'information sur ce syndicat.

PTT Centre
	sur les permanent : OK avec le 91, il y a eu des permanents à la CNT. Ne pas
	s'interdire par avance.

STE75
	contre-motion rejoint directement la motion circonstancielle sur laquelle on a un mandat.

Educ 93
	merci, on voulait ce débat. On débattra du nettoyage dans la motion circonstancielle.
	Camarades qui parlent de permanents sont tous des vieux permanents.... On mélange tout entre
	permanents, salariés, retraités, étudiants... Pas de vote ici, mais on veut ce débat de fond

SII RP
	Ce n'est pas une question de stratégie mais de tactique = comment on se positionne sur le
	champ de bataille pour atteindre nos objectifs. Position non arrêtée car pas de besoin pour nous. Si
	un jour on est plus nombreux, et qu'on a besoin on se posera la question.

Etpic94
	le sujet = permanent. Pas nettoyage.
	Question des permanents relève des différentes réalités de terrain. Cas par cas, syndicat jugent pas
	eux mêmes. Statuts de 2008 ne reprennent pas les statuts de 46 qui autorisaient l'existence de
	permanents.

Etpic30
	On veut savoir de quel syndicat il est permanent, si le SIM se fait représenté au congrès
	par un permanent.

Interco 57
	permanent syndical = quelqu'un qui mange dans la main du patron.
	Question : on veut ça?

CCS44
	on est une orga anarcho-syndicaliste où il y a des gens qui veulent payer des gens pour
	passer le balai à leur place ?

SIPM RP
	question des permanents et salariés sont des questions que la CNT seule choisit, sans
	obligation de l'état ou du patronat comme par ex pour les élections prof. Donc on ne peut pas se
	cacher derrière des obligations. Aujourd'hui, il n'y a pas de motion de congrès interdisant l'existence
	de permanents et salariés, mais cela peut changer. La question pour nous, c'est comment s'organiser
	collectivement pour que l'on n'est pas besoin de salariés ni permanents. Discuter des moyens
	collectifs.
	Permanents et salariés = quelqu'un qui a une relation salariale avec le syndicat.

STE 33
	Problème car lien fait entre le développement du syndicat et l'existence de permanents
	alors que vu qu'on est autogestionnaire, autoformation, ça devrait être l'inverse : plus on est, moins
	on a besoin d'en faire.

nettoyage RP
	Tout travail mérite salaire.

SUTE 69601
	salue manière construire dont STE93 pose le débat sereinement. Position : mandat de
	voter contre la contre-motion car trop floue. Favorables à la participation aux élections prof si
	nécessaire mais syndicat plutôt défavorable aux permanents/déchargés. Débat complexe, dépend de
	notre développement.

Etpic57
	Choix de développement = The question

Interco12
	il faut savoir terminer un débat

SIPM RP
    stratégie de développement. Avant de penser à un quotidien syndical, qui va
    reprendre le CS ? Ingratitude vis à vis des gens qui prennent des mandats.

PTT Centre
    reporter à plus tard ces nécessités. Question du salarié ne s'est posée que
    dans un seul syndicat.



+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 39       |            |          |            |                           |
| interco 86                         |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


**Fin du débat. Pas de vote.**


.. seealso:: :ref:`motion_39_2010`
