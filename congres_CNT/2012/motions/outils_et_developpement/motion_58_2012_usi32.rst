
.. _motion_58_2012:
.. _motion_58_2012_usi32:

=======================================================================================================================================
Motion n°58 : Création d'un secrétariat confédéral «Secteur vidéo» (USI32)
=======================================================================================================================================
  
.. seealso::

   - :ref:`usi32`
   - :ref:`motions_usi32_2012`




Préambule
=========

Cette motion a été rédigée par la commission confédérale «Secteur vidéo».

Une commission ne pouvant pas proposer de motion au congrès, le syndicat
interprofessionnelle du Gers (USI 32) a accepté de la porter.

Argumentaire
============

Compte-tenu des réactions positives que nous avons enregistrées de la part de
nombreux syndicats, militant-e-s et sympathisant-e-s CNT au sujet du travail
fourni par la commission confédérale « Secteur vidéo »
(cf les bilans sur la fréquentation du site, les vidéos produites et/ou
difusées, les projets en cours...), compte-tenu aussi de la décision
du CCN de juin 2012 de doter cette commission d'un premier équipement
en matériel, nous pensons aujourd'hui qu’il faut pérenniser l’existence du
Secteur vidéo.

Par conséquent, nous demandons aux syndicats de donner à cet outil le
statut de secrétariat confédéral.


Motion
======

La CNT décide de créer un secrétariat confédéral « Secteur vidéo » (SV).

Celui-ci se substitue à la commission confédérale existante afin de pérenniser
l'existence de cet outil confédéral et de poursuivre le travail engagé depuis
deux ans :

Mise à disposition des vidéastes de la CNT d'un matériel vidéo et d'un espace
de travail collaboratif, mutualisation de leurs productions et de leurs
compétences, animation d’un site spécifiquement dédié à la vidéo, à l'image
et au son, difusion et production de vidéos spécifiquement CNT ou pas et
visant à contrecarrer la propagande de l'État et du patronat délivrée à
travers les mass-médias.

Le Secteur vidéo est constitué :

- d'un secrétariat d'une ou deux personnes mandatées par le congrès confédéral
  (avec l'aval de leur syndicat respectif),
- d'une équipe constituée de tout-e adhérent-e souhaitant s'investir dans le
  domaine de l'audio-visuel (avec l'aval de leur syndicat respectif).

Procédures de validation des vidéos
-----------------------------------

La validation d'une vidéo, réalisée et/ou montée par le SV et n'engageant que
lui-même (exemple : clip d’auto promotion du secteur vidéo), se fait après
débat et vote à la majorité au sein du SV.

La validation d’une vidéo réalisée et/ou montée par le SV pour le compte
d’un syndicat, d'une union locale, d’une union régionale, d’une fédération,
d’une commission ou d’un secrétariat confédéral, etc, se fait en concertation
avec la structure concernée.

La validation d’une vidéo réalisée et/ou montée par le SV pour le compte de la
confédération dans son ensemble se fait en concertation soit avec la CA et le
secrétariat confédéral (s’il s’agit d’une vidéo d’actualité), soit avec le
CCN ou le congrès confédéral (s’il s’agit d’un projet plus sensible, comme
une présentation générale de la CNT).



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°58          |            |          |            |                           |
| USI32                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
