
.. index::
   pair: relations média; communiqués de presse

.. _motion_relations_medias_cnt_congres_toulouse_2001:

======================================================================================
Motion "Relations médias", 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
======================================================================================


Vote
====

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 29         | 10       | 1          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
=====

Suite a la récente médiatisation de la CNT, c'est à dire le fait que la C.N.T.
apparaisse de plus en plus dons les différents médias, un règlement s'impose.

A l'échelle nationale, soit un poste aux relations médias est créé, soit
il y a élargissement de la responsabilité propagande au B.C. pour sortir
régulièrement des communiqués de presse sur les campagnes nationales ou les
actions d'importance, communiqués moins à caractère de propagande que
d'information pure relatant des faits sur les actions et positions de la C.N.T.

**Le Congrès confédéral désigne un mandaté aux relations médias suite à cette
motion adoptée.**

.. seealso:: :ref:`motions_relations_medias_cnt`
