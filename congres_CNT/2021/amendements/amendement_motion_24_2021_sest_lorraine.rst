
.. _amendement_motion_24_2021_sest_lorraine:

========================================================================================================
Amendements N°1 à la Motion n°24 2021 **Gestion interne des violences patriarcales** par SEST Lorraine
========================================================================================================

- :ref:`motion_24_2021_ste33_cnt42`
- :ref:`ssct_lorraine_2017_03_05`
- :ref:`role_de_linterpro_31_fouad`


Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`


Argumentaire
===============

Le SEST Lorraine remercie les syndicats pour leur travail de réflexion
et pour la proposition de protocole.
**Nous proposons d’y apporter ces amendements.**


Amendements
============

Motion
---------

Toute personne reconnue coupable de viol/violences patriarcales n’a pas
sa place à la CNT.
Adoption par le 35eme congres Confédéral de la CNT d’un protocole qui
vise à établir des outils et une procédure à suivre systématiquement
dès qu'une accusation de viol et/ou de faits de violence patriarcale
est portée à la connaissance de membres de la CNT contre un・e de ses
membres.

Protocole
---------

- Toute personne accusant de viol et/ou agression sexuelle un・e membre
  de la CNT est considérée comme une victime et bénéficie de tous les
  traitements et égards dus a ce statut (aide psychologique, juridique,
  financière en cas d’incapacité au travail) et ce le temps nécessaire
  à l’instruction de l’affaire et sans jugement au préalable de son issue.
- La CNT met en place une liste de personnes référent・e・s compétent・e・s
  sur les questions des violences patriarcales. Ces référent・e・s sont
  désigné・e・s par leur syndicat/Union Régionale.
  Cette liste est mise à disposition de tou・te・s les adhérent・e・s de
  la CNT et actualise quand nécessaire.
- La victime peut alors saisir (elle-même ou par l’intermédiaire d’une
  tierce personne) la commission antisexiste, référente en qui concerne
  les violences patriarcales, pour porter accusation. La commission
  antisexiste alerte le :term:`BC`.
- Le :term:`BC` contacte l'ensemble des referent・e・s. Les referent・e・s
  sont alors charge・es de mettre en place une commission extraordinaire,
  non mixte ou mixte suivant le choix de la victime. Les membres
  de cette commission extraordinaire sont choisis parmi la liste des
  referent・e・s. La victime a la possibilite de recuser un・e ou des
  referent・e・s. Cette commission extraordinaire a tout mandat pour
  instruire l’affaire, decider, en accord avec la victime, quand lever
  l'anonymat. La diffusion du compte-rendu de ses travaux a la Confederation
  sera anonyme ou pas suivant le choix de la victime. La commission n’est
  nommée que pour le temps de l’instruction.
  La victime est seule abilitée à décider des modalités de sa prise en
  charge (mixité choisie, récusations de personnes de la commission antisexiste,
  levée de l’anonymat, dépôt de plainte...)
  La CNT, a travers la commission antisexiste, doit se donner les moyens
  (financiers notamment et/ou d’hébergement pour d’éventuels déplacements,
  voire informatique a travers un espace d’échange sécurise) du bon
  fonctionnement et déroulement de la procédure pour mener à bien sa mission.
- Les membres du syndicat et de l’UD de l’accuse・e aussi bien que de la
  victime ne peuvent en aucun cas faire partie de la commission antisexiste
  qui s’occupera de l’affaire afin d’éviter tout risque de partialité et
  de pressions extérieures.
- Des sa constitution, la commission extraordinaire, le :term:`BC` informe
  le syndicat local de l’accuse ・e des accusations portées contre lui/elle
  (que la victime soit elle-même adhérente de la CNT ou pas). A partir
  de cette information, par principe de précaution, l’accuse・e est suspendu・e
  de la CNT, et ce pendant tout le temps de l’instruction. Le syndicat local
  d’appartenance de l’accuse・e est responsable de l’application de la suspension.
  Il doit être clair que cette **suspension ne préjuge pas de la culpabilité**.
- La commission antisexiste a pour mission de recueillir la parole de la
  victime, de l’accusé.e et tout autre témoignage qu’elle jugera nécessaire.
  Une autre de ses missions est d’aider accompagner la victime à porter
  plainte, si elle le souhaite, et ce faisant de lui permettre de sortir
  d’un silence préjudiciable pour elle a court, moyen et long terme.
- Quelle que soit la décision de la victime de saisir ou non la justice,
  elle ne pourra en aucun cas être jugée sur sa décision par la CNT.
  En cas de refus de porter plainte, il ne pourra en être tenu rigueur à
  la victime et cela ne vaudra pas non plus mise en doute de son accusation.
- Une fois la parole de la victime recueillie, La commission antisexiste
  rend ses conclusions aux référent・e・s. Les référent・e・s et la commission
  extraordinaire se réunissent alors et prennent ensemble une décision qui
  est remise a tous les syndicats de la Confédération.
- L’avis de la commission antisexiste sera automatiquement un point à
  l’ordre du jour du prochain congrès (ordinaire ou extraordinaire) ou à
  défaut du prochain CCN. Le CCN définit les modalités temporaires et le
  congrès les décisions définitives quant à la situation de l’accusé.e
  au sein de la Confédération. Étant entendu que ce.tte dernier.e reste
  suspendu.e jusqu’à l’issue du vote du congrès.
- Il ne peut en aucun cas être laisse a la responsabilité du syndicat
  et de l’UD de l’accusé・e de définir la sanction a porter contre l’accusé・e.
- Selon les conclusions de la commission extraordinaire du congrès,
  l’accusé・e pourra être exclu・e de la CNT. La CNT communique alors
  publiquement sa décision d'exclure un・e camarade.
- Le syndicat de l’accuse.e est chargé d’appliquer immédiatement la
  décision du congrès. A défaut, le congrès se réserve le droit d’exclure
  le syndicat tels que le prévoit les statuts.
- Dans tous les cas, le :term:`BC` s’engage a donner les moyens financiers,
  via la ligne de compte « Solidarité et procédures », à la commission
  antisexiste afin d’assurer un accompagnement aux protagonistes, s’ils le
  souhaitent, sans interférer avec le déroulement de la procédure ni
  évidemment avec ses conclusions. Le :term:`BC` donne les moyens à
  l'ensemble des référent・e・s de la commission antisexiste de continuer
  à se former et de se réunir si ils ou elles en éprouvent la nécessite.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°24 |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
