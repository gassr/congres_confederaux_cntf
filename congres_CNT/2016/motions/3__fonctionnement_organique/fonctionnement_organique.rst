
.. index::
   pair: Motions; Fonctionnement organique 2016
   pair: Fonctionnement ; organique


.. _motions_fonctionnement_organique_2016:

=====================================
Motions Fonctionnement organique 2016
=====================================

.. toctree::
   :maxdepth: 4

   motion_4_2016_etpic30
   motion_5_2016_etpic30
   motion_6_2016_sante_social_lorraine
   motion_7_2016_sante_social_lorraine
   motion_8_2016_sante_social_lorraine
