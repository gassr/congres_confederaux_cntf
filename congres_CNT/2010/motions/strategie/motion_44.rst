


.. _motion_44_2010:

=========================================================================================
Motion n°44 2010 : Développement et campagnes de syndicalisation, Education 13 [adoptée]
=========================================================================================




Argumentaire
============

Pourquoi se développer ? Pourquoi des campagnes de syndicalisation ?

La C.N.T est fragile et faible numériquement, l'implantation de nos fédérations
d'Industrie est géographiquement limitée et à part très modestement dans
certains secteurs, il y a peu de réalité d'implantation syndicale.

Globalement notre Confédération a peu de prise sur l'actualité sociale et
les luttes syndicales. Grossir, se développer, en améliorant notre implantation
dans les secteurs où nous sommes présents puis élargir notre champ d'action
à d'autres secteurs, semble donc important.

Ces dernières années, au niveau confédéral, nous n'avons pas eu de stratégie
de développement et nous sommes contentés d'apparaître de-ci de-là de manière
ponctuelle dans diverses luttes dont nous n'étions pas toujours les initiateurs,
lors des manifestations des grandes «journées d'action » que nous critiquons
par ailleurs...

Tout repose en fait sur l'échelon local sans coordination et élan à l'échelle
nationale... Il appartient alors de s'appuyer sur ces implantations locales
comme sur les fédérations d'industrie existantes, pour donner un élan au
développement syndical de la Confédération.

Le lancement de campagnes de syndicalisation, localement mais coordonné nationalement,
présente plusieurs avantages : la maîtrise de notre propre calendrier, une activité
régulière sur le long terme et non plus en dents de scie au gré de l'actualité, un
travail interprofessionnel fédérateur pour les adhérent-e-s, cela renforce le
développement de la C.N.T et des fédérations existantes dans divers secteurs à l'échelle
nationale, cela permet de faire connaître la C.N.T en dehors des cercles militants et
d'asseoir son identité syndicale y compris dans d'autres catégories de travailleurs
que ceux visés par les campagnes...

De manière générale, la convergence des luttes et la grève générale que nous voudrions,
resteront une illusion sans le développement d'un syndicalisme combattif et révolutionnaire
dans un maximum de secteurs professionnels pour y déborder les bureaucraties syndicales.

Cela serait l'objectif à long terme de ce type de campagne de développement.

Motion
======

Les fédérations d'industrie du public comme du privé, les syndicats implantés
dans des secteurs professionnels particuliers (Nettoyage, Métallurgie,
Transport, Commerce...) ou ayant des sections dans des entreprises nationales
(Exapaq, Interior's, PSA...) réalisent du matériel généraliste de syndicalisation
propre à leur secteur ou entreprise (affiches, tracts reprenant les principales
revendications de la C.N.T dans le secteur ou entreprise, documents juridiques...).

Ce matériel peut être accompagné de documents, à destination des militants,
comportant conseils sur l'intervention dans ces entreprises ou secteurs, des
argumentaires concernant les revendications ou enjeux du secteur.

Dans la mesure du possible, les fédérations et syndicats concernés se dotent
de référents pour ces campagnes. La Confédération se dote d'un secrétaire
confédéral adjoint se chargeant du suivi et de la coordination des campagnes
de développement syndical.

Le secrétariat à la coordination des campagnes de développement syndical,
en lien avec le Bureau confédéral informe les syndicats de la sortie de
ce matériel par le biais de la circulaire confédérale ou autre moyen de
communication.

La sortie coordonnée de matériel à l'échelle nationale, pour réduire les coûts
et améliorer la qualité, est envisageable après retour des syndicats ou unions.

Les syndicats, en particulier les syndicats interprofessionnels ayant vocation
au développement de la C.N.T dans de nouveaux secteurs, ou les unions,
choisissent, en fonction des réalités locales (particularités du bassin
d'emploi...) de mener une ou des campagnes de syndicalisation proposées, sur
le long terme (au minimum plusieurs mois). Ceci par le biais de tractages et
affichages, ciblés et réguliers, autour des lieux de travail de ces secteurs
et entreprises, des réunions publiques, ou encore par un relais dans les
publications cénétistes locales, papier comme Internet.

En cas de retours positifs sur le terrain, les syndicats ou unions prennent
contact avec les fédérations ou syndicats concernés pour envisager ensemble
des réunions d'accueil des nouveaux adhérents, des formations internes, une
assistance juridique.

Le mandaté confédéral peut aussi prêter assistance aux syndicats et unions
notamment en cas de création de nouveaux syndicats.

Le site Internet confédéral est la vitrine de la C.N.T. Il doit donc se faire
largement l'écho de ces campagnes avec des rubriques correspondantes qui
regrouperaient le matériel de syndicalisation propre à chaque secteur.

Le mandaté confédéral supervise la publication de ce matériel sur le site.

Discussion
==========

**SIPM**
    ce sont les syndicats et les fédérations qui décident car la C.N.T est
    autogestionnaire.

**CCS 44**
	On est contre. Les fédérations d'industrie du public comme du privé n'existent
	pas, nous avons seulement des fédérations d'industrie.


Vote développement et campagnes de syndicalisation, Education 13 [adoptée]
==========================================================================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°44          | 35         | 14       | 11         | 4                         |
| Education 13         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

**Motion N°44 est adoptée**
