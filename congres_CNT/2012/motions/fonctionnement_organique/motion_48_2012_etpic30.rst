
.. _motion_48_2012:
.. _motion_48_2012_etpic30:

===================================================================================
Motion n°48 2012 : Examen des candidatures aux mandats confédéraux (ETPIC 30 Sud)
===================================================================================
  

.. seealso::

  - :ref:`motions_etpic30`




Préambule
=========

Le 31e Congrès confédéral de Saint-Étienne a vu apparaitre pour la première
fois, une candidature groupée au mandat des diférents secrétariats confédéraux,
présentée comme indivisible, émanant d’une seule et même localité
(l’agglomération lyonnaise).

Cette situation nouvelle, inédite, a entrainé de longues et difficiles
discussions au cœur d’un Congrès déjà conséquemment alourdi par des questions
difficiles.

Le Congrès n’a pas souhaité traiter cette présentation collective de
mandatement, au profit d’un examen individuel des candidatures.

Le vote de cette décision ne fut néanmoins peu ou pas significatif quant à la
réelle volonté des syndicats CNT présents (32 pour, 24 contre, 2 abstention,
et 10 ne participe pas au vote).

Il reste la traduction d’un sentiment collectif assez partagé.

Face à la volonté persistante de ces candidats à n’agir qu’entre eux, ou face
à cette mise en concurrence totalement déséquilibrée, toutes les autres
candidatures individuelles émanant de diférents territoires se sont désistées
au 31e Congrès.

Nous estimons que l’examen par le Congrès de candidatures collectives:

- Privilégie les grosses agglomérations, plus à même de pouvoir faire émerger des
  équipes par des candidatures groupées ;
- Peut induire une logique de concurrence contraire à l’esprit d’intérêt général
  propre au mandatement confédéral ;
- Ne favorise pas l’élection d’équipes de candidats issus de diférents territoires ;
- Peut conduire à une « logique de tendance » par l’association de programmes aux
  candidatures groupées (vu en 2010) ;
- Peut être couplées de logiques affinitaires contraire à l’esprit de l’action
  syndicale.

Ainsi, l’examen de candidatures collectives nous apparait ainsi contraire, en
l’état numérique actuel de la CNT, aux équilibres de la vie démocratique de
la CNT et à l’investissement d’intérêt général inhérent à l’exercice d’un
mandat confédéral.

Motion
======

Le Congrès confédéral de la CNT examine individuellement les candidatures aux
diférents mandats confédéraux, même lorsque ces dernières sont présentées ou
annoncées de facon collective.

Chaque candidatE est invitéE à présenter directement, ou par le biais de
syndicat d’appartenance, les motivations de candidature en terme de
perspective d’investissement et de travail en commun.

En vue de favoriser un meilleur ancrage territorial et une multiplicité
d’approches, le Congrès confédéral tend à privilégier le mandatement de
personnes émanant de différents territoires et de différents syndicats.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°48          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
