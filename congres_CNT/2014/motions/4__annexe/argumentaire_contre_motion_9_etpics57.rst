

.. _arg_contre_motion_9_2014_etpics57:

===================================================================================
Argumentaire de la contre-motion à la motion n°9 présentée par l'ETPICS 57 nord sud
===================================================================================




Le syndicat ETPICS 57 Nord Sud a pris connaissance, avec intérêt, de la motion présentée par le
SINR 44 concernant un rapprochement éventuel avec le secteur dit fédéraliste de la CNT AIT.
Toutefois, et pour plusieurs raisons, notre syndicat présente cette contre motion afin de palier au
manque criant, selon notre point de vue, d'un argumentaire à la hauteur des enjeux pour les courants
anarcho-syndicalistes et syndicalistes révolutionnaires à la vue de la situation historique actuelle. Ce
manque d'analyse ferme les portes à une construction en phase avec les réalités traversées, pour ne
pas dire subies, de notre organisation et des courants qu'elle représente en tant qu'organisation
syndicale spécifique. S'il est évident, et nous y reviendrons plus bas, que cette réalité, faite de reculs
des luttes sociales et de dépolitisation croissante des classes populaires, touchent l'ensemble des
organisations syndicales et politique se réclamant de la transformation sociale, de la solidarité
internationale et de la lutte des classes, nous ne saurions nous satisfaire de cette seule réponse sans
tirer un bilan de notre action et de l'état de l'organisation ces deux dernières décennies. Soit depuis
la scission de 1993 qui a été de fait, un « aggiornamiento » syndical de l'ASSR en décidant de la
participation aux élections professionnelles dans le privé.
Pour le secteur qui donnera naissance à la CNT-F, le choix de se présenter aux IRP afin de pouvoir
protéger nos militant-e-s nous a permis de construire des sections syndicales d'entreprises,
d'acquérir des pratiques et des connaissances et de donner une visibilité à notre projet syndical.
Nous avons caressé l'espoir de devenir, si ce n'est le pôle de recomposition du syndicalisme de
classe, autogestionnaire et d'action directe, du moins une organisation influente dans le paysage
syndical français, en capacité de porté des luttes dans des secteurs ultra précarisés tout en ayant une
dynamique de développement basée sur des initiatives propres révélant si ce n'est un caractère de
masse du moins un caractère certain. La CNT-F s'est développée, vite, très vite. Trop vite ?
Probablement. Cependant, cette partie de notre histoire s'est produite au moment où une nouvelle
phase s'est ouvertes pour le Capitalisme, liée à deux facteurs majeurs : son état d'évolution
intrinsèque et la chute du mur qui a modifié le paysage géostratégique mondial, et fait (ré)
apparaître de nouveaux acteurs sur la scène internationale.
État structurel du Capitalisme ici et ailleurs....pour faire court :
Depuis le début des années 2000, le MEDEF est en pleine offensive afin de faire progresser le
concept de Refondation Sociale théorisé par Denis KESSLER. Selon lui, « il s'agit de défaire point
par point le programme du Conseil National de la Résistance de 1944 » (Challenge, 4/10/2007).
Pour le patronat français, l'heure est venue de récupérer tout ce qu'il a du céder il y a 70 ans. Il voit
comme une opportunité sans précédant la disparition de toute la génération ayant participé aux
luttes ouvrières du siècle passé. Le développement de la consommation de masse et des moyens de
communication, « l'amélioration » du contrôle social des individus et leur abrutissement grâce aux
médias ont permis de créer une société aseptisée. L'échec historique du bloc soviétique et du
socialisme autoritaire est venu renforcer la stratégie de dénigrement systématique de toutes
alternatives économiques politiques, sociales, environnementales émancipatrices au modèle
capitaliste ambiant : État restreint à ses fonctions régaliennes, ultra libéralisme, flexibilité totale,
guerres pour s'approprier ressources stratégiques et/ou débouchés économiques aux conséquences
de plus en plus dramatiques partout dans le monde....du moins pour les classes populaires.
Les gouvernants et responsables économiques ne sont jamais inquiétés pour leurs agissements, car
toujours protégés soit par leurs armées aux capacités de destructions inégalées dans l'Histoire de
l'Humanité, soit par un système policier ultra sophistiqué.
Pour ce faire, en France, le Capital a organisé, durant près de 15 ans, grâce à ses relais médiatiques
(Lagardère, Bouygue, .....), une guerre de mouvement idéologique afin de déporter le débat sur des

terrains lui permettant de créer une ambiance de sinistrose, ne laissant place à aucune autre
perspective : montée de l'insécurité et guerre contre le terrorisme ont alimentés la presse durant les
15 dernières années.
La crise financière de 2008 a vu les États venir au secours des banques qui se sont servies
allègrement sans aucune riposte de classe.... bien que les grèves de 2010, malgré la défaite,
atténuent ce propos.
Confrontés à une crise sans précédent au niveau mondial, tant économique que politique ou
écologique (raréfaction des matières premières, manque de débouchés et ce malgré l'obselance
programmée, conflits un peu partout sur la planète, affaiblissement politique du système de
domination par la démocratie représentative, corruption éhontée, montée des extrêmes droites et des
fanatismes religieux, ....,), l’État et le Capital Français n'ont d'autres choix que d'accentuer leur
propagande idéologique et leur répression policières afin de contraindre les classes populaires.
Obnubilée par la chimère de l'intervention étatique afin de réguler le capitalisme, la gauche
gestionnaire, si utile aux capitalistes afin de boucler la boucle, finie sa phase de décomposition et
d'implosion : elle termine le travail de libéralisation totale du marché français engagé par elle même
en 1997 en contradiction complète avec ses promesses de campagne. Les sentiments de trahison,
d'impuissance, de désespoirs et de résignations n'ont jamais été aussi fort dans les classes
populaires. La montée de l'extrême droite ne se fait pas seulement qu'avec l'iceberg émergé du Front
National mais également et surtout avec une pénétration de ses idées jusqu'au sein de la gauche de
droite aujourd'hui au pouvoir.
Cette séquence, que nous le voulions ou non, nous impacte directement car elle laisse un vide du
politique dans le débat « public » et se place de fait comme une pierre angulaire dans l'évolution de
la situation qui est de plus en plus catastrophique. A tel point que beaucoup en sont à se demander
quand un gouvernement d'unité nationale verra-t-il le jour sous couvert d'une justification que nous,
Hommes et Femmes du Peuple, n'aurions jamais la cruauté d'inventer, et de ce fait encore
insoupçonnable ? Nous pourrions même nous demander quel bloc impérialiste fera sauter
l'Humanité et la planète le premier tant les scènes surréalistes se succèdent au jour le jour ?
État du mouvement social, de l'ASSR et des CNT.
Les taux importants d'abstention aux différentes élections politiques montrent que toute la
population n'est pas favorable à ce basculement mais l'absence d'un mouvement social puissant, ou
du moins d'une dynamique initiée par les minorités agissantes des classes populaires pour y arriver,
empêche toute perspective pour l'heure. A l'International, la situation est variable en fonction des
pays mais, malgré nombre de mouvements sociaux à travers le monde, aucun pays n'a vu la victoire
des forces révolutionnaires se faire. Il peut y avoir des progressions, des résistances, mais pas de
victoire. Au contraire, les forces réactionnaires progressent dans de nombreux endroits et
l'émergence de nouveaux blocs capitalistes favorisent les conflits entre les États et l'affaiblissement
des luttes émancipatrices. C'est mentir que d'affirmer le contraire.
Le travail idéologique de la classe capitaliste couplé aux défaites ouvrières de 2003, 2007 et 2010
favorisent les replis sectaires. Chaque chapelle veut avoir raison et le nombre de plus en plus
restreint de militant-e-s dans le mouvement social accentue encore plus ce phénomène. C'est le
serpent qui se mord la queue. Moins il y a de victoires, moins il y a de militant-e-s, moins il y a de
dynamiques, moins il y a de luttes, donc moins il y a de victoires. Croire que la paupérisation
croissante de la population l'amènera à s'organiser automatiquement pour la Révolution Sociale est
une erreur stratégique énorme qui ne peut profiter qu'à nos adversaires. Il y a une différence
fondamentale entre être Révolté, qui consiste à ne pas se satisfaire de sa situation individuelle tout
en acceptant les codes et valeurs du système dominant et être Révolutionnaire, qui consiste à l'état
de conscience des processus de développement, de fonctionnement et d'analyse de ce qu'est le
capitalisme en tant que système et que classe afin de lui opposer un plan coordonné afin de le

détruire et de le remplacer par un système qui pour nous doit être Communiste et Libertaire et issu
du syndicalisme afin d'organiser au mieux la future société tant économiquement que politiquement.
A titre d'exemple, la Grèce, qui a vécu des phases insurrectionnelles importantes, n'a pas vu la
révolution se faire car il manque, à notre sens, une structure syndicaliste révolutionnaire de masse
afin de pouvoir opérer le basculement et mettre à bas l’État. Pour le dire autrement, au vu de son
affaiblissement, si l’État Grec n'est pas tombé, ce n'est pas tant grâce à l'appui de ses alliés de
l'Union Européenne que par la faiblesse de notre courant qui est quasiment inexistant las bas. Les
insurectionnalistes sont confrontés à leur faiblesse organisationnelle, car ils ne sont pas en mesure
de présenter un plan cohérent d'organisation de la future vie sociale à la population. Cette situation
permet le maintien de courants autoritaires tel que le KKE ou la recomposition du réformisme au
sein de Syrisa. L'extrême droite perce et seule l'histoire récente de la Grèce empêche à notre sens un
basculement vers la réaction.
Pour en revenir à la France, les brèches ouvertes dans les conquêtes sociales sont également dues à
la faiblesse du syndicalisme de classe qui n'a pas su faire son unité ni assurer son développement en
se dotant d'outils de qualité permettant au maximum de militant-e-s de se former aux pratiques de
démocraties et d'actions directes lorsque la fenêtre de tir s'est présentée à partir de 1993 jusqu'à
2003-2006. Le désaveu grandissant vis à vis des centrales syndicales classiques, empêtrées dans la
cogestion et la collaboration de classe (systèmes paritaires, financement des OS et permanentat), et
donc leur refus d'organiser la lutte sociale nous a profité quelque temps, permettant à la CNT-F,
comme nous le disions plus haut, de se développer. Solidaires en a également bénéficié.
Nous pensons qu'il est important de discuter ici de cette organisation car elle se place, en tant que
syndicat alternatif et pour nombre de ses structures, sur le terrain de la lutte des classes, du moins
dans ses velléités et ses positionnements. Nous la rencontrons régulièrement lors de rencontres
internationales, preuve en est nous avons des partenaires communs et donc, de fait, des analyses
communes. Elle compose ce que nous pouvons appeler la « gauche syndicale » avec des secteurs
importants de la CGT et dans une importante moindre mesure des secteurs de FO et de la FSU,
avec, pour les syndicalistes de classes adhérents dans ses OS de véritables problèmes et
incohérences idéologiques et stratégiques. Elle aspire autour d'une dialectique de lutte et de
conflictualité sociale à être un pôle de recomposition du syndicalisme de classe, lorgnant sur cette
gauche syndicale qui regroupe dans ses composantes nombres d'ASSR faisant le choix d'être dans
des organisations dites de masses bien que le taux de syndicalisation extrêmement faible relativise
terriblement ce qu'est « être de masse » . Toutefois, son manque de projet de société affirmé, au
delà de certains postulats et son refus de se Confédéraliser, favorisent les corporatismes et donc sa
bureaucratisation. Elle voue, à terme, Solidaires à l'échec.
Concernant la CGT, qui est l'organisation historique de la classe ouvrière française, sa
bureaucratisation totalement verrouillée empêche tout retournement historique. Seule la non
existence d'une organisation ASSR, si ce n'est de masse, du moins dynamique, en mouvement et
dotée de perspectives revendicatives immédiates empêche, à notre sens, leur départ. Si le manque
de formation de qualité et le verrouillage systématique par le biais d'une structure au
fonctionnement ultra hiérarchisée empêchent l'émergence au sein de la CGT d'une nouvelle
génération militante consciente de ce que doit être un syndicalisme de classe, force est de constater
qu'il y existe un vivier important de travailleurs et travailleuses qui sans le savoir pratiquent un
syndicalisme ASSR dans leur entreprise. Il est difficile à quantifier mais pour l'heure, ce sont bel et
bien des sections CGT qui animent les conflits les plus durs du pays à quelques et rares exceptions
près.
Venons en aux CNT. La situation n'a jamais été aussi ubuesque avec 4 organisations se
revendiquant du même sigle et issues du même tronc. La scission de 1993 s'est faite sur une
différenciation d'analyse de la phase historique dans un contexte d'irréfragabilité syndicale.
Si ce contexte est aujourd'hui désuet et ce depuis la loi de 2008, force est de constater que le replis

numérique et idéologique du syndicalisme en général et de l'ASSR en particulier nous font analyser
les élections professionnelles, et ce avec tous les défauts intrinsèques à ce qu'elles sont en tant que
délégations représentatives, comme la possibilité de maintenir une présence au sein des entreprises
et de protéger les militant-e-s syndicaux/syndicales. La volonté du gouvernement et du MEDEF
d'accroître les seuils sociaux afin d'affaiblir les implantations syndicales est une preuve flagrante de
cette réalité. Si la participation aux élections professionnelles dans les années 50 pouvait être vue
comme une compromission voire une trahison alors que la période se prêtait à une poussée des
luttes et une percée du syndicalisme, aujourd'hui, notre faiblesse nous oblige à y aller. C'est une
réalité au risque d'en décevoir certain-e-s, mais nous connaissons très peu de personnes motivées
pour se faire licencier en créant une section syndicale, alors si en plus on leur ôte toute possibilité
de protection, nous pouvons être sûr que personne ne nous rejoindra. Par ailleurs, si le nouveau
droit au RSS nous offre la possibilité de créer des points d'appuis et une visibilité au sein des boîtes,
il est difficile pour un seul camarade de tourner au sein d'une entreprise comptant un nombre
importants de salarié-e-s. Tous les syndicats de la CNT pratiquant dans les entreprises le savent.
Nous le constatons quotidiennement dans les boîtes où nous sommes implantés, notamment dans le
privé où la situation est différente du secteur public. Concernant ce dernier, le congrès de 2008 avait
par ailleurs adopté une position juste en laissant le soin aux fédérations d'industries de se
positionner quand à leurs éventuelles participations car elles sont au plus près du terrain et donc par
ce fait les plus à même de définir leurs motivations et leurs stratégies. Si le droit de grève est
assujetti à la représentativité la CNT devrait-elle s'interdire d'y participer ? Croyons nous que la
situation actuelle favoriserait une transgression de la part des travailleurs et travailleuses d'une telle
loi ? Souhaitons nous être à la remorque d'une autre organisation syndicale sans être en capacité de
porter nos propres revendications ? Le fait que certaines fédérations telle la FTE aient réaffirmé
récemment leur refus de participer aux élections n'a pas pour autant écarté des questions
essentielles, notamment concernant le lien qui existe entre représentativité et droits syndicaux. La
FTE s'est ainsi gardée la possibilité depuis son congrès de 2012 de faire évoluer cette position en
fonction de la réalité de ses luttes et de son implantation, laissant ainsi la possibilité à des
perspectives futures visant à pallier un possible échec de la stratégie actuelle.
Cette analyse crée un fossé important entre nous et l'AIT. Aussi important que celui du permanenta
syndical qui a été à l'origine de la scission consumé lors du congrès de Metz. Scission aux
conséquences autrement plus importantes car au lieu de voir un renforcement de la CNT-F, nous
assistons à son affaiblissement, jour après jour. Notre organisation est en incapacité de porter des
initiatives et des projets propres créant des dynamiques, ses équipes militantes sont fatiguées et
éprouvées, elles ont du mal à se renouveler voire ne se renouvelles pas du tout et disparaissent.
D'autres, sections ou syndicats, nous quittent pour la CNT-SO. Et encore, nous ne parlons pas des
nombreux camarades partis à Solidaires. Comme dit plus haut, nous nous refusons à mettre cet état
de fait sur le compte de la seule morosité ambiante, nous comparant aux autres organisations du
mouvement social. La lutte des classes est faite d'une dialectique historique dans laquelle les forces
révolutionnaires, leurs analyses, leurs orientations, leurs décisions et leurs actions influent ou non
sur le cours des événements en fonction de leurs justesses. Force est de constater que nous sommes
passés d'une organisation en capacité, malgré les inévitables conflits liés à toute organisation
humaine, de participer et d'initier des luttes et des projets à une organisation en difficulté sévère et
aux points d'implantations de moins en moins nombreux et nombreuses.
Par ailleurs, nous pouvons constater que la stratégie de la CNT-SO lui permet tout de même de
mener des conflits dans certains secteurs, tout relatifs qu'ils soient et avec toute la mesure que
nécessite la réalité. Nous n'en sommes aujourd'hui pas capable : c'est un implacable constat. De
même, nos outils sont en reculs, notre presse est dévastée, notre activité faible, surtout dans sa
dimension interprofessionnelle.

Pourquoi sommes nous contre la motion présenté par le SINR 44 sous la forme présentée ?
Essentiellement pour deux raisons.
En premier lieu, parce que parler de rapprochement avec la seule CNT AIT nous semble être une
erreur stratégique majeure. Si nous voulons créer une nouvelle dynamique et un nouveau cycle de
développement pour la CNT-F, nous permettant de ravoir de l'ambition sur le terrain de la lutte de
classe, nous pensons qu'il est important d'ouvrir cette perspective à l'ensemble des 4 CNT,
conscients des difficultés évidentes que cette démarche engendrerait au vu des disparités entre une
CNT AIT qui a basculé sur des bases anti syndicales totales, une CNT AIT restant sur un
positionnement anachronique au vu des réalités sociales d'aujourd'hui, d'une CNT-SO dont le
compromis sur le permanenta crée un véritable clivage avec notre histoire et notre CNT-F qui peine
à se remettre d'un conflit qui l'a miné pendant plus de dix ans.
En second lieu, nous pensons que tout rapprochement doit se faire sur du fond, or la motion du
SINR 44 ne pose que des affirmations approximatives faites de « il semblerait que » sans tracer
aucune perspectives stratégiques de développement. Il nous cite en exemple les cortèges communs
fait à Paris et au Mans. Si nous ne pouvons que nous réjouir de ces démarches unitaires, il ne faut
pas non plus les mythifier. Les CNT AIT ne pèsent absolument rien dans le paysage syndical, leur
stratégie les mettant d'emblée en échec en ce qui concerne l'implantation syndicale. Les évolutions
récentes du secteur fédéraliste ne pourront que les amener vers le positionnement que nous adoptons
actuellement. De plus, la motion du SINR 44 ne va pas dans le sens de notre analyse sur la réalité de
l'éclatement de l'ASSR et du fait que de nombreux et nombreuses militant-e-s syndicalistes se
réclamant de ces courants se retrouvent ailleurs qu'au sein d'une des CNT.
Enfin, nous précisons que cet argumentaire ne saurait être exhaustif tant la situation est pour l'heure
complexe. Nous invitons l'ensemble des syndicats de la CNT-F à se saisir de cette question afin
d'avoir un débat riche sur ce sujet essentiel pour notre avenir.
Le sens de notre contre motion
Considérant que la réalité de la situation de mouvement social, comme du courant ASSR, est
aujourd'hui catastrophique, il nous semble qu'il est nécessaire d'éviter tout repli idéologique en
n'invitant à discuter que la seule CNT AIT, et encore une partie seulement. A contrario nous sommes
favorables à l'ouverture d'échanges avec l'ensemble des CNT sur des axes de réflexions touchant
aux stratégies d'implantations et de développement syndical.
Nous pensons que lors des deux prochaines années, il est nécessaire d'organiser ce que nous
pourrions appeler des États Généraux de l'ASSR. Réunissant les syndicats des 4 CNT dans le cadre
de conférences nationales publiques visant à créer une dynamiques. Celles-ci auraient pour objectif
de faire un état des lieux sur les points de clivages comme sur les points de rapprochement au sein
de notre courant, et cela afin de chercher quelles perspectives communes pourraient conduire à
l'unité des anarcho syndicalistes et des syndicalistes révolutionnaires.
Soyons clairs, il est évident que la tenue d'un congrès de réunification de la CNT se pose à l'horizon
de cette démarche.
