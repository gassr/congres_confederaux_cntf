
.. _contre_motions_22_23_24_2014_sipmcs:

=========================================================
**Contre motions aux motions 22,23 et 24 2014** SIPMCS
=========================================================

.. seealso::

   - :ref:`motion_24_2014_ste75`

Préambule
=========

Argumentaire spécifique aux contre-motions proposées ci-dessous.

Aujourd'hui le **secrétaire confédéralLE chargéE des relations avec les médias**,
en lien avec le :term:`BC`, rédige des communiqués et envoie ces communiqués
ainsi que ceux écrits directement par les syndicats à la presse.

Malgré ces importants efforts, les résultats dans la presse nous semblent
peu probants. Nous constatons donc que des textes intéressants sont
écrits au nom de la CNT et n'ont que peu d'effet.

Ci-dessous, nous avons rédigé plusieurs motions, à voter séparément, permettant
de décider ou non de différentes évolutions pour le journal et le site internet.
Ces évolutions découlent pour la plupart des argumentaires des motions.

- :ref:`22 <motion_22_2014_etpic30>`,
- :ref:`23 <motion_23_2014_etpic30>`,
- et :ref:`24 <motion_24_2014_ste75>`.


Contre motions
===============

.. toctree::
   :maxdepth: 3

   contre_motion_22_2014_sipmcs
   contre_motion_23_2014_sipmcs
   contre_motion_24_2014_sipmcs
   motion_A_2014_sipmcs
   motion_B_2014_sipmcs
   motion_C_2014_sipmcs
   motion_D_2014_sipmcs
