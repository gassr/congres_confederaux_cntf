

.. _contre_motion_9_2012_ess34:

================================================================================================
Contre-motion à la motion N°9 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_9_2012_interpro31`
   - :ref:`motion_42_2012_etpic30`



Introduction
============

Attention, cette contre-motion ne peut-être adopté définitivement qu'après
l'adoption de la :ref:`motion n°42 (NDC) <motion_42_2012_etpic30>`.

Contre-motion
==============

Le congrès des syndicats de la CNT suspend intégralement (cf. :ref:`motion 42 <motion_42_2012_etpic30>`)
l'Union régionale Région parisienne (URRP).

La levée de cette suspension sera examinée en congrès.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°9      |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
