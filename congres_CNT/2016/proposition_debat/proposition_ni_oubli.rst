
.. _proposition_debat_inceste_2016:

====================================================================================================
Proposition de débat / concert autour du livre "Ni silence, ni pardon L’inceste : un viol institué"
====================================================================================================




Courriel Secrétaire Confédéral
===============================

::

    Sujet :     [Liste-syndicats] Proposition de débat / concert autour du livre "Ni silence, ni pardon L’inceste : un viol institué"
    Date :  Sun, 9 Nov 2014 16:49:30 +0100
    De :    CNT Secrétariat Confédéral <cnt@cnt-f.org>
    Pour :  liste-syndicats <liste-syndicats@bc.cnt-fr.org>


Bonjour camarades,

Le Secrétariat Confédéral a reçu la proposition de débat / concert ci-dessous
d'une des deux auteurs d'un ouvrage initulé :
"Ni silence, ni pardon L’inceste : un viol institué", par Jeanne Cordelier et
Mélusine Vertelune paru en Février 2014.

Les auteurs sont manifestement partie prenante du Collectif Libertaire
Anti-Sexiste basé à Lyon et, pour l'une d'entre elle au moins, du groupe
Vizcacha Rebelde.

Elles/ils proposent débat autourt du livre et concert à tous les syndicats
et Unions de la CNT.

Voir donc directement avec eux/elles pour les accueillir chez vous !

Salutations AS & SR
Manu - Secrétaire Confédéral CNT cnt@cnt-f.org


Courriel originel
==================

::

    From: ciredutemps
    Date: 2014-10-26 18:28
    To: cnt
    Subject: [Fwd: Ni silence ni pardon]

Bonjour,

S'il y a des personnes intéressées, je serai ravie de venir présenter le
livre que j'ai co-écrit avec Jeanne Cordelier dans le cadre d'une ou
plusieurs réunion(s) publique(s)

Toutes les infos à propos du bouquin sont là :

http://clas.olympe.in/spip.php?article9

Actuellement en France, un-e enfant sur 24 est victime d'inceste. Sur 10
victimes d'inceste, 7 sont des petites filles régulièrement agressées par
des hommes.

Beaucoup ne survivent pas.

Et parmi celleux qui survivent une
écrasante majorité sont confronté-e-s toute leur vie à de multiples formes
d'oppressions dont l'impacte est décuplé par le traumatisme des viols
incestueux et d'un **système familial totalitaire et déshumanisant**.

Parce qu'elles n'ont pas pu se reconstruire, parce qu'elles n'ont pas
trouver la force qui réside dans la révolte et la prise de conscience
politique, beaucoup de ces personnes deviennent des adultes soumises et
vulnérables, précarisées et malléables, cibles de toutes les formes
harcèlements,...

Il est vital et urgent que les professionnel-le-s de la santé, de
l'éducation et du social s'interrogent et se positionnent pour agir
collectivement en tant que professionnel-le-s, en tant que salarié-e-s, en
tant que syndicalistes et en tant qu'humain-e-s. Il est temps de cesser
d'ignorer ces enfants et la dimension politique du sort qui leur est
réservé par leurs bourreaux avec la complicité de l’État et de ses
institutions.

Pour info, voici deux poèmes à propos du même sujet (repris sous forme de
chansons pour le prochain album de Vizcacha Rebelde dont je fais partie):

http://sisyphe.org/spip.php?auteur1691

En ce qui concerne Vizcacha Rebelde, ça nous plairait de jouer à nouveau
en soutien à la CNT (nous l'avons déjà fait, à Grenoble lorsqu'on
s'appelait encore Damn'Dynamite et à Lyon l'année dernière).

Le site de Vizcacha Rebelde :

http://vizcacha-rebelde.olympe.in/

Salutations anarkaféministes,
Mélusine Vertelune
06 75 96 90 41
