
.. _amendement_motion_9_2021_interpro31:

===============================================================================================================================================
Amendement  N°3 à la motion n°9 2021 **Lutte contre la Précarité** par **Interpro31**
===============================================================================================================================================

- :ref:`motion_9_2021_interco69`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`

Argumentaire 
================

C’est par soucis d’exactitude que nous proposons ce léger amendement.

Premièrement, le salariat déguisé ne se limite pas aux entreprises de
plateforme numérique et il existe depuis le début du capitalisme
industriel : marchandage au XIXe, entreprises qui emploient des
auto-entrepreneurs dans le bâtiment …

Deuxièmement, nous ne trouvons pas assez précis le terme d’``économie numérique``.
Nous préférons parler d’entreprise de plateforme numérique (Amazon, Blablacar,
Uber, Deliveroo, Ornika, Facebook, Airbnb, ...).

Amendement 
============

Se mobiliser dans la lutte contre la précarité, l’intérim en particulier
dans le secteur public, et l’expansion d'un salariat déguisé initié par
l'économie numérique accentué par les entreprises capitalistes de plateforme
et la libéralisation dans tous les secteurs (privé/public). »

rajouter plateforme numérique ? Plus clair rajouter le «développement
des entreprise de plateforme numérique»



Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion      |            |          |            |                           |
| n°9 Interpro31              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
