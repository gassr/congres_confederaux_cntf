
.. index::
   pair: Motion 25 ; 2014

.. _motion_25_2014_etpic30:

=======================================================================================================
Motion n°25 2014 : Commissions de travail confédérales – Rôle, limites, et compétences (ETPIC 30 sud)
=======================================================================================================

.. seealso::

   - :ref:`motions_etpic30`
   - :ref:`releve_decisions_xxxiii_congres`





Motion
=======

Cette motion vise à définir le rôle et les compétences des
commissions confédérales existantes à caractère statutaires :
Commission administrative, commission de contrôle, etc.

Dans le but de réaliser un travail allant dans l’intérêt général,
la CNT peut recourir à la constitution de commissions de travail
confédérales.

Ces commissions de travail sont constituées pour l’accomplissement
d’un travail défini par le Congrès, parfois limité dans le temps.
En cas de besoin, leur renouvellement est assuré à chaque
échéance de Congrès confédéral.

Pour qu’une commission puisse se tenir, elle doit être animée par
deux syndicats minimum ou un syndicat et unE mandatéE confédéral
minimum.

Elles sont composées exclusivement de membres de la CNT issus de
syndicats connus.

Une commission peut être expressément placée sous la coordination
du Bureau Confédéral.

Des mandatés confédéraux peuvent faire partie de ces commissions à
titre d’apport technique ou en rapport avec les prérogatives inhérentes
à leurs mandats.

Des contributions externes à la CNT peuvent être provisoirement
envisagées au titre d’apports techniques, juridiques, ou d’expertises,
mais jamais à titre de membres permanents.

Ces commissions sont placées sous le contrôle du Congrès ou du CCN de
la CNT.

Leurs besoins financiers sont couverts proportionnellement à la tache
à accomplir, en lien avec le Bureau Confédéral, plus directement,
la Trésorerie Confédérale.

Sous le conseil et le contrôle permanent du BC et de la CA CNT, les
membres des syndicats mandatéEs constituant les commissions de travail
peuvent aussi être amené à représenter la CNT dans le cadre strictement
défini aux orientations par le Congrès/CCN.

Enfin, les commissions de travail confédérales ne revêtent pas
directement un caractère décisionnel.
Leur autonomie est donc relative.
Pour engager la CNT, leurs productions, leurs propositions, et
leurs préconisations doivent être soumises à l’approbation des
instances confédérales compétentes : BC/CA, CCN, Congrès.

Il est rigueur que le ou les syndicats soutenant la création d’une
commission de travail y associent leurs candidatures.

Mémoire et évolution des décisions confédérales (ETPIC 30 sud)
===============================================================

Argumentaire commun aux motions 26, 27 et 28 2014
--------------------------------------------------

Rappel de la motion :ref:`adoptée au Congrès de 2001 <motion_recueil_2001>`::

    Création d’un recueil des motions de Congrès en vigueur depuis 1993
    Le Congrès confédéral charge une commission d'établir un recueil des
    motions en vigueur.

    Ce recueil devra organiser un classement par catégorie et date des
    motions et il pourra éventuellement y être annexé des documents
    consultatifs en vue d'obtenir un ouvrage clair et unique.

    A l'issue de chaque Congrès, ce recueil devra être mis à jour par le
    bureau confédéral ou par voie de délégation sous sa responsabilité.

    Chaque syndicat pourra sur demande disposer d'un exemplaire laisse à la
    libre consultation de ses adhérents.

Rappel de la :ref:`motion adoptée au Congrès de 2008 <recueil_resolutions_2008>`
à 76% des syndicats  présents au Congrès.

Nous estimons que la motion de 2008 n’établit pas clairement la
 distinction entre:

- Un ouvrage (recueil) qui vise à recenser et conserver l’ensemble des
  motions adoptées par les Congrès successifs de la CNT (mémoire / trace / inventaire) ;
- Et un ouvrage de synthèse « Fonctionnement & Orientation » dont la
  rédaction s’appuie sur les résolutions les plus récentes

Par ailleurs, le travail demandé pour l’élaboration de l’ouvrage de
synthèse « CNT - Fonctionnement & Orientations » s’avère conséquent.

L’effort de synthèse ne peut être placé sous la seule responsabilité
des seuls mandatés du Bureau Confédéral au regard de l’ampleur
du travail ou de la collégialité de réflexion que cela nécessite.

Nous proposons donc, que ce travail puisse être réalisé par une
commission de travail confédérale associant plusieurs syndicats,
commission placée sous la coordination du Bureau Confédéral.

Un travail plus collectif garantira une meilleure collégialité dans la
mesure où l’épreuve de synthèse nécessite une manipulation du texte
d’origine : les motions de Congrès adoptées et en vigueur.

Enfin, même si ces éléments peuvent paraître évidents, il n’est fait
nulle part mention (motions ou dispositions statutaires) du fait que
les résolutions adoptées par le Congrès les plus récentes invalident
les plus anciennes.

Nous proposons donc d’y remédier par une motion spécifique parmi 3
motions (à voter séparément) :


Contre motions
==============

.. seealso::

   - :ref:`contre_motion_25_2014_interpro31`

Amendement
==============

.. seealso::

  - :ref:`amendement_motion_25_2014_interco25`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°25 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
