
.. index::
   pair: Motions ; 2016

.. _congres_CNT_2016_motions:

========================
Motions Congrès CNT 2016
========================

.. toctree::
   :maxdepth: 3

   2__strategie_syndicale/strategie_syndicale
   3__fonctionnement_organique/fonctionnement_organique
   4__annexe/annexe
