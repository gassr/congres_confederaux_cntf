

.. _contexte_ssct_2017_03_05:

========================================================================
III) Sur le contexte de l'accusation dont a été saisi notre syndicat
========================================================================




1. Indépendance de notre syndicat vis à vis de la procédure d'AL
===================================================================


Pour de nombreuses raisons, notre syndicat a tenu à ce que sa procédure
dans le cadre de l'accusation de viol contre l'un de ses adhérents soit
**celle qu'il a lui-même choisie, et ne lui soit pas dictée par une
organisation politique extérieure**, en l'occurrence Alternative Libertaire.

Aussi, bien que nous ayons subis de nombreuses injonctions sur la façon
dont nous devions procéder, notamment en interne de la CNT, nous avons
assumé et mener une procédure différente de celle d'AL.

Pour autant, comme nous l'avons expliqué dans notre première partie
méthodologique, notre seule source d'information pour obtenir des éléments
à charge contre Fouad, a été AL, via le rapport de sa commission non mixte,
constituée pour instruire le dossier et recueillir des témoignages.

Les éléments à charge que nous avons finalement analysé au sein de notre
AG **l'ont donc été suite à un premier filtrage, puisque le rapport de la
commission non mixte d'AL est bien un rapport, et non un compte rendu ni
un recueil de témoignages que nous n'avons pu obtenir en raison du refus
d'AL**.

Question s'est donc posée à notre syndicat de savoir si nous pouvions
faire confiance en le travail de la commission non mixte d'AL.

Nous nous sommes donc penché·es sur les statuts d'AL qui définissent
cette procédure en leur sein, ainsi que sur le document de 4 pages
(transmis au Bureau Confédéral), interne à AL, et qui notifie à Fouad
le début d'une procédure, tout en en décrivant les formes et les règles.

Au regard de cette analyse, nous avons pu constater, avant même de
disposer du rapport de la commission non mixte que celui-ci ne pourrait
qu'être un document à charge.

En effet, la procédure même d'Al en cas d'accusation d'agression sexuelle :

Partant du principe que la parole de la victime ne peut en aucun cas être
remise en cause dès lors qu'elle formule une accusation d'agression sexuelle.

Ne laisse aucune place à la défense de l'accusé, qui n'a pas le droit
d'être assisté lorsqu'il est auditionné, qui est auditionné sans qu'on
lui présente les faits qu'on lui reproche, dont les éléments de défense
sont considérés irrecevables par la procédure dès lors qu'ils remettent
en question la parole de la victime supposée ou son intégrité.

Établit dès la qualification des faits que l'accusé est coupable remettant
ainsi toute notion de **présomption d'innocence**, tel qu'en atteste le
document de 4 pages qui définit cette procédure et présente les faits en
ne laissant aucune place au doute et sans le moindre conditionnel.

Fait la liste de toutes les possibilités de défense existant pour un
accusé pour expliquer, a priori du propos de la défense, que celle-ci
démontreront quoi qu'il en soit sa culpabilité présumée.

Aussi, nous n'avons pas été surpris·e de la teneur du rapport de la
commission non mixte, totalement orienté à charge, lorsque nous l'avons
reçu.
Nous tenons ici à détailler plus amplement les éléments nous permettant
de considérer que ce rapport est biaisé et orienté.

L'utilisation des témoignages par la commission non mixte dans son rapport
---------------------------------------------------------------------------

Il ne nous semble pas nécessaire de nous étendre outre mesure sur les
déclarations faites par Y., A. et R. quant aux pressions, et les injonctions
qu'ils·elles ont subi lors de l'écriture de leurstémoignages.

Nous l'avons fait précédemment.

Mais cet état de fait nous a conduit·es naturellement à remettre en
question la crédibilité des témoignages partiellement cités, et encore
une fois anonymes, présents dans le rapport de la commission non mixte.

Que des témoignages à charge
------------------------------

Au-delà de la contrainte présumée sous laquelle ceux-ci ont été transmis
à la commission non mixte d'AL, il est éloquent de constater que tous
les témoignages cités dans ce rapport sont des témoignages à charge
contre l'accusé.

En effet, nous avons pu le vérifier grâce aux déclarations de Y., A. et R.,
en ce qui concerne leurs témoignages : les éléments à décharge de leurs
déclarations, qui ont pourtant bien été transmis à la commission non mixte
d'AL, ne figurent pas dans le rapport de la commission.

Qu'en est-il des autres témoins ? (Dont nous n'avons d'ailleurs aucun
moyen de vérifier l'existence.)

Des témoignages sans doute bidonnés
--------------------------------------

Mais l'attitude de la commission non mixte concernant les 3 témoins identifié·es
nous laissent penser que les autres témoignages ont eux aussi été tronqués,
voir modifié, de façon à présenter comme à charge pour l'accusé.

La question de la défense dans le rapport de la commission
-------------------------------------------------------------

Comme en atteste de façon criante le compte rendu fait par l'une de nos
adhérente de l'audition de Fouad par la commission non mixte d'AL, et
comme le prévoyait d'ailleurs les statuts d'AL en cas de procédure de viol,
**la défense de Fouad n'a pas été prise en compte dans cette procédure**.

Tout d'abord, lors de son audition - à laquelle il a pu se rendre
accompagné mais lors de laquelle son accompagnatrice ne pouvait pas
s'exprimer - aucun fait ne lui ont été présentés.

Dès lors nous ne voyons pas comment l'accusé aurait pu bénéficier de la
moindre défense, ni même la sienne.

Comment se défendre de quelque chose si l'on ne sait pas ce que l'on
nous reproche ?

Dans l'hypothèse ou Fouad n'aurait pas commis ce viol, il ne pourrait
absolument pas démontrer quelques innocences que ce soit, ni même jouir,
a minima, de sa propre parole.

**Nous sommes ici très loin de la notion de justice équitable que nous
décrivions en introduction**.

Ensuite, les éléments de défense qu'il a pu formuler n'ont pas été rapportés
dans le rapport de la commission, tel que nous pouvons le constater de
façon évidente à sa lecture.

C'est par exemple le cas concernant la défense formulée par Fouad lui-même
lorsqu'il a expliqué à la commission non mixte qu'il était lui-même désireux
que E. dépose plainte à son encontre.

D'ailleurs, à aucun moment de sa procédure AL n'a semblé chercher à
confronter certaines affirmations de Fouad aux déclarations des témoins
afin de chercher à savoir si celles-ci étaient crédibles ou non.

Finalement, l'ensemble des éléments de défenses qui ont eu la chance
d'être rapportés dans le rapport de la commission non mixte, l'ont été
afin de démontrer que ceux-ci était finalement des preuves de la
culpabilité de l'accusé selon un schéma pré établi.
Parce que l'accusé entre dans le cadre de ce schéma alors il serait de
fait coupable du viol dont on l'accuse.

**Encore une fois ce raisonnement irrationnelle, ou raisonnement par
l'absurde ne correspond en rien avec la vision équitable de la justice
que nous défendons**.

Ce raisonnement, qui dicte le rapport de la commission non mixte d'AL ne
laisse ainsi à l'accusé aucun moyen de défense.

Si bien, que même s'il était innocent dans la réalité des faits, il ne
disposerait d'aucun outil lui permettant de la démontrer.

**Dès lors nous ne comprenons pas bien pourquoi il fallait faire toute
cette longue procédure de fond, la définition même de cette procédure
condamnant d'ores et déjà l'accusé, quoi qu'il arrive**.

La considération de la moralité de la victime supposée et de l'accusé
------------------------------------------------------------------------

Afin de se défendre, Fouad a transmis à la commission non mixte d'AL de
nombreuses attestations de moralité, toutes rédigées par des femmes qui
soit ont eu des relations sexuelles avec Fouad, sont l'ont éconduit
lorsque celui-ci avait pu les aborder.

Celles-ci sont aussi considérées par le rapport de la commission non mixte
comme non recevable, sous prétexte qu'elles ne permettent pas d'établir de
vérité sur les faits.

Nous sommes d'ailleurs en accord avec ce principe : le sentiment humain
que peut dégager un personne n'atteste en rien ou non des actes qu'il
commet, comme le dit AL dans son rapport, "ce n'est pas parce qu'un homme
n'a jamais agressé une femme, qu'il ne peut pas le faire".

Néanmoins, lorsque des éléments de la personnalité de l'accusé qui ont é
té décrits par des témoins anonymes ont permis de dresser un portrait
moral à charge pour Fouad, alors ceux-ci ont bénéficié d'un crédit sans
faille et ont été, eux, retranscrits dans le rapport de la commission.

De la même manière, Fouad a apporté pour sa défense, lors de son audition,
des éléments sur la personnalité de la victime.

Éléments d'ailleurs corroborés par les témoignages qu'ont reçu la commission
non mixte. Éléments qui peuvent être potentiellement considérés comme à
décharge car **remettant en cause le crédit de la victime supposée**.

Ces élément ont encore une fois soit été dénigrés dans le rapport de la
commission non mixte en expliquant qui si la défense les mobilisait
alors c'était de fait une preuve de culpabilité, soit retournés à charge
contre l'accusé, **comme c'est notamment le cas en ce qui concerne la
consommation de drogues** tel que nous décrivions plus haut.

En résumé, nous n'avons pu que constater que le rapport de la commission
non mixte d'Alternative Libertaire constitué uniquement un rapport à charge,
qui ne laisse aucune place ni à la parole de la défense, ni aux éléments
à décharge pour l'accusé.

Pour exemple, nous pouvons citer ce courrier de Fouad, adressé à AL
après la publication du rapport de la commission pour s'étonner que
ses éléments de défense ne soient pas cités, courrier qui n'a pas été
transmis aux adhérents d'AL avant leur vote :

Fouad : "A la question posée par Clémence sur le pourquoi demander des
attestations de moralité à des camarades et amies femmes, ma réponse a
été sans équivoque. Pris dans une accusation basée uniquement sur la
parole de la plaignante contre la mienne, et au vue du point précédemment
cité, il ne me restait que ce moyen pour me défendre afin de donner
consistance à ma parole qui depuis le début de cette affaire est
systématiquement soit occultée, soit dénigrée.
Pour preuve, ce point n'apparaît pas non plus dans le rapport. Pourquoi ?"

Ainsi, lorsque que les adhérent·es d'AL ont voté fin janvier sur l'exclusion
de l'accusé **ceux·celles-ci ne disposaient en aucune façon ni du propos
de la défense, ni des éléments à décharge, et n'ont donc pris·es une
décision que sur le seul aspect à charge de cette affaire**, sans avoir
jamais connaissance d'éléments pourtant connus de la commission non mixte
et qui semble déterminants pour comprendre la réalité des faits.

Le rapport à charge étant, de par sa définition même a priori, et de
par la présentation orienté qui est faite de l'affaire, un rapport accablant
pour l'accusé qui a, sans la moindre surprise pour notre syndicat, conduit
**à un vote Stalinien en faveur de son exclusion**.

En conclusion, notre syndicat a également été particulièrement surpris
par l'attitude d'AL qui a consisté à procéder à des injonctions publiques
à l'égard de la CNT quant à l'exclusion de son adhérent.

En effet, la CNT, via sont BC, avait bien attesté à AL que notre syndicat
était saisit de cette affaire, a d'ailleurs fait office d'interface entre
notre syndicat et AL sur le plan national pour que nous puissions disposer
d'éléments concrets d'accusation.

Nous ne comprenons donc pas pourquoi AL, qui sait pertinemment où en est
la CNT sur cette affaire a fait le choix, malgré les relations cordiales
qu'ils·elles entrainaient avec le BC, de mettre sous pression la CNT sous
les yeux du grand public.

Aussi nous ne pouvons que considérer que cette démarche comme une volonté
politique d'Alternative Libertaire de déstabiliser la CNT.

Les raisons d'AL lui sont propres, nous ne les connaissons évidemment pas,
bien que nous puissions considérer certaines hypothèses, mais quoi qu'il
en soit, l'attitude politique d'AL vis à vis de la CNT a conduit notre
syndicat à ne pas considérer les conclusions de cette organisation comme
argent comptant.

Dernière raison pour laquelle nous avons préféré mener notre propre
procédure, au regard des principes militants décrits dans notre partie
méthodologie, procédure qui nous a conduit en remettre en cause le crédit
de l'enquête à charge menée par AL.

2) Chronologie de l'accusation
=================================

Notre syndicat, depuis le début de cette affaire et la formulation des
accusations lors du Congrès Confédéral de la CNT (le dimanche 13 novembre 2016)
par des membres de la commission antisexiste de la CNT, a essayé de
comprendre pourquoi cette accusation nous était parvenue par ce biais.

**Après plusieurs mois d'investigation**, nous avons pu faire la lumière
sur les processus qui ont conduit les protagonistes de cette accusation
à ne pas employer les voix structurelles qu’il y a chez AL, comme
à la CNT, nous semblait être les plus appropriées pour traiter sereinement
l'accusation de l'un de nos adhérents.

Nous ferons donc ici le résumé chronologique des démarches qui ont été
entreprises par les un·es et les autres car il nous semble qu'elles
participent à éclaircir notre analyse.

Tout d'abord, la première formulation de l'accusation par la victime
supposée elle-même, est intervenue sur le trajet retour du Roucous.

C'est sur une aire d'autoroute que E. a fait le premier récit des faits
présumés à Y. **Tous les témoignages factuels concordent pour dire que
cette confidence faite à Y. est intervenue quelques temps après une
altercation violente qu'elle a eue avec Fouad.**

Suite à cette première annonce, les membres du CAL Moselle ont proposé
à E., adhérente du CAL de Meurthe et Moselle, de traiter localement
cette problématique.

Réticente à cette idée dans un premier temps, en raison de la peur
qu'elle a évoqué d'être victime de représailles, E. s'est alors
confiée à des militant·es de la CNT à Nancy.

on trouve J., adhérent de l'interco 54, et qui  entretient une relation amicale avec E
------------------------------------------------------------------------------------------

**Parmi ceux·celles-ci l'on trouve J., adhérent de l'interco 54, et qui
entretient une relation amicale avec E.**

Selon les déclarations de E. à Y., c'est bien J., qui l'a convaincue
que, plutôt que de traiter cela au sein du CAL Moselle d'AL, il valait
mieux solliciter la CNT, sur le plan national, et cela via la commission
antisexiste confédérale.

**A ce stade nous nous interrogeons naturellement sur le pourquoi une telle
démarche de J. qui a préféré adresser la victime présumée à une commission
de la CNT plutôt qu'à Alternative Libertaire, qui dispose également d'une
commission antipatriarcat, et alors que l'affaire porte sur deux
adhérent·es d'AL et le camping d'AL.**

Nous nous interrogeons également sur le pourquoi, si tant est que J. se
soit trompé dans son analyse, cette information dont disposait visiblement
plusieurs adhérent·es de la CNT a mis 4 mois pour parvenir jusqu'à la
commission antisexiste de la CNT.

À moins que celle-ci n'ait disposé de cette information bien plus tôt,
auquel cas **notre interrogation porte sur le pourquoi la commission aurait
attendu 4 mois avant de la porter à la connaissance d'une instance confédérale**,
en l'occurrence le Congrès, lors d’une démarche orale qui a fait sensation
et a conduit à la divulgation immédiate de l'identité de E..

Le rôle de J. nous interroge
-------------------------------

Le rôle de J. nous interroge, car il ne semble pas du tout cohérent avec
une posture de protection de la victime potentielle et de résolution
de sa problématique.

Aussi, notre syndicat n'a pas pu faire autrement que de mettre cette
démarche en perspective avec le vécu relativement récent de J., qui il
y a quelques années s'est retrouvé accusé de violences conjugales par
son ex-compagne, M., elle aussi adhérente de l'Interco 54.

Dans ce contexte E., amie du couple, avait dans un premier temps défendu
la victime supposée, avant de se rétracter pour prendre la défense de J.,
considérant sans doute que la parole de M. n'était pas crédible.

La décision de l'interco 54, saisi de cette accusationd'une adhérente à
l'encontre d'un adhérent avait alors été de considérer que la parole
de M. était mensongère et avait donc, faute d'éléments probants, décidé
de blanchir J., sur la base d'une réflexion radicalement opposée à celle
que ces mêmes camarades défendent aujourd'hui au sujet de l'accusation
faite par E. à l'encontre de Fouad.

Nous rassurons J., le but de ce paragraphe n'est pas de jeter un doute
sur son innocence (dont nous sommes parfaitement convaincu·es) dans le
cadre de cette affaire de violences conjugales présumées, mais bien
de comprendre ce qui l'a conduit à adopter aujourd'hui une démarche si
contradictoire avec ce que lui-même et son syndicat ont vécu il y a
quelques temps.

Contradictoire parce que l'a parole de la victime présumée n'avait pas
fait force de preuve, contradictoire parce que le syndicat avait préféré
gérer cette accusation en son sein plutôt que d'adresser M. à une
commission antisexiste qui aurait peut-être été plus à même de donner
de la considération a la femme potentiellement agressée.

**Aussi, notre syndicat n'a pu que s'interroger sur le fait que le rôle de J.
puisse être une contrepartie offerte à E. pour le soutien qu'elle lui a
apporté il y a quelques années lorsqu'il a été accusé.**

Notre syndicat s'interroge aussi sur le rôle joué par le syndicat interco 54
dans le fait d'adresser E. à la commission antisexiste de la CNT, plutôt
qu'aux instances d'AL en premier lieu.

En effet, il est de notoriété public au sein de la CNT en Lorraine, que
l'un des adhérents actif de l'interco 54 entretient une relation intime
avec l'une des membres de la commission antisexiste, et du syndicat SSCT RP,
ayant participé à la rencontre avec E. le 10 novembre 2016 et à la
communication de l'accusation au Congrès.

Encore une fois, nous nous interrogeons sur le pourquoi, alors que tout
ce petit monde était au courant depuis bien longtemps de l'accusation
de E. à l'égard de Fouad, ils·elles n'aient rien dit jusqu'à la tenue
du Congrès Confédéral et le coup d'éclat intervenu le dimanche 13 novembre 2016.

**L'une des hypothèses étudiées par notre syndicat nous laisse penser que
cette adhérente de SSCT RP ait pu utiliser ses relations au sein de
l'interco 54, et donc la connaissance qu'elle a eu de l'accusation de
E. envers Fouad, pour régler, au niveau Confédéral, des comptes avec
son syndicat d'appartenance et son Union Régionale**.

Pour quelles raisons aurait-elle des comptes à régler ?
===========================================================

Il n'appartient pas à notre syndicat de s'immiscer dans les relations
internes à l'URP, mais nous savons que les motifs de rancœur de cette
personne sont nombreux et remontent à plusieurs mois, bien avant le
Congrès Confédéral de novembre, bien avant le contexte d'accusation actuel.

Reprenons donc la chronologie de l'accusation...
---------------------------------------------------

Suite à ces démarches conjointes de J. et consorts, la commission antisexiste
de la CNT a organisé une rencontre avec E., le 10 novembre 2016, lors de
laquelle elle s'est confiée et a formulé son accusation, qui a été relayée
oralement par des membres de la commission devant le Congrès Confédéral,
lors du dernier jour, le dimanche 13 novembre 2016.

Notre AG de syndicat s'est penchée à ce stade sur un autre élément troublant
de la chronologie de ce Congrès.

En effet, nous rappelons que lors de ce Congrès de la CNT, il n'y avait
aucun·e candidat·e pour le mandat de secrétaire confédéral.

Le samedi, lorsque les syndicats sont passés sur le mandatement du SC,
le Congrès a décidé de repousser ce mandatement au lendemain, en espérant
qu'un·e candidat·e se déclare.

Le dimanche matin, toujours pas de candidat·e.

Au moment du mandatement, notre adhérent, Aurélien, secrétaire confédéral
sortant, s'est donc proposé pour assurer un intérim jusqu'au CCN de mai.

Étrangement, celui-ci a été mandaté avec la quasi-unanimité des suffrages
et aucune voix contre, sans qu'aucun syndicat ne prenne la parole sur sa
proposition de prolongation de son mandat.

Pourtant Aurélien et Fouad sont adhérents du même syndicat, et cet état
de fait a servi d'appui à plusieurs syndicats, ainsi qu'à la commission
antisexiste pour attaquer le SC en l'accusant de partialité et de
complaisance à l'égard de l'accusé (bien que cela n'a jamais été le cas
par ailleurs).

Nous nous interrogeons donc ici sur le pourquoi, ces syndicats si prompts
à accuser le SC de conflit d'intérêt au lendemain du Congrès se soient
abstenus de relayer l'accusation de E. ; à l'encontre de Fouad avant le
mandatement du SC.

Ces syndicats, qui disposaient donc des éléments d'accusation depuis
le jeudi 10 novembre 2016, ont donc visiblement sciemment attendu
qu'Aurélien soit mandaté pour (et ne se sont d'ailleurs pas opposés à son
mandatement dans leur vote) avant de l'accuser d'un conflit d'intérêt
dans lequel ils l'ont eux-mêmes placés.

Pendant le Congrès toujours, alors même que tous les syndicats qui s’étaient
fait le relai de l'accusation pendant la séance plénière avaient mis en
garde la Confédération et notre syndicat sur la nécessité de protéger
la victime supposée et de préserver son anonymat, certaines de leurs
délégations avaient déjà énoncé l'identité de la victime à une ex adhérente
de la CNT en Région Parisienne.
**Ces individus qui tenaient donc clairement double discours** durant
le weekend du Congrès, ignoraient sans doute que la camarade à qui ils·elles
ont livré l'identité de E. était par ailleurs une amie intime de Fouad,
qui a donc communiqué dans les jours qui ont suivi l'identité de
E. à son agresseur potentiel.

Continuons la chronologie de l'accusation dont la CNT a désormais connaissance.
Comme nous l'avons à plusieurs reprises expliqué depuis le Congrès de
novembre 2016 notre syndicat a dès lors tout fait pour lancer une procédure
concernant l'adhésion de Fouad.

Pour cela, nous avons dès le lendemain du Congrès cherché à recueillir
les éléments à charge contre notre adhérent.

**Comme vous le savez aussi nous avons eu toutes les peines à y parvenir.**

**La commission antisexiste de la CNT a d'abord refusé de les transmettre
par écrit, nous a ensuite fait nous déplacer à Paris le 17 décembre 2016
pour une rencontre lors de laquelle elle a à nouveau refusé de nous
transmettre les éléments d'accusation.**

En parallèle, le BC a pris contact avec AL, qui lui a expliqué qu'il ne
pouvait rien transmettre comme éléments avant que ne soit rendue leur
décision.
Si bien que ces éléments concrets d'accusation ne nous sont parvenus
qu'à partir du moment où AL les a transmis à l'accusé, qui nous les a
lui-même adressés.

Refus d'AL de transmettre toutes les informations
-------------------------------------------------

Après avoir constaté que les témoignages à charge dans le rapport de la
commission non mixte d'AL étaient tronqués et partiellement cités, nous
avons demandé à AL de nous transmettre la version complète de ces derniers.
Nous avons alors essuyé un nouveau refus, AL :
"Parce que certains témoins nous ont clairement signifié qu’ils ne souhaitaient
pas une telle transmission, notamment en raison des pressions que Fouad
a pu exercer sur elles et eux"

**Nous nous demandons néanmoins comment Fouad aurait-il pu exercer des
pressions puisque ces témoins sont justement anonymes.**

Au regard de cette chronologie, nous interrogeons sérieusement sur la
volonté qui a été celle des différent·es protagonistes de l’accusation
(AL, sa commission non mixte, la commission antisexiste de la CNT, les
syndicats qui depuis novembre semblent des plus investis et des mieux
informés) de voir la lumière être faite de façon sérieuse sur cette
accusation.

Bien entendu l'ensemble de ces éléments troublants n'innocentent pas
notre adhérent accusé, pas plus qu'il le culpabilise d'ailleurs, mais il
nous pose question car il nous laisse penser que ces structures qui
défendent aujourd'hui mordicus l'exclusion de Fouad sans chercher à
éclaircir la réalité des faits, tentent de nous mettre le couteau sous
la gorge par tous les moyens (internes et externes!) pour que nous
accédions à leur injonction, et ont, depuis des mois, plus cherchés à
poursuivre un objectif intéressé qu'à démontrer sur un plan objectif une
éventuelle culpabilité de notre adhérent, encore moins à protéger une
potentielle victime de viol.

Au regard de cette chronologie, notre syndicat a donc eu le sentiment
- et cela de façon indépendante de la culpabilité ou de l'innocence de
notre adhérent - que cette accusation a surtout permis d'alimenter de
nombreux règlement de comptes inter-personnels **ainsi qu'à servir certaines
postures idéologiques**.

Ce contexte a de fait influé sur notre réflexion et ne peut pas être
ignoré par lors de nos prises de décisions définitives.

IV)Conclusions de notre syndicat
1. Sur le fond de l'accusation et l'exclusion éventuelle de notre adhérent pour viol
Considérant tout d'abord que des éléments du récit de la victime supposée concernant le déroulé fait
semblent être contredit par les témoignages factuels que nous avons pu recueillir,
Considérant les propos de la victime supposée qui explique que son souvenir des faits supposés
repose sur les déclarations que l'accusé lui à lui-même formulé,
Considérant que les éléments à charge présentés dans le rapport de la commission non mixte d'AL
et qui concernent un changement de comportement possible durant le séjour au Roucous pouvant
révéler le fait qu'elle ait subi un viol, ne semblent pas concordants avec les différents éléments et
témoignages factuels dont nous avons pu disposer,
Considérant que les éléments à charge présentés dans le rapport de la commission non mixte d'AL
et qui décrivent une attitude déplacée de la part de l'accusé durant toute la semaine au Roucous, sont
nuancés par les témoins factuels qui décrivent une relation difficile dans les deux sens, laquelle n'a
pas empêché les deux protagonistes de partager de nombreux moments festifs,
Considérant que les consommations ne drogues multiples, tout au long de la semaine, par la victime
supposée comme par l'accusé sont très peu propices à crédibiliser les souvenirs et les discours des
un·es des autres, et peuvent conduire à des interprétations extrêmement variées des comportements
de chacun·e,
Considérant que les éléments de défense apportés par l'accusé, avant sa prise de connaissance des
faits reprochés, et après avoir été confrontés aux éléments d'accusation sont restés concordants et
sont accrédités par les témoins factuels que nous avons auditionnés,
Considérant que l'accusé lui-même a fait preuve de la transparence la plus totale tout au long de
cette procédure, en fournissant notamment à son syndicat d'appartenance, l'ensemble des éléments à
charge contre lui-même et qu'il a pu obtenir,
Considérant que l'accusé, dès les premières formulations de l'accusation par la victime s'est déclaré
devant toutes les organisations auxquelles il adhère qu'il était personnellement favorable au dépôt
de plainte de la victime supposée,
Considérant que la victime supposée disposait de potentielles raisons de formuler une fausse
accusation,
Considérant le rôle joué par Alternative Libertaire, qui a visiblement préféré attaquer publiquement
la CNT plutôt que de lui fournir des éléments objectifs qui lui auraient permis d'analyser
objectivement l'accusation,
Considérant que les pressions équivoques qui ont été faites par la commission non mixte d'AL sur
les témoins que nous avons auditionnés jettent un doute raisonnable sur la crédibilité des
témoignages tronqués et utilisés à charge dans le rapport de la commission,
Considérant que la chronologie de cette accusation révèle finalement des liens multiples entre la
victime supposée et celles et ceux qui ont relayé·es cette accusation et tenté·es par de multiplesmoyens de nous faire prendre une décision sous pression ; liens qui nous laissent penser que la
fragilité de cette femme a potentiellement pu être exploitée par des intérêts politiques,
Le syndicat SSCT Lorraine a estimé que :
– Une analyse objective de cette affaire, suite à une procédure réalisé avec équité à charge
comme à décharge, ne permet pas de tirer de conclusions formelles, ni sur la véracité de
l'accusation portée, ni sur l'innocence de notre adhérent dans cette affaire.
– Que les seuls éléments à charge qui ont été recueillis à l'encontre de Fouad et qui demeurent
cohérent à l'épreuve des faits sont trop peu nombreux et demeurent trop assujettis au doute
pour justifier d'une exclusion de notre adhérent.
2. Appui au dépôt de plainte de la victime supposée
Notre syndicat, après avoir mené cette procédure s'est déclaré incompétent pour établir quelconque
vérité. Malheureusement, au regard des éléments dont nous disposions, et en accord avec des
principes de justice qui sont pour nous inaliénables, nous considérons que notre syndicat n'a pu
rendre d'autre décision et qu'il ne peut de fait disposer de moyens d'investigations plus efficaces que
ceux dont il a déjà usés.
Aussi nous avons bien conscience que notre décision concernant la non exclusion de Fouad
constituerait une injustice terrible pour une femme victime de viol si celui-ci était coupable. Mais
notre syndicat n'a pas aujourd'hui les moyens de se substituer à un travail de police judiciaire ou
d'un magistrat. Pour ces raisons, notre syndicat, tel qu'il l'a déjà dit, se déclare favorable au fait que
la victime supposée dépose une plainte à l'encontre de Fouad.
Bien conscients des nombreuses difficultés qui sont celles rencontrées par une femme lorsqu'elle
porte plainte, notre syndicat pense qu'il est du devoir de la CNT, en tant qu'organisation féministe et
antisexiste que de proposer expressément à E. de bénéficier de son accompagnement dans le cadre
d'une telle démarche, si elle estime que celle-ci peut lui permettre de lui rendre justice.
Nous rappelons que si E. faisait le choix de déposer plainte à l'encontre de Fouad, notre syndicat
tiendra à disposition de la justice et de E. l'ensemble des éléments dont il dispose.
Nous rappelons à nouveau que Fouad s'est déclaré lui-même favorable à ce dépôt de plainte et a
déclaré se tenir à disposition de la justice dans ce cadre éventuel.
3. Perspectives en interne de la CNT
Tout d'abord, il est évident que cette accusation de viol à l'encontre de l'un de nos adhérents à
suscité l'éclosion de vives polémiques au sein de la Confédération, autour des notions
d'antipatriarcat, de féminisme, d'antisexisme, de justice. Ces polémiques semblent d'ailleurs, pour
certaines d'entre elles avoir été suscitées par l'influence d'une organisation politique, en l'occurrence
Alternative Libertaire, en lien avec certaines structures et adhérent·es de la CNT. Aussi, un débat
serein autour de ces différentes notions nous apparait aujourd'hui comme une nécessité absolue pour
préserver l'unité de la Confédération.
Ensuite, tel que l'on proposé différents syndicats, ainsi que la commission antisexiste de la CNT, il
semble nécessaire que la CNT puisse se doter d'un protocole claire et réfléchi pour traiter de lafaçon la plus juste possible ce type d'accusation en interne. S'il ne fait aucun doute que notre
syndicat voit sans doute un tel protocole de manière bien différente que les propositions qui ont déjà
été formulées par certains syndicats, nous rejoignons volontiers l'idée d'une discussion collective à
ce sujet.
Par ailleurs, les pratiques de certaines structures de la CNT, telles que la commission antisexiste de
la CNT, les agissements de syndicats qui remettent en cause l'autonomie d'autres syndicats ou du
Congrès, les agissements de certains syndicats et adhérent·es qui n'ont pas hésité à diffamer
publiquement notre adhérent avant même quelque conclusion de notre part sur le fond de cette
affaire, posent des questions fondamentales à notre fonctionnement Confédéral.
Finalement, nous ne doutons pas que plusieurs syndicats de la CNT, suite au rendu de notre
décision, décideront de proposer l'exclusion de notre syndicat de la Confédération. Il semble
d'ailleurs que cela démange certain·es depuis de nombreuses semaines. Bien que nous le regrettions,
il apparait inévitable que notre syndicat joue prochainement sur sa tête les problématiques
politiques qui déchirent les organisations libertaires, mais notre syndicat est prêt à assumer cet état
de fait. En effet, non seulement nous refusons de nous retrancher derrière une potentielle
impossibilité pour ces syndicats qui rêvent de proposer notre exclusion de la CNT à pouvoir le faire
en raison de leur éventuelle position minoritaire au sein de leur UR qui précédera le prochain CCN,
mais en plus nous refusons de vivre pendant encore 20 mois (c'est à dire jusqu'au prochain Congrès
Confédéral) les pressions et les injures que nous vivons depuis plus de 3 mois et qui entravent
toutes activités syndicales de notre part.
Pour toutes ces raisons, notre syndicat se déclare favorable à la tenue prochaine d'un Congrès
Confédéral extraordinaire, seul moyen pour que la CNT puisse trancher sereinement et
formellement les débats qui la sclérosent aujourd'hui ainsi que l'avenir de notre syndicat en son sein.
4. Relations avec Alternative Libertaires
Compte tenu des tentatives équivoque d'Alternative Libertaire de mettre la CNT sous la tutelle de
ses propres décisions, compte tenu des tentatives de cette organisation politique pour discréditer
notre Confédération sur le plan national, compte tenu des procédés odieux employés par AL dans le
cadre de l'instruction de cette affaire, compte tenu du choix qui a été fait par Alternative Libertaire
d’engager une communication publique laquelle relève de pratiques moyenâgeuses qui nous
horrifient, et cela quel que soit la réalité de la culpabilité ou non de Fouad, notre syndicat décide :
– De communiquer publiquement notre décision concernant Fouad et de dénoncer
publiquement les agissements d'Alternative Libertaire. Nous précisons que sans
communication publique d'AL, nous n'aurions sans doute pas pris·es cette décision. Mais
puisqu'AL a fait le choix de nous mettre devant le fait accompli et compte tenu des
conséquences dramatiques qu'ont eu leur injonction sur l'image de la CNT aux yeux du
grand public, nous n'avons pas d'autres choix.
– De rompre toutes relations inter organisations avec Alternative Libertaire. Si nous
respectons l'intégrité dont ont fait preuve les adhérent·es du CAL Moselle, nous regrettons
que, de fait, nous n'aurons plus de relation avec AL au niveau local, dans la mesure où ces
adhérent·es, affligé·es par les agissements de leur organisation, ont décidé de quitter AL.
– De faire des propositions concrètes à la Confédération, par le biais de motions qui seront
soumises au prochain Congrès extraordinaire, une évolution des relations entre la CNT et
AL sur le plan national et cela afin de protéger la confédération.
