
.. _amendement_motion_32_2012_interpro31:

============================================
Amendement à la motion 32 2012 (interpro31)
============================================

.. seealso::

   - :ref:`motion_32_2012_etpic30`



Amendement
===========


La motion est découpé en 4 points à voter distinctement.

Motion 32-1
------------

La CNT met en place une commission juridique et de formation confédérale.

Elle est composée d’un comité de coordination de mandaté.e.s et personnes
ressources de différentes régions et de différents secteurs d’activités (du
public comme du privé).

Cette commission aura à charge de conseiller, voire assister, l’ensemble des
structures de la CNT et de leurs syndiqué.e.s sur les questions juridiques
(droit syndical, droit du travail).

Elle n’a pas pour vocation de suppléer à l’action et l’investissement des
syndicats eux-mêmes.


Motion 32-2
-----------

La commission juridique confédérale étudie le rattachement de la CNT
d’organismes de formation habilités sur le plan national ou local à recevoir
des camarades sur leurs droits à la formation.

En vertu d’une démarche d’entraide et de solidarité interprofessionnelle, le
congrès invite de même chaque Union régionale à se doter de leur propre
commission juridique.

Elles ont pour vocation d’assurer au plus près conseils et assistances à
l’ensemble des syndicats des Régions et de mutualiser les compétences locales.

Motion 32-3
-----------

Bien que privilégiant les compétences et les outils collectifs internes, les
syndicats et les Unions peuvent être amenés à recourir à l’intervention de
professionnels pour défendre leurs intérêts sur le plan juridique.

En guise d’alternative, la CNT peut promouvoir l’émergence de sociétés
coopératives à vocation juridique avec lesquelles elle développera un
partenariat privilégié.

Motion 32-4
------------

Lorsqu’il est avéré que les ressources et outils internes sont insuffisants ou
inefficients, la CNT admet le recours à un « permanent juridique »,
exclusivement salarié.

Emploi d’un permanent chargé des questions juridiques.

Considérant que l’action juridique doit s’inscrire dans une démarche
inter-corporatiste, seules les Unions régionales et la Confédération ont
compétence pour procéder à cet emploi.

Tout recours au salariat devra répondre des mêmes conditions préalables que
pour les permanents techniques.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°32 |            |          |            |                           |
| INTERPRO31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
