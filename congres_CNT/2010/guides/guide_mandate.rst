

.. index::
   Guide du mandaté au XXXIe congrès de la C.N.T à Saint-Etienne


.. _guide_mandate_CNT_2010:

===============================================
Guide du mandaté pour le XXXIe Congrès CNT 2010
===============================================

Voir https://www.intranet.cnt-fr.org/public.d/bc/congres/preparation_des_congres/congres_2010/

Mode d'emploi du présent cahier des mandaté/e/s.

Vous trouverez dans ce cahier une aide pour reporter le vote de votre syndicat
quant à chaque :term:`motion`, contre-motion et amendement.

Le cadre suivant est placé à la suite de chaque texte mis au vote:

+----------------------------+------------+----------+------------+---------------------------+
| Nom                        | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============================+============+==========+============+===========================+
| Motion n°X                 |            |          |            |                           |
| Nom du syndicat            |            |          |            |                           |
+----------------------------+------------+----------+------------+---------------------------+


La première colonne, intitulée "Nom" donne la nature du texte à voter
(soit: motion, contre-motion ou amendement), suivi du nom du syndicat qui
présente le texte.

Les colonnes suivantes donnent les différents choix de vote possible pour
le syndicat. Il ne reste plus qu'a mettre une petite croix, un chat hérissé
ou quelque autre signe distinctif dans la case correspondante au vote du syndicat
pour lequel vous êtes mandaté/e.

En fin de cahier, vous retrouverez de l'espace afin de faire des prises de
notes ainsi que des feuillets à remettre au moment de votre départ.
Ces feuillets récapitulent les votes de votre syndicat et permettront une
confirmation des votes. Vous souhaitant un bon congrès,

La commission préparation du 31ème congrès confédéral 2010.
