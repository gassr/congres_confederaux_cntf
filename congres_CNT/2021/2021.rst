.. index::
   pair: 2021; Congrès CNT (Dijon)
   pair: xxxv; Congrès CNT 2021 (Dijon)

.. _xxxv_congres_CNT:
.. _xxxv_congres:

=================================================================================
Le **XXXVe Congrès CNT 2021 de Dijon** du vendredi 25 au dimanche 27 juin 2021
=================================================================================

- :download:`Télécharger le CAHIER_de_la_du_mandate·e_35eme_Congres_confederal_2021_06_01.pdf  <preparation/2021/06/01/CAHIER_de_la_du_mandatee_35eme_Congres_confederal_2021_06_01.pdf>`


- :ref:`congres_CNT_2021_synthese_motions`
- :ref:`congres_CNT_2021_motions`
- :ref:`contre_motions_congres_2021`
- :ref:`congres_CNT_2021_amendements`


- :ref:`statuts_conf:recueil_motions_adoptees_cnt`
- :ref:`statuts_conf:congres_statuts_CNT`
- :ref:`fonctionnement_congres_confederal`

Dijon
========

.. figure:: images/dijon_carte.png
   :align: center

   https://fr.wikipedia.org/wiki/Dijon


Dijon (prononcer [di.ʒɔ̃]) est une commune française, préfecture du département
de la Côte-d'Or et chef-lieu de la région Bourgogne-Franche-Comté.

Elle se situe entre le bassin parisien et le bassin rhodanien, sur l'axe
Paris-Lyon-Méditerranée, à 310 kilomètres au sud-est de Paris et 190 kilomètres
au nord de Lyon.

Ses habitants, appelés les Dijonnais, étaient au nombre de 156 920 en 2017.

L'unité urbaine, qui comptabilisait pour sa part 243 376 habitants, est la
première agglomération de la région en nombre d'habitants.
Elle est au centre d'une intercommunalité, Dijon Métropole, comprenant 23 communes
et 251 897 habitants et d'une aire urbaine de 385 400 habitants.


.. _cahier_mandatee_2021:

Cahier de la/du mandaté·e 35e congrès confédéral CNT de Dijon 2021 (mardi 1er juin 2021, 10:38)
====================================================================================================

- :ref:`message_sc_2021_06_01`

::


    Objet: Fwd: Fwd: Re: Fwd: [Liste-syndicats] Cahier des mandaté.e.s du
    XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date: 2021-06-01 10:38
    De: Secrétariat Confédéral <cnt@cnt-f.org>
    À: liste CA <liste.ca@bc.cnt-fr.org>, liste-bureau@bc.cnt-fr.org,
    Liste-syndicats@bc.cnt-fr.org


- :download:`Télécharger le CAHIER_de_la_du_mandate·e_35eme_Congres_confederal_2021_06_01.pdf  <preparation/2021/06/01/CAHIER_de_la_du_mandatee_35eme_Congres_confederal_2021_06_01.pdf>`


.. warning:: Pas de fichiers source odt


- :ref:`congres_CNT_2021_synthese_motions`
- :ref:`congres_CNT_2021_motions`
- :ref:`contre_motions_congres_2021`
- :ref:`congres_CNT_2021_amendements`


Préambule
=========

.. toctree::
   :maxdepth: 3

   preambule/preambule



.. _syndicats_presents_2021:

Nombre de syndicats présents à Dijon, 2021: xx
======================================================


Ordre du jour
=============

.. toctree::
   :maxdepth: 3

   ordre_du_jour/ordre_du_jour

Motions
=======

.. toctree::
   :maxdepth: 3

   motions/motions
   etats_motions/etats_motions


Contre-motions
==============

.. toctree::
   :maxdepth: 2


   contre_motions/contre_motions

Amendements
============

.. toctree::
   :maxdepth: 2

   amendements/amendements


Candidatures
==============

.. toctree::
   :maxdepth: 2

   candidatures/candidatures

Relevé de décisions
====================

.. toctree::
   :maxdepth: 3

   releve_decisions/releve_decisions

Préparation Congrès
====================

.. toctree::
   :maxdepth: 3

   preparation/preparation
