
============
Constitution
============

Article 1
=========

Conformément aux :ref:`statuts <statuts_CNT>`  de la Confédération Nationale du Travail,
il est fondé  entre ceux qui adhèrent aux présents statuts la Fédération des
Travailleurs des  Transports, de la Logistique et Activités Auxiliaires de la
CNT dite  **Fédération CNT Transport**, dont le siège est fixé au siège de la CNT.


Article 2
=========

La Fédération regroupe les syndicats du transport adhérents à la Confédération
Nationale du Travail, ainsi que tout syndiqué dans une 'entreprise relevant du
transport adhérent à un syndicat intercorporatiste de la CNT, ou tout syndiqué
du transport dans un secteur géographique où la CNT n'est pas présente.


But
Article 3: La fédération a pour but de coordonner l'action des sections et des syndicats dans les
secteurs des transports afin d'arriver à constituer le travail libre, affranchi de toute exploitation
capitaliste, par la syndicalisation des moyens de production, au bénéfice exclusif des travailleurs.
Dans l'action journalière, elle se donne pour but :
_ De représenter et de défendre les organisations adhérentes, dans toutes les questions ayant un
caractère d'intérêt général; de resserrer étroitement les liens de solidarité et unir dans un seul bloc
tous les travailleurs des industries qu'elle fédère, sans distinction de sexe et de nationalité;
_ D'organiser partout où il sera possible:

des Sections Syndicales d'Entreprise,

des Syndicats Locaux du transport,
_ De lutter pour la suppression de l'intérim et de la sous-traitance et du travail à la tâche ou aux
pièces;
_ De poursuivre la diminution des heures de travail par tous les moyens en son pouvoir;
_ D'empêcher la baisse et l'avilissement des salaires.



Article 4: La Fédération s'interdit toute prise de position sur les orientations générales et la
stratégie de la CNT, qui sont de la compétence exclusive des syndicats et du Congrès Confédéral.
Administration
Article 5: La Fédération est administrée par un bureau dont les membres sont élus à la majorité
du congrès fédéral et responsables devant lui. Afin d'alléger le fonctionnement administratif, le
congrès peut décider de charger un syndicat adhérent à la Fédération de prendre en charge son
bureau.
Le nombre de membres du bureau est fixé en congrès fédéral. Le bureau est composé d'au moins
deux membres : un secrétaire et un trésorier. Les membres du bureau sont élus pour deux ans. Ils
ne sont rééligibles qu'en cas de carence de candidature.
Article 6: Les membres du bureau ne doivent occuper aucune responsabilité dans une
organisation politique, philosophique ou religieuse. Ils sont révocables par le congrès ordinaire de
la Fédération ou par un congrès extraordinaire.
Article 7: Le bureau se réunit régulièrement autant de fois qu'il en a besoin. Il est tenu de faire
connaître son mode de fonctionnement. Un compte-rendu de séance mentionnant les travaux
effectués peut être établi et conservé dans les archives.
Article 8: ( attributions du secrétaire ) Le secrétaire est chargé de la correspondance et des
autres travaux afférents au bon fonctionnement de la Fédération, procès-verbaux, rapports, qui
seront envoyés à chaque syndicat adhérent ; il sera secondé par autant d'ajoints que le congrès
fédéral le jugera nécéssaire.
Article 9: ( attributions du trésorier ) Le trésorier est chargé d'encaisser les cotisations fédérales,
de faire les paiements et d'opérer le placement des fonds disponibles dans les endroits désignés
par le Congrès Fédéral ; il tiendra le livre des recettes et dépenses ; il dressera à chaque Congrés
Fédéral, un bilan de la situation financière qui sera envoyé à tous les syndicats fédérés ; il sera
secondé par autant d'adjoints que le congrès fédéral le jugera nécéssaire.
Article 10: Le secrétaire, le trésorier, leurs adjoints, ou toute autre adhérent, concourrant à leurs
tâches ne sont ni permanents, ni rétribués, ce qui conforte le caractère autogestionnaire de la
Fédération.
Admissions
Article 11: Ne seront admis à la fédération que les groupements composés de travailleurs des
secteurs des transports et activités auxiliaires,ainsi que les adhérents de syndicats intercorporatif,
travaillant dans les transports, à l'exception de ceux qui tirent profit d'autres travailleurs.
La fédération n'exclue aucun type de transport. Elle regroupe ;
les travailleurs des transports de personnes ou de marchandises, routiers, ferroviaires, fluviaux,
maritimes, aériens, transports en commun ou individuel,les coursiers, les chauffeurs de maitres,
dans le secteur de la santé (ambulanciers, pompes funèbres), les travailleurs des plateformes
logistiques, les déménageurs, les docks et toutes leurs activités auxiliaires (accompagnateurs,
mécaniciens, manutentionnaires, administratifs, ect...) que ces secteurs soient de droit privé ou
public. En bref, tous les salariés dépendant d'une convention collective concernant les transports et
la logistique. Sans oublier les travailleurs étrangers travaillant sous pavillon Français dans la marine
marchande, ou les chauffeurs étrangers employés par des sociétés francaises.
Chaque syndicat devra accompagner sa demande d'admission de deux exemplaires de
ses statuts et indiquer le nombre de ses adhérents, ainsi que les noms et adresses de ses
secrétaire et trésorier.
Son adhésion sera effective et entraînera cotisation après délivrance du label syndical
dépendant de l'avis de la Fédération, du Bureau Confédéral et de la structure régionale si elle
éxiste.
Article 12: Il ne pourra être admis à la Fédération qu'un seul syndicat par secteur géographique
( les délimitations étant définies par le congrés fédéral)
Cotisations
Article 13: Le montant des cotisations est fixé en Congrès Fédéral. Il est de 1,5 euros par mois et
par adhérent, afin d'assurer le bon fonctionnement de la Fédération.
Congrès Fédéral
Article 14: Le congrès des syndicats adhérents à la Fédération a lieu tous les deux ans. Il est seul
habilité à modifier les présents statuts. Toute modification doit être portée à la connaissance de
l'ensemble des syndicats de la Fédération. C'est le congrès qui fixe les axes de travail collectif de la
Fédération, qui discute le rapport moral et financier du bureau fédéral sortant et élit le nouveau
bureau fédéral.
Les modalités de vote au congrès fédéral sont celles en vigueur pour le congrès
confédéral de la CNT.

Congrès Fédéral Extraordinaire
Article 15: En cas de besoin, le bureau fédéral peut convoquer un congrès fédéral extraordinaire.
De même, un congrès fédéral extraordinaire sera automatiquement convoqué si la
moitié au moins des syndicats adhérents à la fédération en fait la demande.
Commissions d'études techniques
Article 16: Des commissions d'études techniques peuvent être adjointes au bureau fédéral pour la
mise au point des questions revendicatives dans les différents secteurs des transports. Les
commissions n'ont aucun pouvoir de décision. Un Compte-rendu de travail effectué lors de chaque
réunion peut être établi dans les mêmes conditions que les procès-verbaux ou les Comptes-rendus
du bureau.
Les commissions sont constituées par des adhérents de la CNT
Grève
Article 17: Les syndicats fédérés s'engagent à avertir la Fédération, en cas de grève et à
communiquer un maximum d'informations sur la grève.
La Fédération s'engage à apporter son soutien, tant matériel que financier et juridique,
dans la mesure de ses possibilités.
Date et signature:
Le secrétaire:
Le trésorier:
* A chaque fois qu'il est fais mention de « transport », la fédération désigne l'ensemble de l'appellation, à savoir,
transport, logistique et activités auxiliaires.
