
.. _amendement_motion_4_2021_stics72:

===================================================================================================
Amendement à la Motion n°4 2021 **Motion Fédéraliste pour une réunification** par **STICS72**
===================================================================================================

- :ref:`motion_4_2021_ptt_centre`

Autres motions STICS72

    - :ref:`syndicats:motions_stics72`


Argumentaire 
============


Amendement 
============

.. raw:: html

    La Confédération Nationale du Travail s’engage à mettre en œuvre <del>par tous
    les moyens à sa portée</del> la convergence et <del>la ré</del><span class="modif_rouge">l’</span>unification
    des différentes composantes anarcho-syndicalistes et syndicalistes révolutionnaires
    <del>souvent</del> issues <span class="modif_rouge">également</span> des scissions de notre Confédération.

    Elle le fait pour garantir la véritable expression du fédéralisme libertaire,
    l’autonomie des syndicats à la base de notre projet, la cohérence et la
    cohésion de ce projet et en vue d’aboutir au renforcement des capacités
    et des moyens d’action, de l’audience de l’anarcho-syndicalisme et du
    syndicalisme révolutionnaire.



Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°4  |            |          |            |                           |
| STICS72                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
