.. index::
   pair: Motion 8 ; 2021

.. _motion_8_2021_etpics94:

================================================================================================================================================
Motion **n°8** 2021: **Prostitution et Abolitionnisme révolutionnaire** par **ETPICS 94**
================================================================================================================================================

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`

Motion
=======

- **Parce que** la prostitution est une des plus anciennes manifestations
  de la **domination patriarcale**,
- **Parce qu'elle** est au cœur de la marchandisation de l'être humain et de
  l'exploitation économique,
- **Parce qu'elle** touche aujourd'hui très majoritairement des personnes
  immigrées dans de véritables trafics atroces et inhumains,
- **Parce qu'elle** détruit les personnes jusque dans leur intimité physique
  et psychologique,

la CNT affirme son positionnement **abolitionniste révolutionnaire**.

**Abolitionniste**, car en tant qu'anarcho-syndicaliste et syndicalistes
révolutionnaires, fidèles à nos principes et notre histoire, partisans d'une
émancipation humaine et de la construction d'une société égalitaire et libertaire,
nous ne saurions tolérer que subsiste cette forme d'exploitation, de domination
et d'aliénation poussée à son paroxysme.

**Révolutionnaire**, car il ne saurait s'agir de condamner ou punir ou
pourchasser via l’État ou des lois quelconques les victimes du système
prostitutionnel, mais parce que nous comptons détruire ce système même en
combattant sans relâche l'exploitation capitaliste, la domination sexiste,
les États et les frontières qui fabriquent les migrations forcées et le racisme.

Amendements
===========

- :ref:`amendement_motion_8_2021_ptt_aquitaine`

Contre-motions
==============

- :ref:`contre_motion_8_2021_cnt42`
- :ref:`contre_motion_8_2021_sinr44`
- :ref:`contre_motion_8_2021_ste72`
- :ref:`contre_motion_8_2021_interco69`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°8 2021 ETPICS94    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
