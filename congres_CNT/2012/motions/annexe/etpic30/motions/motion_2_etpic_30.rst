


.. _motion_etpic2_2012:

==============================================================================
Utilisation des droits horaires syndicaux pour l’activité militante (motion 2)
==============================================================================

Les syndiqué.e.s de la CNT, les élu.e.s CNT (délégué.e.s du personnel,
représentant.e.s au Comité d’entreprise, élu.e.s aux commissions paritaires
de la fonction publique), les délégué.e.s syndicaux CNT peuvent se saisir
librement des heures de délégation et autorisations spéciales d’absences mises
à leur disposition pour l’activité  syndicale (au sens le large) et de
représentation du personnel.

Le recours aux décharges syndicales ou de service, lorsqu’elles sont accessibles,
est limité aux seules fins d’activité syndicale de terrain et/ou de propagande.
Leur utilisation ne peut donc être associée à la tenue des mandats propre à
l’organisation interne de la CNT.
