
.. index::
   pair: Motion 17 ; 2014

.. _motion_17_2014_ste75:

==================================================================
Motion n°17 2014 : Travailleuses et travailleurs du sexe (STE 75)
==================================================================

.. seealso::

   - :ref:`motions_ste75`




Argumentaire
=============

La rencontre entre des membres du bureau confédéral et des membres
du Strass, le 17 mai 2014, nous donne l'occasion d'approfondir la
question d'un partenariat avec les organisations de travailleuses
et travailleurs du sexe.

Cela fait plusieurs années que des membres et des syndicats de la CNT
soutiennent les luttes contre la répression, la précarisation et
l'invisibilisation des travailleuses et travailleurs du sexe
(débats et actions organisés à Toulouse, Paris, signature de
l'appel contre la pénalisation des clients par plusieurs syndicats
comme Besançon, Nancy).

De même, la question du travail sexuel et de l'organisation des
travailleuses et travailleurs suscite un débat au sein de la CNT.

Or dans le cadre de ce débat, comme parmi toutes les organisations qui
interviennent dans la problématique du travail sexuel, il nous paraît
important que la CNT soutienne les dynamiques d'auto-organisation des
travailleuses et travailleurs qui luttent de leur propre initiative et
par leurs propres moyens pour leur émancipation.

Il ne s'agit pas forcément de se faire le relais de l'intégralité
de leurs revendications et leurs actions, mais il est important que
ce soit la parole et les revendications des travailleuses et
travailleurs qui soient prises en compte dans ces débats, et pas
seulement celles d'organisations qui prétendent parler et agir en
leur nom (parfois de façon très paternaliste, en victimisant les
travailleuses du sexe et en dé-légitimant la parole des
travailleuses et travailleurs du sexe, en tant que parole aliénée).

Il s'agit donc d'acter un partenariat avec des organisations dont les
luttes rejoignent les nôtres, avec des militant-e-s que nous côtoyons
sur de nombreux terrains de lutte (contre la précarité, la guerre aux
pauvres, la répression policière, pour les régularisations des personnes
sans-papiers...).


Motion
========

La confédération s'engage à soutenir les luttes des travailleuses et
travailleurs du sexe et à travailler avec les organisations de
travailleuses et travailleurs du sexe à condition qu'elles soient
organisées par elles et eux-mêmes (comme Grisilédis, le Strass,
Putain dans l'âme...).


Contre-motions
===========================

.. seealso::

   - :ref:`contre_motion_17_2014_interco73`


Votes
=====

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°17 2014

       :ref:`STE75 <ste75>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
