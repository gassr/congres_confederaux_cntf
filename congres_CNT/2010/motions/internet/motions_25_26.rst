

.. _motions_25_26_2010:

================================================================
Motions n°25 et 26 2010 : Modération Intranet, CCS de la Vienne
================================================================

.. seealso::

   - :ref:`annexes_motions_25_26_2010`



Argumentaire Communication Culture et Spectacle de la Vienne
============================================================

Après plusieurs années d'utilisation du média internet, outil qui tend à devenir le mode, presque exclusif sinon
essentiel (et sans doute trop) de communication, il apparaît incontournable de réaliser un bilan critique de
l'utilisation de celui-ci.

Il faut préalablement préciser que les éléments critiques et de réflexions formulés ci-dessous, ne concernent pas
uniquement « notre espace intranet », mais plus largement l'ensemble des espaces de discussions dématérialisés,
privés et publics, de nature militante...

En effet, un rapide tour d'horizon des forums existants montrent combien la modération (ou l'automodération) de
ces espaces est difficile, face à des individualités fortes et à certaines incidences du milieu militant lui-même, au
sein duquel la tentation de disposer de la « bonne parole » de la « vérité » face à l'autre est prééminente... Et ce
sentiment est d'autant plus fort que la relation virtuelle et le support écrit et direct, ne renforcent pas le sentiment
d'empathie et la compréhension entre deux personnes physiques qui confronteraient face à face leurs idées...
Il semble également que cette période d'anicroche apparaît être le reflet d'une crise plus globale en interne de la
confédération... Par conséquent, tout n'est pas à mettre sur le compte du média en lui même.

Un rapide parcours des échanges ne serait-ce que ces derniers mois sur le forum de l'intranet confédéral, permet
de pointer des anomalies dans la façon dont nous échangeons sur cet espace (en annexe, un rapide digest des
pires extraits d'échange entre militants sur l'intranet). Nous y assistons à de vrais règlements de compte entre
individualités, en contradiction avec l'esprit de l'outil intranet qui devrait s'apparenter davantage à un espace
d'échange d'informations et de débat, contradictoires certes, mais cordiales, dans un l'esprit de solidarité et de
soutien mutuel entre camarades dans une confédération syndicale de nature anarcho-syndicaliste et syndicaliste
révolutionnaire.

Or, les **pics d'agressivité** récurrents font oublier à leurs auteurs,
que cet espace est aussi visible par tous les militants au même moment
et en tout lieu.

Aussi, dans les faits les militants assistent à des discussions d'ordre
privé entre individus, parfois assez éloignés du champ militant

C'est à la l'aune de ces constats que nous pensons qu'il faut réfléchir à la mise en place d'outils de modération et
en premier lieu sur notre espace intranet. Actuellement sur l'espace intranet, sont inscrits des militants à titre
individuel qui échangent librement entre eux de tous les sujets. Et même si leur adresse est raccrochée à leur
dénomination de syndicat, celle-ci ne permet cependant pas de dire que ce qui est échangé est de l'ordre d'un
mandatement syndical quelconque. Il s'agit donc dans la plupart du temps d'un échange entre individus... Même
si notre outil s'intitule intranet, c'est à dire un espace non accessible au grand public, il ne s'agit pas à priori d'un
espace entièrement privé, mais bien d'un espace autogéré au service de notre confédération.

L'objectif est bien de continuer à faire de cet espace un lieu de fluidité
des échanges, démocratique, sans contrainte sur la vitalité des débats.

Ainsi, nous proposons deux alternatives de motion, qui sont à trancher, tant dans leurs principes (démocratie,
libre expression...) que dans leurs objectifs qui vise avant tout l'efficience. L'horizon qui nous est donné est soit
de développer, d'améliorer l'outil intranet ou une liste de diffusion (pour la transmission d'information et de
débats au sein de notre confédération) ou nous réfugier dans un fétichisme de l'expression virtuelle, sans limite,
au détriment de l'outil de communication et de notre organisation.

La première motion propose de refonder l'utilisation de l'intranet en proposant de n'inscrire sur cet espace que les
seuls mandatés des syndicats de la confédération (on dira plus simplement que ce se sont les syndicats eux même
et non plus les syndiqués qui auront la gestion et les codes d'accès intranet, libre aux syndicats de gérer cet accès
à un ou plusieurs mandatés, mais leurs paroles engageront désormais collectivement leur syndicat)

La seconde motion, à défaut du rejet de la première motion, se contente de réaffirmer le cadre de fonctionnement
actuel (l'inscription individuelle ) tout en proposant la mise en place d'un groupe de modérateurs dudit forum,
conformément à la charte d'utilisation et à son esprit.

Le syndicat CCS de Poitiers insiste sur le fait que si la seconde motion peut apparaître plus simple, car laissant la
part belle à une plus grande « liberté », nous mettons en garde sur la plus grande difficulté à trouver le juste
équilibre en matière de modération, dans un espace virtuel d'échanges entre individus. On ne saurait en effet
libérer la parole des individus sur cet espace dans un premier temps, pour ensuite avoir la tentation de la censurer
à la moindre expression qui apparaîtrait à certains transgressives (en toute subjectivité). On ne peut avoir le
beurre et l'argent du beurre à savoir une absolue liberté individuelle et une expression lissée sur une certaine
ligne idéologique ou un impératif éthique dictée... par... par quoi... et par qui d'ailleurs ?

Au vu des développements de ces derniers mois, le syndicat CCS appuie
évidemment particulièrement la première motion.

Les moyens
===========

Une modération de l'intranet nécessite un suivi régulier des débats, surtout dans le cas de figure où ceux-ci se
résument à des querelles d'individus portant sur des sujets extra syndicaux. Cette modération serait ainsi effectué
par un collectif de mandatés désignés comme modérateurs, issus de syndicats de différents secteurs
professionnels et de secteurs géographiques variés afin de permettre la pluralité des points de vue.
Dans le cas de l'inscription à l'intranet des syndicats et non plus des individus, une modération sera toujours
nécessaire mais sera plus simple et plus légère (un seul mandaté peut suffire) car de fait, les syndicats
effectueront eux-même leur propre modération, car ce qui apparaitra sur cet espace les engagera directement.


.. _motion_25_2010:

Motion n°25 2010, Modération Intranet, CCS de la Vienne
========================================================

La CNT développe un espace intranet en vue de permettre l'échange aussi rapide que possible de d'information,
et met en place les conditions rendant possible le débat et l'échange d'idées entre les différents syndicats qui
composent la confédération.
L'inscription sur cet espace est reservée aux syndicats et donc aux seuls mandatés par les syndicats. Les contenus
diffusés sur cet espace par les mandatés engageront donc les syndicats auxquels ils appartiennent.
Un mandaté de la confédération veillera à la modération éventuelle de cet espace de discussion en rappelant ses
règles d'utilisation, conformément à la charte d'utilisation et à l'esprit de celle-ci. En cas d'entorse à ces règles, le
modérateur interpellera le syndicat et le mandaté du syndicat sur les anomalies de fonctionnement observées. Le
syndicat interpelé par le modérateur, décidera des éventuelles modifications à apporter à sa communication sur
cet espace.

Si le modérateur observe des entorses répétées d'un mandaté sur le site, les instances confédérales (bureau
confédéral, CCN) seront saisis pour opérer une médiation avec le syndicat afin de rétablir les bonnes conditions
d'utilisation de l'intranet.

Il ne saurait y avoir exclusion d'un syndicat sur cet espace.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°25          |            |          |            |                           |
| CCS de la Vienne     |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+



.. _motion_26_2010:

Motion n°26 2010, Modération Intranet, CCS de la Vienne
------------------------------------------------------------

**La CNT met en place une modération de l'intranet confédéral**.

Elle appelle au mandatement d'un collectif de trois personnes issues de
syndicats différents, chargées de suivre la bonne marche des débats sur
cet espace, conformément à la charte d'utilisation et à l'esprit de celle ci.

Les mandatés pourront interpeler et rappeler les règles d'utilisation de
cet espace s'ils considèrent qu'il y a des entorses à la **charte d'utilisation**.

Tout message portant atteinte personnelle à l'encontre d'un camarade de
la CNT pourra être supprimé par les modérateurs.

Les modérateurs ne pourront supprimer un message d'un utilisateur sans
l'en avoir averti auparavant et avoir rappeler les raisons qui les amènent
à supprimer un message.

**En cas d'entorses graves et réitérés, un utilisateur peut être radié de
cet espace**.

Toutefois, cela ne peut être fait sans qu'il ait été fait trois rappels
à l'utilisateur par les modérateurs. L'utilisateur peut dans ce cas
demander un recours par une médiation auprès de son syndicat d'appartenance.

Toute radiation n'est que temporaire.

On ne saurait exclure un militant définitivement d'un espace de démocratie.

Il appartient à la confédération de déterminer la durée de la période de radiation.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°26          |            |          |            |                           |
| CCS de la Vienne     |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`amendement_motions_26_29_2010_stp72`
