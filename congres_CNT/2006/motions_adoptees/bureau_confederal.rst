
.. _motions_bureau_confederal_cnt_congres_agen_2006:


=================================================================================
Motions Bureau Confédéral, 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
=================================================================================

Bilan d’activité du Bureau confédéral & Organigramme de la CNT
==============================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 62         | 2        | 2          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Nous demandons au bureau confédéral et à la trésorerie d'intégrer à leur
bilan fait aux syndicats un rapport sur la structuration de la Confédération,
c’est à dire sur les unions régionales et fédérations :

- Les UR constitués à avec le détail des syndicats rattachés ;
- Les Fédérations constituées avec le nombre de syndicats rattachés (bilan à
  faire éventuellement au Congrès par les Fédérations elles mêmes, si le BC
  manque d'information).


Création d’un secrétariat de propagande électronique
====================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 39         | 8        | 9          | 9                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+



Texte
-----

La Confédération met en place un secrétariat (ou, le cas échéant, une
commission) qui :

- Gère les adresses e-mails sur la base de données de l'administration CS
  (trois mois gratuits) ;
- Envoie et diffusent des informations en coordination avec les structures
  confédérales (sections syndicales, commissions, syndicats, bureaux régionaux
  et confédéral) ;
- Réalisent un envoi régulier (p. ex. une fois par mois) vers toutes les
  adresses e-mail des différentes structures de la confédération et en
  direction des sympathisants (sur la base de données des "trois mois gratuits")
  avec le but de mieux diffuser l'information et la propagande actuelles
  de la CNT.
