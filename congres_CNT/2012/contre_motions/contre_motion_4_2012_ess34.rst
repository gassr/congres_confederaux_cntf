

.. _contre_motion_4_2012_ess34:

================================================================================================
Contre-motion à la motion N°4 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_4_2012_stel`



Contre-motion
=============

Lors des congrès confédéraux, les votes des quitus sont supprimés.

Un mois avant le congrès et les CCN, chaque mandaté-e envoie un bilan de
son mandat.

Un tour de parole sur chaque mandat est organisé, les syndicats donnent alors
leur avis sur le mandat


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°4      |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2012`
