
.. _motions_structuration_confederale_cnt_congres_toulouse_2001:

=========================================================================================================
Motion "Structuration confédérale" de la CNT  27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
=========================================================================================================

Label confédéral
================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 18         | 15       | 8          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Au regard du développement actuel de la confédération, les moyens de
l'obtention du label C.N.T. pour les syndicats locaux nouvellement créés sont
précisés.

Ce label est dépendant de l'avis favorable, conjoint et justifié du BC et de
la structure régionale (UL ou UR) la plus proche du lieu d'exercice de ce
nouveau syndicat.

Cet avis sera soumis pour validation effective à l'approbation d'un CCN.


Coordination des syndicats Interco
==================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 36         | 3        | 9          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Dans l'esprit que sont les INTERCO (syndicats d'accueil pour la création de
syndicats par branche d'industrie), nous estimons qu'il est nécessaire qu'ils
puissent:

- Se coordonner afin de mener -comme le font les syndicats constitués en
  fédérations- des campagnes de développement dans des secteurs précis
  (financiers, hôtellerie, restauration, tourisme..);
- Etre à l'initiative de campagnes nationales sur des thématiques plus larges
  (tout en y incluant les autres syndicats de la Confédération).

Comment se coordonner ? Notre syndicat se propose pour organiser une première
réunion de travail constitutive de cette coordination et de préparer avec les
syndicats intéressés une première ébauche structurelle, avec envoi de textes,
proposition d'une date et d'un lieu de réunion.

Des bilans seront faits lors des CCN et devant le Congrès.
