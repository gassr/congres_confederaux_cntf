
.. index::
   pair: Motion 20 ; 2014
   pair: Motion 20 2014 ; Intranet

.. _motion_20_2014_etpic30:
.. _motion_intranet_2014:
.. _moderer_forum_2014:

===============================================================================
Motion n°20 2014 : **Intranet  Mandat du modérateur du forum** (ETPIC 30 sud)
===============================================================================

.. seealso::

   - :ref:`motions_etpic30`
   - :ref:`statuts_conf:motions_internet_intranet_cnt`




Argumentaire
=============

Le forum de discussion de l’intranet confédéral est espace d’échange
et d’information permettant aux cénétistes de discuter librement.

**Il semble utile toutefois que cet espace soit régi par des règles
collectives d’utilisation et d’expression**.

A ce titre, unE modérateur/trice est désignéE depuis plusieurs années
par les Congrès confédéraux successifs.

Son mandat consiste essentiellement à solliciter auprès de nos camarades
**un ton plus fraternel dans leurs échanges**. Il peut toutefois prononcer
une radiation d’une ou plusieurs personnes de l’intranet confédéral.

Ces avertissements ou ces radiations éventuelles ne s’assoient ni sur
une base de principe claire, ni sur une collégialité suffisante.

Le Congrès doit pouvoir confier ce mandat à unE modérateur/trice sans
que cette acteur soit trop exposé personnellement.

Cette motion vise à donner au mandat de modérateur un socle de principe,
une collégialité à sa prise de décision, et un cadre d’intervention.

.. _texte_motion_20_2014:

Motion mandat du modérateur du forum [2014]
==============================================

Les personnes ayant accès à l’intranet confédéral doivent être
adhérentes à la CNT.

Elles peuvent s’exprimer librement et fraternellement sur le forum
de l’intranet confédéral.

Le forum de l’intranet confédéral est modéré par un **modérateur/trice**
dont le mandat prévoit expressément:

- de **solliciter un ton plus fraternel** dans les échanges ayant cours
  sur le forum ;
- d’adresser des avertissements aux personnes ne respectant pas les
  règles de principes d’utilisation du forum et d’en prévenir leur
  syndicat et les mandatéEs intranet de ces derniers ;
- de prononcer la radiation des personnes ne respectant pas les règles
  de principes d’utilisation du forum et d’en prévenir leur syndicat
  et les mandatéEs intranet de ces derniers ;


Les motifs d’avertissement ou de radiation des accès individuels au
forum sont les suivants:

- **Insultes ou diffamations** de personnes adhérentes à la CNT, ayant
  accès ou non au forum
- **Propos ouvertement discriminatoires** ;
- **Atteintes graves à l’intégrité morale ou physique** ;
- Envoi disproportionné de messages dans le but manifeste de bloquer
  l’utilisation usuelle du forum ;
- **Exportation et diffusion publique de messages d’expression
  individuelle** du forum vers l’extérieur ;

Le modérateur privilégie la recherche de consensus avec la/les personne/s
concernée/s en vue d’**obtenir si nécessaire des excuses, la suppression
ou la modification de son message**.

En fonction de la gravité, l’avertissement précède la radiation.

Un avertissement est adressé à la personne avec copie à son syndicat.

Toute radiation prononcée par le modérateur du forum doit être
suffisamment motivée, argumentée.

La Commission Administrative Confédérale est consultée par le
modérateur préalablement.

La personne radiée du forum de l’intranet confédéral peut solliciter
via son syndicat une intervention de son UR en vue d’un nouvel accès.


Amendement
==============

- :ref:`amendement_motion_20_2014_sse31`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°20 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
