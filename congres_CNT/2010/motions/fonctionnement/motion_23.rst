

.. index::
   pair: Motion 23 ; 2010 (ETPIC94)

.. _motion_23_2010:

=============================================================
Motion n°23 2010 : Motions groupées, Etpic 94 [hors statuts]
=============================================================



Argumentaire
============

Le congrès confédéral de la :term:`C.N.T` est le congrès des syndicats.

Il n’est ni un congrès d’unions régionales et ou de fédérations encore moins
de syndicats groupés.

Si les :term:`U.R` et les fédérations remplissent leur rôle respectivement
sur les plans géographiques et professionnels dans un espace temps différent
de celui du Congrès confédéral c’est parce que ce qui les relie est prévu
et défini dans les statuts de la Confédération.

A quelques reprises ces dernières années, quelques syndicats de la Confédération
ont décidé unilatéralement de présenter des motions de façon groupée.
Ces groupements ont été quelquefois régionaux, à d’autres occasions
des syndicats de régions différentes ont fait de même sur des bases non connues.
Dans notre union régionale, plusieurs syndicats ont fait de même à l’occasion
de notre congrès régional. Ceci nous suscite des réflexions à plusieurs niveaux.

Aucun texte jusqu’à présent «n’interdit» cette pratique. Mais les protagonistes
ne prennent pas soin de demander à l’ensemble de la confédération si cela est
possible à pratiquer.

Cela aurait été utile parce que ce faisant, ils bousculent l’égalité existante
entre tous les syndicats de la Confédération qui veut que chaque syndicat étudie
dans un timing établi par les syndicats eux-mêmes les points
à mettre aux ordres du jour et que le débat s’ouvre au même moment pour tout le
monde avec l’ensemble des syndicats de la Confédération.

En se groupant pour discuter « dans le dos de tous les autres syndicats », ils
commencent le Congrès avant tout le reste de la confédération rompant l’égalité
entre tous les syndicats. Ils semblent vouloir se dispenser d’un débat
avec tout le monde dans la recherche d’un consensus et d’une synthèse.

Pour nous cette pratique relève à vouloir fractionner la :term:`C.N.T` en tendances
«politiques» au sens politicien du terme avec des recherches de majorité avec les
difficultés que cela crée. Pour nous c’est un ferment de division et de mise à mal
de la :term:`C.N.T`.

Motion
------

Nous proposons donc de spécifier dans les règles de fonctionnement de la :term:`C.N.T`
que les motions aux congrès se présentent syndicat par syndicat.

Les alignements, ralliements, amendements, débats, synthèses et votes se font
au cours du congrès, pas avant. Si pour des raisons sociales et syndicales de
lutte, des syndicats d’aire géographiques ou industrielles différentes se contactent,
ils peuvent bien sûr le faire.


Vote indicatif motions groupées, Etpic 94 [hors statuts]
========================================================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°23          | 28         | 17       | 6          |  5                        |
| Etpic 94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`amendement_motion_23_2010_ETPRECI35`


.. warning:: Après vote indicatif, la motion ne sera pas discutée car celle-ci
   n'a pas été déposée dans les temps prévus par les statuts.


**Pas de vote définitif dessus.**
