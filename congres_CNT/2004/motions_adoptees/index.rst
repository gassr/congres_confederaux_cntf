

.. _motions_adoptéees_congres_CNT_2004_saint_denis:

========================================================
Motions adoptées au XXVIIIe Congrès CNT 2004 Saint-Denis
========================================================


.. toctree::
   :maxdepth: 2

   strategie_syndicale
   travail_revendications_salariales
   commission_administrative
   secretariat_international
   structuration_confederale
   internet_et_intranet
   bulletin_interieur
   presse_confederale
   statuts_confederaux
   tresorerie_confederale
