


.. _motion_17_2010:

=========================================================================================
Motion n°17 2010 : Composition des unions régionales, STIS 59/62 [points 2 et 3 adoptés]
=========================================================================================
  


Préalable
=========

Modification de la motion p.15 du recueil de motions confédérales et règles
organiques, Chap: règles organiques, Titre: Unions Régionales, :ref:`motion issue
du 24e congrès confédéral du 6 et 7 février 1993 <motion_structuration_confederale_cnt_congres_1993_paris>`.


Argumentaire
============

Actuellement, l'union régionale Nord-Pas-de-Calais ne devrait pas exister si
l'on suit les statuts. Nous avons fait le choix d'un syndicat régional (le SSEC),
et d'un syndicat interco (le STIS). Ce dernier pourrait se scinder (cela s'est déjà fait),
mais cela poserait tout un tas de problèmes compte tenu de l'état de développement réel de la
:term:`C.N.T` mais aussi en terme stratégique et juridique (une scission du STIS
supprimerait l'ancienneté acquise pour prétendre à la désignation de RSS).

D'autres exemples confédéraux montrent une inadéquation des statuts : en région Centre,
il y a un interco régional, est-il normal qu'il ne puisse participer au processus
démocratique entre les congrès ? Idem pour la Haute-Normandie.

La motion en vigueur ne prend pas en compte non plus l'extrême diversité qui existe
entre les régions : certaines sont composées de deux départements et d'autres
de presque dix. Pour prendre l'exemple du Nord-Pas-de-Calais, il est illusoire
de penser qu'il y aura une multiplication de syndicats éparpillés sans réalité
forte l'appuyant, alors que tout est à moins de 2h de Lille.

Motion
------

1. Dans la motion, le passage “Les :term:`U.R` sont constituées par au moins
   trois syndicats” est remplacée par : “Les :term:`U.R` sont composées par au
   moins deux syndicats ou au moins un syndicat régional”
2. La mention “dans trois villes différentes” est supprimée ;
3. On y ajoute : “Les syndicats isolés dans les régions non constituées en :term:`U.R`
   sont rattachés à l'une des :term:`U.R` les plus proches.”


Discussion
==========

SSEC 59-62
	Entre chaque congrès participation au sein des CCN uniquement des UR. Des
	syndicats sont exclus du fonctionnement démocratique de la confédération
	entre chaque congrès. Nous on est concerné mais on n'est pas les seuls.

STEA
	ok avec le fond de la motion. Ok avec le fait de se rattacher à une UR proche.
	Problème technique donc suppression du point 1

CCS RP
	pour les points 2 et 3. On propose un vote séparé. Quasi pareil que le STEA.


Vote indicatif
==============

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°17          | 25         | 26       | 13         |  3                        |
| STIS 59/62           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


SIPM
    Dans l'esprit d'une UR il nous semblait que 3 syndicats étaient un minimum.

STEA
    ok avec l'intervention précédente.

STE75
	une UR avec deux syndicats ça peut ne regrouper que 3 personnes : le ou la
	secrétaire région et deux mandatés ! C'est peu. A la CNT c'est nous qui
	posons nos règles donc on n'est pas obligé de conserver les régions
	administratives de Giscard D'Estaing.

USI 32
	Si on vote pour le point 1, ça ouvre la possibilité de créer deux UR par
	région et les conséquences pourraient être catastrophiques !
	Très favorable à rester sur 3 syndicats donc.

CCS RP
	C'est effectivement toutes ces raisons qui nous poussent à proposer un vote
	séparé. Si on fait des aménagements sur les points 2 et 3 on pourra aider
	les syndicats qui n'ont pas d'UR.

CCS 44
	Idem 75, on n'est pas obligé de se constituer sur les régions administratives
	d'ailleurs nous-mêmes on déborde.

Educ 86
	Quand y'a pas d'UR, les syndicats sont rattachés à l'UR la plus proche,
	c'est le cas pour les syndicats du limousin qui viennent de rallier l'UR
	Poitou-Charentes. (Poitou-charentes-limousin)

SSEC 59-62
	la problématique concerne trois régions (région centre + région Auvergne
	+ la notre) donc ça pose problème que des grosses régions comme ça ne soient
	pas représentées en CCN.


La motion n°17 est votée en trois temps, distinctement sur les trois points
===========================================================================

.. _motion_17_2010_vote_point_1_rejete:

Vote Point 1 Motion 17 [rejeté]
-------------------------------

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°17          | 19         | 40       | 8          |  0                        |
| STIS 59/62 point1    |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**le point 1 de la motion est rejeté**

.. _motion_17_2010_vote_points_2_3_adopte:

Votes des points 2 et 3 de la motion 17 composition des unions régionales, STIS 59/62  [adoptée]
------------------------------------------------------------------------------------------------


Vote du point 2 de la motion 17 composition des unions régionales, STIS 59/62  [adoptée]
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

STE 57
    voter pour car c'est une réalité de certains syndicats et qu'il faut y
    faire face.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°17          | 43         | 16       | 5          |  0                        |
| STIS 59/62 point2    |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**point 2 de la motion 17 est adopté.**


Vote point 3  de la motion 17 composition des unions régionales, STIS 59/62  [adoptée]
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


STEA
    Est-ce que c'est le syndicat qui choisit l'UR à laquelle il adhère ?

CCS RP
    syndicat isolé choisit l'UR la plus proche.

STE93
	contre les deux premiers points et pour le dernier. Il faut être souple et
	faire un bilan détaillé à chaque CCN.

STE75
	C'est ce qui existe déjà dans les statuts vu que les syndicats du 44 sont
	rattachés à la Bretagne par exemple.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°17          | 55         | 5        | 4          |  1                        |
| STIS 59/62 point3    |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

**point 3 de la motion 17 adopté**

.. seealso::

   - :ref:`amendement_motion_17_2010_stea`
   - :ref:`contre_motion_17_2010_culture_rp`
