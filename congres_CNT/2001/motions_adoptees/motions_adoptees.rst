

.. _motions_adoptéees_congres_CNT_2001_toulouse:

=================================================
Motions adoptées au XXVIIe Congrès CNT 1998 Paris
=================================================

.. toctree::
   :maxdepth: 2

   strategie_syndicale
   international
   antimilitarisme
   relations_medias
   bureau_confederal
   outils_confederaux/outils_confederaux
   structuration_confederale
