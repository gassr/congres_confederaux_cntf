

.. _contre_motion_53_2010_ess34:

===========================================
Contre-motion à la motion N°53 (ess34)
===========================================


Un «Secteur vidéo» comme un outil de propagande syndicale et de création vidéo
« Confédéral » est créé.

Lecongrès mandate des camarades pour le secteur vidéo, qui feront partie du
Secrétariat à la Propagande. Le «Secteur vidéo » est ouvert à tous les militants
de la confédération qui désirent participer au développement de la structure
et à son activité.

Ce sont les membres du « Secteur Vidéo » qui choisissent le mandaté confédéral
pour une durée de 2 ans, que le congrès valide. Le « Secteur Vidéo » labellise
la production vidéo qui reste sous la responsabilité des membres.

Les taches attribuées au mandaté confédéral « Secteur Vidéo »
-------------------------------------------------------------

Le mandaté fait la demande au BC/CA pour l'obtention administratif du label.

En cas de litige pour l'attribution du label à l'intérieur du « Secteur vidéo »,
le mandaté explique la situation au Bureau confédéral et à la Commission
administrative et / ou au CCN, qui prennent une décision et en rendent compte
au mandaté «Secteur vidéo ».

Le mandaté informe le « Secteur Vidéo » de la décision argumentée, communique
l'information de production vidéo labellisé par le Bulletin Intérieur et la
liste syndicats, le rapport d'activité confédéral.

Administration de l'outil internet et contact externe.

Diffuse l'information du « Secteur Vidéo » par : la liste interne de la commission
confédérale "Secteur vidéo" de la CNT : liste.secteur-video@cnt-f.org



+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motions n°53    |            |          |            |                           |
| ess 34                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_53_2010`
