
.. index::
   pair: Motion 7 ; 2016

.. _motion_7_2016_sante_social_lorraine:

==================================================================================
Motion n°7 2016 : Comptabilité Confédérale (Santé Social CT Lorraine) [adoptée]
==================================================================================



Argumentaire
============

Dans le cadre de la candidature récente de a CNT à l'élection TPE, la
confédération a été rattrapée par l'obligation de transparence
financière, qui impose à toute structure syndicale une publication
annuelle de ses comptes depuis l'année 2012.

Bien que le Bureau Confédéral ait pu, dans l’urgence, réaliser un bilan
comptable que pour l'année 2015, la transparence financière est l'un des
nouveaux critères de la représentativité, et la CNT n'est donc toujours
pas à l'abri d'une contestation lorsqu'elle participe à l'élection TPE
ou lorsqu'elle implante une section syndicale qui gagne la représentativité,
que cela soit a niveau local ou confédéral.
Réaliser la comptabilité de  la confédération de façon annuelle, tout
comme celles des structures locales apparaît donc comme une nécessite
pour protéger notre implantation syndicale à l'avenir. Finalement, il
est difficilement concevable de demander à la Trésorerie Confédérale de
réaliser cette tâche compte tenu du travail qu'elle a déjà à sa charge.


Motion
=======

1) Le congrès confédéral prend acte de l'obligation légale de publication
   annuelle des comptes de la CNT depuis 2012.
2) La confédération se dote d'un.e mandaté.e à la Comptabilité dont le
   mandat sera :

   - de réaliser, en lien avec la Trésorerie Confédérale, l'écriture
     comptable des opérations des opérations de la trésorerie afin de
     publier, à chaque fin d'exercice, le bilan comptable de la CNT
   - de réaliser a posteriori, et d'ici le prochain congrès, les bilans
     comptables de la confédération pour les années 2012, 2013 et 2014.



Contre motions
==============


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°7 2016

       :ref:`Santé social CT Lorraine <sest_lorraine>`
     - 37
     - 7
     - 4
     - 3
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
