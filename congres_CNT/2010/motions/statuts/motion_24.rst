
.. _motion_24_2010:

==============================================================
Motion n°24 2010 Disparition du vote par procuration (STE 75)
==============================================================

.. seealso::

   - :ref:`motions_ste75`
   - :ref:`article_16_statuts_CNT_2010`




Motion reprise par STE75, 2012
==============================

- :ref:`motion_1_2012`


Préambule
=========

Attention ! Cette motion modifie les statuts confédéraux (:ref:`titre IV – art. 16 <article_16_statuts_CNT_2010>` :

*Chaque délégué ne peut représenter exceptionnellement que trois syndicats au maximum*

et n’a pas été déposée six mois avant le congrès ainsi que le veulent les statuts
confédéraux (Titre VI « Dispositions diverses » : « Modification des statuts » Art. 27 :
«Les présents statuts ne peuvent être modifiés que par un congrès ordinaire, à condition que
le texte des modifications ait été porté à la connaissance des syndicats six mois
avant la date du congrès.»)

.. _`titre IV – art. 16`:

Argumentaire
============

Lors des congrès de la CNT, il arrive qu'un syndicat donne procuration à un autre pour
transmettre ses décisions d'AG concernant les motions proposées.

Nous avons conscience qu'il n'est pas toujours facile pour certains syndicats d'envoyer des mandatés à chaque
congrès. Cependant, si les décisions se prennent lors de rencontres entre personnes mandatées, c'est pour qu'elles
puissent être l'aboutissement d'une réflexion commune et constructive. Les amendements en sont l'expression.
Un mandat écrit, sans représentant du syndicat qui ait assisté régulièrement à ses AG et qui puisse y rendre des
comptes à son retour, ne permet pas cette richesse.

L'organisation du congrès doit respecter les horaires prévus, afin de permettre aux mandatés tributaires
d'impératifs personnels de rester jusqu'au bout.

Nous pensons que dans l'expression "un syndicat, une voix", le mot voix doit être entendu dans les deux sens:
un vote et une parole.

Motion
======

Lors des congrès confédéraux, chaque syndicat, pour participer aux votes et donc aux débats qui les précèdent,
doit envoyer au moins un mandaté. Il ne peut plus donner procuration à un autre syndicat.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°24          |            |          |            |                           |
| STE 75               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Après vote indicatif, la motion ne sera pas discutée car celle-ci n'a pas été
déposée dans les :ref:`temps prévus par les statuts <temps_prevu_congres>`.

**Pas de vote définitif dessus**.
