
.. _motion_18_2012:

==============================================================================================================================================
Motion n°18 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Permanents syndicaux  (ETPIC30)
==============================================================================================================================================
  
.. seealso::

   - :ref:`etpic_30_pub`



Motion
======

Attachée à son autonomie, à la démocratie directe, au fédéralisme libertaire et
à sa vocation révolutionnaire, la CNT est autogestionnaire et en promeut la
généralisation à l’ensemble de la société.

Elle privilégie l’effort militant et l’action directe et collective.

L’administration, la direction et la représentation syndicales et politiques
de la CNT sont garanties aux travailleurs. La CNT n’entend pas ici confier
ces mandats à un corps de professionnels, soient-ils titrés ``syndicaux``.

À tous les niveaux de l’organisation, **la CNT refuse dès lors tous recours aux
permanents syndicaux en ce sens que l’ensemble de ses mandaté.e.s syndicaux ne
sont ni salarié.e.s, ni déchargé.e.s de service**.

Les travailleurs/ses syndiqué.e.s en charge de ces mandats (confédéraux, fédéraux,
régionaux, d’Unions locales ou de syndicats) demeurent adhérent.e.s à leurs syndicats.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°18          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
