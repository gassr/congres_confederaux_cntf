
.. _motion_47_2012:
.. _motion_47_2012_chimie_bzh:

===========================================================================
Motion n°47 2012: Circulation des informations confédérales (Chimie Bzh)
===========================================================================
  

.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`




Argumentaire
============

D’expérience il est toujours plus facile de commenter et de discuter des
informations issues de la vie confédérale en lisant en réunion une circulaire
papier et en se l’échangeant, se la faisant circuler.

Ces derniers temps les circulaires sont volumineuses et trop espacées dans le
temps pour convenablement permettre aux syndicats de participer et de suivre
la vie confédérale.

C’est pourquoi, idéalement, une circulaire mensuelle, même courte, est la
bienvenue.

Il en est de même pour le Bulletin intérieur dont la diffusion s’est faite
exclusivement par voie électronique ce que nous pensons être une erreur.

La diffusion par voie électronique est certes écologique et moins coûteuse
(quoique des syndicats impriment les circulaires pour leur usage, ce qui reste
coûteux) mais elle ne permet pas à tous et toutes nos adhérentEs de se
l’approprier. En efet, bon nombre de camarades ne disposent pas des accès au
net et des outils informatiques permettant une réactivité aux nombreuses
infos et mails qui circulent sur l’intranet ou sur les multiples listes.

L’usage dans notre Confédération a toujours été la réception des outils
confédéraux sur support papier.

Motion
======

Les circulaires confédérales et autres outils (BI, etc) sont adressés aux
syndicats par voie postale en version papier de manière distinctive
(chaque syndicat recoit son enveloppe même si il s’agit de la même adresse
pour plusieurs syndicats).

L’impression en papier recyclé est à privilégier.

Toutefois, les syndicats habitués et fonctionnant généralement avec le net
peuvent demander explicitement au BC de ne plus recevoir la circulaire papier
mais cette demande doit être écrite et à renouveler à chaque congrès.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°47          |            |          |            |                           |
| Chime-Bzh            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
============

- :ref:`amendement_motion_47_2012_sse31`
