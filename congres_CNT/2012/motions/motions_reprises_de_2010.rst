

.. _motions_2010_2012:

=============================================================
Motions reprises du Congrès CNT de Saint-Etienne 2010 en 2012
=============================================================


- :ref:`motion_4_2010`
- :ref:`motions_5_6_2010`
- :ref:`motion_15_2010`
- :ref:`motion_30_2010`
- :ref:`motion_24_2010`
- :ref:`motions_34_35_2010`
- :ref:`motion_45_2010`
- :ref:`motion_48_2010`
