

.. _motions_adoptéees_congres_CNT_2008_lille:

========================================================
Motions adoptées au XIXXe Congrès CNT 2008 Lille
========================================================


.. toctree::
   :maxdepth: 2

   strategie_syndicale
   international
   travailleurs_immigres
   economie
   precarite_et_reduction_temps_de_travail
   outils_confederaux/motion_2008
   structuration_confederale
   presse_confederale
   tresorerie_confederale
