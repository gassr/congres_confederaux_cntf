.. index::
   pair: Motion 24 ; 2021

.. _motion_24_2021_ste33_cnt42:

================================================================================================================================================
Motion **n°24** 2021: **Gestion interne des violences patriarcales** version unifiée par **STE 33 + CNT42**
================================================================================================================================================

Motion de synthèse:

- :ref:`motion_synthese_24_2021_ste38_stp_67`


Autres motions STE33, CNT42

    - :ref:`syndicats:ste33_motions_congres`
    - :ref:`rhone_alpes:cnt42_motions_congres`


Argumentaire
=============

Pourquoi proposer un protocole de gestion en interne des violences patriarcales ?

Les syndicats n’échappent pas plus au patriarcat qu’ils n’échappent au capitalisme.

Alors que ses statuts appellent à *la transformation totale de la société actuelle*,
à l’émancipation et à la libération des travailleurs/euses par les transformations
sociales et économiques, à la solidarité entre les travailleurs/euses la CNT ne s
e donne toujours pas les moyens de combattre le patriarcat qui est pourtant lui
aussi un obstacle à la réalisation d’une société égalitaire.
Ainsi si La CNT fait le choix de ne pas syndiquer les auteur·es de violences
capitalistes, elle n’a pas d’outils pour gérer en interne la présence d’auteur
de violences patriarcales.

Face à ce manque, chaque fois qu’un cénétiste est accusé de viol et/ou agression
sexuelle, la CNT se trouve démunie pour gérer la situation.

La proposition de protocole ci-après vise à:

- Éviter que la gestion de conflit sur les violences patriarcales ne se fasse
  dans la passion hors du champs politique.
- Couper court à la rumeur et se baser de suite sur des faits.
- Proposer une aide rationnelle et politique à la CNT.
- Proposer un soutien et la solidarité aux victimes de violences patriarcales
  comme on propose un soutien et la solidarité aux victimes du capitalisme.
- Éviter que les scissions et les départs ne soient le seul mode de gestion de
  ces conflits.

La CNT est toujours dans la recherche d’alternatives à ce que propose la société
actuelle et est très attachée à l’autogestion.

Par ce protocole, elle reste fidèle à ses deux principes en se dotant de ses
propres outils de gestion des violences patriarcales.

De même que chaque camarade a sa place dans la CNT et apporte son expertise dans
la pratique syndicale (par exemple compétences juridiques), nous aimerions par
ce protocole que la CNT puisse profiter de l’expertise féministe de certaines
camarades.

Au fil des années la CNT a acquis une expertise dans la lutte contre le capitalisme.
Aujourd’hui elle se fait confiance dans ce qu’elle peut produire comme outils
efficaces dans ses luttes. Tout cela ne s’est pas fait du jour au lendemain.
Le protocole vise à acquérir la maturité politique pour gérer, évaluer, analyser
l’existence de violences patriarcales. Là aussi faisons nous confiance !

Motion
=======

Toute personne reconnue coupable de viol/violences patriarcales n’a pas
sa place à la CNT

Protocole
===========

Cette proposition de protocole vise à établir des outils et une procédure à suivre
systématiquement dès qu'une accusation de viol et/ou de faits de violence patriarcale
est portée à la connaissance de membres de la CNT contre un·e de ses membres.

- Toute personne accusant de viol et/ ou agression sexuelle un membre de la CNT
  est considérée comme une victime et bénéficie de tous les traitements et égards
  dus à ce statut (aide psychologique, juridique, financière en cas d’incapacité
  au travail) et ce le temps nécessaire à l’instruction de l’affaire et sans
  jugement au préalable de son issue.
- La CNT met en place une liste de personnes référent·es jugé·es compétent·es
  sur les questions des violences patriarcales. Ces référent·es sont désigné·es
  par leur syndicat et / ou Union Régionale. Cette liste est mise à disposition
  de tout.es les adhérent·es de la CNT et actualisée quand nécessaire.
- La victime peut alors saisir (elle-même ou par l’intermédiaire d’une tierce
  personne) une personne parmi cette liste pour porter accusation.
- Les référent·es contacté·es sont alors chargé·es de mettre en place une
  commission extraordinaire, non mixte ou mixte suivant le choix de la victime.

Les membres de cette commission extraordinaire sont choisi·es parmi la liste
des référent·es.
Cette commission extraordinaire a tout mandat pour instruire l’affaire, décider
quand lever l'anonymat et diffuser le rendu de ses travaux à la Confédération.
Elle n’est nommée que pour le temps de l’instruction.
La CNT, à travers le BC, doit se donner les moyens (financiers notamment et/ou
d'hébergement pour d'éventuels déplacement, voire informatique à travers un espace
d'échange sécurisé) du bon fonctionnement et déroulement de la procédure.

- Les membres du syndicat et de l’UD de l’accusé·e aussi bien que de la victime
  ne peuvent en aucun cas faire partie de la commission extraordinaire pour éviter
  tout risque de partialité etde pressions extérieures.
- Dés sa constitution, la commission extraordinaire informe le syndicat local de
  l’accusé·e des accusations portées contre lui / elle (que la victime soit elle-même
  adhérente de la CNT ou pas).
  À partir de cette information, par principe de précaution, l’accusé·e est
  suspendu·e de la CNT, et ce pendant tout le temps de l’instruction.
  Le syndicat local d’appartenance de l’accusé·e est responsable de l’application
  de la suspension. Il doit être clair que cette suspension ne préjuge pas de
  la culpabilité.
- La commission extraordinaire a pour mission de recueillir la parole de la
  victime et tout autre témoignage qu’elle jugera nécessaire. Une autre de ses
  missions est d’aider la victime à porter plainte, et ce faisant de lui
  permettre de sortir d’un silence préjudiciable pour elle à court, moyen et
  long terme.
- Quelle que soit la décision de la victime de saisir ou non la justice, elle
  ne pourra en aucun cas être jugée sur sa décision par la CNT. En cas de refus
  de porter plainte, il ne pourra en être tenu rigueur à la victime et cela ne
  vaudra pas non plus mise en doute de son accusation.
- Une fois la parole de la victime recueillie, la commission extraordinaire rend
  ses conclusions aux référent·es. Les référent·es et la commission extraordinaire
  se réunissent alors et prennent ensemble une décision qui est remise à tous
  les syndicats de la Confédération.
- Il ne peut en aucun cas être laissé à la responsabilité du syndicat et de
  l’UD de l’accusé·e de définir la sanction à porter contre l’accusé·e.

- Selon les conclusions de la commission extraordinaire, l’accusé·e pourra être
  exclu·e de la CNT.
  La CNT communique alors publiquement sa décision d'exclure un·e camarade.
- Dans tous les cas, le BC s’engage à donner les moyens à la commission
  extraordinaire d’assurer un accompagnement aux protagonistes, s’ils le
  souhaitent. Sans interférer avec le déroulement de la procédure ni évidemment
  avec ses conclusions. »



Amendements
===========

- :ref:`amendement_motion_24_2021_sest_lorraine`
- :ref:`amendements_motion_24_2021_interpro31`

Contre motions
================

- :ref:`contre_motion_24_2021_tasra`
- :ref:`contre_motion_12_24_2021_cnt30`
- :ref:`contre_motion_24_2021_educ38`
- :ref:`contre_motion_24_2021_interpro07`
- :ref:`contre_motion_24_2021_etpics94`
- :ref:`contre_motion_24_2021_stp67`
- :ref:`contre_motion_12_24_27_2021_ste75`
- :ref:`contre_motion_24_2021_simrp`


Votes
======


+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Motion n°24 2021 STE33/CNT42  |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
