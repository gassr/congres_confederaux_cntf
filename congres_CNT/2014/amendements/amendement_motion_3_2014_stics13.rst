
.. _amendement_motion_3_2014_stics13:

================================================
Amendement à la motion n°3 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_3_2014_sinr44`






Argumentaire
=============

Les délais du congrès sont rarement respectés et les transports des mandaté-es
ont eux, bien souvent, des horaires fixes.

Amendement
==========

(annule et remplace la dernière phrase)
Cependant, un syndicat qui quitte le congrès en cours peut donner procuration
à un autre syndicat, à condition que ce dernier ne détienne pas déjà une
procuration.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°3 2014 <motion_3_2014_sinr44>`

       :ref:`STICS13 <stics13>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
