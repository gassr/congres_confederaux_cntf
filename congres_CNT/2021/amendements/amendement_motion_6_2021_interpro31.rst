
.. _amendement_motion_6_2021_interpro31:

=====================================================================================================================================================
Amendement N°3 à la motion n°6 2021 **De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale** par **Interpro31**
=====================================================================================================================================================

- :ref:`motion_6_2021_etpic30`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


Argumentaire 
================

Le texte est plus un pamphlet syndical qu’une motion.

Le mode d’emploi du Congrès de la CNT stipule : une bonne motion
est un texte concis qui définit avec précision son objectif et les modalités
de sa mise en application. »

Amendement 
============

Reformuler l’ensemble de la motion en : La CNT affirme son engagement en
faveur d’une écologie radicale et sociale. En ce sens, la CNT affirme
la nécessité d'aller vers la décroissance.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion      |            |          |            |                           |
| n°6 Interpro31              |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
