
.. index::
   pair: Transports ; Shenker-Joyau Nord
   ! Shenker-Joyau Nord


.. _syndicat_entreprise_shenker_joyau_nord:

===========================================
Section transport Shenker-Joyau STIS59
===========================================

.. seealso::

   - http://www.cnt-f.org/59-62/2010/11/t-shirt-en-soutien-au-stis-59/
   - http://www.schenker.fr/
   - http://schenker-joyau.fr
   - :ref:`stis59_pub`
   - :ref:`activites_STIS59_2011`
