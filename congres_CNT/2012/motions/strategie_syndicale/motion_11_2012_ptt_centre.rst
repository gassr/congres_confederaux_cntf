
.. _motion_11_2012:
.. _motion_11_2012_ptt_centre:

=======================================================================================================================================================
Motion n°11 2012: Pour un syndicalisme fédéraliste, révolutionnaire et anarcho-syndicaliste (motion dite de « règles organiques », Art. 29) PTT Centre
=======================================================================================================================================================
  
.. seealso::

   - :ref:`ptt_centre`
   - :ref:`article_29_statuts_CNT_2010`
   - :ref:`contre_motion_11_etpic30`



Argumentaire
=============

La Confédération nationale du travail garantit l'autonomie des syndicats dont
elle est l’émanation et réaffirme le fédéralisme comme pacte fondateur de notre
syndicalisme à l’opposé d’une vision centraliste « démocratique » qui
n’aboutirait qu’à l’uniformisation et l'appauvrissement des expériences, des
pratiques et de l’autonomie de ses syndicats.

La Confédération nationale du travail réaffirme son attachement indéfectible aux
principes fondateurs du syndicalisme révolutionnaire et de l’anarcho-syndicalisme.

Pour cela la Confédération nationale du travail entend que les syndicats
puissent, pour eux, décider d’expérimenter et de mener à bien les orientations
tactiques qu’ils ont choisi :

- tant que ces décisions d’orientations ne remettent pas en cause les principes
  fondateurs du syndicalisme révolutionnaire et de l’anarcho-syndicalisme,
- tant que ces décisions ne les conduisent pas à critiquer publiquement ce que
  d’autres syndicats ou unions de syndicats CNT ont choisi pour eux mêmes.

Ainsi si une Union locale, une Union régionale, une Fédération de syndicats ou
la Confédération décide majoritairement d’une orientation tactique et des prises
de décision en rapport avec cette orientation, un syndicat, une Union locale,
une Union régionale, une Fédération ou la Confédération ne pourra mettre en
œuvre une orientation diférente que pour ce qui la concerne.

Chaque syndicat ou groupement de syndicats aura à cœur de faire le bilan pour
lui même et pour l’ensemble de la Confédération de ses prises de décisions en
ce qui concerne la stratégie syndicale choisie, d’autant que ses prises de
décision propres ne sont pas celles prises majoritairement dans les congrès
des autres groupements de syndicats.

La richesse de la Confédération nationale du travail se verra ainsi augmentée des
échanges et des bilans faits des diférentes expériences confrontées et mises
en commun.

Ainsi les Unions locales, régionales, les Fédérations et la Confédération, outils
coordonnant l'action des syndicats peuvent elles adopter des positions pour ce qui les
concerne en tant que telles et les revendiquer à son échelon de compétence. Chaque
syndicat ou groupement de syndicats garde son autonomie de décisions pour, et
uniquement, ce qui le concerne.

L'adoption de cette motion par le Congrès confédéral de la CNT en 2012 entraine
l’amendement de :ref:`l’article 29 <article_29_statuts_CNT_2010>` des statuts
qui affirmera alors que « l’autonomie de chaque structure consiste en la liberté
de pouvoir déroger pour elles, dans le respect des principes fondateurs du
syndicalisme révolutionnaire et de l’anarcho-syndicalisme, aux
décisions tactiques et stratégiques syndicales prises majoritairement par d’autres
structures de la Confédération et sans remettre publiquement en cause le caractère
majoritaire des ces décisions».

Ainsi la Confédération s’enrichirait du pluralisme de ses expériences et
cette richesse à l’opposé des efets néfastes du centralisme et du monolithisme
qui ont partout tant desservi le mouvement syndical. Ce pacte de fonctionnement
véritablement fédéraliste verrait sans doute se dissoudre bien des conflits
finalement mineurs qui peuvent déchirer notre Confédération alors même que pour
l’essentiel nous avons tant à mettre en commun.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°11          |            |          |            |                           |
| PTT centre           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Contre motions
==============

.. seealso::

   - :ref:`contre_motion_11_etpic30`
