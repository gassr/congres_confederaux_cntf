
.. index::
   pair: 34; Motion 2012

.. _motion_34_2012:

======================================================================================
Motion n°34 2012: Création d'un Institut de formation confédéral  (ETPICS 57 Nord Sud)
======================================================================================
  

CNT (motion reprise et modifiée du STE 93 lors du congrès de Saint-Étienne)

.. seealso::

   - :ref:`etpics57`
   - :ref:`motions_etpics57_2012`
   - :ref:`motion_35_2012`





Motion reprise de STE93, 2010
=============================

- :ref:`motions_34_35_2010`

Argumentaire
============

La nécessité de former les adhérent-e-s et militant-e-s n'est pas nouvelle.

Ces dernières années, des syndicats CNT, des régions ont organisé des formations.
Nous pensons qu'il est maintenant possible de proposer la création d'un
« Institut de formation confédéral - CNT ». Celui-ci aurait pour mission de
recenser les besoins des syndicats, de proposer un « catalogue » de formations,
des animateurs, animatrices, voire d'assumer la publication de brochures.

Deux axes de travail peuvent déjà se mener en parallèle :

1er axe. Formations à proposer aux adhérents
--------------------------------------------

hors du temps de travail (en soirées, week-ends) qui pourraient s'organiser en
régions et/ou au niveau national avec un planning à l'année. Resterait à chifrer
le coût des déplacements des participants et/ou animateurs.

2e axe. Le plus important, la formations sur le temps de travail
-----------------------------------------------------------------

Stages de 1, 2 ou 5 jours, sachant que les salariés ont le droit à 12 jours
par an, 18 s'ils assument des mandats de représentations ou des responsabilités
syndicales. Le tout financé sur le 0,8 pour 1000 ou en Droit individuel à la
formation. Cela permettrait aux diférents syndicats de former leurs militants
aux « frais de la princesse ».

Toutefois ce ne sont pas des subventions mais de l'argent socialisé pour la
formation syndicale.

Cette option nous obligera à formaliser un partenariat avec une association
d'éducation populaire agréée par l'État.

En effet, aujourd'hui, seules les confédérations syndicales dites
représentatives ont pu créer leurs instituts de formation et ainsi permettre à
leurs adhérents du public comme du privé de prendre des congés formations sur
le temps de travail.

Pour information, Solidaires a du se lier à l'association d'éducation populaire
« Culture et liberté » afin de faire fonctionner son « Centre d'étude et de
formation interprofessionnelle » (CEFI - Solidaires)

Ces derniers mois, des militants de la CNT éducation avaient pris contact avec
« Culture et liberté » pour envisager un partenariat. Il serait donc possible
de le formaliser sous la condition que des militants de la CNT adhèrent
à « Culture et liberté » (20 euros l'an).

Il faudrait également que certains de nos « formateurs » fassent des stages
payants avec « Culture et liberté » (1 ou 2 peuvent suffir ; prévoir 100 euros
la journée de stage). À noter que ces infos datent.

Il est évident qui si le congrès accepte le principe d'un partenariat avec
cette association, la Confédération sera informée des engagements à
prendre ou non.

Motion
=======

Le congrès confédéral mandate une commission pour mettre en place un
« Institut de formation confédéral - CNT ».

Il travaillera pour mettre en place des sessions décentralisées dans les UR ou
UD sur les thèmes d'actualités : retraites, salaires, contrats à durée
déterminée... Mais aussi : histoire sociale, solidarité internationale,
représentation du personnel et/ou du syndicat au sein de l'entreprise...


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°34          |            |          |            |                           |
| ETPICS57             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Conclusion
===========

.. seealso::

   - :ref:`motions_34_35_36_xxxii`
