
.. _congres_CNT_2012_amendements:

=======================================
Amendements du XXXIIe congrès CNT 2012
=======================================

.. toctree::
   :maxdepth: 2

   amendement_motion_2_2012_stea
   amendement_motion_2_2012_etpics57
   amendement_motion_5_2012_interpro31
   amendement_motion_7_2012_stp72
   amendement_motion_9_2012_interco66
   amendement_motion_17_2012_etpics57
   amendement_motion_17_2012_stas_ra
   amendement_motion_21_2012_etpic30
   amendement_motion_22_2012_sinr44
   amendement_motion_25_2012_sanso_rp
   amendement_motion_26_2012_sse31
   amendement_motion_30_2012_stas_ra
   amendement_motion_32_2012_etpics57
   amendement_motion_32_2012_interpro31
   amendement_motion_37_2012_interco86
   amendement_motion_40_2012_stea
   amendement_motion_43_2012_stgl_rp
   amendement_motion_44_2012_sinr44
   amendement_motion_47_2012_sse31
   amendement_motion_52_2012_sinr44
   amendement_motion_55_2012_interpro31
   amendement_motion_59_2012_etpics57
