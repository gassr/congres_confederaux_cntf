
.. index::
   pair: Stratégie; Représentativité et stratégie syndicale

.. _motion_synthese_droit_syndical_2008_lille:
.. _motion_representativite_2008:

============================================================================
Motion de synthèse "Représentativité et stratégie syndicale" [adoptée, 2008]
============================================================================

Motion de synthèse proposée par une commission de Congrès.


La C.N.T se déclare fermement opposée aux **accords syndicaux** et leur
traduction législative en août 2008 réformant la représentativité syndicale
dans le secteur privé.

Elle invite ses syndicats à développer une critique et une dénonciation
radicale de ces dispositions venant renforcer la cogestion
patronat-Etat-syndicats.

Elle mènera cette contestation avec d’autres syndicats si nécessaire.
La C.N.T revendique le fait que les droits syndicaux ne soient liés ni aux
résultats aux élections professionnelles, ni même à la participation à
celles-ci.


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 28         | 10       | 6          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


.. _motion_strategie_2008:

Stratégie syndicale et représentativité, Lille 2008
===================================================

.. seealso::

   - :ref:`motion_36_2010`


La C.N.T réaffirme que c’est la section syndicale et son syndicat d’appartenance
qui sont les moteurs de l’action syndicale dans l’entreprise. Dans le respect
de ses pratiques et principes ASSR, les syndicats et sections syndicales
cherchent à conserver ou à conquérir les droits syndicaux dans les lieux de
travail.

Cela signifie que chaque syndicat ou section syndicale s’il estime que cela
peut être utile à son développement cherche à obtenir la représentativité
syndicale, dans le cadre des possibilités pratiques validées en congrès
confédéral, tout en s’assurant que ses pratiques lui évitent de tomber dans
le piège de la cogestion, ainsi que **l’illusion qu’une réelle force syndicale
puisse être issue d’une quelconque élection**.

La participation à des élections ne peut se justifier que par le fait de
vouloir protéger un camarade d’une répression exercée par l’employeur ou par
la volonté de conserver et d’acquérir les droits que la C.N.T juge essentiels:

- panneau syndical,
- visite des locaux et services
- organisation d’heures d’informations syndicales
- appel à la grève
- diffusion de tracts
- etc.

Le fait de siéger dans les instances paritaires ou de cogestion ne peut en
revanche être un motif de participation à des élections professionnelles.

La C.N.T rappelle que la liberté des syndicats est également de ne pas se
présenter aux élections professionnelles.

La C.N.T ne limite pas sa lutte syndicale au cadre imposé par l’Etat et le
patronat : elle rappelle que **l’action directe collective et sans délégation
de pouvoir reste la principale arme des travailleurs.**


Discussion
==========

**Interco 54**
    Motion de synthèse réalisée sur une base écrite, mais dans les syndicats, les gens ont pu être pris de court, vu
    que la loi est passée en Aout... Proposition concrète: discussion autour de ce texte sans passer au vote directement. Il faut
    peut-être se laisser le temps de la discussion. Il est possible que les syndicats qui avaient une farouche opposition aient été
    pris de court...

**SII RP**
    point qui pose problème dans la partie 2, demande de précision, paragraphe 2, secteur privé... Notamment sur la
    décision de se présenter ou non...est ce le rôle de l'UR? Que vient faire l'UR là dedans...


**Réponse de la commission**
    position de la CNT avant ce congrès, on ne voulait pas donner une liberté totale aux
    syndicats... recherche de solution entre la liberté totale et le contrôle absolu...on était là pour faire une synthèse, on n'avait
    pas à juger...

**SIPM RP**
    Autant avoir la discussion et avancer, la CNT doit prendre une position là
    dessus...ne nous répétons pas continuellement.
    Deux heures de débats la dessus, sur ce thème discuté depuis longtemps dans
    les syndicats, débats extrêmement variés, complémentaires.
    Cette commission était ouverte à tout le monde, si on ne vote pas ce texte,
    c'est aberrant...

**CS RP**
    Le travail de la commission n'est pas un argument. Sur le fait de différer
    le vote, on ne va pas remettre deux ans pour prendre une décision...
    Pour avoir été formé sur ces nouveaux textes et avoir rencontré des syndicats
    différents, j'ai remarqué que tous ceux qui avaient reprochés les textes
    avaient des positions commune à cette motion...

**Educ 57**
    On tient à préciser qu'il y a 11 syndicats participants à cette synthèse, qui
    va plus loin que la motion d'Agen...
    les interrogations sur la question du contrôle ont toujours été présentes
    dans nos travaux.
    Des camarades se sont sérieusement penchés sur ces questions.
    Notre mandat était de dégager une synthèse, et c'est pour cela que nous
    appelons à voter pour...
    Différer le vote, c'est continuer à permettre à tous les syndicats de faire
    *un peu ce que l'on veut*, nous ne croyons pas que ce soit une bonne chose.

**ESS 34**
    D'accord avec le fait de dire que la somme de travail, n'est pas un argument
    pour ne pas le voter. Pas d'accord avec le fait de dire que les syndicats
    qui sont *contre* n'ont pas lu les textes...

**ETPIC 94**
    On peut voter une motion qui n'est pas parfaite, mais on pourra la
    perfectionner dans deux ans, ou la jeter et créer quelque chose de nouveau...
    On souhaite que la motion de synthèse soit soumise au vote très rapidement,
    qu'on puisse voter un nouveau bureau confédéral...

**Educ 91**
    D'accord avec ETPIC 94 pour voter cette motion, on émet des réserves sur le
    rôle attribué aux UR, et au CCN, donc nous on s'abstient, mais on considère
    que c'est une bonne ouverture pour les sections du privé.

**Educ 42**
    urgence de se positionner...l'état a posé sur le tapis l'urgence de la
    représentativité, on doit prendre le temps qu'on veut... on perd du temps à
    parler de la représentativité, alors que l'on pourrait plus parler
    de campagne syndicale...


Vote
====

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 32         | 0        | 3          | 10                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Pour : 28
----------

ETPRECI 35, Interco 07, ETPIC 94,
Interco 76, Interco 45, Interco 86,
ETPIC 30, Interco 29, STE 93, éduc
35, éduc 75, éduc 57, STE 92, éduc
66, PTT 35, PTT 66, santé social 86,
santé social RP, SIPM RP, CS RP, SII RP
(5 syndicats non identifiés)

Contre: 10
-----------

Interco 32, Interco 33, Interco 31,
STTE 33, éduc 33, santé social 42,
éduc 42, PTT 69, Interco 42 (1
syndicats non identifiés)

Abstention: 6
--------------

Interco 09, Interco 91, Interco 54,
éduc 91, ESS 34, SUB 35
STIS 59, Interco 48, SSEC 59


NPPV: 3
--------


**Motion adoptée**


Le secteur privé
================

**Actant de la réforme de la représentativité syndicale dans le secteur privé**,
la C.N.T adopte dans l’immédiat pour ce secteur les dispositions suivantes:

- la C.N.T réaffirme sa position historique contre le principe des Comités
  d’Entreprise (C.E) ;
- la C.N.T, dans le cadre de la défense des travailleurs dans les entreprises,
  laisse le soin aux structures syndicales au plus près du terrain (sections
  syndicales, en cohésion impérative avec leur syndicat d’appartenance) et de
  l’UR d’organiser son action syndicale, y compris si ces structures jugent
  nécessaire de se présenter aux élections de Comités d’entreprise ;
- les syndicats de la C.N.T étudieront dans les mois à venir la pertinence
  (protection, capacités) de la désignation de représentants de la section
  syndicale (RSS), telle que permise par les nouvelles dispositions
  législatives ;
- A l’égard du critère dit de **l’ancienneté** conditionnant l’émergence de
  section syndicale à une ancienneté du syndicat de plus de deux ans, les
  syndicats sont invités à fonder juridiquement dès que possible des syndicats
  d’industrie en vue de répondre au plus vite à ces exigences ; par défaut,
  la C.N.T pourra asseoir ses sections sur ses syndicats dits **Interco** dont
  les **statuts et intitulés légaux devraient dès lors permettre de couvrir le
  champ de syndicalisation (géographique et professionnel) le plus large et
  plus détaillé**.
- Tout en rappelant la confiance nécessaire entre camarades de notre
  organisation, la C.N.T incite les syndicats à développer des pratiques en
  rupture avec le syndicalisme co-gestionnaire et en accord avec les principes
  ASSR telles que:

      * Toute position d’un élu ou d’un délégué syndical doit être adoptée
        en lien **avec la section syndicale et le syndicat** ;
      * Les élus doivent faire des **compte-rendus de leurs réunions à leurs
        sections syndicales, syndicats et aux salariés de l’entreprise** ;
      * Les sections C.N.T sont invitées dans les négociations des protocoles
        électoraux à revendiquer la tenue des élections d’entreprise tous les
        deux ans, dispositif permettant une meilleure rotation des mandats
        électifs que les quatre ans prévus par la loi

- Les :term:`CCN` inscriront systématiquement à leur ordre du jour un point
  sur les  questions de **stratégie syndicale** en lien avec les réformes de la
  représentativité syndicale. Dès lors, les :term:`UR` de la C.N.T, ou des
  fédérations, y exposeront de façon exhaustive les prises de décisions de
  ses syndicats afin qu’une analyse globale puisse être appréciée en vue
  d’éclairer :ref:`le prochain congrès confédéral <motion_36_2010_saint_etienne>`.
- Concernant l’acquisition de la représentativité de branche ou
  interprofessionnelle nationale, la C.N.T affirme son désintérêt total pour la
  recherche d’une légitimité quelconque tendant à l’amener à être ou devenir un
  interlocuteur permanent des négociations avec le patronat ou l’Etat.
- Pour la compréhension de tous et toutes, une communication spécifique et
  argumentée quant à cette décision de Congrès est publiée dans le Combat
  syndicaliste et adressée à l’ensemble des organisations syndicales avec
  lesquelles la C.N.T entretient des rapports de fraternité et de solidarité à
  l’échelle internationale.


Vote "secteur privé"
--------------------

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 28         | 9        | 5          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Pour : 28
----------

Educ 35, PTT 35, ETPRECI 35, STE
93, Interco 07, ETPIC 94, Interco 87,
Interco 86, Interco 29, ETPIC 30,
Interco 45, Interco 31, Interco 76,
santé social 86, éduc 57, STE 32, éduc
66, PTT 66, SII RP, SIPM RP (8
syndicats non identifiés)

Contre: 9
----------

Interco 32, Interco 54, STTE 33, PTT
32, santé social 42, éduc 42, PTT 69,
Interco 42 (1 syndicats non identifiés)
Interco 33, santé social RP (3 syndicats
non identifiés)


Abstention: 5
--------------

NPPV: 3
-------

Interco 48, STIS 59, SSEC 59


**Motion adoptée**

.. seealso:: :ref:`motion_36_2010_saint_etienne`


Fonctions publiques
===================

Aucun nouveau critère législatif n’étant adopté à l’heure de ce Congrès,
la C.N.T reste sur sa position de refus de participation aux instances paritaires
et aux élections professionnelles dans les fonctions publiques.

En cas de nouvelle loi sur ce sujet, la C.N.T décide d’analyser les nouveaux
critères de représentativité syndicale dans un prochain Congrès afin d’adapter
sa position.

Dans l’attente de ce congrès, il est laissé la possibilité aux fédérations,
sous contrôle des CCN, d’étudier la question de la participation aux élections
si elles estiment que leurs droits syndicaux sont conditionnés par cette
participation.


Vote fonction publique
----------------------

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 31         | 7        | 7          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Pour : 31
Contre: 7
Abstention: 7
NPPV: 3

SUTE 69, éduc 13, éduc 66, éduc 33,
éduc 57, STE 92, éduc 35, Interco 21,
Interco 34, Interco 07, Interco 76,
Interco 87, Interco 29, Interco 86,
ETPIC 30, Interco 45, Interco 33,
ETPRECI 35, SII RP, CS RP, STTE 34,
STTE 33, SSE 31, Santé social 86, santé
social RP, PTT 66, PTT 35, éduc 75,
SIPM RP, ETPIC 94 (1 syndicat non
identifié)

Educ 38, éduc 42, Interco 54, Interco
42, Interco 31, Interco 32, santé social 42
SUB 35, Interco 09, Interco 38, Interco
91, ESS 34, STE 93 (1 syndicat non
identifié)

SSEC 59, Interco 48, STIS 59
