
.. _motion_46_2012:
.. _motion_46_2012_etpic30:

================================================================================================================
Motion n°46 2012 : Structuration du Bureau confédéral de la CNT et de la Commission administrative ETPIC 30 Sud
================================================================================================================
  

.. seealso::

  - :ref:`motions_etpic30`
  - :ref:`article_6_statuts_CNT_2010`



Motion reprise de ETPIC30, 2010
================================

- :ref:`motion_15_2010`

Préambule
=========

À l’occasion de la refonte des statuts de la CNT, le 30 e Congrès confédéral
de Lille (2008) donnait une nouvelle définition de la Commission administrative:
« Dans l'intervalle des Comités confédéraux nationaux, la CNT est administrée
par le Bureau confédéral (BC) et la Commission administrative (CA) élus par le
Congrès. La CA est composée des diférents secrétariats confédéraux mis en
place par le Congrès. Les membres de la CA ne pourront occuper aucun poste
responsable relevant d'un parti politique, d'une secte philosophique ou
religieuse. [...]» (:ref:`Art.6 <article_6_statuts_CNT_2010>`)

Seule, une motion adoptée au 25e Congrès confédéral de Lyon (1996) dresse
une liste des membres du Bureau confédéral, sans réellement en définir le
fonctionnement interne.

La motion adoptée précise d’ailleurs : « Cette liste est peut-être incomplète,
mais cela permet de fixer un premier cadre ».

Le 31e Congrès confédéral de Saint-Étienne (2010) a rejeté deux positions de
structuration de ces instances confédérale (présentée par ETPIC Gard sud et
SUB 69), par une forte abstention notamment.

Cette situation nous semble dommageable dans la mesure où elle laisse les
mandatés confédéraux devant l’imprécision de leur positionnement au sein du
Bureau confédéral et de la Commission administrative.

Les sérieux problèmes de coordination et de positionnement au sein du Bureau
confédéral sortant, en sont la plus amère illustration.

Nous espérons que le Congrès ne laissera pas ses mandatés devant une telle
carence de choix.

Au regard des sensibilités exprimées lors du dernier Congrès, nous faisons
une nouvelle proposition modifiée (notamment intégration du secrétariat
confédéral média et du secrétariat confédéral chargé du développement des
campagnes à l’équipe du BC).

En cas d’amendements ou de contre-motions à cette motion, nous demanderons
malgré tout à ce que le Congrès recourt à une commission de travail, en vue
d’établir tant que possible une synthèse à vocation consensuelle.

Nous proposons que soit maintenue une dissociation de travail entre le Bureau
confédéral et la Commission administrative, En efet, si la CA réunit tous les
mandatés, y compris les mandatés BC, le BC a un rôle de coordination interne
et de « représentation« politique » de la CNT » :

- Le BC valide les décisions confédérales et assure une veille sur le respect
  des orientations et du fonctionnement de la CNT. Il assoit tant que possible
  ses décisions sur une base de réflexion plus collégiales au sein de la CA
  (Secrétariat et Trésorerie confédéraux). Il est en charge de représentation
  pour la CNT.
- Tous les mandatéEs confédéraux sont dans la CA (Secrétariat international,
  secteur propagande, Bulletin intérieur, Postmaster, Webmaster, MandatéEs
  intranet, Combat syndicaliste, Temps maudits).

Dans un souci de voir des candidatures aux mandats mieux préparées, plus
anticipées, nous proposons des descriptifs de mandats plus précis.

**Nous proposons de même deux nouveaux mandats:**

- Un secrétariat aux afaires juridiques ayant pour mandat d’assurer une veille
  et une assistance juridique. Il pourra coordonner une équipe de personnes
  ayant acquis des compétences juridiques ;
- Un secrétariat chargé du développement, de la coordination des campagnes,
  et de la solidarité confédérale.

Motion
======

Annule et remplace la motion « Organisation du Bureau confédéral » du 25e
Congrès confédéral de Lyon (1996)

La Commission administrative se compose des mandatéEs confédéraux suivants
--------------------------------------------------------------------------

- Équipe du Bureau confédéral
- Secrétariat international : animation et coordination d’une commission
  internationale regroupant plusieurs responsables par secteurs géographiques
  ou linguistiques, un webmaster, un chargé des relations avec le CS
- Secrétariat à la propagande : une commission de mandatéEs chargéEs de gérer
  des besoins en matière de matériel de propagande confédéral, d’assurer les
  tirages et productions nécessaires, susciter la réalisation d'affiches tracts,
  répondre aux propositions recues des syndicats, assurer la distribution du
  matériel. Elle peut aussi en lien avec la CA CNT éditer du matériel adapté
  aux campagnes de la CNT et développer une ligne éditoriale confédérale de
  brochures
- Combat syndicaliste : comité de rédaction, administration et webmaster
- Secrétariat au Bulletin intérieur : édition et difusion
- Temps maudits : comité de rédaction et administration
- Secrétariat aux affaires juridiques : animation et coordination d’une
  commission confédérale juridique, assure une veille et une assistance
  juridique pour les syndicats et leurs Unions
- Webmaster : chargéEs de l’animation et de l’administration du site web
  confédéral, de l’ouverture des domaines pour les Unions et syndicats de
  la CNT, de l’assistance technique aux syndicats et Unions, des relations
  avec l’hébergeur
- Postmaster : création et gestion des mails confédéraux, création et gestion
  des listes de discussions confédérales, assistance technique aux syndicats
  et Unions, relations avec l’hébergeur, gestion et animation de la liste
  info contacts
- Intranet : commission de mandatéEs chargée de la création et de la gestion
  des accès des syndicats et Unions à l’Intranet confédéral, assistance
  technique aux syndicats et Unions, de la maintenance technique de l’Intranet
  confédéral, des relations avec l’hébergeur, de la modération du forum

L'équipe du Bureau confédéral se compose des mandatéEs suivantEs
-----------------------------------------------------------------

- Secrétariat confédéral
- Secrétaire confédéral adjoint : mandatéE aux labellisations et à la gestion
  de l’annuaire confédéral des syndicats et des Unions, aide administrative
  générale et représentation du Secrétariat confédéral
- Secrétaire confédéral adjoint : mandatéE aux relations aux contacts isoléEs
  et du développement, des relations contacts diverses, de l’archivage
  électronique et l’animation de l’intranet BC, aide administrative générale
  et représentation du Secrétariat confédéral
- Secrétariat mandaté au développement, de la coordination des campagnes, et
  de la solidarité confédérales, aide administrative générale et représentation
  du Secrétariat confédéral
- Secrétaire confédéral : mandatéE de la coordination générale de la
  Confédération, de la CA et du BC, assure une veille quant au fonctionnement,
  aux orientations et au respect des statuts de la CNT, validation des prises
  de positions confédérales
- Secrétariat mandaté relations média : rédaction et difusion des communiqués
  confédéraux, développement des contacts avec la presse, aide administrative
  générale et représentation du Secrétariat confédéral

Trésorerie confédérale
-----------------------

- TrésorierE confédéralE : mandatéEs à la gestion des comptes confédéraux et la
  validation des engagements financiers
- TrésorierE confédéralE adjointE : mandatéEs à la mise à jour de l’annuaire
  confédéral et communications, aux relances et relations avec le secrétariat
  chargé des labellisations et à la gestion de l’annuaire confédéral, à la
  gestion des prêts confédéraux, aide et représentation du/de la trésorierE
  confédéralE
- TrésorierE confédéralE adjointE : mandatéEs à la gestion des cartes
  confédérales et de la caisse de solidarité. Aide et représentation du/de la
  trésorierE confédéralE


Sauf pour le mandat de Secrétaire confédéral et le mandat de Trésorier
confédéral confiés respectivement qu’à une seule personne chacun, chaque
mandat confédéral peut intégrer plusieurs personnes connues dans un souci de
répartition et de rotation des tâches.

Le Congrès confédéral privilégiera la mixité et les candidatures émanant de
territoires diférents.

Les mandatés de la Commission administrative et du Bureau confédéral sont
invités à concerter leur action régulièrement par tous moyens à leur
convenance : réunions, listes de discussion, vidéoconférences, etc.

Ils agissent collectivement dans l’intérêt de la CNT, dans la fraternité et
la dignité que leur suggère leur mandat. L’échange et le partage des
informations sont favorisés vers la mutualisation des productions respectives,
dans la limite des prérogatives propre chaque mandat.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°46          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Contre-motions
===============

- :ref:`contre_motion_46_2012_ess34`
