
.. _contre_motion_22_2014_sipmcs:

===================================================
Contre motion à la motions 22 2014 SIPMCS
===================================================

- :ref:`motion_22_2014_etpic30`

Contre argumentaire à la motion 22
===================================

Si nous sommes en accord avec l'argumentaire de la :ref:`motion 22 <motion_22_2014_etpic30>`,
nous ne soutenons pas pour autant la motion qui en découle.

La mise en ligne du CS peut se faire par l'équipe qui gère le CS en faisant
une mise en ligne simple, par exemple en donnant la possibilité de télécharger
sous le format pdf le journal, ou autre format logiciel libre.

La réalisation d'un véritable site dédié au CS représente un important
travail dont il nous semble difficile d'assurer la qualité et la mise à
jour que réclame internet.
