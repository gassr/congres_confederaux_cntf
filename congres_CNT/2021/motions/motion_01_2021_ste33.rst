.. index::
   pair: Motion 1 ; 2021

.. _motion_1_2021_ste33:

================================================================================================================================================
Motion **n°1** 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **STE 33**
================================================================================================================================================

Autres motions STE33

    - :ref:`syndicats:ste33_motions_congres`


Argumentaire
==============

À la CNT nous menons nos luttes d’une manière différente et au nom des principes
ci-dessous qui nous sont propres :

- La **lutte des classes** est la base même de notre syndicalisme.
  Nous ne cherchons pas à réformer le capitalisme (contrairement à d’autres
  syndicats) mais à l’abattre.
- La lutte contre toutes les formes d’exploitation et de domination
  fonde notre engagement syndical et notre conception de la lutte des classes.

Mais si nous voulons abattre l’exploitation et construire une société égalitaire,
il faut aller au-delà de la simple **lutte des classes**.

Comme l’ont fait les militant.es de la CNT historiquement en Espagne, notre
anticapitalisme doit dépasser une analyse purement *marxiste*.
Cette volonté d’aller au-delà des simples questions de rapport marchand en
s’intéressant à la condition ouvrière de manière globale fonde notre différence
avec d’autres syndicats.
Elle nous permet aussi de mieux lutter contre les formes multiples et sans cesse
renouvelées que prennent l’exploitation et la domination.

Parce que nous voulons construire une société égalitaire, notre lutte se doit
d’être globale :

- Elle ne peut s’accommoder d’un syndicalisme corporatiste qui choisirait une
  catégorie à défendre plutôt qu’une autre.
- Elle ne peut choisir de combattre une injustice plutôt qu’une autre parce que
  les rapports sociaux inégalitaires et de domination sont la base même du
  capitalisme et du patriarcat et du racisme.

La proposition de l’ajout ci-après à nos :ref:`statuts <statuts_conf:regles_confederales>`
s’inscrit donc dans une continuité politique et historique quant au
positionnement spécifique de la CNT.

Le capitalisme, le patriarcat, le racisme sont trois systèmes de domination
qui coexistent, sont liés entre eux et se renforcent les uns les autres.
Ils façonnent toute la société y compris le monde du travail, ils impactent
donc forcément le travail syndical que nous avons à mener.

L’émancipation des travailleur/euses ne pourra donc pas se faire sans
l’émancipation des femmes et l’émancipation des racisé·es.

La CNT s’engage sur une lutte globale, nous ne pouvons donc pas laisser perdurer
des sphères d’exploitation en imaginant que cela n’impacte pas nos luttes.

Il n’y a pas plus de patriarcat à visage humain que de capitalisme à visage
humain ou de racisme à visage humain : tirons-en enfin les conclusions qui
s’imposent pour nos luttes et pour notre syndicalisme.

Par ce rajout aux statuts nous ne faisons que mettre nos statuts en cohérence
avec nos engagements et convictions syndicales.

Motion
=======

Ajoute à :ref:`l’article premier des Statuts confédéraux <titre_1_statuts_cnt_2014>`,
entre le 3è paragraphe::

    Elle précise que [...] l'ensemble de la société.

et le 4è paragraphe::

    La Confédération Nationale du Travail [...] la direction de l'organisation
    des travailleurs/ses

le paragraphe suivant::

    Elle est consciente que plusieurs formes de domination interagissent et
    se renforcent les unes les autres.
    C’est pourquoi elle tient à préciser que la suppression du capitalisme ira
    de pair avec la suppression du système patriarcal et raciste de la société.
    La CNT entend bien se donner les moyens d’articuler et de prôner aussi bien
    la lutte des classes que la lutte antipatriarcale et antiraciste.


Amendement
===========

- :ref:`amendement_motion_1_2021_interpro07`


Contre-motions
===============

- :ref:`contre_motion_1_2021_etpics94`
- :ref:`contre_motion_1_2021_ste75`
- :ref:`contre_motion_1_2021_simrp`


.. _motion_1_2021_liens_anciennes_motions:

NDLR: anciennes motions en rapport avec la  motion N°1 2021
===============================================================

Chartes en rapport (Charte d'Amiens)
-------------------------------------

- :ref:`statuts_conf:tant_materielles_que_morales`

::

    Le Congrès considère que cette déclaration est une reconnaissance de
    la lutte de classe, qui oppose sur le terrain économique, les travailleurs
    en révolte contre toutes les formes d'exploitation et d'oppression,
    tant matérielles que morales, mises en oeuvre par la classe capitaliste contre
    la classe ouvrière.

- :ref:`motion_identite_cnt_congres_1993_paris`

::

    Partant de cette assise, rien n'est étranger a notre syndicalisme:
    ni l'antimilitarisme, ni l'anticléricalisme, ni l'écologie, ni la lutte
    pour l'égalité des Sexes.


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°1 2021 STE33       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
