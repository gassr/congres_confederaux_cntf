
.. _contre_motion_12_24_27_2021_ste75:

============================================================================================================================================================================================================================================================================================================================
Contre motion N°3 à la motion n°12 2021 **Modification des statuts pour la gestion interne des violences patriarcales** et Contre motion N°7 à la motion n°24 2021 **Gestion interne des violences patriarcales** et contre-motion N°1 à la motion n°27 2021 **Gestion des Conflits au sein de la CNT** par **STE75**
============================================================================================================================================================================================================================================================================================================================

- :ref:`motion_12_2021_sinr44`
- :ref:`motion_24_2021_ste33_cnt42`
- :ref:`motion_27_2021_ptt_centre`


Autres motions STE75

    - :ref:`paris:motions_ste75`

Préambule
==========

Nous présentons la même contre-motion pour ces trois motions.
Donc, soit le congrès la vote une fois car c’est le même sujet,
soit la contre-motion est présentée après le vote de chacune de
ces trois motions.

Nous laissons à la sagesse des congressistes, le soin de trouver la
meilleure solution.

Argumentaire 
=============

Comment articuler la nécessaire autonomie des syndicats et les principes
d’action collective face aux situations d’agression.

Il y a nécessité de ne pas isoler les syndicats des personnes concernées
(personnes victimes et personnes mises en cause) et d’agir collectivement,
autant que possible.
Nous pensons donc qu’il faut surtout poser des principes qui serviraient
de **protocole général**.

Contre-motion 
================

Nécessité d’un cadre d’action collective.

Gestion collective de toute situation d’agression.

Une situation d’agression engendre toujours des tensions, du malaise pour
les personnes, les syndicats, les organisations.

Les syndicats d’appartenance des personnes concernées par la situation
doivent être accompagnés, tout comme les personnes concernées.
Les syndicats concernés, en cohérence avec les souhaits de la ou des victimes,
peuvent faire appel à d’autres syndicats (ou structures : UL, UR…)
pour les soutenir dans la gestion de la situation.

Priorité à la parole de la personne qui dénonce une situation d’agression.
S’il y a dénonciation, c’est que quelque chose ne va pas et doit être géré.
Il y a un impératif d’écoute et de prise en compte de la parole de la
personne qui dénonce. Les coûts de la dénonciation sont énormes pour
les personnes qui la font.

Mise en retrait des personnes mises en cause de la vie de la CNT, tant
que le ou les syndicats concernés n’ont pas tranché (ex : mandats, listes,
événements, manifs…).

Accompagnement des victimes par les personnes ou les syndicats de leur
choix (pourquoi pas au sein d’une liste de personnes « référentes »
proposées par les syndicats, mais sans obligation pour elles de choisir
dans cette liste).

Si des syndicats sont contre les décisions prises par le ou les syndicats
d’appartenance des personnes impliquées dans la situation, notamment si
la situation a des répercussions pour les autres syndicats dans leur
activité de lutte, leur désaffiliation-délabellisation peut être décidée
en congrès (y compris extraordinaire).


Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion aux motions n°12 |            |          |            |                           |
| n°24 et n°27 STE75             |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
