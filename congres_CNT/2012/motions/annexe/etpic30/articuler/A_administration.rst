
.. index::
   pair: Administration; CNT
   pair: organisation; fédéralisme libertaire
   pair: organisation; rotation des tâches

=====================================================
I.A - Administration et Direction politique de la CNT
=====================================================

La CNT a fondé son organisation interne et son pacte associatif selon un
fédéralisme caractérisé par une relative, mais importante, autonomie de ses
syndicats : le fédéralisme libertaire.

Elle est par conséquent anti-autoritaire et réfute toute autorité politique de
tutelle interne ou externe. Elle intègre des instances de coordinations,
d’exécution, et d’administrations dont les mandatés orchestrent légitimement
la vie confédérale.
Leur pouvoir de décision s’inscrit au cœur des orientations et du fonctionnement
tels que définis par les syndicats de la CNT. Ce pouvoir est donc limité mais
inclus un devoir de vigilance, et donc d’alerte.

La rotation des tâches ou des mandats est aussi un principe de fonctionnement
adopté par la CNT depuis de nombreuses années à tous les niveaux fédéraux,
jusqu’aux syndicats eux-mêmes.

Elle est perfectible par une meilleure répartition du poids des mandats comme
en témoignent les dernières motions de Congrès proposant de «découper» les
principaux mandats confédéraux (Secrétariat Confédéral, Trésorerie Confédérale).

Il n’y a donc pas d’encadrement permanent à la CNT. On lira dans la brochure
«La CNT c’est quoi»::

	« Parce que les permanents syndicaux, dans leur ensemble, génèrent -
	inconsciemment ou non- la passivité et la bureaucratie  au sein de leurs
	organisations. Parce que les décisions doivent être prises à  la base par
	les syndiqués eux-mêmes».

En effet, les exemples ne manquent pas, l’émergence des bureaucraties modèle
les organisations syndicales. Leurs permanents perdent leur position de classe
et développent des logiques «carrièristes» et élitistes. Ils tendent à
entretenir collectivement une logique implacable de survie, la même qui motive
le maintien d’un prolétariat qui les délègue et les fait vivre. Cette tendance
se confirme à plus ou moins long terme dans la collaboration avec la machine
étatique et politicienne. L’organisation syndicale devient, de moyen de lutte
collective, une fin en elle-même, à la conservation de laquelle tout doit être
sacrifiée, y compris et surtout la lutte révolutionnaire qui mettrait un terme
à la délégation du pouvoir dont ils sont les produits et les principaux
bénéficiaires.

En monopolisant la représentation politique des travailleurs exploités,
l’encadrement syndical assure, pour sa part propre, l’institutionnalisation
de la lutte de classe, c'est-à-dire son intégration au cadre global de l’Etat
et sa subordination consécutive aux intérêts généraux du capital.

Ainsi, la Fédération des Travailleurs de l’Education CNT déclarait dans un
communiqué de presse en septembre 2011::

	«Pour notre syndicat de lutte, il est  absolument impensable qu’un
	représentant du personnel ne soit plus au fait,  parfois depuis des années,
	de la réalité du travail qu’il est censé défendre.
	La CNT est un syndicat sans permanents, par choix et conviction»

Si l’on veut pourtant bien leur en faciliter la réalisation technique et
matérielle, à la CNT comme ailleurs, les mandatés révocables sont tout à fait
à même de conduire « politiquement » les choix quotidiens de notre organisation.

**Le recours «aux permanents syndicaux» (pour reprendre donc notre définition
initiale), salariés ou bénéficiant de décharge, comme cadres de l’organisation
en charge d’assurer les décisions politiques, nous apparaît contraire à
l’essence même de l’AS & SR, du fédéralisme libertaire d’intérêt général,
lui-même garant organique de la capacité révolutionnaire de la CNT.**
