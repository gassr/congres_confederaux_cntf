
.. _amendement_motion_9_2021_stp67:

============================================================================================
Amendement  N°5 à la Motion n°9 2021 **Lutte contre la Précarité** par **STP67**
============================================================================================

- :ref:`motion_9_2021_interco69`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`

Amendement
============

Amendement : Préciser les plateformes numériques, et non l’économie du
numérique :

« Se mobiliser dans la lutte contre la précarité, l’intérim en particulier
dans le secteur public, et l’expansion d’un salariat déguisé, la libéralisation
dans tous les secteurs  (privé/public), et l’augmentation du contrôle
des travailleuses et travailleurs, notamment via les plateformes numériques. »

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°9  |            |          |            |                           |
| STP67                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
