


.. _motions_syndicats_CNT_2016:

================================================================================
Liste des syndicats ayant déposé des motions/contre-motions amendements CNT 2016
================================================================================



Alsace
=======

- :ref:`stp67`

Bourgogne
=========

- :ref:`interco71`


Franche-comté
===============

- :ref:`interco25`


Ile-de-France
==============

- :ref:`sim_rp`
- :ref:`ste75_pub`
- :ref:`etpreci_75`
- :ref:`ste94`
- :ref:`sipmcs`

Languedoc-Roussillon
=====================

- :ref:`ess34`
- :ref:`etpic30`
- :ref:`interco48`

Lorraine
=====================

- :ref:`etpics57`

Midi-Pyrénées
==============

- :ref:`interco09`
- :ref:`interpro31`
- :ref:`sse31`


Pays de la Loire
=================

- :ref:`ste49`
- :ref:`sinr44`


Provence-Alpes-Cote d'Azur
===========================

- :ref:`stics13`

Rhône-Alpes
============

- :ref:`tasra`
- :ref:`interco73`
