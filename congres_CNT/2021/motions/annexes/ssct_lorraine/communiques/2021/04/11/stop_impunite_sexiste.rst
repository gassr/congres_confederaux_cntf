

.. _communique_ssct_lorraine_2021_04_11:

==================================================================================================================================================================================
Dimanche 11 avril 2021 **L'impunité sexiste c'est non**
==================================================================================================================================================================================



Communiqué
==========

::

    ------- Message transféré --------
    Sujet : 	[Liste-syndicats] L'impunité sexiste c'est non
    Date : 	Sun, 11 Apr 2021 12:19:07 +0200
    De : 	sestlorraine@cnt-f.org
    Pour : 	liste <liste-syndicats@bc.cnt-fr.org>


Bonjour

Notre syndicat informe l'ensemble de la Confédération que plusieurs de
nos adhérentes se sont constituées partie civile dans des affaires de
viols et d'agressions sexuelles impliquant un ancien adhérent de la CNT
ayant eu le mandat confédéral "Relations médias" jusqu'en 2016.

Le syndicat SEST Lorraine soutient les camarades tant humainement que
financièrement.

Nous savons que ce sujet est très dur et nous ne voulons faire revivre
de traumatisme à personne, mais néanmoins, si des camarades ont été
victimes de cette personne et/ou ont été témoin d'agression impliquant
cet ancien adhérent, nous sommes disposé.e·s à les mettre en relation
avec les adhérentes concernées.

vous trouverez ci-joint le communiqué public de notre syndicat

salutations antipatriarcales

CNT SEST LORRAINE

.. figure:: stop_impunite_sexiste.png
   :align: center
