.. index::
   pair: Fonctionnement ; Statuts

.. _motions_fonctionnement_statuts_2021:

=====================================
Motions Fonctionnement statuts 2021
=====================================

.. toctree::
   :maxdepth: 3

   motions_XX_YY_ptt_centre_todo
   motion_numX_2021_cnt_09_tresorerie
