
.. _amendements_motion_26_2021_stp67:

=========================================================================================================================================================
Amendements à la motion n°26 2021 **Intranet confédéral** par **STP67**
=========================================================================================================================================================

- :ref:`motion_26_2021_educ21`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`

.. _amendement_1_motion_26_2021_stp67:

Amendement 1
=================

Argumentaire
-------------

Nous n’avons pas compris ce que signifie « radiation » ?
Blocage de la lecture ? De l’écriture ? Les deux ? Pendant combien de temps ?

Amendement 
----------

Ajout de l’article à la fin : il appartient au syndicat de réinscrire
une personne radiée d’un autre groupe, et de ce groupe de le refuser
(ou de le radier encore).

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement 1 à la motion    |            |          |            |                           |
| n°26 STP67                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_26_2021_stp67:

Amendement 2
================

Argumentaire
---------------

La transparence incite à la participation, et au contrôle démocratique

Article 3, ajout de : « les moyens de mise en œuvres de protection de la
vie privée, contre le vol et la saisie »

Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement 2 à la motion    |            |          |            |                           |
| n°26 STP67                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
