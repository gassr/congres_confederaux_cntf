.. index::
   pair: Motion 14 ; 2021

.. _motion_14_2021_syndicats_42:

================================================================================================================================================
Motion **n°14** 2021: **Financement confédéral des groupes Femmes Libres** par **Syndicats 42**
================================================================================================================================================

- :ref:`analyse_differends_internes`
- :ref:`mujeres_libres_1936_05_01`

Autres motions CNT 42

    - :ref:`CNT42 <rhone_alpes:cnt42_motions_congres>`


Auteurs
========

CNT Éduc 42, CNT Culture et Spectacle 42, CNT Santé-Social 42, Interco 42.

Argumentaire
=============

Le 3 février 2018, à Saint-Étienne, le premier groupe non-mixte Femmes Libres
était créé pour donner suite au combat antipatriarcal entamé par :ref:`Mujeres Libres en 1936 <mujeres_libres_1936_05_01>`.

C’est affligeant mais ce combat est toujours d’actualité !

Aujourd’hui, et alors que les groupes Femmes Libres 33, Femmes Libres BZH-PDL,
Femmes Libres Nord-Picardie, Femmes Libres Lyon sont à leur tour créés, et que
d’autres sont en préparation, les syndicats CNT Éduc 42, CNT Culture et
Spectacle 42, CNT Santé-Social 42, Interco 42, demandent un financement
confédéral pour soutenir et légitimer ces groupes Femmes Libres au sein
de la CNT.

En effet, les militantes des groupes Femmes Libres sont avant tout des cénétistes
et ont volontairement choisi de rester ou d’adhérer à la CNT pour porter leur
combat anarcho-syndicaliste féministe.
Ces groupes sont soutenus par nombre de syndicats qui sont conscients de
l’urgence de prendre à bras le corps la lutte contre les violences patriarcales
et pensent que les groupes Femmes Libres, sur un principe autogestionnaire,
mènent un combat des opprimées pour les opprimées.

Motion
=======

Une cotisation de 0,10€ sur les cotisations syndicales de chaque adhérent.e
est affectée au fonctionnement des groupes Femmes Libres pour organiser des
rencontres, formation, actions.

Cette cotisation ne retire pas aux différents groupes la tâche de s’autofinancer,
mais engagerait la confédération dans leur reconnaissance et le soutien à leur
combat.


Amendements
===========

- :ref:`amendement_motion_14_2021_sest_lorraine`
- :ref:`amendement_motion_14_2021_interpro31`
- :ref:`amendement_motion_14_2021_stp67`

Contre motions
===============

- :ref:`contre_motion_14_2021_cnt30`
- :ref:`contre_motion_14_2021_cnt09`
- :ref:`contre_motion_14_2021_etpics94`


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°14 2021 CNT42      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
