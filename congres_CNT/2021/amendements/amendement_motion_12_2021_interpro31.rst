
.. _amendement_motion_12_2021_interpro31:

==================================================================================
Amendement à la Motion n°12 2021 **Modification des statuts** par **Interpro31**
==================================================================================

- :ref:`motion_12_2021_sinr44`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`

Argumentaire 
================

Qui peut décider au nom de **la confédération** ? Nous ne voulons pas
que ce soit le :term:`BC` ou tel ou tel mandaté·e·s qui puissent seul·e·s se
substituer à un syndicat pour décider de l’exclusion d'un·e adhérent·e.

Amendement 
============

Dans des circonstances exceptionnelles, prévues par des motions de congrès,
la confédération, le CCN ou le Congrès peuvent se substituer à un syndicat
pour décider de l’exclusion de l’un·e de ses adhérent·e·s.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°12 |            |          |            |                           |
| Interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
