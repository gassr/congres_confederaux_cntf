


.. _motion_13_2010:
  
====================================================================================================================================================================================================================
Motion n°13 2010 Précision du laps de temps nécessaire pour la mise à disposition des syndicats de l'ordre du jour du C.C.N et respect mécanique de la date limite de remise des points dudit ordre du jour, CNT 09
====================================================================================================================================================================================================================



.. seealso::

  - :ref:`motions_interco09`


Motion reprise en 2012 par interco09
======================================

- :ref:`motion_44_2012`

Argumentaire
=============

Lors du :term:`C.C.N` des 16 et 17 mai 2009 l'ordre du jour n'était pas mis à
disposition des syndicats avant le 6 mai, ce qui empêche le fonctionnement
démocratique de notre confédération du fait que les syndicats qui ne se
réunissent qu'une fois par mois n'ont pas pu l'examiner à temps pour faire
part de leur point de vue aux Unions Régionales qui de ce même fait n'ont pu
envoyer (sauf une) de mandatés à ce :term:`C.C.N`.

Même si nos statuts confédéraux qui mentionnent «Les Syndicats sont informés au
moins un mois avant la tenue du :term:`C.C.N` de l'ordre du jour définitif.»
avaient été respectés a minima (un mois) certains syndicats suscités
n'auraient pas davantage pu donner leur avis sur l'ordre du jour, vu qu'un mois
ne contient guère plus que 4 semaines auxquelles il faut retrancher une semaine,
si toutefois les réunions d':term:`U.R` prévues pour examiner le dit ordre du jour
se tiennent le week-end précédent le :term:`C.C.N`.

En effet, un syndicat se réunissant tous les mois ne se réunit pas toutes les 3 semaines, mais bien toutes les 4 ou
5 semaines, et c'est le hasard qui fera si une de ses réunions mensuelles peut comporter à son programme
l'examen des points de l'ordre du jour du :term:`C.C.N`.

Motion
=======

Pour que tous les syndicats aient le temps d'examiner les motions lors de leur AG un délai de huit semaines est
indispensable (en comptant deux semaines pour la mise à disposition de l'ordre du jour du :term:`C.C.N` aux syndicats,
délais postaux compris), dans le cas où les réunions d':term:`U.R` se tiennent le week-end précédant le :term:`C.C.N`, il reste cinq
semaines aux syndicats pour examiner les motions au cours de leur AG habituelle.

La date d’échéance de dépôt des points de l'ordre du jour par les unions régionales ainsi fixée 56 jours avant la
date du :term:`C.C.N` doit donc être respectée mécaniquement, le cachet de la poste faisant foi (ou la date d'envoi par e-mail),
les points de l'ordre du jour reçus le lendemain étant reportés au :term:`C.C.N` suivant.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°13          |            |          |            |                           |
| CNT 09               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso::

   - :ref:`amendement_motion_13_2010_ssctrp`
   - :ref:`amendement_motion_13_2010_culturerp`
   - :ref:`contre_motion_12_13_14_2010_interco68`
   - :ref:`contre_motion_13_2010_ess34`
   - :ref:`contre_motion_13_2010_stp72`
