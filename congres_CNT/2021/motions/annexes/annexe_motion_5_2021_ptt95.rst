
.. _annexe_motion_5_2021_ptt95:

================================================================================================================================================
**Annexe à la motion n°5 2021 Pour l’adhésion de la CNT à la Confédération Internationale du Travail. C.I.T - Engager le processus.** (PTT95)
================================================================================================================================================

.. seealso::

   - :ref:`motion_5_2021_ptt95`

Annexe
=========

Les motions de congrès sur la démarche internationale de la CNT.

Notre demande d’adhésion est la suite logique de nos analyses, nos actions et
campagnes internationales de toutes ces dernières décennies, comme il apparaît
dans le récapitulatif des motions de Congrès confédéraux antérieurs, qui suit:

Petit historique des motions qui définissent la politique confédérale en matière
de relations internationales, depuis l’*exclusion* de notre Confédération de
l’AIT (Madrid 1996) :

«La CNT ne reconnait pas la décision du Congrès de Madrid. En conséquence, elle
entend poursuivre son travail internationaliste en liaison avec les sections
de l’AIT qui le souhaitent, et en relation avec les syndicats présentant
des caractéristiques de rupture avec le capitalisme et l’Etat et agissant
hors de l’AIT. (Congrès extraordinaire de Paris, 1997)

La CNT-AIT reste fidèle à ses origines qui sont la Première Internationale et
celle de 1922, et déclare qu’il n’y a actuellement pas l’intérêt de créer une
nouvelle organisation internationale. (26e Congrès à Paris, 1998)

La CNT siégeant à Paris a pris la décision de ne plus utiliser le sigle AIT,
exclue bureaucratiquement. Prenant acte que l’AIT est devenue une organisation
groupusculaire orientée selon des bases dogmatiques et absente des mobilisations
sociales de masse (Cologne, Amsterdam, Nice) la CNT a décidé de supprimer toute
référence à l’AIT.
Au-delà de ce choix, la CNT tient à affirmer qu’elle maintiendra haut et fort
l’esprit et la lettre de ce que fut la véritable AIT et renforcera son activité
sur la scène internationale, non seulement avec les organisations syndicales
qui se revendiquent explicitement du SR et de l'AS mais également avec les
structures syndicales qui luttent sur des bases anticapitalistes et antiétatiques
pour l’action directe et le refus de la collaboration de classe.
(27e Congrès à Toulouse, 2001)


Suite à l’abandon des sigles «AIT», aucune des trois propositions sur le nom
de l’organisation (« CNT » - tout seul, «CNT-1 ère Internationale»,
«CNT-Internationaliste ») n’obtient la majorité des voix !
(27e Congrès à Toulouse, 2001)

Considérant que la construction d’une internationale ou la reconstruction de
l’AIT ne pourrait se faire par en haut, mais qu’elle se fera par le bas, nous
sommes contre une proclamation formelle sur le papier d’une nouvelle
internationale à l’heure actuelle. Mais nous sommes pour la mise en marche
de cette œuvre constructive et reconstructrice dès maintenant, sur des bases
pratiques et concrètes : Dans l’immédiat la poursuite de notre engagement
international. A moyen terme l’organisation de conférences industrielles,
de branches ou secteurs avec tous les contacts internationaux.
La tenue de conférences et la création d’une dynamique par en bas à travers des
campagnes de solidarité internationale.
A long terme, nous envisageons une possible reconstruction future de
l’Internationale. Nous maintenons les statuts de l’AIT dans leur version
d’avant 1996.
(27e Congrès de Toulouse, 2001)

Le Congrès réaffirme la nécessité pour tous les syndicats de respecter l’image
de la confédération et de définitivement abandonner l’usage du sigle AIT. »
(29e Congrès à Agen, 2006)

La CNT travaille au niveau international avec : les autres structures ASSR
(CGT, SAC, FAU, IWW, Solidaridad Obrera etc.) ; les syndicats de lutte (SIMECA,
SNAPAP, CGT-B etc.) ; les structures syndicales, sections d’entreprises, unions
locales etc. combatives faisant partie de confédérations « réformistes (UMT,
syndicats colombiens etc. et en cas d’absence de structures syndicales des trois
types précédents pour des raisons de régime politique dictatoriaux, travail
avec des organisations (hors partis politiques ou organisations à références
religieuses) luttant pour des libertés fondamentales d’organisation, d’expression
etc.
(29e Congrès à Agen, 2006)

Que I-07 soit organisé en Région parisienne par une commission syndicale régionale
en collaboration avec le SI (Avril/Mai ou Automne 2007).
Les principes de la rencontre restent celles des rencontres industrielles déjà
organisées (I-99 à San Francisco et I-02 à Essen), c’est-à-dire des rencontres
industrielles de militants d’organisations syndicales de lutte de classe à la base.
(29 e Congrès à Agen, 2006)


Ces dernières années, le travail de la CNT au niveau international s’est structuré
principalement autour des organisations anarcho-syndicalistes et syndicalistes
révolutionnaires suivantes : la CGT (Espagne), la SAC (Suède), la SKT (Sibérie),
la KASSN (Ukraine), des groupes des IWW (USA), de l’USI (Italie) et des
organisations plus récemment constituées comme l’IP polonaise et l’ESE grecque.
Les liens qui unissent ces organisations à la CNT sont un élément décisif dans
la construction stable d’une coordination d’organisations anarcho-syndicalistes et
syndicalistes révolutionnaires au niveau international.
(29e Congrès à Agen, 2006)
