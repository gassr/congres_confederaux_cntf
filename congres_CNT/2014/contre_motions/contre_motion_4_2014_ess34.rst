
.. _contre_motion_4_2014_ess34:

===================================================
Contre motion à la motion n°4 2014 (ESS34)
===================================================

.. seealso::

   - :ref:`motion_4_2014_sinr44`





Argumentaire
=============

Nous pensons qu'un congrès doit réunir le maximum de syndicats (présents par
leurs mandaté-e-s) pour pouvoir discuter, échanger et prendre des décisions.

Comme nous ne connaissons pas par avance les décisions que le congrès prendra
vis à vis des procurations, nous préférons fixer un quorum sur les syndicats
présents physiquement et uniquement sur ce nombre.

Motion (non applicable pour le 33ème Congrès confédéral)
=========================================================

Afin qu'un Congrès confédéral puisse débuter, il est nécessaire que 50%
minimum des syndicats à jour de cotisations soient présents physiquement.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°4 2014 <motion_4_2014_sinr44>`

       :ref:`ESS34 <ess34>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
