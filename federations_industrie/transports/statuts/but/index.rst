
===
But
===

Article 3
==========

La fédération a pour but de coordonner l'action des sections et des syndicats
dans les secteurs des transports afin d'arriver à constituer le travail libre,
affranchi de toute exploitation capitaliste, par la syndicalisation des moyens
de production, au bénéfice exclusif des travailleurs.

Dans l'action journalière, elle se donne pour but:

- De représenter et de défendre les organisations adhérentes, dans toutes les
  questions ayant un caractère d'intérêt général; de resserrer étroitement les
  liens de solidarité et unir dans un seul bloc tous les travailleurs des
  industries qu'elle fédère, sans distinction de sexe et de nationalité;
- D'organiser partout où il sera possible:

    - des Sections Syndicales d'Entreprise,
    - des Syndicats Locaux du transport,

- De lutter pour la suppression de l'intérim et de la sous-traitance et du
  travail à la tâche ou aux pièces;
- De poursuivre la diminution des heures de travail par tous les moyens en son
  pouvoir;
- D'empêcher la baisse et l'avilissement des salaires.



Article 4
=========

La Fédération s'interdit toute prise de position sur les orientations générales
et la stratégie de la CNT, qui sont de la compétence exclusive des syndicats et
du Congrès Confédéral.
