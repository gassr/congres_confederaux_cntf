
.. _motions_15_16_2012:

=======================================================================================
Motions n°15,16 2012 : Autogestion à la CNT, Abolition du salariat STEA (Alsace)
=======================================================================================
  
.. seealso::

   - :ref:`stea`
   - :ref:`motions_stea_2012`



Argumentaire
============

L'autogestion étant un principe de base de l'organisation des syndicats de la
CNT : il convient de rappeler en quoi la rotation des tâches et des mandats
est un principe essentiel à son bon fonctionnement.

La rotation des tâches et des mandats doit non seulement permettre la
non-spécialisation des individu-e-s au sein du syndicat et des différentes
instances fédérales et confédérales, mais encore contribuer à leur
émancipation...

En effet, la spécialisation de certains acteurs/rices, muté-e-s "expert-e-s" du
syndicalisme, contribue à leur instituer une place dominante dans le syndicat,
monopolisant les compétences et savoir-faire syndicaux que tou-te-s doivent
acquérir, via les formations syndicales récurrentes, mais aussi en pratiquant
une rotation des mandats afin d'appliquer et de partager notre expérience de
l'ASSR, en termes techniques (écriture et mise en page de tract et de la
propagande, fonctionnement interne du syndicat, des fédérations et de la
confédération), juridiques (monter un dossier prud'homal et aller le
défendre, se préparer et réagir face à une arrestation ou plus généralement à
la répression des mouvements sociaux, soutenir les camarades), et en termes
d'action syndicale (connaitre, choisir et appliquer les formes de lutte les
plus à même de renverser le rapport de force face au patronat ou à l'État,
désigner un-e RSS, mener une action de soutien, organiser une AG), etc.

L'autogestion et la rotation des tâches permettent de se passer de permanents
techniques et politiques, quelles que soient leurs domaines de compétence.

La mise en place de permanents a deux conséquences qui nous semblent
problématiques.

D'une part, elle contribue à cette spécialisation des tâches renforcant la
division des tâches et donc la hiérarchie au sein du syndicat, cautionnant de
fait la division  sociale du travail.

Elle contribue à la bureaucratisation qui rend plus difficile, le contrôle des
mandats face à des individus se définissant par leurs compétences spécifiques.
C'est aussi dans ce sens que nous luttons pour une répartition égalitaire du
temps de travail, afin d'investir et de développer nos outils syndicaux.

D'autre part, elle renforce l'aspect "syndicalisme de service" que nous
rejetons, au sens où le syndicat doit être l'arme des travailleureuses
qu'eux/elles-mêmes mettent en pratique, et pas un syndicalisme
professionnel de service spécialisée dans l'intervention partielle et
particulière "au nom des" travailleureuses, face au patronat et à l'État.

Aujourd'hui, seulement 7% de la masse salariale est syndiquée en France. Ce
faible pourcentage, ainsi que son évolution décroissante se retrouvent aussi
dans de nombreux pays au niveau international, à l'exception de la Belgique,
de l'Islande et des pays scandinaves, dont la relation entre l'État et les
syndicats est très diférente, celui-ci déléguant une partie de ces
compétences sociales aux syndicats représentatifs, qui se retrouvent de fait
dans une situation cogestionnaire et qui attire des adhérent-e-s sur la base
des services que celui-ci peut rendre, sans pour autant mener la grève générale.

Cette faiblesse du syndicalisme de classe n'est pas à mettre sur le dos de
la CNT, et cela prouve bien à quel point les stratégies développées par les
syndicats cogestionnaires et professionnalisés dans la distribution de services
ne constituent pas un exemple à suivre.

Au contraire, nous ferions mieux de mettre en avant nos diférences de pratiques,
en tant que syndicat de lutte des classes. Il convient de souligner la diférence
entre un mandat technique, sur lequel un contrôle peut s'exercer, la rotation
pouvant être efective à chaque congrès syndical, fédéral ou confédéral, et un
rôle de permanent, qui met ses compétences à profit sans limite de temps et
sans contrôle de ses actes.

La rotation des tâches, la non-permanence des tâches et la formation syndicale
en interne, permettant la mutualisation des compétences syndicales, est un atout
essentiel conduisant à l'émancipation des syndiqué-e-s et des travailleureuses.

Ne syndiquant pas les patron-e-s, reconnaissant le salariat comme un outil du capital
pour asservir les travailleureuses et une manne à la plus value, la CNT n'a pas
à salarier un de ses membres ou quelconque autre personne.

En salariant, via un contrat de travail (qui peut s'avérer précaire ce que nous
combattons pourtant), une personne, une structure syndicale se retrouverait dans
une position patronale, capable de licencier ou de mettre et de maintenir en
situation de précarité un-e travailleureuse.

Nous ne parlons pas des mesures d'indemnisation des mandaté-e-s en
cas de dépenses ou des possibilités d'appointements (qui ne soient pas contractuels)
sous des conditions tout-à-fait particulières.

Par exemple, nous pourrions penser que le camarade puisse en bénéficier si et
seulement il n'est pas en temps complet chez un-e employeureuse ou retraité-e,
si le syndicat n'a pas les forces économique et militante pour gérer un mandat
à plusieurs, et que l'appointement ne soit pas renouvelable sur
deux mandats de suite pour ne pas créer de carriérisme au sein de la CNT, etc.

Ce n'est pas là l'objet de ces motions, pour autant, des pistes de réflexion
sont à étudier. Imaginez la CNT poursuivie au prud'homme ou ne renouvelant pas
le CDD d'un permanent ! Il nous semble hautement contradictoire, que la CNT,
outil syndical de lutte contre l'exploitation salariale, agisse à la manière
du patronat. En outre, malgré tous les garde-fous, ces acquis sociaux gagnés
par la lutte concernant le droit du travail, qui garantissent un
minimum de sécurité face aux employeureuses, nous savons très bien que c'est
le salariat même en CDI avec un salaire convenable) qui est à la base de la
division inégalitaire de la société. Ainsi, tous les garde-fous possibles
quand à la rémunération contractuelle de travailleureuses au sein du syndicat
ne suffiront jamais à garantir que la CNT ne se dérive pas peu à peu vers ce
pourquoi nous luttons.

Préambule
=========

L'argumentaire est ci-dessus.

Les motions suivantes (numérotées 15 et 16) sont à voter séparément et dans
leur intégralité.

.. _motion_15_2012:

Motion statutaire N°15 Autogestion à la CNT et non recours à des permanents
===========================================================================

.. seealso::

   - :ref:`article_3_statuts_CNT_2010`

Rajouter à :ref:`l'article 3 des statuts <article_3_statuts_CNT_2010>` :

« La C.N.T. est administrée suivant les  directives données et les décisions
prises par les syndicats réunis en Congrès, tous les deux ans.

À la CNT, le pouvoir appartient aux syndicats, cellule de base de la
Confédération, et à leurs adhérents au sein des syndicats.

La CNT fonctionne sur un mode autogestionnaire. Cela implique une attention
toute particulière à la rotation des mandats, et au contrôle des mandaté-e-s,
responsables et révocables, par le syndicat.

La CNT refuse d'avoir recours à des permanents techniques et/ou syndicaux»

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°15          |            |          |            |                           |
| STEA                 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. _motion_16_2012:

Motion statutaire N°16 : Abolition du salariat
==============================================

.. seealso::

   - :ref:`statuts_CNT_2010`


Création d'un article 30 dans les statuts:

«En accord avec l'article 1 des statuts, pour l'abolition du salariat, la CNT
refuse en ce sens de salarier un de ses membres ou quelconque autre personne»

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°16          |            |          |            |                           |
| STEA                 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
