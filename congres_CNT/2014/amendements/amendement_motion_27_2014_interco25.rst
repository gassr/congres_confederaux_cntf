
.. _amendement_motion_27_2014_interco25:

================================================
Amendement à la motion n°27 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_27_2014_etpic30`




Argumentaire
=============

Il est nécessaire qu'après chaque congrès tous les syndicats reçoivent un
exemplaire du recueil des motions adoptées.

Amendement
===========

Suppression de la mention "en faisant la demande" (dernier paragraphe).

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°27 2014 <motion_27_2014_etpic30>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
