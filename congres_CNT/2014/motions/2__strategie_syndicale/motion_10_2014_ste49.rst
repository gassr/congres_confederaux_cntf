
.. index::
   pair: Motion 10 ; 2014

.. _motion_10_2014_ste49:

======================================================================================
Motion n°10 : Mouvement syndical-mouvement social, vers l'unité d'action ? (STE 49)
======================================================================================




Argumentaire
=============

La présidence Hollande, élu en 2012 pour «combattre la finance », et les
mandats ministériels Ayrault et Valls symbolisent l'échec politique de
la **gauche bourgeoise, réformiste, libérale et sécuritaire**.

Les réformes engagées ne marquent aucune rupture avec les précédents
gouvernements de droite (réductions des dépenses publiques, cadeaux au
patronat comme le CICE, expulsions de sans-papiers et des roms,
prolongement de la réforme des retraites de la droite …) tandis que les
rares marqueurs à gauche annoncés en début de mandat ont soit sauté
(droit de vote des étrangers non communautaires aux élections
municipales) soit ont subi des reculs lorsque la France réactionnaire
s'est mise en état de marche (mariage homosexuel mais sans la
procréation médicalement assistée et la gestation pour autrui).

La politique gouvernementale n'a pourtant pas eu comme seul effet de
discréditer ce même gouvernement et les différents partis réformistes
qui le soutenait (EELV, PRG), elle a contribué a détruire l'image même
de l'idée de gauche dans toute sa diversité.
Si nous n'avions nous aucune illusion sur ce qu'allait être ce
gouvernement, une grande partie du monde du travail et du mouvement
social a cru que ce nouveau quinquennat pouvait être l'occasion de
revenir sur une partie des réformes de la présidence Sarkozy tout en
poussant quelques leviers à gauche.

La désillusion qui en a suivi n'en a été que plus forte, après douze
ans à droite, et a engendré aussi bien le séisme politique des élections
européennes de mai dernier tout en augmentant la défiance populaire
vis à vis du cadre institutionnel.

La gauche au pouvoir est donc en situation d'échec et a ainsi alimenté
la montée de l'extrême droite, montée sur laquelle nous reviendrons.
Cette poussée n'est pourtant pas l'entière faute du parti socialiste
et de ses alliées mais bien l'expression d'une gauche, dans son ensemble,
complètement aphone, obligée de glisser sur des thématiques (sécurité,
immigration) qui ne sont pas les siennes sans pouvoir en imposer
d'autres, en partie tétanisée dans les luttes, voulant encore et toujours
donner sa chance au gouvernent PS-MEDEF et incapable de voir qu'une
partie entière du pays tend dangereusement vers la réaction.

La gauche dite *radicale* s'est elle perdue dans les méandres de
*l'opposition constructive* et du jeu électoraliste/médiatique, ce
qui a eu pour effet de la marginaliser dans le champ de la lutte
sociale.
Ni le Front de Gauche, ni le NPA ou encore LO n'ont à un moment réussi
à s'afficher comme une alternative crédible alors que la situation
politique et sociale devraient leur permettre de l'être.

La gauche libertaire, pour sa part, est également inaudible du fait de
ses querelles internes/divisions, de sa relative faiblesse militante
et d'un mouvement social quasi inexistant, encore plus quand celui-ci
est soumis à la passivité des différentes organisations syndicales
représentatives.

C'est cette faiblesse générale de la gauche, dans l'ensemble de ses
composantes, qui a crée la situation d'instabilité et de danger social
et politique dans laquelle nous sommes aujourd'hui.

Aucun courant du système politique traditionnel n'arrive à offrir une
réponse aux questions posées par la crise que ce soit la droite
traditionnelle, qui cherche d'abord à se trouver un champion pour la
prochaine élection présidentielle, ou bien le nouveau gouvernement
Valls II, à la ligne sociale-libérale, dorénavant assumée avec un
ancien banquier de chez Rothschild à la tête du ministère de l'économie.

Seuls l'extrême droite et le Front National tirent aujourd'hui parti
de la situation et arrivent à capter des pans entiers du monde du
travail par un discours prétendument anti-système alors qu'elle n'est
qu'un autre visage de la domination bourgeoise et capitaliste.
Sa poussée aux dernières municipales puis son arrivée en tête lors des
élections européennes ne doivent ni êtres considérées comme une dérive
fasciste du pays ni êtres prises à la légère.

L'extrême droite ne prospère qu'à partir de deux points : l'incapacité
du mouvement social à lui répondre et à reprendre les choses en main
ainsi que par une véritable désaffection vis à vis des institutions
(qui se caractérise par une abstention de plus en plus forte à toutes
les élections intermédiaires et par un rejet de plus en plus massif
de la classe politique traditionnelle).

Nous ne pouvons en aucun cas lui laisser la main et devons être en
capacité de porter nos revendications afin de la contrer et d'être
une alternative pouvant répondre à la crise économique, à la crise
politique et pouvant proposer un changement de société libertaire
face à la tyrannie capitaliste.

Pour cela, la période qui s'ouvre pendant deux ans et demi, c'est à
dire une période sans aucune élection, doit nous être favorable et
doit nous permettre de ré-enclencher une dynamique à l'intérieur
du monde du travail et du mouvement social.
Nous ne pourrons toutefois pas réussir ce travail au sein du seul
cadre de notre organisation.

La situation exige la mise en place d'une véritable unité d'action
entre l'ensemble des organisations anarcho-syndicalistes à l'intérieur
du mouvement syndical puis entre l'ensemble des organisations qui se
veulent une alternative au système capitaliste dans le mouvement
social, unité d'action qui est aujourd'hui non seulement utile
mais surtout indispensable si nous voulons que nos revendications
et fonctionnements rencontrent un écho et contribuent à la réussite
de nos orientations politiques et syndicales.


Motion
=======

Il est demandé au Congrès la création d'une commission de travail à
l'échelle confédérale à qui il est confié les mandat suivants :

1. Prendre l'initiative de mettre en place des rencontres entre les
   différentes organisations syndicales anarchistes et libertaires
   (CNT-AIT voire CNT-SO) avec pour objectif de construire une
   unité d'action et ainsi préparer des campagnes syndicales (matériels
   communs) sur des thèmes communs ;
2. Le Congrès demande au Bureau exécutif d’oeuvrer à des regroupements
   anarchistes et libertaires dans le cadre du mouvement social
   (CGA, FA, AL, Autonomes) et de travailler sur des thèmes communs ;
3. Le Congrès demande aux syndicats d'évaluer leur éventuelle
   participation à des regroupements *anticapitalistes* larges
   (NPA, AL, Ensemble, décroissants, non organisé-e-s …) sur nos propres
   bases à l'échelle locale ;
4. Dans le but de rendre cette unité d'action effective localement, le
   Congrès mandate les différentes UR et les syndicats à prendre les
   initiatives pour des rapprochements, ou à s'intégrer dans des cadres
   existants, sur les bases définies par la Commission de travail.


Amendements
===========

.. seealso::

   - :ref:`amendement_motion_10_2014_ptt66`
   - :ref:`amendement_motion_10_2014_sse31`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°10 2014

       :ref:`STE49 <ste49>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
