
=======================================================================================================
Vendredi 28 mai 2021 formulaire d'inscription au congrès confédéral juin 2021 <interco.21@cnt-f.org>
=======================================================================================================

- https://framaforms.org/congres-cnt-25-27-juin-2021-dijon-1574424395
- https://lite.framacalc.org/9mbn-covoituragecongrescnt


::

    Date: Mon, 24 May 2021 17:18:11 +0200
    From: CNT 21 <interco.21@cnt-f.org>
    To: liste-syndicats@bc.cnt-fr.org
    Cc: ul@interco21.cnt-fr.org
    Subject: [Liste-syndicats] Re : formulaire d'inscription au congrès confédéral juin 2021

    Salut,
    Le congrès, c'est dans un mois !
    Pour rappel, le formulaire pour inscription au congrès se trouve ici:
    https://framaforms.org/congres-cnt-25-27-juin-2021-dijon-1574424395
    -Merci de le remplir rapidement, on aimerait avoir toutes les
    informations d'ici deux semaines.
    -Merci de le remplir une fois par personne.

    Nous ne gérons pas le covoiturage pour venir à Dijon,
    mais je suggère d'utiliser le framacalc suivant :
    https://lite.framacalc.org/9mbn-covoituragecongrescnt

    N'hésitez pas si vous avez des questions,
    Salutations ASSR,

    ned,
    pour l'UL CNT 21
