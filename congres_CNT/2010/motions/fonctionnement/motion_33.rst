

.. _motion_33_2010:

===============================================================================
Motion n°33 2010 : Double affiliation syndicale ou «double carte», ETPRECI 35
===============================================================================



.. seealso::

   - :ref:`motions_etpreci35_2010`

Motion reprise en 2012 par Chimie-Bzh
======================================

- :ref:`motion_37_2012`

Argumentaire
=============

Une motion du précédent Congrès a été mal comprise par de nombreux syndicats
parce que sa mise au vote présentait des choix multiples: «La CNT a toléré
depuis une vingtaine d’année suite à une décision de Congrès et jusqu'à présent
l’usage de la double affiliation syndicale dans les secteurs du Livre et des
Docks en raison du monopole syndical et du monopole d’embauche de la CGT
dans ces deux secteurs.

Aujourd’hui des camarades travaillants en dehors de ces deux secteurs se
trouvent dans des situations où la menace de répression et de licenciement
les poussent à se syndiquer dans un autre syndicat en plus d’être syndiqué
à la CNT.

Mais d’autres camarades travaillants dans la même entreprise ou le même secteur
et uniquement syndiqués CNT, peuvent subir l‘agression de membres d’autres
syndicats, dont les doubles encartés sont aussi membres : il y a un conflit
dés qu’il s’agit d’affirmer une position au sein de la CNT dés lors que certain(e)s
sont cénétistes et d’autres «double encartés », surtout dans la stratégie syndicale.

Cette motion a pour but de clarifier la position de la CNT vis-à-vis de la
double carte, les secteurs industriels où elle serait tolérée et qu’un débat
sur la place et l’identité de la C.N.T vis à vis des autres centrales ait
lieu dans chaque syndicat. »

La CNT n’est pas une tendance syndicale oeuvrant dans les autres syndicats.
Il y a de fait conflit d’intérêt à être double encarté. De plus, cela reflète
une attitude consumériste et opportuniste à l’antipode de nos principes de
fédéralisme et d’autogestion.

Motion
======

A l’exception des secteurs où règnent le monopole syndical à l’embauche,
à savoir actuellement les docks et le livre, la CNT n’autorise pas la double
affiliation syndicale.

**Nul(le) adhérent(e) d’un syndicat CNT ne peut être encarté dans un autre syndicat.**

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°33          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`amendement_motion_33_2010_stea`
