
.. _motion_d_2014_sipmcs:

=======================
Motion D 2014 (SIPMCS)
=======================




Motion D
=========

La commission “CS” est fusionnée avec la commission “site internet” de
la motion B.

Cette fusion donne lieu à la création d'une commission “Communication”
(ou tout autre nom qui conviendra au congrès, “CS et site internet” par
exemple).

Cette commission s'organise comme bon lui semble, certainEs pouvant se
concentrer sur le travail papier, d'autres sur le travail web.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion D 2014 SIPMCS

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
