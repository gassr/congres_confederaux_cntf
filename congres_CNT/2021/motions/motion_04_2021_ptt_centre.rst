.. index::
   pair: Motion 4 ; 2021


.. _motion_4_2021_ptt_centre:

================================================================================================================================================
Motion **n°4** 2021: **Motion Fédéraliste pour une réunification** par **PTT Centre**
================================================================================================================================================


Autres motions PTT-Centre

    - :ref:`syndicats:motions_ptt_centre`


Argumentaire
==============

Voir :ref:`Annexe n°1 2021 <annexe_1_2021_ptt_centre>`

Motion
========

La Confédération Nationale du Travail s’engage à mettre en œuvre par tous les
moyens à sa portée la convergence et la réunification des différentes
composantes anarcho-syndicalistes et syndicalistes révolutionnaires souvent
issues des scissions de notre Confédération.

Elle le fait pour garantir la véritable expression du fédéralisme libertaire,
l’autonomie des syndicats à la base de notre projet, la cohérence et la cohésion
de ce projet et en vue d’aboutir au renforcement des capacités et des moyens
d’action, de l’audience de **l’anarcho-syndicalisme et du syndicalisme révolutionnaire**.


Amendement
===========

- :ref:`amendement_motion_4_2021_stics72`

Contre-motion
================

- :ref:`contre_motion_4_2021_etpreci75`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°4 2021 PTT Centre  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
