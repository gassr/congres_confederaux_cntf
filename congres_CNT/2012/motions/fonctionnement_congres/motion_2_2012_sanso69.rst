
.. _motion_2_2012:
.. _motion_2_2012_sanso69:

=====================================================================================
Motion n°2 2012 : Modalités de vote lors des congrès confédéraux, Sanso 69 [adoptée]
=====================================================================================
  
Modalités de vote lors des congrès confédéraux (Règles organiques, titre IV sur
les modalités de vote)


.. seealso::

   - :ref:`congres_CNT_2010_saint_etienne`
   - :ref:`sanso69_motions_congres_2012`



Argumentaire
============

Lors du dernier :ref:`Congrès de Saint-Étienne <congres_CNT_2010_saint_etienne>`,
les *ne prend pas part au vote* ont  été de fait «assimilés» à des
abstentions et comptabilisés parmi les votes exprimés pour l'adoption des motions.

Il semble important que la possibilité de réellement ne prendre pas part au
vote soit offerte aux syndicats de facon spécifique. En efet, un syndicat peut,
pour des raisons qui lui sont propres, penser que telle ou telle motion ne lui
semble pas pertinente. Il décide donc de ne pas en débattre, et cette décision
se retrouve exprimée en congrès par ``ne prend pas part au vote (NPPV)``.

Cette position doit donc apparaitre concrètement dans la comptabilisation des
votes, en diminuant le nombre de votant servant à définir le seuil
d'admissibilité d'une motion.

Exemple::

    50 syndicats présents.
    15 syndicats ne prennent pas part au vote.

Nombre de votants total 35. Le calcul entre les Pour/Contre/Abstention pour
savoir si la motion est adoptée ou refusée, se fera donc sur ce total de
35 votants.

Motion
======

Le seuil d’admissibilité d’une motion est de 50% de votes «pour» sur la base
des votes exprimés (pour, contre, abstention).

Les votes « ne prend pas part au vote » sont exclus du nombre des votes exprimés
servant de base à la définition de la majorité d'adoption d'une motion.

Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°2           |            |          |            |                           |
| SANSO69              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Decision du Congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Amendements
============

- :ref:`amendement_motion_2_2012_stea`
- :ref:`amendement_motion_2_2012_etpics57`

Contre motions
==============

- :ref:`contre_motion_2_2012_ess34`
- :ref:`contre_motion_2_2012_stp72`

Décision
========

.. seealso::

   - :ref:`motions_deroulement_des_congres`


.. warning:: Lors des congrès de la CNT, les NPPV (Ne Prend Pas Part au Vote)
   ne doivent pas être comptabilisés comme suffrage exprimé.
