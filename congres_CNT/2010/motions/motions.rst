
.. index::
   pair: motions ; XXXIe congrès de Saint Etienne (2010)

.. _congres_CNT_2010_motions:

========================
Motions Congrès CNT 2010
========================


.. toctree::
   :maxdepth: 2


   fonctionnement/index
   strategie/strategie
   statuts/index
   internet/internet
   campagnes/index
   publications/index
   propagande/index
   autres/index
   synthese/index
   etats_motions/index
