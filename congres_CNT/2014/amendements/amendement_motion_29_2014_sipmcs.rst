
.. _amendement_motion_29_2014_sipmcs:

================================================
Amendement à la motion n°29 2014 (SIPMCS)
================================================

.. seealso::

   - :ref:`motion_29_2014_ste75`




Préambule
=========

Nous sommes d’accord avec la logique mais estimons qu’il ne faut pas que
«CNT» apparaisse dans les adresses mails, afin qu’il ne puisse y avoir
d’équivoque sur le fait que à la CNT seuls les syndicats et les
«structures mandatées» peuvent parler au nom de la CNT.

Amendement
===========

Modification de la première phrase::

    Création d’un nom de domaine pouvant héberger des adresses mails
    individuelles pour les adhérents de la CNT, nom de domaine ne pouvant
    comporter « CNT » dans son intitulé, afin de ne pas créer de confusion
    entre prises de positions individuelles et prises de position de la CNT
    ou de ses syndicats.
    Ces adresses sont créées pour tout adhérent qui en fait la demande par
    l’intermédiaire de son syndicat. [Suite motion identique]

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°29 2014 <motion_29_2014_ste75>`

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
