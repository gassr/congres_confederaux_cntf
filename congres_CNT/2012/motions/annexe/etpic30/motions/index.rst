
.. index::
   pair: Motions ; Stratégie syndicale CNT ETPIC Gard sud (2012)


.. _motions_strategie_syndicale_etpic_30_2012:

==========================================================================================================
Motions Action syndicale et militante & Organisation organique et technique de la CNT (ETPIC Gard Sud, 30)
==========================================================================================================


.. toctree::
   :maxdepth: 2

   motion_1_info_etpic_30
   motion_2_etpic_30
   motion_3_etpic_30
   motion_4_etpic_30
   motion_6_etpic_30
