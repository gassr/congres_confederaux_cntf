

.. _motions_international_cnt_congres_agen_2006:


==============================================================================
Motions "international" 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
==============================================================================


Solidarité avec les communautés zapatistes & implication à la future intergalactique
====================================================================================


Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 51         | 1        | 5          | 6                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le prochain secrétariat international aura pour mandat concernant le Chiapas:

- de développer et renforcer la solidarité avec les communautés zapatistes ;
- de travailler sur le volet international de la sixième déclaration en
  s'impliquant dans les initiatives développées ici autour de ce texte et
  dans la préparation de la future réunion intergalactique (notamment en
  liaison avec le comité de solidarité avec les peuples du Chiapas en lutte).
  Il mènera un travail de réflexion à l'intérieur de la confédération pour que
  les positions prises par les mandatés à cette rencontre soient l'émanation
  d'un positionnement officiel de notre confédération.


Stratégie internationale
========================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 37         | 7        | 9          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La CNT travaille au niveau international avec :

- les autres structures ASSR (CGT, SAC, FAU, IWW, Solidarida Obrera, etc.) ;
- les syndicats de lutte (SIMECA en Argentine, SNAPAP en Algérie, Palestine,
  CGT-B au Burkina Faso, etc.) ;
- les structures (sections d'entreprises, unions locales, etc.) syndicales
  combatives faisant partie de confédération «réformistes» (UMT Rabat au Maroc,
  syndicalistes colombiens, etc.) ;
- en cas d'absence de structures syndicales des trois types précédents pour
  des raisons de régime politique dictatoriaux, travail avec des organisations
  (hors partis politiques et organisations à référence religieuse) luttant
  pour les libertés fondamentales d'organisation, d'expression, etc.
  La solidarité dont la CNT peut faire preuve envers ces organisations
  n’est et ne peut être aucunement un alignement sur leurs positions.
  Ainsi, en aucun cas la signature de la CNT ne peut et ne doit être un
  préalable aux relations existantes ou à venir entre elles et nous.



Position internationale vis-à-vis des situations dictatoriales impérialistes ou néocolonialistes
================================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 44         | 3        | 11         | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Face à ces régimes, la CNT :

- dénonce les soutiens internationaux à ces dictatures ;
- réclame l'application des libertés fondamentales d'organisation, d'expression,
  etc. ;
- respecte et soutient les revendications de ses partenaires éventuels dans la
  limite des principes fondateurs de la CNT qui sont l'indépendance vis-à-vis
  des partis politiques, des structures religieuses et des États (pas d'appel
  à voter, pas de soutien à des institutions, etc.)


Mobilisations internationales AS & SR
=====================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 47         | 5        | 8          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le SI devrait essayer au maximum dans les années à venir de relancer les
mobilisations internationales ASSR communes, notamment à l'occasion des
événements sociaux européens (cortèges rouges et noirs comme à Amsterdam,
Cologne ou Gôteborg).

Il doit dans le même ordre d'idées relancer la vocation internationaliste
du camping confédéral de Masseube en invitant les militants des autres
organisations ASSR et en leur proposant d'organiser des débats.

I 2007
======

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 50         | 6        | 4          | 7                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Le congrès repousse les rencontres internationales de 2006 à Avril-Mai 2007
afin d'éviter de se lancer dans l'organisation d'une telle rencontre sans
préparation suffisante et en outre lors d'un changement de secrétariat
international.
Les principes de la rencontre restent celles des rencontres de ce type déjà
organisées (199 à San Francisco et 102 à Essen), c'est-à-dire des rencontres
industrielles de militants d'organisations syndicales de lutte de classe à la
base.

Cet événement sera l'occasion d'une journée de lutte et de mobilisation, même
symbolique : manifestation rouge et noire, concert, etc.


I2007 sera organisé en Région parisienne par une commission syndicale régionale
en collaboration avec le SI, en insistant sur la nécessité que les syndicats
prennent en main cette organisation afin que l'événement ne soit pas une
rencontre de mandatés et de secrétaires internationaux.


Coordination avec les organisations anarcho-syndicalistes et syndicalistes révolutionnaires
===========================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 45         | 2        | 11         | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Ces dernières années, le travail de la CNT au niveau international s'est
structuré principalement autour des organisations anarcho-syndicalistes et
syndicalistes révolutionnaires suivantes:

- la CGT-e (Espagne)
- la SAC (Suède)
- la SKT (Sibérie),
- la KASNN(Ukraine),
- quelques groupes des IWW (USA), de l'USI (Italie) et des organisations
  plus récemment constitués comme l'IAP polonaise et les AS grecs.

Les liens qui unissent ces organisations à la CNT sont un élément décisif dans
la construction stable d'une coordination d'organisations anarcho-syndicalistes
et syndicalistes révolutionnaires au niveau international.


Solidarité avec la Palestine
============================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 41         | 3        | 6          | 8                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

La CNT :

- réaffirme son opposition à toute forme de colonisation et d'occupation.
- réaffirme sa volonté de travailler directement avec les travailleurs, les
  syndicats et les organisations de base palestiniens en dehors de toute
  représentation politique quelle qu'elle soit.
- réaffirme que sa solidarité ira toujours aux occupés et non aux occupants,
  aux opprimés et non aux oppresseurs.


Antimilitarisme international
=============================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 43         | 9        | 7          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

La CNT est antimilitariste, il est donc important qu’au niveau international
les choses soient claires aussi.

Donc le mot d’ordre doit être le retrait pur et simple de l’armée française de
tous les territoires et donc sa dissolution.

D’autre part, les autres armées doivent aussi être retirées et dissoutes.

Nous rappelons que les casques bleus sont aussi une armée (les dernières
guerres, du Rwanda à la Côte d’Ivoire, l’ont montré) et qu’elle est à la solde
du conseil de sécurité de l’ONU, et donc du capitalisme.


Abandon définitif du sigle AIT
==============================


Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 46         | 4        | 8          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Le Congrès réaffirme la nécessité pour tous les syndicats de respecter l’image
de la confédération et de définitivement abandonner l’usage du sigle AIT.

Les Unions Régionales sont chargées de faire appliquer cette motion
