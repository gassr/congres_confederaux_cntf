.. index::
   pair: Motion; Recueil (2001)



.. _motion_recueil_2001:

===============================================================
Création d’un recueil des motions de Congrès en vigueur (2001)
===============================================================




Vote
=====

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 19         | 0        | 14         | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
======

Depuis 1993 le Congrès confédéral charge une commission d'établir un recueil
des motions en vigueur.

Ce recueil devra organiser un classement par catégorie  et date des
motions et il pourra éventuellement y être annexés des documents
consultatifs en vue d'obtenir un ouvrage clair et unique.

A l'issue de chaque Congrès, ce recueil devra être mis à jour par
le bureau  confédéral ou par voie de délégation sous sa responsabilité.

Chaque syndicat pourra sur demande disposer d'un exemplaire laisse à
la libre consultation de ses adhérents.
