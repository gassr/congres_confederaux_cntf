
.. _contre_motion_18_2014_stp67:

=============================================
Contre-motion à la motion n°18 2014 (STP67)
=============================================

.. seealso::

   - :ref:`motion_18_2014_sinr44`




Argumentaire 
=============

Pour nous, le problème ne se pose pas pour les organisations politiques ne se
présentant pas aux élections.


Contre-motion 
==============

Les membres de la CA ne pourront occuper aucun poste responsable relevant d'une
organisation ou d'un groupement politique se présentant à des élections
(hors élections professionnelles), d'une secte philosophique ou religieuse.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°18 2014 <motion_18_2014_sinr44>`

       :ref:`STP67 <stp67>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
