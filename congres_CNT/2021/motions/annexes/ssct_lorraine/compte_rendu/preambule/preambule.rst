

.. _preambule_rendu_2017_03_05:

============
Préambule
============


Avant toute chose, notre syndicat tient à revenir sur le contexte dans
lequel il dû, ces derniers mois, gérer une affaire de la plus grande
gravité et de la plus grande complexité.

**De la plus grande gravité**, parce que nos convictions sont féministes
et que les agressions faites aux femmes par des hommes dans le contexte
d'une société patriarcale que nous combattons font parties à nos yeux
des pires injustices qui soient.

Parce que l'acte de viol en lui-même, dont nous ne remettrons à aucun
moment en cause la définition politique - que nous partageons tou·tes et qui,
vous le savez bien, est incomparable à la définition que peut en faire
le cadre législatif en vigueur - est à nos yeux **le crime le plus odieux
qui soit**.

Parce qu'aussi, et quel que soit le fin mot de l'affaire que nous avons
dû traiter, celle-ci a des conséquences humaines irréversibles pour de nombreuses
personnes, bien au-delà de la situation individuelle de l'une et l'autre
des parties d'ailleurs, dans leur entourage et dans le milieu militant.

De la plus grande complexité parce quoi qu'il en soit, cette affaire
particulière est inévitablement imbriquée avec nombres de réflexions
d'ordre politique qu'elle soulève aujourd'hui dans la CNT, et bien au-delà,
des questions fondamentales et incontournables à notre engagement militant.

Au travers de l'affaire particulière que nous avons traitée, en l'occurrence
l'accusation de viol à l'encontre de l'un de nos adhérents, nous le voyons
bien, ce sont des débats profonds et sensibles qui ont émergé ces derniers
mois autour des notions de féminisme, de justice, mais aussi autour du
projet politique que nous défendons et de la société future à laquelle
nous aspirons.

**Aussi, notre syndicat regrette profondément de ne pas avoir pu traiter
cette affaire dans un cadre serein.**

Depuis plusieurs semaines, notre syndicat a été entouré d'une pression
que nous n'avions jamais vécue. Au-delà de notre décision concernant
notre adhérent, nous avons eu le sentiment d'être au cœur d'enjeux politiques
qui dans une certaine mesure nous dépassent, d'avoir dû incarner une
responsabilité au nom de l'ensemble du mouvement social.

En effet, l'ensemble de nos adhérent·es ont eu le sentiment que la
décision que nous devions prendre serait dans un sens comme dans
l'autre, décisive dans le cadre d'un affrontement interne entre
différentes visions de la justice ou du féminisme, que cette décision
engagerait dans tous les cas la physionomie de la CNT - peut-être son
unité - sa place au sein du mouvement social, du mouvement syndical, et
du mouvement libertaire.

Cette accusation de viol contre l'un de nos adhérents nous a placé dans
une situation de responsabilité extrême que notre syndicat n'a non
seulement jamais sollicitée, mais que l'immense majorité de nos adhérent·es
n'avaient même jamais imaginée en adhérant à la CNT.

Aussi notre syndicat déplore avec véhémence que les syndicats de la CNT,
dans leur immense majorité n'aient pas contribué à installer un cadre de
réflexion serein qui aurait été propice à unedécision la plus objective
possible, et qui **aurait sans doute pu préserver la vie personnelle de
certain·es de nos adhérent·es : et ici, nous ne parlons pas de Fouad**.

Nous tenons donc, avant de vous exposer notre réflexion et notre décision,
à **dénoncer les agissements des syndicats et militant·es qui depuis des
semaines ont tout fait pour que la décision de notre syndicat se fasse
sous la pression, mais aussi sous la menace**.

Nous avons d'abord essuyé les **procès d'intention**: avant même que nous
soyons saisis concrètement de cette affaire certain·es nous expliquaient
que si nous n'allions pas dans le sens de la victime supposée ou que nous
ne défendions pas une certaine version pourtant discutable du féminisme,
**alors nous serions d'horribles masculinistes, voire des complices de viol**,
ne laissant ainsi aucune place au débat.

Et cela, avant même que nous nous penchions sur le problème nous nous sommes
senti·es méprisé·es, insulté·es, et menacé·es du pire des dénigrements
si jamais nous ne prenions pas la décision attendue par certain·es.

Nous comprenons bien l'émotion et les enjeux politiques de cette affaire,
mais tout du long, nous avons eu le sentiment que notre syndicat n'était
pas libre ni autonome dans sa réflexion, **car les individu·es et structures
de la CNT qui avaient relayé ces accusations ont toujours refusé tout échange**,
préférant **nous imposer une marche à suivre préalablement définie par
eux·elles-mêmes** et dont tout écart nous vaudrait la vindicte plutôt que
le débat.

Nous avons ensuite dû composer avec les menaces d'exclusion de la part de
certains syndicats de la CNT, menaces qui sont d'ailleurs intervenues
avant même que nous puissions engager notre réflexion, et **nous tenons
donc à dénoncer à nouveau cette grave atteinte au fonctionnement de la
CNT qui a remis clairement en cause l'autonomie de notre syndicat et la
liberté de réflexion de nos adhérent·es au sein de notre Assemblée Générale**.

Ces menaces d'exclusion - par ailleurs parfaitement inutiles alors que
notre syndicat, adhérent de la CNT, connait et reconnaît le fonctionnement
statutaire de son organisation et particulièrement :ref:`l'article 24 <article_24_statuts_CNT_2014>`
nous ont contraintes à devoir mener une réflexion et à prendre une décision
avec un **véritable couteau sous la gorge**.

**Nous tenons par ailleurs à dénoncer avec la plus grande fermeté, l'attitude
d'Alternative Libertaire par le biais de son communiqué de presse et
public concernant l’exclusion de Fouad**.

Nous la dénonçons d'abord parce que cette organisation, qui n'a franchement
pas cherché à éclairer notre syndicat dans sa réflexion, a fait le **choix
politique de mettre sous pression notre Assemblée Générale, mais également
l'ensemble de la Confédération**.

Dans son communiqué national, c'est bien une **injonction à exclure** qui nous
est faite, ainsi qu'à la CNT dans son ensemble et nous regrettons qu'AL
ait préféré user de ce communiqué pour **attaquer la CNT sur la plan politique**,
plutôt que de nous aider au préalable à prendre notre décision au sujet
d'un adhérent accusé de viol.

**Nous dénonçons également cette communication d'AL au regard de nos convictions
humanistes**.

Soyons clairs : même s'il s'avérait que notre adhérent soit coupable de
viol, nous rejetons en bloc cette pratique de la mise au pilori des
criminels qui nous semble bien éloignée de nos convictions libertaires
et de la notion de dignité humaine.

Cette pratique, digne de la police et de la presse aux États-Unis qui
diffusent avec plaisir les photos des criminels, a des conséquences
dramatiques que nous ne souhaitons à aucun être humain, quel que soit
son degré de culpabilité.

Cette communication d'AL qui cite clairement le nom et la ville de
résidence de Fouad aura probablement des **conséquences extrêmement lourdes**.

**Soyons explicites : nous ne qualifierons pas autrement cette communication
que d'incitation au suicide, voire d'appel au meurtre**.

Sans parler des conséquences qu'une telle communication peut avoir, non
pas seulement pour Fouad lui-même, mais pour ses parents et son entourage,
qui jusqu'ici ne sont impliqués dans aucune histoire de complicité de viol
et vivent une existence bien éloignée de nos problématiques militantes,
nous nous interrogeons sur la vision qu'Alternative Libertaire, tout
comme d'autres dans le milieu libertaire, peuvent avoir sur la question
de la peine de mort...

Outre l'attitude d'Alternative Libertaire en organisation politique
extérieur à la CNT, nous tenons également à pointer du doigt le fait
que certains syndicats de la CNT, tel que l'interco 54 aient fait ce
même choix de communication publique et nominative (rajoutant d'ailleurs
le nom de famille de Fouad, qu'AL s'était tout de même abstenu de mentionner)
avant même que la procédure interne à la CNT n'ait abouti.

Notons finalement que sur le plan politique, ces communiqués ont non
seulement placé la victime supposée dans une situation de danger encore
plus important, mais ont visiblement énormément plu aux mouvances fascistes
qui jubilent publiquement des déchirements internes aux organisations
antifascistes et **alimentent sans retenue leurs réflexions islamophobes
et relayant ces communications sans même les modifier !**


.. _role_de_linterpro_31_fouad:

Les interventions de certains syndicats CNT et notamment l'interpro31 dans l'affaire Fouad
-------------------------------------------------------------------------------------------

Finalement, le summum de cette mise sous pression a sans doute été atteint
à la lecture, ces derniers jours, des agissements de différent·es adhérent·es
de la CNT sur les réseaux sociaux.
Outre celles et ceux qui ont fait le même choix qu'AL en terme de diffusion
publique (et dont nous nous demandons donc si eux·elles aussi défendent
la notion de peine de mort), **nous avons été sidéré** de constater que
certain·es adhérent·es de la CNT, notamment une adhérente de
:ref:`l'interpro 31 <syndicats:interpro31_motions_congres>` proféreraient
d'ores et déjà des menaces de mort publiques à l'encontre de Fouad.

Pour cette Cénétiste, qui s'affiche publiquement en tant que telle, et
qui **participe par ailleurs à la commission antisexiste**, il s'agit de
savoir s'il vaux mieux « couper les couilles » des violeurs ou les « jeter
[Fouad] dans le canal du midi » (avec une préférence pour la deuxième option).

Quelle que soit la culpabilité ou non de Fouad, **ces militant·es font
honte à la CNT et nous ont poussé à réfléchir, en plus de la question
d'un viol, une question de vie ou de mort**.

En conclusion, **nous déplorons donc de manière globale, et cela indépendamment
des conclusions que nous nous apprêtons à rendre sur le fond, le peu de
soutien que nous avons reçu de la part des structures de la CNT pour aider
notre syndicat à gérer sereinement cette affaire de la plus grande complexité.**

Nous aurions préféré avoir pu bénéficier d'un cadre serein et d'appuis
dans notre réflexion, mais nous avons eu le sentiment d'être totalement
livré·es à nous-même et insulté·es, menacé·es, et donc contraint·es
d'assumer nos responsabilités sous la pression, avec cette épée de
Damoclès permanente que l'on nous a brandi tout au long de notre réflexion.

**Pour une organisation qui se dit solidaire et féministe, cela nous pose
question**.

Nous tenons néanmoins à remercier le :ref:`STE 75 <motions_ste75>`, le :term:`Bureau Confédéral` et les
autres syndicats qui, sans jamais se poser en défenseur·euses de
l'accusé, ont **préservé avec intégrité l'autonomie de notre réflexion**.

:ref:`conclusion_al`
