
.. index::
   pair: Motions; Stratégie syndicale 2012
   pair: Stratégie syndicale ; 2012


.. _motions_strategie_syndicale_2012:

======================================
Motions stratégie syndicale 2012
======================================

.. toctree::
   :maxdepth: 4

   motion_10_2012_ptt95
   motion_11_2012_ptt_centre
   motion_12_2012_etpics94
   motions_13_14_2012_etpics57
   motions_15_16_2012_stea
   motion_17_2012_etpic30
   motion_18_2012_etpic30
   motion_19_2012_etpic30
   motion_20_2012_etpic30
