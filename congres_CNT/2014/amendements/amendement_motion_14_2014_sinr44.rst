


.. _amendement_motion_14_2014_sinr44:

===========================================
Amendement à la motion n°14 2014 (SINR44)
===========================================

.. seealso::

   - :ref:`motion_14_2014_interco25`




Argumentaire 
=============

Il nous paraît extrêmement fastidieux et lourd de consulter tous les
syndicats à chaque déplacement envisagé d'une délégation du Secrétariat
international, d'autant plus si ce référendum est fait dans les formes
(consultation faite par courrier postal, avec délai de réponse), et en
connaissant les probabilités de réponse des syndicats à un tel
référendum (lors de la plupart des consultations confédérales même
par mail, peu de réponses sont souvent obtenues.

Nous préférons donc que ces décisions soient prises par la CA, puis
validée par les UR lors des CCN.

Amendement (modification en gras) 
=================================

Le secrétariat international (SI) a pour rôle d'effectuer les tâches
suivantes:

- synthèse et information aux syndicats de l'activité des organisations
  anarcho-syndicalistes et syndicalistes révolutionnaires (toutes
  tendances confondues) et des campagnes de solidarité organisées,
  par une circulaire ou un bulletin au moins trimestriel,
- rédaction d'une rubrique internationale dans le Combat syndicaliste,
- réponse aux demandes d'organisations étrangères, transmission aux
  syndicats des demandes dépassant la compétence du SI, information
  des organisations étrangères sur l'activité de la CNT,
- si la participation de la CNT à la Coordination rouge et noire est
  confirmée : traduction et transmissions aux syndicats de toutes les
  informations nécessaires à leur participation effective.
- pour réaliser ce travail, coordination d'une commission de
  traducteur.trice.s,
- La signature de textes collectifs de solidarité et les déplacements
  du SI à l'étranger sont soumis à une **consultation de la Commission
  administrative confédérale puis à une validation au CCN** suivant.
  Les rencontres et rassemblements en France sont organisés en commun
  avec les structures locales de la CNT, les rencontres de branche avec
  les fédérations et syndicats concernés.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°14 2014 <motion_14_2014_interco25>`

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
