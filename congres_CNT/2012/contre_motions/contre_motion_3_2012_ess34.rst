

.. _contre_motion_3_2012_ess34:

==========================================
Contre-motion à la motion N°3 2012 ESS34
==========================================
.. seealso::

   - :ref:`motion_3_2012_ste72`





Argumentaire
============

Lors des congrès de la CNT, les NPPV (Ne Prend Pas Part au Vote) ne doivent
être comptabilisé comme sufrages exprimés.

S'il y a plus de 25 % de «ne prend pas part au vote», la motion ne peut pas
être votée.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°3      |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2012`
