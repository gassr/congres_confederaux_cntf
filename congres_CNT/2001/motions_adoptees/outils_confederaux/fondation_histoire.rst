
.. index::
   pair: Fondation ; Histoire


.. _cration_fondation_histoire_2001:

===================================================================
Création d’une fondation de l’histoire du mouvement ouvrier (2001)
===================================================================





Vote
=====

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 37         | 4        | 6          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
=====

Le principe de la fondation et la création d'une commission pour faire un
projet et travailler d'abord sur la motion juridique d'une fondation, sont
adoptés.
