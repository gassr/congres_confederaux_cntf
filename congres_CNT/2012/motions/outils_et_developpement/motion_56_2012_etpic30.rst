
.. _motion_56_2012:
.. _motion_56_2012_etpic30:

=========================================================================================
Motion n°56 2012 : Évolution du tarif et du format du Combat syndicaliste (ETPIC 30 Sud)
=========================================================================================
  

.. seealso::

  - :ref:`motions_etpic30`
  - :ref:`motions_presse_2004`





Motion similaire à la motion 48 2010 ETPRECI35
==============================================

- :ref:`motion_48_2010`


Le Combat Syndicaliste
======================

- :ref:`combat_syndicaliste`

Argumentaire
=============

Le Combat syndicaliste connait des évolutions qualitatives importantes ces
dernières années mais sa diffusion reste trop limitée.

La vocation première de l’organe mensuel de la CNT est de connaitre une
diffusion la plus large possible, comme outil de presse et de propagande.

En lien avec la mise en ligne intégrale du Combat Syndicaliste et à son
accès gratuit à tous, nous proposons que la version papier connaisse une
évolution du format et du prix pour une diffusion plus aisée et la plus
large possible.

Le format actuel, certes esthétique, nous apparait trop lourd et trop
encombrant.

Nous privilégions aujourd’hui une impression plus légère, voire moins chère,
de type format ``poche`` (type nouveau format des magazines en A5 ou même
en format A4 sur papier journal) sans pour autant en diminuer le nombre
et la qualité des articles.

En termes de coût, l’Imprimerie 34, SCOP à laquelle nous faisons appel
actuellement, nous indique les tarifs suivants à titre indicatif :

.. figure:: tarifs_impression_cs.png
   :align: center

   Tarifs d'impression du CS

Il nous semble à l’avenir pertinent de tendre à la gratuité, ou prix libre.


Motion
======

Le Combat Syndicaliste a pour vocation d’être diffusé le plus largement
possible.

Son format et son prix doivent être rendus plus accessible (lecture gratuite
sur internet, format plus ``ergonomique``, prix minimum voire  prix libre).

Le Combat Syndicaliste change de format pour passer à un format plus petit et
plus léger en termes d’épaisseur papier.

Son coût doit être réduit au maximum,  tout en intégrant autant que possible
l’utilisation du papier recyclé (:ref:`motion du 28e Congrès confédéral <motions_presse_2004>`
de janvier 2004) et en conservant une belle facture.

Le Comité de rédaction, l’équipe d’administration, et la « commission CS en
ligne » sont en charge d’assurer collectivement cette étude et de proposer
des formules format cout / tarif au CCN suivant ce Congrès.

Les CCN suivant le Congrès confédéral seront en charge de valider ces propositions.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°56          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
