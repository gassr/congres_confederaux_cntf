
.. _contre_motion_14_2021_cnt09:

=================================================================================================================
Contre-motion N°2 à la motion n°14 2021 **Financement confédéral des groupes Femmes Libres** par **INTERPRO09**
=================================================================================================================

- :ref:`motion_14_2021_syndicats_42`


Autres motions CNT09

    - :ref:`motions_interco09`


Argumentaire
================

Nous sommes tout à fait favorable à l'argumentaire développé par les
syndicats du 42.

Par contre le financement énoncé dans le corps de la motion est très
nettement insuffisant. Il y a environ 600 syndiqués.

Cela donnerait un financement annuel de 60 € !


Contre-motion
===============

Nous proposons de créer un compte FL, ce qui reviendrait à avoir sept comptes :
CONF, International, Solidarité/Procédures, CS, Prêts, Propagande et FL.

La répartition des cotisations se ferait ainsi :

.. figure:: cnt09/tableau_cotis.png
   :align: center

Cette cotisation ne retire pas aux différents groupes la tâche de
s’autofinancer, mais engagerait la confédération dans la reconnaissance
et le soutien à leur/notre combat, qui s'inscrit dans la lutte
anarcho-syndicaliste.


Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°14 |            |          |            |                           |
| INTERPRO09                     |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
