
.. _amendements_motion_10_2021_sanso69:

=========================================================================================================================================
Amendements N°1 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **Sanso69**
=========================================================================================================================================

- :ref:`motion_10_2021_etpic30`

Autres motions sans69

    - :ref:`rhone_alpes:motions_sanso69`

Argumentaire général 
========================

Nous pensons comme essentielle une motion sur la lutte antisexiste et
antipatriarcale ainsi que la réactivation de la commission antisexiste
avec une définition de son mandat.

Globalement nous sommes favorable à cette motion mais dans le but de
l’améliorer nous souhaitons la modifier.
Les amendements peuvent être votés séparément.


.. _amendement_1_motion_10_2021_sanso69:

Amendement N°1 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **Sanso69**
============================================================================================================================================

Argumentaire 
----------------

Si c'est toujours utile de s'expliquer dans les argumentaires, nous ne
voyons pas l’intérêt dans la motion de se justifier ni d'expliquer
l’absence de moyen, autrement dit de définir la commission par ce
qu'elle n'est pas.

Amendement
-------------

Suppression de la totalité du  paragraphe:

**Si la Commission peut recueillir la parole, sur les différentes
problématiques rencontrées, elle n’a pas pour vocation de se poser
en juge.
En l’état de son développement, la CNT ne dispose d’aucun moyen suffisant
pour conduire des investigations.**



Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
|  n°10 Sanso69               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. _amendement_2_motion_10_2021_sanso69:

Amendement N°2 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **Sanso69**
============================================================================================================================================

Argumentaire 
----------------

Le rôle  de la commission de conseils apportés aux syndicats est important
et est bien défini dans le dernier paragraphe.
Rajouter la possibilité d’émettre un avis consultatif est inutile et
redondant (à partir du moment où on conseille, on donne un avis consultatif)


Amendement
-------------

Dans le dernier paragraphe, suppression de la dernière phrase

**"Elle peut émettre dans ce  dernier cas un avis consultatif."**



Vote
------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
|  n°2 Sanso69                |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
