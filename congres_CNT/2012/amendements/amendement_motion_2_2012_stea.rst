
.. _amendement_motion_2_2012_stea:

==========================================
Amendement à la motion 2 (STEA)
==========================================

.. seealso::

   - :ref:`motion_2_2012_sanso69`



Argumentaire
=============

Il nous semble essentiel de **prendre en compte le quorum** dans cette situation,
afin que le nombre de votes exprimés soient représentatifs du congrès de la CNT.

L'ajout à la :ref:`motion proposée par le Sanso 69 <motion_2_2012>` est en italique.

Amendement
===========

Le seuil d'admissibilité d'une motion est de 50% de votes «pour» sur la base
des votes exprimés (pour, contre, abstention), *dans la mesure où ces derniers
respectent le quorum, les votes exprimés devant représenter au moins 50% des
syndicats présents à l'ouverture du congrès*.

**Les votes « ne prend pas part au vote » sont exclus du nombre des votes exprimés
servant de base à la définition de la majorité d'adoption d'une motion**.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°2  |            |          |            |                           |
| STEA                        |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
