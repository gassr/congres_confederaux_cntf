.. index::
   pair: Motion 28 ; 2021


.. _motion_28_2021_etpreci75:

===================================================================================================================================================
Motion **n°28** 2021: (hors délai) **Proposition d’une commission de conciliation des problèmes internes au sein de la C.N.T** par **ETPRECI75**
===================================================================================================================================================

Autres motions ETPRECI75

    - :ref:`paris:motions_etpreci75`


Préambule
===========

Il apparait que depuis plusieurs mois un syndicat demandait à l’U.R.
de traiter un conflit avec un autre syndicat, il n’appartient à l’U.R
de traiter ce genre de problème, d’autant qu’aucun élément étayait cette
demande.

Cela à créer un malaise profond, des camarades ont quittés la C.N.T.,
d’autres sont en retrait de celle-ci.

Nous rappelons simplement les principes du fédéralisme, le droit et
l’autonomie de chaque syndicat, à condition que les statuts, les accords
de congrès, l’éthique, la pratique et le fonctionnement fédéraliste
soient respectés.

La fraternité, la solidarité d’entraide, voilà le projet du fédéralisme
libertaire.
La critique devrait être constructive et non l’inverse, ne nous trompons
pas d’ennemis, ceux-ci sont, l’état, les forces répressives, le fascisme,
le capitalisme et tous ceux qui veulent le pouvoir.

Motion 
========

Afin d’essayer de résoudre les conflits internes nous proposons (comme
par le passé…) de créer une commission de conciliation, sur la base de
bonnes volontés et volontariat ; un membre de chaque syndicat serait
souhaitable, ceux-ci n’auraient qu’un rôle technique et non décisionnel,
un bilan écrit serait réalisé avec l’accord des concernés.

Pour ce faire nous pensons que les syndicats concernés, participent aux
réunions de la commission en A.G de leurs adhérent-es.

Il ne s’agit pas d’un tribunal, mais bien de mettre sur la table les
problèmes et d’essayer en commun de les résoudre.

La commission serait donc tenue par un membre de chaque syndicat R.P.
dont le fonctionnement serait prise de note, limitation du temps de parole
et inscription de chacun-es des intervenant voulant s’exprimer.

Si cette proposition est accepté par les syndicats R.P., il restera aux
syndicats de la mettre en œuvre en faisant remonter leur accord ou
désaccord à cette proposition, (bien sûr celle-ci n’est pas exhaustive,
d’autres éléments peuvent y être adjoints).


Amendements
===========


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°28 2021 ETPREC75   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
