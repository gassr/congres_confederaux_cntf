
.. _motion_51_2012:
.. _motion_51_2012_etpics94:

===========================================================================
Motion n°51 2012 : Trésoreries confédérale et régionales (ETPICS 94)
===========================================================================
  

.. seealso::

   - :ref:`etpics94`
   - :ref:`motions_etpics94_2012`




Argumentaire
============

Le fonctionnement de la trésorerie à la CNT devrait être révisé.

Le trésorier de chaque syndicat doit payer ses cotisations respectivement
à la région, à la confédération et à sa fédération d'appartenance si elle
existe.

Tous ces versements se font de manière autonome, alors que la région et la
confédération ont un lien indéniable (entre chaque congrès ce sont les CCN
avec les représentants des régions qui se réunissent).

Le versement des  cotisations des syndicats devraient se faire à la
confédération au travers de la région, c'est à dire que chaque trésorier
de syndicat enverrait  ENSEMBLE le montant correspondant à la région et à la
confédération (qui sera  le même nombre).

Le trésorier de la région effectuera un transfert de la somme correspondante
à la trésorerie confédérale.

Deux avantages à cette facon de faire
-------------------------------------

1°) Alléger le travail de la trésorerie confédérale (gérer 130 syndicats avec
    tous les aléas inhérents à la tâche est très lourd), ainsi il y aurait
    répartition des tâches. Il est plus simple de gérer 12 régions, donc 12
    transactions que 130. Et pour le trésorier régional, de toutes facons il
    gère déjà les syndicats de sa région donc faire un transfert ou un
    chèque supplémentaire avec le relevé des syndicats ne serait pas une
    lourde tâche.
2°) Eviter de la sorte que des syndicats oublient de cotiser à la conf, ou à
    la région, être sûr des chiffres et du nombre de cotisants et éviter
    également d'avoir deux chemins parallèles de cotisations.

Là où n'existe pas de région, les adhérents cotiseront directement à la
trésorerie de la confédération comme à l'accoutumée.

Possibilité d'efectuer un projet de trésorerie mode d'emploi qui définirait
précisément les démarches à effectuer aussi bien pour les trésoriers de
syndicats que pour les trésoriers des régions


Motion
======

Les cotisations régionales et confédérales des syndicats sont payées au
trésorier régional.

Celui-ci garde la part régionale pour les dépenses régionales et effectue un
transfert avec relevé de cotisations pour la trésorerie confédérale pour
la part confédérale (en indiquant les syndicats qui paient).

**Dans le cas où la région n'existe pas, les syndicats cotisent directement à
la trésorerie confédérale**.

Le paiement à la fédération est effectué directement par le syndicat.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°51          |            |          |            |                           |
| ETPICS94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
