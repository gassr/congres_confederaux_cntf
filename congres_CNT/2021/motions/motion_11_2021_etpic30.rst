.. index::
   pair: Motion 11 ; 2021

.. _motion_11_2021_etpic30:

================================================================================================================================================
Motion **n°11** 2021: **De la reconquête syndicale - Motion d'orientations sur la syndicalisation** par **ETPIC 30**
================================================================================================================================================

Autres motions ETPIC30

    - :ref:`syndicats:motions_etpic30`


Motion d'orientations sur la syndicalisation
===============================================

Force est de constater que le taux de syndicalisation s'amoindrit d'année en année.

Ce phénomène est doublé d'un constat de vieillissement de la démographie syndicale.
La CNT n’échappe pas à cette tendance.

Plusieurs processus sont à l'œuvre. Sans être exhaustif·ve·s, on peut citer:

- Une imprégnation accrue de **l’individualisme et du consumérisme libéral** qui
  recouvre plusieurs réalités de vécu individuel et collectif ;
- De nouvelles générations peut-être moins soucieuses de s’organiser au sein
  d’organisations structurées, confédérales.
  La logique de réseau tend à s’imposer. Lorsque l’engagement est là, la
  jeunesse ne fait manifestement et massivement pas le choix du syndicalisme ;
- Une **précarisation générale du travail**, de l'emploi venant à la fois insécuriser
  les parcours de vie et professionnel ;
- Un **morcellement de la densité des collectifs de travail** dans le contexte
  évidemment de nos sociétés occidentales désindustrialisées ;
- Les **mutations du monde syndical** entre clientélisme d’appareil, bureaucratisme,
  autoritarisme, carriérisme, permanentisme , cogestion, compromissions avec
  l’administration et le patronat, accompagnement des réformes gouvernementales,
  perte de culture de classe, et juridisme ;
- Une **pression accrue exercée sur les travailleuses et les travailleurs**, une
  réduction du droit du travail et du droit syndical, une accélération de la
  mutation des droits sociaux inhérentes aux lois successives :

  * Loi sur la représentativité portant rénovation de la démocratie sociale
    et réforme du temps de travail (2008),
  * Loi relative au dialogue social et à l'emploi (2015 – Rebsamen),
  * Loi Macron (2015), Loi travail (2016 – El Khomri),
  * Loi Macron 2 (2018) ;

- Une généralisation de la **méfiance sur la récupération bureaucratique** des
  organisations ;
- Les stratégies syndicales des grandes centrales syndicales majoritaires
  qui ont porté à l'échec les grands mouvements nationaux de ces trente
  dernières années.
  Entre négociation, compromission, cogestion étalement des journées de
  grève et de manifestation sans réel appel à la reconduction.

Bien sûr, sans vouloir *s’autoflageller* outre mesure, il faut ajouter à cela
une difficulté de la CNT à se développer numériquement, à trouver une voie
propice à fédérer plus.

Depuis une trentaine d’années, notre Confédération a connu des évolutions notables.

Entre crises de fond sur des questionnements identitaires et stratégiques de
l’Anarcho-syndicalisme et du Syndicalisme révolutionnaire (crises /scissions
de 1993 et 2014, exclusion de l’AIT et reconstruction des liens internationaux
Noirs et Rouges), refonte de ses statuts (2008), et adaptations contraintes
aux évolutions législatives du droit syndical (Loi de 2008 notamment).

La CNT a ainsi connu beaucoup d’adaptations et de changements favorables, mais
aussi de lourdes secousses plus dommageables, au cœur d’un monde qui bouge
vite, très vite.

Les buts de la CNT, résolument révolutionnaires, supposent l’intégration de tous
les travailleurs et toutes les travailleuses pour asseoir la force du nombre,
le faire ensemble, et plus tard, le faire société.
Sans pour autant renoncer à **l’éthique**, plusieurs lignes d'attaques sont aujourd'hui
nécessaires à la CNT pour assurer ce développement numérique.

Il est temps de se donner les moyens de réflexion pour grossir, assurer une
meilleure syndicalisation, notamment des plus jeunes.
Nous sommes convaincu·e·s que les atouts actuels du modèle syndical proposé
par la CNT peuvent non seulement intégrer de **multiples sensibilités militantes**,
mais aussi constituer une véritable alternative vers un **autre futur, un projet
global, collectiviste, et libertaire**.

Sans avoir la prétention d’être exhausti·f·ve·s, persuadé·e·s que nos camarades
auront beaucoup d’autres idées à suggérer, et sans injonction aucune, nous
proposons déjà la mise en œuvre des orientations suivantes visant à favoriser
la syndicalisation :

Le syndicalisme pour lutter contre l'isolement, contre l’exploitation, contre l'individuation et l’individualisation libérale.
--------------------------------------------------------------------------------------------------------------------------------

Par nature, le syndicalisme est l'expression du **faire ensemble**, de la
collectivisation des questions, de la solidarité, de la sociabilité, de la
lutte contre l'isolement et la précarité.

Différents mouvements récents (Gilets Jaunes, Nuit Debout, ZAD...) nous montrent
que les populations précarisées, révoltées, en lutte, sont en recherche de
liens forts.
Le déclin du collectif, l’érosion des solidarités, la violence de l’économie
libérale entraînent une atomisation des parcours de vie et isole les personnes.

Non content d’être un outil de lutte, **le syndicat doit aussi pouvoir développer
le sens du collectif, de la solidarité, de l’entraide, et de la sociabilité**.

Le syndicat peut dès lors être à l’origine de caisses de soutien, de coopératives
de producteurs et de consommateurs visant à réduire les coûts et favoriser,
dans l’esprit révolutionnaire et écologiste qui nous anime, les **circuits courts**.

Nous préconisons donc que ses activités soient autant que possible **inclusives**
pour permettre et faciliter l’implication de ses adhérent·e·s.

**La transmission de savoir et la formation participent de ce partage de connaissances**
en ce qu’ils contribuent à l’appropriation des échanges et à la participation
de toutes et tous à notre cadre démocratique de décisions.

Cette qualité du syndicalisme doit ici être rappelée sans cesse :
**Ne reste pas seul·e face à ton exploiteu·r·euse**


Promouvoir le développement de vecteurs culturels proches des luttes ou soutenant la conscience et la culture de classe
--------------------------------------------------------------------------------------------------------------------------

La création culturelle joue depuis toujours un rôle majeur dans l’œuvre de
sensibilisation, d’émancipation, et de partage du milieu libertaire notamment.

De la littérature au graphisme, la musique, la production vidéo et cinématographique,
etc. le vecteur culturel favorise la transmission du message révolutionnaire,
la prise de conscience, la politisation des inquiétudes.

Tantôt initiatrice de révolte, tantôt en soutien aux luttes, la culture est un
axe essentiel de notre force de lutte.

La CNT soutient et initie des productions culturelles.

Le parrainage et le marrainage syndical comme d’accompagnement des nouve·aux·elles syndiqué·e·s
---------------------------------------------------------------------------------------------------

Chaque personne approchant la CNT ou souhaitant se syndiquer doit pouvoir
bénéficier d'un accueil adapté à ses besoins, être soutenue dans son intégration
au syndicat techniquement comme humainement, être accompagnée dans la
compréhension de la prise en main des outils syndicaux ou dans ses facultés
de participation et de contribution au cadre démocratique interne.

**Le parrainage, ou marrainage, peut-être ici une solution à un accompagnement
personnalisé**, plus facilité, pour les personnes face à la dimension collective
parfois difficile à appréhender selon le caractère de chacun.
Il permet aussi à la personne de décrypter de façon plus personnalisée, plus
discrète, moins stigmatisant, le syndicat et son fonctionnement.

**Instruire pour révolter** : une démarche d’éducation populaire, de formation, d’émancipation et de prise de confiance en soi
--------------------------------------------------------------------------------------------------------------------------------

Partout où c'est possible, si elle ne le fait pas déjà , la CNT développe des
espaces d’accueil, des collectifs, des projets collectifs, de politisation des
inquiétudes, de sociabilité pour les populations en recherche d'alternatives
ou d'organisation des luttes.

Ses militants développent sur de multiples supports, des outils de formation,
de conscientisation, et d'instruction : formation théorique, pratique, mise
en situation, théâtre, supports culturels, etc.
**Forger la conscience de classe dans la lutte et la compréhension du monde
capitaliste est le socle de la révolte et de l’engagement**.

Renforcer la confiance en soi, **la connaissance, l’autonomie, et le sens
de l’action collective et solidaire** conforte le/la militant.e dans ses choix,
ses capacités à agir.

Fédéralisme, démocratie directe, indépendance, autogestion, projet de transformation sociale globale : mettre en avant la singularité de la CNT
--------------------------------------------------------------------------------------------------------------------------------------------------

Il conviendra de mettre en avant notre **indépendance et notre cadre démocratique
et fédéraliste d'organisation**, notre modèles de syndicalisme entre indépendance,
incorruptibilité, insaisissabilité par le pouvoir, combativité, adaptabilité
aux formes de luttes diversement rencontrées.

L'alliance du syndicalisme révolutionnaire et de l'anarcho-syndicalisme,
tous deux forces de propositions complémentaires et synergiques, sont les
garanties de pouvoir intégrer des sensibilités multiples sans exclure la
multiplicité des formes d'actions.

.. _enjeux_antisexistes:

La prise en compte des **enjeux écologique, antisexistes**...
----------------------------------------------------------------

Ces principes fondateur de la CNT doivent apparaître aux yeux des nouve·au·lle·s
adhérent·e·s pas seulement comme des principes, mais doivent être concrétisés
par des actions et des choix internes comme externes.

voir motions portant sur le sujet:

- :ref:`motion_1_2021_ste33`
- :ref:`motion_23_2021_stt59_62`
- :ref:`motion_24_2021_ste33_cnt42`

.. _moyens_com_motion_11_2021:

Le développement d'outils de communication et de propagande adaptés et assimilables par les nouvelles générations
--------------------------------------------------------------------------------------------------------------------

En ce qui concerne la voix confédérale, le secteur propagande joue un rôle majeur.
Ses productions jouent un rôle central dans la propagation des idées, dans la
construction de la conscience de classe, et dans la désignation des enjeux de
la lutte.
Les communications électroniques et les réseaux sociaux se sont généralisées.
Ces nouvelles technologies amènent avec elles de nouveaux enjeux auxquels la CNT
n’est évidemment pas indifférente : la multiplication et l’immédiateté de l’accès
aux sources d’information enrichit autant qu’elle trouble la communication
et le message.
Elle favorise la participation et les liens tout comme elle renforce l’individualisme
et le consumérisme.
Les réseaux sociaux majoritaires sont **orchestrés par des firmes propriétaires**
à la solde des Etats.
La sécurité des échanges et des données deviennent un enjeu majeur de protection
des libertés individuelles et collectives.

Sans vouloir donc glorifier ici ce nouveau modèle de communication, les syndicats
de la CNT doivent toutefois en pouvoir prendre la mesure pour être présents sur
les réseaux sociaux avec la vigilance qui s’impose.

Les jeunes générations sont de moins en moins perméables au tract papier traditionnel
et utilisent de plus en plus le texte électronique court, la vidéo, la photo.
Les réseaux sociaux, les sites web et autres blogs, les plateformes collaboratives,
sont donc autant de supports à investir.

Si la Confédération n’a pas de stratégie d’ensemble sur la communication et la
propagande électronique, les syndicats doivent pouvoir multiplier les expériences,
**soutenir les alternatives libres**, et les mutualiser entre eux.


.. _outils_motion_11_2021_epic30:

Outils
=======

Dans le respect de l’esprit fédéraliste qui nous anime, il conviendra sans doute
que la CNT se dote dans les prochaines années d’un modèle de communication
électronique réfléchi et approprié pour asseoir son développement, qu’elle se
définisse une ligne de conduite, des outils, pour soutenir les syndicats et
leurs Unions au-delà de ce qui existe déjà (mails, sites web, :ref:`intranet <motion_26_2021_educ21>`).

Amendements
===========

- :ref:`amendement_motion_11_2021_ste72`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°11 2021 ETPIC30    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
