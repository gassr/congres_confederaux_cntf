
.. _amendement_motion_55_2012_interpro31:

==========================================
Amendement à la motion 55 (interpro31)
==========================================

.. seealso::

   - :ref:`motion_55_2012_etpic30`



Argumentaire
=============

Le Combat syndicaliste est l’organe de propagande de la CNT.

La mise en ligne permettra ainsi de pouvoir consulter les articles d’anciens
numéros et pour certaines personnes de découvrir notre travail.

Par contre, nous pensons que le fait de mettre le dernier numéro en ligne le
mois de sa parution risquerait de diminuer l’intérêt d’acheter le journal.

Il semble que ce phénomène est éprouvé par les quotidiens nationaux.

Ce qui pourrait à moyen terme remettre en cause l’édition du journal.

Aussi, nous proposons l’amendement suivant


Amendement
===========

Le 3e paragraphe de la motion serait modifié de la manière suivante:

«L’ensemble des articles déjà parus du Combat syndicaliste sera accessible
gratuitement et ceux concernant le dernier numéro ne seront visibles que le
mois suivant sa parution. La présentation de ces articles se fera selon une
mise en page spécifique.»


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°55 |            |          |            |                           |
| Interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
