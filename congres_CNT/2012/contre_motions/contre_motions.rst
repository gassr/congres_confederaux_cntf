
.. _contre_motions_congres_2012:

=======================================
Contre-motions du congrès CNT 2012
=======================================


.. toctree::
   :maxdepth: 2

   contre_motion_2_2012_ess34
   contre_motion_2_2012_stp72
   contre_motion_3_2012_ess34
   contre_motion_4_2012_ess34
   contre_motions_5_6_7_etpic30
   contre_motion_8_2012_ess34
   contre_motion_9_2012_ess34
   contre_motion_9_2012_ptt95
   contre_motion_11_2012_etpic30
   contre_motion_25_2012_ess34
   contre_motion_26_2012_ess34
   contre_motion_45_2012_stgl_rp
   contre_motion_46_2012_ess34
   contre_motion_60_2012_etpics57
