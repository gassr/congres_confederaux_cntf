
.. index::
   ! Motion 37  2010
   pair: Elections; Professionnelles

.. _motion_37_2010:

=========================================================================================================================================
Motion n°37 2010 : Question des élections professionnelles et de la représentativité syndicale dans le secteur public, Educ 69 [adoptée]
=========================================================================================================================================

.. seealso::

   - :ref:`educ69_motions`
   - :ref:`motion_strategie_2008`



Argumentaire
============

La CNT a adopté lors de son 30e congrès, la motion **Représentativité et stratégie
syndicale**, visant principalement à adapter notre stratégie syndicale dans le
secteur privé  suite aux évolutions induites par la loi du 20 août 2008.

Concernant le secteur public, cette motion précisait::

    "Aucun nouveau critère législatif n’étant adopté à l’heure de ce Congrès,
    la CNT reste sur sa position de refus de participation aux instances
    paritaires et aux élections professionnelles dans les fonctions publiques".


En cas de nouvelle loi sur ce sujet, la CNT décide d’analyser les nouveaux critères
de représentativité syndicale dans un prochain Congrès afin d’adapter sa position.

Dans l’attente de ce congrès, il est laissé la possibilité aux fédérations, sous
contrôle des CCN, d’étudier la question de la participation aux élections si
elles estiment que leurs droits syndicaux sont conditionnés par cette participation.

Or pour le secteur public, vient d'être adopté la loi n° 2010-751 du 5 juillet 2010
relative à la rénovation du dialogue social et comportant diverses dispositions
relatives à la fonction publique, traduction juridique des « accords de Bercy sur
le dialogue social dans la fonction publique du 2 juin 2008 » signés par six syndicats de
fonctionnaires (CGT, CFDT, FSU, UNSA, Solidaires, CGC).

Cela induit des changements profonds pour le syndicalisme dans la fonction publique
avec un rapprochement des règles du secteur privé tel que définis par la loi
du 20 août 2008.

Concernant les règles de présentation aux élections professionnelles, le verrou de
la loi Perben de 1996 saute et tous les syndicats « légalement constitués depuis
au moins 2 ans » et satisfaisant « aux critères de respect des valeurs républicaines
et d'indépendance » peuvent se présenter.

La validité des accords avec les organisations syndicales est conditionné à des seuils
électoraux (validité avec « une ou plusieurs organisations syndicales ayant recueilli
au moins 50 % du nombre des voix » ou « au moins 20 % du nombre des voix » sans rencontrer
« l'opposition d'une ou plusieurs organisations syndicales parties prenantes à la
négociation représentant au total une majorité des voix ».).

Les changements induits par la loi prendront pleinement vigueur fin 2013 au terme
d'une période transitoire.

Même si cela ne figure pas de manière explicite dans la loi (a contrario des accords
de Bercy ou du texte introductif lors du débat de la loi au parlement) celle-ci
vise donc a asseoir le critère électoral comme un critère central de la représentativité
syndicale.

Or déjà actuellement un certain nombre de droits syndicaux essentiels à notre pratique
syndicale quotidienne (préavis de grève, heure d'information syndicale -HIS-
sur le temps de service) sont liés à la notion de représentativité du syndicat.

Cette loi devrait nous couper définitivement accès aux droits syndicaux des syndicats
dit représentatifs d'autant plus si nous prenons en compte le coté jurisprudentiel
(par exemple, Le TA de Lyon après saisine du SUTE 69 nous a dénié le droit d'organiser une HIS
dans un collège, avec les critères prévalant avant juillet 2010, pourtant plus favorables).

Il semble donc aujourd'hui opportun que les syndicats et fédérations de la CNT
relevant du secteur public adaptent leurs stratégies syndicales en fonction de
cette nouvelle donne tout en maintenant une attitude ferme contre la cogestion
et le paritarisme. En effet, la question fondamentale porte non pas sur la
représentativité elle-même mais sur l'acquis de droits syndicaux, armes
essentielles de nos luttes et de notre indépendance. Notre critique du syndicalisme
cogestionnaire et du paritarisme reste entière. Il faudra trouver une position
pragmatique qui permette de concilier réalité idéologique et réalité pratique.

Motion
======

Les syndicats relevant du secteur public, strictement dans un cadre fédéral
lorsqu'ils dépendent de fédérations constituées, étudient les évolutions
induites par la loi du 05 juillet 2010 et ses suites à venir (décrets d'application,
réactualisation de décrets sur les droits syndicaux ou du code du Travail...).
Ils adaptent à cette nouvelle donne, si cela est nécessaire au maintien ou
l'acquisition de droits syndicaux essentiels (dépôt de préavis de grève, réunion
syndicale sur le temps de service) notre stratégie syndicale, en particulier
concernant la question des élections professionnelles.

Cependant cette évolution pratique et pragmatique ne doit pas se faire au
détriment de notre stratégie traditionnelle de refus du système paritaire
et de la cogestion. Charge aux syndicats et fédérations concernées de prendre
les dispositions suffisantes au respect de cette orientation fondamentale.

La confédération et ses syndicats sont tenus régulièrement informés des
orientations  et décisions prises par les fédérations ou syndicats
non fédérés, relevant du secteur public.

.. _vote_motion_37_2010:

Vote Motion n°37 : Question des élections professionnelles et de la représentativité syndicale dans le secteur public, Educ 69
==============================================================================================================================

  
+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°37          | 42         | 22       | 4          | 4                         |
| Educ 69              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion n°37 Adoptée**


.. seealso::

   - :ref:`amendement_motion_37_2010_stp72`
   - :ref:`motions_37_38_39_2010`
