

.. _motion_45_2010:

================================================================
Motion n°45 2010 : Intégration des jeunes, Culture-Spectacle RP
================================================================



Motion similaire en 2012 ETPICS57
=================================

- :ref:`motion_14_2012`

Argumentaire
=============

La CNT a connu un élan dans les années 90 grâce, notamment, à ces sections universitaires très présentes dans
les mouvements estudiantins. Les étudiants se sont fortement impliqués dans les structures de la CNT. Ils ont été
moteurs. En vieillissant, nombre de ces adhérents ont renforcé des syndicats et même été à l’origine de créations
de sections d’entreprise.

Il faut constater que les militants étudiants ont, en ordre général, plus de temps et moins de contraintes
familiales. Ainsi, en semaine, par rapport à de nombreux adhérents en activité, ils peuvent se rendre aux actions
syndicales plus facilement et aider les camarades en lutte. Par la même occasion, ils se forment au syndicalisme
d’entreprise.

Aujourd’hui, la CNT a une assise bien plus forte qu’avant dans le syndicalisme d’entreprise, ce qui concrétise
des années de formation et d’expériences...C’est positif. Nos militants sont de plus en plus dans la réalité du
syndicalisme d’entreprise qui prend du temps. La CNT a, d’un autre coté, moins de présence « politique » sur
des terrains que nous ne voulons pas laisser aux seules organisations politiques comme l’antifascisme,
l’antisexisme, la présence sur les contre-sommet... La CNT a pourtant aussi vocation à être sur ces terrains avec
ses propres analyses. Cette baisse d’activité n’est pas une volonté (aucune motion n’a formulé ce souhait). Il
s’agit d’un manque de temps militant.

Le sens de la motion ne signifie pas qu’il faut intégrer les jeunes pour qu’ils comblent ces manques de présence
plus politique car il devrait en être du rôle de chaque syndicat. Le sens de la motion est de faire remarquer que
l’intégration de la jeunesse est une source de vitalité de l’organisation et de dynamisme.

Cette motion est donc là pour tirer la sonnette d’alarme aux syndicats pour qu’ils réfléchissent à l’intégration des
jeunes. Notamment aux différents syndicats de l’éducation mais aussi aux autres syndicats pour qu’ils facilitent
la formation interprofessionnelle des jeunes militants ! Cette intégration suppose une confiance aux jeunes
militants. Elle suppose aussi la conscience des spécificités des préoccupations de la jeunesse (plus fort sentiment
anti-répression, plus forte confrontation aux militants d’extrême droit...).

Motion
=======

Dans le but d’un développement de la CNT auprès des jeunes travailleurs (nouveaux travailleurs précaires et/ou
contractuels) et des travailleurs en formation (apprentis, lycéens filières techniques et générales, étudiants filières
techniques et générales, stagiaires lycéens et étudiants), la CNT lance une campagne confédérale. La CNT
soutient et développe l’intégration cénétiste et la formation syndicale des jeunes notamment au moyen de la
production de matériel spécifique de campagne et de propagande.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°45          |            |          |            |                           |
| Culture-Spectacle RP |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
