
.. _motions_tresorerie_confederale_cnt_congres_agen_2006:

==================================================================================================
Motions "Trésorerie confédérale" de la CNT  29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
==================================================================================================



.. seealso::

   - :ref:`motion_53_2012`


Prêts confédéraux
=================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 46         | 6        | 5          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Tout prêt demandé par un syndicat doit voir l'assentiment de son :term:`UR`.


laisser au BC / CA le soin d'aviser
===================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 43         | 8        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

En l’absence d’UR, le Congrès, convient, faute de règles de laisser au BC / CA
le soin d’aviser tel que pratiqué jusqu’alors qui est solidaire du remboursement
des sommes prêtées en cas de difficultés.


.. index::
   pair: Trésorerie confédérale; Caisse de solidarité confédérale



.. _cotisation_caisse_solidarite_confédérale_agen_2006:

Cotisation à la caisse de solidarité confédérale (Agen, 2006)
==============================================================

.. seealso:: :ref:`caisse_solidarite_grevistes_lille_2008`

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 44         | 14       | 5          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Les sommes collectées par la vente du timbre confédéral de cotisation du mois
de janvier sont affectées à la :ref:`caisse de solidarité <caisse_solidarite_grevistes_lille_2008>`.

Un timbre spécifique est édité afin de matérialiser cette affectation.
