
.. _contre_motion_11_2014_sinr44:

=============================================
Contre-motion à la motion n°11 2014 (SINR44)
=============================================

.. seealso::

   - :ref:`motion_11_2014_tasra`




Argumentaire 
=============

Nos fédérations ne sont pas au mieux de leur forme.

La fédération des transports , créée récemment, peine à se développer,
la preuve en est l'élargissement du champ de syndicalisation du SGTL RP
à l'échelle nationale.

Il en va de même pour certaines de nos fédération historiques
(Culture-spectacle, Construction), qui ont du mal aujourd'hui
à se réunir, y compris la fédération Santé-social, qui pourtant
compte un certain nombre de syndicats.

Créer une nouvelle fédération pourrait à notre sens avoir plusieurs
conséquences :

- affaiblissement supplémentaire de la fédération Santé-social
- création d'une nouvelle fédération basée sur un seul syndicat de
  branche et quelques syndiqué.es, devenant une nouvelle coquille vide
- amenuisement du lien interprofessionnel si cher à notre confédération

Cependant, il nous paraît pertinent que les travailleurs.euses du
ministère du travail notamment puissent s'organiser à l'échelle
hexagonale pour communiquer,se faire connaître, signer des textes
intersyndicaux etc, mais nous considérons que cela peut se faire
au sein de la fédération Santé-social.

Par ailleurs, nous voyons l'opportunité formidable que constitue la
présence de travailleurs.euses du ministère du travail au sein de
notre organisation, tant en terme de formation que de pertinence
syndicale auprès des travailleurs.euses du secteur privé.

Contre-motion 
==============

La fédération Santé-social se dote d'une coordination du travail, de
l'emploi et de la formation professionnelle.

Cette coordination décide de son propre fonctionnement et peut
communiquer en son nom propre, mais continue de participer de
manière habituelle à la vie fédérale.

Par ailleurs la coordination s'engage à mandater au moins une personne
dans la commission juridique confédérale.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°9 2014 <motion_11_2014_tasra>`

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
