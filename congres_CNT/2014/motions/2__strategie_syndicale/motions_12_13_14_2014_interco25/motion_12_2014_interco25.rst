

.. index::
   pair: Motion 12 ; 2014

.. _motion_12_2014_interco25:

=============================================================================
Motion n°12 2014 **Retrait de la coordination Rouge et Noire** (Interco 25)
=============================================================================

Motions interco25

   - :ref:`syndicats:motions_interco25`


Motion
========

La CNT ne ratifie pas les statuts de la Coordination rouge et noire,
elle continuera d'assister comme observatrice à ses rencontres et
restera en lien avec les organisations qui en sont membres.

Elle cherchera de même à entretenir des relations régulières avec les
organisations internationales anarcho-syndicalistes et syndicalistes
révolutionnaires (AIT, IWW) et leurs sections, ainsi qu'avec les groupes
non affiliés s'inscrivant dans ce mouvement.

Une réflexion devra aussi être menée au cas par cas concernant les
réseaux et campagnes auxquels la CNT participe par ailleurs.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°12 2014

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
