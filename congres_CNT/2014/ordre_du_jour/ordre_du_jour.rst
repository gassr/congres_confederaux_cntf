


.. _odj_xxxiii_congres:

=================================================
Ordre du jour XXXiiie Congrès CNT 2014 d'Angers
=================================================

.. seealso::

   - :ref:`congres_statuts_CNT`
   - :ref:`fonctionnement_congres_confederal`





Vendredi 12 décembre
====================

Accueil et enregistrement des mandats aupres de la commission de controle des
mandats à partir de 10H

Ouverture du congres à 13H30

- Mise en place de la présidence du congres et adoption de l'ordre du jour
- Mise en place de la commission de controle de la Trésorerie confédérale
- Débat et vote des motions de "Fonctionnement du congres",
  numérotées de 1 à 7
- Début des Rapports des mandatéEs confédéraux

Samedi 13 décembre
===================

Travaux de 9H à midi puis de 14 à 18H30

- Séance en soirée de 20H30 à 23H si nécessaire

- Poursuite des rapports des mandatéEs confédéraux
- Rapport de mandat des commissions confédérales
- Débats et votes des motions de "Stratégies et orientations",
  numérotées de 8 à 17

Dimanche 14 décembre
=====================

Travaux de 9H à 15h

- Débats et votes des motions de "Fonctionnement",
  numérotées de 18 à 30
- Interventions des internationaux (?)
- Désignation des nouveaux mandaté-e-s confédéraux
- Discussions ouvertes sur des thématiques à définir

Cloture du congres prévue à 15H
================================
