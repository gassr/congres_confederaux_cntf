
.. index::
   pair: Motion 19 ; 2014

.. _motion_19_2014_sinr44:
.. _refonte_intranet_2014:

====================================================================
Motion n°19 2014 : **Refonte de l'Intranet confédéral** (SINR 44)
====================================================================



Argumentaire
=============

Après plusieurs années de déliquescence, il convient de faire plusieurs
observations à propos de l'intranet confédéral de la CNT:

- **La structure actuelle est devenue obsolète** : système d'exploitation
  non maintenu, logiciels non mis à jour, situation préoccupante d'un
  point de vue sécurité.
- **Les logiciels et fonctionnalités proposés sont vieillissants**.
- À contrario, l'offre en logiciels libres et open source a fortement
  progressé ces dernières années, notamment dans le domaine du travail
  collaboratif et autres outils dont la CNT pourrait se servir pour
  faciliter et fluidifier son fonctionnement.
- Sans se faire trop d'illusions sur l'enjeu, on peut tout de même
  estimer que si la CNT ne se dote pas d'outils de communication
  internes comme externes efficients (saluons à ce sujet la refonte
  du site web confédéral) cela peut avoir des conséquences sur son
  développement et sa réactivité.

  **Nous constatons qu'il est temps d'envisager une refonte complète de
  l'intranet**.


Motion
=======

La commission intranet est réactivée.

Il sera, à la suite de ce 33ème congrès confédéral, procédé à un
inventaire complet des compétences en administration système et
en développement d'applications informatiques afin de bénéficier d'un
large éventail d'avis et de pouvoir répartir au maximum le travail de
conception, de développement et de déploiement du futur intranet et
de ses applications.

**L'avancement des travaux de la commission sera soumis à l'avis des
CCN successifs jusqu'à la validation du projet définitif**.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°19 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
