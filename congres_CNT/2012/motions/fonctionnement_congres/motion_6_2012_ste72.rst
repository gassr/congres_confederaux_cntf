
.. _motion_6_2012:
.. _motion_6_2012_ste72:

==============================================================================
Motion n°6 2012: Organisation des congrès de la CNT et prise de parole STE 72
==============================================================================
  
.. seealso::

   - :ref:`ste72_pub`
   - :ref:`motions_ste72_2012`





Préambule
=========

Cette motion concerne l'organisation des congrès futurs de la CNT, mais est
également proposée comme motion d'ordre pour l'organisation du Congrès
confédéral de 2012.

Chaque point peut être débattu et adopté séparément.


Motion
=======

Pendant les congrès, les dialogues qui monopolisent les débats et les joutes
verbales sans intérêts nuisent à la recherche de compromis et donc au
fédéralisme.

Il faut donc que le président de séance puisse jouer son rôle de manière
efficace.

Nous proposons que les prises de paroles ne puissent se faire que en un point
fixe avec micro, que le président distribue les passages au point de prise de
parole par l'établissement d'une liste de prise de parole, avec priorité aux
syndicats ayant fait le moins de prises de parole


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°6           |            |          |            |                           |
| STE72                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Contre-motions
==============

- :ref:`contre_motions_5_6_7_etpic30`
