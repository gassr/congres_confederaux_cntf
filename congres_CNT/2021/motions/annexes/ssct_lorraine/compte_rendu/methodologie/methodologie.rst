

.. _methodologie_ssct:

==========================================================================================================================
I) Méthodologie et formes de la procédure de notre syndicat
==========================================================================================================================



1. Qualification des faits
==============================

Notre syndicat a été saisi d'une accusation de viol.

Sur ce point, et tel que nous le précisions déjà dans notre préambule,
c'est bien avec ce terme que nous qualifions les faits reprochés à notre
adhérent par la victime supposée au regard de la description qu'elle en
a fait auprès de la commission non mixte d'Alternative Libertaire.

Nous précisons également qu'en cas de procédure judiciaire, au regard
des gestes décrits par la victime, c'est bien également cette qualification
de viol qui serait retenue.

Nous espérons donc ici mettre fin immédiatement à toutes les rumeurs
auxquelles nous avons été confrontés ces derniers jours et qui fustigent
la CNT en lui reprochant de ne pas considérer les faits présumés comme
un acte de viol.

2. Question qui se pose à notre syndicat
=============================================

Il est important pour notre syndicat de préciser, avant toute chose, que
notre procédure et notre réflexion n'ont en aucune façon la prétention
d'être celle d'une police judiciaire ni d'un tribunal.

**Il est évident que notre syndicat, membre de la CNT, défend un projet de
société qui inclura à l'avenir une forme de police et une forme de
justice libertaire**.

Mais à l'heure actuelle, nous avons considéré unanimement qu'en tant
qu'organisation syndicale, nous n'avions pas les moyens de pouvoir établir
quelque vérité sur les faits. Aussi, bien que nous ayons cherché à recueillir
un maximum d'éléments permettant d'éclaircir le fond de l'affaire et
motiver une décision de notre part, nous ne procéderons à aucun vote sur
la culpabilité ou l'innocence de notre adhérent.

La question à laquelle nous avons cherché à répondre tout au long de
cette affaire est bien la suivante : les éléments à charge contre
notre adhérent sont-ils suffisamment crédibles et probants pour conduire
à son exclusion de la CNT ?


Nous précisons donc que :

- Nous ne discuterons en aucun cas le degré de gravité des faits évoqués.
- Nous ne discuterons en aucun cas la nécessité d'exclure un adhérent
  qui serait coupable de viol.

3. Principes ayant guidé notre démarche
===========================================

a) Justice équitable ou justice bourgeoise ?
------------------------------------------------

Il convient de définir ce qu'est une **justice équitable**, notion qui
s'oppose de façon radicale à la notion de justice bourgeoise.

Une justice équitable est une forme de justice dans laquelle la procédure
vise à permettre aux deux parties, qui ne sont pas à égalité au sein de
la société, de jouir des mêmes possibilités d'expression et de justification
de leurs propos.

Attention, il ne faut donc pas confondre la notion de justice "équitable"
avec celle d'une justice "égalitaire".
Cette dernière ne pouvant être mise en œuvre lorsque les deux parties
en présence ne sont pas à égalité à la base, ce qui n'est pas le cas dans la
société actuelle.
La notion de **justice équitable** implique donc que les deux parties soient
considérées au regard de leur position sociale objective, et non d'un
rôle subjectif, souvent sexué, qu'on leur aurait attribué au préalable.

Nous avons considéré que le seul moyen d'appliquer une justice équitable
était d'objectiver au maximum du possible les faits reprochés.

Toute forme d'émotion, d'identification à la victime potentielle ou à
l'accusé, toute forme d'attribution d'un rôle social prédéfini à un sexe
ou à l'autre, sont contradictoires avec la notion de justice équitable
puisqu'elles feraient inévitablement pencher la balance en faveur de
l'une ou l'autre des parties de façon subjective et/ou politique,
indépendamment des faits.

Au fil de l'histoire, ce sont les luttes sociales et révolutionnaires qui
ont instauré ce droit à l'équité, matérialisé aujourd'hui encore par des
gardes fous qui permettent, en théorie, aux deux parties en présence
d'exprimer leurs arguments de façon équitable.

À contrario d'une justice équitable, une justice qualifiée de "bourgeoise"
est une forme de justice qui considère que les éléments reprochés ne
doivent pas être considérés de manière objective, mais au regard de
convictions politiques : la justice bourgeoise est une justice partielle
qui défend les intérêts politiques particuliers, en l'occurrence ceux
de la bourgeoisie, de la classe dominante.

C'est ce que fait régulièrement l'institution judiciaire de la V ème République
lorsqu'elle transgresse les gardes fous de l'équité afin de faire condamner
ses ennemi·es politiques.
Cette forme de justice est d'ailleurs dénoncée régulièrement par la CNT
en tant qu'organisation syndicale lorsque, pour faire condamner les
militant·es du mouvement social, la justice institutionnelle s'assoit sur
les mécanismes qui permettent d'objectiver les faits afin de privilégier
une vision partisane des évènements.

Afin de garantir que la justice soit équitable et non partisane en faveur
d'une classe dominante ou d'un sexe en particulier, l'histoire révolutionnaire
a tenté d'instaurer différents garde fous (la **présomption d'innocence**,
le droit à un·e défenseur·euse, la notion de procédure contradictoire,
etc...).

Ce sont précisément ces gardes fous qui permettent, en théorie, que la
justice ne soit pas instrumentalisée, ni par l'une des parties, ni par
une classe dominante.
Ce sont bien ces principes qui, historiquement, ont permis de rompre
avec la justice de l'Ancien Régime dans lequel la notion même de procès
n'avait pas de sens : les jugements se faisant de manière arbitraire et
étant rendus alors par celui qui détenait le plus de pouvoir.

Ces principes de **présomption d'innocence, de respect du contradictoire**,
du droit à la défense sont précisément les principes qui permettent de
faire la distinction entre une justice équitable et impartiale d'un côté,
et une justice partisane, orientée, souvent bourgeoise de l'autre.

Ces principes, si nous avons bien conscience qu'ils ne permettent pas
de solutionner à hauteur de nos espérances le problème d'iniquité entre
les hommes et les femmes dans la société actuelle - et notamment dans le
cadre des agressions sexuelles où la parole de la femme n'est pas à égalité
avec celle de l'homme - sont pour autant des **principes incontournables
de la justice TOUT COURT**.

**C'est précisément parce que ces principes sont régulièrement bafoués par
l'institution judiciaire que l'on qualifie cette dernière de bourgeoise !**

**Ainsi, notre syndicat ne remettra pas en cause ses principes lors de sa
procédure.**

Si nous sommes d'accord pour dire que la **présomption d'innocence**, comme
ces autres gardes fous, ne suffisent pas à garantir une position équitable
pour une victime féminine vis à vis d'un agresseur masculin, qu'il
faudra dans le cadre de la défense d'une justice antisexiste future songer
à instaurer d'autre gardes fous, remettre en cause ces principes gagnés
historiquement par la classe populaire, seraient la garantie d'appliquer
une justice injuste, partiale, extrêmement proche de la justice de l'Ancien
Régime.

À celles et ceux qui souhaiteraient faire disparaître la notion de
**présomption d'innocence** dans le cadre des affaires d'agressions
sexuelles, **nous rappelons qu'ils défendent là une justice parfaitement
partiale et qui se rapproche au plus près de ce que nous appelons sur
un plan militant "justice bourgeoise", laquelle est d'ailleurs appliquée
aux États-Unis qui ne nous semble pas être un modèle de justice populaire,
révolutionnaire, ou antisexiste.**

C'est donc bien pour garantir une justice équitable et ne pas tomber dans
les travers de la justice bourgeoise que notre syndicat appliquera lors
de sa procédure les principes et gardes fous décrits ci-dessous.


.. _refus_transmission_infos_2017_03_05:

b) La prise en compte de la parole de la victime
---------------------------------------------------

Prendre en compte la parole de la victime ne **signifie pas prendre la parole
d'une partie pour argent comptant**.

Dans le cadre d'une agression sexuelle, nous le savons toutes et tous,
il est extrêmement important que la parole de la femme puisse être libre.
Cela implique que le propos de la personne soit alors recueilli dans un
cadre sécurisant, serein et qui protège celle qui s'exprime de toute pression,
qu’elles soient liées au sexe ou à des convictions politiques partisanes.

Dans l'affaire qui nous préoccupe aujourd'hui, vous le savez, le propos
de la victime n'a pas été recueilli directement par notre syndicat qui,
par respect pour se principe de parole libérée de la femme, n'a pas
cherché à entrer directement en contact avec elle.

Nous avons donc dans un premier temps demandé à la commission antisexiste
de la CNT de nous transmettre cette parole afin que nous puissions la
prendre en compte, dans le respect de la formulation qui a été celle de
la victime supposée.
**Pour rappel, cette demande a essuyé une fin de non-recevoir**.

C'est finalement par le biais du rapport de la commission non mixte d'AL
que nous avons pu accéder à ce propos, que nous avons considéré en tant
que tel, sans le dénigrer, ni le modifier, ni l'interpréter.

Mais attention, prendre en compte de façon libérée la parole de la victime
supposée suivant un évident principe antisexiste ne signifie à aucun
moment prendre ce propos pour argent comptant ou pour vérité.
Loin de nous l'idée que dès lors qu'une femme formule une accusation de
viol alors le viol est avéré et cela pour deux raisons principales:

- Parce que considérer la parole de l'accusation comme une preuve de
  culpabilité nous imposerait de défendre une **justice partiale et arbitraire**,
  loin de toute objectivation des faits et qui a permis, notamment sous
  l'ancien régime, mais aussi dans l'institution judiciaire actuelle, à la
  justice bourgeoise d'opérer !
- Parce que considérer que toute femme dit la vérité est une posture
  issue du constat, réel en ce qui le concerne, que dans l'immense
  majorité des cas, la parole des victimes de viols est bafouée, moquée,
  non considérée. Mais cette réalité, ne peut conduire si l'on raisonne avec
  rationalité, à conclure que toute personne qui dénonce un crime en est
  de fait victime.

À contrario, nous considérons, en tant qu’organisation révolutionnaire
et anti-sexiste, que tout individu mérite d'être libre de sa parole,
quel que soit son genre, et qu'en conséquence un individu ne peut en
aucun cas être dépossédé de ses déclarations par une posture idéologique
établie par un tiers.

Pour notre syndicat, prendre en compte la parole de la victime a donc
été de considérer l'accusation et d'aller chercher tout au long de notre
procédure à ce que cette parole ne nous parvienne pas sous la contrainte
ou la pression. Mais à aucun moment il ne s’est agi de considérer que
ce propos était de fait une réalité objective, car cela aurait constitué
une entrave gravissime à la conception antisexiste qu’il y a Les femmes
que nous nous refuserons en toute circonstance à réduire à leur vagin.

A ce stade, nous tenons également à couper court aux raisonnements odieux
qui instrumentalisent les statistiques en terme d'agressions sexuelles
et de viol à l'aide d'un raisonnement digne de courants politiques qui
n'ont rien à voir avec le nôtre.

C'est une réalité, le fait est que dans l'immense majorité des cas
(plus de 90%) les plaintes pour agression sexuelle ou viol qui parvient
devant les tribunaux finissent par une reconnaissance de la culpabilité
de l'homme.

L'on sait aussi pertinemment, que dans les 10% restants, nombreux sont les
hommes qui ont sans doute bénéficié d'une justice partiale et d’une
non-considération de la parole de la femme et ont donc échappé à la
condamnation en raison de pirouettes judiciaires.

Aussi, les cas dans lesquels les hommes étaient réellement innocents et
dans lesquels l'accusation s'est avérée mensongère sont extrêmement peu
fréquents, bien qu'ils existent.
(Rappelons que nous parlons ici des affaires portées devant les tribunaux,
il existe aussi au stade de la plainte devant la police, une minorité
de cas dans lesquels l'accusation s'était avérée mensongère).

Ces statistiques démontrent de manière flagrante que dans l'immense
majorité des cas la parole de la plaignante est crédible et que
l'homme est coupable.

Cette réalité statistique doit-elle nous conduire à considérer que toute
femme qui accuse dit la vérité ? Cette réalité statistique doit-elle
faire force de preuve, permettant de tirer des conclusions quant à une
affaire précise ? Notre syndicat répond haut et fort que NON.

Transposer légèrement ce raisonnement basé sur la statistique à un autre
type d'accusation permet de comprendre en quoi il ne peut être retenu :
si nous prenons la question du terrorisme dans la société actuelle, il
est évident que dans l'immense majorité des cas d’attentats contemporains,
ceux-ci sont commis par des individus de confession musulmane.

Cette réalité statistique implique-t-elle que lorsque qu'un attentat
se produit, alors il est de fait démontré que le coupable est de confession
musulmane ? Heureusement non.
**Un tel raisonnement, ne peut être celui d’une organisation révolutionnaire,
libertaire, et antisexiste.**

**Ce type de raisonnement par l'absurde, nous préférons le laisser aux fascistes.**

c) **La présomption d'innocence**
-------------------------------------

La procédure que nous avons conduite au sein de notre syndicat respecte
le principe de **présomption d'innocence**.

Non pas pour pouvoir déclarer à terme et dans le cas où nous n'aurions
aucune preuve matérielle l'innocence de l'accusé : nous avons déjà expliqué
précédemment que dans tous les cas, notre syndicat ne pourrait avérer ni
culpabilité ni innocence.
Mais bien parce que ne pas respecter ce principe nous conduirait
inévitablement à déclarer la culpabilité de notre adhérent sans pour
autant connaître la réalité des faits, et donc à lui refuser toute forme
de défense sur les faits.
À quoi bon recueillir la défense d'un accusé si celui-ci est avéré coupable
dès lors que la victime supposée a formulée une accusation ?
Encore une fois, la **présomption d'innocence** n'est pas un élément de
définition de la justice bourgeoise, comme certain·es le pensent, mais
bien **d'une justice juste et équitable** : rappelons simplement à ce
titre qu'aux États-Unis, la présomption de culpabilité est en vigueur,
et il ne nous semble pas que ce pays soit un modèle de justice au service
de la défense des femmes victimes de viols, encore moins que ce pays
présente une société moins patriarcale que la nôtre...

Aussi, dans cette affaire précise d'accusation de l'un de nos adhérents,
notre syndicat s'est attaché à utiliser systématiquement le conditionnel
quant à la culpabilité de notre adhérent, ce qui n'entre pas du tout en
contradiction avec le principe fondamental de prise en compte de l'accusation.

Nous précisons également, que la **présomption de culpabilité** a conduit
diverses structures ces dernières années, à se rendre juridiquement
coupable de diffamation et d'être sujettes à des poursuites, tout simplement
parce qu’elles ont évoqué les faits comme si ceux-ci étaient objectivement
avérés.
Attention, soyons clairs : être coupable de diffamation ne veut pas forcément
dire avoir accusé à tort, c'est avoir accusé sans preuves formelles
reconnues par l'institution judiciaire.

Aussi, il nous semble que respecter le principe de **présomption d'innocence**
permet également à notre syndicat de se prémunir de toute répression de
l'institution judiciaire.
Ce qui n'empêchera en rien, bien qu'il emploiera le conditionnel, à notre
syndicat de prendre des mesures répressives à l'encontre de son adhérent
si nécessaire.

d) Le respect d'une **procédure contradictoire** et du droit à la défense
------------------------------------------------------------------------------

Nous considérons que ces deux principes sont inaliénables, et ne peuvent
être remis en cause par aucune forme de justice, quel que soit la gravité
des faits reprochés.

Le principe d'une procédure contradictoire est directement lié au principe
de présomption d’innocence, afin d'empêcher qu'une accusation en tant
que telle fasse force de condamnation.

Le principe d'une procédure contradictoire est celui de la présentation
à l'accusé des éléments à charge contre lui, afin que celui-ci dispose
alors d'un droit de réponse, lui permettant de se défendre de ce
qu’il lui est reproche.

Cela ne veut pas dire que les éléments de défense apportés par l'accusé
feront foi de manière prépondérante à ceux de la victime supposée.
Lorsque les éléments apportés par l'accusation et ceux de la défense sont
divergents il convient alors de réunir des éléments autres permettant
d'accorder à l'un·e ou l'autre des versions plus ou moins de crédit.
Une procédure contradictoire implique donc que l'instruction d'une affaire
se fasse à la fois à charge mais également à décharge, de façon à pouvoir
soit infirmer, soit confirmer, les éléments présentés par chacune des parties.

e) Le **droit à la défense** pour l'accusé
---------------------------------------------

Le **droit à la défense** pour l'accusé est également un principe inaliénable
de toute justice qui se dit équitable.

Ce principe n'entre à nos yeux en rien avec nos principes politiques
antisexistes en cela que c'est ce principe même qui, encore une fois,
empêche la classe dominante de la société (qui peut être dominante au
titre de sa position politique, économique, mais aussi de genre) de faire
condamner de façon arbitraire au regard de la position sociale de l'accusé.

C'est encore une fois précisément ce que nous reprochons à la justice
bourgeoise qui, pour défendre ses intérêts, use de tous les moyens
possibles pour que l'accusé n'est pas le moyen d'éviter une répression
qui serait décidée pour des motifs politiques.

Le **droit à la défense** de l'accusé lui confère à nos yeux le droit
légitime de s'exprimer devant l'instance qui doit juger son cas, en
l'occurrence l’Assemblée Générale, sur l'ensemble des points qui
l'incriminent.
Pour se faire, il paraît évident que l'accusé, dans le cas d'une telle
procédure, puisse s'exprimer en dernier lieu. En effet, si des faits
lui sont reprochés après qu'il ait pu s'exprimer, il va de soi que celui-ci
n'aura pas eu la possibilité de réagir à leurs sujets et donc de
bénéficier d'un droit équitable à sa défense.

Finalement notre adhérent a eu la possibilité, à chaque fois que nous
l'avons auditionné, d'être assisté par la personne de son choix,
adhérente ou non à la CNT.

4. Recueil des éléments à charge
------------------------------------

Notre syndicat n'a eu que peu de possibilité pour recueillir les éléments
à charge contre notre adhérent.

En interne de la CNT, **toutes les sources d'informations potentielles
- c'est à dire à la fois les membres de la commission antisexiste, mais
également les adhérent·es des syndicats ayant recueilli directement le
propos de la victime supposée lors de son audition juste avant le
Congrès Confédéral le jeudi 10 novembre 2016 - n'ont souhaité nous
transmettre les éléments de l'accusation.**

Comme vous le savez, malgré nos nombreuses sollicitations écrites, puis
lors d'une réunion qui s'est déroulée en décembre entre notre syndicat
et une délégation de la commission antisexiste ne nous ont permis d'obtenir
des éléments à charge contre notre adhérent.

Nous nous sommes alors demandé s'il était judicieux de recevoir nous-même
la victime supposée dont nous avions été informé·es de l'identité dès
le 13 novembre 2016 au soir.
Ayant alors appris que la victime supposée était résidente de la ville
de Nancy, située à 50 km de Metz, ville résidence de l'accusé, et considérant
les liens multiples qui existent dans le réseau militant entre les deux
localités, notre syndicat a estimé que recevoir directement la victime
serait interprété par l'accusation comme une volonté de notre part de
lui mettre la pression.

Nous nous sommes donc totalement interdit cette démarche, tout en signifiant
à la commission antisexiste de la CNT, en lien avec cette personne, que
**si elle le souhaitait nous serions bien entendu ouvert·es à l'auditionner**.

En définitive notre syndicat n'a disposé que d'une seule source
d'information pour recueillir des éléments à charge contre notre adhérent,
en l'espèce du rapport de la commission non mixte chargée d'instruire le
dossier au sein d'Alternative Libertaire.

**Nous précisons d'ailleurs, situation totalement ubuesque, que c'est notre
adhérent accusé qui nous a transmis ce document de son propre chef**.

**Alternative Libertaire n'ayant proposé au Bureau Confédéral de la CNT
la transmission de ce document que le 20 février 2017**.

C'est dans ce document que nous avons pu recueillir:

- le propos partiel de la victime et son récit partiel de l'agression
  potentielle et de son contexte.
- des citations, extraites de témoignages anonymes par le travail de la
  commission non mixte d'AL et décrivant les agissements de notre adhérent,
  le contexte de l'agression potentielle d'autre part.


5. Recueil des éléments à décharge
--------------------------------------

Plusieurs sources d'information nous ont permis de recueillir des éléments
à décharge de l'accusé :

- Les différentes auditions de l'adhérent accusé.
- Les témoignages des 3 seul·es adhérent·es d'Alternative Libertaire dont
  nous avons pu obtenir l'identité. Deux de ces témoins sont des hommes
  (l’un est adhérent du CAL Moselle, le second est adhérent du CAL Moselle
  et de l'ETPICS 57 CNT), la troisième est une femme (adhérente du CAL Moselle).

Ces témoins ont été entendus par une délégation de notre assemblée générale ;
d'abord à tour de rôles, puis dans le cadre d'un entretien collectif permettant
de confronter leurs déclarations.

Par respect pour eux·elles, **et les préserver de quelques lynchages** que
ce soit dans la mesure où ils·elles ont apportés des éléments à décharge,
nous ne communiquerons par leurs identités dans ce document, mais les
transmettons au Bureau Confédéral afin que celui-ci puisse agir à l'avenir
au regard d'éventuelles décisions confédérales futures.

6. Procédure de vote au sein de notre syndicat
------------------------------------------------

Au regard de la qualification des faits présentés par l'accusation, en
l'espèce une accusation de viol, il est évident que la seule sanction
envisageable pour un adhérent coupable de tels actes est l'exclusion.

Le vote de notre assemblée générale sur ce point s'est donc déroulé, au
regard de l'instruction que nous avons pu mener, dans le respect des
dispositions statutaires de notre syndicat.

Bien entendu, ce vote, ainsi que le débat qui l'a précédé se sont déroulés
en dehors de la présence de l'adhérent incriminé qui ne pouvait en aucun
cas, cela va de soi, être à la fois juge et partie.

II) Éléments de contenu de la procédure, les faits
(Pour des questions de facilité rédactionnelle la victime supposée sera ici appelée E.)
Contexte factuel de l'accusation
Ce contexte et ces éléments que nous décrivons dans ce premier paragraphe sont concordants dans
l'ensemble des récits (celui de la victime supposée, de l'accusé, du rapport de la commission d'AL,
des témoins auditionné·es par notre syndicat). Nous considérons donc que ces premiers éléments ne
font pas débat et revêtent une dimension objective qui n'est ni à charge ni à décharge pour l'accusé.
L'accusation formulée par E. auprès de la commission non mixte d'Alternative Libertaire porte sur
des faits qui se seraient déroulés en campagne estivale d'AL, qui s'est tenue en août 2016 au
Roucous. E. tout comme Fouad se sont rendu·es ensemble à ce camping par le biais d'un
covoiturage organisé entre plusieurs membres d'Alternative Libertaire dans le Grand Est. Ce que
décrivent tous·tes les participant·es à ce covoiturage, il était composé de deux véhicules : une
première voiture regroupait A. (militant du CAL Moselle), E. (militante du CAL Meurthe et
Moselle), T. (militant du CAL Marne) et Fouad. La seconde voiture de ce convoi regroupait Y.(militante du CAL moselle) accompagnée de son enfant et R. (militant du CAL Moselle et de
l'ETPICS 57 CNT).
Les deux voitures ne sont pas arrivées au même moment sur le site du Roucous, car la première a dû
faire un arrêt sur le trajet afin d'acheter du matériel de camping. Aussi, c'est seulement en début de
soirée que E., Fouad et leurs camarades de la première voiture montent leurs toiles de tente, alors
que la nuit commence à tomber. Pour cette raison, ce montage se fait à la va vite. La tente de Fouad
est montée correctement tandis que la tente de E. (une tente "2 secondes") n'est pas bien montée
(elle est de travers et dans une pente).
Les autres camarades du Grand Est, alors fatigué·es par la route vont se coucher et s'endorment,
relativement tôt, tandis que E. et Fouad poursuivent la soirée ensemble, accompagné·es d'autres
camarades participants au camping du Roucous. Cette soirée est très festive, et agrémentée de
consommations d'alcool en grande quantité et des drogues variées (cocaïne notamment).
1. L'accusation et les éléments à charge
a) Le déroulé des faits et les accusations formulées par la victime supposée
E. explique devant la commission non mixte d'AL qu'elle quitte cette soirée au petit matin, en
compagnie de Fouad et qu'ils·elles se dirigent vers leurs tentes.
E . explique alors : "Fouad me dit "tiens pose ta tente à côté de la mienne" , je dis "OK mais
attention je ronfle quand je prends mes médocs" et puis, puisque nous stockons tous nos gros sacs
de fringues dans son auvent c'est plus simple pour moi.... ma tente est plus haut, démontée, je la
traîne alors à côté de la sienne je commence à m'installer.... il me dit "viens dormir avec moi
regarde ta tente elle est pas montée tu seras mieux bla bla et je te ferai un massage". Je lui dis
non, et plusieurs fois non, je rentre complètement dans ma tente, la ferme, je fouille dans mon sac à
main je prends mes medocs, défais mes chaussures, et m'installe sur le dos ma tête touche le fond
de la tente, le haut de la tente me colle au nez, et mes pieds dépassent, parce que le matelas est
épais et que la tente n'est pas montée. Je m'énerve toute seule, Fouad m'entend et me dit "bah
viens, tu monteras ta tente demain". Je lui dis "OK.""
Puis :
E. : "j'arrive à sa tente avec ma couette, mon nounours que je mets sur la tête, et mon oreiller .... je
lui dis plusieurs fois FORT ET CLAIR (au cas où je ne le connais pas si bien et me méfie) :
"FOUAD TU NE ME TOUCHES PAS, TU NE M'APPROCHES PAS TU ME LAISSES
TRANQUILLE JE VIENS ICI POUR DORMIR TU GARDES MINIMUM 1M50 DE DISTANCE
AVEC MOI!!!" ( d'ailleurs la femme de la tente d'en face sort pour m'engueuler après ces phrases
prononcées fortement et je lui dis que tout va bien) je rentre dans sa tente énorme, m'enroule dans
ma couette, je suis restée habillée par convenance, me mets à l'opposé de Fouad , le dos tourné à
lui, mon nounours sur la tête, l'oreiller dessous, de toute façon j'ai pris mes médocs donc je ferme
les yeux je sais que je m’endors."
La suite du récite de E. est ensuite rapporté dans les termes suivant par la commission non mixte
d'AL :Rapport de la commission : "Dans son témoignage, E. déclare s’être réveillée une première fois
dans une position inconfortable, sans oreiller, sans couette, sans le nounours avec lequel elle
dormait. A. affirme que sa robe était remontée sur ses hanches et son collant "est troué sur environ
20-30 cm de diamètre entre mes jambes et mes fesses", son sous-vêtement est tiré sur le côté droit
de son pubis. Elle se souvient avoir eu peur et ne pas avoir compris ce qui s’était passé. Son
témoignage fait état d’un sentiment de panique. Elle déclare ensuite avoir récupéré son nounours et
sa couette qui s’étaient retrouvé·es du côté de Fouad, qu’il a grogné mais ne s’est pas réveillé.
Dans son témoignage, A. déclare qu’elle s’est à ce moment-là rhabillée et qu’elle se force à se
rendormir pour se réveiller vers 10h-11h. À ce moment, Fouad n’est plus là. Elle tente alors de ne
pas penser à son premier réveil et part remettre sa tente à sa place initiale et à la monter
convenablement."
b) Les propos qui auraient été tenus dans les jours qui suivent par l'accusé à la victime
supposée
-
Le lendemain en journée
Rapport de la commission : E. se lève et rejoint les espaces collectifs pour manger. Elle affirme que
Fouad "exige" qu’elle lui masse à nouveau les épaules, "ça me gonfle mais je m’exécute" dit-
elle. E. continue son témoignage : "à ce moment il me répète avec véhémence que ça ne se fait pas
du tout de lui avoir dit "me touche pas etc......." En faisant référence aux paroles prononcées
quelques heures plus tôt.
-
Le lendemain en soirée (2 ème soir au Roucous)
E. : "Le soir venu je pars pas très tard me coucher là où j'ai planté ma tente, B. me suit et me
demande de venir dormir avec lui, pour un massage etc... je lui rétorque un NON méchant et
l'ignore, pour m'excuser je lui envoie un message sur FB pour l'éconduire gentiment.... en disant
"non je ne vais pas dans ta tente d'ailleurs hier j'ai pris mes médocs ... et merci tu masses bien»
pour ne pas qu'il soit trop blessé...."
-
Les jours suivants
Rapport de la commission : A. continue son récit à propos de la 3 ème soirée au Roucous : "B. vient
me revoir au milieu de celle-ci me reproposant de la C, je n'accepte pas, il m'emmène vers un banc
où il n'y a personne sous l'auvent de la pièce où l'on déjeune et commence à me parler de la nuit où
j'ai dormi à côté de lui, il me questionne d'un coup sur des choses inconnues à mon service".
Dans son témoignage, A. affirme que B. lui a demandé si elle se souvenait de ce qu’il s’est passé
dans la tente et pourquoi est-ce qu’elle lui a répété de ne pas la toucher. A. rapporte que, au cours
de cette discussion, B. lui raconte ce qu’il lui a fait : attouchements et pénétration avec les doigts,
mais qu’il s'est arrêté lorsqu’il s’est rendu compte qu’elle était inconsciente. A. dit qu’elle a
compris à ce moment-là l’état dans lequel elle s’est réveillée ce matin-là. Rappelons qu’une
pénétration du sexe avec les doigts sans consentement explicite constitue un viol. A. rapporte que
s’en suit une conversation durant laquelle elle essaie de lui faire prendre conscience de son acte
sans mentionner les termes ni de viol, ni d’agression sexuelle.
Dans la suite de son récit, A. déclare que B. lui a à nouveau proposé ce soir-là de la cocaïne,
qu’elle va se coucher une heure plus tard, qu’elle s’installe dans sa tente... mais "quelqu'un
secoue ma tente, c'est B. il me dit "viens dormir avec moi" je lui répète 3 fois non laisse-moi
tranquille...." . De plus, A. déclare au sujet des jours suivants : "je continue d'envoyer chier B. à
chacune de ses tentatives de me faire entrer dans sa tente, ou quand il vient secouer ma tente".Sur la dernière nuit au Roucous, A. déclare : "je recroise B. près des tentes, il me dit, son
sempiternel "allez viens dormir avec moi", je lui dis non, il reviendra quand même secouer ma tente
et je lui rétorque toujours le même NON! Il est 6 h."
Rapport de la commission non mixte d'AL : Dans son témoignage, E. rapporte une discussion
qu'elle a eu avec Fouad où il lui raconte par le menu ce qu'il lui a fait, s'étonnant dans un premier
temps qu'elle ne s'en souvienne pas, puis en justifiant ses actes au prétexte que son corps réagissait,
manifestant ainsi son consentement. A. rapporte ses propos ainsi : "je t'ai embrassé et là j'ai bien
vu que tu dormais mais j'ai continué de te toucher le corps parce que tu avais l'air d'aimer".
c) Un changement de comportement de la victime supposée pendant le séjour au
Roucous
Rapport de la commission : Trois personnes mentionnent également qu’elles ont remarqué un
changement dans le comportement de A. qui pourrait être expliqué par une agression sexuelle.
Deux camarades présents au Roucous mentionnent "un certain mal-être de cette dernière" à
partir de la moitié du séjour et qu’à partir du lendemain de la première nuit A. "restait les 3/4 du
temps enfermée dans sa tente ou au camping mais ne participait plus beaucoup aux espaces
collectifs en journée. Elle avait l'air de souffrir...". Enfin, une dernière personne en contact
téléphonique pendant le Roucous avec A. mentionne un changement dans leurs échanges, entre le
premier jour et les jours suivants. Dans les mois qui suivent il indique avoir eu des contacts
téléphoniques ou de visu avec A. Elle "semble submergée, désemparée", plus loin le témoin dit : «
cet incident a d'après moi fortement entaché sa confiance en elle, sa confiance en autrui et surtout
j'ai pu voir sa peur vis-à-vis de ce qui pourrait lui arriver que ce soit dans sa vie "civile" ou dans
sa vie "militante"".
d) Le mensonge de Fouad lors de son audition au sujet de la prise de cocaïne et de
médicaments
Rapport de la commission non mixte : Lors de son audition, Fouad a reconnu s'être alcoolisé
plusieurs soirs. Il reconnaît aussi avoir apporté des drogues dures, en avoir consommées et en
avoir offert aux camarades présent-e-s lors des soirées. Toujours lors de son entretien, Fouad
affirme ne pas en avoir spécifiquement donné à E. mais "pense" qu'elle en avait consommé. Il
nous a déclaré : "moi j'ai partagé ce que j'avais, j'ai pas vérifié qui en prenait ou pas." E.
confirme en avoir consommé mais affirme avoir prévenu Fouad des effets que cela lui fait : "à part
me faire dormir ça me fait pas grand-chose". Un autre témoignage met aussi en doute le fait que
B. ne savait pas avec certitude que A. avait consommé de la drogue. Le camarade relate : "Fouad
m'a pris par les épaules et m'a alors proposé "qu'on se fasse une trace", si je me souviens bien de
l'expression, et pendant que je déclinais l'invitation nous avons été rejoints par E. Il n'a pas été
insistant outre mesure face à mon refus. E. et Fouad sont rentrés tous les deux dans un toilette et
moi dans un autre".2. La défense de l'accusé
a) Version de l'accusé sur le déroulement des faits
Fouad confirme tout d'abord le déroulé de la première soirée au Roucous. Il décrit de la même
manière que la victime supposée, l'arrivée sur place et le montage des toiles de tente : la sienne est
montée correctement, plus grande, celle de E. est une tente "deux secondes" qu'elle n'a pas réussi à
bien installer parce qu'il faisait noir et que la partie du terrain sur laquelle elle avait souhaité
l'installer était trop pentue.
Fouad explique ensuite qu'il a poursuivi la soirée avec E. et un certain nombre d'autres camarades
d'AL jusqu'au petit matin.
Fouad décrit également le caractère festif, très alcoolisé et la consommation de cocaïne qui a eu lieu
pendant cette soirée. Il a déclaré dès ses premières auditions par l'AG de notre syndicat avoir lui-
même apporté cette substance et en avoir proposé lors de cette soirée à différentes personnes qui
participaient à la soirée. Il déclare en avoir proposé à E. laquelle a accepté, tout en lui indiquant que
cette drogue n'avait pas vraiment d'effet sur elle.
Selon Fouad, lui-même et E. sont bien rentré·es ensemble de la première soirée au Roucous. Il
explique que sur le trajet du retour vers le campement, trajet effectué sur un petit chemin et qui
prend quelques minutes, E. est tombée à plusieurs reprises, cela en raison de son état d'ébriété.
Sur ce trajet, Fouad explique que E. lui a évoqué son problème de tente mal montée. Fouad
reconnaît lui avoir proposé, s'il elle le souhaitait de venir dormir dans sa tente. Il justifie cette
proposition par ces termes. Fouad : "Au final je comprenais bien son problème de tente, c'est vrai
que ça devait être la galère pour dormir dans cette pente, et puis il s'agissait aussi d'avoir une
attitude bienveillante envers quelqu'une qui ne gérait plus rien dans l'état où elle était". Ce à quoi,
selon Fouad, E. aurait répondu qu’elle préférait d'abord essayer de déplacer sa tente sur une surface
plus plane.
Arrivé·es au niveau du campement, Fouad et E. se seraient dirigé·es vers leurs toiles de tente
respectives afin de s'y coucher. Fouad explique qu'il s'allonge alors dans sa tente, "défoncé" et
qu'il "n'a qu'une envie, [s]'endormir". Selon lui, il en est empêché parce que E. s'énerve dans sa
tente située un peu plus loin. Il explique alors qu'il est excédé par le fait qu'elle râle et l'empêche
ainsi de s'endormir et qu'il a compris que si E. s'énerve, crie et parle fort, c'est en raison de la
mauvaise installation de sa tente et de la pente dans laquelle elle se trouve. Fouad lui aurait alors
conseillé de déplacer sa tente, afin de la rapprocher de la sienne dans un secteur moins pentu, et dit
que c'est ce qu'a fait E. Mais d'après Fouad, ceci ne suffisait pas à résoudre le souci de tente de E.
qui continuait à se plaindre bruyamment, jusqu'à lui demander elle-même s’il était possible qu'elle
vienne dormir dans la tente de Fouad.
Fouad déclare être "saoulé par E. qui n'arrête pas de gueuler" et avec qui il "n'a aucune envie"
qu'elle vienne dormir dans sa tente en raison de son état d'agitation, a tout de même accepté pour
les mêmes raisons que celles indiquées précédemment, c'est à dire par bienveillance à l'égard d'une
personne en difficulté.
Fouad rajoute en plus : "Non seulement je ne pouvais pas la laisser dans cette situation, mais en
plus je n'en pouvais plus de ne pouvoir m'endormir à cause de ses beuglements et je me suis dit que
ça lui permettrait peut-être de s'endormir elle aussi et donc me foutre la paix".
Fouad explique que E. est donc venue dans sa tente "avec sa couette et son doudou" et qu'elle eut
un comportement qu'il qualifie de "toujours aussi insupportable". Il décrit ce moment enargumentant "Elle n’arrêtait pas de se retourner dans tous les sens me prenait la tête. Elle me
répétait sans cesse, en parlant très fort, que je ne pourrais pas avoir de relations sexuelles avec elle
et m'a aussi m'expliqué qu'elle devrait prendre ses médicaments pour dormir. Elle a d'ailleurs
essayé de m'en mettre dans la bouche." Fouad explique que l'un·e et l'autre se seraient finalement
endormi·es dos à dos sans qu'il ne se passe quoi que ce soit. Nous précisons que, dès les premières
auditions de Fouad, notre AG lui a demandé à de nombreuses reprises s'il ne s'était vraiment
strictement rien passé, insistant sur le fait que lorsque deux personnes sous l'emprises de multiples
drogues se retrouvent dans une même toile de tente il peut se passer de nombreuses choses que l'on
ne réalisent pas nécessairement sur le moment, qu'à ce moment-là des gestes, dans les deux sens,
ont peut-être été commis et mal interprétés par l'un ou l'autre des protagonistes. Fouad a répondu
catégoriquement à chaque fois qu'il ne s'était strictement rien passé et qu'il en avait la certitude.
Plusieurs camarades au sein de notre AG ont également rappelé à Fouad que nous le connaissions
comme quelqu'un qui appréciait rentrer accompagné en fin de soirée et que cette opportunité de se
retrouver avec E. sous la tente l'avait peut être conduit sous l'effet des drogues, à se complaire dans
cette situation inattendue et éventuellement en profiter pour se rapprocher de quelqu'une qu'il
appréciait et aurait pu susciter en lui un désir sexuel. Fouad a à nouveau été catégorique, et a
expliqué que "quel que soit mon état, E. n'est pas le genre de fille qui m'attire".
Lorsque l'accusation a été présentée à Fouad, dans les termes qui sont ceux de la victime et du
rapport de la commission non mixte d'AL, celui-ci a réitéré les mêmes réponses aux questions de
notre AG et a nié l'intégralité des faits qui lui sont reprochés durant la nuit sous cette tente.
De la même manière, il a déclaré à la commission non mixte d'AL, tel que le cite son rapport :
Rapport de la commission d'AL : Dans son audition, B. affirme qu’ "il ne s’est rien passé sous la
tente, absolument rien."
b) Le récit de l'accusé sur la suite du séjour
Fouad déclare que le lendemain, il s'est levé avant E. et qu'il ne l'a recroisée qu'en début d'après-
midi. Il raconte alors qu'il serait allé la trouver afin de lui faire remarquer les dangers que présentait
pour elle sa consommation de drogue en soirée.
Fouad a expliqué à notre AG, que la suite de son séjour s'était déroulée de manière globalement
agréable et qu'il l'avait apprécié.
Il explique que ses rapports à E. étaient par contre assez problématiques parce qu'ils n'étaient pas du
tout en phase avec son caractère. Fouad décrit alors une personne lourde, pénible et qu'il a beaucoup
de mal à la supporter, notamment en raison du fait qu'elle est tout le temps "défoncée et qu'elle ne
gère pas". Il reconnaît ne pas avoir été spécialement agréable avec elle durant le reste du séjour
mais qu'ils·elles ont tout de même passé des moments sympathiques.
Fouad a précisé dès ses première auditions à notre AG qu'ils·elles "s'étaient pris la tête plusieurs
fois pendant cette semaine" mais qu'elle et lui avaient continué à faire les soirées du camping
ensemble puisque les gens qui faisaient la fête le soir le faisaient collectivement dans l'espace
commun. Selon Fouad "Elle était en soirée avec nous, avec tout le monde, tout au long de la
semaine."Fouad raconte ensuite le retour en voiture en direction de la Lorraine. Ce trajet de retour s'effectue
avec A. (le même conducteur que pour le trajet aller), E. et lui-même dans une voiture commune.
Fouad décrit un trajet qu'il juge insupportable parce que E. n'arrête pas de lui prendre la tête et nous
explique avoir fini par s'emporter à l'encontre de E. de façon plutôt virulente. Il dit ne plus se
souvenir des termes qu’il a employé mais déclare à notre AG : "Je n'en pouvais plus j'ai explosé.
Je l'ai défoncée en lui disant ses 4 vérités, tout ce que je pensais d'elle et je lui ai dit expliqué
qu'elle m'avait gonflé tout au long de la semaine au Roucous". Fouad explique que l'altercation a
été violente oralement pendant quelques minutes, et que E. a fini par lui dire "Ca, tu vas me le
payer" avant de s'enfermer dans le silence pendant environ une heure, jusqu'à une prochaine pause
sur l'autoroute. L'ambiance se serait ensuite apaisée et Fouad indique «ça allait mieux ensuite, je lui
ai même payé son MacDo à la pause suivante."
Fouad rajoute que lorsque A. a déposé E. à Nancy sur le trajet du retour (avant de poursuivre tous
les deux la route vers Metz), il avait ressenti un "véritable soulagement" qui selon lui était
largement partagé par A., également exaspéré par l'attitude de E. pendant le trajet.
c) Défense de l'accusé sur la question des propos qu'il aurait tenus à la victime dans les
jours qui ont suivi les faits
Notre AG a soumis a Fouad les propos de E. relatés dans le rapport de la commission non mixte
d'AL et qui explique que Fouad et elle aurait eu une discussion dans les jours qui ont suivis les faits
et lors de laquelle Fouad lui aurait raconté "par le menu ce qu'il lui a fait."
Confronté à cette accusation, celui-ci se défend d'avoir tenu de tels propos et explique qu'il a
effectivement évoqué la nuit passée sous la tente avec E. le lendemain, mais pour lui expliquer
qu'elle ne gérait pas du tout la situation lorsqu'elle consommait de l'alcool et des drogues et que cela
pouvait être dangereux. C'est également ce qu'a déclaré Fouad lorsqu'il a été auditionné par la
commission non mixte d'AL. Et cette version correspond à celle qu'il avait livrée spontanément lors
de ses premières auditions.
Fouad estime par ailleurs que E. est incohérente lorsqu'elle dit que lors de la discussion du
lendemain : «je ne comprends pourquoi, si j'avais abusé de son état pour avoir des relations
sexuelles avec elle je serais allé la trouver le lendemain pour lui en faire état, elle qui est une
militante féministe d'AL."
Fouad dit également avoir été choqué par les propos répétés de E. disant la veille en le rejoignant
dans sa tente : "je viens dormir dans ta tente mais je te préviens, il ne se passera rien, tu ne me
niques pas". Fouad précise s'être confié à ce sujet à plusieurs camarades dès le lendemain, lui
expliquant qu'il ne comprenait pas cette attitude de E., alors qu'il n'avait de toute façon aucune
intention sexuelle à l'égard de E, qui ne l'attire pas du tout.
Fouad nie également catégoriquement avoir proposé par la suite à E. de dormir à nouveau sous sa
tente.d) Défense de l'accusé vis à vis d'un possible changement d'attitude de E. pendant le
séjour
Notre Assemblée Générale a demandé à Fouad de s'exprimer sur ce point après l'avoir confronté au
rapport de la commission non mixte d'AL qui évoque le fait que la victime aurait montré des signes
d'un mal être, lequel pourrait être expliqué par le fait d'être victime d'agression sexuelle.
Fouad a réagi en indiquant qu'il "ne comprenait pas que des témoignages aient pu décrire une telle
attitude de E. pendant la suite du séjour". Qu'en ce qui le concernait il l'avait trouvé "fidèle à elle-
même pendant toute la semaine, à faire la fête chaque soir jusqu'au petit matin et à se défoncer au
point d'en dormir ensuite toute la journée".
Sur ce point Fouad rajoute qu'il ne comprend pas "qu'on puisse voir dans son comportement
l'attitude d'une femme agressée sexuellement alors qu'elle se serait ventée publiquement au cours de
la semaine "d'avoir niqué avec un mec qui portait un chapeau à peine deux jours après cette soi-
disant agression de ma part."
Fouad a également déclaré pour sa défense qu'il s'étonnait du fait que "s'il l'avait violé" alors
pourquoi "elle lui présente spontanément deux de ses amies, femmes, lors de la dernière soirée au
Roucous" mais aussi sa sœur, le lendemain : "lorsque nous l'avons déposé à Nancy sur le trajet
retour vers Metz, elle nous a demandé d'attendre. Elle est montée chez elle puis est redescendue et
nous a présenté sa sœur, à moi et à A."
e) Défense de l'accusé sur son mensonge lors de son audition au sujet de la prise de
cocaïne
Notre AG a interrogé Fouad sur le pourquoi il avait dit devant la commission non mixte d'AL qu'il
ne savait pas si E. avait consommé ou non de la cocaïne alors qu'il avait précédemment déclaré à
notre syndicat qu'il en avait proposé à tout le monde, et qu'elle en avait consommé.
Sur ce point Fouad explique : "mais parce que je ne suis pas une balance ! Une instance interne de
l'orga me demandait qui et qui avait consommé tel produit au Roucous. J'ai considéré que je
n'avais pas à leur dire ce qui relevait de la vie privée des militant·es."
3. Contenu des témoignages recueillis par notre syndicat
Nous précisons ici, tel que nous l'avions fait dans la partie méthodologique de ce compte rendu, que
notre syndicat a auditionné 3 témoins qui ont accepté de s'exprimer devant notre Assemblée
Générale. Tou·tes 3 sont membres du CAL Moselle d'Alternative Libertaire qui ont fait le
déplacement au camping du Roucous avec E. et Fouad.
Y., est une femme, dans la trentaine, ancienne militante de la CNT aujourd'hui adhérente du CAL
Moselle d'Alternative Libertaire.
R., est un homme, âgé de la quarantaine, militant du CAL Moselle et de la CNT (syndicat ETPICS
57).
A. est un homme la trentaine, militant du CAL Moselle d'AL (et accessoirement de Solidaires).a) Sur le déroulé des faits
Y., A. et R. décrivent le même contexte d'arrivé au camping du Roucous, et confirme que le
montage de la tente de Fouad est correct tandis que celle de E. est vraiment mauvais. Tou·tes les
trois expliquent ensuite qu'ils·elles étaient très fatigué·es par le trajet et qu'ils·elles sont donc allé·es
se coucher très peu de temps après ce montage de leurs propres tentes "situées, à quelques dizaines
de mètres des leurs, un peu plus haut que le secteur dans lequel étaient installé·es Fouad et E."
Ces trois témoins ne participent donc pas avec Fouad et E. à la première soirée du camping
qu'ils·elles ne peuvent donc rien décrire de cette première soirée.
Cependant, deux de ces témoins apportent des éléments sur ce qu'ils·elles ont entendu au petit matin
alors que E. et Fouad étaient rentré·es à leur tente.
Y. déclare qu'elle somnolait au petit matin lorsqu'elle a entendu E. parler fort, mais sans pouvoir
comprendre ce qu'elle racontait.
A., quant à lui est formel et déclare : "Au petit matin, j'étais encore couché mais je ne dormais
plus, j'étais réveillé, sans doute parce qu'à l'extérieur il faisait déjà jour. J'ai entendu des éclats de
voix qui venaient de la direction des tentes de Fouad et de E. qui parlaient fort, puis le bruit de E.
qui déplaçait sa tente dans la direction de celle de Fouad. Je n'ai pas entendu l'ensemble de la
conversation et ne me souviens plus des propos exact des un·es et des autres, mais j'ai compris
clairement de la situation que E. était en train de demander à Fouad de pouvoir dormir dans sa
tente."
Notre syndicat constate que sur ce point factuel, le témoignage de A. est contradictoire avec les
déclarations de E. et semble accréditer la version de Fouad.
R., n'a pas pu apporter d'information sur le déroulé de ce moment-là, car il dormait profondément.
Notre syndicat a par ailleurs demandé à A., R. et Y. s'ils·elles pensaient que le récit de Fouad
pouvait être crédible lorsqu'il expliquait que E. était tombée plusieurs fois sur le chemin du retour. Il
était important pour notre AG de disposer d'éléments à ce sujet dans la mesure où l'accusation
insiste sur la question d'un collant troué, tandis que l'accusé s'en défend en disant qu'il n'est pas du
tout étonné de ce collant abimé puisque E. aurait chuté plusieurs fois au sol avant de se coucher. A.
a indiqué lors de son audition : "je ne sais pas ce qui s'est vraiment passé car je n'y étais pas sur ce
petit chemin de retour, mais ce que je peux dire c'est que ça ne m'étonnerait pas du tout. J'ai vu
beaucoup de gens ivres morts, et E. est vraiment le genre de personne qui quand elle a bu titube
littéralement. Ça arrive quand même a peu de gens de ne pas tenir sur leur jambes après une cuite
mais chez elle s’est récurrent."
b) Sur les jours qui ont suivi la première soirée au Roucous
Y., A. et R. expliquent tout d'abord qu'ils·elles n'ont pas vécu le camping du Roucous sur le même
rythme que E. et que Fouad. Ils·elles expliquent dans des témoignages tout à fait concordants
qu'ils·elles ont tou·tes les trois passé cette semaine sur un rythme classique. Y : "nous ont vivaient
le jour et on dormait la nuit, d'autant que personnellement j'étais au camping avec mon fils. E. et
Fouad c'étaient des oiseaux de nuit".Ces 3 témoins expliquent donc qu'ils·elles n'ont pas partagé beaucoup de moments avec les deux
protagonistes, mais qu'ils·elles les ont croisés quotidiennement, généralement en début de soirée
alors que ces dernier·es "venaient de se lever et se rendaient aux espaces communs, en général à
l'heure du dîner".
Ces trois témoins n'ont pas assisté aux discussions qui ont eu lieu entre E. et Fouad tout au long de
la semaine, mais confirment de façon unanime que E. et Fouad ont continué à passer des moments
et à "boire des coups" ensemble.
Notre AG a interrogé ces 3 témoins sur l'attitude potentiellement agressive de Fouad à l'égard de E.
durant cette semaine au Roucous, et qui a été relaté par le rapport de la commission non mixte
d'AL. Sur ce point A. et R. expliquent que c'était une relation "un peu électrique". A., donne un
peu plus de détails que R. et explique : "c'est clair qu'ils·elles n'arrêtaient pas de se prendre la tête
tous les deux, tout en étant régulièrement dans les mêmes espaces. L'un·e et l'autre s'envoyaient des
"tacles" et se critiquaient mutuellement en public. Les termes n'étaient donc pas forcément très
amicaux mais ce qui est décrit dans le rapport de la commission non mixte ne correspond pas
vraiment à ce à quoi j'ai pu assister personnellement."
Y., quant à elle insiste plus sur les termes employés par Fouad à l'égard de E. qu'elle a trouvé
relativement durs : "ils·elles s'engueulaient l'un·e l'autre, mais j'ai trouvé que Fouad allait parfois
trop loin dans les termes qu'il utilisait." avant de nuancer son propos en disant "Mais en tant que
femme, je sais que je peut-être très vite touchée lorsqu'un homme n'est pas sympa avec une
femme."
Finalement, A., confirme les déclarations de Fouad au sujet de l'altercation qui a eu lieu dans la
voiture sur le trajet du retour et précise, dans les mêmes termes que Fouad : "la déposer à Nancy a
été un soulagement pour moi, je n'en pouvais plus de sa présence."
c) Sur le possible changement d'attitude de E. pendant le séjour
Y., A. et R. font des déclarations concordantes sur ce point et expliquent qu'ils·elles n'ont pas du
tout eu le sentiment que E. avait changé d'attitude au cours du séjour du Roucous.
Y., est plutôt évasive à ce sujet : je n'ai pas le sentiment que son comportement a évolué. "Je ne la
trouvais pas très bien comme personne, on a eu l'occasion de discuter quelques fois pendant la
semaine, mais de là à mettre en perspective avec une potentielle agression sexuelle... Surtout
qu'elle était tout le temps défoncée dès que je la croisais à son réveil."
A. et R. sont un peu plus clair sur ce point. A. déclare notamment "j'ai été choqué par ce
paragraphe du rapport de la commission nous mixte expliquant qu'elle aurait changé de
comportement pour rester prostrée dans sa tente. Effectivement, quand je la croisais elle n'avait pas
l'air en grande forme, comme quelqu'une qui s'était défoncée toute la nuit quoi...Mais laisser
penser à tout le monde qu'elle restait prostrée dans sa tente pour fuir son agresseur n'est pas
conforme à la réalité. On ne le voyait pas de la journée parce qu'elle faisait la fête chaque jour
jusqu'au petit matin et qu'ensuite, elle dormait toute la journée."d) Réponses des témoins aux questions complémentaires de notre syndicat
-
Question SSCT Lorraine : Avez-vous reçu des pressions de la part de Fouad concernant
votre témoignage devant la CNT ?
Y., A. et R. expliquent que Fouad leur a demandé à plusieurs reprises s'ils·elles pouvaient apporter
leurs témoignages auprès de la commission non mixte d'AL, ainsi qu'à la CNT qui devrait se
prononcer sur cette affaire. Fouad ayant déjà informé notre AG qu’il était entré en contact avec ces
témoins pour les solliciter, nous avons alors demandé dans quels termes avait été formulée cette
demande. Tou·tes les 3 nous ont alors certifié que Fouad leur avait simplement demandé de dire ce
qu'ils·elles savaient, leur version telle qu'ils·elles l'avaient vécue et avait lui-même précisé "je ne
vous demande pas d'inventer quoi que ce soit, mais vos témoignages sont importants".
A. explique également : "en ce qui concerne la CNT, nous ne voulions pas témoigner avant de
savoir quelle serait la décision d'AL. Il faut nous comprendre, c'est notre orga, et en interne notre
situation n'est déjà pas facile car nos témoignages ne vont pas dans le sens de l'accusation."
-
Question SSCT Lorraine : Et au sein d'AL avez-vous finalement participé à l'appel à
témoignage lancé par la commission non mixte ?
R. : "je le souhaitais, mais malheureusement je n'ai pas réussi à envoyer mon témoignage. Vous me
connaissez, moi et les ordinateurs ça n'a jamais fait bon ménage. Ce jour-là le mail ne voulait pas
partir et mon témoignage n'est donc jamais parti. Questionné par notre syndicat sur le pourquoi il
n'avait pas songé à envoyer un témoignage par courrier postal R. a expliqué que s'eut été une
possibilité, mais qu'avec le travail, il n'y avait pas pensé et s'en sentait désolé en comprenant que
son témoignage aurait eu de l'importance.
A. a bien envoyé un témoignage écrit à la commission non mixte d'AL et se déclare dubitatif sur le
fait que son témoignage n'ai jamais été cité dans le rapport de la commission. Il explique qu'il a
notamment clairement écrit dans le document qu'il a envoyé le fait qu'il avait entendu E. demander
elle-même à Fouad de pouvoir venir dormir dans sa tente. Il nous montre son témoignage qu'il nous
fait lire sur son téléphone portable. Nous constatons alors que le passage qu'il a rédigé sur ce qu'il a
entendu au petit matin, juste avant l'agression potentielle, représente pourtant 6 ou 7 lignes
dactylographiées et plusieurs phrases très précises qui infirment le récit de E.
Y., explique avoir bien envoyé un témoignage écrit à l'attention de la commission non mixte. Elle
précise également le contexte dans lequel elle a rédigé son témoignage en indiquant : "Vous pouvez
utiliser mes termes comme tels : j'ai vécu une véritable injonction à soutien. J'ai reçu de nombreux
coup de fils de E. une fois les accusations dévoilées. Et puis, j'ai aussi reçu de nombreux appels des
filles de la commission non mixte. L'une d'elle m'a appelé à de multiples reprises pendant que
j'écrivais mon témoignage pour me dire ce que je devais plus ou moins développer. Une fois que j'ai
envoyé mon écrit, on m'a à nouveau appelé pour me dire qu'il fallait que je modifie certains
passages. J'ai refusé en répondant que j'avais écrit ce que j'avais à dire et que c'était maintenant à
elles de faire ce qu'elles avaient à faire avec ça". Y. rajoute ensuite : "J'ai eu le sentiment que ma
parole n'était pas libre, que si je ne soutenais pas la camarade, alors je n'aurais plus ma place dans
le milieu militant."
Suite à son audition par notre AG, Y. a envoyé un mail à notre syndicat dans lequel elle revient sur
ce point. Elle écrit :"Ce que je peux apporter de plus, grossièrement : [...] L’injonction finalement pesante à tenir une
position en soutien de la victime, à l’entrave de laquelle tu es systématiquement taxée de
complaisance à l’égard du violeur, puisque violeur il est à partir du moment où il est désigné
comme tel, remettre en question la parole de l’accusatrice étant inconcevable. Le dossier préalable
à l’appel à témoignages était éloquent en ce sens, et à inciter à taire certains passages, à aller dans
le sens de la victime. Deux poids deux mesures. Ne pas juger les mœurs et le contexte attenant à la
victime. En revanche, les extrapolations sur le comportement de l’accusé sont allées bon train,
nous interrogeant sur la capacité de l’AL à intégrer des prolos (qui parlent forts disent des gros
mots et n’ont pas maitrise h24 de leur langage, ça avait l’air digne d’être mentionné dans le
rapport, et nous fait question). À partir de là, quoique tu dises quoi que tu fasses, tu es soit
naïvement manipulée, voire carrément une traitresse..."
Ainsi que :
Mon témoignage (celui envoyé au sein d'AL) est écrit immédiatement à l'issue de la lecture du
dossier préalable au recueil de témoignages, et de plusieurs contacts avec l'accusatrice, la
commission antipat. Tout ça transpire grave dans mes quelques mots, moi j'ai essayé de faire court
et de ne pas extrapoler... Les faits restent, mais évidemment l'ordre, les tournures, les incidents mis
en lumière n'auraient pas été les mêmes au mois d'août, ni aujourd'hui, après discussions avec mes
camarades du CAL etc...
-
Question SSCT Lorraine : avez-vous signalez ce que vous dites là au Secrétariat Fédéral
d'AL ?
Y., A. et R., expliquent qu'ils·elles ont alors rédigé un courrier signé du CAL Moselle où ils·elles
expliquaient l’incompréhension vis à vis du fait que les informations apportées par leurs
témoignages n'apparaissaient quasiment pas dans le rapport rédigé par la commission non mixte.
Dans ce courrier, ils déclarent également avoir exprimé leur désaccord avec le non-respect de la
présomption d'innocence et avec le fait que la défense de l'accusé n'avait pas été respectée ;
Y., A. et R., ont également précisé que ce courrier du CAL Moselle n'avait pas été transféré à
l'ensemble des adhérent·es d'AL et qu'il avait été conservé par le secrétariat fédéral, précisant qu'il y
avait là un problème démocratique à leurs yeux.
-
Question SSCT Lorraine : Quand avez-vous eu connaissance des accusations de E. au sujet
de Fouad ?
Y. explique qu'elle a été la première personne à qui E. s'est confiée sur cette problématique. Cela
s'est passé sur le retour du camping du Roucous : "moi même mon fils et R. étions dans un voiture,
A, E ; et Fouad étaient dans une autre voiture, mais on roulait ensemble et on s'arrêtait sur les
mêmes aires d'autoroute. À une des pauses E. m'a prise à part en me disant vouloir se confier. C'est
là qu'elle m'a expliqué ce qu'elle a expliqué à la commission non mixte. Qu'elle s'était réveillée
avec son collant troué qu'elle n'avait pas compris pourquoi, mais qu'elle avait compris lorsque
Fouad lui a raconté ce qu'il avait fait."
A. et R. expliquent que c'est Y. qui leur a évoqué cette confidence quelques jours plus tard car elle
ne savait pas quoi en faire. Ceux-ci ont alors considéré qu'il s'agissait d'une accusation formulée par
une camarade en qui on ne peut pas vraiment avoir confiance. Ils n'en n'ont parlé à personne et n'ont
pas averti Fouad car pour eux, ces propos étaient trop peu crédibles pour en faire une véritable
affaire.Les propos de Y. et de A. concordent sur le fait que la première confidence de E. auprès de Y. est
intervenue au moment du premier arrêt, qui sur le trajet a suivi l'altercation qui s’est déroulée entre
E. et Fouad dans la voiture de A.
-
Question SSCT Lorraine : savez-vous pourquoi l'accusation a-t-elle éclatée si tardivement,
au congrès confédéral de la CNT, plutôt que chez AL dans un premier temps ?
Y. explique tout d'abord à ce sujet qu'elle a le sentiment d'un "gros gâchis". Y. explique qu'après
avoir réfléchi avec A. et R., elle avait proposé à E. de venir formuler son accusation auprès du CAL
Moselle, dont Fouad est également adhérent, afin de gérer cette affaire localement. Selon Y., E. ne
lui a pas répondu sollicitant un temps de réflexion pour savoir quelles suites elle comptait donner à
tout cela. Y. poursuit : "quand E. m'a rappelé elle m'a dit que suite à plusieurs échanges
téléphoniques avec des gens sur Nancy, on l'avait convaincue de ce confier à la commission
antisexiste de la CNT à Paris plutôt qu'au CAL Moselle, car cela garantirait son anonymat."
Suite à ce propos de Y., notre AG de syndicat lui a demandé si elle souhaitait nous donner l'identité
des personnes qui avaient fait cette proposition à E.. La réponse de Y. est la suivante : «J., de la
CNT 54".
Y. rajoute dans un mail qu'elle nous a envoyé a posteriori de son audition :
"J'ai eu la sensation de trahir par mon silence : lorsque E. m'a annoncé qu'elle avait fait remonter
l'histoire à la CNT. D'ailleurs dès le 13 novembre il m'a appelé. "Y, je suis accusé de viol. Devines
par qui ? E !" (La révélation du nom m'a donné la sensation d'un gros gâchis, pour moi alors la
seule chose qui justifiait cette remontée fédérale c'était le respect de l'anonymat)"
A., quant à lui explique qu'il "n'a pas d'autre explication" mais qu'il y a à ses yeux une certaine
part de manipulation dans ces accusations, et que le crime "profite avant tout à certains adhérents
et à certaines convictions politiques aussi bien au sein d'AL que de la CNT." Avant de rajouter :
"ce sont d'ailleurs sans doute les mêmes personnes."
-
Question SSCT Lorraine : Fouad déclare qu'il a adressé un mail au Secrétariat fédéral d'AL
après avoir pris connaissance du rapport de la commission non mixte et dans lequel il
explique que certains éléments de sa défense n'ont pas été retranscris dans le rapport. Avez-
vous eu connaissance de ce mail ?
A. : "oui parce que Fouad l'a également envoyé au même moment aux adhérent·es du CAL
Moselle. Ce mail a été transmis à tou·tes les adhérent·es d'AL, mais après le vote sur l'exclusion de
Fouad."
-
Question SSCT Lorraine : Fouad connaissait il E. avant le camping du Roucous ?
A. : "Non pas vraiment. Il me semble qu'ils·elles s'étaient croisé·es une fois, dans le cadre
d'une réunion militante. Mais E. le connaissait de réputation. Vous savez, en Lorraine dans
le milieu militant, tout le monde sait qui est Fouad de la CNT."-
Question SSCT Lorraine : il ne s'agit que d’une hypothèse, mais à votre connaissance, E.
aurait-elle des raisons d'en vouloir à Fouad (pour d'autre motif qu'une potentielle agression
sexuelle) ?
Y. : "Ca je ne sais pas. Mais je tiens à vous raconter ce que nous avons vécu 2 mois avant le
Roucous. Avec A. et R., nous étions au camping de Bure. E. était présente et a passé la soirée avec
nous. Ce jour-là, elle avait passé la soirée à disserter sur la taille des couilles de Fouad, elle ne
parlait que de ça... Sur le moment ça nous a choqués, on ne comprenait pas, surtout qu'elle ne le
connaissait que de nom. On l'a ignorée, en se disant simplement qu'elle n'était pas nette. Mais
quand l'accusation de viol est tombée, naturellement, moi, A et R, avons repensé à cette soirée à
Bure et on n'a pas pu s'empêcher de mettre les deux évènements en perspectives. On s'est tou·tes dit
que cette fixation sur Fouad ne datait pas d'hier.
-
Question SSCT Lorraine : Lors de cette soirée à Bure comment à réagit Fouad ?
A. : "Vous avez mal compris, Fouad n'était pas à Bure. Elle parlait de ses couilles, sans le
connaître et en son absence."
-
Question SSCT Lorraine : Le rapport de la commission non mixte et les témoignages qui y
sont cités évoquent une attitude quotidienne de "prédation sexuelle" et d'agressivité de la
part de Fouad au camping d'AL. L'AG du camping a-t-elle été informée de ces
agissements ?
A. : "Euh... Non. Nous on entend parler de ça que depuis la publication du rapport".
4. Analyse de notre syndicat sur les faits
a) Concernant le déroulé des évènements
Tout d'abord, il apparaît évident que l'accusation et la défense décrivent le même contexte d'arrivée
au Roucous. Leurs déclarations sont totalement concordantes sur les heures d'arrivées, sur le
montage des toiles de tente de chacun·e et sur le type de soirée à laquelle ils·elles prennent part,
tous deux : soirée très alcoolisée et lors de laquelle sont consommées de multiples drogues (cocaïne
notamment) tant par la victime potentielle que par l'agresseur supposée et, visiblement les autres
participant·es à cette soirée festive.
C'est la suite des évènements, à partir du moment où E. et Fouad décident de quitter la soirée pour
regagner le campement que les récits deviennent divergents.
Tout d'abord, sur le trajet, Fouad mentionne des potentielles chutes de E., en raison de son état
d'ébriété. Chutes que E. ne mentionne pas. Ce point de divergence entre les deux discours nous
semble important puisque l'essentiel de l'accusation repose sur la base d'un collant, porté par la
victime supposée, et qu'elle aurait découvert troué à son réveil, après avoir passé la nuit avec Fouad.Sur ce point, aucun témoignage concret ne permet d'établir une quelconque vérité, ni de discréditer
l'une des deux versions. Notre syndicat souligne néanmoins deux points. D'une part les témoins que
nous avons rencontrés et qui ont pu constater dans d'autres contextes les conséquences des drogues
sur l'un·e et l'autre des protagonistes, estiment que l'hypothèse des chutes de E. est une hypothèse
plausible. D'autre part, nous précisions que la mention de ces chutes dans le récit de la défense était
bien présente dès les premières auditions de Fouad, avant qu'il ne prenne connaissance de
l'accusation.
Un autre point de discordance majeur concerne le moment du coucher. E. décrit dans son récit que
Fouad aurait lourdement insisté pour que celle-ci puisse venir dormir dans sa tente, arguant du fait
avéré que la tente de E. est mal montée. Dans le discours de l'accusé celui-ci explique qu'il était
bien au courant de ce problème de tente puisque déjà sur le chemin du retour, E. le lui aurait
évoqué. Fouad aurait alors proposé à E. de dormir dans sa tente, proposition à laquelle elle n'aurait
pas donné suite, préférant tenter de déplacer sa tente sur une surface plus plane avant de se rendre
compte que cela ne résolvait pas le problème et de demander à Fouad à plusieurs reprises de
pouvoir de l'héberger sous sa tente.
A ce stade nous précisons que nous nous contentons ici de pointer les divergences entre les discours
de l'un·e et de l'autre. À aucun moment il ne s'agirait de considérer que c'est celui ou celle qui a fait
la proposition de dormir ensemble qui serait responsable d'un acte de viol éventuel si celui-ci a
réellement eu lieu. (La vision très patriarcale qui voudrait que les femmes soit responsables de leur
viol parce qu'elles ont provoqué le coupable sont odieuses). Non il s'agit là simplement pour notre
AG de comparer les deux versions afin de se faire une idée sur la crédibilité globale de chaque récit.
Concernant ce moment du déroulé des faits, un seul témoin factuel a été retrouvé, que cela soit par
nos soins, ou par la commission non mixte d'AL il s'agit de R. Son témoignage écrit auprès d'AL
(bien que le rapport de la commission ne le reprenne pas), tout comme ses déclarations auprès de
notre syndicat sont absolument formel. Ce témoin a bien entendu que c'est E. qui insistait auprès de
Fouad de façon à pouvoir dormir dans sa tente. Sur ce point, il nous semble donc que le récit de la
victime supposée soit moins crédibles que celui de Fouad, dont les déclarations sur le déroulé des
évènements avaient elles aussi été formulée avant qu'il ne prenne connaissance des propos de
l'accusation.
Concernant, ce qui s'est passé sous la toile de tente une fois E. et Fouad couché·es, le discours de
l'un·e et de l'autre est également divergent : sans surprise d'ailleurs puisqu'il s'agit du moment où se
serait déroulé l'acte de viol. Sur ce moment précis, aucun·e témoin, ni oculaire ni auditif, si bien
qu'il est impossible de se faire quelconque idée sur la véracité des déclarations de l'une ou de l'autre.
Concernant ce moment du déroulé des faits, notre syndicat ne peut donc que considérer que nous
sommes dans une situation très claire de parole contre parole.
b) Sur les jours qui ont suivi cette première nuit au Roucous
Tout d'abord concernant les conversations qui se seraient tenues le lendemain des faits présumés
entre E. et Fouad et lors duquel Fouad aurait expliqué à E. ce qu'il lui avait fait pendant la nuit
précédente. L'accusation et la défense reconnaissent tou·tes les deux avoir discuté ensemble le
lendemain, mais le contenu qu'ils·elles décrivent respectivement de cet échange sont très divergents.
E. explique que Fouad lui a fait le récit du viol, tandis que Fouad déclare qu'il aurait eu une attitude
bienveillante, cherchant à expliquer à E. qu'elle se mettait en danger lorsqu'elle consommait des
drogues et de l'alcool.Aucun témoin n'a assisté à cette conversation. Néanmoins, si dans l'hypothèse ou Fouad serait
coupable notre AG a du mal à concevoir qu'il serait alors allé trouver E. pour lui raconter son acte
dans ses moindres détails. En effet, le cas de culpabilité de sa part, et alors que la victime supposée
ne semblait pas encore se souvenir de ce qui s'était passé cette nuit-là, il nous semble que si Fouad
avait réellement tenu ces propos il aurait alors pris, du point de vu d'un coupable de viol, un risque
considérable de se voir accusé de fais de viol. Nous avons néanmoins étudié l'hypothèse, sous
entendue par le rapport de la commission non mixte d'AL, que ces propos auraient pu permettre à
un individu coupable de chercher à minimiser son geste à l'égard de la victime. Nous doutons
néanmoins que cela puisse être le cas dans cette affaire précise puisqu'il était de notoriété publique
que non seulement E. est une militante formée aux questions féministes et d'agressions sexuelles, et
que dans le cadre du camping au Roucous, sont réunis de nombreu.euses militant.es également
formée à l'antipatriarcat. Dès lors nous imaginons mal que, dans le cas où l'accusé serait coupable, il
ait pu prendre le risque d'employer une telle stratégie de défense à un moment où il n'était pas
encore accusé.
Nous rappelons tout de même qu'il s'agit là d'une analyse au regard des éléments dont nous
disposons et qui nous laissent penser que le récit de la défense semble plus cohérent que celui de
l'accusation. Néanmoins nous ne pouvons ici établir aucune vérité sur l'un ou l'autre des récits.
La description des jours qui suivent, jusqu'à la fin de la semaine au Roucous sont également
importants. Ceux-ci sont décrits par différent·es témoins qui se contredisent totalement.
D'une part des témoins affirment que Fouad a été particulièrement agressif avec E.. Ces
témoignages sont ceux que l'on retrouve dans le rapport de la commission non mixte d'AL : ils sont
anonymes et partiellement cités, et laissent penser à leur lecture qu'un potentiel coupable
d'agression sexuelle aurait alors cherché par ce biais à mettre la pression sur sa victime, à la faire
passer pour ce qu'elle n'est pas ; c'est à dire procéder à un "slut-shaming."
D'autre part, des témoignages, ceux des témoins que nous avons auditionnés et qui n'ont pas cherché
à dissimuler leur identité, reconnaissent le fait que Fouad n'a pas été spécialement sympathique à
l'égard de E. pendant ce séjour, mais décrivent la relation entre ces deux individus comme
réciproque. Selon leurs discours, E. et Fouad ne s'appréciaient pas spécialement et avaient des
caractères relativement peu compatibles, ils·elles expliquent les "pics", "les tacles mutuels" que
ces deux se sont envoyé·es tout au long du séjour. Ces témoins attestent également du fait que cette
relation conflictuelle ne les a pas empêcher de partager les mêmes moments festifs dans les espaces
commun du Roucous et cela en compagnie des autres militant·es présent·es.
Notre syndicat considère que cette divergence des analyses peut s'expliquer par différentes
hypothèses :
–
–
les souvenirs de chacun des témoins sont probablement subjectifs, car remontant à certains
temps et à contexte festif, ce qui laisse peu de place à l'objectivité.
la crédibilité des témoins cité·es de façon anonyme par le rapport de la commission non
mixte d'AL peut être remise en cause. Tout d'abord il nous semble étonnant qu'une telle
attitude d'un homme à l'égard d'une femme au beau milieu d'un camping sensibilité à la
question du patriarcat n'ai été pas réprimé sur le moment par une démarche collective des
témoins qui ont assisté à ce qui peut s'apparenter à du "slut-shaming". Ensuite, on peut
supposer que ceux-ci ont été orientés a posteriori : soit de façon inconsciente, une fois que
les témoins ont pris connaissance de l'accusation de viol, soit orientés de façon consciente
par la commission non mixte d'AL qui a tenté cette démarche avec les témoins que nous
avons nous même auditionné·es.c) Sur le possible changement de comportement de E. durant le séjour au Roucous
Ici encore les témoins anonymes et partiellement cité·es (au nombre de 3) par le rapport de la
commission non mixte, et les témoins que nous avons reçus ne déclarent pas les mêmes choses.
Les témoins du rapport d'AL expliquent un changement flagrant de comportement chez la victime
supposée ; changement de comportement qui pourrait être celui d’une femme victime de viol,
notamment lorsque ces témoins décrivent que E. est restée prostrée une grande partie du temps dans
sa tente durant la journée, cherchant ainsi à se tenir à distance des espaces communs et de son
agresseur potentiel.
Les témoins que nous avons auditionnés livrent quant à eux·elles une version bien déférentes :
ils·elles expliquent qu'effectivement personne ne voyait E. dans la journée parce qu'elle dormait
jusqu'en début de soirée, et que cela ne paraissait pas étonnant puisque chaque nuit, E. faisait la fête
et consommait toutes sortes de substances. Leurs récits s'éloignent donc largement de la description
d'une femme apeurée et prostrée en raison du vécu d'une agression. Ils·elles sont formel·les sur le
fait que celle-ci, si elle semblait porter les stigmates de ses consommations au moment de son réveil
(pour être clair, elle avait a minima la gueule de bois), elle ne semblait pas pour autant
particulièrement mal lors de ce séjour, et a vécu de bout en bout une semaine festive et des soirées
partagées avec Fouad, entre autres, dans les espaces communs.
Encore une fois notre AG s'est demandée ce qui pouvait expliquer ces divergences de discours, sur
des points pourtant très factuels, entre les différent·es témoins et a pensé que soit :
–
–
Soit les témoins d'AL ne sont pas crédibles en raison d'une orientation consciente de la
commission non mixte, telle que l'on vécut les témoins que nous avons reçus.
Soit les témoins d'AL ont été orientés subjectivement par la formulation des accusations, qui
les a conduit subjectivent à être en perspective l'état de gueule de bois de E. avec un
potentiel malaise dû à un viol.
A ce stade, notre AG de syndicat s'interroge également sur le pourquoi, si le changement d'attitude
présentée par E. devant de nombreu·ses témoins, formé·es aux questions d'antipatriarcat, pendant le
séjour pouvait s'apparenter à celui d'une femme victime de viol, aucune démarche de soutien ou
d'écoute n'ait été mise en œuvre à ce moment-là.
Un autre élément troublant du comportement de E. nous laisse également penser que le
comportement de E. pendant le séjour ne relevait pas de celui d'une victime potentielle. Cette
élément, apporté par Fouad dès ses premières auditions relate le fait que E. aurait déclaré
publiquement pendant une soirée au Roucous (postérieure à la nuit des faits présumés) qu’elle
venait d'avoir des relations sexuelles agréables avec un homme "portant un chapeau".
Sur ce point nous précisons que les témoins que nous avons auditionnés, attestent de la véracité de
ce fait, qui ne nous semble dès lors pas abondé dans le sens d'un changement de comportement de
E. durant le séjour en raison d'une agression qu'elle aurait subi.
En dernier lieu, l'accusé a mentionné pour sa défense le fait que ces relations avec E. en fin de
séjour ne semblait pas être celle d'une femme à l'égard de son violeur en expliquant pour sa défense
que E. lui avait présenté, après les faits plusieurs de ses connaissances féminines (2 amies lors de la
dernière soirée au Roucous, sa sœur sur le trajet du retour à Nancy). Ces éléments de défense sont
eux aussi accrédités par les déclarations des témoins que nous avons auditionnés.Ainsi au regard des éléments dont nous disposons, notre syndicat a eu tendance à considérer cette
hypothèse du changement de comportement de E. pendant le séjour, retenue à charge dans le
rapport de la commission non mixte d'AL parce qu'elle pourrait révéler la posture d'une femme
agressée vis à vis de son agresseur, n'est pas accréditée par les éléments dont nous disposons, au
contraire.
d) Sur la consommation de cocaïne et de médicaments
Dans le rapport de la commission non mixte d'AL le fait que Fouad ait proposé à E. de consommer
de la cocaïne, ainsi que le fait que Fouad était au courant que E. ; avait effectivement consommé
cette cocaïne puis pris des médicaments, est présenté comme un éléments à charge pour l'accusé,
dans la mesure où cela lui aurait confirmé la vulnérabilité de la femme qu'il aurait agressé, mais
aussi confédéré une attitude plus agressive qu'à la normale.
Cette hypothèse n'est pas hallucinante et notre syndicat la comprend : en cas de culpabilité de Fouad
elle serait tout à fait cohérente. Néanmoins, il s'agit là ni plus ni moins d'une interprétation possible,
et d'autres lecture de cette consommation de drogue sont tout aussi possibles.
En effet, les conséquences que peuvent avoir les produits psychotropes sur les individus sont
diverses et variées, et généralement associées à la psychologie de chacun·e. Si certain·es peuvent
devenir physiquement vulnérables dans le cadre de consommation, d'autres peuvent à contrario
retrouver un regain de lucidité et un degré de réveil accru (effet d'ailleurs très connu de la cocaïne),
d'autres encore peuvent, notamment en cas de mélange de diverses substances (en l’occurrence
alcool + cocaïne + médicaments) être victime d'une altération de leur degré de conscience. Laquelle
pourrait alors potentiellement conduire l'usager·e de substance à formuler une description de son
vécu qui ne correspond pas à la réalité.
Encore une fois, les cas de figure que nous évoquons ci-dessus sont des possibilités éventuelles, et
établir la réalité des conséquences qu’on eut de telles consommations sur E. et sur Fouad relèverait
sans doute d’un·e médecin psychiatre, et encore... Notre syndicat n'aura donc pas la prétention de
retenir qu'une seule hypothèse à charge, mais considère ici que ces consommations des drogues
peuvent être, à loisir, interprétée ou bien à charge ou bien à décharge...
Rappelons néanmoins, que les différents témoignages qui évoquent l'attitude de E. ; lorsqu'elle est
sous l'emprise de drogues ne semble pas aller dans le sens de l'interprétation retenue par la
commission non mixte d'AL.
Finalement concernant le mensonge de Fouad lors de son audition par la commission non mixte
d'AL, lorsqu'il a déclaré ne pas savoir si E. avait ou non consommé des drogues, notre AG a
considéré qu'il ne s'agissait pas en soi d'un élément à charge, dans la mesure où, avant même la
formulation de l'accusation, celui-ci nous avait fait état des faits qu'il avait lui-même apporté et
proposé de la cocaïne à E. au Roucous. Son explication concernant le mensonge formulé à AL, et
qui invoque le respect des pratiques des adhérent·es d'AL devant une de leurs instances fédérales,
nous semble plausible : elle explique pourquoi Fouad n'a pas eu de problème à le dire au sein de la
CNT.III)
Sur le contexte de l'accusation dont a été saisi notre syndicat
1. Indépendance de notre syndicat vis à vis de la procédure d'AL
Pour de nombreuses raisons, notre syndicat a tenu à ce que sa procédure dans le cadre de
l'accusation de viol contre l'un de ses adhérents soit celle qu'il a lui-même choisie, et ne lui soit pas
dictée par une organisation politique extérieure, en l'occurrence Alternative Libertaire. Aussi, bien
que nous ayons subis de nombreuses injonctions sur la façon dont nous devions procéder,
notamment en interne de la CNT, nous avons assumé et mener une procédure différente de celle
d'AL.
Pour autant, comme nous l'avons expliqué dans notre première partie méthodologique, notre seule
source d'information pour obtenir des éléments à charge contre Fouad, a été AL, via le rapport de sa
commission non mixte, constituée pour instruire le dossier et recueillir des témoignages.
Les éléments à charge que nous avons finalement analysé au sein de notre AG l'ont donc été suite à
un premier filtrage, puisque le rapport de la commission non mixte d'AL est bien un rapport, et non
un compte rendu ni un recueil de témoignages que nous n'avons pu obtenir en raison du refus d'AL.
Question s'est donc posée à notre syndicat de savoir si nous pouvions faire confiance en le travail de
la commission non mixte d'AL. Nous nous sommes donc penché·es sur les statuts d'AL qui
définissent cette procédure en leur sein, ainsi que sur le document de 4 pages (transmis au Bureau
Confédéral), interne à AL, et qui notifie à Fouad le début d'une procédure, tout en en décrivant les
formes et les règles.
Au regard de cette analyse, nous avons pu constater, avant même de disposer du rapport de la
commission non mixte que celui-ci ne pourrait qu'être un document à charge. En effet, la procédure
même d'Al en cas d'accusation d'agression sexuelle :
–
–
–
–
Partant du principe que la parole de la victime ne peut en aucun cas être remise en cause dès
lors qu'elle formule une accusation d'agression sexuelle.
Ne laisse aucune place à la défense de l'accusé, qui n'a pas le droit d'être assisté lorsqu'il est
auditionné, qui est auditionné sans qu'on lui présente les faits qu'on lui reproche, dont les
éléments de défense sont considérés irrecevables par la procédure dès lors qu'ils remettent
en question la parole de la victime supposée ou son intégrité.
Établit dès la qualification des faits que l'accusé est coupable remettant ainsi toute notion de
présomption d'innocence, tel qu'en atteste le document de 4 pages qui définit cette procédure
et présente les faits en ne laissant aucune place au doute et sans le moindre conditionnel.
Fait la liste de toutes les possibilités de défense existant pour un accusé pour expliquer, a
priori du propos de la défense, que celle-ci démontreront quoi qu'il en soit sa culpabilité
présumée.
Aussi, nous n'avons pas été surpris·e de la teneur du rapport de la commission non mixte,
totalement orienté à charge, lorsque nous l'avons reçu. Nous tenons ici à détailler plus amplement
les éléments nous permettant de considérer que ce rapport est biaisé et orienté.
-
L'utilisation des témoignages par la commission non mixte dans son rapport
Il ne nous semble pas nécessaire de nous étendre outre mesure sur les déclarations faites par Y., A.
et R. quant aux pressions, et les injonctions qu'ils·elles ont subi lors de l'écriture de leurstémoignages. Nous l'avons fait précédemment. Mais cet état de fait nous a conduit·es naturellement
à remettre en question la crédibilité des témoignages partiellement cités, et encore une fois
anonymes, présents dans le rapport de la commission non mixte.
Au-delà de la contrainte présumée sous laquelle ceux-ci ont été transmis à la commission non mixte
d'AL, il est éloquent de constater que tous les témoignages cités dans ce rapport sont des
témoignages à charge contre l'accusé. En effet, nous avons pu le vérifier grâce aux déclarations de
Y., A. et R., en ce qui concerne leurs témoignages : les éléments à décharge de leurs déclarations,
qui ont pourtant bien été transmis à la commission non mixte d'AL, ne figurent pas dans le rapport
de la commission. Qu'en est-il des autres témoins ? (Dont nous n'avons d'ailleurs aucun moyen de
vérifier l'existence.) Mais l'attitude de la commission non mixte concernant les 3 témoins
identifié·es nous laissent penser que les autres témoignages ont eux aussi été tronqués, voir modifié,
de façon à présenter comme à charge pour l'accusé.
-
La question de la défense dans le rapport de la commission
Comme en atteste de façon criante le compte rendu fait par l'une de nos adhérente de l'audition de
Fouad par la commission non mixte d'AL, et comme le prévoyait d'ailleurs les statuts d'AL en cas
de procédure de viol, la défense de Fouad n'a pas été prise en compte dans cette procédure.
Tout d'abord, lors de son audition - à laquelle il a pu se rendre accompagné mais lors de laquelle son
accompagnatrice ne pouvait pas s'exprimer - aucun fait ne lui ont été présentés. Dès lors nous ne
voyons pas comment l'accusé aurait pu bénéficier de la moindre défense, ni même la sienne.
Comment se défendre de quelque chose si l'on ne sait pas ce que l'on nous reproche ? Dans
l'hypothèse ou Fouad n'aurait pas commis ce viol, il ne pourrait absolument pas démontrer quelques
innocences que ce soit, ni même jouir, a minima, de sa propre parole. Nous sommes ici très loin de
la notion de justice équitable que nous décrivions en introduction.
Ensuite, les éléments de défense qu'il a pu formuler n'ont pas été rapportés dans le rapport de la
commission, tel que nous pouvons le constater de façon évidente à sa lecture. C'est par exemple le
cas concernant la défense formulée par Fouad lui-même lorsqu'il a expliqué à la commission non
mixte qu'il était lui-même désireux que E. dépose plainte à son encontre.
D'ailleurs, à aucun moment de sa procédure AL n'a semblé chercher à confronter certaines
affirmations de Fouad aux déclarations des témoins afin de chercher à savoir si celles-ci étaient
crédibles ou non.
Finalement, l'ensemble des éléments de défenses qui ont eu la chance d'être rapportés dans le
rapport de la commission non mixte, l'ont été afin de démontrer que ceux-ci était finalement des
preuves de la culpabilité de l'accusé selon un schéma pré établi. Parce que l'accusé entre dans le
cadre de ce schéma alors il serait de fait coupable du viol dont on l'accuse. Encore une fois ce
raisonnement irrationnelle, ou raisonnement par l'absurde ne correspond en rien avec la vision
équitable de la justice que nous défendons. Ce raisonnement, qui dicte le rapport de la commission
non mixte d'AL ne laisse ainsi à l'accusé aucun moyen de défense.
Si bien, que même s'il était innocent dans la réalité des faits, il ne disposerait d'aucun outil lui
permettant de la démontrer. Dès lors nous ne comprenons pas bien pourquoi il fallait faire toute
cette longue procédure de fond, la définition même de cette procédure condamnant d'ores et déjà
l'accusé, quoi qu'il arrive.
-
La considération de la moralité de la victime supposée et de l'accuséAfin de se défendre, Fouad a transmis à la commission non mixte d'AL de nombreuses attestations
de moralité, toutes rédigées par des femmes qui soit ont eu des relations sexuelles avec Fouad, sont
l'ont éconduit lorsque celui-ci avait pu les aborder. Celles-ci sont aussi considérées par le rapport de
la commission non mixte comme non recevable, sous prétexte qu'elles ne permettent pas d'établir de
vérité sur les faits. Nous sommes d'ailleurs en accord avec ce principe : le sentiment humain que
peut dégager un personne n'atteste en rien ou non des actes qu'il commet, comme le dit AL dans son
rapport, "ce n'est pas parce qu'un homme n'a jamais agressé une femme, qu'il ne peut pas le
faire".
Néanmoins, lorsque des éléments de la personnalité de l'accusé qui ont été décrits par des témoins
anonymes ont permis de dresser un portrait moral à charge pour Fouad, alors ceux-ci ont bénéficié
d'un crédit sans faille et ont été, eux, retranscrits dans le rapport de la commission.
De la même manière, Fouad a apporté pour sa défense, lors de son audition, des éléments sur la
personnalité de la victime. Éléments d'ailleurs corroborés par les témoignages qu'ont reçu la
commission non mixte. Éléments qui peuvent être potentiellement considérés comme à décharge
car remettant en cause le crédit de la victime supposée. Ces élément ont encore une fois soit été
dénigrés dans le rapport de la commission non mixte en expliquant qui si la défense les mobilisait
alors c'était de fait une preuve de culpabilité, soit retournés à charge contre l'accusé, comme c'est
notamment le cas en ce qui concerne la consommation de drogues tel que nous décrivions plus haut.
En résumé, nous n'avons pu que constater que le rapport de la commission non mixte d'Alternative
Libertaire constitué uniquement un rapport à charge, qui ne laisse aucune place ni à la parole de la
défense, ni aux éléments à décharge pour l'accusé.
Pour exemple, nous pouvons citer ce courrier de Fouad, adressé à AL après la publication du
rapport de la commission pour s'étonner que ses éléments de défense ne soient pas cités, courrier
qui n'a pas été transmis aux adhérents d'AL avant leur vote :
Fouad : "A la question posée par Clémence sur le pourquoi demander des attestations de moralité
à des camarades et amies femmes, ma réponse a été sans équivoque. Pris dans une accusation
basée uniquement sur la parole de la plaignante contre la mienne, et au vue du point précédemment
cité, il ne me restait que ce moyen pour me défendre afin de donner consistance à ma parole qui
depuis le début de cette affaire est systématiquement soit occultée, soit dénigrée. Pour preuve, ce
point n'apparaît pas non plus dans le rapport. Pourquoi ?"
Ainsi, lorsque que les adhérent·es d'AL ont voté fin janvier sur l'exclusion de l'accusé ceux·celles-ci
ne disposaient en aucune façon ni du propos de la défense, ni des éléments à décharge, et n'ont donc
pris·es une décision que sur le seul aspect à charge de cette affaire, sans avoir jamais connaissance
d'éléments pourtant connus de la commission non mixte et qui semble déterminants pour
comprendre la réalité des faits. Le rapport à charge étant, de par sa définition même a priori, et de
par la présentation orienté qui est faite de l'affaire, un rapport accablant pour l'accusé qui a, sans la
moindre surprise pour notre syndicat, conduit à un vote Stalinien en faveur de son exclusion.
En conclusion, notre syndicat a également été particulièrement surpris par l'attitude d'AL qui a
consisté à procéder à des injonctions publiques à l'égard de la CNT quant à l'exclusion de son
adhérent. En effet, la CNT, via sont BC, avait bien attesté à AL que notre syndicat était saisit de
cette affaire, a d'ailleurs fait office d'interface entre notre syndicat et AL sur le plan national pour
que nous puissions disposer d'éléments concrets d'accusation. Nous ne comprenons donc pas
pourquoi AL, qui sait pertinemment où en est la CNT sur cette affaire a fait le choix, malgré les
relations cordiales qu'ils·elles entrainaient avec le BC, de mettre sous pression la CNT sous les yeux
du grand public. Aussi nous ne pouvons que considérer que cette démarche comme une volonté
politique d'Alternative Libertaire de déstabiliser la CNT. Les raisons d'AL lui sont propres, nous neles connaissons évidemment pas, bien que nous puissions considérer certaines hypothèses, mais
quoi qu'il en soit, l'attitude politique d'AL vis à vis de la CNT a conduit notre syndicat à ne pas
considérer les conclusions de cette organisation comme argent comptant. Dernière raison pour
laquelle nous avons préféré mener notre propre procédure, au regard des principes militants décrits
dans notre partie méthodologie, procédure qui nous a conduit en remettre en cause le crédit de
l'enquête à charge menée par AL.
2) Chronologie de l'accusation
Notre syndicat, depuis le début de cette affaire et la formulation des accusations lors du Congrès
Confédéral de la CNT (le 13 novembre) par des membres de la commission antisexiste de la CNT, a
essayé de comprendre pourquoi cette accusation nous était parvenue par ce biais. Après plusieurs
mois d'investigation, nous avons pu faire la lumière sur les processus qui ont conduit les
protagonistes de cette accusation à ne pas employer les voix structurelles qu’il y a chez AL, comme
à la CNT, nous semblait être les plus appropriées pour traiter sereinement l'accusation de l'un de nos
adhérents.
Nous ferons donc ici le résumé chronologique des démarches qui ont été entreprises par les un·es et
les autres car il nous semble qu'elles participent à éclaircir notre analyse.
Tout d'abord, la première formulation de l'accusation par la victime supposée elle-même, est
intervenue sur le trajet retour du Roucous. C'est sur une aire d'autoroute que E. a fait le premier récit
des faits présumés à Y. Tous les témoignages factuels concordent pour dire que cette confidence
faite à Y. est intervenue quelques temps après une altercation violente qu'elle a eue avec Fouad.
Suite à cette première annonce, les membres du CAL Moselle ont proposé à E., adhérente du CAL
de Meurthe et Moselle, de traiter localement cette problématique. Réticente à cette idée dans un
premier temps, en raison de la peur qu'elle a évoqué d'être victime de représailles, E. s'est alors
confiée à des militant·es de la CNT à Nancy. Parmi ceux·celles-ci l'on trouve J., adhérent de
l'interco 54, et qui entretient une relation amicale avec E. Selon les déclarations de E. à Y., c'est bien
J., qui l'a convaincue que, plutôt que de traiter cela au sein du CAL Moselle d'AL, il valait mieux
solliciter la CNT, sur le plan national, et cela via la commission antisexiste confédérale.
A ce stade nous nous interrogeons naturellement sur le pourquoi une telle démarche de J. qui a
préféré adresser la victime présumée à une commission de la CNT plutôt qu'à Alternative Libertaire,
qui dispose également d'une commission antipatriarcat, et alors que l'affaire porte sur deux
adhérent·es d'AL et le camping d'AL. Nous nous interrogeons également sur le pourquoi, si tant est
que J. se soit trompé dans son analyse, cette information dont disposait visiblement plusieurs
adhérent·es de la CNT a mis 4 mois pour parvenir jusqu'à la commission antisexiste de la CNT. À
moins que celle-ci n'ait disposé de cette information bien plus tôt, auquel cas notre interrogation
porte sur le pourquoi la commission aurait attendu 4 mois avant de la porter à la connaissance d'une
instance confédérale, en l'occurrence le Congrès, lors d’une démarche orale qui a fait sensation et a
conduit à la divulgation immédiate de l'identité de E..
Le rôle de J. nous interroge, car il ne semble pas du tout cohérent avec une posture de protection de
la victime potentielle et de résolution de sa problématique. Aussi, notre syndicat n'a pas pu faire
autrement que de mettre cette démarche en perspective avec le vécu relativement récent de J., qui il
y a quelques années s'est retrouvé accusé de violences conjugales par son ex-compagne, M., elle
aussi adhérente de l'Interco 54. Dans ce contexte E., amie du couple, avait dans un premier temps
défendu la victime supposée, avant de se rétracter pour prendre la défense de J., considérant sans
doute que la parole de M. n'était pas crédible. La décision de l'interco 54, saisi de cette accusationd'une adhérente à l'encontre d'un adhérent avait alors été de considérer que la parole de M. était
mensongère et avait donc, faute d'éléments probants, décidé de blanchir J., sur la base d'une
réflexion radicalement opposée à celle que ces mêmes camarades défendent aujourd'hui au sujet de
l'accusation faite par E. à l'encontre de Fouad.
Nous rassurons J., le but de ce paragraphe n'est pas de jeter un doute sur son innocence (dont nous
sommes parfaitement convaincu·es) dans le cadre de cette affaire de violences conjugales
présumées, mais bien de comprendre ce qui l'a conduit à adopter aujourd'hui une démarche si
contradictoire avec ce que lui-même et son syndicat ont vécu il y a quelques temps. Contradictoire
parce que l'a parole de la victime présumée n'avait pas fait force de preuve, contradictoire parce que
le syndicat avait préféré gérer cette accusation en son sein plutôt que d'adresser M. à une
commission antisexiste qui aurait peut-être été plus à même de donner de la considération a la
femme potentiellement agressée.
Aussi, notre syndicat n'a pu que s'interroger sur le fait que le rôle de J. puisse être une contrepartie
offerte à E. pour le soutien qu'elle lui a apporté il y a quelques années lorsqu'il a été accusé.
Notre syndicat s'interroge aussi sur le rôle joué par le syndicat intero 54 dans le fait d'adresser E. à
la commission antisexiste de la CNT, plutôt qu'aux instances d'AL en premier lieu. En effet, il est de
notoriété public au sein de la CNT en Lorraine, que l'un des adhérents actif de l'interco 54 entretient
une relation intime avec l'une des membres de la commission antisexiste, et du syndicat SSCT RP,
ayant participé à la rencontre avec E. le 10 novembre et à la communication de l'accusation au
Congrès. Encore une fois, nous nous interrogeons sur le pourquoi, alors que tout ce petit monde
était au courant depuis bien longtemps de l'accusation de E. à l'égard de Fouad, ils·elles n'aient rien
dit jusqu'à la tenue du Congrès Confédéral et le coup d'éclat intervenu le dimanche 13 novembre.
L'une des hypothèses étudiées par notre syndicat nous laisse penser que cette adhérente de SSCT RP
ait pu utiliser ses relations au sein de l'interco 54, et donc la connaissance qu'elle a eu de
l'accusation de E. envers Fouad, pour régler, au niveau Confédéral, des comptes avec son syndicat
d'appartenance et son Union Régionale. Pour quelles raisons aurait-elle des comptes à régler ? Il
n'appartient pas à notre syndicat de s'immiscer dans les relations internes à l'URP, mais nous savons
que les motifs de rancœur de cette personne sont nombreux et remontent à plusieurs mois, bien
avant le Congrès Confédéral de novembre, bien avant le contexte d'accusation actuel.
Reprenons donc la chronologie de l'accusation...
Suite à ces démarches conjointes de J. et consorts, la commission antisexiste de la CNT a organisé
une rencontre avec E., le 10 novembre, lors de laquelle elle s'est confiée et a formulé son
accusation, qui a été relayée oralement par des membres de la commission devant le Congrès
Confédéral, lors du dernier jour, le dimanche 13 novembre.
Notre AG de syndicat s'est penchée à ce stade sur un autre élément troublant de la chronologie de ce
Congrès. En effet, nous rappelons que lors de ce Congrès de la CNT, il n'y avait aucun·e candidat·e
pour le mandat de secrétaire confédéral. Le samedi, lorsque les syndicats sont passés sur le
mandatement du SC, le Congrès a décidé de repousser ce mandatement au lendemain, en espérant
qu'un·e candidat·e se déclare. Le dimanche matin, toujours pas de candidat·e. Au moment du
mandatement, notre adhérent, Aurélien, secrétaire confédéral sortant, s'est donc proposé pour
assurer un intérim jusqu'au CCN de mai. Étrangement, celui-ci a été mandaté avec la quasi-
unanimité des suffrages et aucune voix contre, sans qu'aucun syndicat ne prenne la parole sur sa
proposition de prolongation de son mandat. Pourtant Aurélien et Fouad sont adhérents du même
syndicat, et cet état de fait a servi d'appui à plusieurs syndicats, ainsi qu'à la commission antisexiste
pour attaquer le SC en l'accusant de partialité et de complaisance à l'égard de l'accusé (bien que cela
n'a jamais été le cas par ailleurs). Nous nous interrogeons donc ici sur le pourquoi, ces syndicats siprompts à accuser le SC de conflit d'intérêt au lendemain du Congrès se soient abstenus de relayer
l'accusation de E. ; à l'encontre de Fouad avant le mandatement du SC. Ces syndicats, qui
disposaient donc des éléments d'accusation depuis le jeudi 10 novembre, ont donc visiblement
sciemment attendu qu'Aurélien soit mandaté pour (et ne se sont d'ailleurs pas opposés à son
mandatement dans leur vote) avant de l'accuser d'un conflit d'intérêt dans lequel ils l'ont eux-mêmes
placés.
Pendant le Congrès toujours, alors même que tous les syndicats qui s’étaient fait le relai de
l'accusation pendant la séance plénière avaient mis en garde la Confédération et notre syndicat sur
la nécessité de protéger la victime supposée et de préserver son anonymat, certaines de leurs
délégations avaient déjà énoncé l'identité de la victime à une ex adhérente de la CNT en Région
Parisienne. Ces individus qui tenaient donc clairement double discours durant le weekend du
Congrès, ignoraient sans doute que la camarade à qui ils·elles ont livré l'identité de E. était par
ailleurs une amie intime de Fouad, qui a donc communiqué dans les jours qui ont suivi l'identité de
E. à son agresseur potentiel.
Continuons la chronologie de l'accusation dont la CNT a désormais connaissance. Comme nous
l'avons à plusieurs reprises expliqué depuis le Congrès de novembre notre syndicat a dès lors tout
fait pour lancer une procédure concernant l'adhésion de Fouad. Pour cela, nous avons dès le
lendemain du Congrès cherché à recueillir les éléments à charge contre notre adhérent. Comme
vous le savez aussi nous avons eu toutes les peines à y parvenir. La commission antisexiste de la
CNT a d'abord refusé de les transmettre par écrit, nous a ensuite fait nous déplacer à Paris le 17
décembre pour une rencontre lors de laquelle elle a à nouveau refusé de nous transmettre les
éléments d'accusation. En parallèle, le BC a pris contact avec AL, qui lui a expliqué qu'il ne pouvait
rien transmettre comme éléments avant que ne soit rendue leur décision. Si bien que ces éléments
concrets d'accusation ne nous sont parvenus qu'à partir du moment où AL les a transmis à l'accusé,
qui nous les a lui-même adressés.
Après avoir constaté que les témoignages à charge dans le rapport de la commission non mixte d'AL
étaient tronqués et partiellement cités, nous avons demandé à AL de nous transmettre la version
complète de ces derniers. Nous avons alors essuyé un nouveau refus, AL :
"Parce que certains témoins nous ont clairement signifié qu’ils ne souhaitaient pas une telle
transmission, notamment en raison des pressions que Fouad a pu exercer sur elles et eux"
Nous nous demandons néanmoins comment Fouad aurait-il pu exercer des pressions puisque ces
témoins sont justement anonymes.
Au regard de cette chronologie, nous interrogeons sérieusement sur la volonté qui a été celle des
différent·es protagonistes de l’accusation (AL, sa commission non mixte, la commission antisexiste
de la CNT, les syndicats qui depuis novembre semblent des plus investis et des mieux informés) de
voir la lumière être faite de façon sérieuse sur cette accusation. Bien entendu l'ensemble de ces
éléments troublants n'innocentent pas notre adhérent accusé, pas plus qu'il le culpabilise d'ailleurs,
mais il nous pose question car il nous laisse penser que ces structures qui défendent aujourd'hui
mordicus l'exclusion de Fouad sans chercher à éclaircir la réalité des faits, tentent de nous mettre le
couteau sous la gorge par tous les moyens (internes et externes!) pour que nous accédions à leur
injonction, et ont, depuis des mois, plus cherchés à poursuivre un objectif intéressé qu'à démontrer
sur un plan objectif une éventuelle culpabilité de notre adhérent, encore moins à protéger une
potentielle victime de viol.
Au regard de cette chronologie, notre syndicat a donc eu le sentiment - et cela de façon
indépendante de la culpabilité ou de l'innocence de notre adhérent - que cette accusation a surtout
permis d'alimenter de nombreux règlement de comptes inter-personnels ainsi qu'à servir certaines
postures idéologiques. Ce contexte a de fait influé sur notre réflexion et ne peut pas être ignoré par
lors de nos prises de décisions définitives.IV)Conclusions de notre syndicat
1. Sur le fond de l'accusation et l'exclusion éventuelle de notre adhérent pour viol
Considérant tout d'abord que des éléments du récit de la victime supposée concernant le déroulé fait
semblent être contredit par les témoignages factuels que nous avons pu recueillir,
Considérant les propos de la victime supposée qui explique que son souvenir des faits supposés
repose sur les déclarations que l'accusé lui à lui-même formulé,
Considérant que les éléments à charge présentés dans le rapport de la commission non mixte d'AL
et qui concernent un changement de comportement possible durant le séjour au Roucous pouvant
révéler le fait qu'elle ait subi un viol, ne semblent pas concordants avec les différents éléments et
témoignages factuels dont nous avons pu disposer,
Considérant que les éléments à charge présentés dans le rapport de la commission non mixte d'AL
et qui décrivent une attitude déplacée de la part de l'accusé durant toute la semaine au Roucous, sont
nuancés par les témoins factuels qui décrivent une relation difficile dans les deux sens, laquelle n'a
pas empêché les deux protagonistes de partager de nombreux moments festifs,
Considérant que les consommations ne drogues multiples, tout au long de la semaine, par la victime
supposée comme par l'accusé sont très peu propices à crédibiliser les souvenirs et les discours des
un·es des autres, et peuvent conduire à des interprétations extrêmement variées des comportements
de chacun·e,
Considérant que les éléments de défense apportés par l'accusé, avant sa prise de connaissance des
faits reprochés, et après avoir été confrontés aux éléments d'accusation sont restés concordants et
sont accrédités par les témoins factuels que nous avons auditionnés,
Considérant que l'accusé lui-même a fait preuve de la transparence la plus totale tout au long de
cette procédure, en fournissant notamment à son syndicat d'appartenance, l'ensemble des éléments à
charge contre lui-même et qu'il a pu obtenir,
Considérant que l'accusé, dès les premières formulations de l'accusation par la victime s'est déclaré
devant toutes les organisations auxquelles il adhère qu'il était personnellement favorable au dépôt
de plainte de la victime supposée,
Considérant que la victime supposée disposait de potentielles raisons de formuler une fausse
accusation,
Considérant le rôle joué par Alternative Libertaire, qui a visiblement préféré attaquer publiquement
la CNT plutôt que de lui fournir des éléments objectifs qui lui auraient permis d'analyser
objectivement l'accusation,
Considérant que les pressions équivoques qui ont été faites par la commission non mixte d'AL sur
les témoins que nous avons auditionnés jettent un doute raisonnable sur la crédibilité des
témoignages tronqués et utilisés à charge dans le rapport de la commission,
Considérant que la chronologie de cette accusation révèle finalement des liens multiples entre la
victime supposée et celles et ceux qui ont relayé·es cette accusation et tenté·es par de multiplesmoyens de nous faire prendre une décision sous pression ; liens qui nous laissent penser que la
fragilité de cette femme a potentiellement pu être exploitée par des intérêts politiques,
Le syndicat SSCT Lorraine a estimé que :
– Une analyse objective de cette affaire, suite à une procédure réalisé avec équité à charge
comme à décharge, ne permet pas de tirer de conclusions formelles, ni sur la véracité de
l'accusation portée, ni sur l'innocence de notre adhérent dans cette affaire.
– Que les seuls éléments à charge qui ont été recueillis à l'encontre de Fouad et qui demeurent
cohérent à l'épreuve des faits sont trop peu nombreux et demeurent trop assujettis au doute
pour justifier d'une exclusion de notre adhérent.
2. Appui au dépôt de plainte de la victime supposée
Notre syndicat, après avoir mené cette procédure s'est déclaré incompétent pour établir quelconque
vérité. Malheureusement, au regard des éléments dont nous disposions, et en accord avec des
principes de justice qui sont pour nous inaliénables, nous considérons que notre syndicat n'a pu
rendre d'autre décision et qu'il ne peut de fait disposer de moyens d'investigations plus efficaces que
ceux dont il a déjà usés.
Aussi nous avons bien conscience que notre décision concernant la non exclusion de Fouad
constituerait une injustice terrible pour une femme victime de viol si celui-ci était coupable. Mais
notre syndicat n'a pas aujourd'hui les moyens de se substituer à un travail de police judiciaire ou
d'un magistrat. Pour ces raisons, notre syndicat, tel qu'il l'a déjà dit, se déclare favorable au fait que
la victime supposée dépose une plainte à l'encontre de Fouad.
Bien conscients des nombreuses difficultés qui sont celles rencontrées par une femme lorsqu'elle
porte plainte, notre syndicat pense qu'il est du devoir de la CNT, en tant qu'organisation féministe et
antisexiste que de proposer expressément à E. de bénéficier de son accompagnement dans le cadre
d'une telle démarche, si elle estime que celle-ci peut lui permettre de lui rendre justice.
Nous rappelons que si E. faisait le choix de déposer plainte à l'encontre de Fouad, notre syndicat
tiendra à disposition de la justice et de E. l'ensemble des éléments dont il dispose.
Nous rappelons à nouveau que Fouad s'est déclaré lui-même favorable à ce dépôt de plainte et a
déclaré se tenir à disposition de la justice dans ce cadre éventuel.
3. Perspectives en interne de la CNT
Tout d'abord, il est évident que cette accusation de viol à l'encontre de l'un de nos adhérents à
suscité l'éclosion de vives polémiques au sein de la Confédération, autour des notions
d'antipatriarcat, de féminisme, d'antisexisme, de justice. Ces polémiques semblent d'ailleurs, pour
certaines d'entre elles avoir été suscitées par l'influence d'une organisation politique, en l'occurrence
Alternative Libertaire, en lien avec certaines structures et adhérent·es de la CNT. Aussi, un débat
serein autour de ces différentes notions nous apparait aujourd'hui comme une nécessité absolue pour
préserver l'unité de la Confédération.
Ensuite, tel que l'on proposé différents syndicats, ainsi que la commission antisexiste de la CNT, il
semble nécessaire que la CNT puisse se doter d'un protocole claire et réfléchi pour traiter de lafaçon la plus juste possible ce type d'accusation en interne. S'il ne fait aucun doute que notre
syndicat voit sans doute un tel protocole de manière bien différente que les propositions qui ont déjà
été formulées par certains syndicats, nous rejoignons volontiers l'idée d'une discussion collective à
ce sujet.
Par ailleurs, les pratiques de certaines structures de la CNT, telles que la commission antisexiste de
la CNT, les agissements de syndicats qui remettent en cause l'autonomie d'autres syndicats ou du
Congrès, les agissements de certains syndicats et adhérent·es qui n'ont pas hésité à diffamer
publiquement notre adhérent avant même quelque conclusion de notre part sur le fond de cette
affaire, posent des questions fondamentales à notre fonctionnement Confédéral.
Finalement, nous ne doutons pas que plusieurs syndicats de la CNT, suite au rendu de notre
décision, décideront de proposer l'exclusion de notre syndicat de la Confédération. Il semble
d'ailleurs que cela démange certain·es depuis de nombreuses semaines. Bien que nous le regrettions,
il apparait inévitable que notre syndicat joue prochainement sur sa tête les problématiques
politiques qui déchirent les organisations libertaires, mais notre syndicat est prêt à assumer cet état
de fait. En effet, non seulement nous refusons de nous retrancher derrière une potentielle
impossibilité pour ces syndicats qui rêvent de proposer notre exclusion de la CNT à pouvoir le faire
en raison de leur éventuelle position minoritaire au sein de leur UR qui précédera le prochain CCN,
mais en plus nous refusons de vivre pendant encore 20 mois (c'est à dire jusqu'au prochain Congrès
Confédéral) les pressions et les injures que nous vivons depuis plus de 3 mois et qui entravent
toutes activités syndicales de notre part.
Pour toutes ces raisons, notre syndicat se déclare favorable à la tenue prochaine d'un Congrès
Confédéral extraordinaire, seul moyen pour que la CNT puisse trancher sereinement et
formellement les débats qui la sclérosent aujourd'hui ainsi que l'avenir de notre syndicat en son sein.
4. Relations avec Alternative Libertaires
Compte tenu des tentatives équivoque d'Alternative Libertaire de mettre la CNT sous la tutelle de
ses propres décisions, compte tenu des tentatives de cette organisation politique pour discréditer
notre Confédération sur le plan national, compte tenu des procédés odieux employés par AL dans le
cadre de l'instruction de cette affaire, compte tenu du choix qui a été fait par Alternative Libertaire
d’engager une communication publique laquelle relève de pratiques moyenâgeuses qui nous
horrifient, et cela quel que soit la réalité de la culpabilité ou non de Fouad, notre syndicat décide :
– De communiquer publiquement notre décision concernant Fouad et de dénoncer
publiquement les agissements d'Alternative Libertaire. Nous précisons que sans
communication publique d'AL, nous n'aurions sans doute pas pris·es cette décision. Mais
puisqu'AL a fait le choix de nous mettre devant le fait accompli et compte tenu des
conséquences dramatiques qu'ont eu leur injonction sur l'image de la CNT aux yeux du
grand public, nous n'avons pas d'autres choix.
– De rompre toutes relations inter organisations avec Alternative Libertaire. Si nous
respectons l'intégrité dont ont fait preuve les adhérent·es du CAL Moselle, nous regrettons
que, de fait, nous n'aurons plus de relation avec AL au niveau local, dans la mesure où ces
adhérent·es, affligé·es par les agissements de leur organisation, ont décidé de quitter AL.
– De faire des propositions concrètes à la Confédération, par le biais de motions qui seront
soumises au prochain Congrès extraordinaire, une évolution des relations entre la CNT et
AL sur le plan national et cela afin de protéger la confédération.
