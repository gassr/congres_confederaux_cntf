
.. _annexe_motions_etpic30_2012:

==================================================================================================
Annexe motions etpic30 2012
==================================================================================================

.. toctree::
   :maxdepth: 5

   preambule
   introduction_et_rappels
   droits_syndicaux/index
   experiences_permenentisme
   articuler/index
   activite_syndicale_et_militante
   motions/index
