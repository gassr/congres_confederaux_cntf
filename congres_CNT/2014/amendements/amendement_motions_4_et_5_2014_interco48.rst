
.. _amendement_motions_4_5_2014_interco48:

==================================================
Amendement aux motion n°4 et n°5 2014 (Interco48)
==================================================

.. seealso::

   - :ref:`motion_4_2014_sinr44`
   - :ref:`motion_5_2014_sinr44`
   - :ref:`interco48`




Argumentaire
=============

C'est trop souvent parce que les horaires de début et de fin de congrès ne
sont pas respectés que les syndicats finissent par quitter le congrès avant
qu'il ne se termine.

Amendement
==========

Les horaires prévus en début de congrès seront désormais respectés.
