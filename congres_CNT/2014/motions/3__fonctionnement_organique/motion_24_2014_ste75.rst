
.. index::
   pair: Motion 24 ; 2014

.. _motion_24_2014_ste75:

==========================================================
Motion n°24 : Création d'un comité site Internet (STE 75)
==========================================================


Argumentaire
=============

Le site internet de la CNT est un média au moins aussi important en
terme de propagande que le Combat Syndicaliste.

Motion
======

À ce titre, nous proposons qu'un comité site Internet se mette en
place comprenant 2 pôles:

- un pôle éditorial et graphique (éditorial, mise en avant d'article en
  fonction de l'actualité, recherche d'articles auprès des syndicats
  qui n'ont pas le réflexe internet...).
- un pôle purement informatique (mise en ligne, sécurité internet...).

Les syndicats auront toujours la possibilité d'envoyer par eux-mêmes
les articles qu'ils veulent.

Ce comité serait en lien avec le Combat Syndicaliste (par exemple en
publiant aussi en ligne des articles du CS), avec le secrétariat
Média, celui vidéo... pour valoriser leurs productions et leur
audience.

Contre motions
==============

.. seealso::

   - :ref:`contre_motion_24_2014_interpro31`
   - :ref:`contre_motion_24_2014_etpreci75`
   - :ref:`contre_motions_22_23_24_2014_sipmcs`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°24 2014

       :ref:`STE75 <ste75>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
