
.. _motion_5_2012:
.. _motion_5_2012_stea:

======================================================
Motion n°5 2012 : Auto-organisation du congrès, STEA
======================================================
  
.. seealso::

   - :ref:`stea`
   - :ref:`motions_stea_2012`



Argumentaire
============

Nous avons constaté à notre plus grand regret au Congrès confédéral de Saint-Étienne
que c'est l'ambiance survoltée, mais surtout nos erreurs en terme d'auto-organisation
et de respect dans la teneur des débats qui n'a pas permis de mener ce congrès
à bout, d'abandonner des motions, ou de précipiter le vote de certains mandats
avant que les syndicats ne quittent la rencontre confédérale.

En efet, un temps précieux a été perdu en répétitions et en paraphrases.

Nous estimons ainsi que les arguments ou les contre-arguments n'ont pas à être
ré-assénés, par diférents syndicats ou par le même syndicat prenant plusieurs
tours de parole, pour que ceux-ci aient plus de poids.

En cas de mandat semi-impératif, aux mandaté-e-s de peser le pour et le contre,
d'estimer les prises de positions et les idées afin de prendre une décision sur
le vote à tenir. Il ne nous semble pas non plus la peine de prendre la parole
lors de son tour si les arguments que l'on souhaitait défendre ont déjà été
exposés.

De plus, nous avons remarqué à plusieurs reprises que des interventions
débutaient par « je vais essayer de ne pas faire de répétitions » ou
« je vais essayer de ne pas dépasser trois minutes ».
À quoi bon perdre encore quelques secondes avec de telles introductions, qui la
plupart du temps ne servait qu'à justifier a priori une efective redite.

Enfin, il ne nous parait pas non très pertinent que pour exploiter au maximum
les trois minutes de prise de parole, certains syndicats fassent tourner le
micro à plusieurs de leurs mandaté-e-s, alors que l'argumentaire est
vraisemblablement le même.

Motion
======

Pour maintenir un climat apaisé et un débat cohérent, pour que les arguments et
contre-arguments tirent leur poids de leur idée-force et non pas de quels
syndicats ou combien les soutiennent, pour que le congrès statue sur des
motions et non pas sur des querelles intestines, au-delà des clivages qui
traversent la vie de notre confédération, nous devons faire tout particulièrement
attention à la manière dont nous organisons nos débats et nos
prises de parole.

L'autogestion étant l'un des principes de base de notre fonctionnement
syndical, il nous semble nécessaire de l'appliquer aussi à notre fonctionnement
interne et au congrès.

Aussi, il parait essentiel de préparer des doubles listes de tour de parole, de
limiter le nombre d'intervention du même syndicat à deux (hors présentation de
la motion, contre-motion ou amendement), de se conformer au temps imparti pour
les prises de parole (trois minutes voire moins si le débat s'éternise), de
fixer des heures limites à la tenue d'un débat sur une motion, et surtout
d'éviter toute répétition ou paraphrase dans les argumentaires.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°5           |            |          |            |                           |
| STEA                 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_5_2012_interpro31`

Contre-motions
==============

- :ref:`contre_motions_5_6_7_etpic30`
