

.. _motion_antifascisme_cnt_congres_lyon_1996:

================================
Motion "Antifascisme" Lyon, 1996
================================

Le Congrès propose l’ouverture d'un débat dans le :term:`BI` sur l’antifascisme.

Un tract destiné aux entreprises (notamment contre la tentative d'implantation
du Front National dans le monde du travail) devra être fait ainsi qu'une
affiche antifasciste (propositions à envoyer au secrétariat à la propagande :
Union départementale 95).

Le Congrès vote une motion de soutien aux camarades interpellés et inculpés
lors de l'attaque d'un meeting de De Villiers à Strasbourg.
