.. index::
   pair: Motion 3 ; 2021

.. _motion_3_2021_ptt_centre:

================================================================================
Motion **n°3** 2021 **Motion internationaliste** par **PTT Centre**
================================================================================

Autres motions PTT-Centre

    - :ref:`syndicats:motions_ptt_centre`


Argumentaire
==============

Notre Confédération a, parmi toutes ses tâches à développer, une activité
internationaliste complètement inscrite dans notre identité syndicale.

Jusqu’à ce jour il existe un :ref:`Secrétariat International <secretariat_international>`
dans lequel et par secteurs géographiques mondiaux des militants mandatés pour cela
contribuent à défricher ce qu’il est possible d’apporter à notre vision du monde
en nous fournissant des analyses, des articles, des moyens d’actions solidaires.

Il n’y a rien à redire à cette activité militante fondée sur les compétences,
les proximités de chacun vis à vis du secteur international concerné.

Par ailleurs et depuis la scission de notre Confédération avec le secteur
dit AIT et notre expulsion de l’Association Internationale des Travailleurs,
notre Confédération est toujours en recherche d’organisations étrangères
sœurs avec lesquelles nous rapprocher.

Par contre il nous semble qu’il manque un volet important et même crucial à
cette activité internationaliste de la CNT qui manque un peu de base, de corps,
de vécu et de chair.

Motion
=========

La Confédération par le biais de son :ref:`Secrétariat International <secretariat_international>`
aidera, à l’initiative des syndicats qui le souhaitent, à la création, le soutien
et l’activité d’initiatives de Jumelage entre d’une part des sections ou des
syndicats CNT et d’autre part des sections, des syndicats ou des associations
de travailleurs présentes dans d’autres pays et avec lesquelles nous pourrions
échanger et soutenir mutuellement les luttes en cours, les initiatives, les
expériences.

Le rôle de la Confédération et de son :ref:`Secrétariat International <secretariat_international>`
pourrait être d’aider les syndicats CNT, leurs fédérations, demandeurs d’un tel
jumelage:

- à trouver les possibilités de contacts locaux dans les zones géographiques
  qui intéressent ces syndicats ;
- à participer financièrement, dans la mesure du possible, à ces initiatives
  de jumelage.

Amendement
===========

- :ref:`amendement_motion_3_2021_interpro31`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°3 2021 PTT Centre  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
