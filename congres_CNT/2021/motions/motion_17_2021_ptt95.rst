.. index::
   pair: Motion 17 ; 2021

.. _motion_17_2021_ptt95:

================================================================================================================================================
Motion **n°17** 2021: **Prise de décisions en CCN** par **PTT 95**
================================================================================================================================================

Autres motions PTT95

    - :ref:`paris:motions_ptt95`


Argumentaire
==============

Depuis des décennies nous avons fonctionné avec des modalités de prises de
décisions en CCN fondées sur le vote des régions présentes.
Chaque région étant au CCN dispose d’une voix.
Or les régions recouvrent des réalités très diverses en nombre de syndicats.
Ce procédé peut ainsi aboutir à l’adoption d’une décision majoritaire sur le plan
des régions, ces régions pouvant en fait ne représenter qu’une minorité de
syndicats.
Ce constat met en avant une possible dérive bureaucratique, une minorité ayant
techniquement la possibilité d’imposer un choix à une majorité de syndicats.

De plus les syndicats qui ne sont pas constitués en région sont dépourvus de toute
forme d’expression, de fait ils sont exclus de la prise de décision.
Il importe donc, sur ce point, de prévenir de graves dysfonctionnements à venir.
Le pouvoir de décisions en CCN doit demeurer de la compétence exclusive des
syndicats. Pour ces raisons notre syndicat propose la motion suivante.

Motion
========

Lors des CCN les **décisions sont prises à l’unanimité des régions présentes**.

Au compte-rendu figurent les avis émis, à titre consultatif, par les syndicats
présents mais non constitués en région.


Amendements
===========

- :ref:`amendement_motion_17_2021_sest_lorraine`

Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°17 2021 PTT95      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
