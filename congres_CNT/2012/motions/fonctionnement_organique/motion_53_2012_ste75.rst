
.. _motion_53_2012:
.. _motion_53_2012_ste75:

===================================================================================
Motion n°53 2012 : Caisse de solidarité (simplification du reversement)  STE 75
===================================================================================
  
.. seealso::

   - :ref:`ste75`
   - :ref:`motions_ste75_2012`



Argumentaire
============

Il est important que la caisse de solidarité confédérale existe.
Il serait  important de la pérenniser et de la rendre plus transparente.

Aujourd'hui, il existe trois règles qui visent à l'abonder, ce qui amène une
légère incohérence.

Il y a un article statutaire et deux motions de règles organiques.

Article 22
-----------

.. seealso:: :ref:`article_22_caisse_solidarite`


Cette Caisse est alimentée par les timbres solidarité et la vente des cartes.

Deux timbres par an sont obligatoires. Chaque syndiquéE peut en prendre
facultativement autant qu'il lui plait. Le montant du timbre solidarité est
fixé par le Congrès. Les fonds sont inscrits au compte « Caisse de Solidarité ».

Recueil de résolution
---------------------

.. seealso::

   - :ref:`cotisation_caisse_solidarite_confédérale_agen_2006`


En complément de cet article, dans le recueil des résolutions se trouve une
motion qui indique : Les sommes collectées par la vente du timbre confédéral
de cotisation du mois de janvier sont affectées à la caisse de solidarité.

Titre V
--------

.. seealso::

   - :ref:`timbre_confederal`

Et en plus, au Titre V, une règle organique vient préciser la répartition
budgétaire de la cotisation confédérale : Timbre standard : 2,60 € avec 0,50 €
pour l’international, 0,50 € pour solidarité et 1,60 € pour la part
confédérale, pour le timbre précaire : 1,20 € avec 0,30 € pour l’international,
0,30 € pour solidarité et 0,60 € pour la part confédérale.

Donc, on arrive avec une règle de répartition où la caisse de solidarité est
alimentée avec deux timbres, plus le timbre de janvier et une partie des
reversements mensuels.

Outre la complexité de cette répartition, il semble que cela fasse une grosse
contribution qui ne soit pas utile.

Nous proposons donc d'abroger ces deux règles supplémentaires et de ne garder
que la règle statutaire. Bien évidemment, la prochaine trésorière confédérale
devra être vigilante sur le niveau de budget avec cette nouvelle règle et
alerter le Bureau confédéral et le CCN suivant en cas de soucis.



Motion
=======

Abrogation des motions sur les versements auprès de la caisse de solidarité.

Règle de répartition selon l'article 22 des statuts : deux timbres confédéraux
sont consacrés à la caisse de solidarité (ceux de janvier et juillet, début
de semestre).

Pour les autres timbres, la répartition se fait comme suit :

- Timbre standard : 2,60 € avec 1 € pour l’international
  et 1,60 € pour la  part confédérale,
- pour le timbre précaire : 1,20 € avec 0,60 € pour l’international
  et 0,60 € pour la part confédérale.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°53          |            |          |            |                           |
| STE75                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
