
=====
Grève
=====

Article 17
==========

Les syndicats fédérés s'engagent à avertir la Fédération, en cas de grève et à
communiquer un maximum d'informations sur la grève.

La Fédération s'engage à apporter son soutien, tant matériel que financier et
juridique, dans la mesure de ses possibilités.
