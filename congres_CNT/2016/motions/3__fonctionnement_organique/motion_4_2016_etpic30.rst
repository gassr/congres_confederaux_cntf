
.. index::
   pair: Motion 4 ; 2016

.. _motion_4_2016_etpic30:

=========================================================================================
Motion n°4 2016 : Dépôt du combat syndicaliste pour les syndicats (ETPIC 30) [adoptée]
=========================================================================================



Argumentaire
============

Les syndicats peuvent et sont fraternellement invités à demander des
exemplaires du combat syndicaliste en dépôt, cela pour permettre de le
diffuser le plus largement dans les différentes localités.

Nous constatons à regret que peu de syndicats le diffusent, et qu’il y a
peu de retour financier qui est fait sur les ventes éventuelles.


Motion
=======

(Amende le dernier paragraphe de la motion en vigueur
*combat syndicaliste Ventes & Abonnements* 26ème Congrès confédéral
du 5 et 6 Décembre 1998 à Paris).

Les syndicats qui le demandent, peuvent bénéficier de plusieurs
exemplaires du journal « le combat syndicaliste » en dépôt.

Toutefois, si au bout de 3 mois, les syndicats n’ont pas payé les
exemplaires, la mise en dépôt est suspendue.


Contre motions
==============


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°4 2016

       :ref:`ETPIC30 <etpic30>`
     - 34
     - 12
     - 2
     - 1
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
