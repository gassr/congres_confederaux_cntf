
.. _motion_3_2012:
.. _motion_3_2012_ste72:

=======================================================================================================
Motion n°3 2012 : Organisation des congrès de la CNT et non-prise en compte des NPPV, STE72 [adoptée]
=======================================================================================================
  
.. seealso::

   - :ref:`ste72_pub`
   - :ref:`motions_ste72_2012`




Préambule
=========

Cette motion concerne l'organisation des congrès futurs de la CNT, mais est
également proposée comme motion d'ordre pour l'organisation du Congrès
confédéral de 2012.

Chaque point peut être débattu et adopté séparément.

Motion
======

Lors des congrès de la CNT, les NPPV (Ne Prend Pas Part au Vote) ne doivent être
comptabilisé comme sufrage exprimé.

Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°3           |            |          |            |                           |
| STE72                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Contre-motions
==============

- :ref:`contre_motion_3_2012_ess34`
