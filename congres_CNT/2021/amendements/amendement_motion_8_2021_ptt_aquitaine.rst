
.. _amendement_motion_8_2021_ptt_aquitaine:

===============================================================================================================================================
Amendements à la motion n°8 2021 **Prostitution et Abolitionnisme révolutionnaire** par **PTT Aquitaine**
===============================================================================================================================================

- :ref:`motion_8_2021_etpics94`

Autres motions sans69

    - :ref:`syndicats:motions_ptt_aquitaine`

**Du passé, au présent contre la marchandisation du corps**

Argumentaires 
===============

Historiques 
--------------

Mujeres Libres, campagne contre la prostitution Madrid
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
Groupes Femmes Libres CNT FAI. Prostitution.
Les «music-halls» et les maisons de prostitution débordent toujours de
foulards rouges, rouges et noirs et de toutes sortes d'insignes
antifascistes. C'est une incompréhensible incohérence morale que nos
miliciens -qui défendent magnifiquement nos chères libertés sur le
front- soient à l'arrière ceux qui soutiennent et même renforcent la
dépravation bourgeoise dans une des formes les plus dures de
'esclavage : la prostitution de la femme. On ne peut expliquer que les
mêmes esprits qui dans les tranchées sont disposés à tous les sacrifices
pour vaincre, dans une lutte à mort, encouragent dans les villes l'achat
de la chair de leurs sœurs de classe et de condition. COMBATTANTS,
que ce ne soit pas vous, nos propres camarades, qui compromettiez,
par une conduite de petits bourgeois, une tâche si ardue déjà.

Aidez-nous à faire que toutes les femmes se sentent responsables de leur
propre dignité humaine. N'outragez plus celles qui, pour survivre,
supportent votre tyrannie d'acheteurs pendant que nous nous escrimons
à trouver le meilleur moyen d'émanciper ces vies.

COMBATTANTS : coopérez avec nous dans cette tâche difficile.
Femmes Libres, Madrid – Barcelone. (Espagne 1936)

.. figure:: ptt_aquitaine/amendement_motion_8.png
   :align: center

Une des affiches de la campagne contre la prostitution menée par les
groupes "femmes Libres", seuls ou accompagnés par la CNT FAI.
A noter que cette campagne s'accompagnait aussi de slogans sur les paquets
de cigarettes, d'affiches murales, de prospectus, etc.
 
D’hier à aujourd’hui : un combat toujours d’actualité
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 
L’Alliance des Femmes pour la Démocratie-MLF et le mouvement Femen,
représentant ensemble deux générations de combattantes, sont résolument
pour l’adoption de la proposition de loi sur la prostitution, par
l’Assemblée nationale, le 12 juin 2015.
 
Parce que la prostitution est et a toujours été un esclavage, une
exploitation sauvage du sexe, du corps, de la chair et de l’identité des femmes.

Parce qu’en ces temps de crise, de guerres, de migrations forcées, de
régressions sociales dont les femmes sont les premières victimes, nous
devons plus que jamais défendre nos droits.

Parce que la prostitution est un système esclavagiste dans lequel le
prostitueur (le proxénète) et le prostituant (le client), échangent la
prostituée (des femmes à 85%) comme une marchandise.

Le combat contre cette institution est prioritaire et vital pour une
société de liberté et d’égalité et pour la dignité humaine.

La France est depuis 1949 pour l’abolition de la prostitution.
Dans une résolution du 26 février 2014, le Parlement européen a proclamé
que la prostitution est incompatible avec la Charte des droits fondamentaux
de l’Union européenne. Il est temps d’agir concrètement pour le progrès
humain.

L’Assemblée nationale, suivant la voie d’un nombre croissant de pays,
a voté avec courage en 2013 un texte équilibré, articulé autour du r
enforcement de la lutte contre le proxénétisme et la traite des êtres
humains, le renforcement des droits des personnes prostituées, la création
pour elles d’un parcours de sortie et l’interdiction de l’achat d’actes
sexuels.
En ajoutant pour la première fois la pénalisation des clients-prostituants
à celle des proxénètes-prostitueurs, cette loi va donner une force
nouvelle à un combat de longue durée : sans clients, pas de prostitution.
Pas de demande, pas d’offre !

Au jour où ce texte, refusé par le Sénat, revient devant l’Assemblée,
nous demandons aux députées et députés, de réaffirmer leur engagement
premier en faveur des droits humains et contre l’esclavage sexuel.
En votant cette loi, la France franchira une étape décisive et historique
dans l’abolition de l’esclavage sexuel des femmes.
Le MLF et FEMEN, main dans la main, l’encouragent à oser. Deux générations
manifestent ensemble pour l’abolition de la prostitution,
Hier, aujourd’hui, pour demain.
   
Contre le réglementarisme nous donnons la parole au Docteure Michèle Dayras ( présidente SOS SEXISME ( extraits)
 
« Selon eux, le droit à la libre disposition de soi est un droit fondamental
devant s’appliquer à toutes les femmes », on se demande de quel droit
fondamental il est question dans la prostitution et si les femmes choisissent
d’être exposées dans les vitrines d’Amsterdam - comme des animaux les jours
de foire - ou présentées nues dans les bordels de Berlin, évaluées,
jaugées par les regards vicieux, libidineux ou méprisants de prédateurs
en rut, avant de devenir de purs objets sexuels qu’ils utilisent à leur
gré de façon plus ou moins violente, mais toujours destructrice et
parfois mortelle.

La liberté accordée aux femmes de se prostituer, au nom de la libre
disposition de son corps pour laquelle elles ont lutté, est une pure
insulte faite au combat des femmes pour leur libération et leur dignité !

Dans les années 70, nous voulions maîtriser notre procréation pour sortir
de l’esclavage plurimillénaire des grossesses répétées et non désirées.
Nous disions « Un enfant, si je veux, quand je veux ! » mais jamais
nous n’avons imaginé faciliter la prostitution une fois cet avantage obtenu. »
  
Nous avons souhaité donner la parole à des associations amis ou à nos
anciennes compagnes de la CNT-AIT, une multitude d’autres textes du
combat des camarades féministes pourraient servir d’argumentaires mais
nous terminons par le témoignage de Laurence (extraits journal l’huma )
qui nous conforte dans notre combat contre la prostitution et pour
son abolition ….
 
Beaucoup de personnes fantasment sur la call-girl. Vous, vous avez des
mots durs pour décrire la prostitution que vous avez vécue…
 
Laurence  Je travaillais rue Saint-Denis, à Paris. J’étais jeune et jolie.
De la chair fraîche. Je faisais une trentaine de passes par jour,
je me souviens que les anciennes étaient très jalouses, car elles ne
montaient quasiment plus. J’étais un automate qui montait et descendait.
À l’instant où j’ai posé le pied sur le trottoir, je suis devenue une
ombre parmi les ombres. J’ai perdu ma dignité d’être humain.
Une partie de moi a cessé d’être vivante. J’étais devenue un objet,
un déchet, dans la lignée de ce qu’avait été le début de ma vie.
Je n’étais que honte et humiliation. Ça fait mieux de se dire call-girl
que prostituée. Il n’empêche que ce n’est qu’une stratégie d’évitement
par rapport à la honte. Les call-girls se détestent autant mais estiment
avoir de la valeur à travers des clients qui possèdent eux-mêmes de la
valeur. Mais le fait même que le client paie est déjà une violence.
Quand on achète quelque chose, on est en droit d’être exigeant.
 
Que pensez-vous des personnes qui affirment se prostituer par choix ?

Laurence : Moi aussi je l’affirmais quand j’étais dedans. Pour se faire
accepter de la société, mieux vaut parler de son libre choix que d’évoquer
sa souffrance. On dit toutes que c’est notre choix quand on est en prostitution.
Cela me fait penser aux personnes qui boivent. Elles affirment qu’elles
savent gérer. Celles qui s’en sortent avouent en avoir souffert.
Quand on est dedans, on ne voit rien, on est dans le déni.
Étant petites, ne rêvaient-elles pas d’être docteur ou boulangère ?
Que fait-on de nos talents et de nos richesses ? Je ne pense pas que
tailler une pipe en soit un. La prostitution consiste à louer son corps
à n’importe quel homme. Et ils ne sont pas tous des Brad Pitt.
Demandez à une femme qui s’aime, s’estime, d’aller se prostituer.
Même dans la misère, elle ne le fera pas
D’aucuns parlent de la réouverture des maisons closes, de la réglementation
de la prostitution. Qu’en pensez-vous ?
Laurence : La souffrance pour les personnes prostituées restera la même.
Les clients seront toujours les mêmes, avec leurs mêmes exigences, leurs
mêmes fantasmes. On parle souvent de la prostitution avec des mots
châtiés. On débat pour savoir si c’est un métier, on évoque la liberté.
La vérité est tue. On dit que les prostituées aiment « ça ».
Mais comment peut-on aimer avoir une trentaine de rapports sexuels par
nuit avec des hommes de toutes sortes, de tous âges, de tous milieux
sociaux, des petits, des gros, des grands, des maigres, des agressifs,
des pervers, des dépendants sexuels, des malades mentaux, des paumés ?
Il y en a beaucoup qui méprisent les femmes et pensent encore qu’il ne
peut exister que la « putain » ou la « maman ». Ceux-là vont dégazer ?,
se venger, traiter les prostituées de tous les noms pendant les actes.
Et leur faire mal. Comme les clients payent, ils s’autorisent tout.
On prend la femme à sec, estimant qu’elle n’a pas besoin de préliminaires,
pas besoin de mouiller. Il faut qu’un jour je puisse vraiment expliquer
en détail ce qu’est une nuit avec des clients. J’ai encore du mal à en parler.
Que pensez-vous du débat sur la pénalisation du client et sur l’abolition
de la prostitution ?
Laurence : Mais pourquoi depuis des millénaires en est-on encore à des
débats à la con ? Il y a 80 % de femmes qui souffrent et il faudrait
écouter l’infime minorité ? Il faudrait empêcher que la loi passe
pour celles qui, soi-disant, sont fières de se prostituer ?
Oui, la pénalisation peut faire évoluer les choses. Mais ce n’est
pas parce qu’existe une loi que tout se réglerait d’un coup.
Elle marque les limites. L’abolition est la réponse à la question :
dans quelle société voulons-nous vivre ? C’est bien parce que des
personnes affirment que c’est possible que le monde change.
J’ai découvert l’existence de l’Union des survivantes du trafic sexuel,
aux États-Unis, qui m’a contactée pour faire partie de leur réseau.
Je n’ai pas le cœur d’une militante. Je pourrais dire non à ces sollicitations,
mais je pense que la vie me demande de l’être. J’ai été auditionnée
à l’Assemblée nationale une première fois, le 29 mai 2013, à huis clos.
En sortant, j’ai pleuré pendant des heures, mais de grâce, de joie.
C’est un beau cadeau pour moi de constater que mon livre peut œuvrer à u
n projet de loi contre le système prostitutionnel.
C’est une réalisation de soi. C’est poser sa petite pierre en ce monde
et si nous en posons tous une, c’est le monde qui change.



Vote
====

+------------------------------+------------+----------+------------+---------------------------+
| Nom                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==============================+============+==========+============+===========================+
| Amendement la motion n°8     |            |          |            |                           |
| PTT Aquitaine                |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès          |            |          |            |                           |
|                              |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
