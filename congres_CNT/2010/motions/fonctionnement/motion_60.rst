


.. _motion_60_2010:

====================================================
Motion n°60 2010 : Label confédéral (SUB­TP­BAM RP)
====================================================



Motion
======

Ce label est dépendant de l’avis favorable, conjoint et justifié:

- du :term:`B.C`, 
- de la  structure régionale (UL ou UR) la plus proche du lieu d’exercice de 
  ce nouveau syndicat,  
- et de sa fédération d’industrie de rattachement, quand elle existe. 


Cet avis sera soumis pour validation effective à l’approbation d’un
:term:`C.C.N`.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°60          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso:: :ref:`amendement_motion_60_2010_ETPRECI35`
