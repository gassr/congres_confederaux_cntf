

.. _motions_adoptees_congres_CNT_2010:

========================================================
Motions adoptées au XXXIe Congrès CNT Saint-Etienne 2010
========================================================



Stratégie syndicale
===================

- :ref:`motion_56_2010`
- :ref:`motion_36_2010`
- :ref:`motion_44_2010`
- :ref:`motion_synthese_des_motions_40_41_2010`


Fonctionnement organique
========================

- :ref:`motion_17_2010_vote_points_2_3_adopte`
- :ref:`motion_10_2010`
- :ref:`motion_11_2010`
