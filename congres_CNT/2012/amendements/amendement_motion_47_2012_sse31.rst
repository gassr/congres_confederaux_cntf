
.. _amendement_motion_47_2012_sse31:

==========================================
Amendement à la motion 47 (SSE31)
==========================================

.. seealso::

   - :ref:`motion_47_2012_chimie_bzh`



Amendement
=============

Proposition de reformuler la dernière phrase de la motion.

Ne garder que

«Toutefois, les syndicats habitués et fonctionnant généralement
avec le net peuvent demander explicitement au BC de ne plus recevoir la
circulaire papier»

et supprimer

«mais cette demande doit être écrite et à  renouveler à chaque congrès.»


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°47 |            |          |            |                           |
| SSE31                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
