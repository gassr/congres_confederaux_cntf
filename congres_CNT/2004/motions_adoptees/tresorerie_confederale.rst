
.. _motion_tresorerie_confederale_cnt_congres_saint_denis_2004:

==========================================================================================================
Motion "Trésorerie confédérale" de la CNT  28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
==========================================================================================================

Prêts aux syndicats et aux unions de syndicats
==============================================

Des prêts financiers peuvent être accordés à des syndicats CNT ou des unions
de syndicats CNT (statutairement reconnus) par le Bureau confédéral sur la
trésorerie confédérale.

Si l’emprunt s’élève à plus de 20% de la trésorerie confédérale disponible,
l’accord du CCN le plus proche est requis.
L’UR de référence, s’il elle existe, est consultée.

La totalité des prêts accordés par le Bureau Confédéral seuls ne peut engager
plus de 33% (un tiers) de la trésorerie confédérale.
