
.. index::
   pair: CCN; quorum

.. _motion_45_2012:
.. _motion_45_2012_ess34:

==========================================================
Motion n°45 2012 : Les prises de décision en CCN (ESS 34)
==========================================================
  

.. seealso::

  - :ref:`motion_4_2010`
  - :ref:`motions_ess34`




Argumentaire
============

Il nous est apparu lors du compte-rendu interne du dernier CCN qu'il
n'existait pas de texte précisant les modalités de prise de décision en CCN.
Nous proposons donc cette motion au congrès confédéral afin de poser par
écrit les premières règles.

Motion
======

Quorum : Pour que des décisions puissent être prises lors d'un CCN celui-ci
doit réunir au moins la moitié des Unions régionales de syndicats confédérés
à la CNT déclarées.

Adoption d'une motion : Une motion est adoptée si la majorité absolue des UR
présentes (>50%) votent pour celles-ci, dans tous les autres cas la motion
ne peut être adoptée.

Vote
=====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°45          |            |          |            |                           |
| ESS34                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Contre-motions
==============

- :ref:`contre_motion_45_2012_stgl_rp`
