.. index::
   pair: Motion 18 ; 2021

.. _motion_18_2021_ste93:

================================================================================================================================================
Motion **n°18** 2021  **Procédure de la labellisation** par **STE 93**
================================================================================================================================================

- :ref:`motion_19_2021_ste93`

Autres motions STE93

    - :ref:`paris:motions_ste93`


.. _preambule_motions_18_19_2021:

Préambule aux motions 18 et 19 2021 ST93
===========================================

Motion déposée par le STE93, dans le chapitre *Structuration et cohésion confédérale*

Présentation de l’esprit de des motions 18 et 19 (2021)
========================================================

Les statuts de la CNT reconnaissent un pouvoir de vote égalitaire entre tous les
syndicats de la confédération afin d’éviter une prise pouvoir d’un syndicat
qui aurait plus d’adhérent·e·s que les autres.

Nous savons que l’activité d’un syndicat ne peut pas se mesurer uniquement au
nombre de cotisant·e·s.
De plus, ce fonctionnement garantissant le même nombre de voix à chaque
syndicat est un principe fort qui permet, en évitant une influence trop grande
de quelques champs d’industrie, d’œuvrer au développement interprofessionnel
de la CNT.

Ces motions ont pour objectif de maintenir ce principe.

De plus cette motion, dont l’enjeu est de fixer un nombre minimum d’adhérent·e·s
pour la création d’un syndicat, doit permettre une réflexion quant au développement
nécessaire de la CNT.

En effet, un syndicat de deux adhérent·e·s ne peut pas mettre en œuvre un réel
débat en son sein.

De même, un nombre minimum d’adhérent·e·s est nécessaire pour pouvoir participer
aux différentes instances de la CNT (union locale, union départementale, fédération…)
et avoir une autonomie financière.

Enfin, ces motions pourront permettre aux syndicats interprofessionnels, ou aux
UL et/ou UR, de mettre en œuvre des actions pour aider la création de syndicats
et donc de participer au développement de la CNT.


Motion sur la procédure de la labellisation
=================================================

Article 1
----------

Dans le cadre de la procédure de la labellisation, **le syndicat qui en fait la
demande doit être composé d’au moins cinq adhérent-e-s**.

Article 2
----------

*Si aucun syndicat n’existe dans le département*, un **syndicat interprofessionnel
peut être labellisé avec  moins de 5 membres**.


Amendements
===========

- :ref:`amendements_motion_18_2021_interpro31`
- :ref:`amendement_motion_18_2021_stics72`
- :ref:`amendement_motion_18_2021_interco69`

Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°18 2021 STE93      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
