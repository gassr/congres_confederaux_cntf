

.. _contre_motion_43_2010_etpreci35:

=====================================================
Contre-motion à la motions N°43 (ETPRECI35) [rejetée]
=====================================================


Lors de la création d'une section syndicale elle dépend toujours d'un syndicat
interprofessionnel, ce syndicat peut avoir une couverture géographique adapté:

- locale
- départementale
- ou régionale

permettant de réunir l'ensemble des agents d'une collectivité territoriale.


.. _vote_contre_motion_43_2010_etpreci35:

Vote contre-motion à la motions N°43 (ETPRECI35)
================================================

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°43     | 12         | 22       | 16         | 17                        |
| ETPRECI 35                         |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


**Contre-Motion rejetée**


.. seealso::

   - :ref:`motion_43_2010`
