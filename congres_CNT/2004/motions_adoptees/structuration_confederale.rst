

.. _motion_structuration_confederale_cnt_congres_saint_denis_2004:

=============================================================================================================
Motion "Structuration confédérale" de la CNT  28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=============================================================================================================

Label confédéral
================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 34         | 13       | 16         | 9                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


- Un nouveau syndicat qui se créé ou demande son adhésion à la CNT, doit
  demander son adhésion à :

      1. l’Union locale correspondante, si elle existe;
	  2. l’Union régionale correspondante, si elle existe;
	  3. la Fédération d'industrie correspondante, si elle existe.

- Une organisation adhérente à la CNT ayant droit à la marque distinctive
  appelée label confédéral, celui-ci lui sera automatiquement donné par le
  Bureau Confédéral, si aucune des structures existantes précitées ne s’y
  oppose, dans un délai de deux mois.

- si un nouveau syndicat se crée ou demande son adhésion à la CNT, et
  qu’il n’existe aucune Union Locale, Union
  régionale ou Fédération correspondant à sa situation géographique ou
  professionnelle : Celui-ci demandera son adhésion à l’Union Régionale la
  plus proche.

- Une organisation adhérente à la CNT ayant droit à la marque distinctive
  appelée label confédéral, celui-ci lui sera automatiquement donné par le
  Bureau Confédéral, si aucun syndicat, Union Régionale ou Unions Locales
  limitrophes, et aucune Fédération d’industrie existante, ne conteste, dans
  un délai de deux mois.

- Lorsqu’il y a opposition de l’une de ces structures, le B.C. doit suspendre
  sa décision, qui doit être soumise au prochain C.C.N. (ou au C.C.N.
  extraordinaire convoqué dans les conditions prévues par les statuts), qui a
  pouvoir de décision provisoire.

- Les organismes impliqués dans le litige – ou un syndicat CNT par la voie
  d’une motion – peuvent faire appel de la décision devant le Congrès.
  Le Congrès seul peut se prononcer définitivement.
- En cas de circonstances graves, le C.C.N. peut décider la convocation d'un
  Congrès extraordinaire.
- Les organismes concernés, gardent le droit de présenter directement leur
  défense soit au C.C.N., soit au Congrès.
- Tout litige présenté au C.C.N. ou au Congrès devra être inscrit à l'ordre
  du jour, dans les délais.
