

.. _reunion_bc_ca_2021_03_13:

=================================================================
Samedi 13 mars 2021 **RÉUNION DU BC/CA DU samedi 13 MARS 2021**
=================================================================



Présent.e·s:
===============

- ned/educ21, intranet, accueil congrès
- Serge STP 26 /Secrétaire confédéral contact
- Théo SINR 44 (Postmaster)
- Gilles CNT 07 Textile
- Juliette SINR44 (Webmestre)
- Daniel CNT 07 / Adm CS
- Nicolas (SINR44) et MPI (Marseille) rédaction cs
- Bruno STP 26 / Secrétaire confédéral administratif
- Christian CNT 09 / Trésorerie confédérale
- Tristan ETPRECI 75 / SI
- Ya, SIPMCS-RP, intranet
- Fonz, réseaux sociaux (SEST Lorraine)
- Vincent et Kelly CNT 30 (secteur propagande)

Prise de note et compte-rendu : Tristan et Fonz

Ordre Du Jour:
===============

- le congrès confédéral
- point sur les mandats

CONGRÈS CONFÉDÉRAL
===================

LOGISTIQUE

Date: Le prochain congrès de la CNT se tiendra du 25 au 27 juin 2021.

Lieu: aux Tanneries à Dijon

Logement: dortoirs collectifs, chez des camarades, camping.  Possibilité de
logerdans une auberge pour les camarades ayant besoin de plus de confort:
réservationobligatoire au plus tôt.

100 à 120 personnes attendues.Vie quotidienne: la commission
d’organisation du congrès va faire un tableau destâches.

Transport: le tram relie directement la gare de Dijon aux Tanneries.

La CNT 21 s’occupe du co-voiturage intra-muros. Les syndicats s’occupent
du transport longue distance.

Samedi soir: soirée musique, apportez vos instruments et
carnets de chants.Possibilité d’avoir accès à une sono, en faire
la demande.

Commission d’accueil des mandats: elle se réunira dès le jeudi.
Les syndicats devront être à jour de toutes leurs cotisations
confédérales, régionales, fédérales etc.et de la péréquation pour
participer au Congrès.

Contactez la trésorerie confédérale en cas de souci ou de question.

La commission d’organisation du Congrès va envoyer prochainement un
formulaired’inscription.

DÉROULÉ DU CONGRÈS
=======================

Le congrès de déroulera dans le meilleur des cas sur 3 jours.

Il est prévu de s'ouvrir le vendredi matin à partir de 10h
au moment où le quorum de 50% des syndicats sera atteint.

.. _calendrier_motions_2021:

Calendrier des motions mai 2021
=================================

- Les syndicats ont jusqu'au mardi 23 mars 2021 pour déposer des motions non
  statutaires.
- Le secrétariat confédéral envoie le 1er cahier de motions aux syndicats
  le jeudi 25 mars 2021.
- **Les syndicats ont jusqu'au dimanche 23 mai 2021** pour déposer des amendements et des
  contre-motions.

**Le secrétariat confédéral envoie le cahier final des motions le mardi 25 mai 2021**.

L'ordre des motions proposées aux syndicats au début du congrès reprendra
les demandes exprimées par certains syndicats à savoir commencer par:

- les motions :ref:`numéro 18 <motion_18_2021_ste93>` et :ref:`19 <motion_18_2021_ste93>` du syndicat STE 93
- la motion :ref:`numéro 13 du syndicat Interpro 09 <motion_13_2021_cnt09>`
- la motion :ref:`numéro 24 du syndicat STE 33 <motion_24_2021_ste33_cnt42>`

**C’est le Congrès qui décidera au final de l’ordre des motions en début
de congrès**.

Il y aura un point d’ordre au milieu du congrès pour réajuster éventuellement
l’ordredes motions.

Comme lors de chaque congrès il sera fait appel aux différent.e·s mandaté.e·s
afin d'occuper les missions de présidences, de comptes rendus et de tenus
du temps de parole.

Afin de faire avancer le congrès que nous attendons tou.te·s, si les discussions
se crispent sur certains points, il pourra être demandé de limiter le
nombre d’interventions ainsi que la durée du temps de parole,
comme nous l’avons déjà expérimenté dans nos différentes expériences
pratiques de démocratie directe.

Le nombre de motions étant important, un strict respect des horaires est
nécessaire.

Les syndicats sont invités à présenter des président.e·s de séance capable
d’encadrer les débats.
L’organisation de ce congrès se déroulant sur 3 jours sans un jour férié
pouvant pénaliser les mandaté.e·s du secteur privé merci aux syndicats
de faire remonter leurs besoins éventuels.

Des délégations internationales seront invitées.

MANDATS CONFÉDÉRAUX
====================

Les mandats confédéraux seront tenus jusqu'au congrès pour certains,
avis aux syndicats de proposer leur participation à l'autogestion de
l'organisation car il y atoujours des besoins dans certains mandats

Secteur Vidéo
---------------

Juliette 44 (webmasteuse) relèvera les mails mais le site est inactif.

Secrétariat confédéral
-----------------------

Serge 26 récupère toutes les listes tout doucement demande labellisation
Santé social éduc du 44 en cours

Propagande (création visuel)
-----------------------------

Kelly et Vincent (30) mandaté.e·s propagande (imprimerie) souhaitent
travailler plus en relation avec Kadé (59) avant le congrès et proposer
lors du congrès la reprise de ce mandat confédéral

Textile
----------

le 07 reprend le matériel du 69 et vont voir proposer un stand lors du
congrès. Rappel que la Région Parisienne édite aussi en interne en
soutien aux locaux du 33 des Vignoles.

Dernièrement le Nord Pas de Calais Picardie vient de sortir des offres
textiles sur La Commune.

Administration CS
--------------------

depuis 4 ans dans le 07, CS est largement excédentaire dans les comptes
214 abonnés ( dont gratuits) = très peu
525 ex pour dépôts, libraires syndicats prêt à laisser le mandat si
candidats, sinon on reprendra mandat pour 2 ans pas plus.

Rédac CS
----------

On attend toujours vos textes ! Marie Pierre (13) et Nico (44) rendront
leur mandat au congrès avec tuilage évidemment.
mandat forcément assez politique et pas uniquement que technique.
Un peu d'usure des deux mandaté·es à la longue.
Deux semble pas suffisant. Mieux vaudrait une prise en compte
collective plus large 4 ou 5.

Depuis deux mois eu l'appui de deux camarades des Saône-et-Loire, mais pas
assez présent.e·s pour vouloir reprendre reprendre le mandat.
**Perte de deux correcteur·rices.**

Postmaster
------------

Théo (44) qui tient le mandat depuis 2016 le rendra au congrès.

Beaucoup de demande suite à la perte mot de passe lors des passations de
mandat, peu de demande de créations d'adresses ou de listes.

Toujours des confusions mandat webmaster et postmaster.

Secrétariat international
---------------------------

Deux mandatés Pat du Stics 13 et Tristan Etpreci 75 ont repris le mandat
qui comporte 5 places.
Travail en cours : venue de la délégation des compas zapatistas,
réunion du SI à Marseille au printemps, production ric rac pour fournir des
articles dans le CS, une émission radio animée à Paris, réunion de la
Coordination rouge et noire pour le 8 mars 2021 et rencontre de branche,
au niveau international, sur l'éducation en perspective, suit la CIT...

Intranet
------------

Ned et Dad (21) ont repris le mandat
Pierre SIMPCS gestion de nexcloud (,) pour nouveau serveur.

La migration est en cours sur globenet, listes comprises.
Les adresses internes perso en cnt-fr.org ne vont plus fonctionner.

Ya SIMPCS (16 ans de mandat) rend son mandat au congrès.

Réseaux sociaux
---------------------

Fonz (SEST Lorraine) a relancé le twitter confédéral dernièrement,
mais toujours confusion avec le twitter de la RP qui a un twitter au nom
de "La CNT"
