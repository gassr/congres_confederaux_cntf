
.. _amendement_motion_26_2014_stp67:

================================================
Amendement à la motion n°26 2014 (STP67)
================================================

.. seealso::

   - :ref:`motion_26_2014_etpic30`




Argumentaire
=============

Petite précision pas piquée des hannetons.

Amendement
==========

À l’issue de chaque Congrès Confédéral ou de chaque CCN, les résolutions
confédérales les plus récentes entrent en vigueur et invalident les précédentes
dans la mesure où elles les modifient, les complètent, les annulent, ou les
contredisent.

Pour éviter toute ambiguïté, la commission de préparation du Congrès se
chargera en amont d'indiquer sur le cahier des motions si une motion aurait
pour objet de modifier, de compléter, d'annuler ou de contredire une précédente
décision confédérale.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°26 2014 <motion_26_2014_etpic30>`

       :ref:`STP67 <stp67>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
