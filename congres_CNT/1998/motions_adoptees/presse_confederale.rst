
.. _motions_presse_confederale_cnt_congres_paris_1998:

====================================================================================================
Motions Presse confédérale de la CNT  26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
====================================================================================================




Ventes & abonnements du Combat Syndicaliste
===========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 38         | 15       | 10         | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Lors du CCN de Juin 1998, le militant représentant le Comité de Rédaction du
Combat Syndicaliste a présenté un tableau assez sombre des remontées des ventes
du Combat Syndicaliste de la part des syndicats.
Cela pose plusieurs problèmes.

Le Combat Syndicaliste est privé d'une grande partie des produits des ventes
militantes ce qui empêche certaines initiatives :

- publications de suppléments sur un thème donné
- augmentation ponctuelle de la pagination
- tirage de matériel de propagande pour le C.S.;

Problème plus grave : une certaine irresponsabilité des syndicats qui se
comportent en consommateurs en réceptionnant le C.S. sans se soucier de nos
principes fédéralistes et autogestionnaires.

Nous proposons la réorganisation de la diffusion selon les modalités suivantes:

Remise d'une ristourne aux syndicats diffuseurs
+++++++++++++++++++++++++++++++++++++++++++++++

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 29         | 11       | 4          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Une ristourne est accordée aux syndicats diffuseurs : à l'image de ce qui se
fait pour les Temps Maudits, nous proposons que sur chaque numéro vendu le
syndicat prélève la somme de 0,46 euros (3,00 fr.) pour sa trésorerie locale.


Une remontée régulière des sommes perçues lors des ventes
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 35         | 0        | 1          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+



Nous suggérons que le dernier mois de chaque trimestre, chaque syndicat envoie
le règlement de ses ventes, tout en conservant les invendus à des fins de
propagande.

Lorsqu'un syndicat ne règle aucun CS durant 2 trimestres consécutifs nous
proposons que le nombre d'exemplaires soient envoyés à ce syndicat soit
automatiquement ramenés à 3 exemplaires.

Il est en effet inutile d'envoyer des dizaines de numéros à une structure
locale qui ne comprend pas qu'il appartient à une confédération et qui se
soucie peu de l'équilibre des comptes de l'Administration du journal

Proposition systematique d'abonnement aux nouveaux adherents mais non
obligatoire

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 29         | 3        | 0          | 8                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

.. seealso:: :ref:`motions_presse_confederale_cnt`
