
.. _motions_strategie_syndicale_cnt_congres_agen_2006:


====================================================================================
Motions "stratégie syndicale" 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
====================================================================================

Représentation du personnel dans le secteur privé
=================================================

Vote
----

70 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 43         |  18      | 5          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----


**La CNT maintient sa position historique contre le principe des CE.**

La CNT, dans le cadre de la défense des travailleurs dans les entreprises du
secteur privé, laisse le soin aux structures syndicales au plus près du terrain
d'organiser son action syndicale (sections syndicales en cohésion impérative
avec le syndicat d'appartenance) en fournissant à l'UR informations et
explications.


"Développement du secteur privé"
================================


Vote
----

70 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 47         |  8       | 8          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

L'ensemble des syndicats de la CNT s'engage à mettre en œuvre le développement
du secteur privé au sein de la CNT. Cela passe par une propagande particulière.

Mais c'est aussi et surtout un travail quotidien de suivi juridique et
revendicatif, de formation ainsi que de structuration qui devra être assumé
par l'ensemble des syndicats des Unions Locales ou Départementales dans un
esprit inter corporatiste.

Pour ce faire, l'ensemble des structures CNT du secteur privé (fédérations et
syndicats syndiquant des travailleurs du secteur privé) est appelé à constituer
une coordination.

Cette coordination permettra un échange concret d'expériences CNT dans le privé.
Elle permettra d'établir une stratégie et des campagnes syndicales communes à
destination du secteur privé.

Cette coordination se dotera d'un secrétariat qui assurera principalement la
constitution de circulaires. Jusqu'à nouvel ordre ces circulaires seront
financées par la confédération. Ces circulaires seront composées de tracts
et autres contributions en provenance des structures CNT du privé.

Cette circulaire sera envoyée à tous les syndicats de la CNT afin qu'ils s'en
inspirent et répercutent les luttes du secteur privé sur leurs localités.
