
.. _amendement_motion_17_2012_stas_ra:

==========================================
Amendement à la motion 17 (STAS-RA)
==========================================

.. seealso::

   - :ref:`motion_17_2012`



Argumentaire
=============

La motion ci-dessous est un amendement à la :ref:`motion n°17 <motion_17_2012>`
(et non une contre-motion), elle n’a vocation à être discutée que si la
motion  n°17 est adoptée dans un premier temps.

Nos amendements portent sur deux points:

Nous pensons que les 4 dernières conditions énumérées pour pouvoir avoir
recours à un permanent technique (droits sociaux, fiche de poste, encadrement,
viabilité financière) ne constituent pas en tant que telles des conditions
générales de principe devant relever du niveau confédéral mais des modalités
techniques de mises en oeuvre devant être discutées au niveau de l’assemblée
générale de la structure employeuse ( syndicat, union ou confédération).
En revanche la motion n’évoque pas dans ses conditions cumulatives la question
de la formation et de la rotation des tâches. En effet la permanence des
tâches n’implique pas forcément la permanence de la personne. Nous pensons
qu’il convient, même en cas de recours à un permanent technique, de garder
comme objectif une rotation efective des tâches par une mutualisation des
compétences. En conséquence nous souhaitons comme condition supplémentaire
qu’une partie du temps du permanent technique soit obligatoirement consacré
à la formation interne des camarades l’ayant mandaté afin d’une part de
rendre possible à terme une rotation effective des tâches et d’autre part
de tenter parvenir à se passer du recours à un permanent technique

Amendement
===========

Nous proposons la suppression des conditions suivantes:

«Établissement d’un consensus confédéral sur les niveaux de rémunération, de
droits aux congés, d’évolution de carrière, de formation, de droits syndicaux
(y compris au sein de la CNT) ;

- Établissement d’une fiche poste spécifique, détaillée et proportionnée à la
  tâche à accomplir ;
- La désignation d’un encadrement accessible et disponible à la/au salarié.e ;
- La viabilité financière de la création de poste à long terme. »

Et l’ajout de la conditions suivante:

Une partie du temps de travail du salarié ou du détaché permanent est
obligatoirement consacré à la formation des syndiqués l’ayant mandaté avec
pour objectif une mutualisation des compétences permettant une rotation
effective du mandat et, le cas échéant , de se passer du recours à un permanent.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°17 |            |          |            |                           |
| STAS-RA                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
