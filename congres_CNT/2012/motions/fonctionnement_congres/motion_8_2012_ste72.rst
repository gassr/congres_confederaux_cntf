
.. _motion_8_2012:
.. _motion_8_2012_ste72:

===================================================================================
Motion n°8 2012:  Organisation des congrès de la CNT et enregistrement vidéo STE 72
===================================================================================
  
.. seealso::

   - :ref:`ste72_pub`
   - :ref:`motions_ste72_2012`




Préambule
=========

Cette motion concerne l'organisation des congrès futurs de la CNT, mais est
également proposée comme motion d'ordre pour l'organisation du Congrès
confédéral de 2012.

Chaque point peut être débattu et adopté séparément.

Motion
======

De plus, par souci de démocratie et de transparence et comme cela se fait dans
d'autres confédérations syndicales, il convient d’enregistrer sur vidéo les
débats et de les difuser sur l'intranet.

Tous les adhérents auront ainsi une idée claire de ce qui s'est dit lors du
congrès de leur confédération. Chacun-e pourra alors se rendre compte du
niveau de démocratie que la CNT a pu atteindre et méditer dessus pour la
prochaine fois.

Vote
=====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°8           |            |          |            |                           |
| STE72                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Contre-motions
===============

- :ref:`contre_motion_8_2012_ess34`
