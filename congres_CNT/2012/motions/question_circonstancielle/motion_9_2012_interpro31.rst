
.. _motion_9_2012:
.. _motion_9_2012_interpro31:

===================================================================================
Motion n°9 2012 : Suspension de L'Union régionale - Région parisienne Interpro 31
===================================================================================
  
.. seealso::

   - :ref:`article_16_statuts_CNT_2010`
   - :ref:`ste_75_pub`
   - :ref:`motions_ste75_2012`
   - :ref:`motions_interpro31_2012`



Argumentaire
=============

Nous n'allons pas retracer ici le conflit qui anime l' Union régionale -
Région parisienne (UR-RP) depuis des années. Les protagonistes se sont
exprimés sur le sujet à d'innombrables reprises de diverses facons.

Les décisions confédérales concernant ce conflit ont toujours privilégiées
l'apaisement et le dialogue et n'ont jamais favorisées un camp ou l'autre :
plusieurs décisions (congrès...) contre toute mesure d'exclusion,
instauration d'une commission « Nettoyage », élection d'un Secrétariat
confédéral (SC) dont les membres sont indépendant du conflit...

Bref, la confédération considère que chaque syndicat a sa place dans notre
organisation.

Il y a environ une dizaine d'année, en Midi-Pyrénées, la confédération a
suspendu l'Union régionale - MP Un conflit opposait plusieurs syndicats.

La suspension a eu pour efet de redonner à chaque syndicat son autonomie
d'action, de délégitimer l'agressivité, d'empêcher l'instrumentalisation
de la région.
Les syndicats ont continués leurs activités et ils sont toujours existants
aujourd'hui (à part un, pour d'autres raisons), d'autres ont été crées depuis.

La suspension n'enlève pas le caractère d'adhérent à la CNT. Les syndicats
de l'UR suspendue peuvent proposer des motions et voter aux congrès, comme
avoir , bien sûr, leur activité syndicale sous l'étiquette CNT.

La suspension entraine uniquement l'arrêt des activités en tant qu'Union
régionale.

L'image exécrable véhiculée par l'UR-RP ne représente pas l'ensemble de notre
confédération, loin de là, il est temps de le réaffirmer.

Pour réaffirmer les positions prises par la confédération ces dernières années
et parce-qu'une telle mesure avait eu un efet positif en Midi-Pyrénées,
l'Interpro 31 propose la motion suivante::

    Le Congrès des syndicats de la CNT suspend l'Union régionale -
    Région parisienne (UR-RP).
    La levée de cette suspension sera examinée en congrès.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°9           |            |          |            |                           |
| interpro31           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_9_2012_interco66`


Contre-motions
===============

- :ref:`contre_motion_9_2012_ess34`
