

.. index::
   Cooperatives


.. _motion_42_2010:

=======================================================================
Motion n°42 2010 : Bourse du travail et coopératives, CCS de la Vienne
=======================================================================



Argumentaire, Syndicat CNT communication Culture et Spectacle de la Vienne
===========================================================================

Dans un contexte de dégradation continue des rares acquis sociaux ou économiques
obtenus, et devant l'accentuation de la précarisation de nos conditions de travail,
il importe de développer autant que possible les outils qui nous permettent de
contrecarrer l'inéluctable. Le syndicalisme révolutionnaire a toujours tenté, dès que
les conditions le rendaient possible, par pragmatisme et dans une visée de
transformation sociale, de trouver les solutions et d'expérimenter hic et nunc
des outils et structures répondant aux besoins des travailleurs.

Lors du XXXe congrès confédéral de la CNT de 2008 à Lille, le vote d'une motion
entérinait la mise en place d'une commission confédérale
«coopératives syndicales/économie coopérative».

Aussi était-elle chargée de:

- Mutualiser les informations liées aux systèmes coopératifs existants par la création
  d’un site internet.
- Coordonner les camarades déjà présents dans le réseau coopératif.
- Réfléchir à la question des reprises en coopératives comme réponse aux délocalisations,
  fermetures d’entreprise et comme alternative aux petites entreprises et à l’artisanat.

Si le bilan de cette commission reste à faire, il apparaît que son champ
d'investigation  demeure trop limitatif au regard des expérimentations à
caractère économique existantes. Parce que des expériences autres, existent au
sein de la CNT, il pourrait être opportun tant pour mettre en exergue leur
réalisation et leur fonctionnement que pour favoriser leur multiplication
quand les conditions sont réunies, d'étendre le spectre de cette commission.
Aussi en est-il par exemple de la question de la « réhabilitation » des
Bourses du travail. Ce type d'outil apparait comme une des alternatives les
plus originales dans notre histoire sociale, mais aussi une des plus pérennes
dans la mémoire collective.

A coté des différents outils mis en place en leur sein à la fin du XIXème siècle,
tel que le Viaticum, les services culturels, la formation professionnelle...
l'activité ayant le plus légitimé cette institution demeure le placement
des salariés. Durant la Belle époque le placement effectué par les Bourses
du travail avait pour objectif d'arracher les salariés aux offices de placement
municipaux ou privés.

Durant une longue période des années 1920 à nos jours, l'effacement des Bourses
du travail mais aussi du syndicalisme révolutionnaire a permis l'intégration au
sein de l'Etat de différents services sociaux. Aussi, le placement est devenu
un outil géré par des structures paraétatiques tel l'ANPE, devenu Pôle emploi plus
récemment, répondant notamment à l'objectif du plein emploi, véritable cache
sexe de la précarisation du travail.

Depuis le début des années 2000 différentes expérimentations ont été menées
au sein de la CNT ayant des visées économiques et sociales, à l'image de la
Bourse du travail des salariés du particulier de la Vienne. D'autres
initiatives du même type ont été menés avec des fortunes diverses en Bretagne,
en Creuse ou dans la Loire.

Et nous ne parlons pas d'AMAP et autres structures à visée alternative.
De récentes réformes, dont les décrets d'application sont attendus,
permettent par ailleurs d'entrevoir plus généralement la possibilité de
renouer avec la gestion par les travailleurs eux-mêmes du placement des
travailleurs. Ces réformes comportent néanmoins un corollaire dangereux :
la possibilité pour des organismes privés de s'approprier le placement ce qui
constituerait un véritable danger et un terrible recul. On peut sans
exagérer considérer qu'il existe d'ores et déjà une véritable course contre
la montre pour ne pas se faire dessaisir une nouvelle fois de possibilités de
gestion directe de nos vies et de notre travail.

Aussi, dans l’optique de travailler « par nous et pour nous mêmes », le syndicat
de la culture communication et spectacle de la Vienne propose d'associer à la
commission coopérative la réflexion collective portant sur les alternatives
économiques et en premier lieu les bourses du travail.

Motion
======

La commission confédérale «coopératives syndicales/économie coopérative»
devient « commission confédérale coopératives syndicales et alternatives économiques ».

Dans le prolongement du travail de la commission coopérative de la CNT initié en 2008,
cette commission élargie a pour objectif de:

- Mutualiser les informations liées aux systèmes coopératifs et aux alternatives
  économiques signifiantes par l'animation d’un site internet (celui initialement
  créé par la première commission).
- Coordonner les camarades déjà présents dans le réseau coopératif et tout autres
  investis dans différentes initiatives de nature économique (Bourse du travail, AMAP...),
  et ce en favorisant la recension et l'évaluation des différentes initiatives syndicales
  en la matière.
- Réfléchir à la question des reprises en coopératives comme réponse aux délocalisations,
  fermetures d’entreprise et comme alternative aux petites entreprises et à l’artisanat,
  mais aussi la question de Bourses du travail favorisant le placement des salariés par
  les syndicats et de toute initiative de nature économique.
- Mettre en place des outils permettant l'acquisition par les adhérents de la CNT
  par le biais de cycle de conférence, la mise en place de formation et tout document
  (brochure, plaquette de présentation...) des éléments nécessaires à la compréhension,
  la mise en place et la gestion des différents outils économiques alternatifs.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°42          |            |          |            |                           |
| CCS de la Vienne     |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
