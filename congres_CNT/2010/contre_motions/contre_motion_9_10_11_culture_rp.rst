

.. _contre_motion_9_10_11_2010_culture_rp:

======================================================================
Contre-motion aux motions N°9, 10 et 11 Culture spectacle RP [rejetée]
======================================================================


Les syndicats s’engagent à s’acquitter du règlement de leur participation
à la péréquation et en cas de  défaillance de l’engagement, les syndicats
concernés seront considérés comme n’étant pas à jour de cotisation.

+------------------------------------------+------------+----------+------------+---------------------------+
| Nom                                      | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==========================================+============+==========+============+===========================+
| Contre motion aux motions n°9, 10 et 11  | 20         | 22       | 12         | 8                         |
| Culture spectacle RP                     |            |          |            |                           |
+------------------------------------------+------------+----------+------------+---------------------------+

**contre motion rejetée.**

.. seealso::

   - :ref:`motions_9_10_11_2010`
