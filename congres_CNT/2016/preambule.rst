


.. _preambule_xxxiv_congres:

=================================================
Préambule XXXIVe Congrès CNT 2016 de Montreuil
=================================================





Mode d'emploi du présent cahier de la (du) mandatée.
=====================================================

Vous trouverez dans ce cahier une aide pour reporter le vote de votre syndicat
quant à chaque motion, contre-motion et amendement.

Le cadre suivant est placé à la suite de chaque texte mis au vote


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°X 2014

       Syndicat XX
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -



Exemple:

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°1 2014

       STE75
     - OK STICS38
     -
     -
     -
   * - Décision du congrès

       Rejetée
     - 29
     - 26
     - 3
     - 4


La première colonne, intitulée "Nom" donne la nature du texte à voter:

- soit motion, contre-motion ou amendement
  + son numéro à l'intérieur de ce cahier,
  + nom du syndicat qui présente le texte.

Les colonnes suivantes donnent les différents choix de vote possible pour le
syndicat.

Il ne reste plus qu'à mettre une petite croix, un chat hérissé ou quelque
autre signe distinctif dans la case correspondante au vote du syndicat
par lequel vous êtes mandaté.e.

La dernière ligne vous permet de noter les votes du congrès et la décision
finale.
Donc conserver ce cahier, vous pourrez faire un compte-rendu clair et précis
de retour dans vos assemblées générales de syndicat.

En fin de cahier, vous retrouverez de l'espace afin de faire des prises de
notes ainsi que des feuillets à remettre au moment de votre départ.

Ces feuillets récapitulent les votes de votre syndicat et permettront une
confirmation des votes.

Lors des deux congrès précédents, il y avait une distinction dans les
modalités de vote entre les "amendements" et les "contre-motions".

Les amendements n'avaient vocation à être discutés que si la motion initiale
avait été adoptée, et les contre-motions n'étaient discutées que si la motion
initiale était rejetée.

C'est ce qu'il y avait mais le congrès confédéral reste souverain.

De même, tout dans ce cahier n'est qu'une proposition.

Vous souhaitant un bon congrès,
La commission de préparation du XXXIVe Congrès confédéral 2014.
