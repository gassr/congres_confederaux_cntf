
.. index::
   pair: Motion 26 ; 2014

.. _motion_26_2014_etpic30:

==========================================================================================
Motion n°26 2014 **Principe de primauté des décisions les plus récentes** (ETPIC 30 sud)
==========================================================================================

Autres motions ETPIC30

    - :ref:`syndicats:motions_etpic30`


Argumentaire
===============

Extrait de :ref:`l’article 3 des statuts <article_3_statuts_CNT_2012>`::

    La C.N.T. est administrée suivant les directives données et les
    décisions  prises par les Syndicats réunis en Congrès,
    **tous les deux ans**.


Les statuts confédéraux confèrent au Congrès Confédéral la souveraineté
de la définition des grandes orientations et du fonctionnement confédéral.

Le CCN «assure la continuité des décisions adoptées en Congrès »
(:ref:`art.4 <article_4_ccn_2012>` des statuts confédéraux)

Or, il nous semble utile, dans le cas où le ``bon sens`` pourrait être
contesté, de préciser dans le cadre d’une motion dédiée que:

Motion
======

A l’issue de chaque Congrès Confédéral ou de chaque CCN, les résolutions
confédérales les plus récentes entrent en vigueur et invalident les
précédentes dans la mesure où elles les modifient, les complètent,
les annulent, ou les contredisent.


Amendements
===========

- :ref:`amendement_motion_26_2014_stp67`
- :ref:`amendement_motion_26_2014_interco25`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°26 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
