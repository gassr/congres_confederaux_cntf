

.. _motions_syndicats_CNT_2021:

=====================================================================================
Liste des syndicats ayant déposé des motions/contre-motions ou amendements (2021)
=====================================================================================

- :ref:`cahier_mandatee_2021`

Alsace
=======

- :ref:`syndicats:motions_stp67`

Aquitaine
============

- :ref:`syndicats:ste33_motions_congres`


Bretagne
==========

- :ref:`syndicats:motions_staf29_congres`

Bourgogne
=========

- :ref:`syndicats:ref_motions_educ21`

Centre
========

- :ref:`syndicats:motions_ptt_centre`

Franche-comté
===============


Ile-de-France
==============

- :ref:`paris:motions_etpics94`
- :ref:`paris:motions_ptt95`
- :ref:`paris:motions_simrp`
- :ref:`paris:motions_ste93`

Languedoc-Roussillon
=====================

- :ref:`syndicats:etpic30`

Lorraine
=====================


- :ref:`syndicats:motions_sest_lorraine`

Midi-Pyrénées
==============


- :ref:`syndicats:motions_cnt09`
- :ref:`syndicats:interpro31_motions_congres`
- :ref:`role_de_linterpro_31_fouad`


Nord pas de calais
=====================

- :ref:`syndicats:motions_stt59_62`


Pays de la Loire
=================

- :ref:`syndicats:motions_sinr44`
- :ref:`syndicats:motions_ste72`

Provence-Alpes-Cote d'Azur
===========================


Rhône-Alpes
============

- :ref:`rhone_alpes:interpro07_motions_congres`
- :ref:`rhone_alpes:educ_38_motions`
- :ref:`rhone_alpes:cnt42_motions_congres`
- :ref:`rhone_alpes:interco69_motions_congres`
- :ref:`rhone_alpes:motions_tasra`
