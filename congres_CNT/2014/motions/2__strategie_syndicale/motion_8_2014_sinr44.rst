
.. index::
   pair: Motion 8 ; 2014

.. _motion_8_2014_sinr44:

========================================================================================================================================
Motion n°8 2014 **Élections professionnelles et représentativité syndicale dans le secteur public - validation confédérale** (SINR 44)
========================================================================================================================================

Autres motions SINR44

    - :ref:`syndicats:motions_sinr44`


Argumentaire
=============

Si les fédérations du secteur public sont les mieux placées pour
connaître le droit qui leur est propre, et proposer des adaptations
à notre stratégie syndicale, elles n'en sont pas pour autant autonomes
de la Confédération, ni totalement hors de son fonctionnement
démocratique.

Aussi une adaptation stratégique impliquant une participation aux
élections professionnelles dans le secteur public doitelle
être validée formellement par les instituions confédérales.

Ajout à la motion « Élections professionnelles et représentativité
syndicale dans le secteur public » adoptée lors du 31ème Congrès
confédéral des 10, 11, et 12 Décembre 2010 à Saint Etienne
(cf recueil des motions en vigueur)

Motion
=========

Si une fédération décide de se présenter aux élections professionnelles
dans le public, elle est tenue de présenter au CCN suivant cette décision
un argumentaire expliquant la pertinence de ces élections, le CCN
valide ou non cette décision. Si cette décision est validée, la fédération
présente un bilan pour une nouvelle validation au CCN suivant l'élection
concernée.


Amendements
===========


- :ref:`amendement_motion_8_2014_stp67`
- :ref:`amendement_motion_8_2014_sse31`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°8 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
