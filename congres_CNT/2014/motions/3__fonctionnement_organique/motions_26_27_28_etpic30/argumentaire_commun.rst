
.. index::
   pair: Motion 26 ; 2014

.. _argumenttaire_motion_26_2014_etpic30:

======================================================================================================================
Argumentaire commun aux motions 26, 27 et 28 2014 **mémoire et évolution des décisions confédérales** (ETPIC 30 sud)
======================================================================================================================

Autres motions ETPIC30

    - :ref:`motions_etpic30`


Argumentaire
===============

Rappel de la motion adoptée au :ref:`Congrès de 2001 <motion_recueil_2001>`::

    Création d’un recueil des motions de Congrès en vigueur depuis 1993
    Le Congrès confédéral charge une commission d'établir un recueil des motions
    en vigueur. Ce recueil devra organiser un classement par catégorie et date
    des motions et il pourra éventuellement y être annexé des documents
    consultatifs en vue d'obtenir un ouvrage clair et unique.

A l'issue de chaque Congrès, ce recueil devra être mis à jour par le bureau
confédéral ou par voie de délégation sous sa responsabilité.

Chaque syndicat pourra sur demande disposer d'un exemplaire laisse à la libre
consultation de ses adhérents.

Rappel de la motion adoptée au Congrès de 2008 à 76% des syndicats présents
au Congrès ::

    « Le Congrès confédéral décide l'élaboration d'un recueil des résolutions
    confédérales en vigueur pré-titré « CNT - Fonctionnement & Orientations ».
    Cet ouvrage annule et remplace le « recueil des motions en vigueur ».

    A l'issue de chaque Congrès, le recueil des résolutions confédérales en vigueur
    devra être mis à jour par le bureau confédéral ou par voie de délégation sous
    sa responsabilité.

    Les modifications attendues intégreront les nouvelles dispositions et
    orientations adoptées.

    Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
    des différentes motions adoptées.
    L'effort de synthèse portera sur l'intégration des décisions les plus récentes,
    invalidant, supprimant, voire complétant par voie de conséquence les décisions
    antérieures.

    Le recueil des résolutions confédérales en vigueur est structuré selon deux
    axes principaux : **Fonctionnement** et **Orientations**.

    Chacun de ces axes comprend plusieurs catégories thématiques afin de favoriser
    tant que possible la lisibilité de l'ouvrage.

    Chaque résolution sera annotée d'un historique des différents Congrès
    confédéraux ayant apporté des modifications.

    Afin de conserver l'historique des différentes décisions de Congrès Confédéraux,
    l'intégralité des motions adoptées est portée en annexe selon la même
    classification, annotées des Congrès Confédéraux concernés.
    Une fois l'ouvrage mis à jour dans des délais raisonnables, il est porté à
    connaissance des syndicats par le Bureau Confédéral.

    Le CCN le plus proche valide l'ouvrage sur avis du Bureau Confédéral. »


Nous estimons que la motion de 2008 n’établit pas clairement la distinction
entre :

- Un ouvrage (recueil) qui vise à recenser et conserver l’ensemble des motions
  adoptées par les Congrès successifs de la CNT (mémoire / trace / inventaire) ;
- Et un ouvrage de synthèse **Fonctionnement & Orientation** dont la rédaction
  s’appuie sur les résolutions les plus récentes

Par ailleurs, le travail demandé pour l’élaboration de l’ouvrage de synthèse
**CNT - Fonctionnement & Orientations** s’avère conséquent.

L’effort de synthèse ne peut être placé sous la seule responsabilité des seuls
mandatés du :term:`Bureau Confédéral` au regard de l’ampleur du travail
ou de la collégialité de réflexion que cela nécessite.

Nous proposons donc, que ce travail puisse être réalisé par une commission de
travail confédérale associant plusieurs syndicats, commission placée sous la
coordination du Bureau Confédéral.

Un travail plus collectif garantira une meilleure collégialité dans la mesure
où l’épreuve de synthèse nécessite une manipulation du texte d’origine : les
motions de Congrès adoptées et en vigueur.

Enfin, même si ces éléments peuvent paraître évidents, il n’est fait nulle part
mention (motions ou dispositions statutaires) du fait que les résolutions
adoptées par le Congrès les plus récentes invalident les plus anciennes.

Nous proposons donc d’y remédier par une motion spécifique parmi 3
motions (à voter séparément) :
