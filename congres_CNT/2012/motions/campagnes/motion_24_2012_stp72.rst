
.. _motion_24_2012:
.. _motion_24_2012_stp72:

======================================================================================================
Motion n°24 2012 : Création d’un atelier confédéral permanent sur les revendications syndicales, STP72
======================================================================================================
  
.. seealso::

   - :ref:`motions_congres_stp72_2012`



Argumentaire
============

Nous sommes démuniEs actuellement pour proposer des revendications qui touchent
à des questions fondamentales comme le salaire ou la réduction du temps de
travail.

L’absence de réflexion collective conduit le Bureau confédéral à proposer seul
des revendications issues de luttes mais qui ne correspondent pas à des
perspectives révolutionnaires, ofensives et clairement anticapitalistes.

Si on prend l'exemple des retraites, se contenter de revendiquer le retour à
la retraite à 60 ans avec 37,5 annuités, alors que l'on sait que les salariéEs
en fin de carrière ne totalisent en moyenne que 36 voire 35 annuités, revient
à entériner le principe injuste de la décote.

De plus, ne pas dénoncer la division sexuelle du travail – l'assignation forcée
encore massive des femmes au travail domestique gratuit, ce qui a pour
conséquence l'inégalité salariale femmes/hommes sous toutes ses formes
aboutit à accepter de fait que les femmes aient des retraites moitié moindre
que celles des hommes.

Si le rôle social de chacunE est le même une fois en retraite, s'occuper de
son jardin qu'il soit potager ou intérieur, ouvert aux autres ou intime, que
l'on soit homme/femme, médecin/ouvrier, ne faut-il pas converger vers une
retraite uniforme, quelle que soit la chance que chacunE ait eu de pouvoir
cotiser pendant son temps professionnel ?

En période de crise, les attaques du patronat, obsédé par la récupération de
son taux de profit, redoublent d'intensité, et les syndicats réformistes
entretiennent l'illusion que camper sur des positions défensives permettra
d'éviter le pire. En tant qu'organisation révolutionnaire, nous savons qu'il
s'agit là de décourager les travailleurSEs en les privant des analyses et
perspectives qui seules peuvent générer l'élan pour une mobilisation
victorieuse.

**Plus que jamais, nous devons forger des revendications qui aient une portée
émancipatrice pour l'ensemble des classes laborieuses**.

Pour cela, nous devons  considérer et comprendre les antagonismes créés ou
exploités par le capital  entre diférentes catégories de travailleurSEs, et
maintenus pour maximiser  le profit et entraver la solidarité des oppriméEs :
antagonisme hommes /femmes,  travailleurs du centre/ de la périphérie des
puissances impérialistes.

**Nous devons chercher à nous approprier des outils théoriques, notamment d’origine
marxiste, féministe, décoloniale et renouer avec la richesse de l’hybridation à
l’origine de l’anarchosyndicalisme**.

Dans ce cadre, on pourrait donc par exemple étudier les apports de la
décroissance ou du courant de la démocratie radicale.

Bernard Friot
-------------

On pourrait aussi se nourrir des travaux de certains chercheurs comme
Bernard Friot, qui a été récemment invité aux Vignoles.

Ses apports sur le salaire socialisé sont **indispensables** pour élaborer une
critique révolutionnaire du salariat. Ses propositions, comme:

- le salaire à vie (et non pas un revenu garanti) basé sur une qualification
  personnelle définitivement acquise, donc déconnecté de l'emploi, d'où la
  suppression de l'emploi, du marché de l'emploi et des employeurs,
- **le financement de l'investissement productif par une cotisation**, au même
  titre que la maladie ou la vieillesse, donc la suppression des investisseurs
  privés et des banques, se placent dans une perspective d'extension
  généralisée de la part socialisée de la richesse produite qui est **une vraie
  attaque de la propriété des moyens de production**.

Motion
======

Création d’un atelier confédéral permanent sur les revendications syndicales.

Mis en place par une commission ou proposé par le futur organisme de formation
de la CNT, cet atelier doit permettre aux militantEs de s'approprier des outils
théoriques issus de divers champs des luttes sociales, afin de les aider à
construire des revendications réellement émancipatrices pour toutes les
catégories de travailleurSEs.

Il se chargera aussi de favoriser des rencontres avec des militantEs enracinéEs
dans diférentes cultures de lutte radicale, élargissant ainsi nos connaissances
mutuelles des multiples langages et formes de résistance à l'oppression.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°24          |            |          |            |                           |
| STP72                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
