.. index::
   pair: Motion 10 ; 2021

.. _motion_10_2021_etpic30:

================================================================================================================================================
Motion **n°10** 2021: **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie**  par **ETPIC 30**
================================================================================================================================================


Autres motions ETPIC30

    - :ref:`motions_etpic30`


Titre complet
=============

Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat,
l’homophobie & mandatement de la Commission Confédérale antisexiste
et antipatriarcale pour l’égalité et l’équité.


Argumentaire
=============

La CNT groupe des travailleurs et des travailleuses conscient·e·s de la lutte à
mener contre toutes formes de discriminations et d’oppressions économiques et
sociales en lien avec le sexe, le genre, ou les orientations sexuelles.

Par-delà la lutte contre l’exploitation capitaliste, la CNT fait le constat de
la persistance du patriarcat, du sexisme et des atteintes qui en découlent,
qu’elles soient physiques, sexuelles, matérielles, ou morales.

Les femmes et les personnes LGBT+, comptent encore et toujours, et très majoritairement,
parmi les personnes plus touchées.

Ainsi, et de prime abord, les militant·e·s de la CNT déclarent veiller à garantir
autant que possible à ses membres un soutien, une protection, et une défense
contre tous les agissements discriminatoires et les agressions sexistes et sexuelles,
individuels ou collectifs, susceptibles de leur porter atteintes.

La formation et l’accueil des nouveaux et nouvelles syndiqué·e·s intègrent cette
information et présentent cette volonté.

Dans l’œuvre revendicative quotidienne, la CNT poursuit la coordination des
efforts, la réalisation d’améliorations immédiates et la sensibilisation de
tou·te·s à la poursuite de la lutte pour l’égalité homme/femme.

**L’égalité des sexes ne peut se réaliser que par lutte contre le capitalisme
et son inclinaison patriarcale**.

Ainsi, la CNT soutient la lutte pour la suppression des discriminations sexistes
dans les domaines :

- Économiques : pour l’égalité salariale, pour l’égalité devant la retraite
  incluant les compensations propres au temps de congés maternité et au temps
  partiel subi, pour l’accès au logement, pour des prix des produits et des services non genrés ;
- Sanitaires et sociaux : pour le recours libre et gratuit à l’IVG, à la contraception,
  à l’écoute psychologique, aux soins gynécologiques, aux produits d’hygiène.
- De la vie quotidienne : oppressions, exclusions, violences, agressions sexuelles,
  injures
- Juridique : outre l’assistance et le conseil juridique des victimes déclarées
  et plaignant·e·s, les syndicats de la CNT verront à intégrer dans leurs statuts
  respectifs la question de la lutte contre les discriminations pour leur permettre
  de se porter le cas échéant partie civile.

Pour être à la hauteur des enjeux de lutte antisexistes et anti-patriarcales,
la CNT prolonge les travaux de sa **Commission antisexiste** qu’elle renomme
**Commission confédérale antisexiste et anti patriarcale pour l’égalité et l’équité**.

Une Commission de travail mixte source de propositions et de conseils
========================================================================


À l’image des syndicats qu’elle sert et qui la mandatent, du projet révolutionnaire
porté par la Confédération, et dans une logique inclusive de toutes les forces
disponibles et candidates, la Commission confédérale antisexiste et anti patriarcale
est mixte.

Cette Commission est invitée à produire un bulletin interne à la CNT de
réflexion et d’information en lien avec ses travaux, ses recherches, et
ses rencontres.

Elle réfléchit avant tout à l’édition d’un matériel spécifique (affiches,
autocollants, brochures) qu’elle proposera à la Confédération en vue d’actions
ciblées, nationales, internationales en lien avec l’actualité.

Elle étudiera de même les propositions émanant des syndicats et leurs Unions.

La CNT charge donc la Commission confédérale antisexiste et anti patriarcale
pour l’égalité et l’équité de:

- faire des préconisations pour améliorer l’accès aux droits des femmes plus
  particulièrement dans leurs combats contre la violence, la précarité et l’isolement ;
- proposer du matériel spécifique de lutte et des campagnes confédérales ;
- proposer du matériel de sensibilisation, et de réflexion ;
- Réaliser des études visant à éclairer les syndicats des phénomènes et processus
  sociologiques, économiques, et sociaux à l’appui de recherches théoriques,
  d’enquêtes , de sondages, et de témoignages ;

  Pour soutenir ses travaux, la Commission peut être amenée à proposer la non
  mixité qui permet notamment, entre personnes du même genre de libérer et de
  se réapproprier la parole, d’éviter la remise en question du vécu, de gagner
  du temps et de déterminer des priorités, etc.
  La non mixité est vue ici comme un moyen ou un outil, non comme une finalité ;

- proposer des éclairages et des moyens de créer des liens avec d’autres
  organisations en lutte pour l’égalité des sexes et contre les violences sexistes.


Concernant les problématiques d’agressions sexistes et sexuelles dans et en dehors de la CNT
=============================================================================================

Si la Commission peut recueillir la parole, sur les différentes problématiques
rencontrées, elle n’a pas pour vocation de se **poser en juge**.

En l’état de son développement, la CNT ne dispose d’aucun moyen suffisant pour
conduire des investigations.

À leurs demandes, la Commission peut conseiller les syndicats dans leurs
luttes pour l’égalité des sexes, sur les difficultés qu’ils pourraient rencontrer
en terme de discriminations, voire d’agressions sexuelles ou sexistes,
en interne comme en externe.

Elle peut émettre dans ce dernier cas un avis consultatif.


Amendements
===========

- :ref:`amendements_motion_10_2021_sanso69`
- :ref:`amendement_motion_10_2021_interpro07`
- :ref:`amendement_motion_10_2021_sipmcs`
- :ref:`amendement_motion_10_2021_ste72`
- :ref:`amendements_motion_10_2021_stp67`

Contre motions
=================

- :ref:`contre_motion_10_2021_cnt42`
- :ref:`contre_motions_10_2021_ste75`
- :ref:`contre_motion_10_2021_stics72`


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°10 2021 ETPIC30    |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
