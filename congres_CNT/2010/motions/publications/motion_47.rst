

.. _motion_47_2010:

======================================================================================
Motion n°47 2010 : Le Combat syndicaliste : abonnement intégré à la cotisation, STE 93
======================================================================================
  


Argumentaire
============
  
Depuis plusieurs années, les abonnements au mensuel confédéral stagnent voire
régressent. Pourtant, Le Combat syndicaliste est un outil de plus en plus
efficace tant pour ses récits de lutte, ses analyses syndicales, les notions
juridiques présentées ou encore son ouverture culturelle.

Entre les adhérents de la CNT et le journal, il y a une déconnection
dommageable.
Pour de nombreux militants isolés, pour les nouveaux adhérents - comme pour
ceux qui « décrochent » temporairement - le fait de recevoir chaque mois
le journal de l’organisation renforcerait le lien structurel avec la CNT.

La vocation du Combat syndicaliste n’est pas de faire du profit, mais d’être
lu et diffusé. S’il nous semble nécessaire d’abonner l’ensemble des adhérents,
il n’y a peut-être pas lieu d’augmenter les cotisations de 22 euros (coût de
l’abonnement annuel).

C’est à la CNT dans son ensemble de réfléchir au financement d’une plus large
diffusion de son mensuel. Il existe plusieurs pistes : en organisant des
fêtes du Combat syndicaliste dans les unions régionales, en lançant un
appel à souscription, ou encore en augmentant de façon minime la part de
cotisation reversée à la trésorerie confédérale (1 euro par mois).

Motion
======

Chaque adhérent de la CNT est abonné au Combat syndicaliste.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°47          |            |          |            |                           |
| STE-93               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`amendement_motion_47_2010_sipm_rp`
   - :ref:`amendement_motion_47_2010_etpreci35`
