
.. _amendement_motion_2_2021_sest_lorraine:

=================================================================================
Amendement N°2 à la Motion n°2 2021 **Antiproductivisme** par **SEST Lorraine**
=================================================================================

- :ref:`motion_2_2021_staf29`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`


Amendement
============

Suppression de la phrase qui est un slogan::

    Nous voulons des vies riches, pas de vies de riches.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°2  |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
