

.. _contre_motion_46_2012_ess34:

================================================================================================
Contre-motion à la motion N°46 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_46_2012_etpic30`



Argumentaire
============

Le seuil d’admissibilité d’une motion est de 50% de votes « pour » sur la base
des votes exprimés (pour, contre, abstention).

Les votes « ne prend pas part au vote » sont exclus du nombre des votes exprimés
servant de base à la définition de la majorité d'adoption d'une motion.

S'il y a plus de 25 % de « ne prend pas part au vote », la motion ne peut pas
être votée.

Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°2      |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_2_2012`
