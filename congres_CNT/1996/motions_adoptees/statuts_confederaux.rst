

.. _motion_statuts_confederaux_cnt_congres_lyon_1996:

=======================================================================================
Motions Statuts Confédéraux 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
=======================================================================================


Révision des statuts confédéraux
================================


Le syndicat ETPIC 94 concentre et coordonne toutes les propositions de
révision des statuts (motions des Congrès de 1981, 1983, 1985, ...1996 et
autres contributions en tenant compte notamment de la brochure de présentation
"Orientations et Fonctionnement"... ).

Les syndicats doivent envoyer leurs contributions avant Août 1997 (soit
10 mois après notre Congrès), ETPIC 94 aura alors deux mois pour les classer
et les envoyer au BC qui se chargera de tout faire redescendre dans les
syndicats, ces derniers auront alors six mois pour se positionner pour le
Congrès qui aura lieu six mois plus tard.

Les motions: III(1), III(5), III(6), III(7), III(9), V(34) sont donc renvoyées
à ETPIC 94.
