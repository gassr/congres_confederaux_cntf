
.. _motions_international_cnt_congres_lyon_1996:

==================================================================
Motions "international" du XXVe Congrès de la CNT à Lyon, 1996
==================================================================


Groupes de reflexion internationaux par branches professionnelles
=================================================================

Dans le cadre de l'alignement des politiques néo-libérales des divers états
européens, il est nécessaire de mettre en place des groupes de réflexion entre
des syndicats d’autres pays européens d'une même branche ( exemple :
coordination d’enseignants réfléchissant sur le modèle anglo-saxon à partir
de l’expérience des compagnons syndicalistes anglais) et ce dans le cadre de
l'AIT.


Contact avec le chiapas
=======================

Création au sein de la section française de l'AIT et, dans le cadre du
secrétariat à l'Internationale, d'un poste traitant de la situation au Chiapas
notamment dans le cadre d'une solidarité et de relations avec I'armée zapatiste
(EZLN) et sa structure politico-syndicale : le Front Zapatiste de Libération
Nationale (FZLN).

L'accentuation des liens déjà existants pourrait nous permettre d'envisager la
création d'une section de l'AIT au Mexique dans les années à venir.

Plusieurs autres sections de l'AIT se sont investies dans un important travail
de solidarité avec le Chiapas comme la FAU.
