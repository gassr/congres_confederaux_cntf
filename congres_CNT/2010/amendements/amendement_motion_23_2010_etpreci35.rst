
.. _amendement_motion_23_2010_ETPRECI35:


=====================================
Amendement à la motion 23 (ETPRECI35)
=====================================

Remplacement de «au cours du congrès, pas avant» par «à partir de la réception du premier
recueil de motions».

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°23 | 13         | 26       | 6          | 12                        |
| ETPRECI35                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+

- stic38, educ38 : NPPV


.. seealso::

   - :ref:`motion_23_2010`
