
.. _motions_bureau_confederal_cnt_congres_lyon_1996:

=====================================================================================
Motion Bureau Confédéral, 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
=====================================================================================

Organisation du Bureau confédéral
=================================

La CNT connaît depuis plusieurs mois un réel développement. La constitution de
nouveaux syndicats, les nouveaux lieux d'intervention des syndicats, modifient
progressivement les conditions d'exercice des taches confédérales.

Le rôle de coordination du bureau confédéral, la charge de circulation rapide
et massive de l'information augmente donc de manière significative.

Dans le même temps, les autres fonctions du bureau confédéral progressent
également : besoins de matériel de propagande, assistance et soutien lors de
réunions locales organisées en province, soutien logistique aux petits
syndicats, envois massifs de tracts aux contacts isolés, etc.

Dans ce contexte. il est fondamental que la CNT se donne les moyens internes
permettant aux différents rouages de l'organisation de bien fonctionner.

Notre communication interne se doit d'être aussi efficace que nos apparitions
externes ont su parfois l'être.

De plus, un bon fonctionnement des échanges internes est une condition de
démocratie syndicale. une condition à une une réelle autogestion du syndicat.

De ce point de vue, il est nécessaire d'adopter un partage le plus large des
tâches à accomplir, c'est en effet la seule solution viable pour un syndicat
qui revendique le fait de ne pas avoir et de ne pas vouloir de permanents.

Dans cette perspective, il semble nécessaire de bien dissocier les tâches afin
d'en organiser la répartition.

Nous pouvons recenser

- **Secrétariat confédéral** : réalisation des circulaires internes, soutien
  logistique aux nouveaux syndicats, relations avec les syndicats.
  Il gérera également la solidarité financière.
- **Trésorerie confédérale** : collecte des cotisations, paiements des factures,
  règlement des cotisations à l'AIT
- **Secrétariat aux contacts isolés** : C'est une fonction à créer sur le plan
  confédéral. Cela allège le travail du secrétariat confédéral. Il s'agit de
  répondre aux courriers de contacts isolés, parfois d'organiser des petites
  réunions, d'envoyer de la documentation, des tracts....
  Cela sera fait en liaison avec le secrétariat confédéral.
- **Secrétariat à la propagande** : Gestion des besoins en matière de matériel
  confédéral; assurer les tirages nécessaires, susciter la réalisation
  d'affiches tracts, répondre aux propositions reçues des syndicats.
  Peut aussi gérer la mise en place d'une politique éditoriale confédérale:
  brochures.
- **Gestions des stocks de matériel de propagande** : Un syndicat gère les
  commandes de matériel, assure le retirage du matériel, réalise les envois,
  Ceci est une décision du CCN d’Avril 1996.
- **Réalisation du bulletin intérieur (BI)** : Coordonner les envois faits au BI,
  assurer le tirage, les envois aux syndicats et abonnés isolés.
- Relations internationales : Une commission internationale regroupant autour
  du secrétaire à l'internationale, plusieurs compagnons, responsables par
  secteurs géographiques ou linguistiques, serait sans doute efficace.
  Il faudrait dans le budget confédéral prévoir un crédit pour les réunions
  ponctuelles de la commission.
- **CNT Infos national** : Ce bulletin d'informations permet de donner une
  vision globale de l'activité des syndicats et il est destiné en priorité
  aux contacts qui résident dans les départements gérés par le BC, mais les
  syndicats régionaux peuvent le reprendre pour leurs contacts.
  Rédigé en relation avec les syndicats.
- **Formation** : Un syndicat élabore et propose en lien avec les syndicats
  des thèmes d'éducation syndicale (voir motion sur la formation).
- **Soutien juridique** : Il s'agit de constituer des fiches techniques
  pouvant aider les syndicats : création d'un syndicat, recours à l'inspection
  du travail, contentieux aux prud'hommes, tribunaux administratifs....

De même, il serait nécessaire de se prononcer sur la nécessité de se constituer
une BIBLIOTHEQUE JURIDIQUE CONFEDERALE, dont l'inventaire serait remis à chaque
syndicat qui pourrait avoir des copies ponctuelles de tel ou tel point.

Il nous semble que rien ne justifie que toutes ces taches soient gérées par
des militants d'une même région (à l'exception peut-être du
secrétaire confédéral, du secrétaire à l'internationale et du secrétaire aux
contacts isolés).

Cette liste est peut-être incomplète, mais cela permet de fixer un premier
cadre (le BC pourra s'entourer d'une personne pour la circulation de
l'information (voir motion à ce sujet) d'une personne pour les contacts
Internet.... ou autre selon les besoins).

Cette approche nous semble conforme à notre projet autogestionnaire et à nos
choix fédéralistes Simplement il faudra ponctuellement dégager des moyens
financiers pour une rencontre du BC au pire deux fois par an, entre deux CCN.

Organiser cette répartition, c'est sans doute la seule voie pour répondre aux
nouvelles tâches de la CNT sans sombrer dans une  concentration des
responsabilités contraire à notre éthique et lourde de dangers bureaucratiques.

Il s'agit également d'une condition à la rotation des tâches.



Circulation de l’information
============================

De nombreux tracts, dessins, textes sortent dans toute la France signés par
la CNT.

Il n'est pas ici question de remettre la responsabilité de chaque syndicat
en cause, mais il est utile que chaque adhèrent puisse avoir connaissance de
tout ce qui est sorti par sa Confédération, et pas seulement par son syndicat,
question d'anti-corporatisme.

Il serait bien que chaque syndicat envoie à tous les autres syndicats
(par l'intermédiaire du bureau confédéral) un exemplaire de ses tracts.

Une personne sera chargée au bureau confédéral de la circulation de
l'information.
