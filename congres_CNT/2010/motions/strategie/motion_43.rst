

.. _motion_43_2010:

====================================================================================================================
Motion n°43 2010: Syndicalisation des travailleur-ses des collectivités territoriales, Santé-Social CT RP [rejetée]
====================================================================================================================


Argumentaire
============

Le dernier congrès de la fédération santé-social a décidé de retirer de son
intitulé  «collectivités territoriales». Suite aux discussions que nous
avons eu lors de ce congrès  avec les fédérations de la culture et du bâtiment
et suite aux propositions faites par le syndicat Interco 87 au dernier congrès
confédéral, notre syndicat propose de clarifier la manière dont les travailleur-ses
des collectivités territoriales se syndiquent à la CNT.

Motion
======

Les agents des collectivités territoriales adhèrent aux syndicats interprofessionnels
de leur aire géographique ou, quand ils existent, à ceux correspondant à l'activité du service
auxquels ils-elles sont rattaché-e-s.

Quand il y a création d'une section, celle-ci est rattachée au syndicat interprofessionnel
correspondant. Dans le cas de syndicat d'industrie, la solution est de faire dépendre
la section syndicale d’une union de syndicats, dont la couverture géographique
permet de réunir l’ensemble des agents d’une collectivité territoriale : UL pour les
municipalités, UD pour les Conseils départementaux, UR pour les Conseils régionaux.

Seuls les syndicats ayant des adhérents dans ladite section déterminent le fonctionnement
de celle-ci en accord avec les pratiques de la CNT.
Nous proposons qu'un point soit fait au cours des CCN afin de confirmer si ce
mode de fonctionnement est viable.

Vote indicatif
==============

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°43          | 22         | 15       | 22         | 6                         |
| Santé-Social CT RP   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Santé Social RP
	explication/présentation de la motion N°43

Etpreci 35
	travail dans les municipalités, plus cas posent pb pour champ de syndicalisation ; temps
	partiel. syndicat interpro de la fonction publique territoriale. syndicat régional, départemental.

STP72
	pour motion 43.

STICS 38
	Motion qui s'articule avec celle des fédérations. En plus de l'adhésion à des syndicats,
	possible d'adhérer à des fédérations.

Santé Social RP
	Il y avait un syndicat régional mais arrêté car considéré comme contre le
	fonctionnement de la CNT. Pas possible de faire des allers retours tous les deux ans.
	Pragmatiquement, syndicalisation par champ d'industrie apparaît pertinente et plus efficace.
	Interco Limousin: Correspond à motion de synthèse sur fédé d'industrie. Pourquoi donc autant
	d'abstentions ?

Educ13
	Abstention car compliqué à mettre en œuvre les unions de syndicats. Logique de regrouper
	syndicats ensemble.

Etpreci 35
	Comment on fait pour les personnes en temps partiel sur plusieurs champs ? Comment
	on fait avec les salariés de l'éduc employés maintenant par les CT ?

Interco 69
	rejoint l'éduc 13. Certains métiers des CT ne dépendent d'aucun autre champ
	(ex : état civil)

STP 67
	Important qu'il y ait une prise de décision. Plutôt pour une syndicalisation en interpro ou
	syndicat spécifique CT. Réticent à ce que chacun soit rattaché à son champ d'industrie. Réticent à la
	motion, mais pour l'adhésion aux interpros, avec adhésion à la fédé.

Interco Limousin
	section INRAP dispersée dans de multiples syndicats, c'est la fédé qui a déclaré
	la section. Une seule section déclarée sur le lieu de travail, contrôlée par l'union locale. Plusieurs
	statuts à l'INRAP, mais même métier, pas question qu'un archéo CT se syndique dans un syndicat
	CT.

SS-RP
	d'accord avec la CNT 87, pour l'INRAP. Le problème, c'est les CT genre mairie ou conseil
	général, tout est interne aux CT. La section doit regrouper tous ceux qui appartiennent au même
	employeur.

Vote définitif
==============

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°43          | 30         | 12       | 18         | 10                        |
| Santé-Social CT RP   |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion n°43 rejetée.**

Santé Sociale - RP
   ça va être compliqué qu'aucune motion ne soit adoptée, on ne sait pas
   vraiment comment faire.


.. seealso::

   - :ref:`contre_motion_43_2010_stp72`
   - :ref:`contre_motion_43_2010_etpreci35`
