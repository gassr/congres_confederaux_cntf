.. index::
   pair: XXVIe Congres ; CNT 1998 (Paris)

.. _congres_CNT_1998_paris:

================================================================
Le **XXVIe Congrès CNT 1998 de Paris** du 5 au 6 Décembre 1998
================================================================




.. _syndicats_presents_1998_paris:

Nombre de syndicats présents, Paris 1998: 40
============================================

40 syndicats présents à jour de cotisations.


Motions adoptées
================

.. toctree::
   :maxdepth: 2


   motions_adoptees/motions_adoptees


.. seealso::

   - :ref:`congres_CNT_2001_toulouse`
   - :ref:`congres_CNT_1996_lyon`
