
.. _motion_32_2012:
.. _motion_32_2012_etpic30:

=======================================================================================================================================================================================
Motion n°32 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Développement coopératif et prestations techniques externes ETPIC 32  Sud
=======================================================================================================================================================================================
  
.. seealso::

   - :ref:`etpic30`
   - :ref:`motions_etpic30_2012`




Motion
=======

La CNT privilégie deux axes principaux d’intervention juridique distincts:

1 - Constitution de commissions permanentes d’entraide juridique
-----------------------------------------------------------------

La CNT met en place une commission juridique et de formation confédérale.
Elle est composée d’un comité de coordination de mandaté.e.s et personnes
ressources de diférentes régions et de diférents secteurs d’activités
(du public comme du privé).

Cette commission aura à charge de conseiller, voire assister, l’ensemble des
structures de la CNT et de leurs syndiqué.e.s sur les questions juridiques
(droit syndical, droit du travail).

Elle n’a pas pour vocation de suppléer à l’action et l’investissement des
syndicats eux-mêmes.

Dans l’intérêt de la Confédération, ou pour des afaires recouvrant un enjeu
confédéral, elle est directement sollicitée et mise à contribution.

En ce sens, elle recense et archive l’ensemble des afaires juridiques de
nature à assurer la défense des intérêts de la CNT, de ses syndicats et de
leurs Unions.

La commission juridique confédérale développe un axe de formations relatives
au droit du travail et au droit syndical en fonction des demandes et des
besoins des syndicats et des fédérations.

Elle publie également des brochures thématiques. Elle rend compte de ses
activités à chaque CCN. Elle se dote de moyens de communications adaptés et
bénéficie de la possibilité d’acquérir toute documentation utile.

La commission juridique confédérale étudie le rattachement de la CNT
d’organismes de formation habilités sur le plan national ou local à
recevoir des camarades sur leurs droits à la formation.

En vertu d’une démarche d’entraide et de solidarité interprofessionnelle,
le Congrès invite de même chaque Union régionale à se doter de leur propre
commission juridique.

Elles ont pour vocation d’assurer au plus près conseils et assistances à
l’ensemble des syndicats des Régions et de mutualiser les compétences locales.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°32          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_32_2012_etpics57`
- :ref:`amendement_motion_32_2012_interpro31`
