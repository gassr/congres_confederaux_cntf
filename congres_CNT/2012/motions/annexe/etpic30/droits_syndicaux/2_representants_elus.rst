



========================================
II - Les représentants élus ou désignés
========================================

A - Dans la fonction publique
=============================

Les représentants syndicaux élus font office de représentants du personnel.
Les agents titulaires du secteur public (Fonction publique d’Etat, fonction
publique territoriale, fonction publique hospitalière) élus représentants du
personnel aux Commission Administratives Paritaires (CAP), aux Commissions
Techniques Paritaires (CTP) et aux Commission d’Hygiène et Sécurité (CHS)
peuvent bénéficier des Autorisations Spéciales d'Absence (ASA) d'une durée
égale au double de la durée de la réunion à laquelle ils participent,
auxquelles peuvent s'ajouter les délais de route.

Ces ASA de représentation sont indépendantes des droits d’ASA accessible à
chaque agent syndiqué (mais cumulables).


.. index::
   pair: Délégués syndicaux; Heures de délégations

B – Dans le privé
=================

Les délégués syndicaux
----------------------

Un délégué syndical représente son organisation syndicale au sein de son
entreprise (dans le cadre des négociations notamment). Il est désigné par
celle-ci.

Il bénéficie de 20 heures maximum d’heures de délégations par mois, selon
l’effectif de l’entreprise.

========================  ==============================
Effectif de l’entreprise  Nombre d’heures de délégations
========================  ==============================
50 à 150 salariés         10 heures
151 à 500 salariés        15 heures
Au-delà de 500 salariés   20 heures
========================  ==============================

Les représentants du personnel
-------------------------------

Les représentants du personnel pour mener à bien leurs missions bénéficient
d’heures de délégations.
Elles se répartissent comme suit pour chacune des  instances :


Les élus représentants du personnel au Comité d’Entreprise (CE)
----------------------------------------------------------------


Les représentants syndicaux au CE (entreprises de + de 500 salariés)
--------------------------------------------------------------------


Les élus représentants au Comité d’Hygiène et Sécurité et des Conditions de Travail (CHSCT)
-------------------------------------------------------------------------------------------
