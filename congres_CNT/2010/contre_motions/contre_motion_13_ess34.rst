

.. _contre_motion_13_2010_ess34:

=======================================
Contre-motion à la motion N°13 (ESS 34)
=======================================


Sauf en cas de carence de candidatures, toute personne déjà en charge d'un
mandat syndical n'est pas habilitée à en briguer un autre.

Par mandat, on entend les secrétaires et trésorier­es des
structures syndicales : syndicats, UL, UD, UR, fédérations) et mandaté­es du BC.


+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°13 |            |          |            |                           |
| ESS 34                         |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+


.. seealso::

  - :ref:`motion_13_2010`
