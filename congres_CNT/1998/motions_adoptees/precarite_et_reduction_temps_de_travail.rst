
.. _motion_precarite_cnt_congres_paris_1998:


=========================================================================
Motion précarite 26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
=========================================================================




Sur la negociation de la duree et de l'organisation du travail
==============================================================


Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 43         | 0        | 0          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Pour toutes les négociations portant sur la durée et I'organisation du travail,
qu'elles s'exercent dans le cadre de la loi sur les 35 heures ou dans celui du
droit commun de la négociation, là où elle en a les moyens, la C.N.T. imposera
la plus grande réduction possible de la durée du travail des embauches au
prorata de cette réduction du temps de travail.

Là où elle se trouvera en concurrence avec des syndicats prêts à céder aux
exigences patronales et à signer pour leur propre intérêt au mépris de celui
des travailleurs, la C.N.T. se battra pour que l'accord soit le plus favorable
possible aux salariés.

Pour ce faire, elle militera pour la tenue d'une assemblée générale qui
mandatera les négociateurs.

La C.N.T. proposera à l'assemblée générale, qu'ils le soient sur des
revendications telles que :

- Une réduction de la durée du travail qui conviendra le mieux aux salariés,
  et dans l'intérêt collectif des travailleurs, en heures quotidiennes, en
  jours hebdomadaires (semaine de quatre jours) ou en congés payés
  supplémentaires ;
- Le maintien de l'intégralité du salaire de base mensuel sur 39 heures ;
- L'embauche sur la base du salaire de 39 heures et non 35 heures par exemple ;
- L'embauche sous contrat à durée indéterminée et à temps complet sauf
  exceptions liées à certaines professions comme dans le spectacle ;
- L'interdiction du recours à la sous-traitance et au travail intérimaire pour
  compenser la réduction du temps de travail ;
- L'embauche dans le mois qui suit la signature de l'accord ;
- Le maintien des effectifs pendant toute la durée de l’accord ;
- Le refus de l'annualisation de la durée du travail ;
- L'interdiction du travail de nuit et du travail en équipe sauf exceptions
  liées au service du public (transports, hôpitaux...) et dans ce cas la mise
  en place d'une surveillance médicale constante des salaries concernés ;
- Le contingentement au strict nécessaire des heures supplémentaires ;
- Le rejet de toute dérogation conventionnelle, à la baisse, de l'interruption
  journalière légale de deux heures comme du repos quotidien légal
  de onze heures.

En ce qui concerne les salaries contraints à un temps partiel, la réduction
du temps de travail des salaries à temps plein devra permettre l'allongement
de leur durée du travail, s'ils le veulent, avec conséquemment l'augmentation
de leur salaire.

Il ne s'agit là que d'exemples de revendications à modifier, compléter ou
adapter en fonction des professions et des activités exercées.

La C.N.T. tentera d'imposer un contrôle permanent de l'application de l'accord
par l'assemblée générale des travailleurs.

Il s'exercera par les délégués syndicaux sur toutes les décisions de
l'employeur et fera l’objet d'un compte rendu devant l'assemblée générale
qui donnera des mandats en conséquence à ses représentants.
