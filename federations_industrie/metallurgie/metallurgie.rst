
.. index::
   pair: Fédération ; Métallurgie
   ! Fédération Métallurgie


.. _federation_metallurgie:

===============================
Fédération de la métallurgie
===============================


Contact
========

:Adresse électronique: federation.metallurgie@cnt-f.org


Tracts
=======

.. toctree::
   :maxdepth: 3

   tracts/tracts
