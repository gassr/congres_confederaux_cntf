
.. _amendement_motion_3_2014_interco25:

================================================
Amendement à la motion n°3 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_3_2014_sinr44`

Argumentaire
=============

Des syndicats viennent au congrès, avec des positions sur tous les points à
l'ordre du jour, mais doivent parfois repartir avant la fin.

De quel droit les priverait-on de la possibilité de faire valoir ces positions,
aussi légitimes que celles des autres, en leur interdisant de confier leur
mandat à des délégué-e-s d'autres syndicats ?

En outre il est parfois nécessaire de fonctionner comme ça pour traiter des
points importants en fin de congrès, sans ça ils passent à la trappe.

Amendement
===========

Remplacement du dernier paragraphe comme suit::

    "Un syndicat donnant procuration doit le signaler lui-même, soit à la
    commission d'organisation du congrès (avant le congrès), soit au
    congrès directement (en cas de départ avant la fin du congrès)."
