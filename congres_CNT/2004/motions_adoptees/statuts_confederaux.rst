
.. _motions_statuts_confederaux_cnt_congres_saint_denis_2004:

===========================================================================================
Motions Statuts Confédéraux 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
===========================================================================================

Refonte des statuts
===================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 27         | 13       | 9          | 8                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


1. le congrès rejette les décisions du congrès extraordinaire de Lille
   concernant les statuts confédéraux.
   Hormis les modifications nécessaires en rapport avec le retrait du
   sigle AIT, qui découlent des motions adoptées au congrès de Toulouse.
2. le congrès affirme sa volonté de mettre, dés aujourd'hui, réellement et
   pleinement en application les statuts de 1946, modifié en 1949 et 2003
   (cf 1 : dernières modifications concernant l'AIT).
3. en fonction des difficultés qui apparaîtront lors de leur pleine mise en
   application, la CNT réfléchira, avec prudence et méthode, sur la base de
   motions argumentées des syndicats, à des modifications ponctuelles et
   pratiques, ou à l'ajout ou au retrait de certains articles, si nécessaire.
4. la CNT se dote "d'un règlement annexé aux statuts ", qui précise et
   complète, lorsqu'elle le juge nécessaire, par voie d'accords de congres,
   les modalités d'interprétation et de mise en application de certains aspects
   des présents statuts.

Poursuite du travail entrepris
==============================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 27         | 12       | 16         | 18                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Continuer le travail entrepris au congrès extraordinaire de Lille dont nous
soutenons les conclusions

Cependant la très courte majorité ayant permis l'adoption de la modification
de certains articles des statuts pouvant prêter à polémique, les statuts
adoptés devront être réexaminés dans le sens de la recherche d'un plus
grand consensus.
