
.. index::
   pair: Motion 22 ; 2014

.. _motion_22_2014_etpic30:

==============================================================
Motion n°22 2014 Combat syndicaliste en ligne (ETPIC 30 sud)
==============================================================

.. seealso::

   - :ref:`motions_etpic30`

Argumentaire
=============

Le Combat syndicaliste connaît des évolutions qualitatives importantes
ces dernières années mais sa diffusion reste trop limitée.

La vocation de l’organe mensuel de la CNT est de connaître une diffusion
la plus large possible, comme outil de presse et de propagande.

Parallèlement, le web devient un outil incontournable de la diffusion
de l’information.

Si nous soutenons le maintien de la version papier du Combat Syndicaliste
pour des raisons d’accessibilité à tous et toutes, nous souhaitons
néanmoins que la CNT étudie la mise en oeuvre d’un site web spécifique
dédié au Combat syndicaliste du type : http://www.Combat-syndicaliste.org.

Ce nom de domaine devrait être actuellement saisissable techniquement
par la CNT.

Motion
======

Le Congrès désigne une commission confédérale chargée de réaliser la
mise en ligne internet de l’édition mensuelle du Combat Syndicaliste.

La commission confédérale ``CS en ligne`` est chargée d’étudier la
faisabilité et l’élaboration de la mise en ligne du Combat Syndicaliste
sous le nom de domaine www.Combat-syndicaliste.org (ou autre si
indisponible) dans les délais les meilleurs.

L’ensemble des articles du Combat Syndicaliste y seront accessibles
gratuitement selon une mise en page spécifique.

La question de l’interactivité potentielle (commentaires des articles,
prises de contact) fera l’objet d’une étude spécifique.

L’ensemble des travaux de la commission sont placés sous le contrôle
et la validation des CCN qui suivent le Congrès.

Jusqu’au prochain Congrès Confédéral, les CCN pourront observer les
candidatures et le mandatement de webmasters confédéraux chargés de
l’animation du site web dédié au Combat Syndicaliste.

Les internautes et lecteur du CS en ligne seront invités sur ce site
internet (et sur le site web confédéral) à soutenir financièrement
le Combat Syndicaliste.

Amendement
==============

- seealso::

  - :ref:`amendement_motion_22_2014_interco25`

Contre-motions
===============

.. seealso::

   - :ref:`contre_motions_22_23_24_2014_sipmcs`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°22 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
