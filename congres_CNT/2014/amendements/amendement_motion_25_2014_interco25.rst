

.. _amendement_motion_25_2014_interco25:

================================================
Amendement à la motion n°25 2014 (Interco25)
================================================

.. seealso::

   - :ref:`motion_25_2014_etpic30`




Argumentaire
=============

Pour éviter tout fonctionnement vertical.

Amendement
==========

Ajouter à la fin de l'avant dernier paragraphe la phrase suivante :
"En aucun cas une commission ne pourra signer un texte ou une publication
au nom de la CNT, à moins qu'un congrès ne l'ait explicitement mandaté pour
le faire."

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°25 2014 <motion_25_2014_etpic30>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
