

.. _motion_4_2008_etpic30:
.. _motion_4_2008:

===========================================================================================
Motion n° 4 2008 de l’ETPIC 30 : recueil des résolutions confédérales en vigueur [adoptée]
===========================================================================================

.. seealso::

   - :ref:`motions_etpic30`





Demande de modification par la motion 61 2012 ETPIC30
=====================================================

- :ref:`motion_61_2012`

**ESS 34**
	les personnes à la tribune ne devraient pas parler au nom du syndicat.

**ETPIC 30 (Manu à la tribune)**
	J’interviens parce que mes camarades m’ont demandé de le faire pour la
	présentation de la motion du fait de leur présence depuis peu à la CNT
	mais aussi parce que le recueil est un ouvrage que j’ai réalisé pour la
	confédération, sur mandat de la confédération.


Présentation de la motion
=========================

C’est une trace dans l'histoire de la CNT, risque d'oublier ce que l'on décide
et ce que l'on fait.

Aujourd'hui, difficile de rester dans une logique d'inventaire qui supposerait
que des motions nouvelles qui contredisent les anciennes...
Pas suffisant.

Il faut créer une synthèse **fonctionnement & orientation** en  plus du recueil
ou associé au recueil.

**Educ 35**
	amendement a cette motion. Mandat éminemment politique. On pense que c'est
	plutôt à une commission de faire ce travail, plutôt qu'au BC...

**SCIAL RP**
	vote contre. Pas de précision sur les bases de la sélection...Flou artistique.
	On estime qu'il n'y a qu'un Congrès qui peut faire ce genre de travail.


**ETPIC 30**
	est ce que c'est possible pour vous en CCN (réponse non)


**ETPIC 94**
	d'accord avec amendement du 35. On pense que le BC n'a pas à donner son
	avis là dessus, mais que c'est plutôt le rôle d'une commission.

**Educ 91**
	d'accord avec le 94. Recueil des motions est nécessaire et difficile à
	établir.
	**Nécessaire que tout nouveau syndicat/syndiqué puisse consulter
	ce recueil et percevoir les prises de positions troubles ou
	contradictoires**.

**42**
    contre la motion, l'amendement, la réécriture des motions.

**ESS 34**
	ok avec l'ensemble. Si la commission de 5 personnes est décidée, il faut
	qu'à l'issue du congrès, il y ait une commission de 5 personnes.


**Educ 35**
	se propose pour participer à la commission.


**ETPIC 30**
	On ne veut pas faire disparaître les motions adoptées, on voudrait faire
	émerger un nouveau document de synthèse.

**ETPIC 94**
	Proposition que le CCN valide l'ouvrage et qu'il soit confirmé au
	prochain congrès. Peut-être qu'un an, un an et demi seraient nécessaire.


**CS RP**
    On souhaite au moins 5, ou plus...


Modifications proposées
========================

"A l'issue de chaque congrès"...
"Par une commission de plusieurs syndicats différents mandatés par le Congrès
confédéral"

"validation par le CCN le plus proche"


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 36         | 4        | 3          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


**Pour : 36**
	Santé Social 86, Santé social RP, SSE
	31, Interco 07, ETPIC 94, Interco 87,
	Interco 86, Interco 45, Interco 33,
	Interco 48, Interco 29, ETPIC 30,
	Interco 31, Interco 32, Interco 91,
	Interco 34, Interco 09, Interco 38,
	Interco 21, SSEC 59, éduc 66, éduc
	33, STE 92, ESS 34, SUTE 69, éduc
	38, éduc 35, STE 93, SIPM RP, STTE
	33, SII 75, STIS 59, SUB 69, PTT 35,
	STTE 34 (1 syndicat non identifié)

**Contre: 4**
	éduc 42, Interco 42, santé social 42 (1
	syndicat non identifié)

**Abstention: 3**
	SUB 35, éduc 91 (1 syndicat non
	identifié)

NPPV: 3
    éduc 57, Interco 54, PTT 66


MOTION adoptée avec modifications

Une commission est créée avec comme membres:

- :ref:`etpic30`
- PTT 35
- Educ 35
- :ref:`etpreci35`
