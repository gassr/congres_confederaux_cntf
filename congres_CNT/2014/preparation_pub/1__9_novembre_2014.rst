

.. _courriel_9_novembre_2014_pub:

==============================================
Courriel du 9 novembre 2014
==============================================

::

    Sujet : Circulaire n°11 : cahier des mandatéEs, orga 33ème Congrès, Solidarité Fouad ...
    Date :  Sun, 9 Nov 2014 17:16:24 +0100
    De :    CNT Secrétariat Confédéral <cnt@cnt-f.org>


CherEs camarades,

C’est parti, le Congrès confédéral aura bien lieu les 12, 13, et 14 Décembre
2014 à Angers, sur l’espace Ethic Etapes dans le parc de loisirs
du Lac de Maine.

L’organisation générale est assurée par nos camarades de la CNT 49 et de l'UR
BZH et PDLL, Vous pouvez contacter les camarades chargés de l’organisation via
le mail ptt49@cnt-f.org ou bretagne@cnt-f.org.

Le Congrès débutera le vendredi 12/12 2014 à 14h précise.
