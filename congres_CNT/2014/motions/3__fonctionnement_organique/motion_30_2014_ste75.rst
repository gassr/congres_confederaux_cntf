
.. index::
   pair: Motion 30 ; 2014

.. _motion_30_2014_ste75:

==================================================================
Motion n°30 2014 : Caisse de solidarité (simplification) (STE 75)
==================================================================

.. seealso::

   - :ref:`motions_ste75`





Argumentaire
=============

Il est important que la caisse de solidarité confédérale existe.
Il serait important de la pérenniser et de la rendre plus transparente.

Aujourd'hui, il existe trois règles qui visent à l'abonder, ce qui amène
une légère incohérence.

Il y a un article statutaire et deux motions de règles organiques.

Article 22 : Cette Caisse est alimentée par les timbres solidarité et
la vente des cartes. Deux timbres par an sont obligatoires.
Chaque syndiquéE peut en prendre facultativement autant qu'il lui plaît.
Le montant du timbre solidarité est fixé par le Congrès. Les fonds sont
inscrits au compte « Caisse de Solidarité ».

En complément de cet article, dans le recueil des résolutions se trouve
une motion qui indique::

    « Les sommes collectées par la vente du timbre confédéral de cotisation
    du mois de janvier sont affectées à la caisse de solidarité. »

Et en plus, au Titre V, une règle organique vient préciser la répartition
budgétaire de la cotisation confédérale:

- Timbre standard : 2,60 € avec 0,50 € pour l’international, 0,50 € pour
  solidarité
- et 1,60 € pour la part confédérale, pour le timbre précaire : 1,20 €
  avec 0,30 € pour l’international, 0,30 € pour solidarité et 0,60 €
  pour la part confédérale.

Donc, on arrive avec une règle de répartition où la caisse de solidarité
est alimentée avec deux timbres, plus le timbre de janvier et une
partie des reversements mensuels.

Outre la complexité de cette répartition, il semble que cela fasse une
grosse contribution qui ne soit pas utile.

Nous proposons donc d'abroger ces deux règles supplémentaires et de ne
garder que la règle statutaire.
Bien évidemment, la prochaine trésorière confédérale devra être vigilante
sur le niveau de budget avec cette nouvelle règle et alerter le Bureau
confédéral et le CCN suivant en cas de soucis.


Motion
======

Abrogation des motions sur les versements auprès de la caisse de
solidarité.

Règle de répartition selon l'article 22 des statuts : deux timbres
confédéraux sont consacrés à la caisse de solidarité (ceux de janvier
et juillet, début de semestre).

Pour les autres timbres, la répartition se fait comme suit :

- Timbre standard : 2,60 € avec 1 € pour l’international et 1,60 €
  pour la part confédérale,
- pour le timbre précaire : 1,20 € avec 0,60 € pour l’international
  et 0,60 € pour la part confédérale.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°30 2014

       :ref:`STE75 <ste75>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
