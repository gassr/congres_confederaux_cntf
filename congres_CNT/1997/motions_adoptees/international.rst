
.. _motion_international_cnt_congres_paris_1997:

========================================================================
Motion "international" du Congrès extraordinaire de la CNT à Paris, 1997
========================================================================

Définition de la politique confédérale en matière de relations internationales
==============================================================================

Suite à **l'exclusion** de notre Confédération de l'A.I.T. lors du 20ème
Congrès  de l'A.I.T. de Madrid fin 1996.


Vote
----

40 syndicats présents
Adoptée suite à référendum
25 syndicats ont répondu pour plusieurs propositions de textes
21 syndicats ont répondu positivement à cette motion.


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 20         | 10       | 7          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

**La CNT-F ne reconnaît pas la décision du Congrès de Madrid.**

En conséquence, elle entend poursuivre son travail internationaliste en liaison
avec les sections de l'A.I.T. qui le souhaitent et, en relation avec les
syndicats présentant des caractéristiques de rupture avec le capitalisme et
l'Etat et agissant hors de l'A.I.T.
