
.. _motions_13_14_2012:

=============================================================================================
Motions n°13,14 2012 : Orientation stratégique et moyens de développement ETPICS 57 Nord Sud
=============================================================================================
  
.. seealso::

   - :ref:`etpics_57_pub`
   - :ref:`motions_etpics57_2012`




Argumentaire
============

Nous sommes actuellement frappéEs par une crise systémique du Capitalisme.
Partout sur le globe celui-ci se réorganise. Il vise notamment à uniformiser,
vers le bas, les conditions de vie des travailleurs et d'accroitre ses marges.

Pour empêcher l'aggravation des conditions de vie des travailleurs, la CNT doit
se doter d'un plan de bataille, tant en interne qu'en externe. Ce plan doit
viser d'une part à élargir notre base militante et développer nos revendications.
Pour ce faire, il appartient à chaque syndicats de la Confédération de faire
le travail de propagande nécessaire à cet effet en s'appuyant sur les
fédérations d'industries notamment.

En second lieu, nous pensons que les conditions historiques qui ont vu se
matérialiser la Charte de Paris en 1946, ont évolué. Il y a un grand
chambardement dans toute la société. Les cadres structurants de la Classe
ouvrière ont changé. Les conditions de nos relations sur le terrain avec les
autres organisations du mouvement ouvrier ont totalement évolué.

Ainsi il n'est pas rare que de voir des équipes de la CNT travailler ou
non avec telle ou telle structure. Il est donc une réalité : nous ne devons
pas être isoléEs et dans le même temps, nous ne devons pas être sous la coupe de
qui que ce soit. La CNT a ses spécificités aux même titre qu'elle porte des
revendications convergentes avec d'autres organisations syndicales, politiques
ou associatives (hausse des salaires, lutte contre la précarité, régularisation
de tous les sans papiers, lutte contre la répression, etc).

Nous invitons le congrès à mettre en place une commission spécifique sur le sujet
de la stratégie afin d'aboutir à des compléments et à une synthèse, et ainsi
faire en sorte que la CNT soit en ordre de bataille afin d'affronter l'État et
le Capital.

À ce titre, nous proposons donc deux motions à voter séparément.



.. _motion_13_2012:

Motion 13 : Orientation stratégique et mouvement social (ETPICS57)
==================================================================
.. seealso::

   - :ref:`etpics_57_pub`
   - :ref:`motions_etpics57_2012`


Le congrès donne mandat au Bureau confédéral afin de se mettre en lien avec
toutes les structures du mouvement social.

Ce travail vise d'une part à mettre sur place une plate forme revendicative
unifiante qui permettrait de développer une propagande et des actions unitaires.

Il vise à aboutir à un vaste mouvement social nous permettant de gagner sur les
revendications des travailleurs et à améliorer nos conditions de vie.

Cela serait une base pour le développement de la grève générale.

L'objectif pour la CNT est de déborder l'intersyndicale réformiste et d'offrir
ainsi un cadre structurant à toutes les équipes militantes qui cherchent à
affronter, sur le terrain, par l'action directe, par l'auto-organisation des
travailleurs, l'État et le Capital.

Il offrirait un pôle de recomposition  alternatif aux équipes d'autres OS
ainsi qu'aux collectifs de lutte et  associations populaires afin de
coordonner leurs actions.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°13          |            |          |            |                           |
| ETPICS57             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. _motion_14_2012:

Motion 14: Orientation stratégique et moyens de développement (ETPICS57)
=========================================================================

.. seealso::

   - :ref:`etpics_57_pub`
   - :ref:`motions_etpics57_2012`
   - :ref:`motion_45_2010`

Lien avec la motion 45 2010 Culture-Spectacle RP
================================================

.. seealso::

   - :ref:`motion_45_2010`

La motion
==========

Dans le même temps, la CNT se donne les moyens d'amplifier son développement.

**À ce titre, elle cherche à accroitre son implantation dans la jeunesse**.

Matériel spécifique (accès à l'emploi, au logement, à l'indépendance, aux
études, à la sexualité, à la culture...), tournée de propagande dans les
villes universitaires afin d'inculquer l'histoire des luttes et des sections
CNT dans les facs couplé à un travail à destination des FJT.

Dans un second temps, elle demande aux fédérations d'industries constituées de
développer du matériel de propagande (affiches de diférents formats, tracts de
présentation et 4 pages revendicatifs).

Ceci fait, le congrès demande aux syndicats dits Interco et aux Union de
syndicats d'organiser, en appuis aux syndicats concernés constitués, l'action
nécessaire (réunion publique, action directe, tractages et collages
réguliers) au développement de la CNT.

L'idéal serait de mettre en place ses actions de façon coordonnée sur tout le
territoire afin de donner une visibilité nationale à la CNT.

Le secrétariat à  la propagande proposé par les camarades de l'ETPIC 30 Sud
se prête parfaitement à cette tâche.

Le congrès donne mandat au Secrétariat international afin qu'il relaye à nos
organisations sœurs nos initiatives et qu'elles puissent ainsi trouver ensemble
les moyens de coordonner nos actions.

Le congrès met en place une commission de travail spécifique sur le sujet de
la stratégie afin d'aboutir à des compléments et à une synthèse, et ainsi faire
en sorte que la CNT soit en ordre de bataille afin d'afronter l'État et le
Capital.

Cette commission prend pour base de travail toutes les motions  d'orientations
:ref:`stratégiques <strategie_syndicale_congres_2010>` du Congrès de Saint-Étienne ainsi que celle ci.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°14          |            |          |            |                           |
| ETPICS57             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
