
.. _amendement_motion_15_2014_sse31:

================================================
Amendement à la motion n°15 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_15_2014_interco71`




Argumentaire
=============

Nous souhaiterions scinder cette motion en deux points pour qu'ils soient
votés séparément

Amendement
===========

15-a
-----

La CNT est opposée à la mise en place de la Mutuelle obligatoire.

15-b
-----

La CNT apportera son aide militante, juridique et financière à ses
adhérents et syndicats qui refusent que la santé soit cotée en Bourse.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°15 2014 <motion_15_2014_interco71>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
