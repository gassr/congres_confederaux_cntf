
.. _amendement_motion_17_2010_stea:


====================================
Amendement à la motion 17 (stea)
====================================


Dans la motion, la mention «dans les trois villes différentes» est supprimée
et on y ajoute « Les syndicats isolés dans les régions non constituées en UR
sont rattachés à l'une des UR les plus proches ». (suppression du 1) dans
la motion 17)


Vote indicatif
==============


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°17 | 14         | 28       | 6          | 10                        |
| stea                        |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_17_2010`
