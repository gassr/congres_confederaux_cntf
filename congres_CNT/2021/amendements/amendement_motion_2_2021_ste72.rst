
.. _amendement_motion_2_2021_ste72:

=============================================================================
Amendement N°3 à la Motion n°2 2021 **Antiproductivisme** par **STE72**
=============================================================================

- :ref:`motion_2_2021_staf29`

Autres motions STE72

    - :ref:`syndicats:motions_ste72`

Argumentaire 
============

Nous pensons que la motion peut être plus concise, sans pour autant rogner
sur le fond. Par ailleurs nous ne sommes pas forcément très convaincues
par l'utilisation du concept de ``nature``.

Amendement 
============

Les syndicats constituant la CNT visent - par les textes qu’ils publient, -
par les formations qu’ils organisent, - par les luttes sociales où ils agissent,
à déconstruire l’imaginaire productiviste et compétitif et ses corollaires :
l’exploitation salariale, toute forme de travail contre rémunération et
l’idéologie du consumérisme.

Les syndicats inscrivent leurs réflexions et leurs actions d’une part
dans une volonté d’émancipation culturelle des travailleurs et des travailleuses,
et d’autre part, dans une volonté farouche de protéger les équilibres
naturels écosystèmes.

.. figure:: ste72/amendement_2.png
   :align: center

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°2  |            |          |            |                           |
| STE72                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
