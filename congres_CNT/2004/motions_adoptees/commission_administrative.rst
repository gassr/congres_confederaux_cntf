
.. _motion_commission_administrative_cnt_congres_saint_denis_2004:

=================================================================================================
Motion Commission Administrative, 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=================================================================================================

Vote
====


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 37         | 8        | 18         | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
=====

Cette commission est créée à titre provisoire, En effet, elle modifie la
composition statutaire de la commission administrative, et ne **pourra être
intégrée définitivement dans les statuts que lors du prochain congrès**::

	Article 28. - Les présents statuts ne peuvent être modifiés que par un
	Congrès à condition que le texte des modifications ait été porté à la
	connaissance des Syndicats trois mois à l'avance et six mois en ce qui
	concerne les articles 7 et 8"


1. Dans l'esprit des statuts de 1946, activation d'une
   commission administrative (C.A) de la CNT, composée d'un mandaté par union
   régionale.
   Cette C.A n'est pas un deuxième centre de décision et ne sera constituée
   qu'entre deux congrès. Ainsi, le :term:`B.C` reste en contact permanent avec les
   syndicats par l'intermédiaire des unions régionales.
2. Toute décision devant être prise **ne relevant pas des tâches purement
   techniques** et ne se situant pas dans la ligne d'une action confédérale
   décidée en congrès devra, quel que soit son degré d'urgence, être validée
   par l'ensemble de la C.A, par le secrétaire confédéral et éventuellement par
   le secrétaire confédéral concerné (ex : secrétaire international s'il s'agit
   d'une décision relevant de l'international), chacun ayant une voix.
3. S'il n'y a pas unanimité mais qu'une forte majorité se dégage (au moins les
   deux-tiers des mandatés), une procédure express de consultation des syndicats
   par l'intermédiaire des régions est lancée, à laquelle au moins la moitié
   des syndicats doivent avoir répondu pour qu'elle soit validée.
   L'absence de quorum ou d'une majorité claire entraîne la préservation
   du statu quo.
   La question est dans ce cas considérée comme relevant d'une décision de
   congrès ou de CCN et nécessitant un débat préalable qui est initié à cette
   occasion.
