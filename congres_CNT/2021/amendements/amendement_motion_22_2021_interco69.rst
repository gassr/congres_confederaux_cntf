
.. _amendement_motion_22_2021_interco69:

=======================================================================================================
Amendement à la motion n°22 **Drogues, alcool et émancipation révolutionnaire** par **Interco69**
=======================================================================================================

- :ref:`motion_22_2021_etpics94`

Autres motions Interco69

  - :ref:`rhone_alpes:motions_interco69`

Argumentaire
=============

Produire et diffuser du matériel pédagogique impliquerait que la CNT ait
une ligne sur la question.
Il n’en ait rien. Avant toute chose, il faut débattre et définir une
ligne politique quant à cette question dont nous reconnaissons toute
l’importance


Amendement
============

La lutte contre la `culture` de l’alcool doit faire partie de nos réflexions
contre toutes les aliénations, et pour l’émancipation des travailleurs
et travailleuses.

Il nous semble important de réinterroger la consommation d’alcool en
général et au sein de la CNT, ainsi que sa valorisation par le système
dominant, qui par intérêts économiques et par une volonté d’endormir les
consciences a tout à gagner à faire perdurer cette culture.

Aussi parce que nous sommes contre tout ce qui affaiblit la force et
l’élan révolutionnaire des êtres humains. C’est également contribuer à
faire reculer la violence dans la société, entre autres les violences
racistes et sexistes.

La confédération se dote de tous les moyens pour s’informer sur la question,
alimenter la réflexion autour et produire une ligne politique et
organisationnelle quant à ce sujet (brochures, tracts, affiches, conférences...).

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°22 |            |          |            |                           |
| Interco69                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
