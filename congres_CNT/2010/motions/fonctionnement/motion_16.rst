


.. _motion_16_2010:

========================================================================================
Motion n°16 2010 : Mise à disposition de matériel confédéral aux mandaté-e-s, SINR 44
========================================================================================


  
Argumentaire
============

Il serait pertinent de mettre à disposition des mandaté-e-s confédéraux, le matériel informatique, téléphonique et
autre, nécessaire à la bonne exécution de leurs mandats (logiciels, matériel informatique lui-même) afin que le/la
mandaté-e n'ait pas à utiliser son matériel personnel. Dans la mesure du possible, les logiciels utilisés seraient
des *logiciels libres et open source*.
À l'heure actuelle aucun inventaire des biens appartenant à la confédération n'existe, les achats ayant été fait par
le passé n'ayant pas été recensés.

  
Motion
======

La Confédération Nationale du Travail met à disposition les moyens techniques qu'elle jugera nécessaires à la
bonne exécution des mandats confédéraux.

Proposition(s)
--------------

Une commission sera créée, fonctionnant en relation étroite avec la trésorerie
confédérale, sous contrôle des syndicats, par le biais des :term:`C.C.N`, dans un premier
temps, puis du congrès confédéral. À charge du congrès de déterminer le nombre
de mandaté-e-s nécessaires au sein de la commission.

Que soit réalisé :

- Dans un premier temps, un inventaire complet des parcs informatique, téléphonique
  et autre appartenant à la confédération.
- En parallèle, une évaluation des besoins matériels nécessaires à l'exécution
  des mandats confédéraux.
- Une mise en adéquation entre les biens dont la confédération est propriétaire et
  les besoins des mandaté-e-s.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°16          |            |          |            |                           |
| SINR 44              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`amendement_motion_16_2010_stea`
