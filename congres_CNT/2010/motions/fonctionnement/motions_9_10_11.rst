.. index::
   pair: Motion; Péréquation


.. _motions_9_10_11_2010:
  
========================================================================
Motions n°9, 10 et 11 2010 : Péréquation Congrès confédéraux, ETPRECI 35
========================================================================



Argumentaire
============

Les modalités de péréquation des frais de déplacement des mandatés des syndicats
aux Congrès de la :term:`C.N.T` sont précisées pour chacun d’eux par le :term:`C.C.N`
précédant le Congrès.
Pourtant, les syndicats semblent ne pas prendre en considération l’importance financière
et solidaire de leur participation à ces frais.

La péréquation des frais s’opère généralement sur la base du déplacement de deux
mandatés par syndicat.

Actuellement (2010), seul un syndicat a participé et versé solidairement sa côte-part
à la trésorerie confédérale. C’est inadmissible. Ou alors on supprime le principe
de péréquation, ou alors tous les syndicats y participent.

.. _motion_9_2010:

Motion n°9 2010 : Péréquation Congrès confédéraux, ETPRECI 35 [adoptée ?]
==========================================================================

La péréquation s’applique à chaque Congrès en prenant en compte les frais de
déplacement de deux mandatés par syndicats.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°9           | 47         | 3        | 3          | 2                         |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion adoptée ?**

.. _motion_10_2010:

Motion n°10 2010 Péréquation Congrès confédéraux, ETPRECI 35 [adoptée avec amendement]
=======================================================================================

Tous les syndicats représentés (présents ou représentés par mandat) participent à la péréquation.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°10          | 41         | 14       | 3          | 2                         |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**La motion 10 est adoptée en l'état.**

Protestations

:ref:`Vote recommencé <amendement_motions_9_10_11_educ13_2010>`  suite à diverses prises de
parole de syndicats arguant qu'ils auraient voté contre la ``motion en l'état``
s'ils avaient su que l'amendement serait rejeté parce que n'ayant pas obtenu
autant de voix «Pour» que la motion en l'état.


[Résultat définitif] **La motion n°10 amendée est adoptée**.


.. _motion_11_2010:

Motion n°11, Péréquation Congrès confédéraux, ETPRECI 35 [adoptée ?]
====================================================================

Pour avoir droit de vote au Congrès :term:`C.N.T`, les syndicats doivent s’être
acquittés du règlement de leur participation à la péréquation.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°11          | 35         | 21       | 5          | 2                         |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

**Motion adoptée ?**

.. seealso::

   - :ref:`contre_motion_9_10_11_2010_ess34`
   - :ref:`contre_motion_9_10_11_2010_culture_rp`
   - :ref:`amendement_motions_9_10_11_educ13_2010`
