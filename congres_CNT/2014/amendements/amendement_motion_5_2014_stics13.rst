
.. _amendement_motion_5_2014_stics13:

================================================
Amendement à la motion n°5 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_5_2014_sinr44`
   - :ref:`stics13`





Argumentaire
=============

C'est trop souvent parce que les horaires de début et de fin de congrès ne
sont pas respectés que les syndicats finissent par quitter le congrès avant
qu'il ne se termine.

Amendement
==========

Les horaires prévus en début de congrès seront désormais respectés.
