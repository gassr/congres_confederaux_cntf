
===========
Admissions
===========

Article 11
==========

Ne seront admis à la fédération que les groupements composés de travailleurs
des secteurs des transports et activités auxiliaires,ainsi que les adhérents
de syndicats intercorporatif, travaillant dans les transports, à l'exception
de ceux qui tirent profit d'autres travailleurs.

**La fédération n'exclue aucun type de transport**

Elle regroupe les travailleurs des transports de personnes ou de marchandises,
routiers, ferroviaires, fluviaux, maritimes, aériens, transports en commun ou
individuel,les coursiers, les chauffeurs de maitres, dans le secteur de la
santé (ambulanciers, pompes funèbres), les travailleurs des plateformes
logistiques, les déménageurs, les docks et toutes leurs activités auxiliaires
(accompagnateurs, mécaniciens, manutentionnaires, administratifs, ect...) que
ces secteurs soient de droit privé ou public.

En bref, tous les salariés dépendant d'une convention collective concernant
les transports et la logistique. Sans oublier les travailleurs étrangers
travaillant sous pavillon Français dans la marine marchande, ou les chauffeurs
étrangers employés par des sociétés francaises.

Chaque syndicat devra accompagner sa demande d'admission de deux exemplaires de
ses statuts et indiquer le nombre de ses adhérents, ainsi que les noms et adresses de ses
secrétaire et trésorier.
Son adhésion sera effective et entraînera cotisation après délivrance du label syndical
dépendant de l'avis de la Fédération, du Bureau Confédéral et de la structure régionale si elle
éxiste.


Article 12
==========

Il ne pourra être admis à la Fédération qu'un seul syndicat par secteur
géographique (les délimitations étant définies par le congrés fédéral).
