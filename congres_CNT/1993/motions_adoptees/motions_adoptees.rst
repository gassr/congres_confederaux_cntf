
.. index::
   ! XXIVe Congres CNT 1993 (Paris)
   ! Paris (XXIVe congrès 6, 7 février 1993)


.. _motions_adoptéees_congres_CNT_1993_paris:

==============================================
Motion adoptée au XXIVe Congrès CNT 1993 Paris
==============================================

.. sidebar:: XXXIe Congrès dédié à Valérie Franiatte.

   Le XXXIe Congrès est dédié à Valérie, ancienne secrétaire confédérale de
   1993 à 1996.
   On ne t'oublie pas, merci à toi.


.. toctree::
   :maxdepth: 2

   identite
   structuration_confederale
