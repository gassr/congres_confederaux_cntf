

.. _motions_secretariat_international_cnt_congres_agen_2006:

========================================================================================
Motion Secrétariat international, 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
========================================================================================

Structuration interne du Secrétariat international
==================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 43         | 6        | 12         | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Le secrétariat international est composé d'au moins un mandaté en charge :

- du secrétariat : coordination interne du secrétariat international ainsi que
  rapports avec les syndicats de la CNT, participation au BC, convocations et
  compte-rendu des réunions, représentation de la CNT au niveau international ;
- du site Internet ;
- des publications propres au SI et du lien avec le CS ;
- des zones géographiques suivantes : Europe, Afrique, Asie, Amérique du Sud,
  Amérique du Nord, Proche Orient/Moyen Orient et Océanie.

Ces mandats sont confiés lors du congrès confédéral. Si des camarades veulent
rejoindre le SI après un congrès, ils peuvent être mandatés lors d'un CCN.


Publications du Secrétariat International
=========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 46         | 5        | 6          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


Outre les publications régulières ou occasionnelles spécifiques à des zones
géographiques ou à des sujets (Afrique XXI, brochure GT Palestine), il serait
souhaitable que le SI :

- envoie des articles pour chaque numéro du CS, de manière à ce qu’il y ait un
  cahier international dans chaque CS ;
- édite une circulaire internationale interne régulière ;
- publie un bulletin international ASSR sans périodicité fixe (au moins une
  fois par an) avec les autres organisations ASSR dans le monde.


Intégration des syndicats dans la démarche internationale
=========================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 46         | 5        | 11         | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Afin que le travail international ne soit pas assumé que par des mandatés ou
des **spécialistes**, il est nécessaire de mettre à disposition des syndicats
les outils indispensables à leur mobilisation sur les questions internationales.

Pour cela, chaque réunion du SI doit donner lieu à un compte-rendu qui sera
envoyé à chaque syndicat.

De plus, le SI est en contact avec les fédérations d'industrie afin de
favoriser les rapports de solidarité et d'échange par branche et secteur
d'industrie.



28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
70 syndicats présents à jour de cotisations
Pour : 39 contre : 1 Abst. : 6 refus de vote : 1
Organisation du secrétariat international
Etant devenu impossible à un seul militant d'assumer la responsabilité de toutes ces activités, le congrès 2004 mandate un
secrétariat collégial de trois personnes pour assurer la responsabilité des relations internationales de la CNT. Ces trois
Ces trois militants
se répartiront les activités internationales, et se chargeront de s'entourer de-camarades pour les aider dans leurs tâches.
