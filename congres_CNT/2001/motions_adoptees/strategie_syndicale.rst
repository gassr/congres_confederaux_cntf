

.. _motion_strategie_syndicale_cnt_congres_toulouse_2001:

======================================================================================
Motion Stratégie syndicale 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
======================================================================================


Motion de synthèse "Elections professionnelles"
===============================================

Motion de synthèse proposée par une commission de Congrès.


Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 35         | 4        | 10         | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+



Texte
-----

Le Congrès de Toulouse réaffirme les positions de la C.N.T sur la question des
Elections Professionnelles adoptées lors du Congrès de Lyon en 1996 à savoir:
((... MOTION DE 1996...))

En outre le Congrès de Toulouse prend acte que des syndicats, en contradiction
avec les accords de congrès, ont eu participé ou participent à des élections
aux C.E.

Pour ce motif, il est décidé la mise en place d'une commission confédérale
chargée de fournir, au prochain C.C.N., un bilan détaillé de ces expériences,
afin, collectivement, d'en dégager les leçons et de faire des propositions à
la réflexion des syndicats.

Dans l'immédiat, des syndicats qui se présenteraient sans en informer la
Confédération seraient considérés comme bafouant de fait les accords de Congrès
de la C.N.T., qui serait alors à même d'en tirer les conséquences.

Si, pour des raisons impératives et exceptionnelles (répression syndicale),
un syndicat était confronté à la nécessité incontournable de présenter des
candidats aux C.E., il devrait, au préalable, fournir aux U.R. et à la
confédération (BC, BI) un argumentaire détaillé justifiant ce choix.

Par ailleurs, ce syndicat prendrait l'engagement devant l'ensemble de la
Confédération, que ses candidats, dans l'hypothèse où ils seraient élus,
ne siégeraient pas au C.E., ce qui leur assurerait une protection sans
participer à un processus co-gestionnaire.
