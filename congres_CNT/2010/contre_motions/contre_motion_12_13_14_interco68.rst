

.. index::
   interco
   interpro

.. _contre_motion_12_13_14_2010_interco68:

==========================================================
Contre-motion aux motions N°12, 13 et 14 interco68
==========================================================

Première contre motion
======================

- la prise en compte des obligations légales faites aux agents FPT
  en fonction de la dimension géographique seule et non plus  en fonction
  des métiers et corps de  métiers qui créent plus de problèmes qu'il n'en résout
- au sein des territorialités, favoriser la nature Interco ou Interpro
  ce qui feral'écnomie du travail de suivi des agents compte­tenu du trop grand
  nombre de métiers et corps de métiers, compte­tenu des changements
- **répercuter autant que peut ce faire le concept Interco/Interpro dans les
  structures locales CNT ce qui permettra la même économie de travail de suivi**.
  Une adaptation adéquate assurera de ne pas retrouver un­e même adhérent­e dans
  plusieurs dimensions territoriales.
- le regroupement à terme de toutes les formes existantes de FPT disséminées un
  peu partout au sein  dela CNT. Ex: ATOS, Archéos INRAP
- la redéfinition et à terme la réorganisation de la fédération Santé Sociale
  trop imprécise en l'état, de façon à créer une fédération des Services Publics.
- qu'il soit demandé aux camarades Archéologues INRAP de préciser leur choix
  de se fédérer à la Culture (CCS) plutôt qu'à la fédé SS FPT
- que la même demande s'adresse aux ATOS. Qu'il leur soit demandé de préciser
  leur situation exacte aujourd'hui au sein de la CNT



+------------------------------------------+------------+----------+------------+---------------------------+
| Nom                                      | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==========================================+============+==========+============+===========================+
| Contre motion aux motions n°12, 13 et 14 |            |          |            |                           |
| interco68                                |            |          |            |                           |
+------------------------------------------+------------+----------+------------+---------------------------+

Deuxième contre motion
======================

Que le congrès précise où en est la demande de commission interfédérale faite en 2008, et au besoin,
qu'il soit fait un exposé ­ du moins succint ­ du travail effectué.

Que le congrès favorise par tous moyens le rapprochement des agents exerçant une fonction de service
public, au sens entendu par le contexte légal et plus largement, par les personnels mis à disposition
effective de l'employeur public.

Qu'il soit évoqué la question d'une possible création d'une fédération unique des "Services Publics".

+------------------------------------------+------------+----------+------------+---------------------------+
| Nom                                      | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==========================================+============+==========+============+===========================+
| Contre motion aux motions n°12, 13 et 14 |            |          |            |                           |
| interco68                                |            |          |            |                           |
+------------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_12_2010`
   - :ref:`motion_13_2010`
   - :ref:`motion_14_2010`
