
.. _motion_31_2012:
.. _motion_31_2012_etpic30:

=======================================================================================================================================================================================
Motion n°31 2012 : Action syndicale et militante & Organisation organique et technique de la CNT / Développement coopératif et prestations techniques externes ETPIC 31 Sud
=======================================================================================================================================================================================
  
.. seealso::

   - :ref:`etpic30`
   - :ref:`motions_etpic30_2012`




Motion
=======

Notre syndicat propose la réactivation de la commission telle que définit lors
du 30 eCongrès de Lille :

«Il est créé une commission confédérale "coopératives syndicales/économie
coopérative" chargée de :

- mutualiser les informations liées aux systèmes coopératifs existants par la
  création d’un site internet
- coordonner les camarades déjà présents dans le réseau coopératif
- réfléchir à la question des reprises en coopératives comme réponse aux
  délocalisations, fermetures d’entreprises et comme alternative aux petites
  entreprises et à l’artisanat. »


Par son opposition à l’exploitation, il est entendu que la CNT tend à
développer des formes d’organisations professionnelles à caractère coopératif,
collectif ou associatif garantissant autant que possible l’égalité économique
et sociale des travailleurs/ses.

La recherche d’alternative au salariat (ou l’actionnariat) s’inscrit dans
une lutte plus globale contre le capitalisme, pour la transformation totale
de la société.

Les syndicats et Unions de syndicats CNT peuvent néanmoins tisser des liens
privilégiés avec certaines entreprises, artisans ou associations afin de
recourir à leurs services d’ordre techniques ou assurantiels.

Ils préféreront s’adresser à des entreprises coopératives ou dites de
« l’économie sociale » : Coopératives ouvrières (SCOP, SCIC, SCE),
associations, mutuelles.

La CNT n’utilise pas le « label syndical » tel que définit légalement
(Art. L2134-1 du code du travail). Il en fait un usage empirique,
exclusivement à vocation interne et non légal.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°31          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
