
.. _amendement_motion_2_2021_interpro31:

===============================================================================
Amendement N°2 à la Motion n°2 2021 **Antiproductivisme** par **Interpro31**
===============================================================================

- :ref:`motion_2_2021_staf29`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`

Argumentaire 
============

Nous pensons que la motion peut être plus concise, sans pour autant rogner
sur le fond. Par ailleurs nous ne sommes pas forcément très convaincues
par l'utilisation du concept de ``nature``.

Amendement 
============

Les syndicats constituant la CNT œuvrent [...] à déconstruire l’imaginaire
productiviste et ses corollaires : l’exploitation salariale sociale et le consumérisme. […]


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°2  |            |          |            |                           |
| Interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
