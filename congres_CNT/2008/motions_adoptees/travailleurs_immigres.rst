.. _motions_travailleurs_immmigres_cnt_congres_lille_2008:


==============================================================================================
Motion "Travailleurs immigrés" 30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
==============================================================================================

Soutien aux sans-papiers
========================

Vote
----

67 syndicats à jour de cotisations / 47 présents.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 26         | 13       | 8          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

- La CNT peut et doit soutenir, accompagner, défendre, syndiquer, toute
  personne en situation irrégulière, qu'il ou elle soit un travailleur ou pas.
- La CNT décide d'investir toutes les luttes, actions, collectifs visant à la
  disparition des centres de rétention administrative.
