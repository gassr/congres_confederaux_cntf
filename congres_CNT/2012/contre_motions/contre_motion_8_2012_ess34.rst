

.. _contre_motion_8_2012_ess34:

================================================================================================
Contre-motion à la motion N°8 2012 ESS34
================================================================================================

.. seealso::

   - :ref:`motion_8_2012_ste72`



Contre-motion
=============

De plus, par souci de démocratie et de transparence et comme cela se fait dans
d'autres confédérations syndicales, il convient d’enregistrer sur vidéo les
débats.

La vidéo est conservée par le secteur archive.

Elle est consultable mais non difusable.

Tous les adhérents auront ainsi une idée claire de ce qui s'est dit lors du
congrès de leur confédération.

Chacun-e pourra alors se rendre compte du niveau de démocratie que la CNT
a pu atteindre et méditer dessus pour la prochaine fois.


Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°8      |            |          |            |                           |
| ESS34                              |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
