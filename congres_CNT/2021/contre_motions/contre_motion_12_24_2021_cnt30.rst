.. index::
   pair: Justice; ETPIC30

.. _contre_motion_12_24_2021_cnt30:

====================================================================================================================================================================================================================================
Contre motion N°1 à la motions n°12 2021 **Modification des statuts pour la gestion interne des violences patriarcales** et contre motion N°2 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **ETPIC30**
====================================================================================================================================================================================================================================

- :ref:`motion_12_2021_sinr44`
- :ref:`motion_24_2021_ste33_cnt42`

Autres motions ETPIC30
========================

- :ref:`motions_etpic30`

Préambule
==========

Nous rejoignons nos camarades quant à la nécessité de poursuivre sans
relâche la lutte anti patriarcale, anti sexiste et féministe.
Cette lutte est partie intégrante de nos luttes syndicales, dans nos vies
et dans nos localités.
Elle intègre nos fonctionnements, nos formations, nos revendications, et
bien évidemment l’ensemble de nos combats.
Les discours et la vigilance ne suffisent pas.

Les violences sexistes, sexuelles, perdurent dans nos rangs et les
agressions les plus graves nous laissent parfois démuni·e·s.
Il faut en effet poursuivre ce combat, tant en interne qu’en externe.

Nous ne partageons toutefois pas les propositions de protocoles de
:ref:`nos camarades du STE de la Gironde et des quatre syndicats de la Loire <motion_24_2021_ste33_cnt42>`,
ni même la modification de statut attenante proposée par :ref:`le SIRN de
Loire-Atlantique <motion_12_2021_sinr44>`.

Notre opposition à ces motions conjointes est principalement liée aux
raisons suivantes :

- un **attachement viscéral à la justice** et la présomption d’innocence
- un attachement inaliénable à l’autonomie de notre syndicat adhérent de la CNT
- le refus des commissions d’expert politiques.


.. _presomption_innocence_contre_motion_12_cnt_30:

1) Concernant la présomption d’innocence
==========================================

La **présomption d’innocence** est un principe de droit essentiel à la
préservation des libertés individuelles et collectives.

Si la justice bourgeoise en a fait un socle de droit au point de conditionner
l’ensemble de ces procédures judiciaires, **il serait tout de même paradoxal
que des libertaires la sacrifient**.

Dans les deux protocoles proposés (motions :ref:`n°12 <motion_12_2021_sinr44>` et :ref:`24 <motion_24_2021_ste33_cnt42>`,
le ou la plaignant·e "accusant de viol et/ou agression sexuelle un membre
de la CNT est considérée comme une victime et bénéficie de tous les
traitements et égards dus à ce statut (aide psychologique, juridique,
financière en cas d’incapacité au travail) et ce le temps nécessaire
à l’instruction de l’affaire et sans jugement au préalable de son issue".

Nous rejoignons pleinement la volonté de nos camarades de soutenir et
d’entourer les plaignant·e·s, les victimes déclarées, dans tous les
domaines évoqués.

En revanche, nous ne sommes pas légitimes pour leur accorder d’emblée le
statut légal de **victime**.

Bien sûr, il faut replacer l’emploi de ce terme dans la longue histoire
des violences faites aux femmes, qui ont eu, pendant des siècles, à
affronter les accusations de mensonges dès qu’elles souhaitaient dénoncer
les viols ou les agressions dont elles étaient victimes.

En effet, les fausses allégations de viols sont rares.

Pourtant, dans les commissariats, les femmes sont régulièrement mal reçues.

Mais il demeure difficile de parler de **victimes**, car ce terme implique
une culpabilité établie, instituant de facto la présomption de culpabilité.
Or, c’est ce que proposent et permettraient les protocoles exposés
(motions :ref:`n°12 <motion_12_2021_sinr44>` et :ref:`24 <motion_24_2021_ste33_cnt42>`).

Une agression de nature sexuelle relève de l'ignominie la plus absolue
qui soit, l'expression d'une puissance incontrôlée exercée sur autrui.
Quand une plainte est déposée pour viol, elle doit toujours être prise
très au sérieux.
Mais l'attention qui y est apportée ne peut pas, ne doit pas anéantir
le principe de la présomption d'innocence.
Il ne doit pas être mis en avant une quelconque présomption de véracité
ou de crédibilité.
Jamais une allégation de crime ne peut suffire pour considérer le crime
comme établi.

Il nous faut accepter le principe de la présomption d’innocence pour ne
pas céder à une spirale infernale qui jugerait non plus les individus
sur la nature de leurs actes réels mais sur des présupposés ou des préjugés
prenant naissance à la racine de notre cœur.

Pareille posture nous entraînerait dans un monde sans foi ni loi où, avec
la caisse de résonance représentée par les réseaux sociaux, chacun·e
serait libre d'épancher sa rancœur sans se soucier un seul instant de la
véracité des faits. Où l'on jugerait, condamnerait, clouerait au pilori
n'importe quel quidam sans même qu'il ait pu se défendre.

Quelles que soient la gravité des faits reprochés, la nature de l'outrage,
l'intensité du ressenti, le doute doit toujours profiter à l'accusé·e.

Dans le doute, on préférera toujours un·e criminel·le en liberté qu'un·e
innocent·e en prison.
Il ne peut pas y avoir d'exception à cette règle intangible de la
présomption d’innocence.

**Nous n’y renoncerons pas.**

**La défense est aussi le droit fondamental des accusé·e·s**, des mis·e·s en
cause.
Et pourtant, les protocoles proposés (motions :ref:`n°12 <motion_12_2021_sinr44>` et :ref:`24 <motion_24_2021_ste33_cnt42>`)
n’en font nullement mention.

Ces principes fondamentaux qui viennent d'être rappelés n'ont rien à voir
avec une quelconque complaisance de genre, ni faiblesse du sentiment, ou
encore avec une écoute insuffisante des femmes, pas plus qu'ils ne
démontrent une quelconque méfiance générale vis à vis de leurs allégations.

Leur seul objectif, prioritaire, est autant que possible d'éviter les
erreurs de jugement.

Les syndicats de la CNT, en ce qui les concerne directement, ont pour
seule sanction viable, l’exclusion.

Lorsque la culpabilité est ainsi démontrée, l’exclusion devient nécessaire
dans un souci de cohérence idéologique et bien sûr de protection des
militant·e·s au sein de l’organisation.

Mais l’exclusion de la CNT ne résout rien, ou presque.
Aussi indispensable puisse-t-elle être parfois, elle porte en elle
ses limites.

Elle renvoie à la société en place le soin de gérer les condamnations
de l’agresseur, son soin éventuel, et la réparation des victimes sur
les plans juridiques, financiers et médico-sociaux.

En l’état l’actuel de son développement, de son projet révolutionnaire,
la CNT ne dispose pas des moyens suffisants et légitimes pour assurer
et asseoir les investigations utiles et le traitement judiciaire des
affaires d’agressions.
Sans alternative réelle, concrète, elle ne peut et ne doit s’engager sur
la voie d’une pseudo justice parallèle. Elle est donc contrainte de
s’en remettre au système judiciaire dominant, à son corpus juridique
et à ses insupportables inclinaisons patriarcales et capitalistiques.

La CNT doit toutefois sans cesse encourager, d'une manière véhémente et
répétée, les victimes à porter plainte, à se confier, à parler.

Ses militant·e·s doivent aussi pouvoir apporter leurs soutiens aux victimes
déclarées avec tous les moyens humains, juridiques et financiers à leurs
dispositions.

2) Concernant l’autonomie fédéraliste et inaliénable des syndicats de la CNT
==============================================================================

Concernant l’autonomie fédéraliste et inaliénable des syndicats de la CNT
et les dérives potentielles des propositions de « protocoles de gestion
des violences antipatriarcales » (et autres **circonstances exceptionnelles**)

Tel que nos camarades les présentent, les commissions d’expert·e·s,
attendu·e·s comme capables de délibérer, de juger, sont pour nous
l’expression d’un **centralisme démocratique inacceptable, aussi
aventureux qu’illégitime**.

Intégrées à des propositions de protocole, ces commissions s’apparentent
à une forme de **Politburo** que la juste cause féministe/antipatriarcale/antisexiste
qu’elles seraient censées défendre au travers de l’examen de problématiques
d’agressions, ne suffit à justifier.

Outre leurs compositions **pour le moins hasardeuses**, ces commissions
reproduisent des schémas autoritaires, jusqu’à devoir prendre appui sur
une demande de modification de statut (motion :ref:`n°12 <motion_12_2021_sinr44>`)
ouvrant la voie à un **nouveau mode d’exclusion contraire aux principes
fondamentaux de notre fédéralisme**.

En effet, des camarades proposent qu’il puisse être rendu possible une
exclusion directe d’une personne par la Confédération sous le régime de
l’exception : « Dans des circonstances exceptionnelles [?], prévues par
des motions de congrès, la confédération peut se substituer à un syndicat
pour décider de l’exclusion de l’un·e de ses adhérent·e·s. ».

Au-delà la dangerosité de l’introduction de ce type de traitements
dérogatoires, un tel mécanisme d’exclusion est en soit aussi équivoque
qu’inapplicable.

Nombreuses sont les causes que nous défendons.

Nombreux pourraient être demain les motifs d’exclusion.

Cette modification statutaire (:ref:`n°12 <motion_12_2021_sinr44>`) ouvre
le champ du possible à des dérives internes, entre autres procès idéologiques
et comportementaux, dont nous aurons bien du mal à nous défaire.

Par ailleurs, notre fédéralisme implique que chaque cénétiste adhère à
son syndicat.
**Seul le syndicat est adhérent de la Confédération**.

Sur le plan constitutionnel de la CNT, l’exclusion d’un·e adhérent·e par la
Confédération n’a donc pas de sens.

Dès lors, si tant est que la Confédération ait à se prononcer, rien
n’empêche un syndicat de maintenir un·e adhérent·e dans ses rangs
contre l’avis de cette dernière.

C’est donc bien, et de façon intangible, le syndicat qui répond de
l’adhésion ou de l’exclusion de son adhérent·e.
Bien sûr, la Confédération peut contester les choix de l’un de ses
syndicats membres, jusqu’à même l’exclure.
Considérant que c’est déjà notre organisation fédéraliste actuelle, la
motion proposée (motion n°2) est improductive.

Enfin, si le Congrès entérinait une telle modification de statut, elle
placerait la CNT et tous les syndicats (ou le CCN /UR ?) devant l’étude
par tous et toutes de toutes les affaires, affaires sensibles ou
exceptionnelles a priori.

Il nous semble qu’à l’usage, cela pourrait constituer un écueil
organisationnel majeur, une foire confédérale aux interprétations
les plus hétérogènes, loin du local et des faits, une forme de cheval
de Troie prétendument démocratique par sa nature intrinsèquement
conflictuelle.

Au demeurant, et au-delà de son inapplicabilité, nous espérons ici que
les syndicats rejetteront massivement l’orientation hasardeuse de
cette proposition de modification statutaire (:ref:`n°12 <motion_12_2021_sinr44>`).


Ainsi, en guise de contre motion
=====================================


Ainsi, en guise de contre motion, nous proposons que la CNT adopte les
orientations suivantes (assorties de l’argumentaire en introduction
circonstanciée) :


3) Notre approche de la gestion interne des violences patriarcales et sexistes
===============================================================================

Notre approche de la gestion interne des violences patriarcales et sexistes
Nous estimons que la CNT ne dispose pas en l’état de son développement,
ou de la construction de son projet révolutionnaire, de moyens d’investigation
et de justice suffisants et légitimes pour permettre d’asseoir
convenablement la recherche de la vérité dans les affaires d’agression.

Aussi à la CNT, en matière de traitement des violences sexistes ou sexuelles
interne ou externe à l’organisation, seuls l’accompagnement et le soutien
des victimes déclarées et l’éventuelle exclusion d’un·e syndiqué·e reconnu·e
coupable nous semblent dans l’immédiat possible.

Nous sommes conscient·e·s de nos limites organisationnelles en matière
de moyens, de mandat et de déontologie d’investigation.

Nous sommes aussi conscient·e·s de nos limites propres en termes de corpus
juridique, de capacités de traitement judiciaire, de réparations des victimes,
de condamnations, de soins, etc.

Nous sommes enfin conscient·e·s des limites et parfois de l’inconséquence
de la seule exclusion d’un·e agresseur·euse.

Dans le soutien que nous leur apportons, nous considérons pleinement la
parole des personnes qui se déclarent victimes et souhaitons agir en
conséquence, par l’écoute, l’accompagnement,... en aucun cas
comme substitut de justice et nous souhaitons agir en conséquence.

Quand une plainte est déposée pour viol, elle doit toujours être prise
très au sérieux.
Mais, la considération de la plainte des personnes, et dans leur plus
grand majorité historique des femmes, ne suffit à établir leur statut
de victime, tout comme l’accusation d’une personne ne suffit à la rendre
coupable.

**Libertaires, nous sommes attaché·e·s à la présomption d’innocence.**

Ce principe est essentiel à la préservation des **libertés individuelles
et collectives**.

**Jamais une allégation de crime ne peut suffire pour considérer le crime
comme établi**.

Le doute, quand la vérité ne parvient pas à trouver son chemin, doit
toujours être du côté de l'accusé·e.

Que ce dernier ou cette dernière ait agressé une femme ou un homme,
ou commis un meurtre.
Il ne peut pas y avoir d'exception à cette règle intangible.

Par ailleurs, nous ne sommes pas non plus adeptes de la vindicte populaire,
ni du lynchage social, fut-il à l’appui de la vox populi, de la rumeur,
du scandale médiatisé, ou de la frénésie des réseaux sociaux et de ses
chimères démocratiques.

Il existe, en l’état actuel de structuration de la CNT, des outils
suffisants pour traiter de l’exclusion d’une personne, comme du soutien
d’une autre.

Si les fondations d’un droit et d’une justice sans logique de classe
et sans logique patriarcale restent à construire, nous ne croyons pas
aux rôles des seul·e·s expert·e·s et des commissaires, fussent-ils·elles
entendu·e·s ou désigné·e·s comme spécialistes ou idéologiquement
bien formé·e·s.

Lorsqu’un·e mis·e en cause est adhérent·e de la CNT, son syndicat avise
de la conduite à tenir en matière de maintien d’adhésion ou d’exclusion.

Le syndicat répond de son choix devant ses pairs, devant la Confédération
Nationale du Travail.

Tout en préservant le principe de la présomption d’innocence de l’intéressé·e
et en prenant soin de ne pas s’exposer à la diffamation, le syndicat
peut prendre conseil et avis auprès de l’Union des syndicats CNT la plus
proche géographiquement.

Il peut aussi demander l’appui technique et l’avis de la commission
confédérale antipatriarcale et antisexiste
pour l’égalité et l’équité entre les sexes (cf. :ref:`motion n°20 du XXXV ème Congrès <motion_20_2021_etpic30>`).

4) Après avoir formulé ces aveux d’impuissance, provisoires
==============================================================

Après avoir formulé ces aveux d’impuissance, provisoires il faut l’espérer,
nous nous en remettons donc au système judiciaire et policier en place,
aussi imparfait et inique puisse-t-il nous paraître.

En faisant le choix de nous en remettre aux procédures de justice et
aux investigations policières de l’État bourgeois, nous n’abandonnons
d’aucune façon la lutte essentielle pour une véritable prise en compte des
violences faites aux femmes, aux enfants et aux personnes les plus
vulnérables dans les plaintes et les enquêtes de police.

Nous souhaitons pouvoir soutenir les plaignant·e·s, les victimes déclaré·e·s,
les entourer, les accompagner vers l’accès aux droits et aux soins.

Avec leurs consentements, nous favoriserons les dépôts de plainte, leurs
prises en compte et le soutien juridique nécessaire.

Nous veillerons à ne pas laisser le temps s'écouler de trop, tant il
finit par jeter parfois une ombre suspicieuse sur la sincérité des
témoignages.

En attendant un changement radical d’orientations sociétales, nous
poursuivons notre combat immédiat pour tenter de faire infléchir
l’inclinaison patriarcale et capitalistique de la justice actuelle.

La lutte pour le respect et l’égalité des droits, la lutte contre les
discriminations et la répression, l’éducation populaire et la propagande,
la formation, le soutien et la défense aux plaignant·e·s et aux victimes,
la prévention des violences sous toutes leurs formes, constituent nos
axes et nos outils de luttes actuels.

Le chantier est immense et ce combat doit se poursuivre avec une
détermination sans faille.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion aux motions n°12 |            |          |            |                           |
| et n°24 CNT30                  |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
