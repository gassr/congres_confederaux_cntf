
.. _amendements_motion_19_2021_interpro31:

===========================================================================================
Amendements N°1 à la motion n°19 2021 **mise en sommeil d’un syndicat** par **Interpro31**
===========================================================================================

- :ref:`motion_19_2021_ste93`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`



.. _amendement_1_motion_19_2021_interpro31:

Amendement 1
===============

Argumentaire 
------------------

- Voir :ref:`amendement_1_motion_18_2021_interpro31`


Amendement 
------------

Ajout dans le chapitre « Structuration & cohésion confédérales » des
articles suivants :

Article 1 
    Un syndicat **d’industrie** est déclaré en sommeil lorsqu’il déclare moins
    de cinq adhérent-e-s.

Article 2 
    La décision de mise en sommeil est prononcée 3 mois avant le congrès
    confédéral.

Article 3 
    Entre deux congrès, le bureau confédéral met à jour la liste des
    syndicats répondant aux conditions de participation aux congrès en
    terme de nombre d’adhérent-e-s, et a pour mandat, avec l’UR de rattachement,
    d’accompagner les syndicats qui seraient moins de cinq adhérente- s.

Vote
--------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°1 à la motion  |            |          |            |                           |
| n°19 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


Amendement 2
================

Argumentaire 
-------------

- Voir :ref:`amendement_2_motion_18_2021_interpro31`


Amendement 
------------

Ajout dans le chapitre « Structuration & cohésion confédérales » des articles suivants :

Article 1 
    Un syndicat d’industrie est déclaré en sommeil lorsqu’il déclare moins de cinq dix adhérent-e-s.

Article 2 
    La décision de mise en sommeil est prononcée 3 mois avant le congrès confédéral.

Article 3 
    Entre deux congrès, le bureau confédéral met à jour la liste des
    syndicats répondant aux conditions de participation aux congrès
    en terme de nombre d’adhérent-e-s, et a pour mandat, avec l’UR de
    rattachement, d’accompagner les syndicats qui seraient moins de
    cinq dix adhérente- s ne répondent pas aux dites conditions.»


.. figure:: interpro31/amendement_2_motion_19.png
   :align: center

Vote
--------

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement N°2 à la motion  |            |          |            |                           |
| n°19 Interpro31             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
