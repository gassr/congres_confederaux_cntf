
.. _motion_protection_sociale_cnt_congres_saint_denis_2004:


================================================================================================================================
Motion Travail, revendications salariales et protection sociale, 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
================================================================================================================================

.. index::
   retraites
   motion retraites

Retraites
=========

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 21         | 8        | 24         | 8                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Généralités
-----------

- Viser à la création d'un régime unique de Protection Sociale regroupant la
  Sécurité Sociale, les Retraites et les Allocations Familiales, géré et
  administré par les travailleurs eux-mêmes, et viser à la suppression des
  régimes complémentaires et d'épargne retraite.

- Supprimer les exonérations dont bénéficient l'État et les collectivités
  territoriales. Ils doivent cotiser pour leurs salariés au taux général pour
  l'ensemble des risques y compris la retraite et le chômage.

- Transférer la Contribution Sociale Généralisée (CSG) vers le régime unique
  de Protection sociale sans passer par les services de l'État.

- Abrogation des lois Balladur et Raffarin.

Isoler les détenteurs du capital
--------------------------------

Donner la priorité au travail vivant et non à la rente (épargne salariale,
fonds de pension), car c'est le travail ET LUI SEUL qui fait la richesse
d'une société.

Refuser la retraite par capitalisation non parce que moins sure ou moins
rentable que la retraite par répartition, mais parce que l'actionnariat
transforme les travailleurs en individus schizophrènes: exploités dans leurs
entreprises en tant que travailleurs, et exploiteurs des travailleurs des
entreprises en tant qu'actionnaires.

La débâcle du fonds de pension Enron n'est d'ailleurs pas significative
s'agissant d'un fonds de pension " employeur ", alors qu'aussi bien aux USA et
surtout qu'aux Pays-Bas, beaucoup de fonds de pensions sont interentreprises
contrôlés et gérés paritairement, et leurs adhérents ont un " droit " à la
retraite, même en cas de faillite, et non simplement une "promesse de retraite".

Les entreprises ont été tenues de provisionner leurs engagements, autrement dit,
d'avoir toujours en caisse l'équivalent des sommes qui seraient nécessaires
pour payer les pensions.
Enfin, elles ont été obligées de cotiser à une caisse de réassurance pour se
couvrir en cas de faillite.


- Rompre avec le paritarisme (les cotisations sociales et patronales doivent
  être gérées par les travailleurs : le " fric des travailleurs " n'a pas à
  être cogéré avec le patronat) ;
- Supprimer les exonérations encourageant l'épargne salariale, la participation,
  l'intéressement, l'actionnariat, et toutes formes de rémunération des
  travailleurs à base d'épargne ou d'actifs financiers.
- Le financement de la Retraite par Répartition sera assuré par les cotisations
  salariales et patronales qui font partie intégrante du salaire.


Unité des travailleurs
----------------------

- Affirmer le lien entre l'emploi et la cotisation sociale : tout travail
  entraînera le paiement d'une cotisation sociale.
- Systématiser la cotisation sociale proportionnelle au salaire brut :
- Déplafonnement des cotisations.
- Tout salaire brut doit donner lieu à un salaire en deux parties, salaire
  net et cotisation sociale proportionnelle.
- Les taux devront être identiques pour les secteurs publics et privés.
  Les régimes publics devront entrer dans le régime commun.
- Supprimer toutes les exonérations décidées pour " sauver l'emploi ", qui
  correspondent en réalité à des baisses de salaire, puisque les cotisations
  salariales et patronales font partie du salaire. Ces exonérations, qui
  d'ailleurs n'ont pas créé les emplois escomptés sont en grande partie
  responsables des déficits de la Sécurité sociale (celles des jeunes, des
  travailleurs non qualifiés, des chômeurs longue durée, des salariés à
  temps partiel, etc.).
- Remplacer les exonérations supprimées par un fonds de compensation, contrôlé
  et géré par les travailleurs, favorisant la création d'emplois, et donc les
  cotisations sociales, et qui servira à soutenir la création de Sociétés
  Coopératives Ouvrières de Production. Ce fonds pourrait être alimenté par
  une taxe qui portera sur les produits financiers et les entreprises dont le
  rapport coûts salariaux sur chiffre d'affaires est inférieur à la moyenne
  (exemples : importateurs de produits délocalisés, entreprises faisant un
  usage spéculatif de leurs actifs, etc.).
- Plafonner les retraites élevées pour relever les plus basses retraites.
- À supposer que l'équilibre conjoncturel entre cotisations et prestations
  ne puisse pas être rétabli par une hausse des cotisations
  liées aux emplois il faudra réduire les prestations les plus fortes.
- Salarier d'autres activités : " statut étudiant " par exemple.



Critique du revenu garanti universel
====================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 36         | 2        | 12         | 9                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Face aux attaques contre la protection sociale basée sur la répartition et la
solidarité interprofessionnelle, pour défendre la logique du salaire socialisé,
contre toutes les formes de précarité, la CNT oppose son outil de lutte, à
savoir le syndicalisme révolutionnaire et anarcho-syndicalisme et revendique:

- Par rapport aux conditions de travail:

	* Augmentation générale des salaires ;
	* Lutte contre l'inégalité salariale ;
	* Application du code du travail, des conventions collectives ;
	* Transformation des contrats précaires en CDI quand c'est choisi ;
	* Réduction massive du temps de travail sans flexibilité ;
	* Partage du travail pour que tous aient accès au cycle de production, dans
	  la perspective d'une société basée sur la répartition des
	* richesses et fondée sur le travail socialement utile.

- Par rapport au système de protection sociale par répartition:

    * Amélioration et revalorisation des minima sociaux et de toutes les
      allocations grâce à une augmentation des cotisations patronales qui
      rééquilibreraient la hausse constante des cotisations salariales et
      permettraient aux salariés de se réapproprier la plus-value et les
      bénéfices patronaux ;
    * Elargissement de l'accès aux allocations sociales pour tous (Sécu,
      retraites, assurance chômage...) dans une harmonisation par le haut des
      conditions d'accès, au bénéfice de tous les salariés précaires et de tous
      ceux qui ont été exclus de la solidarité interprofessionnelle
    * Unification des caisses pour une caisse unique de protection sociale,
      contre tous les risques sociaux ;
    * Réappropriation de la gestion des caisses Sécu, retraites,
      assurance-chômage... par les seuls salariés (exclusion du patronat et
      refus du paritarisme)
    * Elections des administrateurs des caisses sur des mandats précis,
      révocables à tout moment Contrôlés par les salariés à la base.


Depuis la mise en place du système par répartition, les cotisations patronales
n'ont fait que diminuer, alors que les cotisations salariales, elles, ont connu
une forte hausse, faisant reposer tout l'effort sur le dos des seuls salariés
et conservant aux patrons leurs précieux profits. (Ces vingt dernières années,
le taux de cotisation patronale a augmenté de 1.8%, celui des salariés de 8.2% !)
