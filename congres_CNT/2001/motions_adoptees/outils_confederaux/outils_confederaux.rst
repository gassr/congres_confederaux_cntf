
.. _motions_outils_confederaux_cnt_congres_toulouse_2001:
.. _outils_confederaux_2001:

==========================================================================================
Motion Outils confédéraux, 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
==========================================================================================

.. toctree::
   :maxdepth: 3

   fondation_histoire
   recueil_motions
