
.. _motion_55_2012:
.. _motion_55_2012_etpic30:

===================================================================================
Motion n°55 2012 : Combat syndicaliste en ligne (ETPIC 30 Sud)
===================================================================================
  

.. seealso::

  - :ref:`motions_etpic30`




Motion similaire à la motion 48 2010 ETPRECI35
==============================================

- :ref:`motion_48_2010`

Le Combat Syndicaliste
======================

- :ref:`combat_syndicaliste`


Argumentaire
=============

Le Combat syndicaliste connait des évolutions qualitatives importantes ces
dernières années mais sa diffusion reste trop limitée.

La vocation de l’organe mensuel de la CNT est de connaitre une diffusion la
plus large possible, comme outil de presse et de propagande.

Parallèlement, le web devient un outil incontournable de la diffusion de
l’information.

Si nous soutenons le maintien de la version papier du Combat syndicaliste
pour des raisons d’accessibilité à tous, nous souhaitons que la CNT étudie
la mise en œuvre d’un site web spécifique dédié au Combat syndicaliste
du type : http://www.combat-syndicaliste.org

Ce nom de domaine devrait être actuellement saisissable techniquement
par la CNT.

Motion
======

Le Congrès désigne une commission confédérale chargée de réaliser la mise
en ligne internet de l’édition mensuelle du Combat syndicaliste.

La commission confédérale « CS en ligne » est chargée d’étudier la
faisabilité et l’élaboration de la mise en ligne du Combat syndicaliste
sous le nom de domaine www.combat-syndicaliste.org (ou autre si indisponible)
dans les délais les meilleurs.

L’ensemble des articles du Combat syndicaliste y seront accessibles
gratuitement selon une mise en page spécifique.

La question de l’interactivité potentielle (commentaires des articles,
prises de contact) fera l’objet d’une étude spécifique.

L’ensemble des travaux de la commission sont placés sous le contrôle et la
validation des CCN qui suivent le Congrès.

Jusqu’au prochain Congrès confédéral, les CCN pourront observer les
candidatures et le mandatement de webmasters confédéraux chargés de
l’animation du site web dédié au Combat syndicaliste.

Les internautes et lecteur du CS en ligne seront invités sur ce site
internet (et sur le site web confédéral) à soutenir financièrement le
Combat syndicaliste.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°55          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
