
.. index::
   pair: Relevé de décisions; Congrès CNT (Angers)


.. _releve_decisions_xxxiii_congres:

===========================================================================================================================
Relevé de décisions du XXXiiie Congrès CNT 2014 d'Angers (49, Maine et Loire, Pays de La Loire) du 12 au 14 décembre 2014
===========================================================================================================================

.. seealso::

   - :ref:`congres_statuts_CNT`
   - :ref:`fonctionnement_congres_confederal`


:download:`Releve_de_decisions_Congres_2014.pdf`
