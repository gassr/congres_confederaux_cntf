
.. index::
   pair: Motion 7 ; 2014

.. _motion_7_2014_etpic30:

==================================================================================
Motion n°7 2014 : Examen des candidatures aux mandats confédéraux (ETPIC 30 sud)
==================================================================================

.. seealso::

   - :ref:`motions_etpic30`




Argumentaire
=============

Le :ref:`31ème Congrès Confédéral de Saint Étienne <congres_CNT_2010_saint_etienne>`
a vu apparaître pour la première fois, une candidature groupée au mandat
des différents secrétariats confédéraux, présentée comme indivisible,
émanant d’une seule et même localité (l’agglomération lyonnaise).

Cette situation nouvelle, inédite, a entraîné de longues et difficiles
discussions au coeur d’un Congrès déjà conséquemment alourdi par des
questions difficiles.

Le Congrès n’a pas souhaité traiter cette présentation collective
de mandats, au profit d’un examen individuel des candidatures.

Le vote de cette décision ne fut néanmoins peu ou pas significatif
quant à la réelle volonté des syndicats CNT présents: 32 pour,
24 contre, 2 abstention, et 10 ne participe pas au vote...

Ce vote traduit donc un sentiment collectif assez partagé.

Face à la volonté persistante de ces candidats à n’agir qu’entre eux,
ou face à une « concurrence » de candidatures totalement déséquilibrée,
toutes les autres candidatures individuelles émanant de différents
territoires se sont désistées.

Nous estimons que l’examen par le Congrès de candidatures collectives:

- Privilégierait les grosses agglomérations, plus à même de pouvoir
  faire émerger des équipes par des candidatures groupées ;
- Pourrait induire une logique de concurrence contraire à l’esprit
  d’intérêt général propre au mandement confédéral.
- Ne favoriserait pas l’élection d’équipes de candidats issus de
  différents territoires ;
- Pourrait conduire à une « logique de tendance » par l’association
  de programmes aux candidatures groupés (vu en 2010).
- Pourrait être sous-tendues de logiques affinitaires contraire à
  l’éthique syndicale.

Ainsi, l’examen de candidatures collectives nous apparaît ainsi
contraire, en l’état numérique actuel de la CNT, aux équilibres
de la vie démocratique de la CNT et à l’investissement d’intérêt
général inhérent à l’exercice d’un mandat confédéral.


Motion
=======

Le Congrès Confédéral de la CNT examine individuellement les
candidatures aux différents mandats confédéraux, même lorsque ces
dernières sont présentées ou annoncées de façon collective.

Chaque candidatE est invitéE à présenter directement, ou par le biais
de syndicat d’appartenance, les motivations de candidature en terme
de perspective d’investissement et de travail en commun.

En vue de favoriser un meilleur ancrage territorial et une multiplicité
d’approches, le Congrès Confédéral tend à privilégier le mandatement
de personnes émanant de différents territoires et de différents syndicats.


Amendement
===========

.. seealso::

   - :ref:`amendement_motion_7_2014_stics13`


Contre-motion
=============

.. seealso::

   - :ref:`contre_motion_7_2014_interco73`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°7 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
