
.. index::
   pair: Motion 2 ; 2014

.. _motion_2_2014_ste75:

===========================================================================
Motion n°2 2014 : Disparition du vote par procuration (STE 75, statutaire)
===========================================================================


.. seealso::

   - :ref:`motion_1_2012`
   - :ref:`motions_ste75`




Argumentaire
=============

Nous savons que cette :ref:`motion a déjà été débattue <motion_1_2012_ste75>` lors du dernier congrès.

Elle n' a pas été approuvée à une courte majorité. Mais nous pensons
qu'elle participe de la définition de ce qu'est un syndicat à la CNT.

C'est à dire avant tout une assemblée générale d'individus qui débattent
pour former une voix. Celle-ci bien sûr va s'exprimer en tant qu'actrice
des luttes et en faisant la propagande de celles-ci et des moyens qui
caractérisent la CNT pour les mener.

Pour cela, elles et ils vont se rassembler par localité et/ou par
branche d'industrie en fonction de leur réalité. Puis, autant que
possible, vont s'unir avec d'autres pour mener ces luttes.
Et, à intervalles réguliers, des rencontres entre syndicats de la CNT
sont organisées, par région, par fédération et en confédération.

Lors des congrès de la CNT, il arrive qu'un syndicat donne procuration
à un autre pour transmettre ses décisions d'AG concernant les motions
proposées.
Or nous avons constaté que c'étaient bien ces rencontres qui étaient
enrichissantes avec les débats, les commissions ou les ateliers de
travail qui les caractérisent. Ce sont aussi elles qui nous permettent
de faire vivre notre autogestion et d'enrichir notre pratique syndicale.
D'une richesse qui est en elle-même anticapitaliste.

Du coup, le vote de ces fameuses motions ne représente plus qu'un
fait mineur dans la vie de la confédération.

Bien souvent, un aboutissement logique où chaque adoption est prise à
une large (voire très large) majorité. Ce sont aussi souvent ces motions
là qui fonctionnent le mieux.
Nous avons conscience qu'il n'est pas toujours facile pour certains
syndicats d'envoyer des mandatéEs à chaque congrès.

Cependant, si les  décisions se prennent lors de rencontres entre
personnes mandatées, c'est pour qu'elles puissent être l'aboutissement
d'une réflexion commune et constructive.
Les amendements en sont l'expression.
Un mandat écrit, sans représentant du syndicat qui ait assisté
régulièrement à ses AG et qui puisse y rendre des comptes à son retour,
ne permet pas cela.

**Dans notre fédération (la FTE) et dans notre région (l'UR parisienne),
il n'y a plus de procuration**.
Cela n'empêche pas leur fonctionnement. Et n'oublions pas que pour
autogérer nos rencontres et autres congrès, il faut être là physiquement.
C'est aussi cela, la caractéristique de notre confédération. Nous avons
bien conscience que tous les syndicats veulent participer au moins par
procuration. Mais c'est alors à tous les autres de trouver un moyen de
les faire venir et ne pas leur laisser juste la miette du vote.

L'organisation du congrès doit aussi respecter les horaires prévus,
afin de permettre aux mandatés tributaires d'impératifs personnels
de rester jusqu'au bout.

Nous pensons que dans l'expression "un syndicat, une voix", le mot
voix doit être entendu dans les deux sens : **un vote et une parole
(une parole et un acte)**.

Motion (non applicable pour le 33ème Congrès confédéral)
=========================================================

Modification de :ref:`l'article 16 des statuts <article_16_statuts_CNT_2012>`

La phrase::

    Chaque déléguéE ne peut représenter exceptionnellement
    que trois Syndicats au maximum.

est remplacée par::

    Lors des congrès, chaque syndicat, pour participer aux votes
    et donc aux débats qui les précèdent, doit envoyer au moins
    unE mandatéE.

    Il ne peut plus donner procuration à un autre syndicat.


Note de la commission de préparation
=====================================

Les motions n°3, 4 et 5 ne seront soumises à la discussion et au vote
qu'en cas de non adoption de la motion n°2, puisqu'elles sont
contradictoires à cette dernière.


Amendements
============

.. seealso::

   - :ref:`amendement_motions_2_3_2014_stics13`

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°2 2014

       STE75
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
