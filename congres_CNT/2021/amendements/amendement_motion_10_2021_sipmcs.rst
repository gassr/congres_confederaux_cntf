
.. _amendement_motion_10_2021_sipmcs:

===================================================================================================================================================
Amendement N°3 à la Motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **SIPMCS**
===================================================================================================================================================

- :ref:`motion_10_2021_etpic30`
- :ref:`sipmcs:motions_sipmcs`


"La CNT charge donc la Commission confédérale antisexiste et
antipatriarcale pour l’égalité et l’équité de :..."

AMENDEMENT : Supprimer "équité"



.. figure:: sipmcs/amendement_10.png
   :align: center

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°5  |            |          |            |                           |
| SIPMCS                      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
