
=======================================
II – L’activité syndicale et militante
=======================================




Elle s’organise temporellement au coeur des entreprises et des fonctions
publiques grâce aux droits syndicaux : heures de délégation, congés syndicaux, ASA.

Nombres de nos camarades syndicalistes ne bénéficient pas de ces droits, soient
qu’ils n’exercent aucun mandat syndical ou de représentation du personnel (ou
qu’ils ne peuvent pas– dans les TPE par exemple), soit qui soient privés d’emploi,
soit qu’ils ne sont pas fonctionnaires (ASA acquises pour tous les agents).

Elle s’organise autour des entreprises, dans les mouvements sociaux, dans la
cité parfois grâce à ses mêmes heures, mais surtout, et avant tout, hors
temps de travail.

L’ampleur des heures de délégations (DS, DP, représentant du personnel au CE,
ASA), dédiée pour partie à l’action syndicale, est souvent insuffisante pour
celui qui investit pleinement ses mandats pour sa section syndicale, ou même
pour l’ensemble des salariés et agents de son entreprise ou de son
administration. C’est sans doute pour cela que la CNT n’a jamais souhaité
limiter leur utilisation.

Ces heures de délégations ne concourent pas à l’émergence d’un «permanentisme»
syndical en ce sens qu’elles sont limitées en temps, même par cumul, et
étroitement liées par définition à la prégnance de la technicité des mandats
concernés et de l’action syndicale de terrain.

Concernant les décharges syndicales (ou de service), qu’elles soient données
par un résultat électorale (fonction publique) ou par accord d’entreprise
(privé) elles revêtent un autre enjeu par leur quotas horaires, par ailleurs
nettement supérieurs dans les fonctions publiques.

Elles peuvent suffire à décharger d’activité un ou des permanents à temps plein.

Selon l’utilisation qui peut en être fait, ceux à quoi elles sont consacrées,
elles peuvent concourir à l’émergence du « permanentisme » à l’image d’autres
organisations syndicales qui leur confient souvent la direction de l’organisation.
