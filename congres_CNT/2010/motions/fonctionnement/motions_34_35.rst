


.. index::
   Institut de Formation Confédéral


.. _motions_34_35_2010:

=================================================================================
Motions n°34 et 35 2010 : Création d'un Institut de Formation Confédéral, STE 93
=================================================================================




Motion reprise par ETPICS57 et Chimie Bzh, 2012
================================================

- :ref:`motion_34_2012`
- :ref:`motion_35_2012`

Argumentaire
=============

La nécessité de former les adhérent-e-s et militant-e-s n'est pas nouvelle.

Ces dernières années, des syndicats CNT, des régions ont organisé des formations.
Nous pensons qu'il est maintenant possible de proposer la création d'un
« Institut de Formation Confédéral - CNT ». Celui-ci aurait pour mission de
recenser les besoins des syndicats, de proposer un « catalogue » de formations,
des animateurs, animatrices, voire d'assumer la publication de brochures.

Deux axes de travail peuvent déjà se mener en parallèle:

- 1er axe, formations à proposer aux adhérents, hors du temps de travail
  (en soirées, week-ends) qui pourraient s'organiser en régions et/ou au
  niveau national avec un planning à l'année. Resterait à chiffrer le coût des
  déplacements des participants et/ou animateurs.
- 2ème axe, formations sur le temps de travail. Stages de 1, 2 ou 5 jours. Cette option
  nous obligera à formaliser un partenariat avec une association d'éducation populaire
  agréée par l'Etat. En effet, aujourd'hui, seules les confédérations syndicales dites
  représentatives ont pu créer leurs instituts de formation et ainsi permettre à leurs
  adhérents du public comme du privé de prendre des congés formations sur le temps
  de travail.


Pour information, Solidaires a du se lier à l'association d'éducation populaire « Culture et Liberté » afin de faire
fonctionner son « Centre d'Etude et de Formation Interprofessionnelle » (CEFI- Solidaires)
Ces derniers mois, des militants de la CNT éducation avaient pris contact avec « Culture et Liberté » pour
envisager un partenariat. Il serait donc possible de le formaliser sous la condition que des militants de la CNT
adhèrent à « Culture et Liberté » (20 euros l'an). Il faudrait également que certains de nos « formateurs » fassent
des stages payants avec « Culture et Liberté » (1 ou 2 peuvent suffire; prévoir 100 euros la journée de stage). A
noter que ces infos datent. Il est évident qui si le congrès accepte le principe d'un partenariat avec cette
association, la Confédération sera informée des engagements à prendre ou non.



.. _motion_34_2010:

Motion n°34 2010 , Création d'un Institut de Formation Confédéral, STE 93
==========================================================================

Le congrès confédéral mandate le syndicat des travailleurs de l'éducation de
Seine St Denis pour mettre en place un «Institut de Formation Conféréral-CNT».

Il travaillera pour mettre en place des sessions sur les thèmes d'actualités :
retraites, salaires, contrats à durée déterminée...
Mais aussi: histoire sociale, solidarité internationale...

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°34          |            |          |            |                           |
| STE 93               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`contre_motion_34_2010_ess34`


.. _motion_35_2010:

Motion n°35 2010 , Création d'un Institut de Formation Confédéral, STE 93
==========================================================================

Le congrès mandate le syndicat des travailleurs de l'éducation de Seine St Denis,
au nom de la Confédération, pour finaliser un partenariat avec l'association
« Culture et Liberté » afin de permettre aux adhérents de la CNT de se former
sur le temps de travail. La Confédération sera tenue régulièrement informée des
démarches entreprises et des engagements à prendre.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°35          |            |          |            |                           |
| STE 93               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`amendement_motion_35_2010_etpreci35`
