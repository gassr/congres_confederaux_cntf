

.. _motion_18_2010:

=============================================================
Motion n°18 2010 : Région et syndicats seuls, INTERPRO CNT 31
=============================================================



Argumentaire
============

Nous pensons qu'il n'est pas cohérent de regretter parfois une faible présence
des Union Régionales :term:`C.N.T` lors des :term:`C.C.N` et de ne pas laisser
la possibilité aux syndicats isolés dans une région de participer à ces réunions
confédérales importantes. Pour rappel, une :term:`U.R` doit être constituées
d'au moins, 3 syndicats dans 2 départements (?).

Aussi nous proposons que les syndicats isolés soient intégrés à la région la plus proche
selon leur appréciation.

Motion
======

Les syndicats n'étant pas constitués en :term:`U.R`, peuvent participer au :term:`C.C.N` en mandatant une personne représentant
les syndicats de cette région.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°18          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
