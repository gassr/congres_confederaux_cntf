


.. _odj_xxxiv_congres:

=======================================================
Ordre du jour XXXIVe Congrès CNT 2016 de Montreuil
=======================================================

.. seealso::

   - :ref:`congres_statuts_CNT`
   - :ref:`fonctionnement_congres_confederal`





Vendredi 12 décembre
====================



Samedi 13 décembre
===================



Dimanche 14 décembre
=====================



Cloture du congrès prévue à 15H
================================
