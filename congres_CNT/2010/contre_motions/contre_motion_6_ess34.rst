

.. _contre_motion_6_2010_ess34:

======================================
Contre-motion à la motion N°6 (ESS 34)
======================================

Si le Bureau Confédéral est informé par écrit (mail, courrier) d'une mise en sommeil
d'un syndicat par unE ou plusieurs de ses membres, et après confirmation par l'UR concernée,
il peut être prononcé sa dé­labellisation.

En cas d'information de mise en sommeil, avant tout acte de délabellisation, la
trésorerie confédérale et le Bureau confédéral pourront relancer les syndicats
concernés de la façon suivante:

- Le Bureau Confédéral relance par mail ou courrier simple et attente d'un mois
  minimum pour la réponse (par courrier, mail, téléphone...)
- Si aucune réponse (ou peu satisfaisante), une relance par courrier postal par
  le Bureau Confédéral à l'adresse du syndicat et attente d'un mois minimum pour
  la réponse (par courrier, mail, téléphone...)
- Si aucune réponse (ou peu satisfaisante), une dernière relance du Bureau confédéral
  par courrier postal avec accusé de réception. Attente d'un mois minimum pour la réponse
- Si aucune réponse, envoi dernier courrier du BC avec accusé de réception annonçant
  la dé­labellisation CNT. L'UR concernée, si elle existe, est informée simultanément,
  ainsi que l'ensemble des UR par circulaire confédérale (et par la liste web syndicats).

  Dans un délai de 2 mois, lorsqu’il y a opposition d'une union régionale, le BC
  doit suspendre sa décision, qui doit être soumise au prochain C.C.N.

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°6 |            |          |            |                           |
| ESS 34                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+


.. seealso::

  - :ref:`motion_6_2010`
