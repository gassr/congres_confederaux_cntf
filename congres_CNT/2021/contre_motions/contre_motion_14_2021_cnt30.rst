

.. _contre_motion_14_2021_cnt30:

=================================================================================================================
Contre motion N°1 à la motion n°14 2021 **Financement confédéral des groupes Femmes Libres** par CNT ETPIC30
=================================================================================================================

- :ref:`motion_14_2021_syndicats_42`

Autres motions ETPIC30
========================

- :ref:`motions_etpic30`


Contre motion
=============

Révolutionnaires, nous cultivons les germes d'une autre société, une autre
société de femmes et d'hommes uni·e·s dans l'équité, l'égalité et le
respect mutuel.

Notre approche de la pratique syndicale se veut à dessein inclusive,
éducative et émancipatrice.
De nombreux militants sont sincèrement et logiquement impliqués dans la
lutte antipatriarcale et antisexiste aux côtés des militantes, considérant
que l’union fait ici la force.

Si nous n'ignorons pas le recours possible à la non mixité et ses vertus,
nous estimons que cette lutte doit se mener en mixité.

Au sein de notre syndicat, nous privilégions le choix de poursuivre
notre combat antipatriarcal et antisexiste sous couvert de la diversité
de genre, et par voie de conséquence en mixité
(cf. :ref:`motion_20_2021_etpic30`).

Au-delà de ces différentes approches militantes, les syndicats présentant
cette motion ne proposent aucune motion d'orientation visant à asseoir
la création de ces groupes, d'en définir les buts, la vocation, la composition,
la structuration, le mandatement et surtout le mode de rattachement à la
Confédération (cf. :ref:`motion_20_2021_etpic30`).

Dans notre pacte confédéral, les militant·e·s de la CNT et leurs syndicats
sont libres d'adopter les moyens d'organisation et de regroupement
qu'ils souhaitent pour parfaire notre combat commun.

Le soutien financier à ces groupes autolabellisés « CNT Femmes libres »
incombe à leurs membres et aux syndicats qui les soutiennent.

Nous demandons que la Confédération refuse d'instaurer une cotisation
obligatoire qui s'imposerait à chacun·e· de nos adhérent·e·s ainsi
qu’à ceux et celles des autres syndicats.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°14 |            |          |            |                           |
| CNT30                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
