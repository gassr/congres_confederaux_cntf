

===============
Administration
===============


Article 5
=========

La Fédération est administrée par un bureau dont les membres sont élus à la
majorité du congrès fédéral et responsables devant lui. Afin d'alléger le
fonctionnement administratif, le congrès peut décider de charger un syndicat
adhérent à la Fédération de prendre en charge son bureau.

Le nombre de membres du bureau est fixé en congrès fédéral. Le bureau est
composé d'au moins deux membres : un secrétaire et un trésorier. Les membres
du bureau sont élus pour deux ans. Ils ne sont rééligibles qu'en cas de carence
de candidature.

Article 6
=========

Les membres du bureau ne doivent occuper aucune responsabilité dans une
organisation politique, philosophique ou religieuse. Ils sont révocables par
le congrès ordinaire de la Fédération ou par un congrès extraordinaire.

Article 7
=========

Le bureau se réunit régulièrement autant de fois qu'il en a besoin. Il est tenu
de faire connaître son mode de fonctionnement. Un compte-rendu de séance
mentionnant les travaux effectués peut être établi et conservé dans les
archives.


Article 8 (attributions du secrétaire)
========================================

Le secrétaire est chargé de la correspondance et des autres travaux afférents
au bon fonctionnement de la Fédération, procès-verbaux, rapports, qui seront
envoyés à chaque syndicat adhérent ; il sera secondé par autant d'ajoints que
le congrès fédéral le jugera nécéssaire.

Article 9: ( attributions du trésorier )
========================================


Le trésorier est chargé d'encaisser les cotisations fédérales, de faire les
paiements et d'opérer le placement des fonds disponibles dans les endroits
désignés par le Congrès Fédéral ; il tiendra le livre des recettes et dépenses;
il dressera à chaque Congrés Fédéral, un bilan de la situation financière qui
sera envoyé à tous les syndicats fédérés ; il sera secondé par autant
d'adjoints que le congrès fédéral le jugera nécéssaire.

Article 10
==========

Le secrétaire, le trésorier, leurs adjoints, ou toute autre adhérent,
concourrant à leurs tâches ne sont ni permanents, ni rétribués, ce qui conforte
le caractère autogestionnaire de la Fédération.
