
.. _congres_CNT_2016_amendements:

=========================================
Amendements du XXXIVe congrès CNT 2016
=========================================

.. toctree::
   :maxdepth: 2


   amendement_motion_3_2016_stics13
   amendement_motion_6_2016_sinr44
