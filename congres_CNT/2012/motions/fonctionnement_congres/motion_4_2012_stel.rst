
.. _motion_4_2012:
.. _motion_4_2012_stel:

==========================================================================================
Motion n°4 2012 : Disparition des quitus, STEL [adoptée]
==========================================================================================
  
.. seealso::

   - :ref:`stel`




Préambule
=========

Nous demandons aussi qu'elle soit votée en début de congrès afin qu'elle puisse
s'appliquer dès ce congrès.

Argumentaire
============

Le STEL est conscient de la bonne volonté que chacun-e met à accomplir les
tâches qui lui sont confiées lorsqu’il ou elle prend un mandat confédéral,
et de la difficulté psychologique dans laquelle sont parfois mise les
mandaté-e-s à l’heure du vote des quitus en début de congrès.

Par ailleurs, il lui apparait que le principe des mandats tournants contrôlés
en permanence par l’ensemble des syndiqué-e-s permet un retour perpétuel sur
le mandat et le-la mandaté-e. Si ce-cette dernier-ère outrepassait son mandat
ou n’en respectait pas la teneur, il-elle serait démandaté-e sans attendre le
congrès confédéral suivant.

De plus, il nous semble irrationnel qu'un-e mandaté-e puisse être maintenu-e
pendant tout son mandat sans que celui-ci ne soit remis en cause pendant deux
ans et que le quitus lui soit refusé lors du congrès confédéral suivant.

Enfin, nous considérons que le quitus est donné de fait si le-la mandatée est
élu-e lorsqu'il-elle se représente une seconde fois sur le mandat.
En conséquence, le vote des quitus apparait au STEL comme superflu et inutile
et propice à provoquer des tensions inutiles.

Le STEL rappelle cependant l’importance d’un bilan de mandat réalisé par chaque
mandaté-e envoyé au moins un mois avant le congrès. Lors du congrès, les
syndicats qui le veulent peuvent exprimer leur avis lors d'un tour de parole
organisé sur le bilan de chaque mandaté-e.

Motion
======

Lors des congrès confédéraux, les votes des quitus sont supprimés.

Un mois avant le congrès, chaque mandaté-e envoie un bilan de son mandat.
Un tour de parole sur chaque mandat est organisé, les syndicats donnent alors
leur avis sur le mandat.

Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°4           |            |          |            |                           |
| STEL                 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Contre-motions
==============

- :ref:`contre_motion_4_2012_ess34`
