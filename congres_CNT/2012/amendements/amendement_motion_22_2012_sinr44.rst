
.. _amendement_motion_22_2012_sinr44:

==========================================
Amendement à la motion 22 (SINR44)
==========================================

.. seealso::

   - :ref:`amendements_sinr44_2012`
   - :ref:`motion_22_2012_ptt95`



Motivation : Clarification du passage
======================================

Amendement
==========

Le Congrès de Metz se prononce pour l’organisation d’une présence confédérale
sur...

Affirmer une démarche internationaliste par rapport aux délocalisations en
posant la revendication du salaire égal aligné sur le plus favorable au sein
des mêmes firmes sans distinction de zones géographiques.



Vote indicatif
==============

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°22 | 2          | 32       | 4          | 12                        |
| SINR44                      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
