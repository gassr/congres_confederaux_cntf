
.. index::
   pair: Motions ; 2014

.. _congres_CNT_2014_motions:

========================
Motions Congrès CNT 2014
========================

.. toctree::
   :maxdepth: 2

   1__fonctionnement_congres/fonctionnement_congres
   2__strategie_syndicale/strategie_syndicale
   3__fonctionnement_organique/fonctionnement_organique
   4__annexe/annexe


- :ref:`contre_motions_congres_2014`
- :ref:`congres_CNT_2014_amendements`
