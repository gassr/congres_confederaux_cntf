

================================
Commissions d'études techniques
================================

Article 16
==========

Des commissions d'études techniques peuvent être adjointes au bureau fédéral
pour la mise au point des questions revendicatives dans les différents secteurs
des transports.

Les commissions n'ont aucun pouvoir de décision. Un Compte-rendu de travail
effectué lors de chaque réunion peut être établi dans les mêmes conditions
que les procès-verbaux ou les Comptes-rendus du bureau.

Les commissions sont constituées par des adhérents de la CNT.
