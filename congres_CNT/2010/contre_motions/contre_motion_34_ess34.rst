
.. _contre_motion_34_2010_ess34:

=======================================
Contre-motion à la motion N°34 (ESS 34)
=======================================


Une commission sera mise en place pour travailler sur la création d'un institut
de formation et présenter un projet concret et chiffré à la Confédération
au prochain congrès. (Remplace la :ref:`motion n°34 <motion_34_2010>`)


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion 34       |            |          |            |                           |
| ESS 34                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso:: :ref:`motion_34_2010`
