

.. index::
   ! Permanents
   pair: Permanents ; politiques
   pair: Permanents ; syndicaux


.. _permanents_etpic30:

================
I Les permanents
================

A - Typologie des mandats
=========================

Nous pouvons distinguer deux types de permanents au sein des organisations
syndicales en fonction de la définition de leurs mandats, de leur capacité
décisionnelle.

Nous proposons les définitions suivantes:

Les permanents politiques, dits ``permanents syndicaux``
--------------------------------------------------------

Ce sont des adhérents d’une organisation syndicale assurant un mandat
décisionnel et/ou de représentation politique et syndicale, de propagande, et
d’organisation de la vie militante syndicale à temps plein ou partiel
(au delà d’un mi-temps).

Les permanents techniques
-------------------------

Ce sont des travailleurs salariés qui assurent un travail à caractère technique
à temps plein ou partiel pour le compte une organisation syndicale.
Ils n’ont par conséquent et usuellement aucun mandat de décision et/ou de
représentation, de propagande, et d’organisation compte tenu de leur position
de salarié et de la position employeur de l’organisation syndicale.

B - Statuts & Modalités de rémunérations
========================================

Les permanents peuvent soit être salariés par une organisation syndicale, soit
bénéficier d’une « décharge syndicale » à temps plein, voire à temps partiel.
Les permanents bénéficiant de décharges syndicales (ou de services) ne
sont dès lors plus tenus d'occuper leur poste de travail d’origine et peuvent
militer pour leur organisation syndicale, tout en percevant leur salaire par
leur employeur d’origine.
Ils n’entretiennent donc aucun lien de salariat direct avec leur organisation
syndicale.

Dans la fonction publique, les Etablissements Publics Administratifs (EPA ex :
INRAP, CNED, IGN, etc.), les Etablissements Public à caractère Industriel et
Commercial (EPIC ex : SNCF, INA, etc.) & ex-EPIC à capitaux public (La Poste,
GDF, EDF, etc.), les « décharges de service » sont accordées en fonction des
critères de représentativité de chacune des organisations syndicales découlant
des résultats aux élections professionnelles. En outre, en cas de fin du
détachement, la personne bénéficie d’un droit au retour dans son ancienne
entreprise.

Dans « Les syndicats en France » (La Documentation française, 2007) Dominique
Andolfatto estime à 40 000 le nombre de personnes bénéficiant ``décharges de
service`` de la fonction publique, chiffre dans la fourchette basse selon lui.

De nouvelles dispositions législatives devraient venir modifier le calcul et la
dénomination de ces décharges syndicales dans la fonction publique.

Dans le privé, ``les décharges de service`` bénéficiant aux permanents dépendent
d’un accord spécifique entre un employeur et les syndicats concernés (syndicats
d’entreprise, fédérations, confédérations, UR, UD, UL, etc.) sans aucun
encadrement législatif dédié. Ce fait est réservé qu’à de très grosses entreprises.

Les permanents rémunérés par une organisation syndicale ont une
relation de salariat directe (subordination juridique).

A cela suppose  l’établissement par le syndicat employeur (ou Union de syndicats)
d’un contrat  de travail (CDD ou CDI) définissant la rémunération, la prestation
de travail,  les horaires de travail, le lieu d’exercice, etc.

En cas de rupture dudit contrat de travail le salarié ne bénéficie d’aucun
droit ultérieur autre que ceux de droits communs (Indemnisation Chômage).
