

.. _contre_motion_60_etpics57:

========================================
Contre-motion à la motion N°60 ETPICS57
========================================

.. seealso::

   - :ref:`motion_60_2012_ste75`



Argumentaire
============

La CNT met à disposition de ses mandatés confédéraux, fédéraux et régionaux
des adresses personnelles perso@cnt-f.org.

Vote
====


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motion n°60     |            |          |            |                           |
| ETPICS57                           |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
