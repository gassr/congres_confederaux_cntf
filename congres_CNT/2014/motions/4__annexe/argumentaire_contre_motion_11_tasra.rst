

.. _arg_motion_11_2014_tasra:

================================================================================================================================
Argumentaire à la motion n° 11 : Création d'une fédération du travail, de l'emploi et de la formation professionnelle (TAS-RA)
================================================================================================================================

Complément d'argumentaire à la motion n° 11 : Création d'une
fédération du travail, de l'emploi et de la formation professionnelle
(TAS-RA). (La motion reste inchangée)





Motion
======


Certains syndicats se posent la question suivante pourquoi n’intégrons nous pas la fédération santé
social ?
Tout simplement parce que cette question ne se pose pas ou plus. Nous ne sommes pas dans la santé
ni dans le social. Ce serait comme demander à la fédération PTT de faire partie de la fédération
santé social ou de demander à l'éducation de retourner à santé social !
Nous avons commencé à militer dans un syndicat santé social comme section du syndicat et nous
avons constaté qu'au fur et à mesure de notre développement nous nous éloignions de plus en plus
du syndicat compte tenu de l'importance que prenaît de nos problématiques professionnelles de
terrain.
Lorsque nous étions au syndicat santé social 69 les ordres du jour ne nous concernait pas
directement, par exemple nous n'étions, pas légitimes a prendre position sur la fusion patronale des
conventions collectives alors que nous ne connaissions pas les conséquences concrètes de ce
changement sur et dans le travail.
Sur nos problématiques, il nous fallait les expliquer longuement pour que soit compris nos relations
dans le travail avec notre hiérarchie ou encore les autres syndicats couvrant ce champ professionnel.
Si ces débats pouvaient nous enrichir individuellement, comme lorsqu'un camarade de l'éducation
raconte à un camarade de communication culture et spectacle ses luttes et sa réalité au travail, il
devenait nécessaire et primordial pour notre développement syndical. Plus le maintien au sein de
syndicats départementaux (et/ou d'intercos) santé/social constituait clairement un frein à notre
action par un décalage tant sur le fond (dans les problématiques abordées) que la sur la forme
(éclatement de notre secteur dans de multiples syndicats au lieu de permettre un regroupement).
C'est à partir de ce constat que nous avons créé un syndicat à part entière couvrant un champ de
syndicalisation propre à notre secteur d'activité. C'est à partir de notre sortie du syndicat santé social
69 que nous avons notamment pu développer une stratégie de syndicalisation et réalisé des
adhésions.
De fait si nous nous sommes implantés et développés dans notre secteur d'activité c'est précisément
parce que nous avons quitté santé/social et que nous nous sommes constitués en tant que structure
spécifique permettant de regrouper tous les adhérents de notre secteur et permettant de développer
nos problématiques propres. Sans cela un certain nombre de camarades ne nous auraient jamais
rejoints.Il est important que ce point soit parfaitement clair.
A noter que depuis que nous avons créé le syndicat TAS RA (il y a 4 ans à 2 militant sur le Rhône),
nous sommes maintenant présents sur nos lieux de travail dans le Rhône, la Savoie, la Haute-
Savoie, l'Ardèche, l'Alsace et nous sommes en relations avec des adhérents des syndicats interpros
qui peuvent difficilement participer avec nous à une réflexion commune sur nos problématiques et
pratiques professionnels ( Alsace, Côte d'or, Moselle,...) même si nous essayons de les y associer.
Par ailleurs, nous maintenons des contacts avec des collègues isolés un peu partout qui s'affichent
clairement comme sympathisant et qui pourraient nous rejoindre dans l'éventualité d'une
représentation nationale.
Il nous faut maintenant franchir un nouveau cap dans notre développement.
Inclure nos contacts dans une structure qui parte de leurs réalités professionnelles et leur faire
découvrir par le travail syndical nos valeurs, telles que l'autogestion ou l'action directe, nous

apparaît être le meilleurs moyens de les rapprocher à terme des problématiques
interprofessionnelles. D'autant que la fédération constituera un organe privilégié de communication
des informations confédérales auprès de ces adhérents.
Il nous semble que le moment est venu d'avoir cette représentation nationale, d'autant que notre
crédibilité syndicale est, compte tenu des luttes que nous menons depuis notre création, a son plus
haut niveau au sein du ministère du travail.

La fédération travail, emploi et formation professionnelle, nous permettra
notamment:

- De diffuser légalement sur l'ensemble du territoire national nos tracts, préavis notamment en
  élaborant et développant des outils de communication (site internet, revue périodique, base de
  donnée militante et juridique).
- De gérer directement les contacts isolés et d'organiser des journées de rencontre lors des congrès
  de la fédération entre adhérents isolés et syndicats pour déterminés les grande orientations de notre
  combats syndical et réfléchir sur les pratiques du métier et son sens.
- D’apparaître comme interlocuteur au sein de l'intersyndicale nationale au ministère du travail et
  d'y présenter nos propositions et nos modalités d'action.
- D'élargir à terme notre stratégie de syndicalisation vers l'emploi et la formation professionnelle.

Le rattachement de notre syndicat à la fédération santé social n'aurait d'autres
conséquences que d’alourdir notre fonctionnement et de nous entraver dans les
prises de décisions nécessaires au niveau national, ce qui nuira à notre
rapidité et notre efficacité d'action.

De plus, la fédération santé social ne fait pas parti de notre champs de syndicalisation. Il n'y a donc
aucune conséquence pour cette fédération et ses syndicats. Nous n'affaiblissons pas la fédération
santé social en créant notre propre fédération.
En effet, notre création n'aura pas pour effet d'opérer un transfert d'adhérents d'une fédération à
l'autre, nous voulons seulement élargir notre pouvoir d'agir syndicalement et toucher, pour qu'ils
nous rejoignent, des travailleurs qu'on ne voit ni dans les interpros aujourd'hui ni à la fédération
santé social.
Enfin, il convient de préciser que la création d'une nouvelle fédération ne constitue pas en elle
même un problème au sein de la confédération. Au contraire elle témoigne de notre dynamisme
syndical. Les difficultés de fonctionnement de certaines fédérations ne sauraient constitué un
argumentaire de nature à nous limiter dans notre expansion syndicale dans des secteurs d'activité en
pleine émancipation.
Nous espérons que les syndicats nous encourageront dans cette démarche de développement lors du
congrès.
