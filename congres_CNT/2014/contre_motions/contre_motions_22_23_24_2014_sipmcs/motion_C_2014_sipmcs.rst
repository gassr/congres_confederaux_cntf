
.. _motion_c_2014_sipmcs:

=======================
Motion C 2014 (SIPMCS)
=======================




Texte
=====

Le “secrétaire confédéralLE chargéE des relations avec les médias” est intégré
à la commission “site internet” de la motion B.

Ce qui étaient des communiqués destinés aux médias rédigés par les rélations
médias, deviennent des articles destinés à être lus par le plus grand nombre,
notamment sur le site et le CS.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion C 2014 SIPMCS

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
