
.. _motion_droit_des_femmes_cnt_congres_lyon_1996:

===============================================================================================================
Droit des femmes / égalité des sexes / antisexisme, 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
===============================================================================================================


Face au caractère spécifique de l’exploitation des femmes au travail, ou dans
leur fonction sociale, et dans le cadre de l’élaboration de son projet de
société, la CNT s'est dotée de "commissions femmes", commissions mixtes
chargées de collecter les faits portants atteinte aux droits des femmes,
d’élaborer une défense appropriée, ainsi que de contribuer à la réflexion sur
une société future faisant aux femmes une place égale a celle des hommes.

L’accès à l'IVG, toléré, de manière restrictive aux femmes il y a vingt ans
est aujourd'hui menacé en raison de l’action constante d'un militantisme
anti-IVG, au soutien varié et important, dont celui du mouvement américain
"pro-life" d'une violence et d'une puissance remarquables.

C'est dans cette optique que la CNT doit se charger:

- D’établir des liens avec le personnel hospitalier pour lutter efficacement
  contre les commandos et le lobbying anti-IVG ;
- D'assurer un bon environnement à l’accès des femmes à l’IVG par l’élaboration
  de réseau d’écoute et de soutien ;
- D'affirmer aux femmes que l’IVG dans de bonnes conditions médicales et
  psychologiques est un droit.
- D'impulser la défense de ce droit chaque fois qu’il est remis en cause ;
- De favoriser l’information sur les moyens de contraception et leur diffusion.
