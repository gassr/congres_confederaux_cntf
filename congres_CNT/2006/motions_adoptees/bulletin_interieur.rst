

.. _motions_bulletin_interieur_cnt_congres_agen_2006:

=============================================================================================
Motions Bulletin intérieur de la CNT  29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
=============================================================================================

Mise à disposition du Bulletin Intérieur sur l’intranet
=======================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 48         | 6        | 9          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le bulletin intérieur est mis à disposition gratuitement sur l'intranet
confédéral, sans restriction.


Diffusion gratuite du Bulletin Intérieur
========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 50         | 2        | 9          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----


La version papier du bulletin intérieur est envoyée gratuitement à chaque
syndicat qui le souhaite et cette activité est désormais prise en charge
financièrement par la trésorerie confédérale.
