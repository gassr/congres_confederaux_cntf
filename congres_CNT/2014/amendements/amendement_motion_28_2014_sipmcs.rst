
.. _amendement_motion_28_2014_sipmcs:

==========================================================================================================================================================
Amendement à la motion n°28 2014 **Ouvrage de synthèse permanent - 'CNT Fonctionnement & Orientations – Résolutions confédérales en vigueur'** (SIPMCS)
==========================================================================================================================================================

- :ref:`motion_28_2014_etpic30`

Préambule
=========

Nous considérons que ce document ne doit pas rester interne mais être au
contraire un **outil externe de présentation de ce qu’est la CNT** de par ses
décisions d’orientations et de fonctionnement en congrès confédéral, ce qui
constitue l’essence même de notre syndicalisme.

Nous n’avons pas à ``cacher`` ces décisions mais au **contraire à les mettre
en avant sous forme d’une brochure publique**.

Amendement
===========

- Ajouter « et externe » dans la première phrase : « un document de synthèse
  interne et externe ».
- Ajouter comme dernière phrase «Cette brochure sera également mise à
  disposition publiquement tant sur le site confédéral que en format papier
  à prix libre».

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°28 2014 <motion_28_2014_etpic30>`

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
