

.. _motions_strategie_syndicale_cnt_congres_saint_denis_2004:

=============================================================================================
Motions "stratégie syndicale" 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=============================================================================================

Développement du secteur privé
==============================

L'ensemble des syndicats de la CNT s'engage à mettre en œuvre le développement
du secteur privé au sein de la CNT.

Cela passe par une propagande particulière. Mais c'est aussi et surtout un
travail quotidien de suivi juridique et revendicatif, de formation ainsi que
de structuration qui devra être assumé par l'ensemble des syndicats, des Unions
Locales ou Départementales dans un esprit inter corporatiste.

Pour ce faire, l'ensemble des structures CNT du secteur privé (fédérations et
syndicats syndiquant des travailleurs du secteur privé) est appelé à constituer
une coordination.
Cette coordination permettra un échange concret d'expériences CNT dans le privé.

Elle permettra d'établir une stratégie et des campagnes syndicales communes à
destination du secteur privé.

Cette coordination se dotera d'un secrétariat qui assurera principalement la
constitution de circulaires. Jusqu'à nouvel ordre ces circulaires seront
financées par la confédération. Ces circulaires seront composées de tracts et
autres contributions en provenance des structures CNT du privé.
Cette circulaire sera envoyée à tous les syndicats de la CNT afin qu'ils s'en
inspirent et répercutent les luttes du secteur privé sur leurs localités.


Elections professionnelles & instances de représentation
========================================================

Vote de la motion
-----------------

70 syndicats présents à jour de cotisations.

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 28         | 24       | 14         | 6                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Elections professionnelles & instances de représentation
========================================================

Les motions présentées au Congrès visant à autoriser les syndicats à se
présenter à n’importe quelle élection professionnelle nous semblent dangereuses
au regard du développement d’un syndicalisme révolutionnaire.

Les précautions rhétoriques d’usage sur **l’attachement aux principes du
syndicalisme révolutionnaire et autogestionnaire** n’y changent rien, car ce
n’est pas seulement pour nous, une question d’identité ou de principe, mais
bien plutôt une question de cohérence dans l’action.

Derrière l’apparente neutralité des moyens employés (en l’occurrence l’élection
aux CE et CA) se trouve posée la question de la nature du rapport section
syndicale / entreprise que cela est susceptible de générer, ainsi que la
question de l’indépendance du syndicat dans le cadre de la lutte de classe.

L’article 1 des statuts confédéraux affirme que::

	la CNT a pour but (...) de  poursuivre, par la lutte de classe et l’action
	directe, la libération des  travailleurs qui ne sera réalisée que par la
	transformation de la société actuelle

La question du projet syndical ainsi que ses modalités matérielles et pratiques
de mise en œuvre se trouve posé. C’est ce projet qu’il est nécessaire de
débattre avant d’engager la CNT dans une démarche qui pourrait avoir des
conséquences importantes pour son avenir.

Etant donné qu’il existe déjà un certain nombre d’organisations syndicales, il
ne s’agit donc pas pour nous de simplement développer le syndicalisme, ce qui
peut se faire au travers d‘autres confédérations ou unions syndicales.

Mais il s’agit de développer un syndicalisme de lutte de classes et de s’en
donner les moyens tant du point de vue du développement de masse que de la
cohérence qui le permet. Peut-être même qu’une différenciation claire au niveau
de certaines pratiques, notamment au sein des entreprises et au niveau de la
précarité, pourront, dans un contexte de crise du syndicalisme, et dans un
avenir social difficile à cerner, être plus porteurs qu’une intégration
progressive qui créeraient la confusion et nous assimileraient aux dérives du
syndicalisme en général quelque soit le discours que nous porterions par
ailleurs.

**La seule question qui doit alors se poser est : qu’est-ce qui entrave ou permet
de développer à la fois un syndicalisme de masse et de lutte de classes ?**

Les différentes élections doivent donc être jugées en conséquence.

**Le système des élections professionnelles, loin d’être neutre, soit être
envisagé comme une politique globale de nos adversaires visant à intégrer le
syndicalisme pour mieux nier la lutte de classe au sein des entreprises et
de l’Etat-Nation.**

Dans ce cadre là, toutes les instances paritaires sous-entendues par une
idéologie co-gestionnaire doivent considérées comme des instances de
collaboration de classe.

Notamment, **ces organes permettent à l’employeur d’acter, devant les employés,
la reconnaissance de la démocratie d’entreprise comme le seule cadre légitime
du syndicalisme, ce que cautionne alors la section qui y participe**.

L’action directe est la base incontournable de notre syndicalisme (comme
l’affirmait déjà les Congrès de paris 93, 96, et 2001). A terme le danger qui
nous guette est bien celui-ci : déconnecter de plus en plus notre discours
révolutionnaire de notre stratégie pratique. Or, notre organisation n’est pas
syndicaliste avec par ailleurs une rhétorique révolutionnaire, elle est
syndicaliste révolutionnaire.

Ce qui signifie que notre spécificité n’est pas la propagande autour d’une
quelconque idéologie d’avant garde mais dans notre  positionnement pratique
dans les luttes et dans l’action revendicative par  rapport à l’Etat et au
patronat et au regard de notre propre fonctionnement,  dans un but déterminé.

Si tel n’était pas le cas, alors la nécessité même d’une confédération
Syndicaliste Révolutionnaire et Anarcho-syndicaliste serait à questionner.

On peut légitimement nous répondre que, dans une entreprise, pour pratiquer un
syndicalisme de luttes classes encore faut-il que la section syndicale existe.

La protection relative accordée aux représentants du personnel s'avère souvent
indispensable dans le privé pour éviter le licenciement. Les différentes
motions adoptées depuis 1993 (93, 96, 2001) répondent déjà à cette question en
faisant une distinction entre les principales institutions représentatives.

Ce qui veut dire que les raisons invoquées pour la présentation de candidats
aux élections professionnelles ont une grande importance. S'il est démontré
que dans telle entreprise c'est la condition sine qua non de la constitution ou
du maintient de la section, ce n'est pas la même chose qu'un argumentaire
portant sur l'obtention de moyens matériels. Car de tels mobiles ouvrent la
porte à la recherche de subventions ou de représentativité légale par le biais
de la participation à différentes instances d'encadrement du syndicalisme, qui
le lie à ses financeurs.

C'est l'indépendance syndicale qui est remise en cause et donc une grande partie
des conditions de possibilité du développement pratique d'un syndicalisme de
lutte de classe.

Enfin s'il s'agit de dire que les camarades de travail ne  comprennent pas que
nous n'allions pas au CE ou au CA, comprendraient-ils mieux que nous
condamnions les instances auxquelles nous participons et pour  lesquelles nous
appelons à voter (certaines sections SUD sont passées  spécialistes dans ce
type de schizophrénie) ?

Cette incohérence ne produisant pas plus de clarté d'action que la première,
nous préférons la première qui nous préserve de la perte progressive de notre
indépendance syndicale et d'une progressive déconnection de notre discours
révolutionnaire et de notre stratégie pratique.

Conclusion
----------

Comme chacun sait, cette question n'est pas neuve à la CNT et nous voudrions
dépasser ici l'opposition stérile entre les "idéologues sectaires anti-élections"
d'un côté et les "pragmatiques" qui ne penseraient qu'au bon développement
de la CNT de l'autre, pour reposer la question politique des élections en terme
de **stratégie syndicale**.

L'engagement dans la participation aux différentes instances cogestionnaires
dans le public et dans le privé, loin d'être un simple "moyen tactique, ponctuel"
nous engage dans une orientation stratégique qui concerne l'ensemble de la
confédération.

On ne saurait donc clore le débat au nom de "l’autodétermination"  des sections
syndicales.

Nos choix doivent dépendre du type d'organisation que nous voulons construire,
de ce sur quoi nous misons dans l'avenir, et comment nous allons nous y prendre
pour le faire collectivement.

Nous porterons lors des prochains congrès des propositions d'orientations
syndicales et structurelles, susceptibles de faire avancer le débat sur notre
projet syndical en termes pratiques, au regard duquel nous pourrons alors
collectivement réfléchir aux meilleurs moyens visant à le développer dans la
réalité des contradictions sociales et des rapports de production capitalistes.

Pour l'heure, il est préférable de limiter le recours aux élections
professionnelles, en attendant l'émergence d'un projet syndical plus cohérent
et plus détaillé, nourri des expériences sur le terrain et des cas concrets que
nous rencontrerons dans l'action.

**Nous réaffirmons les positions de la CNT sur la question des élections
professionnelles adoptées lors du Congrès de Lyon en 1996 et réaffirmées par
le Congrès de Toulouse en 2001" à savoir**::

	"... L'action directe est la base incontournable de notre syndicalisme. Elle
	 est applicable partout où nous pouvons être présents, dans l'entreprise ou
	 hors de celle-ci ".


Partant, la CNT opère une distinction entre les principales institutions
représentatives du personnel prévues par le code du travail :

En ce qui concerne en général les élections dans le privé
---------------------------------------------------------

S'agissant du comité d'entreprise (CE), celui-ci n'ayant de pouvoir de décision
qu'en matière de gestion des œuvres sociales et ne donnant que des avis au
patron dans la gestion de l'entreprise, est l'institution type de collaboration
de classes que rejette la CNT, d'autant plus que ses membres sont élus sans
mandat précis et sont irrévocables pendant deux ans.


Le délégué syndical sous le contrôle direct du syndicat et/ou de la section
syndicale, porteur de la revendication syndicale est, au contraire, un outil
de lutte que les militants de la CNT doivent utiliser.

La situation du délégué du personnel est plus complexe. Il est élu par les
travailleurs sans mandat précis et n'est pratiquement pas révocable.
Mais l'expérience acquise ces dernières années par les sections syndicales CNT,
montre son utilité dans certaines circonstances pour porter la revendication
et protéger les militants contre les licenciements, notamment dans les
entreprises de moins de cinquante salariés où n'existe pas le délégué syndical.

En ce qui concerne la fonction publique
---------------------------------------

Le problème ne se pose pas, puisque les droits minima sont acquis.
Par conséquent aucune section CNT ne s'y présente.


Pour ce qui est des cas particuliers dans le privé
--------------------------------------------------

S'il existe des cas ou le C.E. s'avère être, dans telle entreprise donnée, le
seul et unique moyen de créer ou de préserver la section syndicale, le
caractère d'exception de cette situation nécessite de réaffirmer la règle
générale, qui pose le refus de la cogestion et la considère comme néfaste à
l'action syndicale elle-même, à moyen et long terme. Pour traiter efficacement
ces cas très particuliers, la CNT devra se doter des outils de contrôle
préalable et des moyens d'effectuer un suivi et un bilan systématique.

Enfin
-----

Une fois qu'on a affirmé le refus de la cogestion, il ne suffit pas de le
décider pour que cette stratégie syndicale s'applique dans la réalité et
surtout porte des fruits. Une fois que nous avons parlé d'action syndicale
directe, il est nécessaire de définir de quoi il s'agit précisément et de la
promouvoir de manière concrète et pratique.

C'est pourquoi, la CNT doit forger suffisamment d'outils de formation,
d'analyse, d'information et d'échange entre syndicats, pour donner à l'ensemble
des camarades les moyens de mener à bien, en toute circonstance, la
construction sur le terrain de ce syndicalisme révolutionnaire d'action
directe de masse et de classe.
