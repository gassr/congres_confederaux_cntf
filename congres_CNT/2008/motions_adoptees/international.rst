
.. _motions_international_cnt_congres_lille_2008:

=======================================================================================
Motions "international" 30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
=======================================================================================

Renforcer la solidarité internationale
======================================

67 syndicats à jour de cotisations / 47 présents.

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 32         | 1        | 13         | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Sur la base du travail d’approfondissement de ses contacts internationaux de
ces dernières années et sur la base de la motion de stratégie internationale
du congrès d’Agen, la CNT donne comme mandat au Secrétariat international de
poursuivre dans cette voie.

La finalité de cette stratégie est, à moyen et à long terme, la création d’une
structure permanente de solidarité internationale, outil indispensable pour
affronter efficacement et de manière coordonnée le système capitaliste.
