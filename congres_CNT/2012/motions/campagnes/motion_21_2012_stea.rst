
.. _motion_21_2012:
.. _motion_21_2012_stea:

===============================================
Motion n°21 2012 : Campagne des 6 heures STEA
===============================================
  
.. seealso::

   - :ref:`stea`
   - :ref:`motions_stea_2012`



Argumentaire
============

Constatant que la motion « 6 heures de travail » proposée par l'UR Alsace a été
adoptée au CCN de Paris de juin 2012, cela nous confirme que l'idée d'une
campagne sur la répartition égalitaire du temps de travail est pertinente.

Pour interpeler les syndicats de la Confédération et les inviter à participer
à cette campagne, nous proposons qu'un groupe de travail soit créé, qui soit
donc en lien avec le Secrétariat international et le Bureau confédéral.

Revendiquer la journée de 6 heures, la semaine de 30h et les 32 annuités de
travail, c'est reprendre l'initiative après des années de luttes au cours
desquelles nous n'avons pas su défendre les acquis des travailleu-r-ses
victorieu-x-ses du siècle dernier. C'est lancer des dynamiques ofensives pour
promouvoir un projet de société réaliste en rupture avec le capitalisme.

C'est un mot d'ordre dont la portée révolutionnaire est évidente lorsqu'elle
s'inscrit dans notre lutte pour le partage égalitaire du temps de travail et
des richesses.

De plus, dans presque tous les pays, le mouvement ouvrier a lutté à un moment
ou à un autre pour la journée de 8 heures. C'est pourquoi nous pensons que ce
projet aurait une véritable résonance pour le mouvement ouvrier au niveau
international. Face aux divisions (du travail et de son temps, nationales,
ethniques, religieuses, culturelles) qui marquent la classe ouvrière, recréons
une cohérence de classe internationale pour être plus fort-e-s contre les
classes dirigeantes du monde entier.

Dans l'expectative d'une adoption de la motion, nous proposons que le groupe
de travail se réunissent d'emblée au congrès pour entériner son existence et
débuter un travail sur la campagne.

Si les syndicats le souhaitent, des argumentaires plus complets ont été
envoyés sur la liste syndicat.

Motion
======

Afin de mener à bien la campagne sur les six heures de travail, un groupe de
travail constitué de plusieurs syndicats, d'un-e représentant-e du Secrétariat
international et d'un-e représentant-e du Bureau confédéral est mis en place.

Il coordonnera la campagne, fera les liens au niveau international et gérera
la production de matériel de propagande.

Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°21          |            |          |            |                           |
| STEA                 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Amendements
============

- :ref:`amendement_motion_21_2012_etpic30`
