
.. _amendement_motion_22_2021_sest_lorraine:

===========================================================================================================
Amendement N°2 à la motion n°22 **Drogues, alcool et émancipation révolutionnaire** par **SEST Lorraine**
===========================================================================================================

- :ref:`motion_22_2021_etpics94`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`

Argumentaire
===============

Amendements
============

Rajout d’une phrase:

La lutte contre cette ``culture`` de l’alcool doit faire partie de nos réflexions
contre toutes les aliénations, et pour l’émancipation des travailleurs
et travailleuses. Parce que nous sommes contre tout ce qui affaiblit la
force et l’élan révolutionnaire des êtres humains.
C’est aussi contribuer a faire reculer la violence dans la société, entre
autres les violences sexistes.
La confédération se dote de tous les moyens pédagogiques et émancipateurs
pour informer le plus largement possible sur ce sujet (brochures, tracts,
affiches, conférences...).

.. raw:: html

    <ins>Il ne s’agit pas ici de moraliser l’individu.e, mais bel et bien d’assurer
    la prévention quant à la consommation tout en rappelant la dimension
    politique des légalisations et propagation de drogues.</ins>

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°22 |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
