

.. _motions_adoptees_xxxii_congres:

========================================================
Motions adoptées au XXXIIe Congrès CNT Metz 2012
========================================================




Fonctionnement du Congrès
=========================

- :ref:`motion_2_2012`
- :ref:`motion_3_2012`
- :ref:`motion_4_2012`
- :ref:`motion_5_2012`

Stratégie syndicale
===================

- :ref:`motion_10_2012`
- :ref:`motion_13_2012`
- :ref:`motion_14_2012`
- :ref:`motion_15_2012`
- :ref:`motion_16_2012`
- :ref:`motion_18_2012`
- :ref:`motion_20_2012`

Campagnes et commissions
========================

- :ref:`motion_21_2012`
- :ref:`motion_22_2012`
- :ref:`motion_24_2012`
- :ref:`motion_25_2012`
- :ref:`motion_26_2012`
- :ref:`motion_27_2012`
- :ref:`motion_28_2012`
- :ref:`motion_29_2012`
- :ref:`motion_30_2012`
- :ref:`motion_33_2012`

Fonctionnement organique
========================

- :ref:`motions_34_35_36_xxxii`
- :ref:`motion_37_2012`
- :ref:`motion_38_2012`
- :ref:`motion_39_2012`
- :ref:`motion_40_2012`
- :ref:`motion_46_2012`


Outils et développement
========================

- :ref:`motion_54_2012`
