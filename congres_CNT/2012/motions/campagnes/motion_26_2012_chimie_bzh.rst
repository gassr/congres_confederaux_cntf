

.. _motion_26_2012_chimie_bzh:
.. _motion_26_2012:


=====================================================================================================================
Motion n°26 2012 : Le nucléaire et la question énergétique (pétrole, gaz de schistes, éolien, fossile...) Chimie bzh
=====================================================================================================================
  
.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`



Argumentaire
============

La CNT considère la question énergétique comme directement liée au capitalisme
et au productivisme mais elle est aussi fondamentale au fonctionnement
d’une société autogérée.

C’est pourquoi la CNT prend position pour la sortie du nucléaire et crée une
commission ayant pour but d’affiner son argumentaire sur le sujet des énergies,
de leurs usages, de leurs impacts environnementaux, de leurs utilités...

Motion
======

Dans le but de freiner le développement du capitalisme et à titre transitoire
vers le communisme libertaire, la CNT revendique l'encadrement des hauts
revenus. À ce titre aucun revenu ne pourra être supérieur à 20 fois le
revenu minimal. Le diférentiel d'argent ainsi créé pourra donc être utilisé
pour les moyens de production, les services publics, la sécurité sociale,
les retraites.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°26          |            |          |            |                           |
| Chimie-bzh           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_26_2012_sse31`

Contre-motions
==============

- :ref:`contre_motion_26_2012_ess34`
