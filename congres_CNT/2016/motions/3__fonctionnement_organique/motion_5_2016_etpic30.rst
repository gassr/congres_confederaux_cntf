
.. index::
   pair: Motion 5 ; 2016

.. _motion_5_2016_etpic30:

============================================================================================
Motion n°5 2016 : Contribution financière pour le combat syndicaliste (ETPIC 30) [adoptée]
============================================================================================



Argumentaire
============

Le combat syndicaliste, organe de presse de la confédération est un
outil de propagande avant tout.

De ce fait, le but n’est pas d’être bénéficiaire avec le journal.
Cependant l’édition du journal et son amortissement sont un coût
conséquent pour la confédération. Si nous souhaitons faire vivre ce
journal et le conserver sous sa forme actuelle, il appartient à chacune
des structures de la CNT de le diffuser le plus largement mais aussi de
participer à minima à son financement.

Parution du journal :

- Coût total par mois et par année : 850 à 900 euros par mois (600 euros
  d’impression et 200 euros de frais d’affranchissement, auxquels
  s’ajoutent des frais « annexes »). Donc environ 10 200 euros par an.
- Entrées : entre 6000 et 7000 euros/an (abonnements et ventes).
- Déficit : 12000 euros sur 3 ans

Si chaque syndicat achète deux exemplaires du journal par mois, cela
permettrait d’atteindre l’équilibre au niveau de la trésorerie
confédérale. En l’espèce, sur une base de 82 syndicats, cela
ferait près de 4000 euros d’entrée par an


Motion
=======

Afin de pallier le « déficit » et d’amortir au mieux la publication du
**combat syndicaliste**, les syndicats devront contribuer financièrement
à hauteur de deux exemplaires par mois, soit 4 euros/mois.

Cette contribution sera inscrite sur le bordereau de cotisation confédérale.
Ces 2 exemplaires pourront, au choix des syndicats, être revendus auprès
de sympathisant-es ou d’adhérent-es ou sur des tables de presse, et
ainsi contribuer aussi à faire connaître le Combat Syndicaliste et la CNT.


Contre motions
==============


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°5 2016

       :ref:`ETPIC30 <etpic30>`
     - 31
     - 9
     - 7
     - 0
   * - Décision du congrès

       Acceptée
     -
     -
     -
     -
