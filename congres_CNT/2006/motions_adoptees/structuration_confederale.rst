
.. _motions_structuration_confederale_cnt_congres_agen_2006:


==============================================================================================================
Motions "Structuration confédérale" de la CNT  30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
==============================================================================================================

Composition & constitution des Unions Locales
=============================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 50         |  4       | 8          | 3                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Les unions locales de la CNT sont constituées de syndicats locaux et/ou de
syndicats départementaux ou régionaux (par le biais de leurs adhérents locaux
et à partir du moment où ces derniers sont au moins au nombre de deux).

Les dits syndicats cotisent à ces UL et participent à leur action par le biais
de leurs adhérents locaux.


.. index::
   Charte de Paris

Syndicalisation des travailleurs indépendants
=============================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 60         |  4       | 3          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le congrès entérine le souhait formulé dans la charte de création de la CNT:
Charte du syndicalisme révolutionnaire dite **Charte de Paris**, charte adoptée
lors du congrès constitutif de la CNT les 7, 8 et 9 décembre 1946 à Paris.
(Continuatrice de la charte de Lyon de la CGT SR en 1926):

- Les syndicats de la confédération procèdent à l'adhésion des travailleurs
  indépendants par branches concernés, et octrois les mêmes pouvoirs
  décisionnels que tous les autres travailleurs.
