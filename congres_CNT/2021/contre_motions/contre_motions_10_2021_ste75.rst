
.. _contre_motions_10_2021_ste75:

===========================================================================================================================================
Contre-motion N°2 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **STE75**
===========================================================================================================================================

- :ref:`motion_10_2021_etpic30`


Autres motions STE75

    - :ref:`paris:motions_ste75`


Argumentaire
==============

- ?

Contre-motions
================

Nous demandons qu’elle soit découpée en trois et fasse donc l’objet de
trois votes différents.

Découpage comme suit :


.. _contre_motion_10_2021_ste75_partie_1:

Contre-motion à la **motion n°10 2021 partie 1** par **STE75**
----------------------------------------------------------------

La CNT groupe des travailleurs et des travailleuses conscient.e.s de la
lutte à mener contre toutes formes de discriminations […] (dix paragraphes)

les syndicats de la CNT verront à intégrer dans leurs statuts respectifs
la question de la lutte contre les discriminations pour leur permettre
de se porter le cas échéant partie civile.

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°10 |            |          |            |                           |
| STE75 partie 1                 |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+


.. _contre_motion_10_2021_ste75_partie_2:

Contre-motion à la **motion n°10 2021 partie 2** par **STE75**
------------------------------------------------------------------

Pour être à la hauteur des enjeux de lutte antisexistes et antipatriarcales,
la CNT prolonge les travaux de sa « Commission antisexiste » […] (douze paragraphes)
créer des liens avec d’autres organisations en lutte pour l’égalité
des sexes et contre les violences sexistes. 

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°10 |            |          |            |                           |
| STE75 partie 2                 |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+


.. _contre_motion_10_2021_ste75_partie_3:

Contre-motion à la **motion n°10 2021 partie 3** par **STE75**
------------------------------------------------------------------

Concernant les problématiques d’agressions sexistes et sexuelles dans
et en dehors de la CNT […]
Elle peut émettre dans ce dernier cas un avis consultatif. 

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°10 |            |          |            |                           |
| STE75 partie 3                 |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
