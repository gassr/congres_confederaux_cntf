
.. _amendement_motion_14_2021_interpro31:

==========================================================================================================================================
Amendement N°2 à la Motion n°14 2021 **Financement confédéral des groupes Femmes Libres** par Interpro31
==========================================================================================================================================

- :ref:`motion_14_2021_syndicats_42`
- :ref:`role_de_linterpro_31_fouad`

Autres motions Interpro31

    - :ref:`syndicats:interpro31_motions_congres`


Argumentaire 
============

Il faut une entité confédérale « responsable » de ces cotisations,
mandatée par les groupes FLML selon leurs modalités. Libre aux groupes
FLML de s’organiser commes elles l’entendent ensuite.

Amendement 
============

Une entité confédérale FLML, fédérant tous les groupes FL, est créée.

Une cotisation de 0,10€ sur les cotisations syndicales de chaque adhérent.e
est affectée au fonctionnement des groupes Femmes Libres pour organiser
des rencontres, formation, actions et autres est dédié à cette entité.

Cette cotisation ne retire pas aux différents groupes la tâche de
s’autofinancer, mais engagerait la confédération dans leur reconnaissance
et le soutien à leur combat.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°14 |            |          |            |                           |
| Interpro31                  |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
