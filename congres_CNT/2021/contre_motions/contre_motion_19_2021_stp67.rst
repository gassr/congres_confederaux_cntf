
.. _contre_motion_19_2021_stp67:

==========================================================================
Contre-motion à la motion n°20 2021 **Notre fédéralisme** par **STP67**
==========================================================================

- :ref:`motion_19_2021_ste93`


Argumentaire
==============

Le problème de non rotations des mandats, entre autre lié au faible
nombre d’adhérent·e·s aux syndicats.
Ce problème s’étend sur d’autres mandats, par exemple celui des représentants
du personnel.

De plus, il serait d’autant plus difficile de demander l’expulsion
d’adhérent·e (comme proposer par plusieurs autres motions) si cela
pourrait mettre en sommeil automatiquement.

Le COVID montre aussi des baisses conjoncturelles du nombre d’adhérent·e·s.

Enfin, une mise en sommeil pourrait dissuader les syndicats de mettre à
jour honnêtement leur effectif. Tandis qu’une aide pour les syndicats
de moins de 5 membres pourrait tenter de résoudre ces problèmes dans
un esprit de solidarité.

Si nous pensons qu’il est raisonnable d’être 5 pour créer un syndicat,
nous pensons qu’il peut arriver à un syndicat de passer en dessous puis
au dessus de cette limite.

Mettre en sommeil un syndicat aurait un effet inverse de l’argumentaire,
puisque limiterait le nombre de personnes participante au congrès.

Contre-motion
===============

Lorsqu’un syndicat déclare moins de 5 adhérent·e·s, le :term:`BC` l’ajoute à la
liste des syndicats de moins de 5 personnes, et notifie les autres syndicats ;
il revient alors à ces autres syndicats de proposer d’aides et
d’accompagner ce syndicat afin de lui permettre une activité et un
développement normal.


Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°19 |            |          |            |                           |
| STP67                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
