
.. _amendement_motion_5_2021_stp67:

============================================================================================================================================================
Amendement N° à la Motion n°5 2021 **Pour l’adhésion de la CNT à la Confédération Internationale du Travail. C.I.T - Engager le processus** par **STP67**
============================================================================================================================================================

- :ref:`motion_5_2021_ptt95`

Autre motions STP67

    - :ref:`syndicats:motions_stp67`


Amendement
==============

Remplacer la motion par : **La CNT fait la demande d’adhésion à la CIT.**


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°5  |            |          |            |                           |
| STP67                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
