
.. _amendement_motion_23_2021_sest_lorraine:

==============================================================================================================================================
Amendement N°1 à la motion n°23 2021 **Lutte antisexiste et anti-patriarcale en interne et sur nos lieux de travail** par **SEST Lorraine**
==============================================================================================================================================

- :ref:`motion_23_2021_stt59_62`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`

Argumentaire
===============

Le travail proposé peut être intéressant aussi le SEST Lorraine insiste
sur la nécessite de bien travailler sur la rédaction des questions posées
afin que ces dernières ne soient pas des réponses induites dans leur
formulations

Amendements
============

.. raw:: html

    La CNT met en place une démarche collaborative de recueil d'informations,
    de données et de propositions sur les questions fondamentales d’égalité
    <span class="modif_rouge">de genre</span> <del>homme-femme</del>,
    de lutte contre le sexisme, des discriminations et des violences en
    interne et sur les lieux de travail.

La première étape de cette démarche consiste en la réalisation, la
diffusion puis le dépouillement et l'analyse d'un questionnaire en ligne
destine a l'ensemble des adhérent.e·s de la CNT
Les réponses anonymisées, conditionneront les suites du projet.

A l'issue de la première étape, les syndicats de la CNT auront a se prononcer
sur la restitution.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°23 |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
