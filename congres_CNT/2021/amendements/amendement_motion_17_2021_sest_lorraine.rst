
.. _amendement_motion_17_2021_sest_lorraine:

=====================================================================================================
Amendement à la Motion n°17 2021 **Prise de décisions en CCN** par **SEST Lorraine**
=====================================================================================================

- :ref:`motion_17_2021_ptt95`

Autres motions SEST Lorraine

    - :ref:`syndicats:sest_lorraine`

Argumentaire
===============

Le SEST Lorraine rappelle que le  :term:`CCN` n’est normalement pas un lieu de prises
de décision mais de contrôle de l’exécution des décisions du congrès.
Néanmoins ces dernières années, il s’est avéré que le  :term:`CCN` a pu jouer
un rôle plus important que de simple suivi de décisions.

En effet la situation que traverse la Confédération: absence de congrès
ordinaire et extra ordinaire depuis 2016 couplé avec la crise sanitaire,
nous montre qu’il faille s’adapter pour ne pas décourager l’ensemble de
nos adhérent.e·s.

Afin que la Confédération ne soit pas freinée ni dans son fonctionnement
ni dans son développement nous faisons l’amendement suivant afin que
les :term:`CCN` puissent se tenir quoi qu’il arrive.

Amendements
============

Lors des :term:`CCN` les décisions sont prises a l’unanimité des régions présentes.
Au compte-rendu figurent les avis émis, à titre consultatif, par les
syndicats présents mais non constitues en région.

Le :term:`CCN` peut **utiliser des moyens numériques** pour se réunir et
pour connaître l’avis des syndicats non constitués en :term:`UR`.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°17 |            |          |            |                           |
| SEST Lorraine               |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
