
.. _amendement_motion_44_2012_sinr44:

==========================================
Amendement à la motion 44 (SINR44)
==========================================

.. seealso::

   - :ref:`amendements_sinr44_2012`
   - :ref:`motion_44_2012`




Motivation
===========

Notamment dans l'esprit de la :ref:`motion n°47 <motion_47_2012>` avec laquelle
nous sommes d'accord, nous estimons que la date d'envoi d'un e-mail ne peut
pas « faire foi ».

Motion modifiée
================

Pour que tous les syndicats aient le temps d'examiner les motions lors de leur
AG un délai de huit semaines est indispensable (en comptant deux semaines pour
la mise à disposition de l'ordre du jour du CCN aux syndicats, délais postaux
compris), dans le cas où les réunions d'UR se tiennent le week-end précédent
le CCN, il reste cinq semaines aux syndicats pour examiner les motions au
cours de leur AG habituelle.

La date d’échéance de dépôt des points de l'ordre du jour par les Unions
régionales ainsi fixée 56 jours avant la date du CCN doit donc être respectée
mécaniquement, le cachet de la poste faisant foi ``(ou la date d'envoi par
e-mail)^W``, les points de l'ordre du jour reçus le lendemain étant reportés au
CCN suivant.


Vote indicatif
==============

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°44 |            |          |            |                           |
| SINR44                      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
