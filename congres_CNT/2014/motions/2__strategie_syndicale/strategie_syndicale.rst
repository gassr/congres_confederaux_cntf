
.. index::
   pair: Motions; Stratégie syndicale 2014
   pair: Stratégie syndicale ; 2014


.. _motions_strategie_syndicale_2014:

======================================
Motions stratégie syndicale 2014
======================================

.. toctree::
   :maxdepth: 4

   motion_8_2014_sinr44
   motion_9_2014_sinr44
   motion_10_2014_ste49
   motion_11_2014_tasra
   motions_12_13_14_2014_interco25/motions_12_13_14_2014_interco25
   motion_15_2014_interco71
   motion_16_2014_ess34
   motion_17_2014_ste75


- :ref:`contre_motions_congres_2014`
- :ref:`congres_CNT_2014_amendements`
