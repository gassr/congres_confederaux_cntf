

.. _motion_8_2010:

===========================================================================
Motion n°8 2010 : Labellisation des structures CNT, ETPRECI 35 [non votée]
===========================================================================



Rappel
======

Les syndicats qui se constituent se voient attribuer le label confédéral dans
la mesure où:

- ils demandent leur affiliation à la :term:`C.N.T` ;
- ils s’affilient à leurs unions respectives (UL, :term:`U.R` et fédération) ;
- ils sont légalement constitués et autonomes financièrement ;
- ils déposent des statuts en cohérence avec les statuts et règles organiques
  de la :term:`C.N.T`.
  Une procédure pour l’obtention de ce label est décrite par une décision de
  Congrès et annexée aux règles organiques.

Argumentaire
============

La :term:`C.N.T` est constituée par les syndicats d’industries - ou intercorporatifs
lorsqu’il n’est pas possible de créer les premiers - groupés dans:

- les unions locales ;
- les unions régionales de syndicats ;
- les fédérations d’industrie.

Le reste de l’article 2 des statuts précise que toutes les structures confédérées
doivent déposer des statuts en cohérence avec les statuts Confédéraux.

Or à ce jour aucune union de syndicat ne fait l’objet de procédure
d’obtention du label :term:`C.N.T`. Au même titre que les syndicats, les unions,
quelques soient leur champ de syndicalisation et étendue géographique, doivent
se voir attribuer le label :term:`C.N.T` que si il y a cohérence statutaire et
si aucune opposition ne se fait jour.

Motion
======

Une organisation adhérente à la :term:`C.N.T` se verra accorder la marque distinctive  appelée
label confédéral automatiquement par le :term:`B.C` si aucun syndicat :term:`U.R`
ou UL limitrophe et aucune fédération d’industrie existante ne conteste dans un délai de 2 mois.

En cas de contestation, l’article 24 des statuts entrera en application.
  
+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°8           |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
