
.. index::
   pair: Fédération ; transports
   ! Transports


.. _federation_des_transports:

=====================================================================================
Fédération des travailleurs des transports,de la logistique et activités auxiliaires
=====================================================================================

.. image:: logo_fede_transports.png


Adresse
=======

::

    Fédération des travailleurs des transports,de la logistique et activités auxiliaires
    Confederation Nationale du Travail
    STTLA 30 CNT
    6, rue d'Arnal,30000 Nîmes
    federation.transports@cnt-f.org


Nous sommes heureux de vous faire part de la naissance de la
Fédération des transports, de la logistique et des activités
auxiliaires.

**Son congrès constitutif s’est tenu le 12 novembre 2011**.


Presentation
=============

.. toctree::
   :maxdepth: 4

   presentation

Statuts
=============

.. toctree::
   :maxdepth: 4

   statuts/index

Congrès fédéraux
================

.. toctree::
   :maxdepth: 4

   congres/index

Syndicats/Sections syndicales
=============================

.. toctree::
   :maxdepth: 4


   alsace/index
   languedoc_roussillon/index
   nord_pas_de_calais/index
   sgtl/index
