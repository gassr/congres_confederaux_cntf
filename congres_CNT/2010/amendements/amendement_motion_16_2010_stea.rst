
.. _amendement_motion_16_2010_stea:


====================================
Amendement à la motion 16 (stea)
====================================


La Confédération Nationale du Travail met à disposition les moyens techniques
qu'elle jugera nécessaires à la bonne exécution des mandats confédéraux et si
possible utilisera des logiciels libres et open source.

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°16 |            |          |            |                           |
| stea                        |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_16_2010`
