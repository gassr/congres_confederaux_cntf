
.. _amendement_motion_50_2010_educ91:


=====================================
Amendement à la motion 50 (educ91)
=====================================

L'Union locale CNT 91 trouve excellente l'initiative de lancer des Editions
confédérales de brochures et de nouvelles publications en s'attachant aux
sujets d'actualité comme à l'histoire des luttes sociales.

Cela implique de sortir des livres.

Comme c'est en partie le travail effectué par les Editions CNT-RP depuis plus
de dix ans, il nous semble important que l'indépendance des deux éditions
soit maintenue d'une part et que, de l'autre, elles coordonnent leurs projets
de publications, voire fassent des coéditions.

Cela se pratique entre les Editions CNT-RP et Coquelicot de Toulouse
et "Nationalisme et culture" de Rocker l'a été avec les Editions libertaires,
de même avec Nautilus en 2001 "De l'Histoire du mouvement ouvrier révolutionnaire".

Il nous semble également nécessaire de faire des publications virtuelles que
les camarades peuvent consulter ou copier par exemple La revue libertaire
espagnole BICICLETA Anthologie 1977-1982 (http://www.fondation-besnard.org/article.php3?id_article=740)
et huit autres sections.



+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°50 |            |          |            |                           |
| educ91                      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_50_2010`
