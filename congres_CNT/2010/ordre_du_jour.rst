

.. _ordre_du_jour_31e_congres:

==============================
Ordre du jour du XXXIe Congrès
==============================


Proposition croisée de la commission de préparation de Congrès et du STE 75




Vendredi 10 decembre 2010
=========================

De 9 à 11h
----------

Enregistrement des mandats auprès de la commission de contrôle des mandats.

De 11h à 13h
------------

Motions de fonctionnement du congrès

- :ref:`motion_23_2010`
- :ref:`motion_24_2010`
- :ref:`motion_9_2010`
- :ref:`motion_10_2010`
- :ref:`motion_11_2010`


De 14h à 19h
------------

Rapports des mandatéEs confédéraux + quitus.

Mise en place de la commission de contrôle de la Trésorerie Confédérale

Stratégie syndicale
-------------------

- :ref:`motion_56_2010`
- :ref:`motion_36_2010`
- :ref:`motion_40_2010`
- :ref:`motion_41_2010`
- :ref:`motion_37_2010`
- :ref:`motion_38_2010`
- :ref:`motion_39_2010`

Soirée festive
--------------


Samedi 11 decembre 2010
=======================


De 9h à 13h & de 14h00 à 19h
----------------------------



Fonctionnement
--------------

- :ref:`motion_32_2010`
- :ref:`motion_33_2010`
- :ref:`motion_4_2010`
- :ref:`motion_5_2010`
- :ref:`motion_6_2010`
- :ref:`motion_7_2010`
- :ref:`motion_8_2010`
- :ref:`motion_17_2010`
- :ref:`motion_18_2010`
- :ref:`motion_12_2010`
- :ref:`motion_14_2010`
- :ref:`motion_15_2010`
- :ref:`motion_19_2010`
- :ref:`motion_20_2010`
- :ref:`motion_21_2010`
- :ref:`motion_16_2010`
- :ref:`motion_13_2010`
- :ref:`motion_34_2010`
- :ref:`motion_35_2010`
- :ref:`motion_47_2010`
- :ref:`motion_48_2010`
- :ref:`motion_49_2010`
- :ref:`motion_50_2010`
- :ref:`motion_51_2010`
- :ref:`motion_52_2010`
- :ref:`motion_53_2010`
- :ref:`motion_30_2010`
- :ref:`motion_25_2010`
- :ref:`motion_26_2010`
- :ref:`motion_27_2010`
- :ref:`motion_28_2010`
- :ref:`motion_29_2010`

contre-motion, p. 2

De 20h à 22h30
--------------

Rapport de mandat des commissions confédérales
Rapport des mandats confédéraux_2010` secteur propagande, combat syndicaliste + Quitus


Dimanche 12 decembre 2010
=========================

De 9h à la fin du Congrès
-------------------------

Mandatements des mandatéEs confédéraux
Constitution des commissions confédérales

Orientations & campagnes
========================

- :ref:`motion_31_2010`
- :ref:`motion_57_2010`
- :ref:`motion_46_2010`
- :ref:`motion_22_2010`
- :ref:`motion_44_2010`
- :ref:`motion_45_2010`
- :ref:`motion_43_2010`
- :ref:`motion_42_2010`
- :ref:`motion_2_2010`
- :ref:`motion_3_2010`
- :ref:`motion_54_2010`
- :ref:`motion_55_2010`
- :ref:`motion_58_2010`
- :ref:`motion_59_2010`
