
.. _motion_20_2010:

===============================================================
Motion n°20 2010 : Suivi mandats des campagnes, INTERPRO CNT 31
===============================================================



Argumentaire
============

Beaucoup de décisions prises en congrès de la :term:`C.N.T` restent sans effet
si le ou les syndicats qui en sont à l'origine n'ont pas les forces ou la volonté
suffisantes pour porter ces décisions. Nous pensons que ce désintéressement
une fois le congrès passé n'aide pas au développement d'un réel fonctionnement
confédéral.
La :term:`C.N.T` souffre de ce manque de pratique confédérale malgré une conscience
forte au sein de notre conf que les luttes locales ne pourront aboutir sans une
coordination d'ensemble.

La :term:`C.N.T` Interpro 31 propose donc de créer un mandat "suivi des décisions de congrès".

Motion
======

La :term:`C.N.T` se dote d'un secrétariat "suivi des décisions de congrès" ayant comme mandat d'informer les syndicats
de l'avancée des différentes décisions prises durant le congrès. Les mandatés pourront aussi relancer les
syndicats et les inciter à s'impliquer activement dans les décisions prises. Le secrétariat suivi des décisions de
congrès est intégré à la commission administrative de la :term:`C.N.T`. Les mandatés participeront à la CA.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°20          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`contre_motion_20_2010_ste93`
