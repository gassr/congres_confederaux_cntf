

.. _annexes_motions_25_26_2010:

=============================================================
Annexe aux motions n°25 et 26 2010 **Modération Intranet**
=============================================================

.. seealso::

   - :ref:`motions_25_26_2010`


Voici en parcourant rapidement l'intranet confédéral quelques extraits
puisés sur l'intranet CNT d'échange entre militants de notre confédération.

Les noms ont été effacés car l'intérêt n'est pas de savoir qui a dit
ça à qui mais de constater la « qualité » des arguments énoncés....

::

    Tiens, je croyais lire un post sur l'intranet de la CNT et je me retrouve sur les pages rebonds de libération :
    Analyse syndicale = 0
    Analyse politique = 0
    Des lieux communs comme réflexion, la bonne technique du "je renvoie dos à dos" et in fine le statu quo
    étatique. Dommage, j'ai pas trouvé le smiley qui applaudi, il aurait toute sa place ici.

    Continue de bosser (...), ça nous fait des vacances.

    A part ça, c'était bien les vacances? Comment on fait pour concilier congés payés et Lutte des classes chez les
    profs?.. Est-ce qu'on y arrive seulement? M...e! Je viens encore de débiter deux énormes connerie, coup sur
    coup... DSL!

    Arrête de raconter n'importe quoi, surtout en ton nom propre sans jamais foutre les pieds dans ton AG de
    syndicat :

    Maintenant, si tu veux continuer à cracher ton venin sur intranet, libre à toi, mais je te rappelle juste que ce
    n'est pas un défouloir et que la base des décisions de la CNT c'est le syndicat…

    A moins de penser que l'onanisme fasse partie du corpus theorique et pratique des libertaires : pardon des
    libertoides. Pourquoi d'ailleurs parler de "libertoides et pas de "trotzkystoides"? Ou bien "d'hallucinations et de
    masturbations trotzkystoides et marxistoides"? Cela mettrait de l'ambiance dans le subsconscient de camarades
    en proie a des visions territfiantes et a des hallucinations de la sensibilite generale (piqures, decharges
    electriques,etc) Et bien evidemment chez des personnes souffrant d'etats confusionnels (alcoolisme en
    particulier)

    Je peux te garantir qu'à la vue de ton message je fais tout pour me contenir et peser mes mots tant ça me
    débecte!!!

    (...), fermes ta gueule, surtout quand tu ne sais pas de quoi tu parles.

    Alors avant de ramener ta sale trogne de rachot puant, renseigne toi. Toi, ici, tu ne fais que des amalgames
    puants sans rien comprendre à quoi que ce soit. Franchement, je suis de plus en plus motivé pour ton exclusion,
    non seulement de se forum mais également de notre confédération. Avec des positions telles que celles ci, tu n'as
    plus rien à y foutre. Connard!!!

    tu te prends pour qui, monsieur le travailleur des soirées karaokés?je diffais des tracts dans la cité ou j'ai grandi
    que tu pissais encore sur les genoux de ta mere.

    (...), tu n'habites pas les quartiers... Donc, restes dans ton village avec ta cochonnaille et évite de l'ouvrir sur des
    sujets que tu ne maitrises pas. Au lieu de parler des autres, expliques

    Très honnêtement si j'avais vu ou entendu ça dans un cadre différent de ce forum je t'aurai sûrement pris pour
    un facho et je t'aurai gratifié d'une praline pour la peine!
    tu me parles de middle class blanche. mais que que crois tu que tu fais avec le trip redskins qui est un trip anglo
    saxon de fils à papa?

    mais concretement, que reprochez vous à ce reportage de rue89 ? ce que vivent les nanas dans certains
    quartiers ne vous interesse pas? il ne faut pas en parler? c'est vrai qu'il faut mieux faire un foin pour les
    elucubrations des derniers cathos tradis.

    Si tu t'occupais de ta région au lieu de faire le VRP sur les autres régions tu saurais que le syndicat des PTT est
    toujours vivant ainsi que les Travailleurs et Précaires d'Aquitaine qui syndiquent des militants du 33 qui ne
    souhaitent pas militer, cotiser "avec tes copains", ne prends pas tes désirs pour des réalités
    J'arrête de te répondre car je perds mon temps et on le fait perdre à d'autres A relire jusqu'à que compréhension
    s'en suive .

    La question est là et pas ailleurs. La haine délirante contre (...), au fond, ne concerne que quelques névrosés qui
    ont du mal à lâcher l'os qu'ils rongent depuis dix ans.
    et au lieu de ca on se fait insulter en ccn par des gros cons mandatés comme (...)
      
    mais (...), les statuts ont été modifiés dans l'indifférence générale par une poignée de maniaques et avalisés par
    une 40 aine de mandatés saoulés et pressés d'en finir, la moitié ne sachant plus de quoi il retournait.

    Arrête de foutre ta merde dans TOUS les posts (...). ça devient plus qu'agaçant, tu ne fais que ça : tu donnes des
    leçons, tu provoques, tu fais l'avocat du diable, tu fais le bof ostentatoire, tu mitonnes, tu te fais des films en
    direct, et t'es surtout limite correct tant sur la forme que sur le fond.

    tu es vachement gonflé. qui ici juge tout un syndicat pour son "déviationnisme"? qui ici nous explique ce que
    doit etre un bon syndicat cntiste? il est clair que mes messages sont moins policés et cntistement corrects que les
    tiens et d'autres. mais je ne reclame l'exclusion de personne moi. je n'oeuvre pas à descendre notre plus gros
    syndicat pourtant composé de precaires.

    Je ne sais même pas si c'est la peine d'essayer de le raisonner. Le gars est complétement kéblo. A mon avis la
    bonne solution, c'est de donner mandat aux modérateurs de ce forum pour virer les reloux qui refusent toutes
    discussions, donnent des leçons sans arrêt

    A mon avis, (...) et d'autre, vous êtes bien seul. Que vous ne soyez pas d'accord, c'est une chose. Que vous
    essayez de foutre le bordel ici, et de bloquer quand ça vous arrange, c'est autre chose. T'imagines que la CNT va
    se laisser bouffer comme ça ? Tu te trompes...
