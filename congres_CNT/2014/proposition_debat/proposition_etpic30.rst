
.. _debat_2014_etpic30:

=============================================
Proposition débat congrès CNT 2014 (ETPIC30)
=============================================


::

    Sujet :     [Liste-syndicats] Proposition débat ETPIC 30 Congrès -
    Date :  Tue, 4 Nov 2014 19:26:42 +0100
    De :    CNT ETPIC 30 SUD <cnt.nimes@cnt-f.org>
    Pour :  Congrès <congres@cnt-f.org>
    Copie à :   liste-syndicats@bc.cnt-fr.org

Camarades,

Voici une thématique de débat qui, si le temps nous le permet, nous
pourrions aborder:

Nous proposons l'ouverture d'un débat libre sur le projet global que nous
portons, sur sa place aujourd'hui dans le contexte sociétal, sur sa
pertinence d'action, et les moyens qu'il se donnera à l'avenir.

- Quels positionnements, quelles pratiques et quelles inscriptions du
  projet global anarcho-syndicaliste et syndicaliste
  révolutionnaire face aux enjeux majeurs des luttes actuelles : lutte
  contre la précarité, menace sur les systèmes de solidarité, écologie,
  antifascisme.

- Comment valoriser, soutenir et outiller la syndicalisation : formations,
  parrainage, et accompagnement, éducation populaire,
  propagande.


Salutations AS&SR

Bertrand pour l'ETPIC 30
