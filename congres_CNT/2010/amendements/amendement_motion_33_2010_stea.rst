
.. index::
   double affiliation

.. _amendement_motion_33_2010_stea:


====================================
Amendement à la motion 33 (stea)
====================================


A l’exception des secteurs où règnent le monopole syndical à l’embauche, à
savoir actuellement les docks et le livre, la CNT n’autorise pas la double
affiliation syndicale. Le CCN pourrait ajouter ou retirer un secteur des
exceptions


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°33 |            |          |            |                           |
| stea                        |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_33_2010`
