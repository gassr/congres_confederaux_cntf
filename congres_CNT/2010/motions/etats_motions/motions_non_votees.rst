

.. _motions_non_votees_congres_CNT_2010:

==========================================================
Motions non votées au XXXIe Congrès CNT Saint-Etienne 2010
==========================================================

Stratégie syndicale
===================


- :ref:`motion_40_2010`
- :ref:`motion_41_2010`
- :ref:`motion_42_2010`

Fonctionnement organique
========================

- :ref:`motion_5_2010`
- :ref:`motion_6_2010`
- :ref:`motion_7_2010`
- :ref:`motion_8_2010`
- :ref:`motion_18_2010`
- :ref:`motion_19_2010`
- :ref:`motion_20_2010`
- :ref:`motion_16_2010`
- :ref:`motion_13_2010`
- :ref:`motion_34_2010`
- :ref:`motion_35_2010`
- :ref:`motion_2_2010`
- :ref:`motion_3_2010`
- :ref:`motion_60_2010`
- :ref:`motion_22_2010`
- :ref:`motion_12_2010`
- :ref:`motion_14_2010`
- :ref:`motion_32_2010`
- :ref:`motion_33_2010`

Statuts
=======

- :ref:`motion_24_2010`

Motions Info / communication interne (internet, intranet, etc.)
===============================================================

- :ref:`motion_25_2010`
- :ref:`motion_26_2010`
- :ref:`motion_27_2010`
- :ref:`motion_28_2010`
- :ref:`motion_29_2010`
- :ref:`motion_30_2010`


Campagnes nationales
====================

- :ref:`motion_45_2010`
- :ref:`motion_46_2010`

Presse / publications
=====================

- :ref:`motion_47_2010`
- :ref:`motion_48_2010`
- :ref:`motion_49_2010`
- :ref:`motion_50_2010`

Propagande
==========

- :ref:`motion_51_2010`
- :ref:`motion_52_2010`
- :ref:`motion_53_2010`

Autres
======

- :ref:`motion_31_2010`
- :ref:`motion_54_2010`
- :ref:`motion_55_2010`
- :ref:`motion_57_2010`
- :ref:`motion_58_2010`
- :ref:`motion_59_2010`
