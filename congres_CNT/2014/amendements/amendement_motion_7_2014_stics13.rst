
.. _amendement_motion_7_2014_stics13:

================================================
Amendement à la motion n°7 2014 (STICS13)
================================================

.. seealso::

   - :ref:`motion_7_2014_etpic30`
   - :ref:`stics13`





Argumentaire
=============

Pour nous les candidatures aux mandats confédéraux ne peuvent être présentés
par des individus.
Elles sont présentées par les syndicats d'appartenance des candidats.
De ce fait nous proposons l'amendement suivant (en **gras** dans le texte)

Amendement
==========

Le Congrès Confédéral de la CNT examine individuellement les candidatures
aux différents mandats confédéraux, même lorsque ces dernières sont présentées
ou annoncées de façon collective.
Chaque **syndicat est invité à présenter son-sa candidat-e selon** les
motivations de candidature en terme de perspective d’investissement et
de travail en commun.

En vue de favoriser un meilleur ancrage territorial et une multiplicité
d’approches, le Congrès Confédéral tend à privilégier le mandatement de
personnes émanant de différents territoires et de différents syndicats.
