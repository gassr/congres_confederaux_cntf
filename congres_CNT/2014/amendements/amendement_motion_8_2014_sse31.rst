
.. _amendement_motion_8_2014_sse31:

================================================
Amendement à la motion n°8 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_8_2014_sinr44`




Argumentaire
=============

On trouve que la formulation de cette motion n'est pas claire.

Amendement
===========

Si une fédération décide de se présenter aux élections professionnelles dans
le public, elle est tenue de présenter un argumentaire expliquant la
pertinence de ces élections au CCN suivant.

La fédération présente un bilan régulier à chaque CCN.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°8 2014 <motion_8_2014_sinr44>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
