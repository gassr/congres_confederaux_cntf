

.. _motions_5_6_2010:

======================================================================================================================
Motions n°5 et n°6 2010 : Dé-labellisation d'un syndicat par absence de cotisation ou inactivité avérée, Etpic 30 Sud
======================================================================================================================

.. seealso::

   - :ref:`motion_40_2012`
   - :ref:`motions_etpic30`



Motion reprise par ETPIC30, 2012
================================

- :ref:`motion_40_2012`

Préalable
=========

Cette motion reprend en substance la proposition administrative du :term:`B.C`
adoptée à l'occasion du :term:`C.C.N` des 16 & 17 Mai 2009 à Bordeaux.

Son adoption par le Congrès viendrait compléter la motion consacrée à la
labellisation des syndicats. Elle viendrait modifier dés lors les règles
organiques associées aux statuts.

Argumentaire
============

Pour la :term:`C.N.T` et ses instances d'administratives (B.C, C.A), être en capacité de déterminer de façon fiable quels
sont les syndicats adhérents ou actifs est important. En effet, le nombre de syndicats considérés à jour de
cotisations influe sur le quorum d'ouverture du Congrès confédéral.

Il est dit par ailleurs qu'un syndicat non adhérent de la :term:`C.N.T` ne peut agir en son nom.

Pour le Bureau Confédéral, ou même pour différents mandats confédéraux de la Commission administrative, la
mise à jour de l'annuaire des syndicats influe sur différents domaines : envoi des circulaires confédérales,
abonnements listes, accès intranet, site web cnt-f.org, annuaire web, etc.

En vue de l'établissement d'une règle commune, Le B.C a donc été amené à proposer aux U.R un processus
transparent de relance de trésorerie pouvant permettre d'éviter une délabellisation.
Les conséquences importantes d'une délabellisation pour le syndicat, comme pour la Confédération des
syndicats, nous amènent à proposer au Congrès d'entériner les dispositions suivantes:


.. _motion_5_2010:

Motion n°5 2010 : Dé-labellisation d'un syndicat par absence de cotisation, Etpic 30 Sud [non votée]
=====================================================================================================

En cas de retard de cotisations confédérales, avant tout acte de délabellisation,
la trésorerie confédérale et le Bureau confédéral pourront relancer les syndicats
concernés de la façon suivante:

- Si retard de plus de 12 mois, le/la trésorierE confédéralE relance par mail
  ou courrier simple > attente d'un mois minimum pour la réponse (par courrier,
  mail, téléphone...) ;
- Si aucune réponse (ou peu satisfaisante), une relance par courrier postal
  par le/la trésorierE confédéralE à l'adresse du syndicat > attente d'un mois
  minimum pour la réponse (par courrier, mail, téléphone...) ;
- Si aucune réponse (ou peu satisfaisante), une dernière relance du Bureau
  confédéral par courrier postal avec accusé de réception.
  Attente d'un mois minimum pour la réponse ;
- Si aucune réponse, envoi dernier courrier du :term:`B.C` avec accusé de réception
  annonçant la dé-labellisation :term:`C.N.T`.
  L'UR concernée, si elle existe, est informée simultanément, ainsi que l'ensemble
  des UR par circulaire confédérale (et par la liste web syndicats).
  Dans un délai de 2 mois, lorsqu’il y a opposition d'une union régionale,
  le :term:`B.C` doit suspendre sa décision, qui doit être soumise au prochain C.C.N.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°5           |            |          |            |                           |
| Etpic 30 Sud         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+



.. _motion_6_2010:

Motion n°6 2010 : Dé-labellisation d'un syndicat par inactivité avérée, Etpic 30 Sud [non votée]
=================================================================================================

Si le Bureau Confédéral est informé par écrit (mail, courrier) d'une mise en
sommeil d'un syndicat par unE ou plusieurs de ses membres, et après confirmation
par l'UR concernée, il peut être prononcé sa dé-labellisation.

Le retour de courrier en NPAI (ne siège pas à l'adresse indiquée), doublé d'absence
de réponse téléphonique/mail ou de toute autre information, peut-être interprété,
sur avis de l'UR concernée, comme un état d'inactivité manifeste.

Ce constat sérieusement établi peut faire l'objet d'une dé-labellisation sans courrier.
L':term:`U.R` concernée, si elle existe, est informée simultanément, ainsi que l'ensemble
des :term:`U.R` par circulaire confédérale (et par la liste web syndicats).

Dans un délai de 2 mois, lorsqu’il y a opposition d'une Union Régionale, le :term:`B.C` doit suspendre sa décision, qui
doit être soumise au prochain C.C.N.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°6           |            |          |            |                           |
| Etpic 30 Sud         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::  :ref:`contre_motion_6_2010_ess34`
