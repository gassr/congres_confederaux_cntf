
.. _amendement_motion_18_2021_stics72:

===========================================================================================
Amendement N°2 à la motion n°18 2021 **Procédure de la labellisation** par **STICS72**
===========================================================================================

- :ref:`motion_18_2021_ste93`

Autres motions STICS72

    - :ref:`syndicats:motions_stics72`


Article 1
===========

.. raw:: html

    Dans le cadre de la procédure de labellisation, le syndicat qui en fait
    la demande doit être composé d’au moins 5 adhérent·es <span class="modif_rouge"> ou membres
    d’une Union Locale ou Départementale.</span>

Article 2
=========

Si aucun syndicat n’existe dans le département, un syndicat interprofessionnel
peut être labellisé avec moins de 5 membres.

Vote
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion      |            |          |            |                           |
| n°18 STICS72                |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
