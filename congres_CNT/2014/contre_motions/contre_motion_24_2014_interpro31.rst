
.. _contre_motion_24_2014_interpro31:

===================================================
Contre motion à la motion numéro 24 2014
===================================================

.. seealso::

   - :ref:`motion_24_2014_ste75`





Argumentaire
=============

Ne garder que la dernière phrase de la motion.

Contre-motion
=============

Il est de rigueur que le ou les syndicats soutenant la création d'une
commission de travail y associent leurs candidatures.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°24 2014 <motion_24_2014_ste75>`

       :ref:`Interpro31 <interpro31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
