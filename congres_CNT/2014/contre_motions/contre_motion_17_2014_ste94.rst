
.. _contre_motion_17_2014_ste94:

================================================
Contre-motion à la motion n°17 2014  (STE94)
================================================

.. seealso::

   - :ref:`motion_17_2014_ste75`






Argumentaire
=============

Les temps sont probablement venus de débattre de la position de la CNT par
rapport à la prostitution.

En effet, la CNT ne s’est toujours pas positionnée sur l’abolition de la
prostitution.

La CNT compte parmi ses objectifs la disparition du salariat, c'est-à-dire de
la fourniture d'une prestation contre rémunération.

Par conséquent, la CNT ne peut qu'être opposée à la prostitution.

Pour autant, s'affirmer clairement en faveur de la disparition de la
prostitution ne signifie pas ignorer les nécessités actuelles engendrées par
cette activité. Il est donc tout autant nécessaire d'être solidaires des
travailleuses et travailleurs du sexe s'organisant pour défendre leurs droits,
en particulier l'accès à la prévention, aux soins, à un toit, mais aussi
contre les fausses solutions que sont le recours aux interdictions
et aux flics.

C’est aussi l’occasion de rappeler que les luttes de certains-nes travailleuses
et travailleurs du sexe n’ont pas toutes un objectif d’émancipation.
En effet, l’institutionnalisation de cette activité ne va pas dans ce sens.

La défense immédiate des droits des travailleuses et travailleurs du sexe et
l'abolition de la prostitution ne sont pas des objectifs dissociables.
Au contraire, ils sont complémentaires pour construire une société où la
dignité de chacune/chacun serait respectée, où les corps des unes, des
uns, ne constitueraient plus un bien marchandable, où les rapports entre
les femmes et les hommes ne seraient plus conditionnées par la taille du
portefeuille.


Contre-motion
=============

La confédération est solidaire des luttes des travailleuses et travailleurs
du sexe à condition qu’elles soient organisées par elles et eux-mêmes et
que ces organisations visent réellement à l’abolition de la prostitution.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°17 2014 <motion_17_2014_ste75>`

       :ref:`STE94 <ste94>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
