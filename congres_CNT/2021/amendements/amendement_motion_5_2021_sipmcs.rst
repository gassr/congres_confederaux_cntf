
.. _amendement_motion_5_2021_sipmcs:

=============================================================================================================================================================
Amendement N°1 à la Motion n°5 2021 **Pour l’adhésion de la CNT à la Confédération Internationale du Travail. C.I.T - Engager le processus** par **SIPMCS**
=============================================================================================================================================================

- :ref:`motion_5_2021_ptt95`
- :ref:`sipmcs:motions_sipmcs`


.. _amendement_motion_5_2021_sipmcs__1:

Amendement 1 (Rayer « ou un groupe de travail spécifique »)
==============================================================

"Le prochain SI, ou un groupe de travail spécifique, en lien avec le BC,... "


Rayer « ou un groupe de travail spécifique »


Vote
------

+------------------------------+------------+----------+------------+---------------------------+
| Nom                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==============================+============+==========+============+===========================+
| Amendement 1 à la motion n°5 |            |          |            |                           |
| SIPMCS                       |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès          |            |          |            |                           |
|                              |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+



.. _amendement_motion_5_2021_sipmcs__2:

Amendement 2 (être présents au sein de la Coordination Rouge et Noir")
=========================================================================

Rajouter à la motion: L’adhésion de la CNT à la CIT ne nous empêchera pas
de continuer de travailler avec les contacts que nous avons gardés,
d'être présents au sein de la Coordination Rouge et Noir"


.. figure:: sipmcs/amendement_5.png
   :align: center

Vote
-------

+------------------------------+------------+----------+------------+---------------------------+
| Nom                          | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==============================+============+==========+============+===========================+
| Amendement 2 à la motion n°5 |            |          |            |                           |
| SIPMCS                       |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès          |            |          |            |                           |
|                              |            |          |            |                           |
+------------------------------+------------+----------+------------+---------------------------+
