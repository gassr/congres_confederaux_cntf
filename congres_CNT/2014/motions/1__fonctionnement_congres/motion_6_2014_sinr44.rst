
.. index::
   pair: Motion 6 ; 2014

.. _motion_6_2014_sinr44:

======================================================
Motion n°6 2014: Réglementation des débats (SINR 44)
======================================================



Argumentaire
=============

Parfois lors des Congrès on assiste à des débats extrêmement longs
autour de motions dont le vote indicatif a montré qu'elles seraient
adoptées à la quasi unanimité.

Ces débats consistent alors en l'accumulation d'interventions où des
syndicats expliquent pourquoi il sont favorables à la motion.

Si ces argumentaires peuvent être très intéressants, ils coûtent un
temps précieux au Congrès qui pourrait être utilisé pour des motions
peu sujettes au débat, et de plus invisibilisent les interventions
des syndicats opposés à cette motion.

Motion
=======

Si, lors du vote indicatif, une motion semble obtenir une large
majorité (15% ou moins de voix contre, 15% ou moins d'abstentions),
la tribune donne d'abord la parole aux syndicats opposés.

La parole est ensuite donnée aux autres syndicats, dans la limite
d'un tour de parole par syndicat.

Un second tour de parole est ouvert si et seulement si un syndicat
opposé demande à nouveau la parole.


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°6 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
