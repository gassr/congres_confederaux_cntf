
.. index::
   pair: Transports; SGTL (Syndicat Général des Transports et de la Logistique)


.. _sgtl:

====================================================
Syndicat Général des Transports et de la Logistique
====================================================



Annonce
=======

::

    Sujet:  [Liste-syndicats] Coordonnées SGTL
    Date :  Mon, 10 Mar 2014 12:15:23 +0100
    De :    sgtl@cnt-f.org
    Pour :  Liste-syndicats@bc.cnt-fr.org

Camarades,


Nous vous informons suite à notre dernier congrès,
que le SGTL-RP est devenu le SGTL - Syndicat Général des Transports
et de la Logistique. N'ayant plus de champ géographique,
nous pouvons donc prendre en charge toutes demandes syndicales
venant des salariéEs du transport et de ses activés annexes.

Notre adresse mail est désormais : sgtl@cnt-f.org .

Voici nos coordonnées::

    Confédération Nationale du Travail - CNT SGTL
    Syndicat Général des Transports et de la Logistique
    BP 58 - La Bourse du travail
    27, BD des Alliés 94600 Choisy-le-Roi

Secrétaire : 06.68.57.81.82
Nom : BREBION    Prénom : Éric
erbrebion@gmail.com

Trésorier : 06.77.07.16.32
Nom : BEAUDET   Prénom : Guy
guy.beaudet0544@orange.fr
