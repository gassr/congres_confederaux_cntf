
.. _motion_outils_confederaux_cnt_congres_agen_2006:

==========================================================================================
Motion Outils confédéraux, 29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
==========================================================================================


.. index::
   Imprimerie Confédérale

Imprimerie confédérale
======================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 56         | 2        | 7          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le syndicat des Travailleurs et des Précaires d'Aquitaine continuera à réaliser
les tirages de tracts A4 pour les petits syndicats et les tirages A3
(en général les 4 pages des fédérations) avec la possibilité de tirage en deux
couleurs.

Nous le faisions avec plus ou moins de réussite faute de temps avec le mandat
du bureau confédéral. N’ayant plus cette charge, nous nous proposons de
poursuivre car cela aide bien les nouvelles fédérations.


Organisation de crèches confédérales
====================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 44         | 2        | 6          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

La confédération organisera une crèche (ou atelier d'enfant) confédérale pour
les rencontres confédérales : camping, congrès, CCN, etc., pour aider les
parents à s'investir.



Acquisition d’un lieu de vie
============================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 40         | 5        | 10         | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

- En premier lieu créer une commission nationale pour définir d'une manière
  plus précise le projet de ce lieu de vie autogéré, de sa viabilité financière
  (budget de fonctionnement, location pour des associations, etc.).
- Définir un lieu d'implantation accessible à tous et en toutes saisons
  (pour notre part , nous pensions au 86 milieu de la France , autoroutes, TGV
  et à l'achat des prix moyens , à voir par la suite .......) .
- Définir une étude financière.

La commission devra dès le premier CCN faire des propositions puis par étapes
au fur et à mesure de l'avancer du projet (une rencontre avec les militants
de « Ruesta » sera à un moment nécessaire pour prévoir le fonctionnement
interne du lieu ).

L'achat final ne pourra pas se réaliser sans consultation d'un CCN (en cas
d'urgence par un CCN extraordinaire) .

**Création de la commission**
