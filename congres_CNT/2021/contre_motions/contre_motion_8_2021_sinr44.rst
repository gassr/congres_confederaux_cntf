
.. _contre_motion_8_2021_sinr44:

==========================================================================================================
Contre-motion N°2 à la motion n°8 2021 **Prostitution et Abolitionnisme révolutionnaire** par **SINR44**
==========================================================================================================

- :ref:`motion_8_2021_etpics94`

Autres motions SINR44

    - :ref:`syndicats:motions_sinr44`


Argumentaire
================

**On est contre le système prostitutionnel (l’exploitation par le sexe),
mais son existence ne sera pas supprimée en décrétant l’abolitionnisme.**

Comme tou·tes les travailleurs, travailleuses, les TDS ont le droit de
se constituer en syndicat et de se donner des moyens de lutte car la CNT
ne peut se satisfaire ni des pressions fiscales, policières, administratives,
ni des jugements moralistes ou condescendants pesant sur ces TDS .

La CNT reste cependant contre un monde où certain.es ne peuvent pas
survivre autrement qu'en se prostituant, et particulièrement lorsque
cela est soumis à la pression d'un mac ou d'un "patron".

Les TDS doivent pouvoir se rassembler et la loi actuelle (Loi 2003-239 2003-03-18 art. 50 1° JORF 19 mars 2003)
doit être modifiée, parce qu’elle créé une situation de devoir (impôt sur le revenu)
sans droits.

Cette loi est dangereuse, car elle marginalise, pénalise et criminalise ;
isolant ainsi les TDS.

Contre-Motion
=============

La CNT permet aux TDS, travailleuses et travailleurs du sexe, de
s’organiser en syndicat (y compris en interpro) et en fédération et
leur donne tous les outils pour le faire (mail / formation / etc).

En ce sens elle s'engage également à se donner tous les moyens de lutter
contre la loi actuelle sur le proxénétisme, qui a pour conséquence
principale de marginaliser, criminaliser et précariser les TDS (il me
semble qu'on avait décidé ça à la dernière AG).

Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°8 |            |          |            |                           |
| SINR44                        |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
