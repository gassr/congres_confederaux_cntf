


.. _motion_12_2010:

==================================================
Motion n°12 2010 : Mandats confédéraux, ETPRECI 35
==================================================



Motion
======

Afin d’éviter des passations de mandats sèches, que des acquis et savoir-faire
disparaissent, un suivi des dossiers en cours, etc. les mandaté(e)s sortants
accompagnent et aident les nouveaux-elles à la tenue de leur mandat.

Cet accompagnement n’excédera pas une durée maximale allant jusqu’au premier
:term:`C.C.N` après le congrès.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°12          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`contre_motion_12_2010_culture_rp`
   - :ref:`contre_motion_12_13_14_2010_interco68`
