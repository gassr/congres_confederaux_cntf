
.. _motion_etpic1_2012:


======================================================
Formation – Non soumise au débat & au vote (motion 1)
======================================================

Notre syndicat considère cet axe comme essentiel mais ne présente pas là de
proposition particulière considérant que le 30ème Congrès de la CNT a accueilli
des propositions concrètes (motions n° 34 & 35) auxquelles nous aurions pu
adhérer et contribuer.

Nous espérons juste qu’elles puissent être réintroduites au débat comme un
outil de solidarité et d’entraide interprofessionnel.

Nous rappelons néanmoins cet extrait de la motion adoptée au :ref:`31ème Congrès <congres_CNT_2010_saint_etienne>`
(présentée par le SUTE 69-01)::

	«Dans ce cadre le syndicat doit jouer un rôle  d'éducation populaire permanent
	par la formation et l'organisation régulières  de débats, internes comme
	externes, la diffusion de nombreuses publications  et l'édition de journaux
	revues ou livres».
