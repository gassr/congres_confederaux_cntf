
.. _motion_b_2014_sipmcs:

=======================
Motion B 2014 (SIPMCS)
=======================




Motion B
=========

Une commission “site internet” est créée/modifiée.

Cette commission est un comité éditorial faisant vivre le site, choisissant
les priorités à donner aux informations, allant à la pêche aux articles,
mettant en ligne les différents articles lui parvenant, utilisant au mieux
les possibilités que proposent internet pour diffuser nos idées.

Cette commission sera composée aussi bien de personnes ayant un savoir
informatique, rédactionnel, relationnel ou sans aucun savoir “revendiqué”.
Cette commission a le même rôle que la commission CS, l'une pour le site,
l'autre pour le journal.

L'accès direct des syndicats au site confédéral pour la mise en ligne
d'articles est maintenu comme actuellement.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion B 2014 SIPMCS

       :ref:`SIPMCS <sipmcs>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
