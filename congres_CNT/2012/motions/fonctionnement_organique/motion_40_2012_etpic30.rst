
.. index::
   pair: 40; Motion 2012
   pair: 40; Motion dé-labellisation


.. _motion_40_2012:
.. _motion_40_2012_etpic30:

====================================================================================================================================================
Motion n°40 2012 : Cohésion de la CNT / Dé-labellisation d'un syndicat par absence de cotisations confédérales ou par inactivité avérée ETPIC 30 Sud
====================================================================================================================================================
  

.. seealso::

  - :ref:`motion_4_2010`
  - :ref:`motions_etpic30`
  - :ref:`contre_motion_6_2010_ess34`
  - :ref:`label_confederal`



Motion reprise des motions 5,6 ETPIC30, 2010
============================================

- :ref:`motions_5_6_2010`

Préambule
=========

.. seealso:: :ref:`label_confederal`

Cette motion reprend en substance la procédure administrative adoptée à
l'occasion du CCN des 16 et 17 mai 2009 à Bordeaux.

Son adoption par le  congrès viendrait compléter la motion consacrée à la
labellisation des syndicats.

Faute de temps, le Congrès confédéral de 2010 n’avait pas pu en discuter
sa première version.

Nous la re-présentons amendée de la contre-motion de l’ESS 34 du Congrès
2010.


Argumentaire
============

Pour la CNT et ses instances d'administratives (BC, CA), être en capacité de
déterminer de facon fiable quels sont les syndicats adhérents ou actifs est
essentiel.

En effet, le nombre de syndicats considérés à jour de cotisations influe sur
le quorum d'ouverture du congrès confédéral.

Il n’est pas par ailleurs pas possible qu’un syndicat non adhérent de la CNT
puisse agir en son nom.

Pour le Bureau confédéral, ou même pour diférents mandats confédéraux de la
Commission administrative, la mise à jour de l'annuaire des syndicats influe
sur diférents domaines : envoi des circulaires confédérales, abonnements listes,
accès intranet, site web cnt-f.org, annuaire web, etc.

En vue de l'établissement d'une règle commune, le CCN avait donc été amené à
établir un processus transparent de relance de trésorerie pouvant permettre
d'éviter une dé-labellisation.

Les conséquences importantes d'une dé-labellisation pour le syndicat, comme
pour la Confédération des syndicats, nous amènent à proposer au congrès
d'entériner les dispositions suivantes :

Motion
======

En cas de retard de cotisations confédérales, avant tout acte de
dé-labellisation, la trésorerie confédérale sous couvert du Bureau confédéral
pourra relancer les syndicats concernés de la facon suivante :

1. En cas de retard de plus de 12 mois, le/la trésorierE confédéralE relance
   par mail ou courrier simple. Une attente minimum d'un mois pour permettre
   la réponse est requise (par courrier, mail, téléphone...).
2. Si aucune réponse ne parvient au Bureau confédéral (ou peu satisfaisante),
   une relance par courrier postal de le/la trésorierE confédéralE à l'adresse
   du syndicat. Une attente minimum d'un mois pour permettre la réponse est
   requise (par courrier, mail, téléphone...).
3. Si aucune réponse ne parvient au Bureau confédéral (ou peu satisfaisante),
   une dernière relance est adressée au syndicat par le Bureau confédéral par
   courrier postal avec accusé de réception. Une attente minimum d'un mois
   pour permettre la réponse est requise (par courrier, mail, téléphone...).
4. Si aucune réponse, envoi dernier courrier du BC avec accusé de réception
   annoncant la dé-labellisation CNT. L'UR concernée et la Fédération
   d’appartenance, s'il elles existent, sont informées simultanément, ainsi
   que l'ensemble des UR par circulaire confédérale (et par la liste web
   syndicats). Dans un délai de 2 mois, lorsqu’il y a opposition d'une Union
   régionale, le BC doit suspendre sa décision, qui doit être soumise au
   prochain CCN.


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°40          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
===========

- :ref:`amendement_motion_40_2012_stea`
