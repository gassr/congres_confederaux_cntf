
.. _motions_presse_confederale_cnt_congres_lille_2008:

====================================================================================================
Motions Presse confédérale de la CNT 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
====================================================================================================



Nouve(au/(elles) adhérentEs
===========================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 37         | 3        | 4          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Tout-te nouvelLE adhérentE à la CNT, **avec son accord**, se verra abonnéE aux
**3 mois gratuits** du Combat Syndicaliste.

Charge à son syndicat de transmettre les coordonnées de cetTE adhérentE ou de
recevoir son abonnement pour lui transmettre.


AdhérentE avec un bas revenu
============================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 36         | 6        | 3          | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

ToutE adhérentE avec un bas revenu peut avoir accès à un abonnement à
"tarif réduit" (12 euros/11n°).

**La demande d'abonnement est transmise par son syndicat.**
