.. index::
   pair: intranet ; 2004
   ! Signature des courriels

.. _motion_liste_conf_2004:
.. _motion_listes_2004:

=============================================================================================================
**Motion Internet & intranet** de la CNT  28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
=============================================================================================================

.. seealso::

   - :ref:`motions_internet_intranet_cnt`



Rôle et régulation de la liste confédérale
==========================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 34         | 9        | 18         | 11                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


.. _texte_motion_liste_conf_2004:

Texte
-----

En conséquence de quoi, le Congrès rappelle que la liste dite **Confédérale**
n'est qu'un outil d'information qui ne peut se substituer à la souveraineté
des syndicats de la Confédération réunis en Assemblée générale ;

**Cette liste doit être modérée**, et le rôle du modérateur est de faire
respecter sa finalité et donc d'éliminer tout ce qui n'entre pas dans
son objet.


**Règle d’utilisation et gestion des abonnements de la liste confédérale, Rôle et régulation de la liste confédérale**
=========================================================================================================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 31         | 6        | 19         | 14                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Le Congrès demande au **postmaster** de la liste confédérale, de vérifier
au plus tôt la réalité d'appartenance et d'adhésion à un syndicat CNT
des personnes inscrites sur la même liste.

**Le Congrès demande, pour la clarté des débats, que les participants
des listes signent de leur nom (ou prénom) & de leur appartenance syndicale
(ex : Albert -Syndicat CNT " X "), et donc que le postmaster veille à le
faire respecter.**
