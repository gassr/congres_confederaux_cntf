
.. _contre_motion_8_2021_ste72:

==========================================================================================================
Contre-motion N°3 à la motion n°8 2021 **Prostitution et Abolitionnisme révolutionnaire** par **STE72**
==========================================================================================================

- :ref:`motion_8_2021_etpics94`

Autres motions STE72

    - :ref:`syndicats:motions_ste72`


Argumentaire
================


Contre-Motion
=============

La CNT combat toutes formes de dominations patriarcales ainsi que toutes
formes de marchandisations de l’être humain.
À ce titre, la CNT combat toute forme forcée de travail du sexe (proxénétisme,
réseaux mafieux, exploitation de migrant·es...etc.)

Pour autant, le travail du sexe peut être un travail à part entière s'il
est pratiqué par une personne majeure qui en a fait librement le choix.

C’est pourquoi la CNT s’engage à :

- Défendre syndicalement toute personne travailleuse du sexe qui le
  souhaite et/ou à quitter un quelconque réseau criminel.
- Accompagner toute personne travailleuse du sexe à la sortie du travail
  du sexe si elle en exprime le souhait.
- Lutter contre et viser à abolir tout trafic, migrations forcées,
  implication de personnes mineures ou autre abus de pouvoir qui
  imposerait d'une façon ou d'une autre le travail du sexe à une personne.
- Lutter contre toutes les formes de dominations, économiques ou autres,
  qui peuvent d'une façon ou d'une autre altérer la liberté de choix
  d'un travail du sexe. »

Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°8 |            |          |            |                           |
| STE72                         |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
