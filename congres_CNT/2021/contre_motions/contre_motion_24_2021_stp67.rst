
.. _contre_motion_24_2021_stp67:

==========================================================================================================================
Contre-motion  N°6 à la motion n°24 2021 **Gestion interne des violences patriarcales** par **STP67** **[Adoptée,🆗]**
==========================================================================================================================

- :ref:`motion_synthese_24_2021_ste38_stp_67`
- :ref:`motion_24_2021_ste33_cnt42`

Argumentaire
==================

Notre contre motion est assez proche de celle du STE38.
Tout comme ielles, nous pensons qu’il est possible d’avoir un protocole
pour gérer les violences patriarcales et sexuelles, et que la :ref:`motion 24 <motion_24_2021_ste33_cnt42>`
ne perd rien à respecter l’autonomie des syndicats.

Elle peut d’ailleurs être utile en rendant la procédure moins dépendante
du :term:`BC`, et en faisant en sorte que les référent·e·s soient plus
indépendant·e·s via l'usage d'un pseudo.

Nous avons défini la suspension, en partant du principe que la confédération
et les fédérations devaient refuser les mandats de personnes accusé·e·s,
mais permettre leurs présence dans des formations et les groupes de travail,
avec l’accord de toutes les autres personnes.

En effet il est possible — et souhaitable — que des formations et des
groupes de travail apparaissent en interne afin d’accompagner des
personnes porteur·euse·s d’oppressions, notamment patriarcale.

Dans cette même optique, notre contre-motion permet toujours de demander
à un syndicat d’exclure un·e de ses membres, et de sinon dé-labelliser
ce syndicat.
Cependant nous avons voulu citer d’abord d’autres possibilités, pour que
des pratiques alternatives demeurent à l’esprit des personnes durant
la procédure.



Contre motion
===============

**Titre : Gestion interne des violences sexuelles, sexistes et patriarcales**

Aucune personne reconnue coupable de viol/ violences patriarcales n’a
sa place à la CNT.

Adoption par le 35ème congrès confédéral de la CNT d'un protocole qui
vise à établir des outils et une procédure à suivre systématiquement
dès qu'une accusation de viol et/ou de faits de violence patriarcale
est portée à la connaissance de membres de la CNT contre un·e de ses membres.

- Toute personne accusant de viol et/ ou agression sexuelle un membre
  de la CNT est considérée comme une victime et bénéficie de tous les
  traitements et égards dus à ce statut (aide psychologique, juridique,
  financière en cas d’incapacité au travail) et ce le temps nécessaire
  à l’instruction du viol et/ou agression sexuelle et sans jugement au
  préalable de son issue.

- La CNT met en place une liste de personnes référentes pour la prise
  en charge d’accusations de viol et de violence patriarcale mandatées
  par leurs syndicats.
  Cette liste est actualisée à chaque fois syndicat mandate un·e référent·e
  par ses membres.
  La liste des référent·e:s pourra être anonyme, mais devra faire au
  moins figuré le syndicat d’appartenance, et être transmis à l’ensemble
  des syndicats.
  Comme pour tout mandatement au sein d’une commission de travail, les
  syndicats s’assurent que les personnes mandatées soient formées à ce
  qui constitue l’objet de la commission (lutte contre le patriarcat,
  prise en charge de violences) ou se forment pour cela.
  La victime (elle-même ou par l’intermédiaire d’une tierce personne)
  peut saisir cette liste de référant·e pour qu’elle constitue une
  commission, en mixité ou non selon le choix de la victime.
  La liste des membres de cette commission avec leurs syndicats
  d’appartenance sera transmis à la victime, ou la personne intermédiaire
  qu’elle aura choisit, et elle pourra en récuser des référent·e·s.

- Aucune démarche, décision ne sera mise en œuvre sans l’approbation
  préalable de la victime. Cette commission extraordinaire a tout mandat
  pour instruire l’affaire, décider quand lever l'anonymat de l’agresseur/ violeur
  et diffuser le rendu de ses travaux à la Confédération.
  Elle n’est nommée que pour le temps de l’instruction.
  Comme toute commission de travail confédérale, la commission extraordinaire
  peut solliciter des contributions externes à la CNT.
  La victime peut décider, à n’importe quel moment de la procédure et
  quelles qu’en soient les raisons, ou de saisir d’autres personnes parmi
  la liste de référent·e·s directement ou via leur syndicat.

- La CNT, à travers le BC, doit se donner les moyens (financiers notamment
  et/ ou d'hébergements, pour d'éventuels déplacements, voire informatiques
  à travers un espace d'échange sécurisé) du bon fonctionnement et
  déroulement de la procédure.

- Les membres du syndicat et de l’UD de l’accusé·e aussi bien que de la
  victime ne peuvent en aucun cas faire partie de la commission extraordinaire
  pour éviter tout risque de partialité et de pressions extérieures.

- Dès sa constitution, la commission extraordinaire informe le syndicat
  local de l’accusé·e des accusations portées contre lui/ elle (que la
  victime soit elle-même adhérente de la CNT ou pas).
  À partir de cette information, par principe de précaution, l’accusé·e
  est suspendu·e de la CNT, et ce pendant tout le temps nécessaire à
  l’établissement du rapport.
  Le syndicat local d’appartenance de l’accusé·e est responsable de
  l’application de la suspension. Il est clair que cette suspension est
  provisoire et n’est pas une prise de décision de la CNT sur le fond
  de l’affaire. Cependant, les organisations confédérale ou fédérales
  ne reconnaissent aucun mandat confié à une personne suspendue.
  De même, la participation aux groupes de travail, et aux formations
  ne sont possibles qu’au consensus des syndicats y ayant des participant·es.

- La commission extraordinaire a pour mission de recueillir la parole
  de la victime (ce faisant de lui permettre de sortir d’un silence
  préjudiciable pour elle à court, moyen et long terme) et tout autre
  témoignage qu’elle jugera nécessaire. Elle pourra par ailleurs
  l’accompagner dans ses démarches (y compris juridiques) si elle est la
  demande de la victime.

- Quelle que soit la décision de la victime de saisir ou non la justice,
  elle ne pourra en aucun cas être jugée sur sa décision par la CNT.
  En cas de refus de porter plainte, il ne pourra en être tenu rigueur
  à la victime et cela ne vaudra pas non plus mise en doute de son accusation.
  La commission ne doit en aucun cas se substituer à la victime et
  l’influencer sur cette décision.

- Une fois la parole de la victime recueillie, ainsi que tout autre
  témoignage que la commission jugera nécessaire, la commission
  extraordinaire rend ses conclusions aux référent·e·s.
  Les référent·e·s et la commission extraordinaire rédigent un rapport
  à partir de ces conclusions qui sera remis à tous les syndicats de la Confédération.

- Selon ses conclusions, la commission extraordinaire pourra demander
  au syndicat d’appartenance de l’accusé·e de proposer une voie de
  résolution, après lui avoir transmis son rapport ; si cette voie de
  résolution est suffisante pour la victime, elle sera retenue.
  Par exemple, il peut s’agir d’accompagnements, de formations, ou de
  suspension temporaire auprès de la confédération. Si cette médiation
  échoue, la commission peut demander jusqu’à son exclusion en conformité
  avec les statuts. En cas de refus du syndicat de répondre à cette
  demande et plus largement en cas de non-respect de ce protocole ou
  d’obstruction à son application, la commission pourra préconiser la
  suspension du syndicat au prochain CCN, et son exclusion au prochain
  congrès, en accord avec les règles organiques « Élaboration de l’ordre du jour du CCN »,
  « Commissions de travail confédérales : rôle, limites et compétences »
  et avec l’article 24 des statuts confédéraux (« gestion des conflits internes »).
  La CNT communiquera alors publiquement sa décision.

- Enfin le travail de cette commission est complémentaire d’un travail
  mené auprès de l’agresseur (orientation notamment vers une structure
  d’accompagnement des personnes auteures de violences sexuelles -structure
  reconnaissant le système de domination patriarcale), qui peut en constituer un
  prolongement, sous réserve que ce travail soit compatible avec la
  protection de la victime et que l’agresseur en reconnaisse la nécessité
  et son inscription dans un système de domination patriarcale.


Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°24 |            |          |            |                           |
| STP67                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
