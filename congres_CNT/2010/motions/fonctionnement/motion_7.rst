


.. index::
   Fédérations d'industrie

.. _motion_7_2010:

================================================================================
Motion n°7 2010 : Labellisation des fédérations d’industrie, SIPM-RP [non votée]
================================================================================




.. seealso::

   - :ref:`motions_sipm_rp`


Motion reprise par Chimie-Bzh en 2012
=====================================

- :ref:`motion_41_2012`


Argumentaire
============

Si la :term:`C.N.T` décide de créer légalement des fédérations aptes à désigner
des représentants de sections syndicales, il est nécessaire de mettre en place
une labellisation des fédérations, afin de savoir quelle fédération existe
réellement au sein de la :term:`C.N.T` et peut donc s’y exprimer, par exemple
en étant présente au :term:`C.C.N`.

Les fédérations non labellisées n’auront alors vocation qu’à pouvoir créer des
section syndicales, selon la procédure indiquée dans la motion n°40
(voir :ref:`IV - Stratégie syndicale <strategie_syndicale_congres_2010>`).

Quant aux fédérations déjà existantes, on considère qu’elles sont, de fait, labellisées.

Motion
======

Une fédération d’industrie nouvellement créée doit être labellisée par
un :term:`C.C.N`, en suivant la même procédure qu’un syndicat.

Elle doit être composée d’au moins trois syndicats d’industrie différents.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°7           |            |          |            |                           |
| SIPM-RP              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
