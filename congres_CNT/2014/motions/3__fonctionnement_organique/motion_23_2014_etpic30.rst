
.. index::
   pair: Motion 23 ; 2014

.. _motion_23_2014_etpic30:

=========================================================================================
Motion n°23 2014 : Evolution du tarif et du format du Combat syndicaliste (ETPIC 30 sud)
=========================================================================================

- :ref:`motions_etpic30`

Argumentaire
=============

Le Combat syndicaliste connaît des évolutions qualitatives importantes
ces dernières années mais sa diffusion reste trop limitée.

La vocation première de l’organe mensuel de la CNT est de connaître une
diffusion la plus large possible, comme outil de presse et de propagande.

En lien avec la mise en ligne intégrale du Combat Syndicaliste et à
son accès gratuit à tous, nous proposons que la version papier connaisse
une évolution du format et du prix pour une diffusion plus aisée et
la plus large possible.

Le format actuel, certes esthétique, nous apparaît trop lourd et
trop encombrant.

Nous privilégions aujourd’hui une impression plus légère, voire moins
chère, de type format «poche» (type nouveau format des magasines en A5
ou même en format A4 sur papier journal) sans pour autant en diminuer
le nombre et la qualité des articles.

Il nous semble à l’avenir pertinent de tendre à la gratuité, ou prix libre.

Motion
======

Le Combat Syndicaliste a pour vocation d’être diffusé le plus largement
possible. Son format et son prix doivent être rendus plus accessible
(lecture gratuit sur internet, format plus « ergonomique », prix minimum
voire prix libre).

Le Combat Syndicaliste change de format pour passer à un format plus
petit et plus léger en termes d’épaisseur papier.

Son coût doit être réduit au maximum, tout en intégrant autant que
possible l’utilisation du papier recyclé (motion du 28ème Congrès Confédéral
de Janvier 2004) et en conservant une belle facture.

Le Comité de rédaction, l’équipe d’administration, et la
``commission CS en ligne`` sont en charge d’assurer collectivement
cette étude et de proposer des formules format / coût / tarif
au CCN suivant le Congrès.

Les CCN suivants le Congrès confédéral seront en charge de valider
ces propositions.

Contre motion
==============

.. seealso::

   - :ref:`contre_motion_23_2014_ess34`

Amendement
==============

.. seealso::

   - :ref:`amendement_motion_23_2014_sse31`

Contre-motions
===============

.. seealso::

   - :ref:`contre_motions_22_23_24_2014_sipmcs`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°23 2014

       :ref:`ETPIC30 <etpic30>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
