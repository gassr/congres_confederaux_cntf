

.. _contre_motions_5_6_7_etpic30:

================================================================================================
Contre-motion aux motions 5, 6 et 7  ETPIC30
================================================================================================

.. seealso::

   - :ref:`motion_5_2012_stea`
   - :ref:`motion_6_2012_ste72`
   - :ref:`motion_7_2012_ste72`




Argumentaire
============

Notre syndicat est d’accord avec le fond de la motion du STEA concernant la
nécessité de gérer le temps de facon plus rigoureuse au congrès confédéral, la
nécessité de respecter l’ordre du jour afin que l’ensemble des motions
proposées puisse être examiné.

Nous trouvons néanmoins trop rigides les solutions et modalités proposées en
termes de limitations du nombre de prises de parole, et du temps imparti à
chaque prise de parole ; nous considérons d’ailleurs comme une perte de temps
considérable le fait de devoir faire le déplacement à la tribune à chaque
prise de parole.

Aussi, nous proposons donc les modalités suivantes au débat et au vote du
congrès:

La gestion du temps est confiée à l’arbitrage de la Présidence de séance et
à ses assesseurs nommés par le congrès.

Chaque motion est observée selon 3 étapes successives:

1. Vote indicatif (décompte simple sans identification des votes)
2. Discussion
3. Vote final

Le temps consacré à la discussion (2) de chaque motion est déterminé
proportionnellement aux résultats des votes indicatifs, étant entendu que
plus les majorités dégagées au vote indicatif sont fortes, moins le temps
consacré aux discussions sera important.

Dans le cadre d’une très forte majorité exprimée au vote indicatif, la minorité
conserve toujours un droit d’expression.

Il est par ailleurs établi une double liste de tour de parole permettant de
privilégier l’expression des syndicats s’étant le moins exprimés.

Vote
====

+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion aux motions 5,6 et 7 |            |          |            |                           |
| ETPIC30                            |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès                |            |          |            |                           |
|                                    |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+
