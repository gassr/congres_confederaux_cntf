
.. index::
   pair: 2012; Congrès CNT (Metz)
   pair: XXXII; Congrès CNT 2012 (Metz)

.. _congres_CNT_2012_metz:
.. _congres_CNT_metz_2012:
.. _xxxii_congres_CNT:
.. _xxxii_congres:

=======================================================================
Le **XXXIIe Congrès CNT 2012 de Metz (/mɛs/)** du 1 au 4 novembre 2012
=======================================================================

.. seealso::

   - :ref:`congres_statuts_CNT`
   - :ref:`fonctionnement_congres_confederal`





Metz
=====

.. figure:: images/metz_carte.png
   :align: center

   https://fr.wikipedia.org/wiki/Metz

Metz /mɛs/ est une commune française située dans le département de la
Moselle, en Lorraine. Préfecture de département, elle fait partie, depuis le
1er janvier 2016, de la région administrative Grand Est, dont elle accueille
les assemblées plénières.
Metz et ses alentours, qui faisaient partie des Trois-Évêchés de 1552 à 1790,
se trouvaient enclavés entre la Lorraine ducale et le duché de Bar jusqu'en 1766.

Par ailleurs, la ville a été de 19742 à 2015, le chef-lieu de la région de Lorraine.

Au dernier recensement de 2017, Metz comptait 116 429 habitants, ce qui en fait
la commune la plus peuplée de Lorraine et la troisième du Grand Est, après
Strasbourg et Reims.

Son unité urbaine compte environ 290 000 habitants et son aire urbaine 391 187
habitants en 2016, faisant d’elle la deuxième aire urbaine de Lorraine et la
troisième du Grand Est après celles de Strasbourg et Nancy.

Ses habitants sont appelés les Messins.

.. _syndicats_presents_2010_metz:

Nombre de syndicats présents à Metz, 2012: 60
======================================================

60 syndicats présents ou représentés au Congrès sur 94 à jour de cotisations.

Nous vous annonçons et confirmons tout d'abord que, tout comme décidé
lors du dernier CCN, le prochain congrès confédéral aura bien lieu du
jeudi 1er (férié) au dimanche 4 novembre 2012 à Metz.

.. hlist::
   :columns: 3

   * 07 Interpro,
   * 09 Interco,
   * 12 Interco,
   * 13 Education,
   * 13 STICS,
   * 25 Interco,
   * 27 SGI Interco,
   * 29 STAF Quimper,
   * 30 ETPIC Sud,
   * 30 SITAC Nord,
   * 30 STTLA,
   * 31 Interco,
   * 31 SSE,
   * 32 Interco,
   * 33 PTE (PTT),
   * 33 Santé & Action sociale,
   * 33 SUB-TP BAM,
   * 34 ESS (éduc),
   * 35 ChiMEE,
   * 35 ETPRECI,
   * 35 SS-FPT,
   * 35 STE, 37 PTT Centre,
   * 38 Santé-Social CT,
   * 38 STICS,
   * 42 Education,
   * 42 Interpro,
   * 44 SINR (Interco),
   * 49 STE,
   * 49 Santé-social,
   * 53 STE,
   * 53 STPM,
   * 54 Interco,
   * 57 Education,
   * 57 ETPICS,
   * 57 Interco Forbach,
   * 57 STTLA,
   * 59 STIS,
   * 59-62 STERC,
   * 66 Education,
   * 66 Interco,
   * 66 PTT,
   * 67 STEA (Education),
   * 67 STP (Interco),
   * 69 PTT,
   * 69 SanSo,
   * 69 Travail & affaires sociales - STAS RA,
   * 69-01 SUTE (Education),
   * 71 Interco,
   * 72 STE,
   * 72 STP (Interco),
   * 74 Interco,
   * 75 Culture-Spectacle RP
   * 75 Energie RP
   * 75 ETPRECI, 75 SGTL RP
   * 75 SII RP
   * 75 SIM RP
   * 75 SIPM RP
   * 75 SS-FPT RP
   * 75 STE,
   * 75 SUB-TP-BAM RP
   * 92 Interpro,
   * 93 Education,
   * 94 ETPICS,
   * 94 STE,
   * 95 Interco,
   * 95 PTT

Motions
=======

.. toctree::
   :maxdepth: 2


   motions/motions


Contre-motions
==============

.. toctree::
   :maxdepth: 2


   contre_motions/contre_motions


Amendements
============

.. toctree::
   :maxdepth: 2

   amendements/amendements


Etat des motions
================

.. toctree::
   :maxdepth: 2


   etats_motions/etats_motions
