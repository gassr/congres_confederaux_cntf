.. index::
   ! meta-infos

.. _doc_meta_infos:

=====================
Meta
=====================

Gitlab project
================

- https://gitlab.com/cnt-f/congres_confederaux


Commits
-----------

- https://gitlab.com/cnt-f/congres_confederaux/-/commits


Fichiers source
------------------

- https://gitlab.com/cnt-f/congres_confederaux/-/tree/main


Issues
--------

- https://gitlab.com/cnt-f/congres_confederaux/-/boards

Pipelines
-----------

- https://gitlab.com/cnt-f/congres_confederaux/-/pipelines


Membres
---------

- https://gitlab.com/cnt-f/congres_confederaux/-/project_members

pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
