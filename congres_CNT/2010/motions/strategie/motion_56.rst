
.. index::
   pair: Motion; Orientation syndicale
   pair: 56; Orientation Syndicale

.. _motion_56_2010:

====================================================================
Motion n°56 2010 : Orientation syndicale, Education 69-01 [adoptée]
====================================================================



Argumentaire
============


**La C.N.T s'apprête à franchir une étape décisive pour son développement à
venir**.

D'une part, depuis :ref:`congres_CNT_2008_lille` , des militant-e-s, investi-e-s
depuis plusieurs années, nous ont malheureusement quitté, usé-e-s de
voir les efforts de construction et d'implantation syndicale anéantis par des
polémiques internes stériles voire des questions de pouvoir sans lien et sans
conséquences sur les luttes sociales et du monde du travail.

D'autre part, nous n'avons pas été capables d'attirer les syndiqués en rupture
avec leurs bureaucraties, de plus en plus nombreux, au sein des autres
organisations syndicales. Au delà des questions internes à la vie de
l'organisation, le véritable enjeu en terme sociétaire est, aujourd'hui, de
**pouvoir peser sur les luttes sociales actuelles**, de faire face à l'offensive
sans précédent du patronat sur le monde du travail et de contribuer au
développement d'un mouvement social émancipateur.

De plus, il s'agit de faire vivre un syndicalisme de lutte, et qui plus est
révolutionnaire, par-delà les tentatives de normalisation du syndicalisme et
les logiques d'intégration et de collaboration de plus en plus fortes des
institutions étatiques mais aussi **des grandes centrales syndicales**.

Une logique de normalisation syndicale ayant pour corolaire un risque évident
de marginalisation voire **de criminalisation ou même, tout simplement, de
disparition du modèle syndical que nous incarnons**.

Dans ce contexte se posent inévitablement pour la C.N.T des enjeux de choix et
de priorités.

Notre organisation est souvent sollicitée de toutes parts, de par
son identité multiple, sa réalité militante de terrain et ses aspirations
révolutionnaires. En raison de nos forces réelles, de notre réalité numérique
et d'implantation, il y a une véritable nécessité à opérer des choix tactiques
et stratégiques et à définir des priorités d'action afin d'éviter deux écueils:

- celui d'une intervention militante basée sur un activisme politique radical
  du type d'une agitation gauchiste
- celui d'un repli dans une tour d'ivoire idéologique et identitaire nous
  enfermant dans une posture **seul face au monde** mais sans prise réelle sur
  celui-ci et inefficace par rapport à notre projet d'ensemble.

Il ne s'agit pas pour autant de hiérarchiser les luttes ni d'écarter certaines
thématiques qui peuvent être chères à certain-e-s mais de définir une méthode
d'intervention sociale en partant de ce qu'est la réalité d'une organisation
syndicale, auto-gestionnaire et révolutionnaire.

Cette motion n'a pas vocation à être un catalogue de revendications et ne peut
donc se substituer à une plate-forme revendicative.

Elle vise à fixer quelques bases stratégiques dans l'objectif d'un
développement  homogène d'ensemble de notre organisation syndicale.
**Il s'agit avant tout de doter notre organisation d'une motion cadre, orientant
son développement pour les années à venir**.

Une telle démarche revient à choisir pour objectif commun de développer la C.N.T
comme un syndicat ayant l'ambition de devenir une organisation de **masse**,
même modérément dans une premier temps.

**Une organisation ayant défini des buts et les moyens pour y arriver.**

Une organisation ancrée dans les réalités des luttes du monde du travail
d'aujourd'hui et porteuse d'un projet révolutionnaire, tout en offrant un
espace d'émancipation individuelle et collective.

.. _motion_56_orientation_syndicale_saint_etienne_2010:

Motion orientation syndicale, Saint Etienne, 2010
=================================================

Dans le contexte économique, social et politique actuel, le rôle de la C.N.T
doit être de faire vivre une expression syndicale permettant dans un premier
temps de maintenir l'existence d'un syndicalisme de classe et de lutte, tout
en rendant crédible cette alternative face aux évolutions actuelles du
syndicalisme dans son ensemble, et dans un second temps de pouvoir construire
sur ces bases une organisation révolutionnaire ouverte à un maximum de
travailleurs.

**Cette organisation doit être inscrite dans la tradition et la finalité
révolutionnaire qui nous sont propres.**

Néanmoins cela ne doit pas nous enfermer dans une posture idéologique et
incantatoire stérile mais doit au contraire, comme à toutes les époques où
l'anarchosyndicalisme et le syndicalisme révolutionnaire ont su peser sur
le mouvement ouvrier et l'évolution des sociétés, pouvoir proposer un outil
adapté au monde du travail contemporain basé sur une tactique, une stratégie
et un projet social.

Dans cette perspective, la C.N.T se donne aujourd'hui comme priorité les axes de
développement et d'intervention suivants :

Favoriser un développement et une implantation axés sur des bases syndicales
----------------------------------------------------------------------------

La réalité d'un syndicat est avant tout basée sur des adhésions issues du monde
du travail, en tant que salarié-e-s, sur une base professionnelle puis
interprofessionnelle.

La crédibilité de la C.N.T passe donc tout d'abord par l'ancrage et **l'action de
ses militant-e-s dans leur réalité professionnelle**.

Ce constat met en avant l'importance du développement de *syndicats
professionnels*. Ceux-ci peuvent par exemple axer leur action sur la
constitution de sections d'entreprises ou d'établissements et la réalisation
de bulletins ou **tracts professionnels locaux**.

Par ailleurs, le syndicalisme prôné par la C.N.T est fondé sur l'importance
du **lien interprofessionnel**. C'est ce lien qui donne la dimension de
solidarité de classe, un des fondements de notre pratique et de notre
projet révolutionnaire.

Ces deux constats amènent à acter l'importance de la **double structuration
des syndicats professionnels**:

- regroupés d'une part au plan local puis à l'échelle nationale en fédérations
  professionnelles et/ou d'industries, et,
- d'autre part, organisés localement dans des structures interprofessionnelles
  (unions locales ou départementales puis régionales).

La mise en place de **fédérations professionnelles** permet l’action revendicative
au niveau professionnel à l’échelle nationale, entraînant la solidarité dans
une même branche.

Les **organismes territoriaux, unions locales ou départementales**, manifestent
un autre type de solidarité, dépassant les diversités des professions pour
s’inscrire dans des rapports de proximité et de solidarité
interprofessionnelles sur une **base géographique**.

**La force de notre syndicalisme doit venir de cette action coordonnée entre une
action professionnelle, du local au national, et un enracinement local
interprofessionnel**.

Afin de faciliter le développement de *ces deux échelles*, indispensables à
l'enracinement d'une organisation syndicale révolutionnaire, il convient d'acter
que la structuration en fédérations nationales professionnelles doit s'accompagner
aussi d'une large autonomie d'action et d'orientation de celles-ci en lien
avec la réalité et la grande diversité des différents secteurs professionnels,
dans le cadre des orientations confédérales.

Il s'agit ainsi de respecter un des principes essentiels de notre syndicalisme
qui s'élabore de bas en haut, par la pratique et l'expérimentation sociale.

Néanmoins, il faut être conscient qu'une fédération professionnelle ne peut se
constituer que lorsque sont atteints une diversité d'implantation géographique
et un seuil numérique suffisants.

**Dans cette perspective, le rôle des syndicats interco et/ou interpro,
essentiels dans un premier temps pour pouvoir grouper les syndiqué-e-s isolés
dans leur secteur professionnel, doit être de favoriser et d'aider à la
création de syndicats professionnels dans différents domaines en lien avec
ses adhérents, tout en offrant un espace de solidarité et de formation
syndicale.**

Ils n'ont pas pour but de se pérenniser en tant que tels ni d'offrir une
finalité en tant qu'adhésion.

Cette méthode doit tendre à faire émerger une unité d'intervention au plan
confédéral et à donner une lisibilité sur l'extérieur du syndicalisme que
l'on propose et du projet qu'il porte.

Afin de permettre un développement syndical de ce type, **la C.N.T doit se doter
d'une véritable politique d'aide au développement syndical et se donner
les moyens humains et matériels de cette politique.
Un tel choix doit constituer une priorité dans la période actuelle.**

De plus, la construction de ce type de syndicalisme ne pourra se faire que
par un travail syndical de base. Au-delà de luttes thématiques, souvent
conjoncturelles et médiatiques, la priorité reste celle d'assurer la **besogne
syndicale** de base, certes souvent peu excitante et valorisante mais
fondamentale pour notre implantation et notre développement à long terme.

Une **besogne syndicale** qui consiste à mener une action de renseignement,
d'information et de défense des collègues tout en luttant sur des
revendications concrètes et immédiates de nos secteurs professionnels,
en **diffusant inlassablement, par l'écrit ou l'oral, des éléments d'analyses
et d'informations sur les évolutions en cours du monde du travail, de la
société en général et des perspectives à court ou long terme que nous
pouvons avancer.**

Une **besogne syndicale** qui visera toujours à faire de nos collègues de
travail des acteurs des luttes. Le moteur interne de notre fonctionnement
à tous les niveaux doit être la pratique de la **démocratie directe**.

Pas de hiérarchie, de centralisation ni d'activisme militant se substituant
aux décisions des :term:`A.G` syndicales.

**Le syndicat professionnel  d'appartenance et ses assemblées générales restant
l'organe de base des décisions.**

**Notre autonomie d’action s’exprime principalement au travers des principes de
démocratie et d’action directe revendiqués comme pratique syndicale.**
La pratique de l’action directe comme mode d’action n’est que le prolongement
des conceptions de démocratie directe.

Nous devons réaffirmer, loin de tout radicalisme contemporain voulant faire
de l'action directe un mode d'action violent, l’action directe comme une
pratique syndicale où ce sont les travailleurs eux-mêmes qui interviennent
directement dans leur lutte, à tous les niveaux et à toutes les étapes, sans
recours aucun à des spécialistes de la représentation et de la négociation.

**L’action directe c’est l’idée toute simple que ce sont les personnes
directement concernées qui agissent en définissant, menant et contrôlant
collectivement leur mouvement.**

Ces remarques n'empêchent pas néanmoins que des camarades possédant des
compétences  ou souhaitant se former dans des domaines précis puissent se
spécialiser sur des tâches très spécifiques comme les outils juridiques ou
de communication. Dans ce cas, ces mandats doivent bien évidemment être contrôlés
collectivement par le syndicat, l'union locale, la fédération correspondante
ou la confédération. Enfin, notre syndicalisme doit être vécu comme une école
d'émancipation au plan individuel comme collectif.

Au plan individuel, il n'est pas nécessaire au départ d'être révolutionnaire
pour adhérer à la C.N.T. Notre but est d'attirer le plus grand nombre de
travailleurs dont la caractéristique commune est *le statut d'exploité*.

Le rôle de la C.N.T est alors de faire que les travailleurs qui y adhèrent
deviennent révolutionnaires par la pratique de lutte que nous proposons doublée
de la **dimension éducative** qui est la nôtre.

Dans ce cadre **le syndicat doit jouer un rôle d'éducation populaire permanent
par la formation et l'organisation régulières de débats, internes comme
externes, la diffusion de nombreuses publications et l'édition de journaux,
revues ou livres.**

Collectivement, notre syndicalisme est une véritable école d’émancipation par
la pratique de l’autogestion, de l’auto-organisation interne.

En partant du principe que l’autogestion sera le moteur de la société future
et que celle-ci n’est pas innée, mais s’apprend, se pratique et se confronte
à la réalité, nous **devons la mettre au centre de nos dynamiques et de nos
initiatives**.

Cela renvoie aussi à l’idée que **le mode d’organisation et de fonctionnement
que nous choisissons aujourd’hui, dans la société dans laquelle nous luttons,
doit refléter et préfigurer le modèle de société que nous voulons**.

D’où le développement des pratiques d’autogestion dans tous les domaines, de
démocratie et d’action directe.

Axer notre intervention sur les luttes du monde du travail, sur les luttes et revendications ouvrières contemporaines
---------------------------------------------------------------------------------------------------------------------

Afin de donner une réalité et une crédibilité au quotidien à notre syndicalisme,
il est nécessaire dans un premier temps de se *saisir des revendications
immédiates liées aux conditions de travail de nos différents secteurs
professionnels mais aussi de conditions de vie*. C'est une telle démarche qui
peut nous permettre de toucher concrètement nos collègues de travail et espérer
ainsi les entraîner dans des luttes revendicatives auxquelles nous pourrons aussi
donner un caractère révolutionnaire.

Cela sera d'autant plus accentué que nous pourrons proposer lors de ces luttes
notre forme d'auto-organisation dans le but de contrecarrer d'éventuels
bureaucrates syndicaux et **surtout de redonner confiance en sa force à la
classe ouvrière**.

Pour cela, il est indispensable de mener la laborieuse **besogne syndicale**
évoquée précédemment. Il s'agit aussi d'accepter de partir de la réalité de
terrain, des luttes, nécessité et aspirations du monde du travail plus que
de ce qui nous semble être nos priorités ou thèmes de prédilections.

Ce mouvement de sensibilisation et de luttes revendicatives quotidiennes et
spécifiques doit également servir à nourrir un mouvement social d'ensemble
ayant pour objectif de **faire face à l'offensive sans précédent du patronat et de l'Etat
contre le monde du travail et ceux qui en sont exclus**.

Il s'agit en effet de se nourrir de nos luttes quotidiennes de terrain pour
créer ou alimenter des mobilisations d'ensemble contre les logiques globales
de destruction des conquêtes sociales, résultats de plus de 100 ans de
luttes ouvrières, pour, à terme, **être en mesure de reprendre l'offensive
sur le plan revendicatif en faveur du monde du travail**.

Dans cette perspective, les luttes et mobilisation liées aux grandes questions
du monde du travail et de la société en général telles que:

- **les retraites**,
- **la sécurité sociale**
- **le code du travail**
- ou encore les **services publics**,

nous semblent essentielles.

Par exemple, la protection sociale dans son ensemble (**retraite et
sécurité sociale**), de part sa nature actuelle, basée sur le principe de
répartition et le salaire continu versé tout au long de la vie, pose des
questions essentielles de répartition des richesses produites et plus
largement de **choix de société**.

Se saisir pleinement de ces questions et en faire des axes de
mobilisation prioritaires, c'est être capables de défendre à la fois des
conquêtes sociales essentielles en faveur du monde du travail pour notre
vie de tous les jours dans la société actuelle mais aussi se
saisir de thèmes pouvant alimenter une réflexion et des perspectives
sur un **autre type de société**.

C'est dans ce sens que lors de chaque grève interprofessionnelle nationale,
la C.N.T se devra d'avoir une position claire, même si elle peut parfois être
très critique.

Dans tous les cas, la C.N.T ne doit pas rester en dehors de l'action et des
mobilisations syndicales.

Et là encore, pour ne pas rester attentiste vis-à-vis du timing de
mobilisation que les bureaucraties syndicales imposent au mouvement social,
la C.N.T se devra aussi souvent que possible d'offrir aux travailleurs
mobilisés des **espaces de discussions et d'échanges** qui, à terme,
ont vocation à devenir des **assemblées générales** décidant ou non de la
poursuite des mouvements de grève.

.. _motion_56_2010_revendications:

Articuler des revendications immédiates avec des revendications de rupture
--------------------------------------------------------------------------

Il est fondamental de lier nos revendications immédiates professionnelles et
de conditions de vie avec une analyse des causes de ces revendications et de
**proposer des solutions de rupture avec celles-ci**.

Ne pas se contenter de s'attaquer aux conséquences de l'exploitation et de
la domination capitaliste et étatiste mais aussi de s'attaquer à ses causes.

La capacité à articuler en permanence des revendications immédiates avec
des revendications de rupture donne corps à une pratique concrète à mettre
en oeuvre dans nos luttes syndicales quotidiennes.

Si nous ne pouvons oublier que le but immédiat du syndicalisme est l’amélioration
immédiate des conditions de travail et de vie, **il doit aussi être porteur
à terme d’un projet de transformation sociale**. Ni réformisme de principe,
ni incantation révolutionnaire stérile mais une capacité à partir de la réalité,
de revendications et de besoins concrets et à y apporter satisfaction par
l’organisation et la lutte dans la société du moment, tout en rendant lisibles
les causes de ces revendications et en élaborant des réponses sociétaires
alternatives.

Dans cette dynamique, la pratique de la grève est conçue comme une
**gymnastique révolutionnaire**, permettant d’améliorer le quotidien tout en
préparant un mouvement révolutionnaire. La C.N.T doit donc tendre à faire de
cette arme son moyen d'action privilégié tant au plan local sur des
revendications professionnelles qu'au plan national sur les grandes questions
touchant le monde du travail.

Elle doit se donner les moyens d'avoir une position lisible et des appels
clairs et relayés lors de chaque grande échéance nationale.

La promotion et la construction de mouvement de grève générale, qui ne doit
pas pour autant être une ritournelle systématique masquant notre incapacité
à porter des perspectives concrètes, s'inscrit alors comme une stratégie
révolutionnaire.

En effet un appel systématique à la grève générale n'aura plus aucune
crédibilité, par contre nous ne devons jamais perdre une occasion d'en
faire la promotion comme arme fondamentale du mouvement ouvrier dans
l'optique d'anéantissement de sa condition d'exploité.

**L'action interprofessionnelle est une fois de plus fondamentale dans cet
objectif**.

Enfin, face à l'offensive libérale au service du capitalisme, il y a, plus
que jamais, une véritable nécessité à faire émerger **un autre projet de
société anticapitaliste et anti-autoritaire**.



.. seealso::

   - :ref:`amendement_motion_56_2010_ste34`
   - :ref:`contre_motion_56_2010_ste92`


Vote indicatif orientation syndicale, Education 69-01
=====================================================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°56          | 36         | 19       |  6         |  9                        |
| Education 69         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**SUTE 69-01**
    La C.N.T est dans une étape importante : en interne, les débats importants
    de fonctionnement et les perspectives sont en questionnement ;
    en externe, nombreux enjeux liés aux mouvements sociaux /luttes importantes
    comme le mouvement sur les retraites etc.. confronté à la mutation du
    paysage syndical.

    C'est une motion d'orientation syndicale pour le développement de
    l'organisation visant à favoriser l'implantation d'une base syndicale,
    lutter au quotidien dans le monde du travail tout en luttant de
    manière générale contre le capitalisme : articuler une revendication
    de rupture, un autre projet de société avec nos luttes immédiates.
    Cette motion d'orientation est une sorte de feuille de route pour
    la C.N.T d'ici le prochain congrès et pour le futur :term:`Bureau confédéral`.


**Etpic 30**
    Contre car pas d'éléments nouveaux, ne sert qu'a faire un texte.

**STE 75**
    Contre, mais on trouve intéressant, mais n'est-ce pas un argumentaire ?
    changer le titre et à diffuser en interne comme texte de réflexion,
    formation. Beaucoup de points sont déjà en discussion dans les motions
    suivantes.

**Etpic94**
    Une question pour l'Educ 69, quel était leur vote pour le quitus du
    mandant du Secrétaire confédéral ?

**SUTE 69-01**
    Ne voit pas le rapport entre la question et motion discutée. Nos votes
    sur tous les quitus étaient de ne pas prendre part au vote, pas par
    manigance, non pas par arrière pensée, mais pour une raison : on a
    fait une réunion spéciale congrès, on a voté toutes les motions, on
    s'est positionné sur toutes les candidatures mais on ne s'est pas
    positionné sur les quitus.
    C'est pour cela qu'on a fait la demande que les nppv ne soient pas
    comptabilisés. Malheureusement statutairement ce n'est pas le cas.
    Mais on ne voit toujours pas le lien avec la motion qu'on discute.


**Etpic 94**
    C'était pour avoir des explications qu'on n'a pas eu hier. On en prend acte.
    Contre la motion parce que concomitance entre une motion fleuve et la
    présentation d'un syndicat au mandat du secrétariat confédéral. On ne
    veut pas qu'il y ait une orientation politique au bureau confédéral.
    sinon, c'est un parti politique.


**STE93**
    Sur la forme, contre, la longueur des motions, c'est fatiguant, on ne
    peut pas avoir ce type de fonctionnement, si on veut être un syndicat
    de masse. Les arguments de fond sont cachés. la formation est importante,
    les grèves nationales, etc.. Pas grand chose de neuf pour nous et c'est la
    forme qui n'apporte rien.
    Sur le fond, il y a un élément qui apparait: très large autonomie des
    syndicats mais ils le sont déjà.
    Que veut dire cette autonomie ?. Cela laisse passer au milieu des questions
    de fond.

**Sim RP**
    On vote oui. Elle met les points sur les I , elle accompagne un projet
    de :term:`BC`. On a relu le PV de la rencontre du syndicat du nettoyage
    et les représentants de la RP. Frappé de la double lecture. Étant donné
    que le syndicat du nettoyages a 800 adhérent-e-s... ils sont la moitié de
    la confédération.
    Une confusion, deux syndicalismes:

		1. qui prend en compte quotidien,
		2. le coté politique

    En face, une structure rigide, avec une vision du syndicalisme d'il
    y a 30 ans, qui n'est pas applicable aujourd'hui.
    Pour la motion proposée parce que c'est **une motion sans langue de bois**.
    (le quitus rappelle la distribution des prix, alors on ne fait pas une
    critique pour détruire ou pour passer de l'huile)

**Santé Social 69**
    OK elle est fleuve et il y a des redites; Au delà des questions de
    formes, on a voté pour, il y a un esprit, il y a un fond; les interventions
    qui sont contre, disent qu'on répète quelque chose d'évident.
    De l'autre coté, on dit qu'il y a des orientations politiques.
    important: réaffirmation de l'identité syndicale de la C.N.T. Si on veut
    établir un vrai rapport de force, cela passe par l'implantation syndicale
    auprès des travailleurs sur les lieux de travail qui ne sont pas forcément
    anarcho-syndicalistes, mais si on veut être dans le rapport de force,
    c'est par là qu'il faut passer.
    La critique qui dit qu'elle est politique, si c'est pas au congrès
    qu'on parle de cela, alors c'est quand ?

**Santé social RP**
    Demande de raccourcir les interventions. nppv; Ils ne considérent pas
    cela comme une motion. Retour sur l'intervention de l'etpic 30, ils
    sont d'accord que la question de la longueur des motions rend impossible
    à discuter. Les motions longues posent de gros problèmes. Merci de ne
    pas faire de réflexion déplacée sur les profs.

**Sipm rp**
    Aspects intéressants mais **doit avoir sa place dans une brochure**.
    Un ton donneur de leçon.
    Le :term:`BC`, c'est un mandat technique, et là cela donne une motion
    extra politique, cela fait fourre tout global, l'aspect autogestionnaire
    n'est pas évoqué ainsi que la question des permanents.
    Syndicalisme de lutte non mais de transformation sociale. Pour le SIPM,
    quel rapport avec le syndicat du nettoyage ? Pour nous, c'est une question
    éthique, c'est la moindre des choses de faire en sorte qu'il y ait du
    respect pour l'accueil aussi et pour les mandats confédéraux. Il y a
    des choses qui se font qui ne sont pas régulières, il y a eu des syndicats
    qui ne sont pas présents qui ont voté.

**Educ 13**
    Chez nous cette motion n'a pas fait débat, on a voté pour, affirmer
    l'identité, c'est bien aussi. Il nous parait intéressant de clarifier
    les choses. c'est aussi bien d'avoir des orientations générales et
    pas que de la technique. Cela ne nous dérange pas que le BC porte une
    orientation politique, l'important, c'est que les mandatés soient
    contrôlés par l'organisation.

**ESS34**
    Nous sommes ravis d'une motion politique. Important de discuter ensemble.
    On s'excuse de notre amendement, s'il n'est pas compréhensible, on le
    retirera. Je rappelle que l'educ c'est pas que les profs. Notre amendement,
    revendication de rupture, c'est l'aboutissement vers le communisme libertaire ;
    oui c'est une redite, mais c'est l'intérêt des congrès.
    Ajouter à long terme le communisme libertaire, et pas déléguer aux
    partis politiques.

**SII RP**
    Au syndicat de l'informatique, cette motion a été comprise, on l'appuyait,
    on est pour, on est contents qu'apparaisse une motion avec de la politique,
    pas de contrôle ni de mise au carré. La forme on peut discuter, mais
    **sur le fond , on appuie , on est content. C'est un projet**.

**ETPIC 57**
    Soutient la motion pour des raisons opposées au sipm RP. Les lyonnais
    posent de bonnes questions, cela fait du bien d'avoir des rappels et
    d'avoir des motions politiques et des revendications de rupture mais
    on veut définir ce que c'est une revendication de rupture ?
    Le syndicalisme  ne se développe pas dans la défaite, si tu n'obtiens
    rien, les gens ne te rejoignent pas.

    Sur les revendications de rupture, c'est pas "on veut le communisme
    libertaire" c'est obtenir des revendications  qui nous ressemblent,
    les coopératives par exemple . Cela c'est une revendication de rupture.
    Le communisme libertaire c'est l'aboutissement, pas la rupture.
    Un mandat cadre doit être indispensable pour un :term:`B.C`.
    On va maintenant définir des orientations sur les autres motions.

**USI 32**
    **Très content de lire ce texte**, pas de problème sur ce texte. C'est le
    retour du syndicalisme et de la politique en même temps.

**SSEC 59 62**
    Pour parce que, on est d'accord, motion d'orientation, et elle se place
    dans le cadre d'un BC et c'est dans ce cadre là. C'est pour ca qu'on
    a voté pour.

**CUlt SPec RP**
    Des choses à éclaircir, mais pas vu comme une motion cadre. Toutes les
    motions doivent être appliquées. Rien ne nous a choqué, on aurait préféré
    que ce soit un article et nppv.

**SUTE 69-01**
    Il y a des discours contradictoires qui sont tenus : d'un côté on nous
    dit qu'il n'y a rien nouveau et d'un autre que cela pose des problèmes.
    Cette motion reprend des pratiques, des fonctionnements syndicalistes.
    Aujourd'hui une place s'est libérée dans le mouvement syndical et **la
    mobilisation sur les retraites nous a montré qu'on a une place à prendre**.
    On veut le réaffirmer et montrer qu'il il y a un espace sur cela.
    **On a aussi des congrès locaux, c'est l'occasion de sortir la tête
    du guidon et de réfléchir**. C'est ce qu'on a voulu faire avec cette motion.

**Ptt centre**
    **On soutient cette motion, on l'attendait depuis longtemps**, enthousiasmante,
    que les syndicats soient anciens ou nouveaux, il y a à se positionner sur
    une orientation. On ne peut pas faire la critique sur le fait que ce
    soit trop long et reprocher que ce ne soit pas détaillé sur l'autogestion.
    **C'est une motion qui donne envie de se battre avec la C.N.T**.
    C'est une des seules motions qui ne soient pas de gestion boutiquière,
    sous prétexte que cela à déjà été dit. Tous les syndicats qui sont là sont
    des syndicats qui existent. Il n'y pas de syndicat bidon.

**Etpic 94**
    Relier une motion et un mandat dont le quitus sera voté par rapport à cela.
    Répéter c'est étonnant, oui mais sur l'exemple les orientations sur
    les D.S, on est dans une perspective de développement par rapport à cela.
    Les sections syndicales, c'est pas la garantie de l'action syndicale.
    Les sections syndicales n'ont pas toujours été en mesure de se mettre
    en grève. Quelque soit le nombre d'adhérents du syndicat.

**Sipm RP**
    Des camarades qui se donnent une feuille de route, on trouve cela gênant.
    C'est comme une campagne, convaincre, être le meilleur, cela implique
    une compétition. On n'aime pas cet aspect des choses. Même si il a des
    qualités, il se fait dans un contexte particulier. On ne s'impose pas dans
    les entreprises dans les textes mais dans les faits. Si ce texte qui dit
    des évidences, mais qui est flou c'est pas sur cela qu'on va pouvoir
    s'assoir , c'est pas bien, c'est flou et quand on voit l'histoire il y a
    des semaines, des mois avant, c'est gênant.

**Educ 38**
    **On appuie cette motion**, les risques de déviances ce serait de donner
    des mandats à des camarades mal définis. Le fait de décider d'orientation
    au travers des motions et au travers de celle ci ne nous choque pas.
    Elle devra être prise en compte de toute façon si elle est votée.

**Ste 92**
    Problème d'organisation, notre contre-motion n'a pas été prise en compte.
    Le BC a fait son mea culpa mais elle a été oubliée de nouveau dans le
    cahier du mandaté, est ce que les syndicats se sont positionnés et
    l'ont ils vus ? Ce n'est pas pour s'opposer à la motion d'origine, c'était
    pour attirer l'attention sur d'autres points.
    Relecture de la contre-motions proposée.

**nettoyage RP**
    Je représente le nettoyage rp. Je suis peut être un peu hors sujet mais
    cela me fait plaisir de voir les gens se chamailler. J'entends quelques
    mots sur le nettoyage, si les gens veulent en savoir plus, on vous attend
    les bras ouverts et bien comprendre la situation et on arrête de se
    chamailler, les "langues de bois" etc... c'est pas intéressant, je
    demande à toute personne qui veut en savoir plus sur le nettoyage, on est
    là et fraternel, on aimerait bien communiquer avec vous, et en
    savoir plus. Merci.

**SUTE 69-01**

    1. la motion pose le débat qui vient d'avoir lieu pour **qu'on puisse
       s'interroger sur le type de syndicalisme qu'on veut**.
       Pour les lacunes soulevées, elles sont a priori intégrées: que ce soit
       surl'autogestion et l'action directe il y a plusieurs paragraphes
       (lu par le sute 69) pour nous, elle est présente dans cette motion.
    2. sur ce à quoi servirait cette motion : c'est une dimension assumée
       pleinement, pour nous ce qui est important, c'est le mandat et non
       les mandatés. **Pour nous, savoir dans quel sens on va diriger notre
       action, on ne veut pas être mandaté sur un chèque en blanc, sur nos
       têtes ou une simple action administrative. La question du mandat
       passe avant les mandatés**.

**SIM RP**
    La nécessité de victoire dans les luttes sociale oui, bien sûr. Et pour
    ce qui est des choses d'il y a 30 ans... en Espagne, il y avait des
    permanents. Intervention du nettoyage appréciée, et sipm aussi il faut
    appliquer cette camaraderie.

**SII RP**
    On n'entend pas de critiques sur le fond... à part de dire que c'est
    une redite, alors **pourquoi ne pas voter pour ?**

**SIPM RP**
    C'est tellement flou qu'on ne pourra pas évaluer, le fait d'écrire
    action directe ou camaraderie, c'est cela qui va changer les choses ???
    Quand on n'ouvre pas ses portes pendant des années, et qu'on dit qu'on
    ouvre ses bras comme au nettoyage quand c'est pas traduit par des faits,
    c'est problématique.

**Ste 75**
    Merci de ne pas redire des choses déjà dites.

**Santé social RP**
    Merci de ne pas faire de pique personnelle.



Vote indicatif orientation syndicale, Education 69-01 [adoptée]
===============================================================


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°56          | 38         | 3        |  24        |  6                        |
| Education 69         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+



**Motion n°56 Adoptée**
