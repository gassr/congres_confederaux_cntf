
.. _amendement_motion_37_2012_interco86:

==========================================
Amendement à la motion 37 (interco86)
==========================================

.. seealso::

   - :ref:`motion_37_2012_chimie_bzh`



Argumentaire
=============

Considérant que la motivation de la motion n°37 est une clarification
stratégique vis a vis de personnes syndiqués au sein de notre confédération
et dans d'autres organisations , elle demande un positionnement strict alors
que même si elle reconnait la possibilité des secteurs avec monopoles
syndicaux (qui resteraient d'ailleurs à définir plus précisément).

Cette motion nous parait incomplète du point de vue internationale.

Pourquoi celle-ci empêcherait des syndicats ou syndiqué-e-s de s'affilier à une
organisation internationale telle les IWW ou autres internationale syndicale
conforme à buts et moyens ?

Si ceux-ci restent avant tout syndiqué-e-s dans notre confédération et à ce
titre ne joueraient pas un double jeu comme les individus décris dans la
motion n°37.

Amendement
===========

À l’exception des secteurs où régneraient encore le monopole syndical à
l’embauche, la CNT n’autorise pas la double affiliation syndicale.

Nul(le) adhérent(e) d’un syndicat CNT ne peut être encarté dans un autre syndicat.

Par ailleurs tout syndiqué-e ou syndicat de la CNT reste libre en plus de son
adhésion confédéral de choisir librement son adhésion internationale.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°37 |            |          |            |                           |
| interco86                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
