
======================
Introduction & Rappels
======================

.. seealso::

   - :ref:`motion_synthese_droit_syndical_2008_lille`


La CNT est une organisation se réclamant de l’anarcho-syndicaliste et
syndicalisme révolutionnaire.

A visée révolutionnaire, elle se donne pour but de poursuivre, par la lutte
de classes et l'action directe, la libération des travailleurs/ses qui ne sera
réalisée que par la transformation totale de la société actuelle.

Elle précise que cette transformation ne s'accomplira que par la suppression du
salariat et du patronat, par la syndicalisation des moyens de production, de
répartition, d'échange et de consommation, et le remplacement de l'Etat par un
organisme issu du syndicalisme lui-même et géré par l'ensemble de la société.»
(:ref:`art.1 des statuts confédéraux <article_premier_statuts_CNT_2010>`)

La CNT est fédéraliste et affirme sur le plan organisationnel : «La Confédération
Nationale du Travail reposant sur le producteur/trice, garantit à celui/celle-ci
la direction de l'organisation des travailleurs/ses. Elle est indépendante de
tout type d'organisation politique, religieuse ou autre. »

Le :ref:`31ème Congrès de 2010 <congres_CNT_2010_saint_etienne>`  affirme que
**le moteur interne de notre fonctionnement  à tous les niveaux doit être la
pratique de la démocratie directe.  Pas de hiérarchie, de centralisation ni
d'activisme militant se substituant aux  décisions des AG syndicales**.

Le syndicat professionnel d'appartenance et ses assemblées générales restant
l'organe de base des décisions.
Notre autonomie d’action s’exprime principalement au travers des principes de
démocratie et d’action directe revendiqués comme pratique syndicale.
La pratique de l’action directe comme mode d’action n’est que le prolongement
des conceptions de démocratie directe.**

Ce même Congrès confirme de même que **collectivement, notre syndicalisme est
une véritable école d’émancipation par la pratique de l’autogestion, de
l’auto-organisation interne.
En partant du principe que l’autogestion sera le moteur de la société future
et que celle-ci n’est pas innée, mais s’apprend, se pratique et se confronte à
la réalité, nous devons la mettre au centre de nos dynamiques et de nos
initiatives. Cela renvoie aussi à l’idée que le mode d’organisation et de
fonctionnement que nous choisissons aujourd’hui, dans la société
dans laquelle nous luttons, doit refléter et préfigurer le modèle de société
que nous voulons. D’où le développement des pratiques d’autogestion dans tous
les domaines, de démocratie et d’action directe.**

Les récentes dispositions législatives (Loi du 20 Août 2008 pour le secteur
privé, décret du 17 Février 2012 pour la fonction publique d’Etat, etc.), les
confrontations juridiques avec les employeurs et l’administration sont venus
successivement interroger l’action militante et syndicale de la CNT au regard
des nouvelles modalités d’accès au droit syndical : recours aux élections
professionnelles, participation au paritarisme, accès aux droits d’expression
syndicaux élémentaires, accès au droit de grève, utilisation des heures de
délégation, d’absence, ou de détachement syndicaux.

Pour partie, :ref:`le 30ème Congrès de 2008 <congres_CNT_2008_lille>` a apporté
une première réponse en la matière par l’adoption de motion intitulée
**Représentativité et stratégie syndicale** (introduite initialement par notre
syndicat et retravaillée par une commission de synthèse)

Par ailleurs, le recours non concerté du syndicat CNT du nettoyage RP au
salariat d’un permanent dit « juridique » a suffisamment heurté la sensibilité
de nombreux camarades pour que cette question, introduite par la motion
circonstancielle n°1 du dernier Congrès, y mobilise un temps considérable au
dépend des autres motions dont près de **50% ne furent pas étudiées**.

Tout en réaffirmant les « bases autogestionnaires » de la CNT, le 31ème Congrès
de 2010 a accordé « temporairement au syndicat du nettoyage RP un fonctionnement
dérogatoire concernant l'emploi d'un salarié » et lui demande alors « de tout
mettre en oeuvre pour que ses tâches soient assurées dans un cadre plus
conforme aux principes de fonctionnement de la  CNT » (Texte adopté sur
proposition d’une commission de synthèse).

La constitution d’une commission confédérale devait permettre d’assurer cette
« transition en trouvant les modalités de la constitution d'une équipe de
mandatés juridiques capable de prendre le relais dans le futur pour le soutien
juridique du syndicat du nettoyage, voire de toute la confédération et en
renforçant son action en matière de formation (juridique, syndicale,
linguistique...). »

Tout en soulignant la densité de leurs besoins et la prégnance de l’action
juridique, le syndicat du nettoyage CNT RP a depuis déclaré souhaiter recourir
à l’avenir à l’embauche de permanent. Au regard de ces affirmations, certains
syndicats, dont le nôtre, se sont désengagés de la commission confédérale
considérant que « l’objet même de la commission est devenu par conséquent
obsolète ».

En terme de rétribution syndicale, les ex-statuts confédéraux de 1946
(modifié 49) précisaient que «les membres du Bureau confédéral et du
«Combat Syndicaliste» appointés ou non ne pourront occuper aucun poste
responsable relevant d’un parti politique, d’une secte philosophique ou
religieuse. » (:ref:`art.7 <article_7_statuts_CNT_1946>`).

Il est précisé que «chaque membre du Bureau appointé n’est rééligible et ne
peut faire acte de candidature pour quelque fonction syndicale rétribuée que
ce soit, avant une période de trois années » (:ref:`art.8 <article_8_statuts_CNT_1946>`)
mais aussi que « le C.C.N. désigne le nombre d’employés (traducteurs, sténos, dactylos, etc.),
nécessaires au bon fonctionnement de la C.N.T., et fixe leurs appointements »
(art.5).

On voit bien ici, que sous le vocable ``rétribué`` ou ``appointé``, nos anciens
statuts confédéraux autorisaient le recours au salariat de permanents à temps
très partiel ou pour des tâches ou mandat bien déterminés à l’échelle
confédérale : Membres du Bureau Confédéral, du Combat Syndicaliste, sténo,
dactylo, traducteurs...

Or, dans le compte-rendu du 7 Avril 2011 à Paris de la commission confédérale
chargée d’étudier la «transition» pour le syndicat CNT nettoyage RP, le
Secrétariat Confédéral déclare « Les statuts [de 1946] prévoient la possibilité
des permanents et la motion de 1996 l’interdiction.

On a deux textes contradictoires. [...] Actuellement, pas d'autorisation ni
d'interdiction. La motion du STP72 votée à Saint-Etienne va dans le même sens,
le débat n’est pas tranché ».

Plus loin, les représentants du syndicat du syndicat du nettoyage CNT RP
confirment que **sur la question du permanent du syndicat, il est rappelé que
cette décision a été prise lorsque la CNT avait les statuts de 46 où le droit
à l'appointement de permanent était accepté. Depuis la confédération a changé
les règles du jeu.**

On le voit bien, les interprétations du Secrétariat Confédéral et du syndicat
du nettoyage CNT RP ne sont pas réellement fidèles aux textes statutaires
de 1946. Bien qu’il n’ait jamais été fait mention de la rétribution de
permanent juridique à l’échelle d’un syndicat, on peut comprendre la tentative
(sur une interprétation très subjective) du syndicat du nettoyage CNT RP de
justifier son positionnement par extrapolation.

Il est plus délicat, voire cavalier, que le Secrétariat Confédéral, seul, au
nom de la CNT, puisse prétendre invalider ouvertement une motion du Congrès
de Lyon de 1996 au seul motif d’une contradiction partielle avec les statuts
de 46...

En effet, sans l’interdire, les statuts modifiés 2008 ne font plus aucune
mention à une quelconque rétribution. Ils ne changent donc pas
« la règle du jeu », comme le prétend le syndicat CNT du nettoyage RP.

Ils laissent néanmoins totalement la liberté aux motions de Congrès de
Fonctionnement ou d’Orientations le soin de l’encadrer.

Au-delà donc de la réelle acquisition des éléments de discussion et de
réflexion par le biais de la commission confédérale, le syndicat du nettoyage
CNT RP prolonge son positionnement et l’UR CNT RP étudie l’élaboration et la
mutualisation d’outils juridiques régionauxinterprofessionnels.

**La position de la CNT quant au recours aux permanents syndicaux et/ou techniques
demeure donc contestée de fait**.

Face aux désastreuses conséquences de la politique du fait accompli, et à
l’absence manifeste de propositions de motions émanant des plus fervents
défenseurs du recours aux permanents rétribués, **notre syndicat vous propose
de refonder démocratiquement nos positions confédérales**.

A notre sens, la motion de stratégie syndicale **Permanents syndicaux**
du 25ème Congrès confédéral de 1996 à Lyon mérite d’être réactualisée,
adaptée, voire renforcée.

Comme trame de fond de ce travail, nous faisons notre cet extrait de la charte
du syndicalisme révolutionnaire :ref:`dite de Paris <chartes_de_paris>`, adoptée
à Paris en 1946 à la  création de la CNT-F: **Le Congrès affirme que le
syndicalisme, expression  naturelle et concrète du mouvement des producteurs,
contient à l'état latent  et organique toutes les activités d'exécution et de
direction capables  d'assurer la vie nouvelle. Il lui appartient donc, dès
maintenant, de rassembler sur un plan uniquement d'organisation toutes les
forces de la  main-d'oeuvre, de la technique et de la science, agissant
séparément, en ordre  dispersé, dans l'industrie et aux champs.**

«N'ayant pour unique ambition que d'être les pionniers hardis d'une
transformation sociale dont les agents d'exécution et de direction oeuvreront
sur le plan du syndicalisme, les syndicalistes désirent que leur mouvement,
vivant reflet des aspirations et des besoins matériels et moraux de l'individu,
devienne la véritable synthèse d'un mécanisme social déjà en voie de
constitution où tous trouveront les conditions organiques, idéalistes et
humaines de la révolution prochaine, désirée par tous les travailleurs. »

«En indiquant que les syndicats constitueront les cadres de la société
nouvelle, le Congrès déclare qu'en ouvrant l'accès du syndicat aux techniciens
et aux savants, ceux-ci s'y trouveront placés sur un pied de complète égalité
avec les autres travailleurs. C'est de la collaboration intelligente et amicale
de tous ces éléments que surgira le véritable Conseil économique du travail,
qui aura pour mission de poursuivre le travail de préparation à la gestion des
moyens de production, d'échange et de répartition et aura à charge, sous la
direction des Congrès, de chercher les moyens les meilleurs pour faire aboutir
les revendications ouvrières.»

C’est donc complémentairement aux motions précitées, que nos propositions
visent à encadrer le recours aux heures syndicales et aux permanents en
fonction des deux axes clés de notre activité militante:

- L’organisation interne
- L’action syndicale directe

Préalablement, il nous apparait nécessaire de procéder à quelques
éclaircissements sémantiques et quelques rappels juridiques et historiques.
