
.. _contre_motion_7_2021_ste72:

==========================================================================================================
Contre-motion à la motion n°7 2021 **Drogues, Alcool et émancipation révolutionnaire** par **STE72**
==========================================================================================================

- :ref:`motion_7_2021_etpics94`

Autres motions STE72

    - :ref:`syndicats:motions_ste72`


Contre-motion
===============

.. raw:: html

    La Confédération prend toute la mesure des conséquences de l’alcool et
    autres drogues sur le mouvement révolutionnaire. Elle se donne les moyens
    en son sein de préserver ses espaces de réflexion et de prises de
    décisions, pour développer et construire l’émancipation, l’organisation
    et ses luttes <span class="modif_rouge">en adoptant un comportement de
    protection des personnes et en les orientant vers des organismes qualifiés.</span>


Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°7 |            |          |            |                           |
| STE72                         |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
