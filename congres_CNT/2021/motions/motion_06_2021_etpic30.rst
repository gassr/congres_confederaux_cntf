.. index::
   pair: Motion 6 ; 2021

.. _motion_6_2021_etpic30:

================================================================================================================================================
Motion **n°6** 2021: **De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale** par **ETPIC 30**
================================================================================================================================================


Autres motions ETPIC30

    - :ref:`motions_etpic30`


Préambule
==========

La question écologique imprègne toute la société **tant les enjeux
environnementaux deviennent préoccupants**.

Il nous est apparu essentiel que la CNT adopte des orientations d’ensemble
et **puisse pleinement intégrer la question écologiste à ses luttes**.

Les nouvelles générations sont et seront autant concernées qu’impliquées dans
la lutte écologiste.
Dans la mesure où cette lutte nous semble indissociable de l’anticapitalisme
et d’un projet révolutionnaire global, nous proposons l’adoption du texte
ci-après.

Motion De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale
====================================================================================================

Destruction de la biodiversité, menaces nucléaires, pollutions chimiques et
technologiques, dérèglement climatique et ses conséquences, recours aux OGM,
brevetage du vivant, surproduction & surconsommation, marchandisation, exploitation.

**Face au désastre environnemental, la lutte est salutaire**.

L’avenir s’assombrit et la prise de conscience en faveur d’une écologie radicale
et sociale est plus que jamais nécessaire.

Le productivisme capitaliste gaspille, épuise, et détruit les ressources de la
nature. La Terre devient une immense poubelle dont certains déchets, comme ceux
du nucléaire, pollueront pour des millions d’années.

Nous entendons bien reprendre le contrôle de nos vies, libérer l’humanité et la
planète des contraintes que le système capitaliste et ses moyens de répressions
font peser sur notre désir d’une vie plus juste, plus libre, plus respectueuse
du vivre ensemble et de l’environnement.

Stopper le capitalisme ravageur, autodestructeur
==================================================

L’impact de la logique capitaliste sur l’environnement naturel et sur
la vie des êtres humains est catastrophique.
L’exploitation capitaliste, et son avatar néo-libéral actuel, ont pour
seule logique la recherche du maximum de profits.

**Cette logique ne peut conduire qu'à la destruction de toutes formes de vie.**

Notre destin n’est pas d’augmenter sans cesse la production et la consommation,
de dilapider de façon irréversible les ressources du milieu.
Nous n’avons aucun espoir dans le développement durable et la croissance
dite *verte*.
Ce ne sont que des pis-aller dans lesquels le confort des un·e·s ici, suppose
souvent des désastres environnementaux pour les autres là-bas.

Soumis à la pression constante de la société marchande, nous n’attendons
rien des Etats et de leurs grands sommets.

Il nous semble illusoire d'espérer que les entreprises diminuent leur impact
écologique. Elles ne voient dans l'écologie qu'un nouveau champ d’investissement
pour accroître leurs profits.

Pour les militant·e·s de la CNT, anarcho-syndicalistes et écologistes,
la seule voie pour sortir de la crise climatique est un renversement
complet des rapports de productions et sociaux.

Pour une décroissance assumée, absolue, choisie
==================================================

**L’économie est un moyen pas une fin**.
Nous la voulons à notre service et pas l’inverse.

**Nous devons prendre le contrôle des moyens de production** (cf. :ref:`motion du 30ème Congrès <motion_economie_cnt_congres_lille_2008>`).

Nous soutenons que le nombre de productions inutiles doit décroître parce
qu’elles sont polluantes et nuisibles.

Nous sommes radicalement critiques du rapport d’exploitation utilitaire
que la modernité essentiellement technologique entretient avec la nature.

Celle lucidité ne peut être déléguée aux seul·e·s spécialistes,
expert·e·s, et défenseur·se·s d’un *capitalisme propre*.

Nous voulons pouvoir choisir de produire par le débat démocratique pour
que le choix et l’intérêt collectif l’emportent sur l’initiative et les
intérêts privés (patrons, états, armées, technosciences...).

La CNT défend le projet d’une société autogestionnaire, antiautoritaire,
solidaire. Libertaire.  Une société débarrassée des logiques d’exploitation,
des intérêts nationaux, et des profits mortifères du capitalisme.

Une société véritablement démocratique centrée sur une croissance maîtrisée
d’un grand nombre d’activités favorisant une vie meilleure : éducation, santé,
transports collectifs, productions vivrières relocalisées et non polluantes,
habitat décent...

Nous ne pouvons laisser le terrain de la question écologiste au **green washing**
capitaliste et aux incantations électorales des carriéristes politiques.

Pour la CNT, l’écologisme ne peut être dissocié d’un projet alternatif de
société, d’une volonté de transformation sociale et radicale, vers une reprise
en main globale de la production et de la consommation.

La CNT doit pouvoir intégrer pleinement à ses luttes, à ses combats, les
questions écologistes évoquées ci-avant et contribuer de ses forces aux
mouvements de révoltes et d’alternatives écologistes.

Elle y apporte un angle de vue anticapitaliste, collectiviste, syndicaliste,
révolutionnaire, et internationaliste.

Elle produit un matériel de propagande visant à ancrer publiquement ce message
radicalement écologiste propre à notre approche.

Amendements
===========

- :ref:`amendement_motion_6_2021_sanso69`
- :ref:`amendement_motion_6_2021_cnt42`
- :ref:`amendement_motion_6_2021_interpro31`
- :ref:`amendement_motion_6_2021_ste72`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°6 2021 ETPIC30     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
