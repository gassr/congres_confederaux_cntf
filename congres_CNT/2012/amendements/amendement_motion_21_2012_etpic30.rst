
.. _amendement_motion_21_2012_etpic30:

==========================================
Amendement à la motion 21 (ETPIC30)
==========================================

.. seealso::

   - :ref:`motion_21_2012_stea`



Argumentaire
=============

Nous soutenons la création d’une commission de travail centrée sur l’émergence
de revendications et de matériel propres aux revendications liées au 6h de
travail par jour.

Mais notre syndicat considère que le Bureau confédéral et le Secrétariat
international n’ont pas pour vocation de contribuer systématiquement aux
travaux de cette commission de travail confédérale.

Amendement
===========

Les mandatés confédéraux du BC et du SI relaieront naturellement les décisions
prises par le CCN (suite aux propositions de la commission de travail
confédérale).

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°21 |            |          |            |                           |
| ETPIC30                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
