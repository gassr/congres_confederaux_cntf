
.. _contre_motion_1_2014_interco25:

===================================================
Contre motion à la motion n°1 2014 (Interco25)
===================================================

.. seealso::

   - :ref:`motion_1_2014_sinr44`





Argumentaire
=============

L'autogestion est à la base de nos décisions et le syndicat est le lieu où se
met en place cette autogestion, par la discussion et le débat sur nos rêgles
et nos orientations, et par le mandatement de personnes qui portent la voix
des décisions qui y ont été prise.

Ces discussions et ces débats doivent être menés à partir de rêgles connues à
l'avance, et non en fonction de rêgles qui changeront éventuellement en cours
de congrès.

Contre-motion
==============

Sauf mention explicite, rendue nécessaire par l'urgence de la décision, les
motions de fonctionnement de congrès confédéral, placée en début de cahier,
ne rentrent en application qu'au congrès suivant.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°1 2014 <motion_1_2014_sinr44>`

       :ref:`Interco25 <interco25>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
