
.. _motion_37_2012:
.. _motion_37_2012_chimie_bzh:

=================================================================================
Motion n°37 2012 : Double affiliation syndicale ou « double carte » (Chimie Bzh)
=================================================================================
  

.. seealso::

   - :ref:`chimie_bzh`
   - :ref:`motions_chimie_bzh_2012`
   - :ref:`motion_34_2012`



Motions de 2010 concernant la double carte
==========================================

- :ref:`motion_32_2010`
- :ref:`motion_33_2010`

Argumentaire
============


Une motion du 30e Congrès (et non discuté au 31e) a été mal comprise par de
nombreux syndicats parce que sa mise au vote présentait des choix multiples :
«La CNT a toléré depuis une vingtaine d’année suite à une décision de congrès
et jusqu'à présent l’usage de la double affiliation syndicale dans les secteurs
du Livre et des Docks en raison du monopole syndical et du monopole d’embauche
de la CGT dans ces deux secteurs.

Aujourd’hui des camarades travaillants en dehors de ces deux secteurs se
trouvent dans des situations où la menace de répression et de licenciement les
poussent à se syndiquer dans un autre syndicat en plus d’être syndiqué à la CNT.

Mais d’autres camarades travaillants dans la même entreprise ou le même secteur
et uniquement syndiqués CNT, peuvent subir l‘agression de membres d’autres
syndicats, dont les doubles encartés sont aussi membres : il y a un conflit
dés lors qu’il s’agit d’affirmer une position au sein de la CNT dés lors que
certain(e)s sont cénétistes et d’autres « double encartés », surtout dans
la stratégie syndicale.

Cette motion a pour but de clarifier la position de la CNT vis-à-vis de la
double carte, les secteurs industriels où elle serait tolérée et qu’un débat
sur la place et l’identité de la CNT vis à vis des autres centrales ait lieu
dans chaque syndicat. »

La CNT n’est pas une tendance syndicale oeuvrant dans les autres syndicats.

Il y a de fait conflit d’intérêt à être double encarté. De plus, cela reflète
une attitude consumériste et opportuniste à l’antipode de nos principes de
fédéralisme et d’autogestion.

Motion
======

À l’exception des secteurs où règneraient encore le monopole syndical à
l’embauche, la CNT n’autorise pas la double affiliation syndicale.

Nul(le) adhérent(e) d’un syndicat CNT ne peut être encarté dans un autre
syndicat.


Vote
=====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°37          |            |          |            |                           |
| Chime-Bzh            |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Amendements
===========

- :ref:`amendement_motion_37_2012_interco86`
