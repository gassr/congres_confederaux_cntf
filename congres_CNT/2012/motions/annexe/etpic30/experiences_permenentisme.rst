

.. index::
   pair: Expériences; Permanentisme


============================================================================================================
Expériences du permanentisme anarcho-syndicaliste et syndicaliste révolutionnaire international & historique
============================================================================================================




Nombre d'entités anarcho-syndicalistes et syndicalistes révolutionnaires ont
souscrit au recours aux permanents dans le passé comme dans le présent.

I - Historiquement
===================

En Espagne, la "glorieuse et immortelle CNT" (expression véridique) dans les
années trente, forte de son million d'adhérent.e.s, rétribuait une bonne
dizaine de militant.e.s dans diverses tâches administratives (secrétariat,
imprimerie) voire "politiques" notamment dans le cas du rédacteur en chef du
journal confédéral.

En France, les pères fondateurs du syndicalisme d'action directe (Pelloutier,
Griffuelhes, Pouget) étaient tous payés pour assumer leurs mandats, tout
comme les secrétaires des Bourses du travail et administratifs de la CGT
d'avant 1914.

En France toujours, dans les années 30, la CGT-SR (bien que numériquement
faible de quelques milliers d'adhérent.e.s) rétribuait les services de son
secrétaire confédéral (Pierre Besnard entre autre) et des rédacteurs du
Combat Syndicaliste (Alexandre Schapiro, Aristide Lapeyre).
Il n'est pas neutre, à cet égard, qu'à sa fondation la CNT, en 1946, ait
inscrit dans ses statuts la possibilité de rémunérer des adhèrent.e.s.

La CNT française de 1946 s'appuyait, en effet, sur l'expérience et les acquis
en terme de fonctionnement de la CGT-SR de l'entre deux guerres.


I – A l’international
=====================

L'USI italienne (1000 à 2000 membres) s'est posée la question de la rémunération
d'un ou de plusieurs militant.e.s à de tâches de coordination (secrétariat
confédéral entre autre). La question a été ponctuellement mise entre parenthèse
du fait de l'extrême disponibilité de certain.e.s retraité.e.s de l'organisation.

Cette solution n'est cependant que transitoire d'autant que l'USI a une
certaine difficulté à recruter des militant.e.s plus jeunes et ainsi créer
en son sein les bases d’un renouvellement générationnel.

Les IWW américains (1000 membres) salarient, mensuellement à partir des
cotisations des adhérent.e.s, leur secrétaire confédéral afin d'avoir la
disponibilité de parcourir régulièrement l’immense territoire étasunien et
résoudre les questions administratives de l'organisation.

La SAC suédoise (forte de 9000 membres il y a encore une dizaine d'années et
de quelques 5000 adhérent.e.s environ aujourd'hui) a, depuis les années 50,
toujours rétribué pas moins d'une vingtaine de militant.e.s. Le fait d'une
crise interne qui ronge les anarcho-syndicalistes suédois et du fait surtout
de la fin du financement de la SAC par l'intermédiaire de sa gestion des
caisses d'allocation chômage (dans le cadre de la fin du fameux "modèle
suédois") ont obligé la SAC à revoir son train de vie.

Elle a ces dernières années, de ce fait, réduit son nombre de permanents.
Cela pose d'ailleurs un autre type de questions (auquel nous devrons nous
aussi réfléchir), certain.e.s permanent.e.s mécontent.e.s d'être
licencié.e.s attaquant aujourd'hui devant les Tribunaux leurs anciens
employeurs, en l'occurrence la SAC et ses adhèrent.e.s.

Dernier exemple notoire, celui de nos camarades de la CGT espagnole. Nul ne
peut contester qu'il s'agisse (malgré ses contradictions et imperfections),
aujourd'hui en 2012, du seul cas d'organisation se réclamant de notre courant
idéologique ayant atteint un tel seuil de développement (70 à 90 000 membres).

Dans l'Espagne en crise depuis ces trois dernières années ce développement
devrait d'ailleurs se poursuivre sur la base entre autre de la poursuite du
basculement de sections entières issues de l'UGT et surtout des CCOO, les
syndicats majoritaires et institutionnels.

Depuis sa séparation en 1979 avec la CNT espagnole, la CGT-E (qui s'appellera
comme telle en 1984) a toujours eu en ses rangs des militant.e.s payé.e.s
(par l'organisation ou par l'Etat et les patrons dans le cadre de leur
militantisme en entreprise du fait du cumul de leurs heures de Délégués syndicaux).

Dans la Fonction publique, et en particulier dans l’enseignement, ce qui a
poussé la Fédération Education de la CGT-E à accepter ces décharges syndicales
rémunérées s’explique par le fait qu’à la différence de la France, il n’existe
pas en Espagne d’ASA (Autorisation Syndicale Administrative). Ces ASA qui pour
l’heure permettent, ici en France, à nos militant.e.s de la CNT FTE d’avoir la
possibilité de participer à des réunions, Congrès, CAF.

Si nos camarades espagnol.e.s de la CGT continuent à se remettre en cause
régulièrement, Congrès après Congrès, sur le moyen de mieux contrôler et
procéder à une meilleure rotation de tâches et des mandats) personne en son
sein aujourd'hui, au vu de l'énormité du travail à fournir en interne
(fonctionnement, disponibilité, travail juridique, mandat international,
publications), ne remet en cause la nécessité absolue de recourir au salariat
de ces militant.e.s.

La question principale, néanmoins, qui se pose à la CGT-E réside dans le
constat que les mandats rétribués par le patron ou l'Etat ne sortent pas
d'une certaine catégorie de militant.e.s qui ont la possibilité d'être
"liberé.e.s" par leur entreprise : en l'occurrence le Fonctionnaires d'Etat et
les ouvriers en CDI des grandes entreprises (SEAT, RENFE, FORD).

Pour aller vite nous pourrions parler à leur propos d'une certaine forme
d'"aristocratie ouvrière". Cette contradiction n'est pas sans sens au point
de vue de l'éthique (et les camarades espagnol.e.s se la posent dans ce termes):
un ouvrier d'une petite entreprise, un travailleur précaire (pléonasme dans
l'Espagne d'aujourd'hui) ne pourront jamais accéder à un mandat CGT rémunéré
par leur entreprise. Cette contradiction n'est pas imputable en soi à la
CGT-E, elle l'est à la société espagnole et sa réalité des droits syndicaux.
