
.. _amendement_motion_35_2010_ETPRECI35:


=====================================
Amendement à la motion 35 (ETPRECI35)
=====================================

Argumentaire : Une étude financière est nécessaire, validée par le BC ou en CCN.
De plus, nous souhaitons que le Congrès ait plus d’informations sur cette
association que nous ne connaissons pas.

**Amendement**: Remplacement de «l’association Culture et Liberté» par «une association ou organisme»


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°35 |            |          |            |                           |
| ETPRECI35                   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_35_2010`
