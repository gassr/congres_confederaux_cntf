
.. _motion_39_2012:
.. _motion_39_2012_etpic30:

============================================================================================================================================
Motion n°39 2012 : Cohésion de la CNT / Labellisation confédérale d'un syndicat (ou réactivation d’un syndicat CNT en sommeil) ETPIC 30 Sud
============================================================================================================================================
  

.. seealso::

   - :ref:`motions_etpic30`



Motion reprise de ETPIC30, 2010
===============================

- :ref:`motion_4_2010`

Introduction
============

Cette motion reprend en substance la procédure de labellisation adoptée à
l'occasion du CCN des 5 & 6 décembre 2009 à Rennes par une large majorité
des UR CNT.

Faute de temps, le Congrès confédéral de 2010 n’avait pas pu en discuter
sa première version.

Nous la re-présentons amendée des contre-motions de l’ESS 34, de l’amendement
l’ETPRECI 35 et de la motion (n°60) du SUB TP-BAM RP présentées au Congrès
confédéral 2010.

Son adoption par le Congrès viendrait remplacer la motion « Label confédéral »
adoptée au 28e Congrès confédéral de 2004 et la procédure administrative
adoptée au CCN de Décembre 2011 ? (contestée et incomplète).


Argumentaire
============

La labellisation constitue, par essence, l’acte d’intégration et d’inclusion
d’un syndicat au sein du pacte statutaire de la CNT.

Dans le respect des orientations, du fonctionnement, des statuts et des
règles organiques, elle confère à tout nouveau syndicat la faculté:

- De participer à la vie interne et démocratique de la CNT ;
- De mener son action syndicale sous couvert du label « CNT ».

Les expériences récentes de conduite à la labellisation sous la responsabilité
des derniers Bureau confédéral, nous amènent à constater de sérieuses et
dommageables approximations autour de cette procédure d’accueil : labellisation
de syndicats non déclarés, labellisation de syndicats n’ayant versés aucune
cotisation (assorti d’une absence de consultation préalable de la Trésorerie
confédérale), chevauchement géographique des champs de syndicalisation avec
des syndicats existants, etc.

Nous nous trouvons devant la nécessité de traduire une procédure unique et
transparente en vue de la labellisation d'un syndicat, la motion de 2004 se
montrant particulièrementincomplète.

Si « les syndicats doivent déposer des statuts en cohérence avec les statuts
confédéraux » (article 2 des statuts), nous considérons qu’il appartient bien
au BC de réceptionner dans un premier temps cette demande de labellisation
et de procéder à une première étude en vue d’une validation la plus rapide
possible, considérant que « le B.C. doit veiller, en toute circonstance, au
respect des statuts et des décisions de congrès et de CCN » (article 7 des statuts).

Une attention particulière est nécessaire quant aux buts, au caractère syndical,
à l'adhésion aux buts de la CNT, au(x) champ(s) de syndicalisation
géographique et d'industrie du syndicat.

Réclamer un Procès-verbal de dépôt de statuts et/ou de bureau en mairie
pourquoi ? Parce que le syndicat, selon nous, doit avoir une existence légale
avant tout car c'est indispensable à la création de section syndicale ou même
pour pouvoir prétendre à acter en justice.

Il est donc de même nécessaire de produire le nouveau PV de bureau pour
réactivation de vieux syndicats inactifs.

C’est une procédure qui peut avoir l'air fastidieuse mais qui peut ne prendre
que deux semaines au total.

Ce processus a aussi un rôle d'accueil. Il permet un temps d'accompagnement et
d'explication sur ce qu'est l'organisation de la CNT.

La « lettre de bienvenue » aux nouveaux syndicats doit en être l'illustration
finale. Aussi, fort de l'expérience de notre syndicat au cœur du Bureau
confédéral et de la Commission administrative de 2008 à 2010, nous proposons
la motion suivante :

Motion : Label confédéral
=========================

La labellisation constitue, par essence, l’acte d’intégration et d’inclusion
d’un syndicat au sein du pacte statutaire de la CNT.

Dans le respect des orientations, du fonctionnement, des statuts et des règles
organiques, elle confère à tout nouveau syndicat la faculté :

- De participer à la vie interne et démocratique de la CNT ;
- De mener son action syndicale sous couvert du label « CNT ».

La procédure s’inscrit en deux phases distinctes et consécutives :

Phase 1. Une phase consultation interne d’accueil, d’accompagnement administratif, et de consultation interne
--------------------------------------------------------------------------------------------------------------

Dans l’ordre le plus à même de favoriser une labellisation rapide, le Bureau
confédéral (Secrétariat et Trésorerie confédéral) est en charge d’organiser
avec bienveillance et compréhension l’intégralité de la procédure d’accueil
et de labellisation suivante :

Le syndicat réclamant le label prend contact avec le Bureau confédéral de la
CNT. Le BC assure le contact avec les militantEs souhaitant constituer un
syndicat CNT en termes de :

a. Apport d’information sur l’identité de la CNT (si demandé) ;
b. Explication de la démarche de création et transmission de la procédure
   complète, à organiser en lien avec l’UR de référence si elle existe ;
c. Information et échange sur l’existence de syndicats sur la même aire
   géographique ou à proximité ;
d. Vérification de l’existence de syndicat CNT déclarés en sommeil et
   information ;
e. Information et échange sur la nécessité d’apparaitre sur un champ de
   syndicalisation « professionnel » et un champ géographique de syndicalisation
   inexistant, ou renvoi sur les syndicats existants ;
f. Apports de conseils sur la définition du champ de syndicalisation ;
g. Pour les syndicats inter-corporatifs, il est requis une inscription du champ
   géographique de syndicalisation à minima sur un ou des bassins d’emploi /
   économique reconnu (commerce et industrie par exemple, STICS, ETPIC,
   ETPRECI, etc) ;
h. Transmission de statuts type (si demandés).

3 Le BC informe simultanément le Bureau régional de l’UR CNT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Le BC informe simultanément le Bureau régional de l’UR CNT  concernée et le
Bureau de la/des Fédérations d’industrie de rattachement, quand elles existent,
de la demande de labellisation.

4 Réclamation au « syndicat demandeur » des documents nécessaires
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Réclamation au « syndicat demandeur » des documents nécessaires à la
validation par le BC :

a. Projet de statuts à déposer > validation ou rejet par le BC (en lien avec
   l’UR et la Fédération concernées) en fonction de la conformité avec les
   statuts et les orientations de la CNT et des champs de syndicalisation
   (géographique et « professionnel »). Suivi et avis motivé des rejets.
   Une souplesse d'adaptabilité sera observée pour les syndicats CNT réactivés
   si inactivité manifeste depuis plusieurs années ou dé-labellisation antérieure ;
b. Le récépissé de dépôt en Mairie faisant apparaitre le numéro de déclaration
   pour : Procès-verbal d'AG (désignation nouveau bureau ou renouvellement du
   bureau), dépôt des nouveaux statuts ;
c. Les coordonnées des membres du bureau (non obligatoire mais utile pour des
   contacts directs) ;

5. Réception des premières cotisations confédérales par la Trésorerie
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Réception des premières cotisations confédérales par la Trésorerie confédérale
du « syndicat demandeur ».


Phase 2 - Une phase de validation et de labellisation
-----------------------------------------------------

Une fois les précédentes étapes de phase 1 intégralement réalisées et validées,
la validation de la labellisation se poursuit dans l’ordre suivant :

1. Sollicitation de l'accord et/ou de l’avis de l’UR et de la/des Fédérations
   d’industrie concernées (si elles existent).
2. Labellisation par le BC : Dans un délai de 2 mois maximum, information au
   syndicat et simultanément à son UR et parution à la circulaire confédérale
   suivante (+ liste web d'information aux syndicats).

- Lorsqu’il y a opposition d'une Union régionale ou d'une Fédération
  d'industrie, le BC doit suspendre sa décision, qui doit être soumise au
  prochain CCN ou congrès confédéral.
- Les « syndicats demandeurs », conservent le droit de présenter directement
  ou par courrier un recours sur un temps dédié et limité à l’ouverture des
  débats au CCN le plus proche ou par défaut au congrès confédéral.

3. Information du syndicat demandeur par le BC de sa labellisation et
   information sur la mise à disposition de l’ensemble des outils confédéraux :
   circulaires confédérales, recueil des motions en vigueur,
   Livret « Fonctionnement & orientations », aide du secteur propagande,
   outils web, accès aux colonnes du CS, etc.
4. Inscription à l’ordre du jour du CCN ou du congrès confédéral suivant en
   vue de la labellisation définitive.
5. Parution de la labellisation définitive à la circulaire confédérale suivante
   via le Compte-rendu du CCN ou du congrès confédéral.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°39          |            |          |            |                           |
| ETPIC30              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
