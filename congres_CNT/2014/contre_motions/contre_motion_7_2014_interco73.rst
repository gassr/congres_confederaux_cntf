
.. _contre_motion_7_2014_interco73:

================================================
Contre-motion à la motion n°7 2014  (Interco 73)
================================================

.. seealso::

   - :ref:`motion_7_2014_etpic30`






Argumentaire
=============

Le Congrès Confédéral de la CNT examine individuellement les candidatures aux
différents mandats confédéraux.

Les candidatures présentées ou annoncées de façon collective ne sont pas
acceptées.

Chaque candidatE est invitéE à présenter directement, ou par le biais de
syndicat d’appartenance,les motivations de candidature en terme de perspective
d’investissement et de travail en commun.

Bien que le congrès confédéral souhaite un meilleur ancrage territorial et une
multiplicité d’approches, il n’a pas à privilégier le mandatement de personnes
émanant de différents territoires et de différents syndicats.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à :ref:`la motion N°7 2014 <motion_7_2014_etpic30>`

       :ref:`Interco73 <interco73>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
