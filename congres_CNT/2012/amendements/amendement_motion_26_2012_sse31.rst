

.. _amendement_motion_26_2012_sse31:

==========================================
Amendement à la motion 26 (SSE31)
==========================================


.. seealso::

   - :ref:`motion_26_2012_chimie_bzh`



Argumentaire
=============

Proposition d’ajout à la motion:

«Afin de se positionner au mieux sur les questions anti-nucléaire et
anti-productiviste, il nous semble essentiel de mettre en avant l’idée de la
décroissance énergétique dans notre argumentaire»

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°2  |            |          |            |                           |
| EPICS57                     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
