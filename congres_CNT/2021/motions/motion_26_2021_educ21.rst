.. index::
   pair: Motion 26 ; 2021

.. _motion_26_2021_educ21:

=================================================================
Motion **n°26** 2021 **Intranet confédéral** par **Educ 21**
=================================================================

- :ref:`syndicats:ref_motions_educ21`
- :ref:`outils_motion_11_2021_epic30`

Autres motions Educ21

    - :ref:`syndicats:ref_motions_educ21`


|Nextcloud|


Argumentaire
===============

Pour des raisons à la fois **techniques et de fonctionnement**, nous proposons
une modification des inscriptions aux structures sur l'outil intranet.

Nous en avons profité pour synthétiser les motions existantes.

Cette motion remplace les motions relatives à l'intranet adoptées lors
des congrès depuis 2006:

- 29e Congrès confédéral des 2, 3 et 4 juin 2006 à Agen - :ref:`Mise en oeuvre de l'intranet confédéral <mise_en_oeuvre_intranet_2006>`,
- 29e Congrès confédéral des 2, 3 et 4 juin 2006 à Agen - :ref:`Commission permanente intranet <commission_permanente_intranet_2006>`,
- 29e Congrès confédéral des 2, 3 et 4 juin 2006 à Agen - :ref:`Budget intranet <budget_intranet_2006>`,
- 29e Congrès confédéral des 2, 3 et 4 juin 2006 à Agen - :ref:`Intranet: Gestion autonome des régions et gestion des accès des syndicats et autres structures <gestion_autonome_intranet_2006>`,
- 29e Congrès confédéral des 2, 3 et 4 juin 2006 à Agen - :ref:`Gestion éditoriale des services de niveau confédéral <gestion_editoriale_intranet_2006>`,
- 33e Congrès confédéral des 12, 13 et 14 décembre 2014 à Angers - :ref:`Refonte de l'intranet confédéral <refonte_intranet_2014>`
- 33e Congrès confédéral des 12, 13 et 14 décembre 2014 à Angers - :ref:`Intranet: mandat du modérateur du forum <moderer_forum_2014>`

Outre quelques synthèses et reformulations plus contemporaines, le
changement principal concerne le paragraphe 1b:

**Le principal changement de fonctionnement est la gestion des structures**.

Précédemment, chaque entité nécessitait la création d’un compte dédié
avec des identifiants propres confiés aux personnes mandatées à sa gestion,
ce qui pouvait nécessiter pour un·e cénétiste de gérer plusieurs identifiants
et jongler entre les comptes.

**La nouvelle infrastructure ne nécessite qu’un identifiant par cénétiste**.

La participation aux structures se fait par inscription de son compte à
des **cercles**.
Chaque **cercle** est administré (inscription/désinscription) par une personne
mandatée par la structure concernée.

D'un point de vue technique: le compte utilisateur de cette personne
reçoit le **rôle d'administration du cercle**.


Motion
=======

La CNT se dote d'un ensemble de services en ligne accessibles uniquement
aux adhérent·e·s des syndicats de la CNT, l'intranet confédéral.

**1. Accès**
--------------

Les cénétistes accèdent à l'intranet en tant qu'adhérent·e·s de leurs
syndicats.
Chaque syndicat peut gérer des ressources réservées à ses adhérent·e·s.

Les autres structures de la Confédération (unions locales et régionales,
fédérations, le :term:`BC`, la :term:`CA`, les commissions, les :term:`TM`,
le :term:`CS`, etc.) peuvent également gérer des ressources propres.

a) La gestion des accès des cénétistes est effectuée dans les syndicats
   auxquels ils et elles sont affilié·e·s.

   Ainsi, chaque syndicat adhérant à la CNT qui le souhaite se dote d'au
   moins un·e mandaté·e qui:

   - reçoit un accès au nom de tout le syndicat;
   - gère et tient à jour les accès à l'intranet confédéral des adhérent·e·s
     de ce syndicat;
   - fournit l'assistance technique nécessaire aux adhérent·e·s de son
     syndicat;
   - gère les services réseaux associés à ce syndicat.

b) Chaque structure (syndicat, union, fédération, bureau, commission,
   groupe de travail, etc.) est un **cercle privé** avec une personne mandatée
   à l'administration (inscription/désinscription de membres).

   Chaque syndicat désigne un·e mandaté·e chargé·e de créer un nouvel
   accès à la plateforme pour ses adhérent·e·s et de désactiver les comptes
   des ancien·ne·s adhérent·e·s.

   Contrairement aux syndicats, les autres structures ne peuvent pas
   créer de nouvel accès à la plateforme.
   Chacune de ces structures désigne en son sein un·e mandaté·e qui gère
   l'adoption des adhérent·e·s.

   Tout·e adhérent·e habilité·e à rejoindre cette structure peut en faire
   directement la demande auprès de cette personne mandatée, afin d’avoir
   accès aux ressources sur la plateforme de cette structure pour son
   activité.

**2. Services**
-----------------

L'intranet confédéral offre des outils de travail collaboratif:

- échanges écrits (p. ex. listes de discussion),
- audio/visio-conférence
- ainsi que divers services pour la rédaction, le partage et le stockage
  de documents électroniques.

D'autres services peuvent être mis en place, mais en aucun cas l'intranet
confédéral ne peut fournir des services allant à l'encontre des principes
de fonctionnement de la Confédération ou mettant celle-ci dans l'illégalité.

**3. Autonomie et confidentialité**

L'intranet confédéral est mis en œuvre de manière autonome par la Confédération.

Les informations qui s'y échangent ne doivent pas être communiquées à
l'extérieur.

L'infrastructure physique mise en place pour accueillir l'intranet est
choisie de manière à minimiser les risques de vol ou de saisie.

**4. Commission intranet**
-----------------------------

Il s'agit d'une commission de personnes mandatées chargée de la création
et de la gestion des accès à l’intranet confédéral, de l'assistance technique,
de la maintenance, des relations avec l’hébergeur et de la modération des
espaces confédéraux.

La commission intranet fait elle-même les choix techniques pour mettre
en œuvre l'intranet confédéral, en accord avec les principes adoptés par
la Confédération et le budget qui lui est alloué.

Les services réseau sont installés sur un serveur administré uniquement
par des membres de la commission intranet et mandatés par celle-ci.

La commission en assure le fonctionnement.

Elle met en oeuvre une assistance technique et des formations auprès des
syndicats, UL, UR, fédérations et autres structures de la Confédération.

Elle reçoit de la Confédération les moyens (pécuniaires et matériels)
nécessaires au fonctionnement de l'intranet et à la formation.

**5. Budget intranet**
------------------------

La Confédération alloue à l'intranet confédéral un budget de frais de
fonctionnement de 100 euros par mois (serveur exclusivement dédié à la CNT,
hébergé par Globenet à prix coûtant).

**6 - Gestion éditoriale des services de niveau confédéral**

La commission intranet choisit en son sein un·e mandaté·e à la gestion
du contenu sur l'intranet au niveau confédéral, qui peut intervenir dans
la bonne tenue des débats:

- solliciter un ton plus fraternel dans les échanges ayant cours sur le
  forum;
- adresser des avertissements aux personnes ne respectant pas les règles
  de principes d’utilisation et d’en prévenir leur syndicat et les
  mandaté·e·s intranet de ces derniers;
- de prononcer la radiation des personnes ne respectant pas les règles
  de principes d’utilisation du forum et d’en prévenir leur syndicat et
  les mandaté·e·s intranet de ces derniers.

Les motifs d’avertissement ou de radiation des accès individuels au forum
sont les suivants:

- insultes ou diffamations de personnes adhérentes à la CNT, ayant accès
  ou non au forum;
- propos ouvertement discriminatoires;
- atteintes graves à l’intégrité morale ou physique;
- envoi disproportionné de messages dans le but manifeste de bloquer
  l’utilisation usuelle du forum;
- exportation et diffusion publique de messages d’expression individuelle
  du forum vers l’extérieur.

Le modérateur privilégie la recherche de consensus avec la/les personne·s
concerné·e·s en vue d’obtenir si nécessaire des excuses, la suppression
ou la modification de son message.

**En fonction de la gravité, l’avertissement précède la radiation.**
Un avertissement est adressé à la personne avec copie à son syndicat.

Toute radiation prononcée par le modérateur du forum doit être suffisamment
motivée, argumentée.

La Commission administrative confédérale est consultée par le modérateur
préalablement.

Amendements
===========

- :ref:`amendements_motion_26_2021_stp67`

Votes
========

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°26 2021 Educ21     |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
