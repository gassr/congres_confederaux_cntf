
.. _amendement_motion_47_2010_sipm_rp:


====================================
Amendement à la motion 47 (sipm rp)
====================================


Les abonnements au Combat syndicaliste sont compris dans la cotisation mensuelle
des adhérents.

Le coût supplémentaire serait d'un euro par mois pour les travailleurs et gratuit
pour les précaires.

Les syndicats enverront les adresses de leurs syndiqués au Combat syndicaliste,
afin qu'ils reçoivent le CS à domicile.


+----------------------------------+------------+----------+------------+---------------------------+
| Nom                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==================================+============+==========+============+===========================+
| Amendement à la motion n°47      |            |          |            |                           |
| sipm rp                          |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_47_2010`
