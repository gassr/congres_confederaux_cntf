
.. _contre_motion_1_2014_ess34:

============================================
Contre-motion à la motion n°1 2014 (ESS34)
============================================

.. seealso::

   - :ref:`motion_1_2014_sinr44`





Argumentaire
==============

Nous pensons qu'il est problématique de changer les règles de fonctionnement
pendant un congrès.

Cela pourrait poser des problèmes aux syndicats qui ont préparé le congrès
en se référant aux règles édictées précédemment (exemple des procurations)
ou nous amènerait à adopter ou refuser des motions avec des règles différentes
lors du même congrès (exemple des conditions d'adoption d'une motion ou des
règles de quorum).

Motion
=======

Les motions de fonctionnement de congrès confédéral, placées en début de
cahier, s'appliquent à partir du congrès suivant.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°1 2014 <motion_1_2014_sinr44>`

       :ref:`ESS34 <ess34>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
