
.. _amendement_motion_6_2021_cnt42:

===============================================================================================================================================
Amendement N°2 à la motion n°6 2021 **De la nécessité d’affirmer notre engagement en faveur d’une écologie radicale et sociale** par **CNT42**
===============================================================================================================================================

- :ref:`motion_6_2021_etpic30`

Proposée par les syndicats CNT Educ 42, CNT Culture et
Spectacle 42, CNT Santé-Social 42, Interco 42.

Autres motions CNT 42

    - :ref:`CNT42 <rhone_alpes:cnt42_motions_congres>`


Argumentaire 
================

Amendement 
============

Passage à compléter:
    **Elle y apporte un angle de vue anticapitaliste, collectiviste,
    syndicaliste, révolutionnaire, et internationaliste**.

Ajout
    féministe, antiraciste et décolonial.


Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion n°6  |            |          |            |                           |
| CNT42                       |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
