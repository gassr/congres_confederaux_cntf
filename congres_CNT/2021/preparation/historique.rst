


Historique planning d’envoi des motions et contre motions
===============================================================

Les motions, contre motions et amendements, sont à envoyer aux adresses
suivantes :

- cnt@cnt-f.org
- congres@cnt-f.org


Petit rappel utile
======================

Nous vous rappelons qu'un :term:`amendement` constitue une proposition
de modification du texte initialement proposé par une :term:`motion`.

Le congrès confédéral **ne procédant à l'examen des propositions d'amendements
que lorsque la motion initiale a été adoptée par le congrès**.

Une :term:`contre-motion`, constitue quant à elle une proposition qui
entre en contradiction avec le sens de la motion initiale.
Les contre-motions ne sont examinées par le Congrès Confédéral que
lorsque la motion initiale n'a pas été adoptée.
