
.. index::
   pair: Fédération transports; congrès I (28 novembre 2011)
   pair: 2011; congrès constitutif fédération transports



.. _congres_federation_des_transports_i:

================================================================================================
Ier Congrès Fédération des travailleurs des transports,de la logistique et activités auxiliaires
================================================================================================

Nous sommes heureux de vous faire part de la naissance de la
Fédération des transports, de la logistique et des activités
auxiliaires.

**Son congrès constitutif s’est tenu le 12 novembre 2011**.
