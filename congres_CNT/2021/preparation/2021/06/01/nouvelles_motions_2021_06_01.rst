

.. _message_sc_2021_06_01:

=======================================================================================================================================
Cahier des mandaté.e.s du XXXVe Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon **(Version du mardi 1er juin 2021 10h38)**
=======================================================================================================================================


- :download:`Télécharger le CAHIER_de_la_du_mandate·e_35eme_Congres_confederal_2021_06_01.pdf  <CAHIER_de_la_du_mandatee_35eme_Congres_confederal_2021_06_01.pdf>`

::

    -------- Courriel original --------
    Objet: Fwd: Fwd: Re: Fwd: [Liste-syndicats] Cahier des mandaté.e.s du
    XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date: 2021-06-01 10:38
    De: Secrétariat Confédéral <cnt@cnt-f.org>
    À: liste CA <liste.ca@bc.cnt-fr.org>, liste-bureau@bc.cnt-fr.org,
    Liste-syndicats@bc.cnt-fr.org


On va y arriver...

Bonne réception

Serge

S.C. provisoire


::

    Fwd: Re: Fwd: [Liste-syndicats] Cahier des mandaté.e.s du XXXV e Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    DATE : Mon, 31 May 2021 23:04:04 +0200
    DE : congres@cnt-f.org
    POUR : cnt@cnt-f.org

    Voici le cahier de la.du mandaté.e avec les amendements et la
    contre-motion de l'Interco 69 qui étaient restés coincés dans les
    tuyaux. Et la feuille de vote modifiée en conséquence... Les autres
    documents définitifs n'étaient que provisoires, qu'on se le dise !!!

    Valérie, pour la commission de préparation du congrès 2021


::

    Objet: Re: Fwd: [Liste-syndicats] Cahier des mandaté.e.s du XXXV e
    Congrès confédéral les 25, 26 et 27 juin 2021 à Dijon
    Date: 2021-05-31 22:07
    De: Margo <XXXXXX>
    À: congres@cnt-f.org, "cnt@cnt-f.org administration"@cnt-f.org

    Salut à toutes et tous,

    Je pensais avoir réglé nos problèmes de boite mail ce soir, il n'en
    est rien, je ne peux même plus m'y connecter du tout...

    Si vous pouvez répondre à notre mail de détresse quant à nos
    amendements et contre-motion sur ma boite perso, ça serait vraiment
    très gentil.

    J'avais envoyé nos amendements au secrétariat confédéral la semaine
    dernière, depuis ma boite perso, mais je n'ai jamais eu de réponse non
    plus...

    Mille excuses pour ce gros cafouillage,

    Margo pour l'interco 69
    Le 31/05/2021 à 20:24, Margo a écrit :

    > -------- Message transféré --------
    >
    > SUJET :
    > Re: [Liste-syndicats] Cahier des mandaté.e.s du XXXV e Congrès
    > confédéral les 25, 26 et 27 juin 2021 à Dijon
    >
    > DATE :
    > Mon, 31 May 2021 19:58:26 +0200
    >
    > DE :
    > INTERCO 69 <interco69@cnt-f.org>
    >
    > POUR :
    > Secrétariat Confédéral <cnt@cnt-f.org>, congres@cnt-f.org,
    > liste.ca@bc.cnt-fr.org, liste-bureau@bc.cnt-fr.org,
    > Liste-syndicats@bc.cnt-fr.org
    >
    > Salut à toutes et tous,
    >
    > Au syndicat on avait envoyé nos amendements et contre motions le 22
    > Mai, elles ne sont apparemment jamais arrivées et on ne s'en rend
    > compte que maintenant, parce que la mandatée au x mails vient
    > seulement de résoudre le problème avec Globnet de son côté, et
    > qu'on est du coup passé.e.s au travers de tous vos mails depuis une
    > dizaine de jours, c'est pas faut de s'etre débatu.e.s, quand ça veut
    > pas ça veut pas...
    >
    > On espère que vous pourrez les prendre en compte, même si on a
    > conscience qu'il est bien trop tard, mais on a beaucoup bossé en
    > syndicat et ça nous démoraliserait bien tout ce foirage.
    >
    > Avec nos excuses.
    >
    > L'interco 69
