.. index::
   pair: Motion 16 ; 2021

.. _motion_16_2021_cnt09:

================================================================================================================================================
Motion **n°16** 2021 **Contribution au Combat Syndicaliste** par **CNT 09**
================================================================================================================================================

Autres motions CNT09

    - :ref:`syndicats:motions_interco09`


Motion pour la contribution au Combat Syndicaliste
===================================================

- :ref:`motion_5_2016_etpic30`

Modification de la :ref:`motion adoptée au 34ème congrès en novembre 2016 <motion_5_2016_etpic30>`.

Argumentaire
===============================

.. figure:: motion_16/combat_syndicaliste.png
   :align: center

Le compte du Combat syndicaliste se présente à fin novembre 2019 comme suit :

.. list-table::
   :widths: 30 18 18 18
   :header-rows: 1

   * - Remises chèques
     - Contribution
     - Sortie
     - SOLDE
   * -
     -
     -
     - 6 873,90 €
   * - 5 613,40 €
     - 3 540,00 €
     - 5 946,00 €
     - 10 081,30 €

Avec un peu plus de 300 euros de contribution, nous serions à l’équilibre pour
ce compte.

C’est presque parfait !

Certains syndicats oublient de contribuer, d’autres ont peu de moyen.

Motion
========

Modification de la motion adoptée au 34ème congrès en novembre 2016 comme suit:

    La contribution est toujours de 4€ par mois, mais n'est pas obligatoire.
    Que chaque syndicat fasse avec ses moyens.

Il est facile et le trésorier peut le faire, de donner régulièrement un état
des comptes du combat syndicaliste.
Ainsi nous pouvons tous voir régulièrement où en est le compte du combat syndicaliste.


Amendements
===========


Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°16 2021 CNT09      |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
