
.. _contre_motion_10_2021_cnt42:

===========================================================================================================================================
Contre-motion N°1 à la motion n°10 2021 **Lutte pour l’égalité des sexes - contre le sexisme, le patriarcat, l’homophobie** par **CNT42**
===========================================================================================================================================

- :ref:`motion_10_2021_etpic30`

Proposée par
=============

Proposée par les syndicats CNT Educ 42, CNT Culture et Spectacle 42, CNT Santé-Social 42, Interco 42.


Argumentaire
==============

La constitution d’une commission anti-sexiste, ou « commission confédérale
antisexiste et antipatriarcale pour l’égalite et l’équité » ne nous parait
pas une structure adaptée et efficace pour permettre une réelle amélioration
de la prise en compte de ces problématiques au sein de la CNT.

En effet il n’est pas judicieux de défendre les personnes, de lutter pour
elles, à leur place.
Il est préférable de leur donner les moyens de le faire par et pour
elles-même et donc d'éviter paternalisme et condescendance.

C’est ce que s’emploient à faire de manière concrète les groupes CNT Femmes
Libres/Mujeres Libres, qui ont émergés du désir de s’organiser en non-mixité
par des cénétistes concernées.

En outre, l’opposition entre non-mixité et inclusivité formulée dans
l’argumentaire de la motion 10 n’est pas pertinente : l’existence de
groupes non-mixtes n’enlève en rien la nécessité des syndicats de s’emparer
également des questions antipatriarcales en mixité.

Que la commission antisexiste soit « invitée a produire un bulletin interne »
est symptomatique des limites de celle-ci : une production purement
théorique, et le refus d’assumer publiquement les problèmes que rencontre
le syndicat.
Cela implique une négation violente pour les personnes qui vivent des
oppressions, et l’installation d’un climat de déni.

Pour rappel : cette commission a été tentée, avec un fonctionnement qui
a démontré son inutilité, si ce n'est d'avoir un parapluie antisexiste
à brandir, sans réel objectif, moyen ou légitimité.
Les personnes qui souhaitaient s'investir dans cette lutte ne se sont
pas appropriés cet outil qui ne leur semblait pas approprié, en terme
d'enjeux, d'attentes, d'objectifs. Des groupes au sein de la CNT
existent, mais n'ont pas les moyens confédéraux de lutter ou de former
les syndicats qui le souhaitent, en-dehors du soutien de leurs syndicats
particuliers.
Les groupes FLML existent déjà, sont actifs, répondent aux exigences
posées dans cette motion.

Contre-motion
==============

Dans les UL, les questions d'oppressions et de discriminations doivent
être posées en mixité, en préservant des espaces de non-mixité si certaines
cénétistes souhaitent se retrouver au sein de groupes Femmes Libres/Mujeres Libres..

Les syndicats doivent par ailleurs s'emparer de ces questions quand bon
leur semble.

Les syndicats CNT Educ 42, CNT Culture et Spectacle 42, CNT Santé-Social 42, Interco 42

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°10 |            |          |            |                           |
| CNT42                          |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
