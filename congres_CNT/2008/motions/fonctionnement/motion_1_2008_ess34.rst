

.. _motion_1_2008_ess34:

=============================================================
Motion n°1 2008 : Identification des votes, ess34 [adoptée]
=============================================================


**Présentation ESS 34**
	Il est essentiel qu'à la CNT, chaque vote des mandatés au Congrès confédéral
	puisse être contrôlé par le syndicat mandant.

	Dans les statuts, un syndicat peut représenter deux autres syndicats, cela
	renforce la nécessité de pouvoir contrôler.

	Le congrès n'étant pas une chambre d'enregistrement (possibilité de mandats
	semi-ouverts) importance de devoir vérifier l'efficience des votes.


**Interco 91**
	proposition d'un amendement. Sur la forme, le terme démocratie
	(même directe) peut gêner...sinon d'accord avec la motion.

**ESS 34**
	pour nous, anarchosyndicalisme et SR ne sont pas aussi précis que « démocratie directe ». Proposition de mettre
	les deux termes.


**Interco 34**
	même suggestion qu'ESS 34, rajouté AS/SR...Mais AS/SR ne sont pas des
	principes, alors que la **démocratie directe** est un principe.


**Interco 54**
	vote contre la motion car « les relations de confiance sont clairement
	établis...quand on sera deux mille, on avisera... ».


**Interco 34**
	AS et SR, ne sont pas des principes à proprement parler: je suggère la
	formulation **en accord avec ses principes essentiels,
	la démocratie directe,  la CNT, AS et SR, ...**


+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 35         | 8        | 4          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


MOTION Adopté avec reformulation

Présidence : L’identification du vote des syndicats prend effet immédiatement.
