
.. _motion_7_2012:
.. _motion_7_2012_ste72:

===============================================================================
Motion n°7 2012 : Organisation des congrès de la CNT et temps de parole STE 72
===============================================================================
  
.. seealso::

   - :ref:`ste72_pub`
   - :ref:`motions_ste72_2012`




Préambule
=========

Cette motion concerne l'organisation des congrès futurs de la CNT, mais est
également proposée comme motion d'ordre pour l'organisation du Congrès
confédéral de 2012.

Chaque point peut être débattu et adopté séparément.

Motion
======

Il est préférable de discuter moins mais mieux. Un congrès permet environ
20 heures de débat sur 3 jours. Ainsi, compte tenu des probables seconds
tours de votes pour les motions les plus partagées, **un congrès ne peut traiter
qu'une trentaine de motions**.

Il faut donc limiter le temps consacré à une motion de manière à leur
consacrer 45 minutes maximum et contenir les interventions à 3 minutes par
personnes.

Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°7           |            |          |            |                           |
| STE72                |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

Amendements
============

- :ref:`amendement_motion_7_2012_stp72`

Contre-motions
==============

- :ref:`contre_motions_5_6_7_etpic30`
