
.. _contre_motion_24_2014_sipmcs:

===================================================
Contre motion à la motion 24 2014 SIPMCS
===================================================

.. seealso::

   - :ref:`motion_24_2014_ste75`

Contre argumentaire à la motion 24
===================================

À nouveau nous soutenons l'argumentaire, mais sommes en opposition totale
avec la motion.

La motion propose clairement une séparation entre les technicienNEs et
les intellectuelLEs.
Nous pensons, et espérons sans en douter, que cela relève plus d'une
maladresse que d'une réelle volonté.

Nous sommes fermement opposés à ce qu'une telle distinction ait lieu.
D'un côté il y aurait les mains, de l'autre les cerveaux...

Motion C
=========

Le **secrétaire confédéralLE chargéE des relations avec les médias** est
intégré à la commission **site internet** de la motion B.

Ce qui étaient des communiqués destinés aux médias rédigés par les rélations
médias, deviennent des articles destinés à être lus par le plus grand nombre,
notamment sur le site et le CS.
