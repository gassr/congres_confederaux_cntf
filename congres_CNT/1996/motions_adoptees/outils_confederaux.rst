
.. _motions_outils_confederaux_cnt_congres_lyon_1996:

==========================================================================================
Motion Outils confédéraux, 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
==========================================================================================


- Création d'une commission de réflexion sur des minorités en France (Basques, Bretons...)


Annuaire confédéral
===================

Création d'un annuaire interne sommaire des membres du BC. Il est nécessaire
que le BC fasse parvenir ou puisse tenir à disposition de chaque syndicat cet
annuaire précisant d'une façon nominative qui s'occupe de telle ou telle
commission, de telle ou telle tache, qui a la charge de telle ou telle fonction.


Fête confédérale du Combat Syndicaliste
=======================================

Il serait souhaitable que la CNT ait chaque année à date fixe une fête
confédérale (fête du Combat Syndicaliste) sur le modèle de LO (Lutte Ouvrière)
ou de l'Humanité (en plus modeste bien sûr! ).

Elle se déroulera sur une journée bien sur pour commencer, quitte à s'étendre
plus tard si cela marche. En effet, outre le fait que cela constitue un
excellent moyen de propagande auprès des gens, relayé par les médias cela
permettrait aussi aux adhérents de se rencontrer autrement que pendant
un Congrès.

Sans compter qu'aux vues des résultats des organisations suscitées, cela
constitue une importante rentrée financière, ce qui est loin d'être négligeable.

Mais cela représente un travail considérable et nécessite le travail d'une
commission nommée par le Congrès pendant un an et qui présentera un projet
de fête, si possible pour le printemps ou l'été prochain.

Le Congrès donne mandat au syndicat de l'éducation région parisienne pour
proposer un projet concret lors du prochain CCN.

Bibliothèques
=============

Bien que des livres soient disponibles aux Vignoles et dans quelques autres
villes, les militants n'ont pas toujours la possibilité de se former à la
connaissance politique, syndicale, culturelle par eux-mêmes.

Il serait donc souhaitable que chaque union régional créé une bibliothèque au
niveau de leur région.

Une liste de livres, brochures disponibles dans chaque région sera envoyée au
bureau confédéral et à tous les syndicats qui le désirent.

Un syndicat de lutte doit aussi se doter de moyens littéraires.
Un pas nouveau vers l'autogestion serait franchi.


Formation
=========

Depuis plusieurs mois, nous constatons une affluence importante dans notre
syndicat.

Les adhésions se sont faites sur une des bases de la CNT : la COMBATIVITE mais
en méconnaissant le projet autogestionnaire de la CNT

C'est pourquoi nous demandons:

- Un cycle de formation rapide pour les nouveaux adhérents sur des questions
  syndicales indispensables (juridiques, orientations de la CNT, pratiques
  syndicales...) ;
- en plusieurs jours pour une formation nationale (week-end ou autre) ;
- en intervenant dans chaque UR ;
- Le syndicat SSE 38 prend la suite de la "commission formation" et se charge
  donc:

      * de recenser les besoins en formation des différents syndicats ;
      * de trouver les personnes ou les textes pour répondre à ces besoins
        (Attention! ce n'est pas au SSE 38 d'élaborer les textes de formation).

- Les différentes formations seront enregistrées sur cassettes audio ou vidéo.
  ou seront mises par écrit afin que tous les syndicats de la Confédération
  puissent en profiter.


**Pour l'avenir de la CNT, il serait important de renforcer nos militants sur
des bases anarcho-syndicalistes.**
