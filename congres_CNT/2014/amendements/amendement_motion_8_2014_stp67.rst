
.. _amendement_motion_8_2014_stp67:

================================================
Amendement à la motion n°8 2014 (STP67)
================================================

.. seealso::

   - :ref:`motion_8_2014_sinr44`




Argumentaire
=============

Si une fédération se présente aux élections, il nous semble que le CCN serait
plus à même de se prononcer sur les décisions d'une fédération après que
celle-ci ait fait le bilan de sa participation aux élections.

Amendement
==========

Si une fédération décide de se présenter aux élections professionnelles
dans le public, elle est tenue de présenter au deuxième CCN suivant les
élections un bilan de sa participation à ces dernières.

En fonction de ce bilan, le CCN se prononcera préalablement à la
participation ou non aux prochaines élections.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°8 2014 <motion_8_2014_sinr44>`

       :ref:`STP67 <stp67>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
