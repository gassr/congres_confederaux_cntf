.. index::
   pair: 34_35_36; Motion 2012

.. _motions_34_35_36_xxxii:
.. _motions_34_35_36_2012:

========================================================================================
Fusion des motions 34, 35 36 commission **Institut de Formation Confédéral CNT** (2012)
========================================================================================

.. seealso::

   - :ref:`motion_34_2012`
   - :ref:`motion_35_2012`
   - :ref:`motion_36_2012`


Le congrès mandate une commission **Institut de Formation Confédéral CNT**.

Elle est mise en place dans le but de créer un institut de formation:
mettre en place des sessions décentralisées dans les UR ou UD sur les
thèmes d'actualités :

- retraites,
- salaires,
- contrats à durée déterminée...

Mais aussi :

- histoire sociale,
- solidarité internationale,
- représentation du personnel et/ou du syndicat au sein de l'entreprise...

Elle évaluera un partenariat avec une association ou un organisme
permettant aux adhérents de la CNT de se former sur le temps de travail.

La Confédération sera tenue régulièrement informée des démarches entreprises
et des engagements à prendre.

Elle présentera un projet concret et chiffré à la Confédération au
prochain congrès.

.. toctree::
   :maxdepth: 4

   motion_34_2012_etpics57
   motion_35_2012_chimie_bzh
   motion_36_2012_chimie_bzh
