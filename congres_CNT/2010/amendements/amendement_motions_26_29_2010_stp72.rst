
.. _amendement_motions_26_29_2010_stp72:

===============================================
Amendement aux motion 26 et 29 2010 (STP 72)
===============================================


1. L'organisation de la modération du forum intranet suit les propositions
   de :ref:`la motion n° 26 2010 <motion_26_2010>`, en particulier le mandatement
   collectif de **3 personnes issues de 3 syndicats différents**.


+----------------------------------+------------+----------+------------+---------------------------+
| Nom                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==================================+============+==========+============+===========================+
| Amendement à la motion n°26      |            |          |            |                           |
| STP 72                           |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+



2. Nous proposons de préciser dans la liste des motifs d'avertissement de
   la :ref:`motion n° 29 2010 <motion_29_2010>` ce que nous entendons par atteintes
   graves à l'intégrité: **propos à caractère misogyne, raciste, ou relatif
   aux goûts personnels, tout ce qui humilie un individu et n'a rien à
   voir avec la vie syndicale**.

+----------------------------------+------------+----------+------------+---------------------------+
| Nom                              | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+==================================+============+==========+============+===========================+
| Amendement à la motion n°29      |            |          |            |                           |
| STP 72                           |            |          |            |                           |
+----------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_26_2010`
   - :ref:`motion_29_2010`
