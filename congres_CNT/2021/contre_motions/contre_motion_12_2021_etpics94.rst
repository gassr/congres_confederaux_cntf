
.. _contre_motion_12_2021_etpics94:

==========================================================================================================================================
Contre-motion N°2 à la motion n°12 2021 **Modification des statuts pour la gestion interne des violences patriarcales** par **ETPICS94**
==========================================================================================================================================

- :ref:`motion_12_2021_sinr44`

Autres motions ETPICS94

    - :ref:`paris:motions_etpics94`


Argumentaire
================

La motion 12 ne peut s'appliquer qu'en excluant la plupart des paragraphes
de :ref:`l'article 24 des statuts <statuts_conf:article_24_statuts_CNT_2014>`.

Contre-motion
===============

Maintien de l'intégralité des dispositions de l'article 24.

Vote
====

+--------------------------------+------------+----------+------------+---------------------------+
| Nom                            | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+================================+============+==========+============+===========================+
| Contre motion à la motion n°12 |            |          |            |                           |
| ETPICS94                       |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès            |            |          |            |                           |
|                                |            |          |            |                           |
+--------------------------------+------------+----------+------------+---------------------------+
