
.. _amendement_motion_40_2012_stea:

==========================================
Amendement à la motion 40 (STEA)
==========================================

.. seealso::

   - :ref:`motion_40_2012_etpic30`



Argumentaire
=============

Il nous semble nécessaire de rajouter une cinquième étape à ce processus de
délabellisation.

Amendement


Amendement
===========

5. Lors d’une délabellisation confirmée, le BC fait une lettre recommandée
   avec accusé de réception à la mairie concernée.
   Le recommandé oblige le destinataire à répondre, la réponse devra être
   archivée.

Vote
====

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Amendement à la motion 40   |            |          |            |                           |
| STEA                        |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
