

.. _motion_21_2010:

====================================================================================================
Motion n°21 2010 : Création d’un secrétariat confédéral au développement, Santé-social 69 [rejetée]
====================================================================================================



Argumentaire CNT
================

Considérant que le développement de la :term:`C.N.T` est une nécessité impérieuse
pour peser dans les luttes, une coordination de ce développement au niveau confédéral
nous semble nécessaire. Il s’agit de sortir de l’improvisation qui parfois est la nôtre.

Trop souvent les camarades et syndicats en difficulté ou novices doivent
se débrouiller seuls et/ou ne savent pas à qui s’adresser pour trouver de l’aide.
Cette situation pénalise de fait les camarades qui ne sont pas insérés dans un
réseau social :term:`C.N.T` local suffisamment développé ou qui n’ont pas le
temps et les moyens suffisants pour faire face aux nécessités de l’action syndicale.

Conscients que cette situation est préjudiciable à notre développement, il nous
paraît indispensable de mettre en place une structure d’appui et de soutien aux
syndicats et travailleurs isolés.

Conformément à notre fonctionnement fédéraliste de bas en haut, cette structure
aura pour fonction, non de remplacer l’action des militants sur le terrain,
dans l’entreprise, l'atelier ou l'établissement, mais de l’accompagner en fonction des
besoins exprimés.

A cette fin il nous paraît nécessaire de mettre en place une structure permanente
à part entière au niveau confédéral par la création d’un secrétariat confédéral
au développement.

Une telle structure permettra une meilleure lisibilité et cohérence de ce mandat
vis-à-vis des syndicats et adhérents.

Motion
======

La :term:`C.N.T` se dote d'un secrétariat au développement intégré au :term:`B.C`. Le rôle du secrétaire confédéral (adjoint) en
charge des campagnes de développement est de donner (en lien avec le reste du :term:`B.C`) l’impulsion à l’échelle
nationale de l’action syndicale de la :term:`C.N.T` en vue de son développement mais toujours dans un esprit fédéraliste
allant du bas vers le haut.

Le travail du secrétariat confédéral en charge du développement consistera à répondre aux attentes des syndicats
(sections d’entreprises) et à leurs besoins:

- De par sa lecture globale de l’actualité militante de nos structures locales, le secrétariat au développement
  pourra en consultation avec le reste du :term:`B.C` impulser des campagnes et en assurer la coordination. Ce secrétariat
  privilégiera les contacts directs pour plus d’efficacité et de réactivité.
- Par un va et vient permanent, il assurera l'interface entre les futures/nouvelles/sections demandeuses et les
  structures confédérales concernées.
- Dans le cas de demande d'adhésions collectives de travailleurs sur une entreprise déterminée, il assurera la
  représentation de la :term:`C.N.T` dans les secteurs géographiques isolés et présentera notre organisation syndicale.
- Dans les secteurs géographiques où la :term:`C.N.T` est présente et où un lien de travail est possible avec la structure
  :term:`C.N.T` locale, il s'assurera à la demande de celles-ci des bonnes conditions d'accueil des nouveaux arrivants.
- En lien avec les relations médias et le web master confédéral de la :term:`C.N.T`, il s'assurera de la visibilité des luttes
  en cours.
- Par le biais du secteur propagande, il s’assurera de la production de matériel à cet effet.
- Si besoin, suivant les cas de figure, il supervisera la question juridique avec les mandatés en charge de cet
  aspect. Cette question sera essentielle.
- Enfin, son travail sera supervisé par le :term:`B.C` qui se chargera en outre, comme traditionnellement, de la besogne
  administrative interne à la :term:`C.N.T` (labellisations éventuelles, rappel des échéances démocratiques, coordination
  générale…) Bien évidemment, le secrétariat au développement n’a pas vocation à se substituer à l’action des
  fédérations d’industrie constituées et autres structures de base de la :term:`C.N.T`.

S’il faut se donner les moyens pour un développement de la :term:`C.N.T` dans de nouveaux secteurs, il faut pouvoir
faire en sorte que les sections existantes se pérennisent. On remarquera que le facteur récurant de disparitions de
sections à la :term:`C.N.T` est bien souvent la répression antisyndicale. Tout comme il assurera l’aspect développement,
le secrétariat devra coordonner les campagnes de soutien/solidarité aux sections en proie à la violence patronale.
La base de travail sera la même que pour les campagnes de développement.

En conclusion
=============

Notre démarche consiste à choisir pour objectif commun de développer la :term:`C.N.T` comme un syndicat ayant
l'ambition de devenir une organisation de « masse », même modérément dans un premier temps. Une
organisation ayant défini des buts et les moyens pour y arriver. Une organisation ancrée dans les réalités des
luttes du monde du travail d'aujourd'hui et porteuse d'un projet révolutionnaire, tout en offrant un espace
d'émancipation individuelle et collective.

Pour se faire, la méthode de travail envisagée se déclinera dans le temps à court et « long » terme. Dans un
premier temps, à court terme par la pertinence des campagnes de terrain prioritaires qui seront à mener. Et aussi
sur le long terme par l'image renvoyée par la :term:`C.N.T` sur l'extérieur par le biais des communiqués de presse, de sa
réactivité face à l'actualité sociale etc. Les différents outils confédéraux de propagande devront illustrer au mieux
la cohérence et la pertinence de notre projet syndical, afin d'en assurer la visibilité, la lisibilité et sa crédibilité
non seulement auprès des syndiqués en rupture avec leur bureaucratie au sein des autres organisations mais aussi
auprès de l'ensemble du monde du travail.

Discussion
==========

Santé social 69
    Proposer de dégager un mandat à part entière au développement pour donner
    un soutien aux syndicats ou adhérents isolés et pour impulser des
    campagnes de développement, de soutien et de solidarité.

STE57
	Contre cette idée car cela pose plusieurs problèmes. La manière dont on doit
	développer la CNT n'a pas encore été résolue. On n'est pas tous d'accord sur
	cette manière de ce développement donc que va faire ce secrétariat et sur
	quelle ligne ?

STE93
	contre cette motion bien que la question du développement est essentielle.
	Lien et jonction à faire avec les syndicats OK. Les campagnes sont impulsées
	par les CCN, les syndicats et pas par un secrétariat. Question de l'accueil
	essentielle aussi mais ce travail au plus près dans les syndicats ou UL.
	Mandat particulièrement large (trop).

SIPM
	pour l'autogestion. Uniquement pour des mandatés techniques et pas de mandat
	politique.
	Quid des CCN ?

STEA
	remise en cause de l'autonomie des syndicats et des principes d'action et
	de démocratie directe. On est pour un syndicat de masse sur des valeurs
	autogestionnaires et un fonctionnement communiste libertaire et pas pour
	adopter des positions décidées par d'autres que nous.

Santé Social 69
    C'est bien d'avoir des mandats référents. Au niveau des campagnes, si on regarde
	le SI, ils ont bien un mandat qui ne sort pas forcément du CCN et ils proposent
	plein de trucs. Entre janvier et septembre, la CNT ne s'est pas prononcée sur
	le mouvement des retraites. Du coup la CNT n'a pas appelé à la grève interprofessionnelle.
	C'est une vision globale que doit avoir le BC pas du tout un mandat pour imposer
	quoi que ce soit.

SSEC 59-62
	Pas de risque de bureaucratisation car si le secrétariat impulse mais que
	les syndicats ne reprennent pas cette campagne rien ne se fera donc aucun
	risque. On votera donc pour cette motion.

ETPIC 30
	Favorable au développement de ce secrétariat. On ne comprend pas trop les
	craintes qui peuvent y avoir sur cette motion.


.. _vote_motion_21_2010:

Vote motion n°21 : Création d’un secrétariat confédéral au développement, Santé-social 69 [rejetée]
===================================================================================================

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°21          | 33         | 22       | 7          | 4                         |
| Santé social 69      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion n°21 rejetée**
