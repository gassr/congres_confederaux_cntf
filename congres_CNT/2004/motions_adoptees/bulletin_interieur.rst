

.. _motion_bulletin_interieur_cnt_congres_saint_denis_2004:

====================================================================================================
Motion Bulletin intérieur de la CNT  28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
====================================================================================================

Rôle et diffusion du Bulletin Intérieur
=======================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 34         | 9        | 18         | 11                        |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Texte
-----

Le Congrès rappelle donc que le Bulletin intérieur (:term:`B.I`) est le
dispositif  confédéral qui permet aux syndicats et aux adhérents de suivre,
de s'exprimer,  d'échanger et de débattre des orientations, des positions
comme des activités  syndicales, cela dans la facilité et la visibilité
de tous ;

Par ailleurs, il décide que des améliorations sur sa diffusion doivent être
prises.
