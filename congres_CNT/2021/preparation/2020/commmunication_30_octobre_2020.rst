
.. _bc_2020_10_30:

================================================================================
Le 30 octobre 2020 : **Tenue du congrès confédéral les 5 & 6 décembre 2020**
================================================================================



Introduction
=============

Le 30 octobre 2020
Objet: Tenue du congrès confédéral les 5 & 6 décembre 2020

Aux syndicats de la confédération
Aux unions de syndicats

Cher.es camarades,

Dans le cadre de la réunion du bureau confédéral et de la commission
administrative confédérale qui s’est tenue le samedi 24 octobre 2020
aux 33 rue des Vignoles, nous avons discuté de l’organisation du prochain
congrès confédéral et des modalités.

Au vu des éléments transmis par l’interco 21 à propos de la salle «les tanneries»,
dans laquelle nous ne pouvions réunir que 40 à 50 délégué.es, et au regard
de la situation sanitaire actuelle, le report du congrès confédéral à
la mi-mai voir juin 2021 semble de fait se justifier au regard d’un
nouveau confinement dans tout l’hexagone jusqu’au 1er décembre 2020
mais qui va certainement se prolonger.

**Nous allons contacter l'interco 71** pour avoir plus d’information quant
à leur proposition d’alternative.

Nous tenons à les en remercier, même s’il nous semble à priori qu’aucun
département n’offre les conditions  de  sécurité  sanitaire  et,
partant,  de sérénité nécessaire à la tenue du congrès confédéral.

**Nous tenons à remercier l’interco 21** pour le travail déjà effectué et
privilégierions à priori leur proposition initiale, par respect
pour celui-ci.

Si l’option Saône-et-Loire n’est pas retenue, nous inviterons d’ailleurs
les camarades de ce département à apporter leur aide au 21.

Plus largement, nous rappelons à tous les syndicats que les bonnes
volontés seront les bienvenues pour permettre la tenue du congrès
confédéral dans les meilleures conditions possibles.

En ce qui concerne l’aspect statutaire, nos présents statuts précisent
un délai maximum de 30 mois entre deux congrès confédéraux.

Or, nous nous assumons nos mandats depuis **novembre 2016**.

Nous vous informons qu’une consultation référendaire sera mise en place
par le bureau confédéral pour le renouvellement de l’ensemble
des mandats confédéraux.

Vous le comprendrez, il nous parait important pour la vie démocratique
de la confédération, et donc par principe de fonctionnement interne de
ne pas proroger automatiquement les mandats sans consultation et vote
des syndicats.

Il s’agit donc de repasser par des propositions, portées par des
syndicats, de candidatures nouvelles de mandaté·es ou de poursuite
par intérim des mandaté.es actuel.les.

A ce titre, les syndicats auront jusqu’au 5 décembre 2020 inclus pour
faire parvenir les différentes propositions de candidatures pour les
mandats suivants:

Mandat du Bureau Confédéral
===========================

- Secrétariat confédéral >Niko (ETPIC 30) > cnt@cnt-f.org
- Secrétaire Confédéral Adjointe Administratif > Vacant depuis juin 2017 mais géré
  en parti par Niko (ETPIC 30)> administration@cnt-f.org& annuaire@cnt-f.org
- Secrétaire Confédéral Adjoint (Gestion des contacts avec la CNT) > Martial (SIPMCS) > contact@cnt-f.org
- Trésorerie confédérale > Chritian (Interco 09) > tresorerie@cnt-f.org
- Secrétariat international> Vacant géré collectivement par les GT>international@cnt-f.org
- Secrétariat aux Relations avec les médias > vacant mais géré en parti par Niko (ETPIC 30)> media@cnt-f.org
- Webmaster site web CNT > Juliette (SINR 44) webmaster@cnt-f.org
- Secteur Vidéo> Martial (SIPMCS)> secteur-video@cnt-f.org
- Comité de rédaction du Combat Syndicaliste > MPI (STICS 13), Nico (SINR 44) > combat-syndicaliste@cnt-f.org
- Administration du Combat syndicaliste -abonnements et envois > Interco 07 > cs-administration@cnt-f.org


.. _ca_2016_2021:

Commission Administrative 2016 à 2020
==========================================

- Secrétariat juridique> Fred (Chimie Bretagne) secretariatjuridique@cnt-f.org
- Secteur propagande CNT-affiches / autocollants / brochures / badges :
  Gestion des commandes > Kelly (ETPIC 30) secteur-propagande@cnt-f.org
  Maquettage visuels> STICS 59 prod.propa@cnt-f.org
- Secteur textile> PTT 69> secteur-textile@cnt-f.org
- Postmaster > Théo (SINR 44)> postmaster@cnt-f.org
- Mandatés intranet > coordination et helping Ya (SIPMCS),
  Admin logiciel Pierre (SIPMCS),
  Administrateur système Ludovic (SIPMCS),
  modération du forum  Nico (interco 21)> intranet@cnt-f.org
- Comptabilité > Vacant depuis novembre 2016 mais géré par Christian(interco 09 > Tresorerie@cnt-f.org
- Réseaux sociaux > Fonz (SSCT Lorraine) > reseauxsociaux@cnt-f.org
- Revue théorique > Daniel (STE Maine) Imprimerie Confédérale > Interco 42

Les syndicats qui seraient intéressés pour proposer un.e camarade
à l’un des mandats, peuvent se rapprocher du ou de la mandaté.e actuel.le.

Cela pour avoir les informations nécessaires sur le contenu du mandat.

Le 6 décembre 2020, le bureau confédéral lancera une consultation
référendaire dans laquelle les syndicats devront s’exprimer sur les
propositions reçues (Un document sera envoyé).

**Les syndicats auront donc jusqu’au 15 janvier 2021 minuit pour y répondre**.

Les modalités de votes sont les mêmes que pour un congrès confédéral.

**Toutefois, pour qu’une décision soit prise, il faut que 2/3 des
syndicats à jour de cotisations se soit exprimé.**

**Nous rappelons donc à nouveau** que seuls les syndicats à jour de
cotisations confédérales (septembre 2020), et  ayant  payé la
péréquation du dernier congrès confédéral et n’ayant pas de dette
au niveau de la propagande peuvent:

- Proposer des candidatures aux différents mandats;
- Voter sur les différentes propositions;

Par la suite, il appartiendra au nouveau bureau confédéral mandaté,
d’envoyer les modalités pour le prochain congrès confédéral de la CNT
et par conséquent d’établir un nouveau calendrier pour l’envoi des motions
contre-motions etc.

Ce bureau confédéral provisoire, assumera ses fonctions jusqu’au prochain
congrès confédéral.

Recevez cher.es camarades nos salutations syndicalistes révolutionnaires,

Pour le bureau confédéral et la commission administrative confédérale de
la CNT, Niko

.. note:: Pour proposer des mandaté.es et/ou pour répondre à la consultation
   confédérale, merci d’envoyer un mail(seront pris en compte uniquement
   les mails de syndicats
