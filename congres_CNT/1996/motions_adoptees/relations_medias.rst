
.. _motions_relations_medias_cnt_congres_lyon_1996:

======================================================================================
Motion "Relations médias", 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
======================================================================================

Les Relations avec la Presse
============================

Au moins un membre du :term:`B.C` sera chargé des relations avec la presse
(écrite et audiovisuelle).

Il aura notamment pour tâche de constituer un fichier des adresses, contacts,
qui sera mis à la disposition de toute structure de la Confédération qui en
ferait la demande.

.. index::
   DOM-TOM


Contacts avec les DOM-TOM
=========================

Création au sein du bureau confédéral d'un poste aux contacts avec certaines
organisations des Dom-Tom.
