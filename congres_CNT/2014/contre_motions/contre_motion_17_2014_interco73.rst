
.. _contre_motion_17_2014_interco73:

==================================================
Contre-motion à la motion n°17 2014  (Interco 73)
==================================================

.. seealso::

   - :ref:`motion_17_2014_ste75`




Argumentaire
=============

Le débat sur la prostitution qui a récemment occupé l'espace médiatique a
suscité également des discussions dans notre confédération.

S'il semble légitime que les **travailleurs et travailleuses du sexe**
revendiquent des droits, ce sont les propos à la marge qui interpellent :
prostitution dite **autogérée et choisie**, **un métier comme les autres**,
**droit de disposer librement de son corps**, **abrogation des infractions
de proxénétisme**....

Les femmes sont majoritairement représentées chez les prostituéEs même si
une minorité d'hommes est aussi concernée.

Les clients, eux, sont quasi exclusivement des hommes.

Le système prostitutionnel s'inscrit donc bien dans des relations patriarcales
et sexistes.

Le **choix** de la prostitution ne veut rien dire dans une société inégalitaire
bâtie sur la division sexuée du travail.

Choisit-on également de rester à la maison, de porter le voile et pourquoi
pas de travailler pour un salaire inférieur au SMIC ?
Aux Pays Bas, on en arrive à la notion de **consentement de son plein gré à sa
propre exploitation**!

De surcroît, ce discours d'une minorité de prostituéEs occulte la situation
générale des prostituéEs qui souffrent et souhaitent s'en sortir.
C'est la misère qui pousse les femmes à se prostituer, des femmes de la
classe ouvrière principalement, qui n’auront probablement jamais l’occasion
de faire entendre leur opinion sur la question.

Au Strass, on reconnaît que le **choix** de la prostitution est tout relatif ;
mais on affirme que, puisqu’il faut bien gagner sa vie, il s’agit d’un métier
pas plus pénible que beaucoup d’autres et au final, un métier comme les autres.

Louer son sexe ou louer ses bras, ce serait donc pareil ?

Ce serait nier la différence fondamentale que, dans le cas d’une profession,
le corps est un outil de travail, il n’est pas considéré comme une marchandise.

Et puis nier cette différence peut mettre en danger la reconnaissance du viol
comme d’un crime spécifique : violer quelqu’un serait-il alors différent de
lui casser le bras ?

Quant à ce qui concerne la victimisation dont sont accusés les abolitionnistes,
être victime n'est pas un état psychologique (une attitude passive et timorée)
mais la place occupée dans un rapport de forces. Personne ne peut rester une
victime consentante de l'exploitation ou de la répression mais doit être aidée
à la prise de conscience de ce qui opprime et à la révolte contre cet état de
fait.

Le STRASS n'est pas invisible, ses propos sont bien relayés dans les médias.
Mais les attaques anti  féministes, les abolitionnistes traitéEs de puritainEs
et/ou de **putophobes** posent problème et par dessus tout, la banalisation et
la pérennisation de la prostitution sont inacceptables.

La liberté de disposer de son corps qui reprend le slogan **Mon corps
m'appartient** (dans un sens très différent de celui des années 70 !)
s'inscrit **dans un rapport marchand inacceptable et ne contrarie en rien
les visées ultralibérales**.

Dans the Economist, journal ultralibéral, on a pu lire : **On devrait permettre
aux gens d'acheter et de vendre ce qu'ils veulent, y compris leur propre
corps**.

La liberté revendiquée s'inscrit plus dans une vision libérale que libertaire.

Doit-on aussi remettre en cause les lois de bio éthique interdisant de
vendre ses organes ?
Dans les pays où elle a été mise en place, comme aux Pays Bas ou en Allemagne,
la légalisation de la prostitution n'a pas amélioré la condition des
prostituéEs et n'a pas empêché l'augmentation de leur nombre toujours
constitué majoritairement de femmes.

L’Allemagne est devenue le plus grand marché d’Europe en ce qui concerne la
prostitution avec pour corollaire une dégradation de la situation des
prostituéEs.

Difficile d'imaginer la persistance de la prostitution bâtie sur des rapports
marchands, sur la domination patriarcale et économique dans la société que
nous voulons.

L'expression **le plus vieux métier du monde** est un des outils de propagande
de cette domination.

La prostitution ne peut être un **travail**, elle est et restera un fléau au
même titre que la misère qui l'engendre. La dénomination de **travailleurs et
travailleuses du sexe** est donc une posture pro-légalisation à laquelle il
nous est impossible d'adhérer.

La CNT lutte quotidiennement pour la défense des travailleurs et travailleuses
mais agit aussi pour un autre futur, pour une société égalitaire et
autogestionnaire, aux relations entre individus sans domination et sans
exploitation.

Dans cet avenir, la prostitution même **autogérée et choisie** n'a pas sa place.


Contre-motion
=============

Si la CNT peut participer avec des prostituéEs auto organiséEs à leur lutte
pour des droits sociaux universels ou contre les lois sécuritaires, elle ne
peut en aucun cas cautionner les luttes pour des droits relatifs au **métier**
ou au statut de **travailleurs et travailleuses du sexe**.

Travailler avec le STRASS peut être perçu comme une approbation de la totalité
de leurs revendications.

La CNT, organisation libertaire, qui lutte déjà pour l'abolition du salariat,
doit se déclarer clairement pour l'abolition de la prostitution.



Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°7 2014 <motion_17_2014_ste75>`

       :ref:`Interco73 <interco73>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
