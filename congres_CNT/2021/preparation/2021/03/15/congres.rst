

.. _cnt_2021_03_15_congres:

=============================================================
Lundi 15 mars 2021  **Congrès**
=============================================================

- :ref:`reunion_bc_ca_2021_03_13`

::

    ---------------------------- Message original ----------------------------
    Objet:   [XXXXX] CONGRES
    De:      "SECRETARIAT CONF." <cnt@cnt-f.org>
    Date:    Lun 15 mars 2021 10:13
    --------------------------------------------------------------------------



Le Bureau Confédéral et la commission administrative se sont réunis le
samedi 13 mars 2021. Prenant en compte les réponses des syndicats le bureau
confédéral provisoire et la commission administrative en présence de
mandaté de l'Interpro 21 ont arrêté les dates du congrès confédéral qui
se déroulera sur 3 jours du vendredi 25 au dimanche 27 juin 2021 à
Dijon.

Il s'ensuit les dates suivantes:

Les syndicats ont jusqu'au 23 mars 2021 pour déposer des motions non
statutaires

Le secrétariat confédéral envoie le 1er cahier de motion, fourni
par la commission de préparation, aux syndicats le jeudi 25 mars 2021.

Les syndicats ont jusqu'au dimanche 23 mai 2021 pour déposer des amendements
et des contre-motions

Le secrétariat confédéral enverra le cahier final des motions et contre
motions éventuelles le mardi 25 mai 2021.

Pour les motions reçues hors délais celles-ci seront soumises à
l'approbation des syndicats lors de l'ouverture du congrès.

Par ailleurs pour faciliter la tenue du congrès, il serait préférable
que les syndicats s'assurent d'être à jour des cotisations confédérales
(cotisations syndicales, Combat syndicaliste, péréquation du précédent
congrès, prêts...), Fédérales, Régionales, Locales.

Un Compte Rendu de cette réunion vous sera communiqué rapidement.
Vous constaterez que certains mandats ne sont pas pourvus, d'autres
seront à reprendre ou renforcer aussi nous vous demandons d'y réfléchir
d'ores et déjà.

Bonne préparation du Congrès.

Salutations A.S.S.R.
Serge secrétaire confédéral mandaté provisoire
