
.. index::
   pair: Motion 5 ; 2014

.. _motion_5_2014_sinr44:

================================================================================
Motion n°5 2014 : Modification des règles du quorum durant le congrès (SINR 44)
================================================================================



Motion (non applicable pour le 33ème Congrès confédéral)
========================================================

Pour que le Congrès confédéral puisse se poursuivre, il est nécessaire
que 50% minimum du nombre de syndicats présents physiquement à
l'ouverture du Congrès soient encore présents

Les entrées et sorties définitives sont décomptées.

Amendements
============

.. seealso::

   - :ref:`amendement_motion_5_2014_stics13`
   - :ref:`amendement_motions_4_5_2014_interco48`


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°5 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
