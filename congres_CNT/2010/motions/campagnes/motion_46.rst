

.. _motion_46_2010:


========================================================
Motion n°46 2010 : L’augmentation des salaires, ETPIC 94
========================================================


Argumentaire
=============

De notre point de vue, la CNT se doit de défendre l’augmentation des salaires pourquoi ?

- C’est un élément de la répartition des richesses. Plus pour les salaires c’est moins
  pour les actionnaires. C’est un élément qui favorise l’alimentation de toutes les caisses
  de solidarité sociale (maladie, vieillesse…)
- C’est un élément qui permet aux retraités de partir avec une meilleure retraite.

Cette lutte devra aussi s’accompagner de tout ce qui disqualifie le salaire et empêche
les augmentations : les aides, les exonérations (de cotisation pour les employeurs
de salariés au Smic), les défiscalisations d’heures supplémentaires par exemple.

Et bien sûr, favoriser les augmentations pour les plus bas salaires en revendiquant
de l’inversement proportionnel, ou des augmentations fixes. Nous faisons confiance
aux syndicats de la CNT pour étoffer encore plus l’argumentaire ci-dessus.

Motion
======

Nous proposons donc que les syndicats de la CNT s’engagent sur cette revendication de façon soutenue pour une
période mini de 1 an et qu’elle soit ponctuée par le 1er Mai 2011 sur cette thématique.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°46          |            |          |            |                           |
| ETPIC 94             |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
