

.. index::
   pair: motion 2 ; Création d’une commission juridique et de formation (2010)

.. _motion_2_2010:

=======================================================================================================
Motion n°2 2010 : Création d’une commission juridique et de formation, Culture Spectacle RP [reportée]
=======================================================================================================



Argumentaire
============

Pour la :term:`C.N.T`, le moyen d'action privilégié dans la lutte des classes
est l'action directe, et l'action juridique, parfois nécessaire, reste optionnelle.

Cependant le développement actuel de l’activité syndicale de la :term:`C.N.T` fait
apparaître de plus en plus la nécessité de formation sur les droits et pratiques
du syndicalisme d’entreprise. En effet, certains camarades ne savent pas quels sont
les rôles des RSS, des DP et autres élus des divers comités CE, CHSCT, etc.

Par ailleurs, au niveau juridique, il est parfois prioritairement fait appel
aux avocats, alors que d’autres camarades ont une connaissance des textes juridiques
qui permettrait d’y remédier.

Face à cette réalité déjà ancienne, des motions sont régulièrement votées sur la
formation ou sur des commissions juridiques chargées de mutualiser les connaissances
et expériences. Cependant, malgré ces motions, le problème perdure.

Pour ce qui est des formations, elles sont dispensées en dehors des heures de
travail sur le temps de loisir ou de militance active. Elles apparaissent parfois
trop généralistes (histoire de l’anarcho-syndicalisme) ou trop techniques
(rédactions de tracts, etc.), en tous cas ne répondant pas aux questions que
peuvent se poser les militants sur le terrain.

Il est souvent difficile d’anticiper les problèmes que l’on va rencontrer.
Parfois les élus ne savent pas qu’ils peuvent profiter de certaines formations
syndicales payées par l’employeur sur leur temps de travail.

Pour ce qui est de la mutualisation des expériences juridiques, même si cela
peut avoir l’avantage de nous confronter à des cas concrets, qui resteraient
abstraits enfermés dans un livre de code, elle n’apporte pas de solutions à
celui qui n’a pas d’expérience du droit.

Rien ne vaut le travail en commun et la discussion d’un cas concret avec
quelqu’un qui sait de quoi il en ressort.

De la même façon, des camarades montant des sections syndicales dans des
entreprises ayant une faible histoire syndicale ont parfois besoin de l’aide
de la Confédération : conseils, soutien, formation adéquate.

Aujourd’hui, ce rôle est souvent confié à la bonne volonté d’un camarade et à
ses disponibilités.

Cette personnalisation du lien à la Confédération peut s’avérer problématique
quand la section n’a plus comme interlocuteur qu’un camarade et non plus une
structure collective.

Nous proposons donc que soit créée, au niveau confédéral, une commission
chargée de palier à ces différents problèmes. Elle serait composée de
camarades disposés à intervenir concrètement, et non pas seulement à
recueillir de l’information ou à théoriser.

Cela signifie qu’en cas de problème juridique, les syndicats puissent
contacter, préalablement à la consultation d’un avocat, cette structure pour
avoir un avis.

Que s’il s’avère que les offices d’un avocat ne sont pas nécessaires, ils
pourraient travailler avec le camarade à la résolution du problème.
Ils ne travailleraient que pour les adhérents de la :term:`C.N.T` et non
comme un cabinet de conseil gratuit pour monter des dossiers prud’hommes
pour l’extérieur.

De même, en cas de besoin de soutien à la création ou à la formation des
camarades montant des sections syndicales, ils pourraient se déplacer pour
les appuyer.

Comme cette structure serait confédérale, il est indispensable qu’elle
regroupe un nombre important de camarades ayant des expériences diverses.

Elle devrait être représentée dans toutes les régions, même si elle peut
faire appel à des camarades d’autres régions. Elle devra être budgétisée,
pour permettre les déplacements et être coordonnée en interne par des
mandatés confédéraux. Son but est de permettre l’autonomisation des
syndicats et sections et l’optimisation des dépenses juridiques.

Motion
======

La :term:`C.N.T` décide de la création d’une commission juridique et de formation
chargée d’intervenir en soutien aux besoins de cet ordre émanant des syndicats.

Elle est dotée:

- d’un ou de plusieurs mandatés par le congrès,
- d’un budget,
- d’un  ou plusieurs numéros de téléphone
- et d’une adresse mail.

Elle se compose de tous les adhérents de la :term:`C.N.T` qui désirent
mutualiser leur savoir et leur expérience.


Discussion
==========

**CCS RP**
    motion courte qui matérialise cette commission juridique.

**SUB35**
	Pas de quitus sur la commission juridique sortante, du coup on en n'a pas
	parlé. On a tenté de développer certains contacts. On a mis en place des
	petites formations en droit du travail, histoire de sortir une brochure.

	J'ai pu répondre à Pascal du Gard sur des demandes juridiques sur le droit
	du travail mais peu de demande à part lui.

**SIPM**
	contact se fait entre les syndicats par mutualisation. Garder les procédures
	en tête. Il ne s'agit pas de monter un cabinet de juristes à la CNT.
	On est aussi pour les contre motion donc prêts à retirer notre motion si une
	contre motion est adoptée


Vote
====

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°2           |            |          |            |                           |
| Culture Spectacle RP |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+



.. seealso::

   - :ref:`contre_motion_2_3_2010_ste93`
   - :ref:`contre_motion_2_3_2010_etpreci35`
   - :ref:`amendement_motions_2_3_2010_ste93`
