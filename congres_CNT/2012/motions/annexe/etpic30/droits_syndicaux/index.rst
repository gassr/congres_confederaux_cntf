

.. index::
   pair: Droits Syndicaux; Recours aux permanents


==========================================
Droits syndicaux et recours aux permanents
==========================================


Panorama sémantique, juridique, et historique.

.. toctree::
   :maxdepth: 3

   1_permanents
   2_representants_elus
   3_ASA
