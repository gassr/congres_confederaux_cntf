

.. _motions_adoptéees_congres_CNT_1996_lyon:

==============================================
Motions adoptées au XXVe Congrès CNT 1996 Lyon
==============================================

.. toctree::
   :maxdepth: 2

   strategie_syndicale
   international
   antifascisme
   droits_des_femmes
   ecologie
   precarite_et_reduction_temps_de_travail
   relations_medias
   bureau_confederal
   outils_confederaux
   bulletin_interieur
   presse_confederale
   statuts_confederaux
   tresorerie_confederale
   crise_et_scission
