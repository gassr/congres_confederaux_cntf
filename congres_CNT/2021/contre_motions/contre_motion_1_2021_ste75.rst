
.. _contre_motion_1_2021_ste75:

=========================================================================================================================
Contre-motion N°2 à la motion n°1 2021 **Ajout aux statuts des luttes antipatriarcales et antiracistes** par **STE75**
=========================================================================================================================

- :ref:`motion_1_2021_ste33`
- :ref:`motion_1_2021_liens_anciennes_motions`

Autres motions STE75

    - :ref:`paris:motions_ste75`


Argumentaire
================


Contre-motion
===============

Dans un esprit consensuel, nous proposons cette contre-motion::

    La CNT est une organisation syndicale de lutte des classes qui, parce
    qu'elle est pour l'égalité des travailleurs et des travailleuses,
    lutte contre le sexisme et le racisme.

Vote
====

+-------------------------------+------------+----------+------------+---------------------------+
| Nom                           | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+===============================+============+==========+============+===========================+
| Contre motion à la motion n°1 |            |          |            |                           |
| STE75                         |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
| Décision du congrès           |            |          |            |                           |
|                               |            |          |            |                           |
+-------------------------------+------------+----------+------------+---------------------------+
