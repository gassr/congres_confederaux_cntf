
.. _amendement_motion_10_2014_sse31:

================================================
Amendement à la motion n°10 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_10_2014_ste49`




Argumentaire
=============

Pour ne pas bloquer par faute de volontaire, nous voudrions rajouter
uniquement « si possible » à cette motion

Amendement
===========

Il est demandé au Congrès la création, si possible, d'une commission de
travail à l'échelle confédérale à qui il est confié les mandat suivants : ...


Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°10 2014 <motion_10_2014_ste49>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
