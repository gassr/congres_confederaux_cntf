
.. index::
   pair: Motions; Stratégie syndicale 2016
   pair: Stratégie syndicale ; 2016


.. _motions_strategie_syndicale_2016:

======================================
Motions stratégie syndicale 2016
======================================

.. toctree::
   :maxdepth: 4

   motion_1_2016_ste93
   motion_2_2016_etpic30
   motion_3_2016_sante_social_lorraine
