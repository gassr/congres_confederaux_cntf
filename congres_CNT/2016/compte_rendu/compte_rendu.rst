
.. index::
   pair: Compte rendu; Congrès CNT (Montreuil)


.. _releve_decisions_xxxiv_congres:

===============================================================
Compte rendu du XXXIVe Congrès CNT 2016
===============================================================


:download:`Télécharger le compte rendu_34e congrès confederal de Montreuil 2016 <Compte_Rendu_34eme_Congres_confederal_Montreuil.pdf>`
