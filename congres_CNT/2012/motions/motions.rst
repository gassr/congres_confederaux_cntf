
.. index::
   pair: Motions ; 2012

.. _congres_CNT_2012_motions:

========================
Motions Congrès CNT 2012
========================

.. toctree::
   :maxdepth: 2

   fonctionnement_congres/index
   question_circonstancielle/index
   strategie_syndicale/index
   campagnes/index
   fonctionnement_organique/fonctionnement_organique
   outils_et_developpement/index
   annexe/index
   motions_reprises_de_2010
