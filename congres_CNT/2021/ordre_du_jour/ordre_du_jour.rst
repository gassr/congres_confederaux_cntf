
.. _odj_xxxv_congres:

=================================================
Ordre du jour xxxve Congrès CNT 2021 de Dijon
=================================================

- :ref:`congres_statuts_CNT`
- :ref:`fonctionnement_congres_confederal`


Proposition d'Ordre du jour du XXXVe Congrès confédéral


Jeudi 24 juin 2021
===================

- Accueil et enregistrement des mandats auprès de la commission de
  contrôle des mandats, à partir de 14 h, aux Tanneries de Dijon
- Mise en place de la commission de contrôle de la Trésorerie confédérale

Vendredi 25 juin 2021
======================

- Ouverture du congrès à 10h
- Mise en place de la présidence du congrès et adoption de l'ordre du jour
- L'ordre des motions proposées au débat et au vote des syndicats au
  début du congrès reprendra les demandes exprimées par certains syndicats,
  à savoir commencer par :

  - les :ref:`motion_18_2021_ste93` et :ref:`motion_19_2021_ste93`
  - la :ref:`motion_25_2021_ptt_centre`
  - la :ref:`motion_13_2021_cnt09`
  - la :ref:`motion_24_2021_ste33_cnt42`
  - la :ref:`motion_12_2021_sinr44`
  - la :ref:`motion_1_2021_ste33`
  - la :ref:`motion_8_2021_etpics94`
  - la :ref:`motion_10_2021_etpic30`
  - les :ref:`motion_14_2021_syndicats_42` et  :ref:`motion_15_2021_syndicats_42`
  - la :ref:`motion_23_2021_stt59_62`

suivies du reste des motions dans l’ordre.

**C’est le Congrès qui décidera au final de l’ordre des motions en début
de congrès.**

Il y aura un point d’ordre au milieu du congrès pour réajuster éventuellement
l’ordre des motions.

- Rapports des mandaté·e·s confédéraux·ales
- Rapport de mandat des commissions confédérales

Samedi 26 juin 2021
===================

Séances plénières de 9h à 12h, puis de 13h30 à 17H.

Dimanche 27 juin 2021
=======================

- Heure de début à définir selon l’état d’avancement du congrès
- Mandatement des nouveaux·elles mandaté·e·s confédéraux·ales
- Prévoir un temps pour l’intervention des organisations internationales
  invitées, si elles le désirent
- Clôture du congrès prévue en fin d’après-midi (17h)
