

.. _conclusion_ssct_2017_03_05:

====================================
IV) Conclusions de notre syndicat
====================================




1. Sur le fond de l'accusation et l'exclusion éventuelle de notre adhérent pour viol
=======================================================================================

**Considérant** tout d'abord que des éléments du récit de la victime supposée
concernant le déroulé semblent être contredit par les témoignages
factuels que nous avons pu recueillir,

**Considérant** les propos de la victime supposée qui explique que son
souvenir des faits supposés repose sur les déclarations que l'accusé
lui à lui-même formulé,

**Considérant** que les éléments à charge présentés dans le rapport de
la commission non mixte d'AL et qui concernent un changement de comportement
possible durant le séjour au Roucous pouvant révéler le fait qu'elle ait
subi un viol, ne semblent pas concordants avec les différents éléments et
témoignages factuels dont nous avons pu disposer,

**Considérant** que les éléments à charge présentés dans le rapport de
la commission non mixte d'AL et qui décrivent une attitude déplacée de
la part de l'accusé durant toute la semaine au Roucous, sont nuancés par
les témoins factuels qui décrivent une relation difficile dans les deux sens,
laquelle n'a pas empêché les deux protagonistes de partager de nombreux
moments festifs,

**Considérant** que les consommations ne drogues multiples, tout au long
de la semaine, par la victime supposée comme par l'accusé sont très
peu propices à crédibiliser les souvenirs et les discours des un·es des
autres, et peuvent conduire à des interprétations extrêmement variées
des comportements de chacun·e,

**Considérant** que les éléments de défense apportés par l'accusé, avant
sa prise de connaissance des faits reprochés, et après avoir été confrontés
aux éléments d'accusation sont restés concordants et sont accrédités par
les témoins factuels que nous avons auditionnés,

**Considérant** que l'accusé lui-même a fait preuve de la transparence
la plus totale tout au long de cette procédure, en fournissant notamment
à son syndicat d'appartenance, l'ensemble des éléments à charge contre
lui-même et qu'il a pu obtenir,

**Considérant** que l'accusé, dès les premières formulations de l'accusation
par la victime s'est déclaré devant toutes les organisations auxquelles
il adhère qu'il était personnellement favorable au dépôt de plainte de
la victime supposée,

**Considérant** que la victime supposée disposait de potentielles raisons
de formuler une fausse accusation,

**Considérant** le rôle joué par Alternative Libertaire, qui a visiblement
préféré attaquer publiquement la CNT plutôt que de lui fournir des
éléments objectifs qui lui auraient permis d'analyser objectivement l'accusation,

**Considérant** que les pressions équivoques qui ont été faites par la
commission non mixte d'AL sur les témoins que nous avons auditionnés
jettent un doute raisonnable sur la crédibilité des témoignages tronqués
et utilisés à charge dans le rapport de la commission,

**Considérant** que la chronologie de cette accusation révèle finalement
des liens multiples entre la victime supposée et celles et ceux qui ont
relayé·es cette accusation et tenté·es par de multiples moyens de nous
faire prendre une décision sous pression ; liens qui nous laissent penser
que la fragilité de cette femme a potentiellement pu être exploitée par
des intérêts politiques,

Le syndicat SSCT Lorraine a estimé que:

- Une analyse objective de cette affaire, suite à une procédure réalisé
  avec équité à charge comme à décharge, ne permet pas de tirer de
  conclusions formelles, ni sur la véracité de l'accusation portée,
  ni sur l'innocence de notre adhérent dans cette affaire.
- Que les seuls éléments à charge qui ont été recueillis à l'encontre
  de Fouad et qui demeurent cohérent à l'épreuve des faits sont trop
  peu nombreux et demeurent trop assujettis au doute pour justifier
  d'une exclusion de notre adhérent.


2. Appui au dépôt de plainte de la victime supposée
======================================================

Notre syndicat, après avoir mené cette procédure s'est déclaré incompétent
pour établir quelconque vérité.

Malheureusement, au regard des éléments dont nous disposions, et en accord
avec des principes de justice qui sont pour nous inaliénables, nous
considérons que notre syndicat n'a pu rendre d'autre décision et qu'il
ne peut de fait disposer de moyens d'investigations plus efficaces que
ceux dont il a déjà usés.

Aussi nous avons bien conscience que notre décision concernant la non
exclusion de Fouad constituerait une injustice terrible pour une femme
victime de viol si celui-ci était coupable.

Mais notre syndicat n'a pas aujourd'hui les moyens de se substituer à un
travail de police judiciaire ou d'un magistrat.

Pour ces raisons, notre syndicat, tel qu'il l'a déjà dit, se déclare
favorable au fait que la victime supposée dépose une plainte à l'encontre
de Fouad.

Bien conscients des nombreuses difficultés qui sont celles rencontrées
par une femme lorsqu'elle porte plainte, notre syndicat pense qu'il est
du devoir de la CNT, en tant qu'organisation féministe et antisexiste
que de proposer expressément à E. de bénéficier de son accompagnement
dans le cadre d'une telle démarche, si elle estime que celle-ci peut
lui permettre de lui rendre justice.

Nous rappelons que si E. faisait le choix de déposer plainte à l'encontre
de Fouad, notre syndicat tiendra à disposition de la justice et de E.
l'ensemble des éléments dont il dispose.

Nous rappelons à nouveau que Fouad s'est déclaré lui-même favorable à
ce dépôt de plainte et a déclaré se tenir à disposition de la justice
dans ce cadre éventuel.

3. Perspectives en interne de la CNT
======================================

Tout d'abord, il est évident que cette accusation de viol à l'encontre
de l'un de nos adhérents à suscité l'éclosion de vives polémiques au
sein de la Confédération, autour des notions d'antipatriarcat, de féminisme,
d'antisexisme, de justice.

**Ces polémiques semblent d'ailleurs, pour certaines d'entre elles avoir
été suscitées par l'influence d'une organisation politique, en l'occurrence
Alternative Libertaire, en lien avec certaines structures et adhérent·es
de la CNT**.

Aussi, un débat serein autour de ces différentes notions nous apparait
aujourd'hui comme une nécessité absolue pour préserver l'unité de la
Confédération.

Ensuite, tel que l'on proposé différents syndicats, ainsi que la commission
antisexiste de la CNT, il semble nécessaire que la CNT puisse se doter
d'un protocole claire et réfléchi pour traiter de lafaçon la plus juste
possible ce type d'accusation en interne.

S'il ne fait aucun doute que notre syndicat voit sans doute un tel
protocole de manière bien différente que les propositions qui ont déjà
été formulées par certains syndicats, nous rejoignons volontiers l'idée
d'une discussion collective à ce sujet.

**Par ailleurs, les pratiques de certaines structures de la CNT, telles
que la commission antisexiste de la CNT, les agissements de syndicats
qui remettent en cause l'autonomie d'autres syndicats ou du Congrès,
les agissements de certains syndicats et adhérent·es qui n'ont pas hésité
à diffamer publiquement notre adhérent avant même quelque conclusion
de notre part sur le fond de cette affaire, posent des questions
fondamentales à notre fonctionnement Confédéral.**

Finalement, nous ne doutons pas que plusieurs syndicats de la CNT, suite
au rendu de notre décision, décideront de proposer l'exclusion de notre
syndicat de la Confédération.

Il semble d'ailleurs que cela démange certain·es depuis de nombreuses
semaines.

Bien que nous le regrettions, il apparait inévitable que notre syndicat
joue prochainement sur sa tête les problématiques politiques qui déchirent
les organisations libertaires, mais notre syndicat est prêt à assumer
cet état de fait.

En effet, non seulement nous refusons de nous retrancher derrière une
potentielle impossibilité pour ces syndicats qui rêvent de proposer
notre exclusion de la CNT à pouvoir le faire en raison de leur éventuelle
position minoritaire au sein de leur UR qui précédera le prochain CCN,
mais en plus nous refusons de vivre pendant encore 20 mois (c'est à dire
jusqu'au prochain Congrès Confédéral) les pressions et les injures que
nous vivons depuis plus de 3 mois et qui entravent toutes activités
syndicales de notre part.

Pour toutes ces raisons, notre syndicat se déclare favorable à la tenue
prochaine d'un Congrès Confédéral extraordinaire, seul moyen pour que
la CNT puisse trancher sereinement et formellement les débats qui la
sclérosent aujourd'hui ainsi que l'avenir de notre syndicat en son sein.

.. _conclusion_al:

4. Conclusion et relations avec Alternative Libertaire
========================================================

- :ref:`amendement_motion_24_2021_sest_lorraine`
- :ref:`role_de_linterpro_31_fouad`

Compte tenu des tentatives équivoques d'Alternative Libertaire de mettre
la CNT sous la tutelle de ses propres décisions, compte tenu des
tentatives de cette organisation politique pour **discréditer notre
Confédération sur le plan national**, compte tenu des **procédés odieux**
employés par AL dans le cadre de l'instruction de cette affaire, compte
**tenu du choix qui a été fait par Alternative Libertaire d’engager une
communication publique laquelle relève de pratiques moyenâgeuses** qui nous
horrifient, et cela **quel que soit la réalité de la culpabilité ou non de
Fouad**, notre syndicat décide :

- De communiquer publiquement notre décision concernant Fouad et de dénoncer
  publiquement les agissements d'Alternative Libertaire.
  Nous précisons que sans communication publique d'AL, nous n'aurions
  sans doute pas pris·es cette décision.
  Mais puisqu'AL a fait le choix de nous mettre devant le fait accompli
  et compte tenu des conséquences dramatiques qu'ont eu leur injonction
  sur l'image de la CNT aux yeux du grand public, nous n'avons pas
  d'autres choix.
- De rompre toutes relations inter organisations avec Alternative Libertaire.
  Si nous respectons l'intégrité dont ont fait preuve les adhérent·es
  du CAL Moselle, nous regrettons que, de fait, nous n'aurons plus de
  relation avec AL au niveau local, dans la mesure où ces adhérent·es,
  affligé·es par les agissements de leur organisation, ont décidé de
  quitter AL.
- De faire des propositions concrètes à la Confédération, par le biais
  de :ref:`motions <amendement_motion_24_2021_sest_lorraine>` qui seront
  soumises au prochain Congrès extraordinaire, une évolution des relations
  entre la CNT et AL sur le plan national et cela afin de protéger la confédération.
