.. index::
   pair: Motion 9 ; 2021

.. _motion_9_2021_interco69:

================================================================================================================================================
Motion **n°9** 2021: **Lutte contre la Précarité** par **Interco 69**
================================================================================================================================================

Autres motions Interco69

    - :ref:`rhone_alpes:interco69_motions_congres`


Argumentaire
=============

Les nouvelles formes de travail, intérim, contrat court, recours à
l’auto-entreprenariat en lieu et place de la contractualisation salariale
nous impose aussi de repenser nos moyens d’actions et de protection des personnes
les plus précaires sur nos lieux de travail et de vie.

Récemment, l’émergence de l’auto-entreprenariat et le recours massif à cette
nouvelle forme de contrat de travail fait sauter toutes les protections qui
s’appliquent aux salarié·es :

- des protections des travailleur·ses inexistantes.
  Dans le cas des coursiers à vélo, les plateformes de livraison se dédouanent
  complètement sur leurs coursier·ères si illes se retrouvent accidenté·e·s ou
  sont responsables d’un accident
- le retour du travail à la tâche, avec des plages de travail énormes si les
  travailleur·ses veulent un revenu décent
- une rémunération de la prestation, notamment dans l’accueil périscolaire de
  certaines municipalités, équivalente à celle reçu en tant que salarié·e pour
  ces mêmes activités sans tenir compte du fait que l’auto-entrepreneur·se doit
  y provisionner ses charges, se retrouvant effectivement plus pauvre qu’avant,
  avec la même charge de travail,Le patron ayant lui réussi à diminuer la
  rémunération du travail.

Pour cela, il nous semble important d’intégrer dans la motion de positionnement
de la CNT une référence à cette nouvelle forme de travail précaire qui va
au-delà de toutes les espérances du patronat, nous faisant de fait revenir à la
rémunération à la tâche contre laquelle le mouvement ouvrier s’est battu.

Motion
========

Dans le chapitre **Lutte contre la Précarité**, modification de l’axe de
positionnement de la CNT dans la liste initiale, actuellement::

   Se mobiliser dans la lutte contre la précarité et l’intérim en particulier
   dans le secteur public**,

par la proposition suivante::

    Se mobiliser dans la lutte contre la précarité, l’intérim en particulier dans
    le secteur public, et l’expansion d'un salariat déguisé initié par l'économie
    numérique et la libéralisation dans tous les secteurs (privé/public).

Amendements
===========

- :ref:`amendement_motion_9_2021_tasra`
- :ref:`amendement_motion_9_2021_sest_lorraine`
- :ref:`amendement_motion_9_2021_interpro31`
- :ref:`amendement_motion_9_2021_stics72`
- :ref:`amendement_motion_9_2021_stp67`

Votes
======

+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°9 2021 Interco69   |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
