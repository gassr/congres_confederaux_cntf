

.. _motion_5_2008_etpic94_ptt35:

=======================================================================
Motion n°5 2008 : Refonte des statuts. Proposée par PTT 35 et ETPIC 94
=======================================================================


**Présentation par PTT 35**
	seconde « commission statuts »...travail de plusieurs années. Nouveau
	document de travail, ré-explication plus conséquente...
	Ces statuts, n'ont pas été inventés par la commission, ce sont des
	décisions issues des Congrès successifs.
	Ces statuts n'ont pas à être figés non plus. A partir de cela, on voit à
	peu près comment ça fonctionne et rien n'empêche, au prochain Congrès,
	qu'il y ait des modifications de statuts.
	Il est à présent nécessaire d’être en conformité vis-à-vis de nous même,
	ou vis à vis des institutions.

**Interco 91**
	Il est difficile de remodeler ou re-lifter les statuts de 1946. N’aurait-il
	pas fallu en réécrire de nouveaux ? Nous sommes surpris des modalités de
	vote proposées.

**SCIAL RP**
	Nous sommes de même surpris que les modalités de vote aient été définies
	en CCN. La relation travailleur/producteur est à débattre.

**ETPIC 94 (membre de la commission)**
	La Commission n’a rien créée (sauf la nouvelle proposition de CA, cf.
	problématique dans le dossier)

**ESS 34**
	certainement des choses à redire. Dans deux ans, possibilité de déposer
	motions pour amender les statuts et les améliorer. Bonne Base de travail
	pour évoluer, avancer et débattre sereinement.
	Ne pas s'accrocher sur des virgules.

**EPTIC 30**
	Approbation de ce travail, cheminement long et difficile. Accent
	l’article n°26 extrêmement positif dans les nouveaux statuts: définition
	de l'autonomie des syndicats. Définition positive là ou les statuts pose
	souvent un cadre contraignant (et nécessaire).

**Interco 54**
	cela emmène un interdit.

**ESS 34**
	maintenant on va pouvoir exister et fonctionner réellement. Ils sont mieux,
	car conformes avec les décisions que la CNT a prise en Congrès.
	Essentiel qu'il y ait une concordance avec ce qu'on a voté.

**Santé Social RP**
	Question par rapport à l'autonomie sur «sans aller publiquement à
	l'encontre de ces décisions»

**Interco 57**
	on tient à soulever le fait de sortir du mythe des statuts magiques de 1946.
	On vit en 2008 plus en 1946...

**ETPIC 30**
	Réponse à santé social RP : Exemple de la participation aux élections des
	CE, aux positions d’orientations de Congrès (antifascisme, etc.)...
	La CNT a des positions sur ces sujets. L’exemple de l’antifascisme est
	grossier, mais un syndicat qui se dirait publiquement fasciste se
	positionnerait contre les positions de la CNT. Idem s’il participe à des
	élections de CE si la stratégie confédérale prévoit le contraire.
	Il y a rupture avec les orientations confédérales.
	Il était important de faire le lien avec nos statuts. C’est la règle
	collective. Rien n’empêche les débats contradictoires internes. Le pacte
	associatif est un cadre collectif contraignant aussi. Attire l’attention
	sur modification sur l’article premier...anciens statuts: « la CNT
	syndique les salariés », nouveau: « la CNT syndique les travailleurs ».
	Changement de champs de syndicalisation.

**Interco 54**
	le parallèle avec un syndicat fasciste, est faux

**Santé social R P**
	la réponse qui a été faite ne me convient pas. Un syndicat peut il
	s'exprimer sur le fait d'être d'accord ou pas...


Vote de la motion n°5
=====================

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 33         | 9        | 5          | 2                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+



**Pour : 33**
	éduc 66, STE 92, éduc 57, Interco 07,
	ETPIC 94, Interco 76, Interco 87,
	Interco 86, Interco 48, Interco 29,
	ETPIC 30, Interco 31, santé social RP,
	SII RP, CS RP, SIPM RP, PTT 66, ESS
	34, éduc 91, Interco 91, SUB 35, SSE
	31, Interco 34, Interco 09, Interco 38,
	éduc 38, Interco 21, éduc 13, SUTE
	69, éduc 35, PTT 35, ETPRECI 35,
	SSEC 59

**Contre: 9**
	STIS 59, Interco 32, Interco 33, STTE
	33, éduc 33, éduc 42, Interco 42,
	santé social 42, PTT 69

**Abstention: 5**
	Interco 45, STE 93, SCIAL RP, STE 93,
	STTE 34

**NPPV: 2**
	SUB 69, Interco 54

**adoptée**




MOTION adoptée (à 67%) > Adoption des nouveaux statuts tels que proposés par
la 2ème commission confédérale.


Second vote
===========

Second vote sur l’intégration de la Commission Administrative telle que
définie par la motion du STP Aquitaine

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 15         | 20       | 10         | 4                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+





**Pour : 15**
	Interco 09, Interpro 34, Interco 42,
	Interco 76, Interco 87, Interco 29,
	Interco 33, Interco 31, Interco 32,
	Santé social 42, éduc 42, éduc 33, PTT
	69, SII RP, STTE 33

**Contre: 20**
	ESS 34, éduc 38, éduc 35, éduc 91,
	éduc 66, SSE 31, santé social RP, SUB
	35, Interco 38, ETPRECI 35, Interco
	91, SSEC 59, Interco 07, ETPIC 94,
	ETPIC 30, PTT 35, SCIAL RP, STIS 59,
	CS RP, SIPM RP

**Abstention: 10**
	SUTE 69, éduc 13, STE 93, éduc 57,
	éduc 92, Interco 21, Interco 86,
	Interco 45, STTE 34, Santé social 86

**NPPV: 4**
	SUB 69, PTT 66, Interco 48, Interco 54

**MOTION non retenue**
