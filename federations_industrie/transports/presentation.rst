
.. _presentation_federation_CNT_transports:

================================================
Présentation de la Fédération CNT des transports
================================================

::

	Fédération des Travailleurs des Transports,de la Logistique et des Activités auxiliaires
	Confederation Nationale du Travail
	6, rue d'Arnal,30000 Nîmes
	federation.transports@cnt-f.org


La CNT se dote d'une nouvelle arme syndicale ! La Fédération des
Travailleurs  des Transports, de la Logistique et des activités auxiliaires.

Elle a pour but de réactiver l'esprit combatif des travailleurs des transports,
trop longtemps assommé par la classe dominante, le patronat et ses sbires.

Cette Fédération se donne comme objectif, de regrouper les travailleurs
des  transports et de la logistique afin de réunir les différentes
luttes qui  permettront de reconquérir nos acquis sociaux et surtout
d'en gagner  de nouveaux.

La **FTTLA** de la CNT englobe tous les secteurs des transports et de la
logistique, qu'ils soient de marchandises ou de personnes, quels que soient
leurs statuts, public ou privé, quels que soient leur mode, aérien,
maritime,  fluvial, ferroviaire, routier.....etc.

La CNT un syndicalisme autogestionnaire ?

- Pour le contrôle des négociations par la base
- Pour des assemblées générales souveraines
- Pour le refus des permanents syndicaux
- Pour le refus des hiérarchies salariales
- Pour des délégué(e)s élu(e)s et révocables
- Pour la défense de tous les travailleurs, quelle que soit leur nationalité

Nous ne sommes pas des chiens ! Il est grand temps qu'un syndicalisme
de lutte de classe se développe dans notre secteur d'activité et
au delà !

**Face à ton exploiteur, ne reste pas seul, syndique toi !**
