
.. _motion_33_2012:

===============================================================
Motion n°33 2012 : Secrétariat juridique confédéral (SIPM-RP)
===============================================================
  
.. seealso::

   - :ref:`sipm_rp_pub`
   - :ref:`motions_sipm_rp`





Motion reprise de SIPM-RP , 2010
=====================================

- :ref:`motion_3_2010`


Argumentaire
============

Il existe de plus en plus de savoir-faire juridique au sein de la confédération,
du fait de l’expérience syndicale des uns et des autres, mais aucune mise en
commun n’est réellement faite, ni aucune formation des camarades qui le
souhaiteraient.

Le projet d’émancipation collective et individuelle porté par la CNT, ainsi
que le développement de l’autogestion comme mode de fonctionnement, nécessitent
que chacun, individu ou syndicat, soit de moins en moins obligé d’avoir
recours à des spécialistes.

Une mutualisation des connaissances et la mise en place de formations
juridiques sont nécessaires pour la CNT.

Motion
=======

Il est créé au sein du bureau confédéral de la CNT un secrétariat juridique.

Il est composé de deux mandatés, un en droit privé, l’autre en droit public.

Ces deux mandatés sont membres de la commission administrative confédérale et
s’entourent de tous les camarades qui le souhaitent, via une liste Internet
et des réunions.

Ce secrétariat juridique n’a pas vocation à monter des dossiers juridiques ni
à régler des problèmes individuels, mais à conseiller les adhérents de la CNT
lorsqu’il le peut, à les mettre en relation avec d’autres adhérents ayant
déjà une expérience sur la question soulevée.

Le secrétariat juridique a pour vocation première de mettre en place des
formations en droit syndical et doit du travail, public ou privé, à la demande
des syndicats, unions ou fédérations qui le souhaitent.

Le secrétariat juridique publiera également des brochures de formation
à bas coût.



+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°33          |            |          |            |                           |
| SIPM-RP              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
| Décision du congrès  |            |          |            |                           |
|                      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
