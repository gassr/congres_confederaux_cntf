

.. _contre_motion_19_2010_ste93:

===========================================
Contre-motion à la motion N°19 (STE93)
===========================================


En partant du constat de la difficulté globale à trouver des mandaté-e-s,
le STE93 souhaite l'ajout suivant::

    En cas de carence, les candidatures spontanées devront être validées par
    le syndicat d'appartenance dans les plus brefs délais.


+------------------------------------+------------+----------+------------+---------------------------+
| Nom                                | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+====================================+============+==========+============+===========================+
| Contre motion à la motions n°19    |            |          |            |                           |
| STE 93                             |            |          |            |                           |
+------------------------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`motion_19_2010`
