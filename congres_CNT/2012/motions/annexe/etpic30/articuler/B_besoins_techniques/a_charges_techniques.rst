



=============================================
I.B.a – Les charges techniques structurelles
=============================================

Réalisation technique du Combat Syndicaliste, Webmaster & intranet,
Imprimerie, etc.)

Chacun mesure la lourde tâche qui est celle de nos camarades mandatés à la
réalisation technique / maquettage du CS (nous ne parlons pas ici du comité
de rédaction). Il est même parfois difficile de voir émerger les candidatures
à ces mandats...

Au delà de son animation régulière, le site web confédéral ne connaît pas
d’évolution qualitative. L’intranet confédéral est vieillissant de l’aveu
même des camarades de la commission chargée de son entretien.

Tout comme la gestion des outils internet de la CNT (réalisation & animation
du site web, gestion technique de l’intranet, gestion des listes, maintenance
des serveurs), les besoins en imprimerie et en sérigraphie (CS, tracts,
circulaires, matériel de propagande), etc., ces réalisations nécessitent des
compétences et souvent des connaissances techniques et une disponibilité
importante. Elles reposent dés lors sur les épaules de quelques camarades
spécialistes.

Parfois, la viabilité de ces outils ne tient plus qu’à un fil.

Notre confédération, nos fédérations, nos Unions Régionales, nos syndicats, font
régulièrement appel à des prestations externes et payantes faute d’outils ou
de services techniques plus intégrés. Ces prestataires se situent souvent dans
un réseau « amical », par défaut au gré des opportunités tarifaires et/ou de
proximité.

Si le recours à ces services techniques permet à la CNT de ne pas recourir au
salariat de personnes dédiées, il n’est pas pour autant satisfaisant pour
différentes raisons :

- Il évacue de facto (dans l’état actuel des choses) la question des conditions
  de travail  des salariés de ces prestataires ;
- Il n’intègre pas totalement les clauses environnementales et alternatives ;
- Il maintient la CNT dans une dépendance économique et technique vis-à-vis
  d’autres organisations ou des entreprises capitalistes.


Nous imaginons plusieurs types de réponses qui nécessitent pour chacune un examen
spécifique:

Le développement de la formation
--------------------------------

Il suppose que les compétences soient
acquises en interne et que des camarades soient en mesure de les transmettre
dans la continuité.
Elle est néanmoins à privilégier dans un esprit d’autoformation et d’acquisition de
compétences personnelles, professionnelles, et militantes ;

Le recours à l’emploi de salarié.e.s formés (ou à former)
---------------------------------------------------------

Il place la CNT en position d’employeur, de gestion des « ressources humaines »
avec toutes les questions  sous-jacentes : Etablissement des contrats,
salaires, encadrement, évolution  de carrière, gestion des conflits, droits
syndicaux, etc.

Le recours au salariat s’oppose par ailleurs aux principes même inscrits à
l’article 1er des statuts confédéraux quant à la perspective révolutionnaire
d’abolition du salariat.


Le développement d’outils externes techniques et économiques à caractères
--------------------------------------------------------------------------

Autogestionnaires et coopératifs : Cet axe de réflexion, entrevu lors du
30ème Congrès confédérale de 2008 à Lille, est pour l’heure limité aux seuls
travaux de la commission confédérale ``coopératives syndicales et économies coopératives``
(http://www.cnt-f.org/cooperatives/).

Il mériterait d’être prolongé en vue conforter un véritable outil confédéral
de développement et de soutien à la création d’entités coopératives ;


I.B.b- L’action juridique, un enjeu spécifique
==============================================

Si la voie du juridique n’est pas forcément incompatible avec notre engagement
révolutionnaire, elle ne saurait se suffire à elle-même.

Elle est une composante immédiate de l’action syndicale mais notre syndicalisme
de transformation sociale ne peut se résumer à une prestation de service,
soit-elle efficace.

Certains syndicats sont effectivement plus soumis à une pression juridique de
par les difficultés des travailleurs de leur secteur d’industrie face un
patronat organisé et offensif.

Bien souvent, ces secteurs cumulent de même des difficultés en termes de
précarité, de méconnaissance des droits, et d’accès au droit (illettrisme,
maîtrise de la langue, problème de niveau scolaire). Nous l’avons vu et compris
au cours des discussions de la commission confédérale liée au syndicat du
nettoyage RP.

La loi d’août 2008 portant sur le droit syndical dans le privé, certes
critiquable au regard de bons nombres d’aspects, a permis malgré tout à la CNT
de connaitre un développement de son implantation dans les entreprises.

La constitution de sections syndicales étant facilitée à travers la désignation
des RSS et une acquisition « simplifiée » de la représentativité nécessaire à
l’expression syndicale dans l’entreprise.

Dans la fonction publique, le décret paru au journal officiel le 17 février
2012, remplaçant la circulaire de 1982 jusqu’alors en vigueur, modifie en
certains points la réglementation du droit syndical dans la fonction publique
d’Etat.

Dans nos expériences récentes, les syndicats ont été confrontés à des enjeux
juridiques plus ou moins lourds en vue de l’implantation de leurs sections
d’entreprises. La CNT a eu a affronté plusieurs procès relatifs à la liberté
d’expression sur internet. Des Unions Locales CNT ont dû faire face à des
procédures judiciaires pour l’accès ou le maintien à des locaux syndicaux.


Certaines affaires ont eu une résonnance confédérale et interprofesionnelle eu
égard aux enjeux jurisprudentiels (affaires BAUD / SCIAL RP, Fédération des
entreprises de propreté / nettoyage RP)

L’action juridique prend par conséquent dans certains secteurs, ou plus
largement, une place non négligeable que notre inter-corporatisme ne peut ignorer.

Dès lors, elle requiert:

- Compétences spécifiques et renouvelées (évolution du droit)
- Disponibilité

:ref:`Le XXXIème Congrès de 2010 <congres_CNT_2010_saint_etienne>`  adoptait les
orientations suivantes::

	«Le syndicalisme prôné par la CNT est fondé sur l'importance du lien
	interprofessionnel. C'est ce lien qui donne la dimension de solidarité de
	classe, un des fondements de notre pratique et de notre projet révolutionnaire»

L’approche intercorporatiste ou interprofessionnaliste propre à l’identité de
la CNT, ou l’AS&SR, doit nous amener à développer de prime abord des outils
internes mutualisés basés essentiellement sur l’effort militant.

Il conviendrait dès lors à tous les niveaux de l’inter-professionnalisme ou des
Unions de la CNT que cette action juridique constitue un effort collectif
d’entraide mutuelle.

L’établissement de commissions juridiques permanentes doit pouvoir répondre à
ces besoins avant tout autre type d’intervention à caractère professionnel ou
prestataire (cabinet d’avocat, voire embauche d’un « permanent juridique »).

La formation syndicale et juridique est un autre levier indispensable pour notre
développement et pour nos militants et plus particulièrement pour ceux qui
exercent une activité syndicale dans l’entreprise.

Les camarades militants élus ou représentants des sections d’entreprises ont
parfois peu ou pas d’expériences et sont amenés à siéger dans les IRP (DP, CE,
CHSCT) sans pour autant en connaitre les fonctionnements et prérogatives.

Des sections syndicales sont donc livrées à elles-mêmes et ont aujourd’hui
recours aux conseils et à l’assistance de quelques camarades reconnus pour
leur expertise. Cette situation n’est pas satisfaisante. Elle repose
exclusivement sur un système de type informel ou non mutualisé.

Cette hiérarchie de compétences peut tendre de surcroit à l’instauration d’une
ascendance sur les choix d’orientation des syndicats par la dépendance qu’elle
peut générer.

Ainsi, le développement de sections syndicales d’entreprises, l’arrivée de
nouveaux adhérents doivent pouvoir s’accompagner de formations syndicales,
trop souvent laissées au second plan alors que de tout évidence il s’agit d’une
nécessité.

Ces formations syndicales doivent être organisées et construites pour et par une
démarche interprofessionnelle.
