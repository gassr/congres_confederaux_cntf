


.. _motion_31_2010:

=============================================================================================
Motion n°31 2010 : Pour une CNT plurielle, fraternelle, démocratique et dans l'action, SUB 69
=============================================================================================




Argumentaire
=============

Une fois de plus notre CNT est confrontée à une crise.

Crise de croissance bien logique somme toute.

Une fois de plus car notre courant ne compte plus le nombre de fois où ces
questions ont traversé l'organisation dans la douleur.

Et la question est celle de la confrontation entre la théorie et la pratique,
entre l'idée et l'action.

Bien sûr, tout le monde dira que nous sommes pour un aller-retour permanent
entre la théorie et la pratique.
Cependant, la CNT souffre de sa « jeunesse », de son inexpérience ce qui fait
qu'elle peut, dans certains cas, apparaître dogmatique et donneuse de leçons.

Notre CNT s'est redéveloppée dans l'idéologie : l'idée qu'un autre
syndicalisme est possible. Beaucoup de jeunes ont adhéré à la CNT sur une base
idéologique comme on adhère à une organisation révolutionnaire. Or c'est bien
une organisation syndicale que nous voulons construire. Ce qui nous plaît à
nous, Révolutionnaires, dans le syndicalisme c'est bien la confrontation au
réel, à la société, aux gens, aux travailleurs.

Le syndicalisme se doit donc sans cesse d'évoluer au fil de son expérience
pratique, de sa confrontation aux travailleurs de son secteur s'il ne veut
pas rester comme beaucoup d'orgas révolutionnaires : incantatoire. C'est là
la force de la CNT : mêler la visée révolutionnaire à une action quotidienne
ancrée dans le réel.

La CNT s'est développée ces dernières années sur le terrain syndical échappant
petit à petit à son image « trop politique ». Il est intéressant d'analyser
les choix des syndicalistes de la CNT : participations à toutes les
institutions représentatives du personnel (DP, CE, CHSCT....), bourse de placement
(service d'aide à la personne), création de coopérative de production, pratique
d'un militantisme payé (aux PTT ou dans les fonctions publiques avec les ASA ou
encore avec les délégations dans le secteur privé) voire création d'un poste
de permanent dans le nettoyage. Des choses que la CNT a pu critiquer
officiellement.

Mais que fait-on quand on se frotte au réel ? Les détracteurs de ces acteurs
n'ont jamais proposé d'autres routes...

Par ailleurs, d'autres expériences ont eu lieu que ce soient les coopératives de consommation, la réappropriation
collective de nourriture ou de locaux.... ces expériences ont elles aussi eu leurs détracteurs. Il est terrible de noter
que quoiqu'il se fasse à la CNT, il se trouve des cénétistes pour condamner les actions menées. La critique et le
débat démocratique sont nécessaires et sains mais ce n'est pas ce qui se passe. L'insulte, la condamnation sans
appel, les guerres fleurissent trop souvent dans notre confédération sous prétexte que les uns sont des jaunes, les
autres des réformistes, ou encore des totos, des rouges ou des noirs, des branleurs ou des gauchistes....
Il est temps que les cénétistes acceptent la différence, la diversité et apprennent à vivre en démocratie.

Motion
======

Partant du principe que la CNT est révolutionnaire, notre but est de rallier la masse des travailleurs à notre cause.
Partant du principe que la CNT est syndicaliste, notre but est d'être dans l'action quotidienne.
Partant du principe que la CNT est pour la lutte des classes notre but est de défendre tous les opprimés.

Tout en se réclamant de l'anarchosyndicalisme et du syndicalisme révolutionnaire, ces courants au fondement de
notre identité ne doivent pas devenir des entraves en étant érigés en dogmes.
C'est l'expérimentation quotidienne de la lutte des classes dans des actions concrètes et quotidiennes, c'est la
construction d'une organisation de classe englobant tous les pans de la vie (du droit du travail au sport, en
passant par la bouffe) qui nous permettra de transformer cette société.

Respectons les différences d'approches et enrichissons-nous les uns les autres. Arrêtons les guerres fratricides.
Ni Etienne Deschamps, ni Pierre Bance, ni le SCIAL-RP, ni l'UD 69, ni l'UR-RP, ni aucun cénétiste ne sont les
ennemis des travailleurs. Nos ennemis sont regroupés principalement au Medef (en France) et dans d'autres
organisations patronales. Il est temps de retrouver le chemin de la fraternité et du respect, conditions de la
pratique démocratique.

La richesse de notre syndicalisme est bien de pouvoir développer à la fois une coopérative de consommation,
une section d'entreprise siégeant au CE, une UD présente dans toutes les luttes politique de sa ville. Personne n'a
raison ou tort.

En conséquence, toutes les organisations de la CNT (syndicats, sections, UL, UD, UR, ...) s'engagent à vivre
dans la fraternité avec toutes les autres organisations de la CNT. Les querelles, faux procès, manipulations
orchestrées par des groupes affinitaires, insultes de tout type ... doivent cesser. Toutes les organisations de la
CNT doivent retrouver l'ouverture d'esprit propre au syndicalisme. La critique et le débat seront portés par les
syndicats dans les réunions régulières de la CNT (AG de syndicats, d'UD, d'UR, CCN, congrès) dans le respect
et la fraternité.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°31          |            |          |            |                           |
| SUB 69               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
