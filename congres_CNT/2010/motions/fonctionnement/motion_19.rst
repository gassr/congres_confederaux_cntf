

.. _motion_19_2010:

====================================================================
Motion n°19 2010 : Candidat aux différents mandats, INTERPRO CNT 31
====================================================================



Argumentaire
=============

Nous avons observé depuis plusieurs congrès, une tendance au forcing et à
l'improvisation pour trouver certains mandatés confédéraux.

Nous savons bien que les mandats confédéraux réclament beaucoup d'énergie et
qu'ils effrayent beaucoup de syndicats qui y voient plus un frein à leur
développement local plutôt qu'une opportunité de s'impliquer dans la vie
confédérale.

Ainsi à chaque congrès, certains mandats se trouvent donc sans syndicat
candidat et il nous faut motiver des syndicats qui se déclareront candidat...
un peu par défaut. Nous pensons qu'en procédant de la sorte, nous contribuons
à masquer et à reproduire ce problème qui pourrait devenir plus important
dans les années à venir, si nous ne l'affrontons pas directement.

Aussi, nous proposons de mettre les syndicats en face de leurs responsabilités en envisageant la possibilité de
sortir de nos congrès avec des mandats confédéraux non pourvus. Il appartiendra par la suite à chaque syndicat
de considérer la situation, d'en discuter en assemblée générale et de se porter ou non candidat en attendant le
prochain :term:`C.C.N` qui confirmera leur candidature.

Motion
======

Aucun des candidats aux différents mandats confédéraux ne pourra se présenter
lors du congrès sans en avoir préalablement discuté dans son syndicat.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°19          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso:: :ref:`contre_motion_19_2010_ste93`
