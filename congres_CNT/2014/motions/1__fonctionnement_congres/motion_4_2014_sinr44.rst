
.. index::
   pair: Motion 4 ; 2014

.. _motion_4_2014_sinr44:

=====================================================================================
Motion n°4 2014 : Modification des règles du quorum d'ouverture du congrès (SINR 44)
=====================================================================================




Motion (non applicable pour le 33ème Congrès confédéral)
==========================================================


Afin qu'un Congrès confédéral puisse débuter, il est nécessaire que
50% minimum des syndicats à jour de cotisations soient présents,
dont 40 % présents physiquement.

Contre motions
==============

.. seealso::

   - :ref:`contre_motion_4_2014_ess34`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°4 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
