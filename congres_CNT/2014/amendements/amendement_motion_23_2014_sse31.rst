
.. _amendement_motion_23_2014_sse31:

================================================
Amendement à la motion n°20 2014 (SSE31)
================================================

.. seealso::

   - :ref:`motion_20_2014_etpic30`




Argumentaire
=============

Nous ne voyons pas un format plus petit qu'A4.

Amendement
==========

Modification du deuxième paragraphe::

    Le Combat Syndicaliste change de format pour passer à un format A4 plus
    léger en termes d’épaisseur papier. Son coût doit être réduit au maximum,
    tout en intégrant autant que possible l’utilisation du papier recyclé
    (motion du 28 ème Congrès Confédéral de Janvier 2004) et en conservant
    une belle facture.


Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Amendement à la :ref:`motion N°23 2014 <motion_23_2014_etpic30>`

       :ref:`SSE31 <sse31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
