
.. _contre_motion_23_2014_sipmcs:

===================================================
Contre motion à la motion 23 2014 SIPMCS
===================================================

.. seealso::

   - :ref:`motion_23_2014_etpic30`





Contre argumentaire à la motion 23
===================================

Là encore, nous sommes en accord avec l'argumentaire de la motion 23, mais
nous pensons que la motion qui en découle est plus restreignante qu'autre
chose.

Nous pensons qu'il faut laisser de la liberté à la commission CS.

Le congrès souhaite que le CS soit moins cher ? ,ce que nous soutenons,, alors
laissons la commission CS étudier ce dossier et faire des choix sans devoir en
référer au préalable à un CCN.

Lors des CCN, nous pourrons discuter, voire revenir sur, des évolutions
apportées au CS.
