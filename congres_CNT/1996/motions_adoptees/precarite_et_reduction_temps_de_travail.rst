
.. _motion_precarite_cnt_congres_lyon_1996:

===============================================================================================================
Motions Précarité et réduction du temps de travail, 25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon
===============================================================================================================

RTT et precarite
================

L'interco 44 centralise toutes les contributions sur la RTT, sur la précarité,
sur la notion de travail...

Le BC devra ensuite impulser une réunion avec tous les syndicats désirant
travailler sur la question pour débattre des contributions et faire des
propositions concrètes aux syndicats qui décideront (étoffer la plate-forme
faire une brochure...).
