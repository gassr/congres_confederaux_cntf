.. index::
   pair: Motion 27 ; 2021


.. _motion_27_2021_ptt_centre:

================================================================================================================================================
Motion **n°27** 2021 (hors délai) **Gestion des Conflits au sein de la CNT** par **PTT Centre**
================================================================================================================================================

Autres motions PTT-Centre

    - :ref:`syndicats:motions_ptt_centre`


Motion
========

- Pour tout adhérent incriminé faisant l'objet d'une mise en cause
  personnelle, la CNT préservera dans toutes ses instances le principe
  de la présomption d'innocence."

- Les syndicats sont seuls juges de la validité de l'adhésion, de son
  éventuelle suspension ou de son exclusion d'un de leur membre en leur
  sein.
  La Confédération en tant que telle ne peut contrevenir à cette règle de
  base sans revenir sur le **principe d'autonomie des syndicats**.

  Si un conflit existe et n'est pas résolu entre les structures internes
  de la Confédération et en derniers recours il subsistera la possibilité,
  par décision de Congrès pour la Confédération de délabelliser et de
  désaffilier le ou les syndicats en question.


Amendements
===========

- :ref:`amendement_motion_27_2021_stp67`

Contre-motions
================

- :ref:`contre_motion_12_24_27_2021_ste75`


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°27 2021 PTT Centre |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
