



.. _motion_53_2010:

========================================================================================================================================
Motion n°53 2010 Validation confédérale du « Secteur vidéo » et proposition de fonctionnement interne, Culture Spectacle 32 [non votée]
========================================================================================================================================



Argumentaire
============

Le projet de mise en place du « Secteur Vidéo » a été la volonté
d Éric SSEC59/62 et de Jean-Yves USI 32, tous deux bidouilleurs vidéastes
non mandatés.

Ce travail a mis en évidence l'importance pour la confédération de se munir
d'une structure de propagande et de création vidéo. Après l'accord de principe
du Bureau confédéral et de la Commission administrative sur la base du projet
écrit, le CCN de Rennes 2009 a approuvé la création du «Secteur vidéo».

Par l'un des principes fondamentaux de la confédération, la démocratie directe et
autogestionnaire, les membres du secteur vidéo définissent et décident du
développement interne et externe à cette structure indépendante et dont le mandaté
confédéral est garant du fonctionnement.

Cette proposition a été transmise aux syndicats par tous les moyens de communication
confédérale.


Motion
------

Le syndicat Culture Spectacle 32 demande au congrès de valider le « Secteur vidéo » comme un outil de
propagande syndicale et de création vidéo « Confédéral » . Le « Secteur Vidéo est indépendant de la structure
« Secteur propagande » tout en étant complémentaire. Le « Secteur vidéo » est ouvert à tous les militants de la
confédération qui désirent participer au développement de la structure et à son activité. Ce sont les membres du
« Secteur Vidéo » qui choisissent le mandaté confédéral pour une durée de 2 ans, que le congrès valide.
Le « Secteur Vidéo » labellise la production vidéo qui reste sous la responsabilité des membres.

Les tâches attribuées au mandaté confédéral « Secteur Vidéo ».
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Le mandaté fait la demande au BC/CA pour l'obtention administratif du label. En cas de litige pour l'attribution
du label à l'intérieur du «Secteur vidéo», le mandaté explique la situation au Bureau confédéral et à la
Commission administrative et / ou au CCN, qui prennent une décision et en rendent compte au mandaté
« Secteur vidéo ».

Le mandaté informe le «Secteur Vidéo» de la décision argumentée, communique l'information
de production vidéo labellisé par le Bulletin Intérieur et la liste syndicats, le rapport
d'activité confédéral.

Administration de l'outil internet et contact externe. Diffuse l'information du « Secteur Vidéo » par :
la liste interne de la commission confédérale "Secteur vidéo" de la CNT : liste.secteur-video@cnt-f.org


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°53          |            |          |            |                           |
| Culture Spectacle 32 |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso::

   - :ref:`contre_motion_53_2010_ste93`
   - :ref:`contre_motion_53_2010_ess34`
