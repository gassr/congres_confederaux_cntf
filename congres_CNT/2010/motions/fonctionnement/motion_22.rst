


.. _motion_22_2010:

=======================================================================
Motion n°22  2010 Mandat cadre pour le Bureau confédéral, Education 13
=======================================================================


  
Argumentaire
============

A plusieurs reprises ces derniers mois, la Confédération n'a pas porté de discours concernant des mobilisations
sociales organisées par les centrales syndicales, que cela soit pour s'y joindre ou bien apporter une argumentation
critique sur cette tactique des journées d'actions de 24H. Il nous semble que ce silence est préjudiciable pour la
Confédération, ne permettant pas à des sympathisants ou n'importe quel salarié de connaître nos positionnements
et tactiques de lutte, nous éloignant symboliquement du champ syndical.

De plus, loin d'une cohésion et coordination d'ensemble de la Confédération, notre apparition n'est que locale,
alors même que les problématiques soulevés sont nationales. Or on constate à chaque fois que nombre de
syndicats, unions ou fédérations de la :term:`C.N.T` participent aux manifestations et à la grève, pour y faire connaître
nos positions et tenter d'impulser des luttes plus dures. Les outils de consultation des syndicats à disposition du
:term:`B.C` devrait lui permettre de dégager un positionnement confédéral sur ces sujets.

Motion
======

La Confédération appelle, en principe, aux manifestations syndicales, interprofessionnelles, unitaires, dans le but
de faire connaître aux nombreux salariés et chômeurs qui s'y rendent, les positions de la :term:`C.N.T` sur les thèmes
syndicaux évoqués à cette occasion et y encourager des tactiques de lutte en rupture avec celles des « grandes »
confédérations : grève reconductible, autogestion des luttes...

Toutefois, il se peut que le contexte, ou d'autres éléments liés au contenu de l'appel, rende inopportune notre
participation à tel ou tel évènement de ce type. Pour cela, le bureau confédéral prendra systématiquement avis
des syndicats par tous les moyens nécessaires. Les syndicats qui jugeront que la Confédération ne doit pas
appeler à de telle manifestation, pour telle ou telle raison précise, le feront alors savoir. Le bureau, assisté de la
CA, prendra position à partir de la majorité des avis exprimés.

Par défaut, le bureau confédéral a mandat du congrès pour rendre publique notre appel aux manifestations et/ou à
la grève : communiqué de presse, publication sur le site Internet ou la presse confédérale. Un bilan de notre
participation sera demandé systématiquement aux syndicats par le Bureau Confédéral.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°22          |            |          |            |                           |
| Education 13         |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+
