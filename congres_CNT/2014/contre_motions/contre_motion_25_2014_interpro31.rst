
.. _contre_motion_25_2014_interpro31:

===================================================
Contre motion à la motion numéro 25 2014
===================================================

.. seealso::

   - :ref:`motion_25_2014_etpic30`





Argumentaire
=============

Nous rappelons que tous les syndicats de la CNT ne maitrisant pas suffisament
les technologies pour mettre en ligne leurs textes, vidéos, etc. peuvent faire
une demande d'aide auprès du webmaster confédéral.

À terme, ces syndicats doivent se former en faisant appel à des référents
web régionaux ou confédéraux dans le but de devenir autonomes et d'alléger
le mandat du webmaster.

Contre-motion
=============

Chaque région proposera avant le prochain CCN unE ou plusieurEs référentEs web
dont le rôle sera de former les syndicats qui manifestent un besoin
dans ce domaine.

Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Contre-motion à la :ref:`motion N°25 2014 <motion_25_2014_etpic30>`

       :ref:`Interpro31 <interpro31>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
