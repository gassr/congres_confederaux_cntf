
.. index::
   pair: Statuts; Fédération Transports


.. _statuts_federation_CNT_transports:

========================================================================================================
Statuts de la Fédération des Travailleurs des Transports, de la Logistique et des activités auxiliaires
========================================================================================================

:download:`Télécharger les status de la fédération CNT transports au format pdf <statuts_fttla_congres.pdf>`.


::

	CONFEDERATION NATIONALE DU TRAVAIL
	6, rue d'Arnal,30000 Nîmes
	fédération.transports@cnt-f.org




.. toctree::
   :maxdepth: 4

   constitution/index
   but/index
   administration/index
   admission/index
   cotisations/index
   congres_federal/index
   congres_federal_extraordinaire/index
   commissions/index
   greve/index



:Date et signature:


:Le secrétaire:



:Le trésorier:



A chaque fois qu'il est fait mention de **transport**, la fédération désigne
l'ensemble de l'appellation, à savoir, transport, logistique et activités
auxiliaires.
