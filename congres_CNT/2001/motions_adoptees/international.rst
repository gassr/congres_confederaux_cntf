

.. _motions_international_cnt_congres_toulouse_2001:

======================================================================================
Motions "international" du 27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
======================================================================================


Pour l'abandon de la reference A.I.T
====================================

Vote
----

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Refus de vote             |
+============+==========+============+===========================+
| 23         | 16       | 3          | 1                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Texte
-----

Lors de son 27ème Congrès Confédéral tenu à Toulouse, les 2, 3 et 4 mars 2001,
la Confédération Nationale du Travail siégeant a paris, a pris la décision de
ne plus utiliser le sigle A.I.T. (Association Internationale des Travailleurs),
exclue bureaucratiquement (2 syndicats pour, un contre, le reste s'abstenant).

Lors du Congrès International de Madrid en décembre 1996, notre organisation
avait alors choisi de conserver le sigle A.I.T. dans la mesure où par ses
principes et ses pratiques, nos syndicats interviennent au quotidien sur les
bases des positions historiques de l'A.I.T. fondée en 1922 à Berlin.

En 2001, prenant acte que l'A.I.T. est devenue une organisation groupusculaire
orientée selon des bases dogmatiques et absente des mobilisations sociales de
masse (Cologne, Amsterdam, Nice) la C.N.T. a décidé de supprimer toute
référence à l'A.I.T.

Au-delà de ce choix, la C.N.T. tient à affirmer qu'elle maintiendra haut et
fort l'esprit et le lettre de ce que fut la véritable A.I.T., qu'elle
renforcera son activité sur la scène internationale, non seulement avec les
organisations syndicales qui se revendiquent explicitement du SR et de l'AS
mais également avec les structures syndicales qui luttent sur des bases
anticapitalistes et antiétatiques pour l'action directe et le refus de la
collaboration de classe.

Aujourd'hui, encore plus qu'hier, la C.N.T. continuera son action sur la scène
sociale internationale, espace où se joue désormais toute perspective
révolutionnaire


27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse
61 syndicats présents à jour de cotisations
Pour : 22 Abstention : 6 Contre : 4 Refus de Vote : 0


61 syndicats présents à jour de cotisations
Pour : 22 Abstention : 6 Contre : 4 Refus de Vote : 0

Concernant une nouvelle Internationale
======================================

Considérant que la construction d'une Internationale (ou la reconstruction de
l'AIT) ne pourrait se faire par en haut, mais qu'elle se fera par en bas, nous
sommes contre une proclamation formelle sur le papier d'une nouvelle
internationale à l'heure actuelle, qui ne serait qu'une création prématurée
et artificielle.

Mais nous sommes d'autant plus pour la mise en marche de cette œuvre
constructive ou reconstructrice dès maintenant, et ceci sur des bases pratiques
et concrètes - tout en considérant que nous devons d'abord nous donner les
moyens et remplir plusieurs conditions pour la réalisation de ce projet, qui ne
s'effectuera que successivement. Cette démarche serait :

Dans l'immédiat
---------------

La poursuite de notre engagement international, réalisé dans le bon travail du
secrétariat international sortant suivant le dernier mandat denotre dernier
Congrès, c'est-à-dire la continuation de ce travail avec les contacts sérieux
et permanents établis, par des actions de solidarité - comme p. ex. pour
Alcatel Berlin, Björn Söderberg ou Mumia Abu-Jamal – et par des projets
concrets et ciblés comme pour le convoi syndical au Kosovo.


Avec Mai 2000 dans sa partie internationaliste il est devenu manifeste que le
regard des anarcho-syndicalistes et syndicalistes révolutionnaires du monde
entier sont aujourd'hui tournés vers nous. Ceci entraîne une attente... et une
responsabilité militante de notre part face au syndicalisme de lutte de classe
et d'action directe de ce monde.

Il nous faut être conscient de cette "image de marque", de ce rôle que nous
tenons.

Mais il ne faut pas en rester là et se contenter de la "vitrine".

Le pôle international autour de la CNT française ne doit pas être perdu.

Gardons la dynamique internationale créée avec les autres tout en tendant
- toujours - la main à ceux et celles qui restent - encore – en retrait,
prenons l'initiative et avançons des projets concrets (voir point 2 de cette
motion).

A moyen terme
-------------

- l'organisation de conférences industrielles, de branches ou de secteurs avec
  tous les contacts internationaux à la demande des groupes, syndicats et
  fédérations intéressés, qui appuieraient le SI dans la mise en place de ces
  projets, avec pour but l'échange d'informations sur le travail et
  l'élaboration de revendications et actions communes à l'échelle
  internationale. Toutefois, il ne s'agira pas de singer les conférences
  syndicales politiciennes et réformistes en dressant des catalogues de belles
  revendications à la table ronde, mais l'organisation de telles rencontres et
  échanges doit être axée dès le départ sur des luttes et des revendications
  concrètes, réalisables dans l'immédiat et dans la limite de nos moyens
  actuels.
- La tenue de conférences par entreprises multinationales avec les mêmes moyens
  et finalités à la demande des sections syndicales d'entreprise, secteurs ou
  groupes respectifs intéressés et leur appui pratique.
- Création d'une dynamique par en bas à travers des campagnes de solidarité
  internationale (nous pensons surtout à des actions en soutien de grèves :
  manifs, tractages, information des consommateurs, appel au boycott afin de
  faire une pression sur les entreprises concernées) : D'une part, le SI doit
  coordonner de telles campagnes à l'appel des syndicats ou en proposer selon
  les informations reçues, en ayant fait le choix des luttes qui lui semblent
  les plus importantes et prometteuses. Dans ce cas le SI devrait en informer
  rapidement par des communiqués internes. D'autre part, les syndicats devront
  s'engager à mettre en œuvre ces actions de soutien international là où ils
  sont implantés. Tout/e militant/e doit pouvoir s'engager sur du concret et à
  la base !


A long terme
------------

- Nous envisageons une possible reconstruction future de l'Internationale en
  restant fidèles aux principes de la Première Internationale, reconstituée en
  1922 à Berlin, avec le projet de combattre le capitalisme à l'échelle
  mondiale en nous alliant avec toutes les structures syndicales qui affirment
  une alternative sociale au capitalisme, refusant le dogmatisme tout comme la
  voie réformiste pour construire une réelle solidarité internationale fondée
  sur l'action directe et favorisant l'émergence d'un internationalisme
  anarcho-syndicaliste et syndicaliste révolutionnaire actif.
- Nous maintenons les statuts de l'AIT - dans leur version d'avant 1996 -
  comme point de référence et rassemblement pour les syndicats (et groupes de
  soutien désirant participer aux activités syndicales) anarcho-syndicalistes
  et syndicalistes révolutionnaires dans le monde.
  Ces mêmes statuts serviront également comme base pour une éventuelle
  reconstruction de l'Internationale, donnant une ligne générale pour le chemin
  à prendre vers le communisme libertaire (voir les statuts où ce projet est
  énoncé).
  Ce texte, avec ses principes et ses finalités - c'est à dire la préambule sur
  le syndicalisme révolutionnaire et les statuts mêmes, représente par
  excellence une stratégie d'ouverture telle que nous la concevons.
  Avec ses 78 ans d'existence ce texte élaboré par Rudolf Rocker et adopté par
  le Congrès de Berlin de 1922 reste moderne dans son analyse de l'exploitation
  et le combat contre le capital et l'Etat tel qu'il le définit reste
  applicable dans le contexte actuel.

Conclusion
----------

Restons raisonnables en mettant un pas devant l'autre. D'énormes manifestations
communes comme Lyon, Amsterdam, Cologne et finalement Paris (Mai 2000) marquent
un point fort de notre syndicalisme internationaliste et peuvent engendrer une
dynamique (et ça fait tellement de bien!), mais tout ceci ne fait pas une
Internationale.

Notre dernier Congrès avait adopté cette motion::

	"La CNT-AIT reste fidèle à ses origines qui sont la Première Internationale et
	celle de 1922 et dit qu'il n'y a pas l'intérêt de créer une nouvelle
	organisation internationale."

Par contre, l'organisation, allant plus loin, s'est exprimée en refusant sa
ré-adhésion à l'AIT actuelle et en faveur de la poursuite de la lutte
internationale avec les sections de l'AIT qui le désirent et d'autres
organisations.

Aujourd'hui, profitons de cette situation extraordinaire dans laquelle nous
nous situons. Ce n'est pas une faiblesse mais une chance de rester ouvert aux
différentes organisations hors et dans l'AIT actuelle.

Ne tombons pas dans les travers similaires à ceux rencontrés au sein de l'AIT
dont nous avons été exclus. Gardons notre liberté d'action. Ne mettons pas la
charrue avant les bœufs ; la démarche logique dans le développement d'une
pratique internationale ne peut être que :

1. Présence sur le terrain ;
2. création et poursuite d'un rapprochement international autour d'actions
   syndicales communes, par branche d'industrie, sur la base du travail concret
   et par en bas. Et non l'inverse.
