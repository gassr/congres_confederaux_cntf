
.. index::
   pair: Motions; Imprimerie Confédérale


.. _motions_51_52_2010:

===========================================================
Motions n°51 et 52 2010 Imprimerie confédérale, ETPRECI 35
===========================================================


.. seealso:: :ref:`motions_etpreci35`




Argumentaire
============

Actuellement, l’:term:`I.C` (Imprimerie Confédérale) est constitué d’un dupli A3 bicolore et
le mandat est géré  par l’ETPIC 30. Devant servir à l’origine à aider les
« petits » syndicats dans le besoin, cela n’est plus le cas. En effet, aucune
demande ou commande de  tract n’a été faite auprès de l’:term:`I.C` ces deux dernières années.
Cela s’explique  par la dotation en duplicopieurs de nombreux syndicats et unions
par les deux  opérations d’achats successifs au niveau confédéral.

De fait la sollicitation de l’I.C s’est réduite d’autant plus avec les frais
d’expédition et les délais d’impression et d’acheminement liés.

Nous suggèrons de redéfinir cette :term:`I.C` et ses objectifs. L’idée de ces deux
motions est :

- d’aider les syndicats ne disposant pas de moyens de tirages localement à
  disposer de tracts (confédéraux ou autres) ;
- d’aider le BC en lui enlevant la charge technique d’impression, d’assemblage
  et d’expédition des documents internes.


.. _motion_51_2010:

Motion n°51 2010 Imprimerie confédérale, ETPRECI 35
===================================================

Aide aux syndicats : Les syndicats confédérés (petits ou pas, cette notion
étant très relative et non définie) et autres unions peuvent solliciter
l’:term:`I.C` pour des tirages. La facturation de ces derniers sera répartie comme
suit : la motié des frais liés au tirage à charge du syndicat (ou union)
demandeur, l’autre moitié et les frais d’expédition à la charge de la
trésorerie confédérale.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°51          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. _motion_52_2010:

Motion n°52 2010 Imprimerie confédérale, ETPRECI 35
===================================================

L’:term:`I.C` s’occupe des tirages des outils internes de la CNT à savoir les circulaires
confédérales, bulletin intérieur, brochures de formation, etc. et leur expédition
afin d’allèger la tâche du Bureau confédéral.

+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°52          |            |          |            |                           |
| ETPRECI 35           |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


Proposition
============

Le syndicat ETPRECI 35 se propose à la reprise du mandat de l’:term:`I.C`.
Les autres syndicats de l’UL de Rennes y sont favorables.
