
.. index::
   motion 3 Secrétariat juridique confédéral (2010)

.. _motion_3_2010:

================================================================================
Motion n°3 2010 : Secrétariat juridique confédéral, SIPM ­RP [synthèse]
================================================================================



Motion reprise par SIPM-RP , 2012
=====================================

- :ref:`motion_33_2012`

Argumentaire
============

Il existe de plus en plus de savoir-faire juridique au sein de la confédération,
du fait de l’expérience syndicale des uns et des autres, mais aucune mise en
commun n’est réellement faite, ni aucune formation des camarades qui le souhaiteraient.

Le projet d’émancipation collective et individuelle porté par la :term:`C.N.T`,
ainsi que le développement de l’autogestion comme mode de fonctionnement,
nécessitent que chacun, individu ou syndicat, soit de moins en moins obligé
d’avoir recours à des spécialistes. Une mutualisation des connaissances et la mise
en place de formations juridiques sont nécessaires pour la :term:`C.N.T`.


Motion
======

Il est créé au sein du bureau confédéral de la :term:`C.N.T` un secrétariat juridique.

Il est composé de deux mandatés, un en droit du travail privé et un en droit public.
Ces deux mandatés sont membres de la commission administrative confédérale et
s’entourent de tous les camarades qui le souhaitent, via une liste Internet
et des réunions.

Ce secrétariat juridique n’a pas vocation à monter des dossiers juridiques ni à
régler les problèmes individuels, mais à conseiller les adhérents de la :term:`C.N.T`
lorsqu’ils le peuvent, à les mettre en relation avec d’autres adhérents
ayant déjà une expérience dans la question soulevée.

Le secrétariat juridique a pour vocation première de mettre en place des formations
en droit syndical et droit du travail, public ou privé, à la demande des syndicats,
unions ou fédérations qui le souhaitent.

Le secrétariat juridique publiera également des brochures de formation juridique à bas coût.


Discussion
==========

**Tribune**
    la 3 semble compléter la :ref:`2 <motion_2_2010>` donc on gagne du temps à
    les voter ensemble.

**ETPRECI 35**
    Pour la contre motion

**STE93**
	importance des aspects formation et mutualisation pour éviter la spécialisation
	et des prises de pouvoir du juridique sur la vie des syndicats.

**ETPIC30**
    merci à la commission juridique sortante car très réactive.
    Pour les 2 motions.

**STE75**
	Pas de raison qu'au niveau confédéral on forme des spécialistes. On a
	abandonné cette idée à la FTE naturellement car pas nécessaire.
	Chacun peut se cogner des dossiers.

**CCS RP**
	Se compose de tous les adhérents de la CNT. On n'a pas voulu personnaliser
	les postes et les mandats.

**ESS 34**
	Secrétariat plus simple qu'une commission car CR obligatoire pour le
	secrétariat. Le congrès doit mandater des gens qui feront un CR au
	congrès suivant.

**STE 57**
	Indispensable de mutualiser. Idéal pour nous c'est la contre motion
	ETPRECI 35 car disparition du secrétariat au profit d'une commission.

**Educ 86**
	Proposition de fusionner les motions avec les contre motions

**SIPM**
	On ne veut pas créer un cabinet d'avocat. Tout ce qui peut être apporté
	au secrétariat juridique est centralisé dans un même lieu, auprès d'une
	personne ressource, de référents qui organisent tout cela.

**Educ 13**
	pour motion 2. Volonté de fusionner tout ça. Il faut qu'on ait la capacité
	de répondre, de conseiller les syndicats qui le demandent pour régler
	des conflits.

**Nettoyage**
	Proposition pour régler le problème juridique. Toute section syndicale peut
	prendre quelques heures pour aller aux prud'hommes, écouter ce que les
	avocats disent. Se développer dans ce sens là pour éviter qu'une seule
	personne se cogne le boulot.

**SIPM**
	Mandater une ou deux personnes veut dire que ces personnes conseilleront
	seulement les syndicats.

**CCS Rp**
	**proposer plus tard une motion de synthèse**

**Présidence**
    pas d'opposition donc OK


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°3           |            |          |            |                           |
| SIPM RP              |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


.. seealso::

   - :ref:`contre_motion_2_3_2010_ste93`
   - :ref:`contre_motion_2_3_2010_etpreci35`
   - :ref:`amendement_motions_2_3_2010_ste93`
