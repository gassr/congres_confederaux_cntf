

.. index::
   Elections paritaires

.. _motion_38_2010:

=================================================================================
Motion n°38 2010 : Représentativité et motion de synthèse 2008, STE 75 [rejetée]
=================================================================================





Argumentaire, Syndicat des travailleuses et des travailleurs de l'éducation à Paris
===================================================================================


Au dernier congrès confédéral (Lille 2008), la question de la participation des
syndicats des fonctions publiques aux élections professionnelles était restée
en suspens en attendant la nouvelle loi sur la représentativité syndicale.

La loi est tombée, et elle ne change rien pour la CNT dans le secteur public.
Nos droits sont conservés, sans restriction supplémentaire. Les inquiétudes
formulées par le congrès de Lille ne nous paraissent donc plus de mise.

Les élections servent à fournir des «représentants» des personnels dans
les commissions paritaires qui sont des instances consultatives.

Le dernier mot reste à l’administration. Notre argumentaire classique concernant
les élections paritaires et leurs effets pervers sur le syndicalisme restent valables.

Motion
======

La CNT reste sur sa position de refus de participation aux instances paritaires et aux
élections professionnelles dans les fonctions publiques.

- Les syndicats CNT font vivre le syndicalisme : se regrouper et lutter pour défendre
  des intérêts de classe communs. Ses pratiques syndicales de base sont la caisse
  de solidarité, la propagande, et la défense des travailleurs.
  Ses moyens sont les mandats tournants, la démocratie et l’action directe;
- Les droits ne sont jamais donnés aux travailleurs mais imposés par eux dans l'instauration
  d'un rapport de force ;
- Elle réaffirme la vocation du syndicat au quotidien et dans les luttes : un outil
  d'organisation, de réflexion, de formation et de défense des travailleurs, et non
  un but en soi ;
- Elle réaffirme que la grève et les assemblées générales souveraines restent les moyens
  privilégiés de lutte et d'émancipation des travailleurs contre les aliénations du salariat.


.. _vote_motion_38_2010:

Vote Motion n°38 : Représentativité et motion de synthèse 2008, STE 75
======================================================================

  
+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°38          | 22         | 45       | 3          | 2                         |
| STE 75               |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+


**Motion n°38 rejetée**

   - :ref:`motions_37_38_39_2010`
