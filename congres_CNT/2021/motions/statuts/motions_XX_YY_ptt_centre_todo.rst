.. index::
   ! Présomption d'innocence


.. _motion_XX_YY_2021_ptt_centre:

===========================================================
Motions XX + YY de nature statutaire par CNT PTT Centre
===========================================================

Courriel
==========


::

    ---------------------------- Message original ----------------------------
    Objet:   [Liste-syndicats] envoi de 2 motions
    De:      ptt-centre@cnt-f.org
    Date:    Dim 24 janvier 2021 15:34
    À:       liste-syndicats@bc.cnt-fr.org
    --------------------------------------------------------------------------

Aux syndicats de la CNT, à son Bureau Confédéral

Compagnons,

La date envisagée pour le Congrès Confédéral n'étant toujours pas fixée
définitivement, les syndicats devant être prévenus statutairement de cette
date 7 mois avant sa tenue pour laisser à chacun le temps nécessaire à
l'élaboration, l'envoi et la discussion de motions, notre syndicat demande
à ce que soit mis à l'ordre du jour du futur congrès les deux motions
suivantes,toutes deux à précéder de la mention :
"motions de nature statutaire ou à modifier ses règles organiques".

motion 1
==========

"la Confédération Nationale du Travail étant une Confédération de syndicats
et non pas d'individus : un syndicat doit être composé, pour être labellisé,
d'un minimum de 2 adhérents, le nombre maximum n'étant pas limité."


motion 2
==========

au Titre gestion des Conflits:

- Pour tout adhérent incriminé faisant l'objet d'un mise en cause
  personnelle, la CNT préservera dans toutes ses instances le principe
  de la présomption d'innocence.

- Les syndicats, sont seuls juges de la validité de l'adhésion, de
  son éventuelle  suspension ou de son exclusion d'un de leur membre en
  leur sein.
  La Confédération en tant que telle ne peut contrevenir à cette règle
  de base sans revenir sur le principe d'autonomie des syndicats.
  Si un conflit existe et n'est pas résolu entre les structures internes
  de la Confédération et en derniers recours il subsistera la possibilité,
  par décision de Congrès pour la Confédération de dé-labelliser et de
  désaffilier le ou les syndicats en question."

pour le syndicat CNT PTT Région Centre
le secrétaire
Serge Morisset
