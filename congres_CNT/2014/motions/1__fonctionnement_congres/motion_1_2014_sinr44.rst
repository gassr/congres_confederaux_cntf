
.. index::
   pair: Motion 1 ; 2014

.. _motion_1_2014_sinr44:

=================================================================================
Motion n°1 2014 : Application des motions de fonctionnement de congrès (SINR 44)
=================================================================================



Argumentaire
==============

Dans les :ref:`règles organiques <regles_organiques>`, rien n'est précisé
quant au moment où  s'appliquent les motions de fonctionnement d'un
Congrès confédéral qui viennent d'être adoptées.

Cela peut parfois donner lieu à de longs, fastidieux et inutiles débats
qui peuvent amputer le Congrès de temps permettant de traiter d'autres
motions qui auraient pu être précieuses pour la CNT.

L'idée est donc qu'à part certaines motions dont l'application immédiate
pourrait empêcher au Congrès de se tenir, toute motion de fonctionnement
de Congrès adoptée s'applique immédiatement.

Les syndicats déposant les motions et la commission de préparation de
Congrès étudient en amont cette applicabilité.

Motion
=======

Sauf mention explicite dans la motion, les motions de fonctionnement de
congrès confédéral, placées en début de cahier, s'appliquent immédiatement.



Contre motions
==============

.. seealso::

   - :ref:`contre_motion_1_2014_ess34`
   - :ref:`contre_motion_1_2014_interco25`

Votes
======


.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°1 2014

       :ref:`SINR44 <sinr44>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
