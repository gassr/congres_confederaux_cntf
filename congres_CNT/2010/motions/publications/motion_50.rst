

.. _motion_50_2010:

===================================================================
Motion n°50 2010 Mandat Editions CNT confédérales, INTERPRO CNT 31
===================================================================



Argumentaire
============

Nous avons toujours accordé beaucoup d'importance à la mémoire collective
des luttes sociales.

Pourtant nous constatons l'absence d'une structure d'Éditions confédérales.

Nous le regrettons malgré de nombreuses initiatives locales allant des
Éditions CNT RP ou de brochures éditées par les syndicats de Strasbourg
ou de Rennes par exemple.

Nous proposons donc de créer un secrétariat "Éditions CNT confédérales"
qui aurait pour comme objectif prioritaire de veiller à la ré-actualisation
des différentes brochures de la CNT (parfois indisponibles) et la
diffusion de petites brochures de vulgarisation à prix libre dans la mesure
de nos finances.

C'est normalement les mandatés à la propagande qui avaient cette responsabilité,
mais fort de notre expérience de ce mandat nous devons constater que sa gestion
quotidienne ne laisse pas la possibilité de développer ses brochures qui nous
semble être une nécessité pour la CNT.

Motion
======

La CNT se dote d'un secrétariat aux éditions CNT avec comme mandat prioritaire
de travailler à la réactualisation des différentes brochures de la CNT ainsi
qu'à leur diffusion et à leur acquisition par le plus grand nombre.

Les mandatés aux Editions CNT pourront aussi proposer de nouvelles publications
en s'attachant aux sujets d'actualités comme à l'histoire des luttes sociales
de façon équivalente. Ces mandatés participeront à la CA.


+----------------------+------------+----------+------------+---------------------------+
| Nom                  | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+======================+============+==========+============+===========================+
| Motion n°50          |            |          |            |                           |
| INTERPRO CNT 31      |            |          |            |                           |
+----------------------+------------+----------+------------+---------------------------+

.. seealso::

   - :ref:`amendement_motion_50_2010_educ91`
