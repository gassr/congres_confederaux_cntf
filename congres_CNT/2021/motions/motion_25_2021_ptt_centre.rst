.. index::
   pair: Motion 25 ; 2021

.. _motion_25_2021_ptt_centre:

================================================================================================================================================
Motion **n°25** 2021: **Conditions à la création d’un syndicat CNT** par **PTT Centre**
================================================================================================================================================

Autres motions PTT-Centre

    - :ref:`syndicats:motions_ptt_centre`

Motion
========

La Confédération Nationale du Travail étant une Confédération de syndicats
et non pas d'individus : un syndicat doit être composé, pour être labellisé,
d'un minimum de 2 adhérents, le nombre maximum n'étant pas limité.


Amendements
===========


Votes
======


+-----------------------------+------------+----------+------------+---------------------------+
| Nom                         | Pour       | Contre   | Abstention | Ne prend pas part au vote |
+=============================+============+==========+============+===========================+
| Motion n°25 2021 PTT Centre |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
| Décision du congrès         |            |          |            |                           |
|                             |            |          |            |                           |
+-----------------------------+------------+----------+------------+---------------------------+
