
.. index::
   pair: Motion 15 ; 2014

.. _motion_15_2014_interco71:

==============================================================================
Motion **n°15** 2014  **Opposition à la mutuelle obligatoire** (Interco 71)
==============================================================================

.. seealso::

   - :ref:`motions_interco71`





Argumentaire
==============

Une atteinte à la liberté de choisir sa propre Mutuelle, on ne demande
l'avis des salariés que pour choisir le niveau de cotisation.

Une inégalité entre les cotisations et prestations selon que ton
entreprise soit prospère ou misérable.

En cas de chômage, de licenciement, de départ en retraite ou de
fermeture de l'entreprise plus de Mutuelle.

L'adhésion à la Mutuelle est décidée par le patron et la Mutuelle
avec consultation des syndicats *représentatifs* sans consulter
les salarié(e)s.

Motion
=======

Pour toutes ces raisons la CNT est opposée à la mise en place de la
Mutuelle obligatoire et apportera son aide militante, juridique et
financière à ses adhérents et syndicats qui **refusent que la santé
soit cotée en Bourse**.


Amendements
===========

.. seealso::

   - :ref:`amendement_motion_15_2014_stics13`
   - :ref:`amendement_motion_15_2014_sse31`



Votes
======

.. list-table::
   :widths: 30 18 18 18 16
   :header-rows: 1

   * - Nom
     - Pour
     - Contre
     - Abstention
     - Ne prend pas part au vote
   * - Motion N°12 2014

       :ref:`Interco71 <interco71>`
     -
     -
     -
     -
   * - Décision du congrès

       Acceptée/Rejetée
     -
     -
     -
     -
